/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used as controller for ReOpenAnnualRoll aura component ASR-8088.
//-----------------------------
public class ReOpenAnnualRoll {

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method checks permissions before allowing user to ReOpen Annual Roll.          
    // @return : Response wrapper object
	@AuraEnabled
    public static Response checkPermission(Id recordId) {
		
        Response response = new Response();    
        Boolean hasPermission = false;
        List<Profile> profile = [SELECT Id, Name FROM Profile WHERE Name='System Administrator' or Name='Sapient Admin'];     
        if(UserInfo.getProfileId() == profile[0].id || UserInfo.getProfileId() == profile[1].id)hasPermission = true; 
        RollYear__c rollYear = [Select Status__c,IntegrationStatus__c,Year__c from RollYear__c where id =: recordId];    
        
        if (rollYear.Year__c != String.valueOf(system.today().year())) {
        //check if Roll Year is not current calendar year, if not return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.CALENDAR_ROLL_YEAR_BUTTON_WARNING ;
            return Response; 
        }else if(rollYear.Status__c != CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS){
            //Check if roll year is not closed, if not closed return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.OPENED_ROLL_YEAR_BUTTON_WARNING;        
            return Response;    
        }else if(rollYear.IntegrationStatus__c != CCSFConstants.SENT_TO_TTX) {
        //check if Integration status is not 'Sent to TTX', if not return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.INTEGRATION_STATUS_WARNING ;
            return Response; 
        }else if(!hasPermission) {
        //check if user has Custom Permission to run this batch, if not return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.NO_PERMISSION;      
            return Response;       
        }else {
        //if all the above condtions are false, means user can run this batch
            response.isSuccess = true;
            response.message = System.Label.HasPermissionReOpenAnnualRoll;     
            return Response;  
        }             
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method runs the ReOpenAnnualRoll batch.          
    // @return : Response wrapper object
    @AuraEnabled 
    public static Response runReOpenAnnualRoll() {
        
        Response response = new Response();        
        
        List<AsyncApexJob> apexJob = [Select Id, Status,ApexClass.name from AsyncApexJob 
											 where ApexClass.name =: CCSFConstants.DIRECTBILL_VESSEL_GENERATOR_BATCH_NAME
											 and Status IN : CCSFConstants.BATCHMSG.BATCH_STATUSES order by CreatedDate desc limit 1];
		
		//Check if the same batch is not already running.
        if(!apexJob.isEmpty())
        {
            response.isSuccess = false;
            response.message = CCSFConstants.BATCHMSG.BATCH_ALREADY_RUNNING_MSG;
        }else {            
            //calling Batch
            ReOpenAnnualRollBatch reOpenAnnualRollBatch = new ReOpenAnnualRollBatch();
            Id jobId = Database.executeBatch(reOpenAnnualRollBatch,Integer.valueOf(Label.ReOpenAnnualRollBatchSize));

            AsyncApexJob apexJobs = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                     TotalJobItems, CreatedBy.Email, ExtendedStatus
                                     from AsyncApexJob where Id = :jobId];
            
            if(apexJobs.Status != CCSFConstants.BATCHMSG.BATCH_STATUS_FAILED) {
                response.isSuccess = true;
                response.message= System.Label.BatchQueued + apexJobs.Status; 
            } else { 
                response.isSuccess = false;
                response.message = System.Label.BatchErrors + apexJobs.NumberOfErrors;
            }  
        }        
        return response;
    }
	
	//wrapper class for response
    public class Response {
        @AuraEnabled
        public Boolean isSuccess;
        @AuraEnabled
        public String message;
        
        public Response() {
            isSuccess =false;
            message='';
        }  
    }
}