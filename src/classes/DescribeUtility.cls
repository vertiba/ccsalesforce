/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Utility Class that can be used for all types of Schem.getGlobalDescribe functions.
//-----------------------------
public class DescribeUtility {
    public static String originLocation = 'DescribeUtility';
    /*
     * Method Name: createApexBatchLog
     * Description: This method will create a apexBatchLog
     */ 
    public static void createApexBatchLog(String asyncJobId,String batchClassName,String asyncParentJobId, Boolean areBatchParamsPresent,
                                          String queryParams,Integer asyncNumberOfErrors)
    {
        BatchApexLog__c batchLog = new BatchApexLog__c();
        try{
            batchLog.BatchClassName__c = batchClassName;
            batchLog.AreBatchParamsPresent__c = areBatchParamsPresent;
            batchLog.JobId__c = asyncJobId;
            batchLog.AsyncParentJobId__c = asyncParentJobId;
            if(asyncNumberOfErrors > 0){
                batchLog.status__c = 'Failed';
            }else if(asyncNumberOfErrors == 0){
                batchLog.status__c = 'Completed';
            }
            batchLog.QueryParams__c = queryParams;
            insert batchLog;
        }catch(Exception ex){
            system.debug('exception in utility-->'+ex.getMessage());
        }
        
        system.debug('batchLog-->'+batchLog);
    }
    /**
     * Method takes a object and a field name API. If field is picklist then picklistentries are
     * returned, else null is returned.
     */
    public static List<Schema.PicklistEntry> getPicklistValues(String sObjectName, String fieldName){
        Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
        if(fieldMap.get(fieldName).getDescribe().getType() == Schema.DisplayType.PICKLIST){
            return fieldMap.get(fieldName).getDescribe().getPickListValues();
        }
        return null;
    }

    /**
     * Lightning page can pass values to attributes in lightning components and they can be passed dynamically,
     * and can be made configurable. Like all the picklist values in a field can be passed.
     * This method takes an object, field name and convert the picklist values into
     * visualeditor.dynmiacpicklistrows which is used in lightning app builder.
     */
    public static VisualEditor.DynamicPickListRows getDynPickListRows(String sObjectName, String fieldName){
        VisualEditor.DynamicPickListRows dynamicPickListRows = new VisualEditor.DynamicPickListRows();
        List <Schema.PicklistEntry> pickListEntries = getPicklistValues(sObjectName,fieldName);
        if(pickListEntries == null || pickListEntries.isEmpty()) {
            return null;
        }
        for(Schema.PicklistEntry picklistEntry : pickListEntries) {
            dynamicPickListRows.addRow(new VisualEditor.DataRow(picklistEntry.getLabel(), picklistEntry.getValue()));
        }
        return dynamicPickListRows;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : objectName
    // @description : Get all the fields that for this object. Create a
    // 			SOQL statement with all the creatable fields and return it
    // @return : String
    //-----------------------------
    public static string getCreatableFieldsSOQL(String objectName){
        // Get a map of field name and fieldToken
        Map<String, Schema.SObjectField> sobjectFieldByAPIName = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        list<string> creatableFields = new list<string>();
        if(sobjectFieldByAPIName != null){
            for(Schema.SObjectField fieldToken : sobjectFieldByAPIName.values()){
                Schema.DescribeFieldResult field = fieldToken.getDescribe();
                if(field.isCreateable()){ // field is creatable
                    creatableFields.add(field.getName());
                }
            }
        }
        //At least name and Id will be returned
        return 'select ' + String.join(creatableFields, ', ') + ' from ' + objectName ;
    }
    
     //-----------------------------
    // @author : Publicis.Sapient
    // @param : Schema Sobject Type of Parent and Child
    // @description : Get all the fields of Parent and Child object. Create a
    // 			SOQL statement with all the fields and return it
    // @return : String
    //-----------------------------
     // Building query to include all fields of Parent object and Child object Dynamically 
    public static String buildQueryToFetchAllDetailsOfParentandChild(Schema.SObjectType Parent, Schema.SObjectType ChildName){
    
        String ParentName = String.valueof(Parent);
        String ChildsObjectName = String.valueOf(ChildName);
        String ChildReltationName;
        
        Schema.DescribeSObjectResult parentResult = Parent.getDescribe();
        for(Schema.ChildRelationship child : parentResult.getChildRelationships()) {
            if (child.getChildSObject().getDescribe().getName() == ChildName.getDescribe().getName() && child.getRelationshipName() != null) {
               ChildReltationName = String.valueOf(child.getRelationshipName());
            }
        }
   
        String queryTemplate ='Select {0},(Select {1} from '+ ChildReltationName + ') FROM '+ ParentName ;
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        
        // 1 - Get all Parent object fields
        Set<String> parentFieldsSet = globalDescribe.get(ParentName).getDescribe().fields.getMap().keyset();
        String parentFields = String.join(new List<String>(parentFieldsSet), ',');
        
        // 2 - Get all Child fields
        Set<String> childFieldsSet = globalDescribe.get(ChildsObjectName).getDescribe().fields.getMap().keyset();
        String childFields = String.join(new List<String>(childFieldsSet), ',');
        
        return String.format(queryTemplate, new List<String>{parentFields,childFields} );
    }
    
     //-----------------------------
    // @author : Publicis.Sapient
    // @param : sobjectType , recordTypeDeveloperName
    // @description : get recordtype id based on object and recordtype developer name
    // @return : Id
    //-----------------------------
    public static Id getRecordTypeId(Schema.SObjectType sobjectType, String recordTypeDeveloperName) {
        return sobjectType.getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName).getRecordTypeId();    
    }    
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method is used to insert sobject records.
    // @return : boolean , success or error on record insertion
    //-----------------------------
    public static Map<String,Object> insertRecords(List<SObject> records,Database.DMLOptions dmlOptions) {
        Map<String,Object> result = new Map<String,Object>();
        Boolean insertSuccess = true;
        //Get Database.insert results

        List<Database.SaveResult> insertResults = dmlOptions != null ? Database.insert(records, dmlOptions) : Database.insert(records, true);

        for(Integer i=0;i<insertResults.size();i++) {
            if (!insertResults.get(i).isSuccess()) {
                insertSuccess = false;
                // DML operation failed
                Database.Error error = insertResults.get(i).getErrors().get(0);
                Id recordId = records[i].getCloneSourceId();
                //check if the records which is getting inserted is cloned from some other record, in case of exception tag parent clone record to error
                if(recordId !=null){
                    String sObjName = recordId.getSObjectType().getDescribe().getName();
                    switch on sObjName {
                        when 'Case' {
                            Case failedAssessment = new Case(Id=recordId);
                            Logger.addRecordExceptionEntry(failedAssessment, new SaveResultException(CCSFConstants.errorInsertingCloneRecord+error.getMessage()),originLocation);
                        }
                        when 'AssessmentLineItem__c' {
                            AssessmentLineItem__c failedAssessmentLineItem = new AssessmentLineItem__c(Id=recordId);
                            Logger.addRecordExceptionEntry(failedAssessmentLineItem, new SaveResultException(CCSFConstants.errorInsertingCloneRecord+error.getMessage()),originLocation);
                        }
                        when else {
                            Logger.addExceptionEntry(new SaveResultException('error: ' + error.getMessage()), originLocation);
                        }
                    }
                }
                else {
                    Logger.addExceptionEntry(new SaveResultException(CCSFConstants.errorInsertingRecord+error.getMessage()), originLocation);
                }
                Logger.addExceptionEntry(new SaveResultException('error: ' + error.getMessage()), originLocation);
            }
        } 
        result.put('insertSuccess',insertSuccess);
        result.put('insertResults',insertResults);
        return result;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : String
    // @description : get objectName by passing string
    // @return : String
    //-----------------------------
     
    public static String getObjectName(String objectName){
        
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getName();
        
    }
    // @author : Publicis.Sapient
    // @description : This method is used to update sobject records.
    // @return : boolean , success or error on record updation
    //-----------------------------
    public static Map<String,Object> updateRecords(List<SObject> records,Database.DMLOptions dmlOptions) {
        Map<String,Object> result = new Map<String,Object>();
        Boolean updateSuccess = true;
        //Get Database.update results
        List<Database.SaveResult> updateResults = dmlOptions != null ? Database.update(records, dmlOptions) : Database.update(records, true);

        for(Integer i=0;i<updateResults.size();i++) {
            Id recordId = records.get(i).Id;
            if (!updateResults.get(i).isSuccess()) {
                updateSuccess = false;
                // DML operation failed                
                Database.Error error = updateResults.get(i).getErrors().get(0);
                String sObjName = recordId.getSObjectType().getDescribe().getName();
                switch on sObjName {
                    when 'Case' {
                        Case failedAssessment = new Case(Id=recordId);
                        Logger.addRecordExceptionEntry(failedAssessment, new SaveResultException(CCSFConstants.errorUpdatingRecord+error.getMessage()),originLocation);
                    }
                    when 'AssessmentLineItem__c' {
                        AssessmentLineItem__c failedAssessmentLineItem = new AssessmentLineItem__c(Id=recordId);
                        Logger.addRecordExceptionEntry(failedAssessmentLineItem, new SaveResultException(CCSFConstants.errorUpdatingRecord+error.getMessage()),originLocation);
                    }
                    when else {
                        Logger.addExceptionEntry(new SaveResultException('error: ' + error.getMessage()), originLocation);
                    }
                }
            }
        } 
        result.put('updateSuccess',updateSuccess);
        result.put('updateResults',updateResults);
        
        return result;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method is used to return result of insert sobject records.
    // @return : map with , success or error on record insertion and success and error record list
    //-----------------------------
    public static Map<String,Object> getInsertRecordsResults(List<SObject> records,Database.DMLOptions dmlOptions, String originLocation) {
        Map<String,Object> result = new Map<String,Object>();
        Boolean insertSuccess = true;
        Set<Id> successIds = new Set<Id>();
        List<SObject> successRecords = new List<SObject>();
        List<SObject> errorRecords = new List<SObject>();
        
        //Get Database.insert results
        List<Database.SaveResult> insertResults = dmlOptions != null ? Database.insert(records, dmlOptions) : Database.insert(records, false);
        
        for(Integer i = 0; i < insertResults.size(); i++){
            if(insertResults.get(i).isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                successIds.add(insertResults.get(i).getId());
            } 
            else {
                // DML operation failed
                Database.Error error = insertResults.get(i).getErrors().get(0);
                Id recordId = records[i].getCloneSourceId();
                //check if the records which is getting inserted is cloned from some other record, in case of exception tag parent clone record to error
                if(recordId !=null){
                    String sObjName = recordId.getSObjectType().getDescribe().getName();
                    System.debug('sObjName+++'+sObjName);
                    switch on sObjName {
                        when 'Case' {
                            Case failedAssessment = new Case(Id=recordId);
                            Logger.addRecordExceptionEntry(failedAssessment, new SaveResultException(CCSFConstants.errorInsertingCloneRecord+error.getMessage()),originLocation);
                        }
                        when 'AssessmentLineItem__c' {
                            AssessmentLineItem__c failedAssessmentLineItem = new AssessmentLineItem__c(Id=recordId);
                            Logger.addRecordExceptionEntry(failedAssessmentLineItem, new SaveResultException(CCSFConstants.errorInsertingCloneRecord+error.getMessage()),originLocation);
                        }
                        when else {
                            Logger.addExceptionEntry(new SaveResultException('error: ' + error.getMessage()), originLocation);
                        }
                    }
                }
                else {
                    Logger.addExceptionEntry(new SaveResultException(CCSFConstants.errorInsertingRecord+error.getMessage()), originLocation);
                }
            }                          
        }
       
        for (SObject record : records) {
            if (successIds.contains(record.Id)) {
                successRecords.add(record);
            } else {
                errorRecords.add(record);
            }
        }
        if(errorRecords.size()>0) {
           insertSuccess = false;
        }
        result.put('insertSuccess',insertSuccess);
        result.put('successRecords',successRecords);
        result.put('errorRecords',errorRecords);
        return result;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method is used to return result of update sobject records.
    // @return : map with , success or error on record updtions and success and error record list
    //-----------------------------
    public static Map<String,Object> getUpdateRecordsResults(List<SObject> records,Database.DMLOptions dmlOptions, String originLocation) {
        Map<String,Object> result = new Map<String,Object>();
        Boolean updateSuccess = true;
        List<SObject> successRecords = new List<SObject>();
        List<SObject> errorRecords = new List<SObject>();

        //Get Database.insert results
        List<Database.SaveResult> updateResults = dmlOptions != null ? Database.update(records, dmlOptions) : Database.update(records, false );
        
        for(Integer i = 0; i < updateResults.size(); i++) {
            Id recordId = records.get(i).Id;
            if(updateResults.get(i).isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                successRecords.add(records.get(i));
            } 
            else {                
                // DML operation failed
                errorRecords.add(records.get(i));
                Database.Error error = updateResults.get(i).getErrors().get(0);
                String sObjName = recordId.getSObjectType().getDescribe().getName();
                switch on sObjName {
                    when 'Case' {
                        Case failedAssessment = new Case(Id=recordId);
                        Logger.addRecordExceptionEntry(failedAssessment, new SaveResultException(CCSFConstants.errorUpdatingRecord+error.getMessage()),originLocation);
                    }
                    when 'AssessmentLineItem__c' {
                        AssessmentLineItem__c failedAssessmentLineItem = new AssessmentLineItem__c(Id=recordId);
                        Logger.addRecordExceptionEntry(failedAssessmentLineItem, new SaveResultException(CCSFConstants.errorUpdatingRecord+error.getMessage()),originLocation);
                    }
                    when else {
                        Logger.addExceptionEntry(new SaveResultException('error: ' + error.getMessage()), originLocation);
                    }
                }
            }                                   
        }

        if(errorRecords.size()>0) {
            updateSuccess = false;
        }
        result.put('updateSuccess',updateSuccess);
        result.put('successRecords',successRecords);
        result.put('errorRecords',errorRecords);
        return result;
    }


    //-----------------------------
    // @author : Publicis Sapient 
    // @param : ASR-7736 String fieldSetName, String objectName
    // @description : This method takes feildsetName and Sobject Name and returns List<Schema.FieldSetMember>
    // @return : List<Schema.FieldSetMember>
    //-----------------------------

    public static List<Schema.FieldSetMember> getFieldSet(String fieldSetName, String objectName) {
        Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType sObjectTypeObject = globalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult describeSObjectResultObject = sObjectTypeObject.getDescribe();
        Schema.FieldSet fieldSetObj = describeSObjectResultObject.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    }  

    //-----------------------------
    // @author : Publicis Sapient 
    // @param : 
    // @description : This method takes Strings Object and Field API Name and returns Set<String>
    // @return : String
    //-----------------------------

    public static Set<String> getPicklistAndValues(String objectName,String fieldName){

        Set<String> picklistByValue = new Set<String>();
        Schema.sObjectType objectApiName = Schema.getGlobalDescribe().get(objectName);
	    Schema.DescribeFieldResult fieldResult = ObjectApiName.getDescribe().fields.getMap().get(fieldName).getDescribe();		
		List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry entry : pickListEntries){
            picklistByValue.add(entry.getValue());
                      
		}   
		
		return picklistByValue;

    }

     // Inner class for exceptions
     private class SaveResultException extends Exception {}

    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String,String
    // @description : This method takes sobjectApi name and filters and returns String query
    // @return : String
    //-----------------------------

    public static String getDynamicQueryString(String sobjectAPIName, String filters){
        String query='';
      
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        Set<String> fieldName = globalDescribe.get(sobjectAPIName).getDescribe().fields.getMap().keyset();
        String fieldsQuery = String.join(new List<String>(fieldName), ',');

        if(String.isNotBlank(filters)){
            query = 'SELECT ' + fieldsQuery + ' FROM ' + sobjectApiName +' WHERE '+ filters;
        }else{
            query = 'SELECT ' + fieldsQuery + ' FROM ' + sobjectApiName ;
        }       
        return query;
    }

    // @author : Publicis.Sapient
    // @description : This method is used to upsert sobject records.
    // @return : boolean , success or error on record updation
    //-----------------------------
    public static Map<String,Object> upsertRecords(List<SObject> records) {
        Map<String,Object> result = new Map<String,Object>();
        Boolean upsertSuccess = true;
        //Get Database.upsert results
        
        List<Database.UpsertResult> upsertResults = Database.upsert(records);

        
        for(Integer i=0;i<upsertResults.size();i++) {
            if (!upsertResults.get(i).isSuccess()) {
                upsertSuccess = false;
                break;
            }
        } 
        result.put('upsertSuccess',upsertSuccess);
        result.put('upsertResults',upsertResults);
        
        return result;
    }

    // @author : Publicis.Sapient
    // @description : This method is used to build query for getting fields of an assessment and its parent assessment for submit for approval screen.
    // @return : String
    //-----------------------------
    public static String buildQueryToFetchDetailsOfAssessmentandParentAssessment() {
        String  query =  'SELECT ID,CaseNumber,Type,Subtype__c,AdjustmentType__c,ParentAssessmentNumber__c,SequenceNumber__c,AssessmentNumber__c,Status,SubStatus__c,Roll__c,RollYear__c,';
                query += 'BasisFixtureValue__c,TotalPersonalPropertyValue__c,BasisPersonalPropertyValue__c,AssessmentYear__c,TotalAssessedValue__c,VesselTotalValueOverride__c,';
                query += 'BasisExemptAmount__c,RecordType.Name,RecordTypeId,Property__c,Parent.Type,Property__r.id,TotalFixturesValue__c,TotalFixturesValueOverride__c,CalculatedAmountExempt__c,ExemptAmountManual__c,';
                query += 'TotalPersonalPropertyValueOverride__c,MinimumCapAssessedValue__c,Parent.BasisExemptAmount__c,';
                query += 'Penalty__r.Name,InterestCode__c,Parent.Status,Parent.AssessmentNumber__c,AdjustmentReason__c,TotalAssessedCost__c,TotalCounterlinesPartitionsectValue__c,';
                query += 'RollCode__c,ParentId,Parent.CaseNumber,Parent.TotalPersonalPropertyValue__c,Parent.TotalFixturesValue__c,OriginalTotalAssessedValue__c,';
                query += 'Parent.ExemptAmountManual__c,Parent.CalculatedAmountExempt__c,Parent.TotalAssessedValue__c,Parent.TotalAssessedCost__c,TotalATMsCost__c,Parent.TotalATMsCost__c,';
                query += 'TotalATMsValue__c,Parent.TotalATMsValue__c,TotalConstructioninProgressCost__c,Parent.TotalConstructioninProgressCost__c,';
                query += 'TotalConstructioninProgressValue__c,Parent.TotalConstructioninProgressValue__c,TotalCounterlinesPartitionsetcCost__c,';
                query += 'Parent.TotalCounterlinesPartitionsetcCost__c,Parent.TotalCounterlinesPartitionsectValue__c,TotalCarpetsDrapesCost__c,';
                query += 'Parent.TotalCarpetsDrapesCost__c,TotalCarpetsDrapesValue__c,Parent.TotalCarpetsDrapesValue__c,TotalDriveupWalkupKioskValue__c,';
                query += 'Parent.TotalDriveupWalkupKioskValue__c,TotalDriveupWalkupWindowKioskCost__c,Parent.TotalDriveupWalkupWindowKioskCost__c,';
                query += 'TotalFurnitureAppliancesValue__c,Parent.TotalFurnitureAppliancesValue__c,TotalFurnitureandAppliancesCost__c,Parent.TotalFurnitureandAppliancesCost__c,';
                query += 'TotalLandDevelopmentValue__c,Parent.TotalLandDevelopmentValue__c,TotalLandandLandDevelopmentCost__c,';
                query += 'Parent.TotalLandandLandDevelopmentCost__c,TotalLandImprovementsCost__c,Parent.TotalLandImprovementsCost__c,TotalLandImprovementsValue__c,';
                query += 'Parent.TotalLandImprovementsValue__c,TotalLeasedEquipmentCost__c,Parent.TotalLeasedEquipmentCost__c,TotalLeasedEquipmentValue__c,Parent.TotalLeasedEquipmentValue__c,';
                query += 'TotalLeaseholdImprovementsFixCost__c,Parent.TotalLeaseholdImprovementsFixCost__c,TotalLeaseholdImprovementsFixValue__c,';
                query += 'Parent.TotalLeaseholdImprovementsFixValue__c,TotalLeaseholdImprovementsStrCost__c,Parent.TotalLeaseholdImprovementsStrCost__c,TotalLeaseholdImprovementsStrValue__c,';
                query += 'Parent.TotalLeaseholdImprovementsStrValue__c,TotalLANMainframesCost__c,Parent.TotalLANMainframesCost__c,TotalLANMainframesValue__c,Parent.TotalLANMainframesValue__c,';
                query += 'TotalMachineryEquipmentCost__c,Parent.TotalMachineryEquipmentCost__c,TotalMachineryEquipmentValue__c,Parent.TotalMachineryEquipmentValue__c,';
                query += 'TotalOfficeFurnitureEquipmentCost__c,Parent.TotalOfficeFurnitureEquipmentCost__c,TotalOfficeFurnitureEquipmentValue__c,Parent.TotalOfficeFurnitureEquipmentValue__c,';
                query += 'TotalOtherEquipmentCost__c,Parent.TotalOtherEquipmentCost__c,TotalOtherEquipmentValue__c,Parent.TotalOtherEquipmentValue__c,';
                query += 'TotalOtherFurnitureEquipmentCost__c,Parent.TotalOtherFurnitureEquipmentCost__c,TotalOtherFurnitureEquipmentValue__c,Parent.TotalOtherFurnitureEquipmentValue__c,';
                query += 'TotalPersonalComputersCost__c,Parent.TotalPersonalComputersCost__c,TotalPersonalComputersValue__c,Parent.TotalPersonalComputersValue__c,';
                query += 'TotalSignsCameraTVEquipmentetcCost__c,Parent.TotalSignsCameraTVEquipmentetcCost__c,TotalSignsCameraTVetcValue__c,';
                query += 'Parent.TotalSignsCameraTVetcValue__c,TotalSuppliesValue__c,Parent.TotalSuppliesValue__c,TotalSuppliesReportedCost__c,Parent.TotalSuppliesReportedCost__c,';
                query += 'TotalToolsMoldsDiesJigsCost__c,Parent.TotalToolsMoldsDiesJigsCost__c,TotalToolsMoldsDiesJigsValue__c,Parent.BasisFixtureValue__c,Parent.BasisPersonalPropertyValue__c,';
                query += 'Parent.TotalToolsMoldsDiesJigsValue__c,TotalVaultDoorNightDepositoriesCost__c,Parent.TotalVaultDoorNightDepositoriesCost__c,TotalVaultDoorNightDepositoryValue__c,';
                query += 'Parent.TotalVaultDoorNightDepositoryValue__c,OriginalTotalPersonalPropertyValue__c,ValueDifferencePersonalProperty__c,TotalAmountExempt__c,OriginalFixturesValue__c,';
                query += 'NetAssessedValue__c,IntegrationStatus__c,ParentCancelCase__c,ParentCancelCase__r.Id,ParentCancelCase__r.AssessmentNumber__c,ParentCancelCase__r.ParentId,Parent.Subtype__c,IsLocked__c,IsLegacy__c,ParentRegularAssessment__c,';
                query += 'MinimumCapAsssessedValue__c,IsCorrectedOrCancelled__c,ExemptionType__c,ExemptionSubtype__c,ExemptionPenalty__c,ExemptionPenalty__r.Name,PercentExempt__c,AssessmentNumberSequence__c,NumberOfMonthsFor506Interest__c,Roll__r.Status__c,WaiveInterest__c FROM Case'; 
               
        return query;
    }
}