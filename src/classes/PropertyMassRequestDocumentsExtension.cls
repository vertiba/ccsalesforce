/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class PropertyMassRequestDocumentsExtension {

    private ApexPages.StandardSetController setController;
    private RestApi.ListViewResult listView;
    private List<Property__c> sampleRecords;
    private List<Property__c> processedProperties;
    private List<Property__c> problematicProperties;
    private static final String SOBJECT_NAME = Schema.Property__c.SObjectType.getDescribe().getName();
    public Boolean hasPermission; // Added for validating custom persmission for User
    public String recordStatus {get;set;}

    public PropertyMassRequestDocumentsExtension(ApexPages.StandardSetController setController) {
        this.setController = setController;
         //In VF page limit to 50 for display.
        this.sampleRecords = Database.query(this.getQuery()+' LIMIT 50');        
        this.processedProperties   = new List<Property__c>();
        this.problematicProperties = new List<Property__c>();
        this.hasPermission 	     = FeatureManagement.checkPermission(Label.MassGenerateDocumentRequest); 

    }

    //-----------------------------
    // @author : Publicis.Sapient    
    // @description : To Generate Notices for Vessel Properties    
    // @return : void
    //-----------------------------
    public void processSampleProperties() {
        if(!hasPermission){ recordStatus = CCSFConstants.BUTTONMSG.NO_PERMISSION; return;}
        Map<String, TH1__Document_Setting__c> documentSettingsBySObjectName = SmartCOMMUtility.getDocumentSettingsBySObjectName(SOBJECT_NAME);		
        TH1__Document_Setting__c documentSetting, emailDocumentSetting;
        for(Property__c property : this.sampleRecords) {
            if(property.RecordType.DeveloperName != 'Vessel') {
                problematicProperties.add(property);
            } else if(property.HasMailingAddress__c == false) {
                problematicProperties.add(property);
            } else if(property.RequiredToFile__c == null || property.RequiredToFile__c == 'No') {
                problematicProperties.add(property);
            } else if(property.Status__c == 'Inactive') {
                problematicProperties.add(property);
            } 
            else 
            {
                if(property.NoticeType__c == 'Direct Bill' && documentSettingsBySObjectName.containsKey(Label.PropertyDirectBillNotice576D)) {
                    processedProperties.add(property);
                } else if(property.NoticeType__c == 'Low Value' && documentSettingsBySObjectName.containsKey(Label.PropertyLowValueNotice576D)) {
                    processedProperties.add(property);
                } else if(property.NoticeType__c == 'Notice to File' && documentSettingsBySObjectName.containsKey(Label.PropertyNoticeOfRequirementToFile576D)) {
                processedProperties.add(property);
                } else {
                    problematicProperties.add(property);
                }
        }
    }
    }

    public RestApi.ListViewResult getListView() {
        if(this.listView != null && this.listView.id == this.setController.getFilterId()) return listView;
        String sobjectName = Schema.Property__c.SObjectType.getDescribe().getName();
        RestApi.ListViewResult listView = RestApi.getListView(sobjectName, this.setController.getFilterId());
        this.listView = listView;
        return listView;
    }

    public List<Property__c> getProcessedProperties() {
        return this.processedProperties;
    }

    public List<Property__c> getProblematicProperties() {
        return this.problematicProperties;
    }

    public String getQuery(){
        String queryPrefix = 'SELECT Id, Name, Account__c, Account__r.Name, HasMailingAddress__c, MailingAddress__c,NoticeType__c,';
        queryPrefix       += 'PrimaryFormType__c, Status__c,RecordType.DeveloperName,RequiredToFile__c FROM Property__c ';
        String listViewQuery;
        if(!Test.isRunningTest()){
            listViewQuery = this.getListView().query;
        }
        if(listViewQuery == null){
            listViewQuery = queryPrefix;
        } else {
            //getting where clause from list view otherwise all records are displayed and it is wrong.
            listViewQuery = queryPrefix+listViewQuery.substringAfter('FROM Property__c');
        }
        System.debug('listViewQuery+++ '+listViewQuery);
        return listViewQuery;
    }
    
    public PageReference generateNotices() {
        Database.executeBatch(new PropertyDocumentRequestGenerator(this.getQuery()));  
        String returnUrl = ApexPages.currentPage().getParameters().get('retURL');
        if(String.isEmpty(returnUrl)) {
            returnUrl = '/' + Schema.Property__c.SObjectType.getDescribe().getKeyPrefix();
        }
        return new PageReference(returnUrl);
    }

}