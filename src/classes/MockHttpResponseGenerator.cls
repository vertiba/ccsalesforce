/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/*-----------------------------
     @author : Publicis Sapient
     @param : String,Boolean
     @description : generate the Mock response for HTTP callout
     @return : String                              
     ----------------------------- */
@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
  public String serviceName;
  public String sObjectName;
  public Boolean isMockResponseSuccessful;  
    public MockHttpResponseGenerator(String serviceName,Boolean isMockResponseSuccessful) {
        this.serviceName=serviceName;   
        this.isMockResponseSuccessful=isMockResponseSuccessful;
    }
      
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
       String endPoint = req.getEndpoint();
         HttpResponse resp = new HttpResponse();
        if(serviceName == 'Case' && isMockResponseSuccessful == true){
            // Create a Mock response
            resp.setHeader('Content-Type', 'application/json');
            resp.setBody('{"label":"LIST_VIEW_LABEL","id":"00Br0000001SOxjEAG","developerName":"LIST_VIEW","sobjectType":"Case","soqlCompatible":true,"query":"SELECT Id FROM Case"}');
            resp.setStatusCode(200);
        }
        if(serviceName == 'DocumentGenerationRequest__c' && isMockResponseSuccessful == true){
            // Create a Mock response
            resp.setHeader('Content-Type', 'application/json');
            resp.setBody('{"label":"LIST_VIEW_LABEL","id":"00Br0000001SOxjEAG","developerName":"LIST_VIEW","sobjectType":"DocumentGenerationRequest__c","soqlCompatible":true,"query":"SELECT Id FROM DocumentGenerationRequest__c"}');
            resp.setStatusCode(200);
        }
        return resp;
    }
}