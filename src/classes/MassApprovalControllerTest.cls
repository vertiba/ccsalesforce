/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
public class MassApprovalControllerTest {

    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Date currentYearEventDate=  Date.newInstance(currentYear, 1, 1);

    @testSetup static void dataSet(){
        //Insert manager user
        Id profileId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
        Id cheifRoleId = [select Id,Name from UserRole where Name='BPP Chief'].Id;
        Id auditorRoleId= [select Id,Name from UserRole where Name='BPP Auditor'].Id;
        User mang = TestDataUtility.buildUser('Testing1', 'newuser@testccs1.com', 'newUse1', profileId, cheifRoleId);
        insert mang;
        
        // Insert a user and a assign manager to it.
        User user1 = TestDataUtility.buildUser('Testing', 'newuser@testccs.com', 'newUser', profileId, auditorRoleId);
        user1.ManagerID = mang.id;
        insert user1;
        
        System.runAs(user1){
        //insert roll years
        String lastFiscalYear    = String.valueOf(FiscalYearUtility.getCurrentFiscalYear() - 1);
        String currentFiscalYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());

        RollYear__c rollYearLastFiscalYear    = TestDataUtility.buildRollYear(lastFiscalYear, lastFiscalYear, CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS);
        RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(currentFiscalYear, currentFiscalYear, CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        insert new List<RollYear__c>{rollYearLastFiscalYear, rollYearCurrentFiscalYear};

        //insert account
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;

        //insert property
        List<Property__c> properties= new List<Property__c>();
        Property__c bppProperty = TestDataUtility.getBPPProperty();
        bppProperty.Account__c = businessAccount.Id;
        properties.add(bppProperty);

        Property__c bppProperty1 = TestDataUtility.getBPPProperty();
        bppProperty1.Account__c = businessAccount.Id;
        properties.add(bppProperty1);

        insert properties;

        //insert penalty
        List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty1 = TestDataUtility.buildPenalty(500, 10, System.Label.Late_Filer_Penalty_R_T_Code);
        penalties.add(penalty1);
        Penalty__c penalty2 = TestDataUtility.buildPenalty(500, 10, System.Label.Fraud_Penalty_R_T_Code);
        penalties.add(penalty2);
        insert penalties;

        List<Case> assessments = new List<Case>();

        Case assessment1  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                          rollYearCurrentFiscalYear.Id,rollYearCurrentFiscalYear.Name,rollYearCurrentFiscalYear.Name,
                                                          CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,currentYearEventDate);
        assessments.add(assessment1);

        Case assessment2  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty1.Id,
                                                          rollYearCurrentFiscalYear.Id,rollYearCurrentFiscalYear.Name,rollYearCurrentFiscalYear.Name,
                                                          CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,currentYearEventDate);

        assessments.add(assessment2);
        insert assessments;

        //insert assessment line item
        AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,assessment1.id);
        insert assessmentLineItem;

        DocumentGenerationRequest__c docgenerationrequest  = new DocumentGenerationRequest__c();
        docgenerationrequest.RelatedRecordType__c = 'Case';
        docgenerationrequest.Status__c = 'New';
        docgenerationrequest.RelatedRecordId__c = assessment1.id;
        insert docgenerationrequest;
        }
    }

    // Calling Controller when few are records selected
    @isTest
    static void massApprovedTest(){
        User user1 = [SELECT Id FROM User WHERE LastName = 'Testing'];
        
        System.runAs(user1){
            List<Case> processingRecords = approvalSubmit();
            // Set mock
            Test.setMock(HttpCalloutMock.class,new MockHttpResponseGenerator('Case',true));
            Test.startTest();
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(processingRecords);
            controller.setSelected(processingRecords);
            MassApprovalController extension = new MassApprovalController(controller);

            // Get Listview
            RestApi.ListViewResult listView = extension.getListView();
            extension.approveActionFunction();
            List<Case> processedRecords=[Select Id, Status from Case where Id in:processingRecords];
            List<Case> result= new List<Case>();
            for(Case record:processedRecords ){
                result.add(record);
            }
            test.stopTest();
            system.assertEquals('Closed', result[0].Status);
            String recordmsg= extension.recordStatus;
            system.assert(recordmsg != null);
        }
    }
    @isTest
    static void massUpdateTest(){

        Id profileId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
        Id cheifRoleId = [select Id,Name from UserRole where Name='BPP Chief'].Id;
        Id auditorRoleId= [select Id,Name from UserRole where Name='BPP Auditor'].Id;
        User mang = TestDataUtility.buildUser('Testing1', 'newuser@testccs1.com', 'newUse1', profileId, cheifRoleId);
        insert mang;
        User user1 = TestDataUtility.buildUser('Testing', 'newuser@testccs.com', 'newUser', profileId, auditorRoleId);
        user1.ManagerID = mang.id;
        insert user1;
        System.runAs(user1) {
            List<DocumentGenerationRequest__c> processingRecords = [SELECT ID FROM DocumentGenerationRequest__c];
            // Set mock
            Test.setMock(HttpCalloutMock.class,new MockHttpResponseGenerator('DocumentGenerationRequest__c',true));
            Test.startTest();
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(processingRecords);
            controller.setSelected(processingRecords);
            MassApprovalController extension = new MassApprovalController(controller);

            // Get Listview
            RestApi.ListViewResult listView = extension.getListView();
            extension.approveActionFunction();
            List<DocumentGenerationRequest__c> processedRecords=[Select Id, Status__c from DocumentGenerationRequest__c where Id in:processingRecords];
            List<DocumentGenerationRequest__c> result= new List<DocumentGenerationRequest__c>();
            for(DocumentGenerationRequest__c record:processedRecords ){
                result.add(record);
            }
            test.stopTest();
            system.assertEquals('Approved', result[0].Status__c);
            String recordmsg= extension.recordStatus;
            system.assert(recordmsg != null);
        }
    }
    // Calling for Batch if non is selected
    @isTest
    static void massApprovedBatchTest(){
		User user1 = [SELECT Id FROM User WHERE LastName = 'Testing'];

        System.runAs(user1) {
            List<Case> processingRecords = approvalSubmit();
            // Set mock
            Test.setMock(HttpCalloutMock.class,new MockHttpResponseGenerator('Case',true));
            Test.startTest();
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(processingRecords);
            MassApprovalController extension = new MassApprovalController(controller);
            // Get Listview
            RestApi.ListViewResult listView = extension.getListView();
            extension.approveActionFunction();
            test.stopTest();
            List<Case> processedRecords=[Select Id, Status from Case where Id in:processingRecords];
            List<Case> result= new List<Case>();
            for(Case record:processedRecords ){
                result.add(record);
            }
            system.assertEquals('Closed', result[0].Status);
            String recordmsg= extension.recordStatus;
            system.assert(recordmsg.contains('the batch is in processing for Mass Approval'));
            system.assert(recordmsg != null);
        }
    }


    static List<sobject> approvalSubmit(){
        Approval.ProcessSubmitRequest [] requestList = new Approval.ProcessSubmitRequest []{};
        for(Case record :[Select Id,OwnerId,Type,Status,SubStatus__c from Case]){
            // Create an approval request
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setProcessDefinitionNameOrId('BPP_Assessment_Route_to_Manager');
            req1.setSubmitterId(record.OwnerId);
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(record.id);
            requestList.add(req1);
        }

        List<Id> submitRecords = new List<Id>();
        if(requestList.size()>0){
            User user1 = [SELECT Id FROM User WHERE LastName = 'Testing'];
    
            System.runAs(user1) {
            List<Approval.ProcessResult> result = Approval.process(requestList, false);
            System.assert(result[0].isSuccess());
            }    
        }

        for(Approval.ProcessSubmitRequest apr : requestList){
            submitRecords.add(apr.getObjectId());
        }
        List<Case> processingRecords = [Select Id, OwnerId,Type,Status,SubStatus__c from Case where Id In : submitRecords];
        return processingRecords;
    }
}