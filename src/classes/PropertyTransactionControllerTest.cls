/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: PropertyTransactionControllerTest 
 * Description : Test Class for PropertyTransactionController Class
*/
@isTest
public class PropertyTransactionControllerTest {

    /*
     * Method Name: propertyTransactionTest
     * Description: This method tests all the functions in the PropertyTransactionController class 
     */ 
    public static TestMethod void propertyTransactionTest(){
        Account accountRecord = new Account();
        accountRecord.Name='New Test Account';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord.EntityId__c = '654321';
        accountRecord.BusinessStatus__c='active';
        database.insert(accountRecord);
        
        Property__c propertyRecord = new Property__c();
        propertyRecord.Name = 'new test property';
        propertyRecord.PropertyId__c = '153536';
        propertyRecord.MailingCountry__c = 'US';
        propertyRecord.Account__c = accountRecord.Id;
        database.insert(propertyRecord);
        
        Ownership__c ownership = new Ownership__c();
        ownership.StartDate__c = system.today();
        ownership.Account__c = accountRecord.Id;
        database.insert(ownership);

        PropertyTransaction__c propertyTransaction = new PropertyTransaction__c();
        propertyTransaction.Property__c = propertyRecord.Id;
        propertyTransaction.PurchaseAmount__c = 200;
        
        Test.startTest();
        PropertyTransactionController.getCurrentOwners(propertyRecord.Id);
        PropertyTransactionController.searchAccounts(propertyRecord.Id, 'Test');
        PropertyTransactionController.saveData(propertyTransaction, new List<Ownership__c>{ownership}, new List<SObject>{accountRecord});
        system.assertEquals(true, propertyTransaction.Id!=null);
        Test.stopTest();
    }
}