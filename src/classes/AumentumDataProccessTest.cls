/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @description : ASR:2366 This class is used to AumentumDataProccess .
//-----------------------------
@isTest
public with sharing class AumentumDataProccessTest {
   
    @TestSetup
    static void dataForTest(){
        List<Account> accounts= new List<Account>();
        Account account1= TestDataUtility.buildAccountAumentum('FCL LOUISE','2340028','45-4766086', Date.newInstance(1990,09,30), null);
        Account account2= TestDataUtility.buildAccountAumentum('SG Indutries', '2340029','45-4766087',Date.newInstance(1993,09,30),null);
        Account account3=TestDataUtility.buildAccountAumentum( 'DKSHVN ELEC CORP','2340030','45-4766088',Date.newInstance(1968,10,10), Date.newInstance(1993,09,30));
        Account account4= TestDataUtility.buildAccountAumentum('BYJUS INC', '2340038',	'45-4766096',Date.newInstance(1908,10,10),null);
        Account account5= TestDataUtility.buildAccountAumentum('UNI COLORS INC','2340044','45-4766101',Date.newInstance(1968,10,10),null);		
        Account account6= TestDataUtility.buildAccountAumentum('UNI COLORS','2340045','45-4766100',Date.newInstance(1998,10,10),null);		
        Account account7= TestDataUtility.buildAccountAumentum('FIJITSU UNITED CO','2304505','45-4766100',Date.newInstance(1918,10,10),null);		
        Account account8= TestDataUtility.buildAccountAumentum('TIGI PROPERTIES','2304507','45-4766909',Date.newInstance(1978,10,10),null);		
        Account account9= TestDataUtility.buildAccountAumentum('SKCROWN BUILDING CO','2340041','45-4766099',Date.newInstance(1928,10,10),null);		
        Account account10= TestDataUtility.buildAccountAumentum('SKCROWN BUILDING','2340041','45-4766099',Date.newInstance(1938,10,10),null);
        Account account11= TestDataUtility.buildAccountAumentum('UNI COLORS INC','2340044','45-4766101',Date.newInstance(1908,10,10),null);		
        Account account12= TestDataUtility.buildAccountAumentum('UNI COLORS','2340045','45-4766100',Date.newInstance(1908,10,10),null);	
        Account account13= TestDataUtility.buildAccountAumentum('GPC CONTRACTORS INC','2340000','45-4766101',Date.newInstance(1908,10,10),null);
        Account account14= TestDataUtility.buildAccountAumentum('GPC CONTRACTORS INC','2340000','45-4766101',Date.newInstance(1908,10,10),null);
        Account account15= TestDataUtility.buildAccountAumentum('GPC CONTRACTOR','2340056','45-4766095',Date.newInstance(1908,10,10),null);
        Account account16= TestDataUtility.buildAccountAumentum('GPC CONTRACT','2340037','45-4766096',Date.newInstance(1908,10,10),null);
        Account account17= TestDataUtility.buildAccountAumentum('GPC CONTRACT','2340038','45-4766096',Date.newInstance(1908,10,10),Date.newInstance(2000,10,10));
        account17.BusinessStatus__c = CCSFConstants.ACCOUNT.BUSINESS_STATUS_INACTIVE;
        Account account18= TestDataUtility.buildAccountAumentum('GPC CONTRACTOR','6340038','55-4766096',Date.newInstance(1908,10,10),null);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        accounts.add(account4);
        accounts.add(account5);
        accounts.add(account6);
        accounts.add(account7);
        accounts.add(account8);
        accounts.add(account9);
        accounts.add(account10);
        accounts.add(account11);
        accounts.add(account12);
        accounts.add(account13);
        accounts.add(account14);
        accounts.add(account15);
        accounts.add(account16);
        accounts.add(account17);
        accounts.add(account18);
        insert accounts;

        List<Property__c> properties= new List<Property__c>();
        Property__c property1 =	TestDataUtility.buildAumentumProcessProperty('2340028-00-000','FCL WAREHOUSE','571-L',Date.newInstance(1900,10,10)	,null	,account1.Id	,'1 CHASE MANHATTAN PLZ 23TH FLR'	,'2799'	,null	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94115'	,'2513');
        Property__c property2 =	TestDataUtility.buildAumentumProcessProperty('2340029-02-001','SG INDUSTRIES HEADOFFICE'	,'571-L',Date.newInstance(1956,10,10)	,null	,account2.Id	,'Market '	,'115'	,'ST'	,null	,null	,null	,'SAN FRANCISCO','US-CA','94115','2513');
        Property__c property3 =	TestDataUtility.buildAumentumProcessProperty('2340029-02-707','DKSHVN ELEC CORP OFFICE','571-L'	,Date.newInstance(1911,10,10)	,null	,account3.Id	,'Market '	,'1157'	,'ST'	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94115'	,'2513');
        Property__c property4=	TestDataUtility.buildAumentumProcessProperty('2340029-02-900','ELEC CORP OFFICE','571-STR'	,Date.newInstance(1908,10,10)	,null	,account4.Id 	,'6700 LAWRENCE ST STOP 21'	,null	,null	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94110'	,'4107');
        Property__c property5=	TestDataUtility.buildAumentumProcessProperty('2340045-06-171','TIGI AGENCY','571-L',Date.newInstance(1978,10,10),	null	,account8.Id	,'4310 Montgomery HWY 67 BSMT'	,null	,null	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94107'	,null);
        Property__c property6=	TestDataUtility.buildAumentumProcessProperty('2340045-06-172','TIGI AGENCY','571-L',Date.newInstance(1978,10,10),	null	,account8.Id	,'4310 Montgomery HWY 67 BSMT'	,null	,null	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94107'	,null);
        Property__c property7=	TestDataUtility.buildAumentumProcessProperty('2340042-02-007','FIJITSU CORPORATE OFFICE'	,'571-STR',Date.newInstance(1968,10,10)	,null	,account7.id	,'3700 HWY 280-431 N'	,null	,'ST'	,null	,null	,null	,'SAN FRANCISCO','US-CA'	,'94107'	,'1123');
        Property__c property8 = TestDataUtility.buildAumentumProcessProperty('2340042-02-006', 'Candy Crush 1', '571-STR', null, null, account12.Id, '1 AVENUE OF THE PALMS #3041650AVE',null, null, null, null, null, 'SAN FRANCISCO', 'US-CA', '94124', '1143');
        Property__c property9 = TestDataUtility.buildAumentumProcessProperty('2340042-02-008', 'Candy Crush 2', '571-R', null, null, account11.Id, '0 BROADWAY & DAVIS (480-23)BLVD',null, null, null, null, null, 'SAN FRANCISCO', 'US-CA', '94118', '3305');
        Property__c property10 = TestDataUtility.buildAumentumProcessProperty('2340042-02-009', 'Candy Crush 3', '571-R', null, null, account11.Id, '0 BROADWAY & DAVIS (480-23)BLVD',null, null, null, null, null, 'SAN FRANCISCO', 'US-CA', '94118', '3305');
        Property__c property11 = TestDataUtility.buildAumentumProcessProperty('2340042-02-010', 'Candy Crush 3', '571-R', null, null, account10.Id, 'DAVIS (480-23)BLVD',null, null, null, null, null, 'SAN FRANCISCO', 'US-CA', '94118', '3305');
        Property__c property12 = TestDataUtility.buildAumentumProcessProperty('2340042-02-011', 'Candy Crush 3', '571-R', null, null, account10.Id, 'DAVIS (480-23)BLVD',null, null, null, null, null, 'SAN FRANCISCO', 'US-CA', '94118', '3305');
        Property__c property13 = TestDataUtility.buildAumentumProcessProperty('2340042-02-012', 'GPC CONTRACT', '571-L', null, null, account17.Id, 'GC BLVD',null, null, null, null, null, 'SAN FRANCISCO', 'US-CA', '94118', '3305');
        Property__c property14 = TestDataUtility.buildAumentumProcessProperty('2340042-03-011', 'GPC CONTRACTOR', '571-L', null, null, account18.Id, null ,null, null, null, null, null, 'SAN FRANCISCO', 'US-CA', '94118', '3305');
        Property__c property15 = TestDataUtility.buildAumentumProcessProperty('2340042-04-011', 'GPC CONTRACTOR', '571-L', null, null, account18.Id, null,null, null, null, null, null, 'SAN FRANCISCO', 'US-CA', '94118', '3305');
        
        properties.add(property1);
        properties.add(property2);
        properties.add(property3);
        properties.add(property4);
        properties.add(property5);
        properties.add(property6);
        properties.add(property7);
        properties.add(property8);
        properties.add(property9);
        properties.add(property10);
        properties.add(property11);
        properties.add(property12);
        properties.add(property13);
        properties.add(property14);
        properties.add(property15);
        insert properties;

    }
    @isTest
    static void aumentumProcessTest1(){
        
        List<StagingAumentumBusiness__c> records = Test.loadData(StagingAumentumBusiness__c.sObjectType, 'AumentumProcessTestData');
        List<StagingAumentumBusiness__c> cleanedRecords= new List<StagingAumentumBusiness__c>();
        List<Id> stagingIds= new List<Id>();
        for(StagingAumentumBusiness__c record:records){
            record.IsDataClean__c = true;
            cleanedRecords.add(record);
            stagingIds.add(record.Id);
        }

        test.startTest();
        update cleanedRecords;
        AumentumDataProccess.processData(cleanedRecords);
        test.stopTest();
        List<StagingAumentumBusiness__c> withNewStatus= new List<StagingAumentumBusiness__c>();
        List<StagingAumentumBusiness__c> withWarnStatus= new List<StagingAumentumBusiness__c>();
        List<StagingAumentumBusiness__c> withProcessedStatus= new List<StagingAumentumBusiness__c>(); 
        List<StagingAumentumBusiness__c> withReviewStatus= new List<StagingAumentumBusiness__c>();     
        List<StagingAumentumBusiness__c> processedResult=[Select id, Status__c from StagingAumentumBusiness__c where Id IN: stagingIds];
        for(StagingAumentumBusiness__c record:processedResult){           
            if(record.Status__c =='New'){
                withNewStatus.add(record);
            }else if(record.Status__c =='Processed'){
                 withProcessedStatus.add(record);
            }else if(record.Status__c =='Warning'){
                 withWarnStatus.add(record);
            }else if(record.Status__c =='In Review'){
                 withReviewStatus.add(record);
            }
        }
      
        system.assert(withNewStatus.size() ==0,'No record with New Status');
        system.assert(withProcessedStatus.size() >0,'Record with Processed Status');
        system.assert(withWarnStatus.size()>0,'Record with Warning Status');
        system.assert(withReviewStatus.size()>0,'Record with in Review Status');        
                
    }

    @isTest
    static void coveringAccountAndProperty(){
        // Actual logic covered in StandingBusinessHandlerTest
        List<StagingAumentumBusiness__c> records = Test.loadData(StagingAumentumBusiness__c.sObjectType, 'AumentumProcessTestData');
        records[10].BusinessEndDate__c = null;
        records[10].LocationEndDate__c = null;
        update records[10];
       test.startTest();
        Account account1= AumentumDataProccess.populateAccount(records[10]);
        insert account1;
        Property__c  property=AumentumDataProccess.populateProperty(records[10],account1.Id);
        insert property;
        test.stopTest();
        
        Property__c propertyResult=[Select Id, LocationIdentificationNumber__c from Property__c where Id =: property.Id];
        StagingAumentumBusiness__c stageResult=[Select Id,LocationIdentificationNumber__c from StagingAumentumBusiness__c where Id=:records[10].Id];
        system.assertEquals(propertyResult.LocationIdentificationNumber__c, stageResult.LocationIdentificationNumber__c,'LIN should be same');

    }
    
    @isTest
    static void ignoreStatus(){
        String FILTERS = 'IsDataClean__c = true and (Status__c = \'New\' OR Status__c =\'Reprocessed\')';
        String STAGEQUERY = DescribeUtility.getDynamicQueryString('StagingAumentumBusiness__c', FILTERS);
        StagingAumentumBusiness__c staging = TestDataUtility.buildStagingAumentumBusiness('2340038', '2340042-02-012', '45-4766096','GPC CONTRACT', 'GPC CONTRACT', 'SAN FRANCISCO', 'CA', '0','0', null, 'GC BLVD', '2000-01-01','2000-10-10', '1998-01-01', '2000-10-10');
        staging.IsDataClean__c = true;
        insert staging;
        List<StagingAumentumBusiness__c> processRecords = database.query(STAGEQUERY);
        Test.startTest();
        AumentumDataProccess.processData(processRecords);
        Test.stopTest();
        StagingAumentumBusiness__c processRecord = [Select Status__c,Messages__c from StagingAumentumBusiness__c where Id =:staging.Id limit 1];
        system.assertEquals('Ignored', processRecord.Status__c, 'Account is Inactive in Smart');
    }
    
    @isTest
    static void potentialProperty(){
        String FILTERS = 'IsDataClean__c = true and (Status__c = \'New\' OR Status__c =\'Reprocessed\')';
        String STAGEQUERY = DescribeUtility.getDynamicQueryString('StagingAumentumBusiness__c', FILTERS);
        StagingAumentumBusiness__c staging = 
        TestDataUtility.buildStagingAumentumBusiness('6340038','2340042-05-011','55-4766096', 'GPC CONTRACTOR', 'GPC CONTRACTOR', null, null, '0', '0', null, null, '1990-01-01', '1990-10-01', '1990-01-01', '1990-10-01');
        staging.IsDataClean__c = true;
        staging.ContactPostalCode__c = '94103';
        staging.LocationPostalCode__c = '941183305';
        staging.LocationCity__c ='SAN FRANCISCO';
        staging.LocationState__c ='CA';
        insert staging;
        List<StagingAumentumBusiness__c> processRecords = database.query(STAGEQUERY);
        Test.startTest();
        AumentumDataProccess.processData(processRecords);
        Test.stopTest();
        StagingAumentumBusiness__c processRecord = [Select Status__c,Messages__c from StagingAumentumBusiness__c where Id =:staging.Id limit 1];
      	system.assert(processRecord.Messages__c.contains('Potential Matches found'),'Potential Matches found');
       
    }
}