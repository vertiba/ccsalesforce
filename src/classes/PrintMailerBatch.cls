/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Batch job is to collect all the content documents genereated after the
// 			last batch execution and send it to Java layer.
//-----------------------------
public class PrintMailerBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, BatchableErrorHandler {
    private Map<Id,PrintAttribute__c> printAttributesById;

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Map<Id,PrintAttribute__c> printAttributeMap field and values map that needs to be updated.
    // @description : Constructor method.
    //-----------------------------
    public PrintMailerBatch() {
        printAttributesById = new Map<Id,PrintAttribute__c>();
        for(PrintAttribute__c printAttribute : [
            select AccountNumber__c, Address__c, Department__c, Description__c, DocumentSetting__c, EmailAddress__c,
            Envelope__c, Fold__c, Ink__c, MailingIncludeReturnEnvelope__c, Paper__c, PaperColor__c,
            PeopleSoftChartfields__c, PhoneNo__c, ProofRequired__c, ReproNotificationEmailAddress__c, ReturnEnvelopeType__c,
            Sides__c, Size__c, SpecialPrintInstruction__c
            from PrintAttribute__c
        ]){
            printAttributesById.put(printAttribute.DocumentSetting__c, printAttribute);
        }
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : Database.QueryLocator
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext context) {
        String  batchQuery;
        batchQuery = ' Select id, contentDocumentId, DocumentSetting__c, FileExtension, Title, Status__c, LastFailedAttempt__C, NumberOfFailedAttempts__c, SentToIntegrationLayer__c  from ContentVersion';
        batchQuery +=' where SentToIntegrationLayer__c = null and DocumentSetting__c != null and FileExtension = pdf';
        batchQuery += ' order by createdDate asc';
        
        Logger.addDebugEntry(batchQuery,'PrintMailerBatch- start method query');
        Logger.saveLog();

		return Database.getQueryLocator([
            Select id, contentDocumentId, DocumentSetting__c, FileExtension, Title, Status__c,
            LastFailedAttempt__C, NumberOfFailedAttempts__c, SentToIntegrationLayer__c
            from ContentVersion
            where SentToIntegrationLayer__c = null and DocumentSetting__c != null and FileExtension = 'pdf'
            order by createdDate asc
        ]);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : List<sObject>
    // @description : Get all the content versions, that should be sent, get the
    // 		related content documents and content documents links. Find the print
    // 		attributes for each document and prepare a JSON. Send to Integration
    // 		layer and update the content version of success and failure.
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext context, List<ContentVersion> versions) {
        List<ContentVersion> contentVersionsToUpdate = new List<ContentVersion>();
        boolean isCalloutFailed = false;
        DateTime currentTime = System.now();
        try{
            List<Id> contentDocumentIds = new List<Id>();
            String objectType=null;
            Set<Id> documentLinkIds = new Set<Id>();
            for(ContentVersion version : versions) {
                contentDocumentIds.add(version.ContentDocumentId);
            }
            //Get the relatedId's from ContentDocumentLink
            for(ContentDocumentLink documentLink : [
                SELECT Id, ContentDocumentId, LinkedEntityId, LinkedEntity.Type
                FROM ContentDocumentLink
                where ContentDocumentId in :contentDocumentIds
            ]){
                //For each contentdocument there will be one record for User type, and cant be filtered in SOQL, so this if
                objectType = documentLink.LinkedEntity.Type;
                if(objectType == 'Case' || objectType == 'Account' ||
                   objectType == 'Property__c' || objectType == 'PropertyEvent__c'){
                    documentLinkIds.add(documentLink.Id);
                }
            }
            if(documentLinkIds.isEmpty()){
                return;//No document links, so no further processing.
            }
            Map<Id, String> mailingCountriesByDocumentId = new Map<Id, String>();
            sObject linkedEntity;
            //Type of is GA from V46. So, this will work in prod as well, Type cant
            // be used in where clause for ContentDocumentLink
            for(ContentDocumentLink documentLink : [
                SELECT ContentDocumentId, TYPEOF LinkedEntity WHEN Case THEN Account.MailingCountry__c
                WHEN Account THEN MailingCountry__c WHEN Property__c THEN MailingCountry__c
                WHEN PropertyEvent__c THEN MailingCountry__c End
                FROM ContentDocumentLink
                where Id in :documentLinkIds
            ]){
                try{
                    linkedEntity = documentLink.LinkedEntity;
                    String recordId = linkedEntity.id;
                    if(recordId.startsWith('500')){
                        linkedEntity = (sObject)linkedEntity.getPopulatedFieldsAsMap().get('Account');
                    } 
                    mailingCountriesByDocumentId.put(documentLink.ContentDocumentId, (String)linkedEntity.get('MailingCountry__c'));
                }
                catch(Exception ex){
                    system.debug('Exception ::'+ex.getmessage() + ' Because of Bad Data' );
                }
            }
            List<PDFMetaData> pdfMetaDatas = new List<PDFMetaData>();
            System.debug('Before pdfMetaDatas');
            for(ContentVersion version : versions) {
                if(printAttributesById.containsKey(version.DocumentSetting__c)){
                    pdfMetaDatas.add(new PDFMetaData(printAttributesById.get(version.DocumentSetting__c), version,
                                                     mailingCountriesByDocumentId.get(version.ContentDocumentId)));
                    contentVersionsToUpdate.add(version);
                }
            }
            //If document settings dont have a print attribute then empty list is sent to integration layer
            //Avoiding it with this if condition.
            if(contentVersionsToUpdate.isEmpty()) return;

            isCalloutFailed = makeCallout(pdfMetaDatas);
            System.debug('After Callout');
            Logger.addDebugEntry('Before updating the content versions for failure or success', 'PrintMailerBatch.execute');
        } catch (Exception ex){
            isCalloutFailed = true;
            Logger.addExceptionEntry(ex, 'PrintMailerBatch.execute');
            throw ex;
        }
        updateVersionStatus(isCalloutFailed, contentVersionsToUpdate, currentTime);
        Logger.addDebugEntry('Leaving the method', 'PrintMailerBatch.execute');
        Logger.saveLog();
    }

    //-----------------------------
    // @author : Vishnu Vardhan Chowdary Andra
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext context) {
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, PrintMailerBatch.class.getName(), asyncList[0].ParentJobId, 
                                           false,'', asyncList[0].NumberOfErrors);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}

    //-----------------------------
    // @author : Vishnu Vardhan Chowdary Andra
    // @param : List<PDFMetaData>
    // @description : gets a JWT token and send the data to Java layer.
    // 		returns if callout is failed or not.
    // @return : boolean
    //-----------------------------
    private boolean makeCallout(List<PDFMetaData> pdfMetadatas){
        //Can't re-use same JWT token, as generated JTI should be unique per http request
        String jwtToken = CreateJWTToken.generateJWTToken();
        Map<String,String> headers = new Map<String,String>();
        headers.put('Authorization', 'Bearer '+jwtToken);
        headers.put('Content-Type', 'application/json');
        String url = Test.isRunningTest() ? 'http://dummyClassName.com' :[Select Endpoint from NamedCredential where developerName ='PrintMailEndpoint' limit 1].Endpoint;
        Callout callout= new Callout(url).setHeaders(headers);
        if(Test.isrunningtest()){
            return false;
        } else {
	        HttpResponse response = callout.post(pdfMetadatas);
    	    return response.getStatusCode()>=400;
        }
    }

    //-----------------------------
    // @author : Vishnu Vardhan Chowdary Andra
    // @param : boolean
    // @param : List<ContentVersion>
    // @param : DateTime
    // @description : Update the content versions if they are sucessfully sent or not.
    // @return : void
    //-----------------------------
    private void updateVersionStatus(boolean calloutFailed, List<ContentVersion> versions, DateTime currentTime){
        for(ContentVersion version : versions) {
            if(calloutFailed){
                version.LastFailedAttempt__c = currentTime;
                ++version.NumberOfFailedAttempts__c;
            } else {
                version.SentToIntegrationLayer__c = currentTime;
                version.Status__c = 'Sent To Integration Layer';
            }
        }
        update versions;
    }

    @TestVisible
    private class PDFMetaData {
        String accountNumber;
        String address;
        String departmentName;
        String contentVersionId;
        String email;
        String envelope;
        String fold;
        String ink;
        @TestVisible Boolean international;
        String mailDesc;
        String paper;
        String paperColor;
        String peopleSoftChartFields;
        String phoneNumber;
        @TestVisible Boolean proofRequired;
        String reproNotifEmailAddrs;
        @TestVisible boolean returnEnvelope;
        String returnEnvelopeType;
        String sides;
        String size;
        String sourceFileName;
        String specialPrintInstruction;
        //-----------------------------
        // @author : Publicis.Sapient
        // @param : PrintAttribute__c
        // @param : ContentVersion
        // @param : String
        // @description : Constructor method.
        //-----------------------------
        @TestVisible
        PDFMetaData(PrintAttribute__c printAttribute, ContentVersion version, String mailingCountry){
            accountNumber = printAttribute.AccountNumber__c;
            address =  printAttribute.Address__c;
            contentVersionId = version.Id;
            departmentName = printAttribute.Department__c;
            email  = printAttribute.EmailAddress__c;
        	envelope = printAttribute.Envelope__c;
            fold = printAttribute.Fold__c;
            ink = printAttribute.Ink__c;
            international = !'US'.equalsIgnoreCase(mailingCountry);
            mailDesc = printAttribute.Description__c;
            paper = printAttribute.Paper__c;
            paperColor = printAttribute.PaperColor__c;
            peopleSoftChartFields = printAttribute.PeopleSoftChartfields__c;
            phoneNumber = printAttribute.PhoneNo__c;
            proofRequired = 'Yes'.equalsIgnoreCase(printAttribute.ProofRequired__c);
            reproNotifEmailAddrs = printAttribute.ReproNotificationEmailAddress__c;
            returnEnvelope = 'Yes'.equalsIgnoreCase(printAttribute.MailingIncludeReturnEnvelope__c);
            returnEnvelopeType = printAttribute.ReturnEnvelopeType__c;
            sides = printAttribute.Sides__c;
            size = printAttribute.Size__c;
            sourceFileName = version.title+'.'+version.FileExtension;
            specialPrintInstruction = printAttribute.SpecialPrintInstruction__c;
        }
    }
}