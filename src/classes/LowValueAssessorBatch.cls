/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Batch job is to get all active accounts, with no secured properties. For these
// 			accounts, check if there are any regular assessments and all of them to be in 
// 			Ready to send to TTX integration status. Total value of all the properties less than 4000.
// 			For those accounts, cases set the NoticeType__c as lowvalue.
//-----------------------------
public class LowValueAssessorBatch implements Database.Batchable<sObject>, BatchableErrorHandler {
    private String rollYear;

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Constructor method.
    //-----------------------------
    public LowValueAssessorBatch() {
        rollYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : Database.QueryLocator
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext context) {
        Boolean runBatch = false;
        //ASR-9713 run only When NonFiler Batches has been run
        RollYear__c currentYear = [SELECT Id,Year__c,IsNonFilerBulkGeneratorProcessed__c,
                                       IsVesselNonFilerGeneratorProcessed__c
                                  FROM RollYear__c 
                                 WHERE Year__c =: rollYear];		        
        if(currentYear.IsNonFilerBulkGeneratorProcessed__c && currentYear.IsVesselNonFilerGeneratorProcessed__c){
            runBatch = true;
        }       
        String fixedQuery= 'Select id from Account';
        if(runBatch){
            fixedQuery += ' WHERE BusinessStatus__c = \'Active\' and '; 
            fixedQuery += ' RecordType.DeveloperName = \'BusinessAccount\' and NumberofSecuredProperties__c = 0 and';
            fixedQuery += ' (LowValueAssessedYear__c = null or LowValueAssessedYear__c < :rollYear)';
        }else{
            fixedQuery += ' WHERE  IsDeleted=true AND IsDeleted=false';
        }
        Logger.addDebugEntry(fixedQuery,'LowValueAssessorBatch - Start method query');
        Logger.saveLog();
        return Database.getQueryLocator(fixedQuery);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : List<sObject> 
    // @description : This method receives business accounts that have zero secured properties,  
    // 		total value of property less than 4000 and are not assessed for low values in this year.
    // 		Filter out for the accounts, that has Assessments with status not as 'Ready to send to TTX'.
    // 		Update those assessments for low value and accounts with low value and as assessed for low value.
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext context, List<Account> accounts) {
        if(accounts == null || accounts.isEmpty()) return;
        Set<Id> allAccountIds = new Set<Id>();
        Set<Id> filteredAccountIds = new Set<Id>();
        Map<Id,Decimal> lowValueAccounts = new Map<Id,Decimal>();
        for(Account account : accounts){
            allAccountIds.add(account.Id);
        }
        for(AggregateResult aggregateResult : [
            SELECT Property__r.Account__c accountId FROM Case 
             WHERE IsAssessment__c = true
               AND Property__r.Account__c in :allAccountIds AND RollYear__c = :rollYear
               AND Type =: CCSFConstants.ASSESSMENT.TYPE_REGULAR group by Property__r.Account__c]){
                //get all accounts that have cases and ignoring rest of accounts
                filteredAccountIds.add((Id)aggregateResult.get('accountId'));
            }
        if(filteredAccountIds.isEmpty()) return;
        for(AggregateResult aggregateResult : [
            SELECT Property__r.Account__c accountId FROM Case 
            WHERE IsAssessment__c = true
              AND Property__r.Account__c in :filteredAccountIds AND RollYear__c = :rollYear
              AND  Status != 'Closed' 
              AND SubStatus__c != 'Completed' 
              AND Type =: CCSFConstants.ASSESSMENT.TYPE_REGULAR group by Property__r.Account__c]){
                //Filter accounts that have even one case which is not approved
                filteredAccountIds.remove((Id)aggregateResult.get('accountId'));
            }
        //Making sure legacy assessment are not edited
        List<Case> casesToUpdate = [SELECT id, SubType__c,TotalAssessedValue__c, Case.Property__r.Account__c,IsLegacy__c 
                                      FROM Case 
                                    WHERE RollYear__c = :rollYear 
                                      AND IsAssessment__c = true 
                                      AND Property__r.Account__c in :filteredAccountIds 
                                      AND Type =: CCSFConstants.ASSESSMENT.TYPE_REGULAR ORDER BY Property__r.Account__c ];
        //Total Assessed Value on Account
        Decimal AssessedValueSum;
        Id accRecord ;
        for(Case cas : casesToUpdate){
            AssessedValueSum = cas.TotalAssessedValue__c;
            if(!lowValueAccounts.containsKey(cas.Property__r.Account__c)){                 
                lowValueAccounts.put(cas.Property__r.Account__c,AssessedValueSum);
            } else {
                AssessedValueSum+=lowValueAccounts.get(cas.Property__r.Account__c);
                lowValueAccounts.put(cas.Property__r.Account__c,AssessedValueSum);
            }
        }
        for(Case cas : casesToUpdate){ 
            if(cas.IsLegacy__c){continue;}
            if(lowValueAccounts.containsKey(cas.Property__r.Account__c) &&  lowValueAccounts.get(cas.Property__r.Account__c)<= Integer.valueOf(Label.LowValueAmount) ){
                cas.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_LOWVALUE; //ASR-8551 Changes
            }
        }
        update casesToUpdate;
        List<Account> accountsToUpdate = [select id, LowValueAssessedYear__c, NoticeType__c 
                                          from account where id in :filteredAccountIds];
        for(Account account : accountsToUpdate){
            account.LowValueAssessedYear__c = rollYear;
        }
        update accountsToUpdate;
    }

    //-----------------------------
    // @author : Vishnu Vardhan Chowdary Andra
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext context) {
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, LowValueAssessorBatch.class.getName(), asyncList[0].ParentJobId, 
                                           false,'', asyncList[0].NumberOfErrors);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
}