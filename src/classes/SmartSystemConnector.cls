/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class SmartSystemConnector {

    @AuraEnabled
    public static String verifyUrl(String url){
        HttpResponse response = new Callout(url).get();
        System.debug('response.getBody()=' + response.getBody());
        System.debug('response.getStatus()='+response.getStatus());
        List<String> keys = response.getHeaderKeys();
        for(String key:keys){
            System.debug(key+':'+response.getHeader(key));
        }
        return 'Status:'+response.getStatus()+' Body:'+response.getBody();
    }
}