public with sharing class AnnualRollCloseFileGeneration {

    private Static String JWT_TOKEN;

    @AuraEnabled
    public static Response checkRequiredPermissions(String recordId){

        Response response = new Response();   

        Boolean hasPermission = FeatureManagement.checkPermission(System.Label.MassGenerateNonFilerAssessments); 
        RollYear__c rollYear = [SELECT Status__c,IntegrationStatus__c,Name 
                                FROM RollYear__c 
                                WHERE id =: recordId];
        
        if(rollYear.IntegrationStatus__c != CCSFConstants.AABFILE_CONSTANTS.ROLLYEAR_INTEGRATIONSTATUS_SENT_TO_TTX ) {
            response.isSuccess = false;
            response.message = CCSFConstants.AABFILE_CONSTANTS.ERROR_MSG_INTEGRATION_STATUS;      
            return Response;  

        }else {
            response.isSuccess = true;
            response.message = CCSFConstants.AABFILE_CONSTANTS.CONFIRM_MSG_INTEGRATION_STATUS ; 
            response.rollYear = rollYear.Name;    
            return Response;  
        } 

    }

    @AuraEnabled
    public static Response triggerAABCallout(String rollYear){

       
        Response responseWrapper = new Response();  

        try{
            JWT_TOKEN = CreateJWTToken.generateJWTToken();
            if(JWT_TOKEN != null) {
                Map<String,String> headers = new Map<String,String>();
                headers.put('Authorization', 'Bearer '+JWT_TOKEN);
                String url = [Select Endpoint from NamedCredential where developerName =: CCSFConstants.AABFILE_CONSTANTS.NAMED_CREDENTIAL_NAME limit 1].Endpoint +rollYear;
                Callout callout= new Callout(url).setHeaders(headers);
                HttpResponse response = callout.post();
                //calloutResponse.put('isSuccess',true);
                if(response.getStatusCode()!=400 || response.getStatusCode() != 500) {
                    responseWrapper.isSuccess = true;
                    responseWrapper.message = CCSFConstants.AABFILE_CONSTANTS.PROCESS_TRIGGERED_SUCCESSFULLY_MSG;
                
                }else {
                    responseWrapper.isSuccess = false;
                    responseWrapper.message = CCSFConstants.AABFILE_CONSTANTS.PROCESS_TRIGGERED_FAILED_MSG;
                }
            }else {
                responseWrapper.isSuccess = false;
                responseWrapper.message = CCSFConstants.AABFILE_CONSTANTS.PROCESS_TRIGGERED_FAILED_MSG;
            }
            
        }
        catch(Exception ex) {
            responseWrapper.isSuccess = false;
            responseWrapper.message =CCSFConstants.AABFILE_CONSTANTS.PROCESS_TRIGGERED_FAILED_MSG;
            
        }
        return responseWrapper;

    }

    //wrapper class for response
    public class Response {
        @AuraEnabled
        public Boolean isSuccess;
        @AuraEnabled
        public String message;
        @AuraEnabled
        public String rollYear;
        
        public Response() {
            isSuccess =false;
            message='';
            rollYear='';
        }  
    }

}