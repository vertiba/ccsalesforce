/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
* Class Name: PropertiesPurgeByLegacyPropertyRest
* Description: This class purges Legacy Properties
* Author : Publicis Sapient 
*/
@RestResource(urlMapping='/PropertiesPurgeByLegacyProperty/*')
global with sharing class PropertiesPurgeByLegacyPropertyRest {
    
    @HttpPost
    global Static Map<String, String> postMethod(){
        
        String requestBody = RestContext.request.requestBody.toString(); 
        Map<String, String> deletedObjectNameByCount = new Map<String, String>();
        LegacyPropertyWrapper legacyPropertyIds = (LegacyPropertyWrapper)System.JSON.deserialize(requestBody, LegacyPropertyWrapper.class);
        if(System.Label.AllowDataPurging == 'True'){
            try{
                DataPurge propertiesPurge = new DataPurge();
                deletedObjectNameByCount = propertiesPurge.deletePropertiesByLegacyProperty(legacyPropertyIds.entityIds);
                if(Test.isRunningTest()){
                    // Manually invoking an error for covering the exception test scenario 
                    // if the above callout does not fail by itself.
                    Integer count = 10/0;
                }
            }catch(Exception ex){
                deletedObjectNameByCount.put('Error', ex.getMessage());
                system.debug('error message'+ deletedObjectNameByCount);
            }
        }else{
            deletedObjectNameByCount.put('Error', system.label.DataPurgingError);
        }
        return deletedObjectNameByCount;
    } 
    
    public class LegacyPropertyWrapper {
        List<String> entityIds;
        public LegacyPropertyWrapper(){
            
        }
    }
    
}