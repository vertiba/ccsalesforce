/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: BatchableErrorTestJob 
 * Description : Test Batch to validate the Batch Retry Framework
 */  
public class BatchableErrorTestJob implements Database.Batchable<SObject>, BatchableErrorHandler {

    private List<Account> accounts; 
    private Boolean retryMode = false;

    public BatchableErrorTestJob() { 
        retryMode = true;
    }

    public BatchableErrorTestJob(List<Account> accounts) { 
        this.accounts = accounts;
    }
    
    public List<Account> start(Database.BatchableContext context) {
        return accounts;
    }

    public void execute(Database.BatchableContext context, List<Account> scope) {
        string JobId = context.getJobId();
        BatchApexLog__c batchLog = new BatchApexLog__c();
        batchLog.BatchClassName__c = BatchableErrorTestJob.class.getName();
        batchLog.AreBatchParamsPresent__c = false;
        batchLog.JobId__c = JobId;
        batchLog.QueryParams__c = '';
        database.insert(batchLog,false);
        system.debug('batchLog-->'+batchLog);
        
        Set<Id> accountIds = new Map<Id, Account>(scope).keySet();
        system.debug('accountIds-->'+accountIds);
        List<Account> accounts = [select Id, LastName from Account where id in :accountIds];
        system.debug('accounts 1->'+accounts);
        // Throw a test exception?
        if(accounts[0].LastName == 'Bad') {
            System.debug('Throwing exception');
            throw new TestJobException('Test exception');
        }
        // Update account if all good
        for(Account account : accounts) {
            account.LastName = 'All good';
        }
        system.debug('accounts 2->'+accounts);
        database.update(accounts);
    }
    
    public void finish(Database.BatchableContext context) {
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'accounts' => this.accounts});                                
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, BatchableErrorTestJob.class.getName(), 
                                           asyncList[0].ParentJobId, true, queryParams, asyncList[0].NumberOfErrors);
    }

    public void handleErrors(BatchableError error) { 
        // Provide the test code a means to confirm the errors have been handled
        List<String> accountIds = error.JobScope.split(',');
        List<Account> accounts = [select Id, LastName from Account where id in :accountIds];
        for(Account account : accounts) {
            account.LastName = 'Handled';
        }
        database.update(accounts);
    }

    public class TestJobException extends Exception { }
}