/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class BusinessAccountAccessControllerTest {

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    private static void setupData() {
        // Create a test account - Entity ID is automatically populate via trigger logic
        Account account = TestDataUtility.buildBusinessAccount('test account');
        account.AccessPIN__c      = '1111';
        account.BusinessStatus__c = 'active';
        account.MailingCountry__C = 'US';
        account.RequiredToFile__c = 'Yes';
        insert account;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to validate EntityId And Pin For Access
    //----------------------------- 
    @isTest
    private static void validateEntityIdAndPinForAccess() {
        Account account = [SELECT Id, EntityId__c, AccessPin__c FROM Account];

        String message = BusinessAccountAccessController.validateEntityIdAndPin(account.EntityId__c, account.AccessPin__c);
        System.assertEquals(message, 'Access Pin Matched');
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to validate EntityId And Pin For Access for not match
    //----------------------------- 
    @isTest
    private static void validateEntityIdAndPinForNoMatch() {
        Account account = [SELECT Id, EntityId__c, AccessPin__c FROM Account];

        try {
            String message = BusinessAccountAccessController.validateEntityIdAndPin(account.EntityId__c, account.AccessPin__c);
        } catch(Exception ex) {
            Boolean expectedExceptionThrown = ex.getMessage().contains('exception');
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to varify for submission with Access Pin
    //----------------------------- 
    @isTest
    private static void submitWithAccessPIN() {
        Account account = [SELECT Id, EntityId__c, AccessPin__c FROM Account];

        Account returnedAccount = BusinessAccountAccessController.submitWithAccessPIN(account.EntityId__c, account.AccessPin__c);
        System.assertEquals(account.Id, returnedAccount.Id);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to varify for submission with Access Pin and wrong entity ID
    //----------------------------- 
    @isTest
    private static void submitWithAccessPINIfWrongEntityId() {
        Account account = [SELECT Id, EntityId__c, AccessPin__c FROM Account];

        String invalidEntityId = '9';
        System.assertNotEquals(account.EntityId__c, invalidEntityId);

        try {
            Account returnedAccount = BusinessAccountAccessController.submitWithAccessPIN(invalidEntityId, '1111');
            String message = BusinessAccountAccessController.validateEntityIdAndPin(invalidEntityId, account.AccessPin__c);
        } catch(Exception ex) {
            Boolean expectedExceptionThrown = ex.getMessage().contains('exception');
            System.assertEquals(expectedExceptionThrown, true);
        }
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to fetch Contact Relation
    //----------------------------- 
    @isTest
    private static void createAccountContactRelationTest() {
        Account account = [SELECT Id, EntityId__c, AccessPin__c FROM Account];
        User portalUser = TestDataUtility.getPortalUser();
        Boolean exceptionThrown = false;
        try {
            Test.startTest();
                System.runAs(portalUser){
                    BusinessAccountAccessController.createAccountContactRelation(account.id);
                }
            Test.stopTest();
        } catch(Exception ex) {
            exceptionThrown = ex.getMessage().contains('exception');
        }
        System.assertEquals(false, exceptionThrown);
        AccountContactRelation relation = [select id,EndDate,AccountId,IsActive,StartDate,ContactId from AccountContactRelation];
        System.assertEquals(true, relation.IsActive);
        System.assertEquals(Date.newInstance(System.today().year() + 1, 1, 1), relation.EndDate);
        System.assertEquals(System.today(), relation.StartDate);
        System.assertEquals(account.id, relation.AccountId);
    }

    /*@isTest
    private static void saveTheFile() {
        Account account = new Account();
        account.Name='test account';
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.BusinessAccountNumber__c = '123456';
        account.AgentPIN__c = '1234';
        account.NoticeType__c='Notice to File';
        account.RequiredToFile__c='Yes';
        insert account;
        Account returnedAccount = BusinessAccountAccessController.saveTheFile(123456, '1234', 'abc.pdf', 'base64Data', 'pdf');
        System.assertNotEquals(null, returnedAccount);
    }

    @isTest
    private static void validateEntityIdAndPinForAgent() {
        Account account = new Account();
        account.Name='test account';
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.BusinessAccountNumber__c = '123456';
        account.AgentPIN__c = '1234';
        account.NoticeType__c='Notice to File';
        account.RequiredToFile__c='Yes';
        insert account;

        String message = BusinessAccountAccessController.validateEntityIdAndPin(123456, '1234');
        System.assertEquals(message, 'Agent Pin Matched');
    }

    @isTest
    private static void validateAgentPINIfWrongEntityId() {
        Account account = new Account();
        account.Name='test account';
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.BusinessAccountNumber__c = '123456';
        account.AgentPIN__c = '1234';
        account.NoticeType__c='Notice to File';
        account.RequiredToFile__c='Yes';
        insert account;

        try {
            String message = BusinessAccountAccessController.validateEntityIdAndPin(111111, '1212');
        } catch(Exception ex) {
            Boolean expectedExceptionThrown = ex.getMessage().contains('exception');
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }*/

}