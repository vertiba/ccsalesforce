@isTest
private class DescribeUtilityTest {

    @isTest
    static void it_should_return_picklist_entries_for_picklist_field() {
        List<Schema.PicklistEntry> expectedPicklistEntries = Schema.Account.Type.getDescribe().getPickListValues();

        Test.startTest();
        List<Schema.PicklistEntry> returnedPicklistEntries = DescribeUtility.getPicklistValues('Account', 'Type');
        Test.stopTest();

        System.assertEquals(expectedPicklistEntries, returnedPicklistEntries);
    }

    @isTest
    static void it_should_return_null_picklist_entries_for_non_picklist_field() {
        Test.startTest();
        List<Schema.PicklistEntry> returnedPicklistEntries = DescribeUtility.getPicklistValues('Account', 'Name');
        Test.stopTest();

        System.assertEquals(null, returnedPicklistEntries);
    }

    @isTest
    static void it_should_return_dynamic_picklist_rows_for_picklist_field() {
        VisualEditor.DynamicPickListRows expectedDynamicPickListRows = new VisualEditor.DynamicPickListRows();
        for(Schema.PicklistEntry picklistEntry : Schema.Account.Type.getDescribe().getPickListValues()) {
            expectedDynamicPickListRows.addRow(new VisualEditor.DataRow(picklistEntry.getLabel(), picklistEntry.getValue()));
        }

        Test.startTest();
        VisualEditor.DynamicPickListRows returnedDynamicPickListRows = DescribeUtility.getDynPickListRows('Account', 'Type');
        Test.stopTest();

        System.assertEquals(Json.serialize(expectedDynamicPickListRows), Json.serialize(returnedDynamicPickListRows));
    }

    @isTest
    static void it_should_return_null_dynamic_picklist_rows_for_non_picklist_field() {
        Test.startTest();
        VisualEditor.DynamicPickListRows returnedDynamicPickListRows = DescribeUtility.getDynPickListRows('Account', 'Name');
        Test.stopTest();

        System.assertEquals(null, returnedDynamicPickListRows);
    }

    @isTest
    static void it_should_return_creatable_fields_soql() {
        List<String> creatableFields = new List<String>();
        for(Schema.SObjectField field : Schema.Account.SObjectType.getDescribe().fields.getMap().values()) {
            if(field.getDescribe().isCreateable()) {
                creatableFields.add(field.getDescribe().getName());
            }
        }
        String expectedQuery = 'select ' + String.join(creatableFields, ', ') + ' from Account';

        Test.startTest();
        String returnedQuery = DescribeUtility.getCreatableFieldsSOQL('Account');
        Test.stopTest();

        System.assertEquals(expectedQuery, returnedQuery);
    }

}