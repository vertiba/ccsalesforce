/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This is a test class to DirectBillVesselAssessmentGenerator

@isTest
public class DirectVesselAssessmentGeneratorTest {
    
    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Date eventDate           = Date.newInstance(currentYear, 1, 1);
    private static Date eventDateToLastYear = Date.newInstance(lastYear, 1, 1);
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    static void createTestData() { 
        
        TestDataUtility.disableAutomationCustomSettings(true);
        //insert roll years
        List<RollYear__c> rollYears = new List<RollYear__c>();
        RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYearCurrentFiscalYear.IsLowValueVesselAssessmentgenerator__c = true;
        rollYears.add(rollYearCurrentFiscalYear);
        RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastYear),String.valueof(lastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYears.add(rollYearLastFiscalYear);
        insert rollYears;
        
        //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        
        //insert account
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAccount = TestDataUtility.buildAccount('Johansson&Jon', CCSFConstants.NOTICE_TYPE_DIRECT_BILL, businessAccountRecordTypeId);
        Account objAccount1 = TestDataUtility.buildAccount('Johansson', CCSFConstants.NOTICE_TYPE_DIRECT_BILL, businessAccountRecordTypeId);
        Account objAccount2 = TestDataUtility.buildAccount('Milky', CCSFConstants.NOTICE_TYPE_DIRECT_BILL, businessAccountRecordTypeId);
        insert new List<Account>{ objAccount, objAccount1, objAccount2};
            
            
            //insert property
        Property__c vesselProperty = TestDataUtility.getVesselProperty();
        vesselProperty.Account__c = objAccount.Id;
        vesselProperty.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        
        
        Property__c vesselProperty1 = TestDataUtility.getVesselProperty();
        vesselProperty1.Account__c = objAccount1.Id;
        vesselProperty1.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        
        Property__c vesselProperty2 = TestDataUtility.getVesselProperty();
        vesselProperty2.Account__c = objAccount2.Id;
        vesselProperty2.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        
        insert new List<Property__c>{ vesselProperty,vesselProperty1, vesselProperty2}; 
            
        Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),vesselProperty.Id,
                                                               rollYearLastFiscalYear.Id,rollYearLastFiscalYear.Name,rollYearLastFiscalYear.Name,
                                                               CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDateToLastYear);
        assessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        
         Case assessment1  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),vesselProperty1.Id,
                                                           rollYearCurrentFiscalYear.Id,rollYearCurrentFiscalYear.Name,rollYearCurrentFiscalYear.Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        
        assessment1.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW; 
        
        Case assessment2 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME),vesselProperty2.Id,
                                                           rollYearLastFiscalYear.Id,rollYearLastFiscalYear.Name,rollYearLastFiscalYear.Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDateToLastYear);
        
        assessment2.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        
        insert new List<Case>{ assessment,assessment1, assessment2 }; 
        
        
        TestDataUtility.disableAutomationCustomSettings(false);
        
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method For where case belongs to last year
    //-----------------------------
    @isTest
    static void testGenerateDirectBillWithCaseLastYear() {
        
        Test.startTest();
        Database.executeBatch(new DirectVesselAssessmentGenerator());
        Test.stopTest();
        
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Johansson&Jon' LIMIT 1].Id;
        // Assert the functionality
        System.assertEquals(2, [SELECT count() FROM Case where Property__c =: propId]);
        System.assertEquals(CCSFConstants.ASSESSMENT.STATUS_CLOSED,[Select Status from Case where Property__c =: propId and RollYear__c =: String.valueOf(currentYear)].Status);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method For where case belongs to current year
    //-----------------------------
    @isTest
    static void testGenerateDirectBillWithCaseCurrentYear() {
         
        Test.startTest();
        Database.executeBatch(new DirectVesselAssessmentGenerator());
        Test.stopTest();
        
         Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Johansson' LIMIT 1].Id;
        // Assert the functionality
        System.assertEquals(1, [SELECT count() FROM Case where Property__c =:propId ],'Current Year Assessment Will not be generated');
    }
    
    @isTest
    static void errorFrameworkTest(){
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Johansson&Jon' LIMIT 1].Id;
        
        Case assessment = [Select Id from Case where property__c =:propId limit 1];
        
        
        Id testJobId = '707S000000nKE4fIAG';
        BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
        testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testJob.JobCreatedDate__c = System.today();
        testJob.JobId__c = testJobId;
        database.insert(testJob);        
        
        BatchApexError__c testError = new BatchApexError__c();
        testError.AsyncApexJobId__c = testJobId;
        testError.BatchApexErrorLog__c = testJob.Id;
        testError.DoesExceedJobScopeMaxLength__c = false;
        testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
        testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testError.JobScope__c = assessment.Id;
        testError.Message__c = 'Test exception';
        testError.RequestId__c = null;
        testError.StackTrace__c = '';
        database.insert(testError);
        
        
        BatchableError batchError = BatchableError.newInstance(testError);
        DirectVesselAssessmentGenerator batchRetry= new DirectVesselAssessmentGenerator();
        batchRetry.handleErrors(batchError);
        system.assertEquals('707S000000nKE4fIAG', testError.AsyncApexJobId__c);
    } 
    
     @isTest
    static void directBillLegacyAssessment() {
               
      Test.startTest(); 
      Database.executeBatch(new DirectVesselAssessmentGenerator());
      Test.stopTest();
        
        // Assert the functionality
        
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Milky' LIMIT 1].Id;
        System.assertEquals(2, [SELECT count() FROM Case where Property__c =: propId]);
        //  new direct bill Assessment will have RecordType Vessel
        System.assertEquals(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME), [Select RecordTypeId from Case where Property__c =: propId and AssessmentYear__c =: String.valueof(currentYear)].RecordTypeId);
    	System.assertEquals(CCSFConstants.ASSESSMENT.STATUS_CLOSED,[Select Status from Case where Property__c =: propId and RollYear__c =: String.valueOf(currentYear)].Status,'Status Closed for new assessment for direct bill');
    } 
   
    
}