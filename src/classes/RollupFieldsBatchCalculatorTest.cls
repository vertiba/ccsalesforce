/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: RollupFieldsBatchCalculatorTest 
 * Description : Test Class for RollupFieldsBatchCalculator Class
*/
@isTest
public class RollupFieldsBatchCalculatorTest {

    /*
     * Method Name: rollupBatchCalculatorTest
     * Description: This method tests all the functions in the RollupFieldsBatchCalculator class 
     */ 
    public static TestMethod void rollupBatchCalculatorTest(){
        Schema.SobjectField entityField = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().get('EntityId__c');
        Schema.SObjectType objectType= Schema.getGlobalDescribe().get('Property__c');
        
        RollupFieldsBatchCalculator.ChildSObject batch = new RollupFieldsBatchCalculator.ChildSObject(objectType,entityField);
        RollupFieldsBatchCalculator.ChildSObject batch2 = new RollupFieldsBatchCalculator.ChildSObject(objectType,entityField,100);
        List<RollupFieldsBatchCalculator.ChildSObject> childSObjects = new List<RollupFieldsBatchCalculator.ChildSObject>{batch};
            
        Test.startTest();
        RollupFieldsBatchCalculator.startBatchCalculation(childSObjects);
        Test.stopTest();
    }
}