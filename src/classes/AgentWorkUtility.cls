/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class AgentWorkUtility {
    /**
     * @author : Publicis.Sapient
     * @description : When a user accepts OmniChannel work related to a statement, 
     * process builder calls this method to reassign the parent assessment as well
     * @param : List<ID> workItemIds sObject Ids from Process Builder
     * @return : void
     */
    @InvocableMethod(label='Reassign Parent Assessment to Statement Owner')
    public static void assignParentAssessmentToStatementOwner(List<Id> workItemIds){
        Set<Id> statementIds = new Set<Id>();

        for(Id workItemId : workItemIds){
            if(workItemId.getSObjectType() == Statement__c.getSObjectType()){
                statementIds.add(workItemId);
            }
        }

        if(statementIds.isEmpty())return;

        List<Statement__c> statementsWithAssessments = [SELECT Id, OwnerId, Assessment__c, Assessment__r.OwnerId
                                                        FROM Statement__c
                                                        WHERE Id IN : statementIds
                                                        AND Assessment__c != null];

        List<Case> assessmentsToUpdate = new List<Case>();

        for(Statement__c statement : statementsWithAssessments){
            if(statement.OwnerId != statement.Assessment__r.OwnerId){
                assessmentsToUpdate.add(new Case(Id = statement.Assessment__c,
                                                OwnerId = statement.OwnerId));
            }
        }

        update assessmentsToUpdate;
    }
}