/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : End-to-end testing of Omnichannel Routing
//-----------------------------
@isTest
public with sharing class OmnichannelRoutingTest {
    @TestSetup
    static void makeData(){
        Account ac1 = TestDataUtility.buildBusinessAccount('Test Account');
        insert ac1;

        Property__c prop1 = TestDataUtility.buildBusinessPersonalProperty('Test BPP', ac1.Id);
        insert prop1;

        RollYear__c roll1 = TestDataUtility.buildOpenRollYear('Test Roll', '2019');        
        insert roll1;

        Penalty__c pen1 = TestDataUtility.buildPenalty(1000.0, 4, System.Label.Late_Filer_Penalty_R_T_Code);
        insert pen1;

        Case ass1 = TestDataUtility.buildRegularBPPAssessment(prop1.Id, roll1, '2019', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, null, Date.newInstance(2019, 06, 15));
        insert ass1;
        
    }

    @IsTest
    static void testCustomerServiceRouting(){
        Account testAcc = [SELECT Id FROM Account LIMIT 1];
        Case csCase = new Case(RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(CCSFConstants.ASSESSMENT.CUSTOMER_CARE_RECORD_TYPE_API_NAME).getRecordTypeId(),
                                AccountId = testAcc.Id);

        Test.startTest();
        insert csCase;
        Test.stopTest();
        csCase = [SELECT Id, RecordTypeAPIName__c FROM Case WHERE Id = :csCase.Id];
        System.assertEquals(CCSFConstants.ASSESSMENT.CUSTOMER_CARE_RECORD_TYPE_API_NAME, csCase.RecordTypeAPIName__c, 'Record Type not being set correctly');
        List<PendingServiceRouting> psrs = [SELECT Id, (SELECT Id, Skill.DeveloperName FROM SkillRequirements) FROM PendingServiceRouting WHERE WorkItemId = :csCase.Id];
        System.assert(!psrs.isEmpty(), 'No PendingServiceRoute created for new Customer Service Case');
        System.assert(!psrs[0].SkillRequirements.isEmpty(), 'No SkillRequirements found for Customer Service Case');
        System.assertEquals('CustomerService', psrs[0].SkillRequirements[0].Skill.DeveloperName, 'Customer Service Skill not applied to Customer Service Cases');
        
    }
}