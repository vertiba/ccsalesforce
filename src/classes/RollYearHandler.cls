/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class RollYearHandler extends TriggerHandler {

    protected override void executeAfterUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {
        List<RollYear__c> updatedRollYears    = (List<RollYear__c>)updatedRecords;
        Map<Id, RollYear__c> oldRollYearsById = (Map<Id, RollYear__c>)oldRecordsById;
        this.startEscapeAssessmentGenerator(updatedRollYears, oldRollYearsById);// to generate Escape Assessment batch
        this.sendToIntegrationLayer(updatedRollYears, oldRollYearsById);// to start AnnualRollBatchSync batch
        this.updatePropertyAssessedValue(updatedRollYears, oldRollYearsById);
       
    }
    // To execute Escape Assessment Generation for closing year
    private void startEscapeAssessmentGenerator(List<RollYear__c> updatedRecords, Map<Id, RollYear__c> oldRollYearsById){
        // Assumption is that only 1 roll year is active at a time,
        // and only 1 record is updated at a time
        if(updatedRecords.size() != 1) return;
        RollYear__c rollYear    = updatedRecords[0];
        RollYear__c oldRollYear = oldRollYearsById.get(rollYear.Id);
        if((rollYear.Status__c == 'Roll Closed') && oldRollYear.Status__c != rollYear.Status__c) {
            // Generate Escape Assessment for Closing Year
            Logger.addDebugEntry('Starting Generate Missing Escape Assessments'+ rollYear.Name,'from Rollyearhandler');
            Database.executeBatch(new EscapeAssessmentBatchGenerator(rollYear)); 
        }
        
    }
    // To execute UpdateRollCloseCaseStatusBatch for closing year
    private void sendToIntegrationLayer(List<RollYear__c> updatedRecords, Map<Id, RollYear__c> oldRollYearsById){
        // Assumption is that only 1 roll year is active at a time,
        // and only 1 record is updated at a time
        if(updatedRecords.size() != 1) return;
        RollYear__c rollYear    = updatedRecords[0];
        RollYear__c oldRollYear = oldRollYearsById.get(rollYear.Id);
        if((rollYear.Status__c == 'Roll Closed') && oldRollYear.Status__c != rollYear.Status__c) {
            // updating eligible cases integration status so that it can send assessments to integration layer for Closing Year
            Logger.addDebugEntry('Started updating eligible cases integration status'+ rollYear.Name,'from Rollyearhandler');
            Database.executeBatch(new UpdateRollCloseCaseStatusBatch(rollYear)); 
        }
        
    }
    //-----------------------------------------------------------
    // @author : Publicis.Sapient
    // @description : This method updatePropertyAssessedValue is used to call batch once roll year is closed 
    //                    to update the property AssessedValue on Property records from closed roll year assessment.
    // @params : Once Roll Year Closed
    //------------------------------------------------------------
        private void updatePropertyAssessedValue(List<RollYear__c> updatedRecords, Map<Id, RollYear__c> oldRollYearsById){
        // Assumption is that only 1 roll year is active at a time,
        // and only 1 record is updated at a time
        if(updatedRecords.size() != 1) return;
        RollYear__c rollYear    = updatedRecords[0];
        RollYear__c oldRollYear = oldRollYearsById.get(rollYear.Id);
        if((rollYear.IntegrationStatus__c == CCSFConstants.ROLLYEAR.SENT_TO_TTX) && oldRollYear.IntegrationStatus__c != rollYear.IntegrationStatus__c) {
            Logger.addDebugEntry('Started updating eligible cases integration status'+ rollYear.Name,'from Rollyearhandler');
            Database.executeBatch(new UpdatePropertyAssessedCost()); 
        }        
    }
}