/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for ReOpenAnnualRoll APEX Controller which has been used on ReOpenAnnualRoll Lightning Component
 *
 * @Author: Publicis.Sapient
 *
 * ModifiedBy           ModifiedDate   Description
 * Publicis.Sapient     2020-06-09     Initial development
*/


@isTest
public class ReOpenAnnualRollTest {

    @testSetup
    static void createTestData() {
        User nonAdmin = TestDataUtility.getOfficeAssistantUser();
        User sysAdmin = TestDataUtility.getSystemAdminUser();
        insert new List<User>{ nonAdmin, sysAdmin };
            System.runas(sysAdmin){
                RollYear__c rollYear1 = new RollYear__c();
                rollYear1.IsLowValueVesselAssessmentgenerator__c = true;
                rollYear1.Name                    = String.valueOf(system.today().year());
                rollYear1.Status__c               = 'Roll Closed';
                rollYear1.Threshold__c            = 10;
                rollYear1.Year__c                 = String.valueOf(system.today().year());
                rollYear1.IntegrationStatus__c     = CCSFConstants.SENT_TO_TTX;
                insert rollYear1;
            }
    }
    
    // TEST METHODS
    @isTest
    static void testReOpenAnnualRollForSysAdmin() {
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c];

        Test.startTest();
        ReOpenAnnualRoll.Response response;
        ReOpenAnnualRoll.Response response1;

        System.runAs(systemAdmin) {
            response = ReOpenAnnualRoll.checkPermission(rollYear.Id);
            response1= ReOpenAnnualRoll.runReOpenAnnualRoll();
        }
        Test.stopTest();

        // Assert the functionality i.e users with System Administrator profile
        // has system admin access
        System.assertEquals(response.isSuccess, true);
        system.assertEquals(response1.isSuccess, true);
    }

    @isTest
    static void testReOpenAnnualForNonAdmin() {
        User nonAdmin = [SELECT Id FROM User WHERE Email = 'testOfficeAssistantUser@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c];
        Test.startTest();
        ReOpenAnnualRoll.Response response;
        System.runAs(nonAdmin) {
            response = ReOpenAnnualRoll.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e users without System Administrator profile i.e.Office Assistant in this case
        // has not assigned the custom permission "MassGenerateDirectBillVesselAssessments"
        System.assertEquals(response.isSuccess, false);
    }
    
    @isTest
    static void testReOpenAnnualForOpenRollYear() {
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c];
        rollYear.Status__c='Roll open';
        update rollYear;

        Test.startTest();
        ReOpenAnnualRoll.Response response;
        ReOpenAnnualRoll.Response response1;

        System.runAs(systemAdmin) {
            response = ReOpenAnnualRoll.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e for roll closed
        System.assertEquals(response.isSuccess, false);
    }
    
    @isTest
    static void testReOpenAnnualForValidateIntegrationStatus() {
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c];
        rollYear.Status__c='Roll Closed';
        rollYear.IntegrationStatus__c=CCSFConstants.IN_TRANSIT_TO_TTX;
        update rollYear;

        Test.startTest();
        ReOpenAnnualRoll.Response response;
        ReOpenAnnualRoll.Response response1;

        System.runAs(systemAdmin) {
            response = ReOpenAnnualRoll.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e for roll closed
        System.assertEquals(response.isSuccess, false);
    }
    
    @isTest
    static void testReOpenAnnualForNonCurrentRollYear() {
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c];
        rollYear.Status__c='Roll Closed';
        rollYear.Year__c                 = String.valueOf(system.today().year()-1);
        rollYear.IntegrationStatus__c     = CCSFConstants.SENT_TO_TTX;
        update rollYear;

        Test.startTest();
        ReOpenAnnualRoll.Response response;
        ReOpenAnnualRoll.Response response1;

        System.runAs(systemAdmin) {
            response = ReOpenAnnualRoll.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e for roll closed
        System.assertEquals(response.isSuccess, false);
    }
}