@isTest
/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Test Class for TriggerHandler.cls
//-----------------------------
public with sharing class TriggerHandlerTest {
    private static final Id ACCOUNT_RECORDTYPEID = [SELECT Id FROM RecordType WHERE Name = 'Business Account' AND SobjectType = 'Account'].Id;
    private static final Id PROPERTY_RECORDTYPEID = [SELECT Id FROM RecordType WHERE SobjectType = 'Property__c' AND IsActive = TRUE LIMIT 1].Id;

    @TestSetup
    static void makeData(){
        insert new TriggerHandlerSettings__c(DisableDLRSCalculationTriggers__c = false);
        insert new LoggerSettings__c();



        Account testAccount1 = TestDataUtility.buildAccount('Test Account 1', 'SDR', ACCOUNT_RECORDTYPEID);
        insert testAccount1;

        List<Property__c> testProperties1 = new List<Property__c>{ TestDataUtility.buildProperty(PROPERTY_RECORDTYPEID, testAccount1.Id, 'LOC_ID1', 'DBA', 'Test Address'),
                                                                    TestDataUtility.buildProperty(PROPERTY_RECORDTYPEID, testAccount1.Id, 'LOC_ID2', 'DBA', 'Test Address'),
                                                                    TestDataUtility.buildProperty(PROPERTY_RECORDTYPEID, testAccount1.Id, 'LOC_ID3', 'DBA', 'Test Address'),
                                                                    TestDataUtility.buildProperty(PROPERTY_RECORDTYPEID, testAccount1.Id, 'LOC_ID4', 'DBA', 'Test Address'),
                                                                    TestDataUtility.buildProperty(PROPERTY_RECORDTYPEID, testAccount1.Id, 'LOC_ID5', 'DBA', 'Test Address'),
                                                                    TestDataUtility.buildProperty(PROPERTY_RECORDTYPEID, testAccount1.Id, 'LOC_ID6', 'DBA', 'Test Address'),
                                                                    TestDataUtility.buildProperty(PROPERTY_RECORDTYPEID, testAccount1.Id, 'LOC_ID7', 'DBA', 'Test Address'),
                                                                    TestDataUtility.buildProperty(PROPERTY_RECORDTYPEID, testAccount1.Id, 'LOC_ID8', 'DBA', 'Test Address')
                                                                    };
        insert testProperties1;                                                        
    }

    @isTest
    public static void testUpdateNoChange() {
        List<Property__c> properties = [SELECT Id FROM Property__c];

        Test.startTest();
        update properties;
        Test.stopTest();

        System.assert(!TriggerHandler.rollupsCalculated.contains('Property__c'), 'Rollups were calculated for unmodified Property records');
    }

    @isTest
    public static void testUpdateWithChange(){
        List<Property__c> properties = [SELECT Id FROM Property__c];
        properties[0].CountActiveExemptions__c=6;

        Test.startTest();
        update properties;
        Test.stopTest();

        System.assert(TriggerHandler.rollupsCalculated.contains('Property__c'), 'Rollups were not calculated for modified Property records');
    }

    @isTest
    public static void testUpdateForObjectWithoutRule(){
        Account testAccount = TestDataUtility.buildAccount('Test Account', 'SDR', ACCOUNT_RECORDTYPEID);

        System.assert(TriggerHandler.getApplicableRules('Account').isEmpty(), 'Test Cannot proceed: There are Rollup rules for the Account object');

        Test.startTest();
        insert testAccount;
        Test.stopTest();

        System.assert(!TriggerHandler.rollupsCalculated.contains('Account'), 'Rollups were calculated for an Account even though no rules for Accounts exist');
    }

    @isTest
    public static void testUpdateOnUntrackedField(){
        List<Property__c> properties = [SELECT Id FROM Property__c];
        List<dlrs__LookupRollupSummary2__mdt> rules = TriggerHandler.getApplicableRules('Property__c');
        System.assert(!rules.isEmpty(), 'Test cannot proceed - no rules found for Property__c');
        properties[0].Name = 'Test New Name';

        for(dlrs__LookupRollupSummary2__mdt rule : rules){
            System.assert(rule.dlrs__FieldToAggregate__c != 'Name', 'Test cannot proceed - valid rule found for Property__c.Name');
        }


        Test.startTest();
        update properties;
        Test.stopTest();

        System.assert(!TriggerHandler.rollupsCalculated.contains('Property__c'), 'Rollups were calculated for modified Property records in an untracked field');
    }

    @isTest
    public static void testDelete(){
        List<Property__c> properties = [SELECT Id FROM Property__c];
        List<dlrs__LookupRollupSummary2__mdt> rules = TriggerHandler.getApplicableRules('Property__c');
        System.assert(!rules.isEmpty(), 'Test cannot proceed - no rules found for Property__c');

        Test.startTest();
        delete properties[0];
        Test.stopTest();

        System.assert(TriggerHandler.rollupsCalculated.contains('Property__c'), 'Rollups were not calculated for deleted Property records');
    }

    /**
     * @author : Publicis.Sapient
     * @description : Method that is used to test GetFieldsFromRules
     * @return : void
     */
    @isTest
    public static void testGetFieldsFromRules(){
        List<Property__c> properties = [SELECT Id FROM Property__c];
        List<dlrs__LookupRollupSummary2__mdt> rules = TriggerHandler.getApplicableRules('Property__c');
        System.assert(!rules.isEmpty(), 'Test cannot proceed - no rules found for Property__c');
        properties[0].Name = 'Test New Name';
        Set<String> fieldsToVerify = new Set<String>();
        for(dlrs__LookupRollupSummary2__mdt rule : rules){
            if(String.isNotBlank(rule.dlrs__relationshipcriteriafields__c)){
                fieldsToVerify.add(rule.dlrs__FieldToAggregate__c);
                fieldsToVerify.add(rule.dlrs__relationshipfield__c);
                fieldsToVerify.addAll(rule.dlrs__relationshipcriteriafields__c.split('\n'));
                break;
            }
        }

        Test.startTest();
        Set<String> fieldsReturned = Triggerhandler.getFieldsFromRules(rules);
        for(String fieldToVerify: fieldsToVerify){
            System.assert(fieldsReturned.contains(fieldToVerify), fieldToVerify+' Field should be present in the return list');
        }
        Test.stopTest();
    }
}