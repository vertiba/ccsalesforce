/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Conroller for UpdateAccountNoticeType VF page
//-----------------------------
public with sharing class UpdateAccountNoticeTypeController {
    
    private ApexPages.StandardSetController setController;
    public String recordStatus {get;set;} 
    
    public UpdateAccountNoticeTypeController(ApexPages.StandardSetController setController) {
        this.setController = setController;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This class is responsible to call Batch class
    // @return : void
    //-----------------------------
    public void redirect(){
        // 1 - Check whether user has permission to perform the action or not
        Boolean hasPermission = FeatureManagement.checkPermission(System.Label.AccessToUpdateAccountNoticeType); 
        
        
        // 2 - If user has permission then execute the batch job else, send error
        if (hasPermission) {            
            Database.executeBatch(new UpdatePropertyRequiredToFile(),Integer.valueOf(System.Label.UpdatePropertyRequiredToFileAndUpdateAccountNotiiceTypeBatchSize));
        	recordStatus = System.Label.UpdateAccountNoticeType;
        } else {
            recordStatus =System.Label.NoPermission;
        }
	}

}