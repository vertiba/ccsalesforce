/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/******************************************************************************************************
* This file is part of the Simple Trigger handler project, released under the MIT License.            *
* See LICENSE file or go to https://github.com/jongpie/SimpleTriggerHandler for full license details. *
******************************************************************************************************/
//-----------------------------
// @author : Publicis.Sapient
// @description : Abstract class that provides common logic & functionality for all Apex triggers
//-----------------------------
public abstract class TriggerHandler {

    private static final Set<Integer> PROCESSED_HASH_CODES = new Set<Integer>();
    // TODO consider making these configurable via custom setting or custom metadata
    private static final String IS_LOCKED_FIELD_NAME       = 'IsLocked__c';

    //Checks whether rollups were calculated during test
    @TestVisible private static Set<String> rollupsCalculated = new Set<String>();


    public void execute() {
        // Don't execute the trigger handler if triggers are disabled for the current user
        if(TriggerHandlerSettings__c.getInstance().DisableAllTriggers__c) {
            String message = 'Triggers disabled via custom setting, skipping processing';
            Logger.addDebugEntry(LoggingLevel.FINEST, message, this.getClassName());
            Logger.saveLog();
            return;
        }

        this.executeCustomCode();

        Logger.addDebugEntry(LoggingLevel.FINEST, 'Finished processing event ' + Trigger.operationType, this.getClassName());

        // If the SObject Type has been configured to use the custom locked records approach, validate any record changes
        if(Trigger.operationType == TriggerOperation.BEFORE_UPDATE) {
            this.preventChangesToLockedRecords();
        }

        // Our processing is done - flag the records as processed and call DLRS rules to calculate parent SObjects
        if(Trigger.isAfter) {
            this.flagRecordsAsProcessed();
            this.processDlrsRules();

            Logger.saveLog();
        }
    }

    protected virtual void executeBeforeInsert(List<SObject> newRecords) {}
    protected virtual void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {}
    protected virtual void executeBeforeDelete(List<SObject> deletedRecords, Map<Id, SObject> deletedRecordsById) {}
    protected virtual void executeAfterInsert(List<SObject> newRecords, Map<Id, SObject> newRecordsById) {}
    protected virtual void executeAfterUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {}
    protected virtual void executeAfterDelete(List<SObject> deletedRecords, Map<Id, SObject> deletedRecordsById) {}
    protected virtual void executeAfterUndelete(List<SObject> undeletedRecords, Map<Id, SObject> undeletedRecordsById) {}

    protected String getClassName() {
        return String.valueOf(this).split(':')[0];
    }

    private void executeCustomCode() {
        // Determine the list of records that should be processed by the trigger
        Boolean recordsProcessed = this.checkProcessedRecords();
        if(recordsProcessed) {
            Set<Id> recordIds = Trigger.newMap != null ? Trigger.newMap.keySet() : Trigger.oldMap.keySet();
            Integer currentHashCode = System.hashCode(recordIds);
            String message = 'Hash code ' + currentHashCode + ' already processed, skipping processing for event ' + Trigger.operationType.name();
            Logger.addDebugEntry(LoggingLevel.FINEST, message, this.getClassName());
            return;
        } else {
            Integer recordCount = Trigger.new != null ? Trigger.new.size() : Trigger.old.size();
            String message = 'Processing ' + recordCount + ' records in class ' + this.getClassName() + ' for event ' + Trigger.operationType.name();
            Logger.addDebugEntry(LoggingLevel.FINEST, message, this.getClassName());
        }

        switch on Trigger.operationType {
            when BEFORE_INSERT  { this.executeBeforeInsert(Trigger.new); }
            when BEFORE_UPDATE  { this.executeBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap); }
            when BEFORE_DELETE  { this.executeBeforeDelete(Trigger.old, Trigger.oldMap); }
            when AFTER_INSERT   { this.executeAfterInsert(Trigger.new, Trigger.newMap); }
            when AFTER_UPDATE   { this.executeAfterUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap); }
            when AFTER_DELETE   { this.executeAfterDelete(Trigger.old, Trigger.oldMap); }
            when AFTER_UNDELETE { this.executeAfterUndelete(Trigger.new, Trigger.newMap); }
        }
    }

    private Boolean checkProcessedRecords() {
        // Before insert, records do not have an ID, so there is no map, but we know the records are not processed yet
        if(Trigger.operationType == TriggerOperation.BEFORE_INSERT) return false;

        Set<Id> recordIds = Trigger.newMap != null ? Trigger.newMap.keySet() : Trigger.oldMap.keySet();
        return PROCESSED_HASH_CODES.contains(System.hashCode(recordIds));
    }

    private void preventChangesToLockedRecords() {
        // Dynamically retrieve the IsLocked field & LockedFields fieldset
        Schema.DescribeSObjectResult sobjectDescribe = Trigger.new.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> schemaFieldMap = sobjectDescribe.fields.getMap();
        String sObjectName = String.valueOf(Trigger.new.getSObjectType()).toLowerCase();
        Schema.SObjectField isLockedField = schemaFieldMap.get(IS_LOCKED_FIELD_NAME);
        Schema.FieldSet allowedFieldSet;

        LockedRecordSettings__c	byPassRecordLockSettings = LockedRecordSettings__c.getInstance(UserInfo.getUserId());
        byPassRecordLockSettings = byPassRecordLockSettings == null ? LockedRecordSettings__c.getInstance(UserInfo.getProfileId()) : byPassRecordLockSettings;
        byPassRecordLockSettings = byPassRecordLockSettings == null ? LockedRecordSettings__c.getOrgDefaults() : byPassRecordLockSettings;

        if(byPassRecordLockSettings == null || byPassRecordLockSettings.ObjectName__c == null) return;
        // We will use same name for fieldset across objects
        if(byPassRecordLockSettings.ObjectName__c.toLowerCase().contains(sObjectName) ) {
            allowedFieldSet = sobjectDescribe.fieldSets.getMap().get(byPassRecordLockSettings.FieldSetName__c);
        }

        // Check if the SObject Type has been configured to use locked records
        // If not, skip processing
        if(isLockedField == null) return;
        if(byPassRecordLockSettings.BypassRecordLock__c == true) return;

        Set<String> fieldsToNotCheck = new Set<String>();
        Set<String> fieldsToCheck = new Set<String>();

        if(allowedFieldSet != null) {
            for(Schema.FieldSetMember allowedFieldSetMember : allowedFieldSet.getFields()) {
                String allowedFieldName = allowedFieldSetMember.getFieldPath();
                fieldsToNotCheck.add(allowedFieldName.toLowerCase());
            }
        }

        // Loop for all Custom fields that need to be checked except those present in allowed fieldset.
        for(String fieldName: schemaFieldMap.keySet()) {
            Schema.SObjectField field = schemaFieldMap.get(fieldName);
            fieldName = fieldName.toLowerCase();
            if(!fieldsToNotCheck.contains(fieldName) && field.getDescribe().isUpdateable()) {
                fieldsToCheck.add(fieldName);
            }
        }

        // Check for field-level changes to locked records
        for(SObject record : Trigger.new) {
            // If the record isn't locked, skip processing
            if(record.get(isLockedField) == false) {
                Logger.addRecordDebugEntry(LoggingLevel.FINEST, record, 'Record is unlocked, skipping lock prevention logic', this.getClassName());
                continue;
            }

            SObject oldRecord = Trigger.oldMap.get(record.Id);

            // If the record was just changed to be locked, don't validate other field changes
            if(record.get(isLockedField) == true && oldRecord.get(isLockedField) == false) {
                Logger.addRecordDebugEntry(LoggingLevel.FINEST, record, 'Record was just updated to locked, skipping lock prevention logic', this.getClassName());
                continue;
            }

            // Thrown an error if any of the fieldset's fields have changed
            RecordChangeValidator recordChangeValidator = new RecordChangeValidator(record, oldRecord);
            if(recordChangeValidator.hasAnyFieldChanged(new List<String>(fieldsToCheck))) {
                try{
                    record.addError(System.Label.Locked_Record_Error_Message);
                } catch(Exception ex) {
                    Logger.addRecordDebugEntry(LoggingLevel.FINEST, record, 'Record is locked & 1 or more locked fields have detected', this.getClassName());
                    Logger.saveLog();
                    throw ex;
                }
            }
        }
    }

    private void flagRecordsAsProcessed() {
        Set<Id> recordIds = Trigger.newMap != null ? Trigger.newMap.keySet() : Trigger.oldMap.keySet();
        PROCESSED_HASH_CODES.add(System.hashCode(recordIds));
    }

    /**
	 * @author : Publicis.Sapient
     * @description : This method returns list of Map<Id, SObject> records as follows:
     * 		on Insert, Undelete Trigger.NewMap as the first entry in the list. 
     * 		on Delete Trigger.oldMap as the first entry in the list.
     * 		for update if no rules exists, or no fields that require calc then empty list. 
     * 		else, Map of new and old records, for which calculation is required.
     * @param : Map<Id, SObject> New map of records
     * @param : Map<Id, SObject> old map of records
     * @return : List<Map<Id,SObject>> returns list of DLRS rules
     */
    private static List<Map<Id,SObject>> getRecordsToRollup(Map<Id, SObject> oldMap, Map<Id, SObject> newMap){
        List<Map<Id,SObject>> toReturn = new List<Map<Id,SObject>>();
        //Get the object type to get the dlrs rules
        SObject example = Trigger.isDelete ? oldMap.values()[0] : newMap.values()[0];
        String sobjectType = example.getSObjectType().getDescribe().getName();
        //get the rules
        List<dlrs__LookupRollupSummary2__mdt> applicableRules = getApplicableRules(sobjectType);
        //if records dont exist, then return empty list
        if(applicableRules.isEmpty()){
            return toReturn;
        }

        //if insert or undelete, then populate trigger.newMap in list and return.
        if(Trigger.isInsert || Trigger.isUndelete){
            toReturn.add(newMap);
            return toReturn;
        }

        //if delete, then populate trigger.oldMap in list and return.
        if(Trigger.isDelete){
            toReturn.add(oldmap);
            return toReturn;
        }

        //Get the fields that needs to be verified if they are changed. 
        List<String> aggregatedFields = new List<String>(getFieldsFromRules(applicableRules));
		Map<Id,SObject> oldvalues = new Map<Id,SObject>();
        Map<Id,SObject> newvalues = new Map<Id,SObject>();
        for(Id objectId : newMap.keySet()){
            SObject oldRecord = oldMap.get(objectId);
            SObject newRecord = newMap.get(objectId);
            //Verify if there is any field that is part of DLRS rules is changed. 
            RecordChangeValidator rcv = new RecordChangeValidator(newRecord, oldRecord);
            if(rcv.hasAnyFieldChanged(aggregatedFields)){
                //if the field is changed then add to below lists to re-calculate DLRS
                newvalues.put(newRecord.id, newRecord);
                oldvalues.put(oldRecord.id, oldRecord);
                continue;
            }
        }

        if(newvalues.size()>0){
            //Either both has values or dont have values. 
            toReturn.add(newvalues);
            toReturn.add(oldvalues);            
        }
        return toReturn;
    }

    /**
     * @author : Publicis.Sapient
     * @description : Returns list of active DLRS rules for a given object.
     * @param : sObjectType  object type for which DLRS rules are returned
     * @return : List<dlrs__LookupRollupSummary2__mdt> returns list of DLRS rules
     */
    @testVisible private static List<dlrs__LookupRollupSummary2__mdt> getApplicableRules(String sObjectType){
        return [SELECT dlrs__FieldToAggregate__c, dlrs__relationshipcriteriafields__c, dlrs__relationshipfield__c
                FROM dlrs__LookupRollupSummary2__mdt
                WHERE dlrs__ChildObject__c = :sObjectType
                AND dlrs__Active__c = true 
                AND dlrs__CalculationMode__c = 'Developer'];
    }

    /**
     * @author : Publicis.Sapient
     * @description : take a list of DLRS rules and from each rule extract, field to 
     * 		aggregate, critera, relation ship fields and send as a set. If any of 
     * 		those fields are changed for a record, then DLRS is executed for that record.
     * @param : rules DLRS rules for a given object
     * @return : Set<String> field APIs as a set of string. 
     */
    @testVisible private static Set<String> getFieldsFromRules(List<dlrs__LookupRollupSummary2__mdt> rules){
        Set<String> toReturn = new Set<String>();
        for(dlrs__LookupRollupSummary2__mdt rule : rules){
            toReturn.add(rule.dlrs__FieldToAggregate__c);
            toReturn.add(rule.dlrs__relationshipfield__c);
            if(String.isNotBlank(rule.dlrs__relationshipcriteriafields__c)){
                toReturn.addAll(rule.dlrs__relationshipcriteriafields__c.split('\n'));
            }
        }
        return toReturn;
    }
    
    /**
     * @author : Publicis.Sapient
     * @description : by calling other methods, prepares a list of records and calls 
     * 		AsynchDLRScalculator for executing DLRS on those records.
     * @return : void. 
     */
    private void processDlrsRules() {
        if(TriggerHandlerSettings__c.getInstance().DisableDLRSCalculationTriggers__c) {
            String message = 'DLRS calculations disabled via custom setting, skipping processing in ' + this.getClassName();
            Logger.addDebugEntry(LoggingLevel.FINEST, message, this.getClassName());
        } else {
            List<Map<Id,SObject>> modifiedObjects = getRecordsToRollup(Trigger.oldMap, Trigger.newMap);
            if(modifiedObjects != null && !modifiedObjects.isEmpty()){
                String message = 'Running DLRS calculations in ' + this.getClassName();
                if(!Trigger.isDelete){
                    rollupsCalculated.add(Trigger.newMap.values()[0].getSObjectType().getDescribe().getName());
                }else{
                    rollupsCalculated.add(Trigger.oldMap.values()[0].getSObjectType().getDescribe().getName());
                }
                Logger.addDebugEntry(LoggingLevel.FINEST, message, this.getClassName());
                if(trigger.isUpdate && modifiedObjects.size() == 2){
                    AsyncDlrsCalculator.getInstance().calculate(modifiedObjects.get(0), modifiedObjects.get(1));
                } else {
                    AsyncDlrsCalculator.getInstance().calculate(modifiedObjects.get(0), null);
                }
            }
        }
    }
}