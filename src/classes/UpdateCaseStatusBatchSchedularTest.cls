/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test UpdateCaseStatusBatchSchedular
//-----------------------------
@isTest
public class UpdateCaseStatusBatchSchedularTest {
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to verify that a job is scheduled successfully in a future date.
    //-----------------------------  
    @isTest
    private static void jobShouldBeScheduledToRunInFuture() {
        Test.startTest();
        String jobId = System.schedule('UpdateCaseStatusBatchSchedularTest',
                                       '0 0 0 15 3 ? 2032', 
                                       new UpdateCaseStatusBatchSchedular()); 
        Test.stopTest();
        
        system.assertEquals(1, [SELECT count() FROM CronTrigger where id = :jobId],
                            'A job should be scheduled');
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to verify that a job name prefix is correct.
    //----------------------------- 
    @isTest
    private static void getJobNamePrefixTest() {
        system.assertEquals('UpdateCaseIntegrationStatusBatch Schedular',new UpdateCaseStatusBatchSchedular().getJobNamePrefix());
    }
}