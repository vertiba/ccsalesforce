/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class VIPFormsList extends VisualEditor.DynamicPickList {

    public override VisualEditor.DataRow getDefaultValue() {
        VisualEditor.DataRow value = new VisualEditor.DataRow('None', '');
        return value;
    }

    public override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DynamicPickListRows picklistValues = new VisualEditor.DynamicPickListRows();
		picklistValues.addRow(getDefaultValue());

        for(VIPForm__VIP_Template__c template : [SELECT Id, Name FROM VIPForm__VIP_Template__c]) {
            picklistValues.addRow(new VisualEditor.DataRow(template.Name, template.Name));
        }

        return picklistValues;
    }

}