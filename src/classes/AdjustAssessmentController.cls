/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR-8552. Controller claas to be called after Adjust Assessment button pressed.
//-----------------------------
public class AdjustAssessmentController {
    public String CASE_QUERY;
    public string recordStatus {get;set;} 
    public string baseURL {get;set;}
    Map<Id,Id> recordTypeId = new Map<Id,Id>();
    List<Case> cas = new List<Case>();
    Case currentCase = new Case();
    private String originLocation ='AdjustAssessmentController';
    
    public AdjustAssessmentController(ApexPages.StandardController standardController){
       currentCase = (Case) standardController.getRecord();
    }
    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @description : This method is redirecting to VF page AdjustAssessmentPage
    // @return : void
    //-----------------------------
    public void redirect(){
        //Check whether user has permission to perform the action or not
        Boolean hasPermission = FeatureManagement.checkPermission(originLocation); 
        //If user don't have permission display a message of No Permission
        if(!hasPermission) { 
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + currentCase.Id;
            recordStatus = System.Label.NoPermission;
            return;
        }

        CASE_QUERY = DescribeUtility.getDynamicQueryString('Case',null);
        
        String recordId = currentCase.id;
        String query = CASE_QUERY + ' WHERE Id =: recordId';
        cas = new List<Case>((List<Case>)Database.query(query));
        /* Filter to be passed for AssessmentsUtility.retrieveCaseToPerformAction method */
        String filter = 'AND Status =\''+ CCSFConstants.ASSESSMENT.STATUS_CLOSED +'\'';
        filter += ' AND AssessmentYear__c =\''+ cas[0].AssessmentYear__c+'\'';
        filter += ' AND AssessmentNumber__c =\''+ cas[0].AssessmentNumber__c+'\'';
        filter += ' AND ( SubStatus__c =\''+ CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED+'\'';
        filter += ' OR  SubStatus__c =\''+ CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING +'\'';
        filter += ')';
        
        //As part of ASR-9866 considering In Transit to RP/Sent to RP cases and disabling Ready to Send to TTX cases
      	if( (!CCSFConstants.ASSESSMENT.SENT_TO_TTX.equalsIgnoreCase(cas[0].IntegrationStatus__c) && !CCSFConstants.ASSESSMENT.IN_TRANSIT_TO_TTX.equalsIgnoreCase(cas[0].IntegrationStatus__c) 
           	&& !CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_IN_TRANSIT_TO_RP.equalsIgnoreCase(cas[0].IntegrationStatus__c) && !CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_SENT_TO_RP.equalsIgnoreCase(cas[0].IntegrationStatus__c))
           	|| !AssessmentsUtility.retrieveCaseToPerformAction(cas[0].Property__c,cas[0].Id,filter)) {
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + cas[0].Id;
            recordStatus = System.Label.AdjustAssessmentCreatedNotAllowed;
            return;
        }
        
        if(cas[0].RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_LABEL_NAME).getRecordTypeId()){
            recordTypeId.put(cas[0].RecordTypeId,Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.BPP_RECORD_LABEL_NAME).getRecordTypeId());
        }else if(cas[0].RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_LABEL_NAME).getRecordTypeId()){
            recordTypeId.put(cas[0].RecordTypeId, Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.VESSEL_RECORD_LABEL_NAME).getRecordTypeId());
        }

        cloningOfAssessmentAndLineItems(); 
        recordStatus = System.Label.AdjustAssessmentCreated;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method is called from redirect method for cloning the Assessments
    // @return : void
    //-----------------------------
    public void cloningOfAssessmentAndLineItems() {
        List<RollYear__c> closedRollYears = [Select Id,Status__c,Year__c from RollYear__c where Status__c ='Roll Closed' ORDER BY Year__c DESC];
        Map<String,List<AssessmentLineItem__c>> assessmentLineItems = new Map<String,List<AssessmentLineItem__c>>();
        List<Case> assessment =  new List<Case>(); 
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        List<AssessmentLineItem__c> lineItem  = new List<AssessmentLineItem__c>();
        Id assessmentLegacyBPPRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME);
        Id assessmentBPPRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
        Id assessmentLegacyRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME);
        Id assessmentVessselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME);
        Map<Id,Id> recordtypeIdForLegacyRecord = new Map<Id,Id>{assessmentLegacyRecordTypeId => assessmentVessselRecordTypeId,assessmentLegacyBPPRecordTypeId => assessmentBPPRecordTypeId};
        Map<Id,List<AssessmentLineItem__c>> caseByLineItems = AssessmentsUtility.retrieveAssessmentAndLineItems(cas);
        
        //Calling the Assessment Utility method for cloning
        Map<String,Object> clonedDataMapByListNames =  AssessmentsUtility.retrieveCloneAssessmentsForAdjustments(cas,caseByLineItems,recordtypeIdForLegacyRecord);
       	Date eventDate = Date.newInstance(Integer.valueOf(cas[0].AssessmentYear__c), 1, 1);
        try{
            if(!clonedDataMapByListNames.isEmpty()) { 
                assessment =  (List<Case>)clonedDataMapByListNames.get('assessments');
                assessmentLineItems = (Map<String,List<AssessmentLineItem__c>>)clonedDataMapByListNames.get('assessmentLIBySourceId');
            }
            for(Case cs : assessment){
                cs.Status = CCSFConstants.ASSESSMENT.STATUS_NEW;
                cs.SubStatus__c =  null;
                cs.IntegrationStatus__c = null;
                cs.SequenceNumber__c = null;
                cs.isLocked__c = false;
                cs.AssessmentNumber__c = null;
                cs.ownerId = userInfo.getUserId();
                if(cas[0].Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR && cas[0].AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                    cs.AdjustmentType__c = null;
            	}
                cs.Roll__c = closedRollYears[0].Id;
                //cs.EventDate__c=cas[0].EventDate__c;
                //ASR-10029 -Updating Event Date as 01/01/Assessment Year of the Parent case
                cs.EventDate__c	= eventDate;
                cs.Type=cas[0].Type;
                cs.SubType__c=cas[0].SubType__c;
                cs.ParentId = cas[0].id;
                //ASR-10768 Changes
                cs.AdjustmentReason__c = null;
            }
        	
            Map<String,Object> insertCase = DescribeUtility.insertRecords(assessment,dmlOptions);
            List<AssessmentLineItem__c> assessmentLineItemsToInsert = new List<AssessmentLineItem__c>();
            if(!assessmentLineItems.isEmpty()){
                lineItem = (List<AssessmentLineItem__c>)assessmentLineItems.get(cas[0].id);
                for(AssessmentLineItem__c assessmentLineItem : lineItem){
                    AssessmentLineItem__c clonedLineItem = assessmentLineItem;
                    assessmentLineItem.Case__c = assessment[0].id;
                }
                Map<String,Object> insertLineItems = DescribeUtility.insertRecords(lineItem,dmlOptions);            
            } 
            if(assessment[0].Id != null){
                baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + assessment[0].Id; 
            }
       }catch(Exception ex) {
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + cas[0].Id;
            recordStatus = System.Label.NoPermission;
            String error = ex.getmessage();
            Logger.addDebugEntry(error, originLocation); 
        }
        Logger.saveLog();
    }
}