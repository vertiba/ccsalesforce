/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Batch class to set Notice type on Account
//-----------------------------
public class UpdateAccountNoticeType implements Database.Batchable<sObject>, BatchableErrorHandler {
    private String rollYear;
    private String lastYear;
    private String currentYear;

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Constructor method.
    //-----------------------------
    public UpdateAccountNoticeType() {
        rollYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        lastYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        currentYear = String.valueOf(System.today().year());
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : Database.QueryLocator
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext context) {
        String query='SELECT Id, Name, TotalValueofProperties__c, TotalDirectBillCost__c,TotalCostofProperties__c, RequiredToFileYear__c, NoticeType__c ';
        query += ' FROM Account WHERE BusinessStatus__c = \'Active\' and RecordType.DeveloperName = \'BusinessAccount\' ';
        Logger.addDebugEntry(query, 'UpdateAccountNoticeType-start method' );
        Logger.saveLog();

        return Database.getQueryLocator([
         SELECT Id, Name, TotalValueofProperties__c, TotalDirectBillCost__c,TotalCostofProperties__c, RequiredToFileYear__c, NoticeType__c,
            (SELECT Id, RecordType.DeveloperName FROM PropertyLocations__r) 
         FROM Account
         WHERE BusinessStatus__c = 'Active' and RecordType.DeveloperName = 'BusinessAccount'
        ]);         
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : List<sObject> 
    // @description : Validating all the criterias and assigning notice type to account
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext context, List<Account> accounts) {
        Map<Id, Account> accountMap = new Map<Id, Account>(accounts);
        Map<Id, String> noticeTypeMap = AccountNoticeTypeUtility.getNoticeTypesForAccounts(accounts);
        List<Account> accountsToProcess = new List<Account>();

        for(Id accountId : accountMap.keySet()){
            Account acc = accountMap.get(accountId);
            String noticeType = noticeTypeMap.get(accountId);
            if(acc.NoticeType__c != noticeType){
                acc.NoticeType__c = noticeType;
                accountsToProcess.add(acc);
            }
        }
        
        if(accountsToProcess.isEmpty()) return;
        update accountsToProcess;
    }

    //-----------------------------
    // @author : Akhil Kumar
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext context) {
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, UpdateAccountNoticeType.class.getName(), asyncList[0].ParentJobId, false, '', asyncList[0].NumberOfErrors);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
}