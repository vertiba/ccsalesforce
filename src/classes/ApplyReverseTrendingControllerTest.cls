/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class ApplyReverseTrendingControllerTest {

    @testSetup
    private static void commonDataSetupForTesting(){
		Test.startTest();
        Account account = TestDataUtility.buildBusinessAccount('New Test Account');
        insert account;
        
        List<Penalty__c> penalties = new List<Penalty__c>();
        penalties.add(TestDataUtility.buildPenalty(500,10,'463'));
        penalties.add(TestDataUtility.buildPenalty(500,25,'214.13'));
        penalties.add(TestDataUtility.buildPenalty(500,25,'504'));
        insert penalties;

        Integer fiscalYear = FiscalYearUtility.getCurrentFiscalYear();
		Date filingDate = Date.newInstance(fiscalYear,4,5);

        List<RollYear__c> rolls = new List<RollYear__c>();
        rolls.add(TestDataUtility.buildRollYear(String.valueOf(fiscalYear-1),String.valueOf(fiscalYear-1),'Roll Closed'));
		rolls.add(TestDataUtility.buildRollYear(String.valueOf(fiscalYear),String.valueOf(fiscalYear),'Roll Open'));
		rolls.add(TestDataUtility.buildRollYear(String.valueOf(fiscalYear+1),String.valueOf(fiscalYear+1),'Pending'));
        insert rolls;

        Property__c property = TestDataUtility.buildBusinessPersonalProperty('new test property',account.Id);
        insert property;
		TestDataUtility.disableAutomationCustomSettings(true);
        /*Statement__c statement = new Statement__c();
        statement.FileDate__c = filingDate;
        statement.Property__c = property.id;
        statement.PropertyScenario__c = 'Existing';
        statement.RecordTypeId = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalPropertyStatement').getRecordTypeId();
        insert statement;*/
        
        Id bppAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Case cas = TestDataUtility.buildAssessment(bppAssessmentRecordTypeId,
            property.Id, rolls.get(1).Id, rolls.get(1).Year__c, rolls.get(1).Year__c, 'Regular', 48, 'Notice to File',null,
            Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1));
		cas.AdjustmentType__c = 'New';
        cas.MailingCountry__c = 'US';
        insert cas;

        Case previouscas = TestDataUtility.buildAssessment(bppAssessmentRecordTypeId,
            property.Id, rolls.get(0).Id, rolls.get(0).Year__c, rolls.get(0).Year__c, 'Regular', 48, 'Notice to File',null,
            Date.newInstance(FiscalYearUtility.getFiscalYear(System.today())-1, 1, 1));
        previouscas.AdjustmentType__c = 'New';
        previouscas.MailingCountry__c = 'US';
        insert previouscas;
        
        List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
        AssessmentLineItem__c assessmentLineItem1 = new AssessmentLineItem__c();
        assessmentLineItem1.Category__c ='Supplies';
        assessmentLineItem1.Subcategory__c ='Supplies';
        assessmentLineItem1.AssetSubclassification__c ='General';
        assessmentLineItem1.Cost__c = 15000.00;
        assessmentLineItem1.AcquisitionYear__c = '2017';
        assessmentLineItem1.Case__c = previouscas.id;
        assessmentLineItem1.Description__c = 'Assessment Line Item for last year';
        assessmentLineItems.add(assessmentLineItem1);
        AssessmentLineItem__c assessmentLineItem2 = new AssessmentLineItem__c();
        assessmentLineItem2.Category__c ='Equipment';
        assessmentLineItem2.Subcategory__c ='Machinery & Equipment';
        assessmentLineItem2.AssetSubclassification__c ='Billiard';
        assessmentLineItem2.Cost__c = 25000.00;
        assessmentLineItem2.AcquisitionYear__c = '2016';
        assessmentLineItem2.Case__c = previouscas.id;
        assessmentLineItem2.Description__c = 'Assessment Line Item for last year';
        assessmentLineItems.add(assessmentLineItem2);
        AssessmentLineItem__c assessmentLineItem3 = new AssessmentLineItem__c();
        assessmentLineItem3.Category__c ='Supplies';
        assessmentLineItem3.Subcategory__c ='Supplies';
        assessmentLineItem3.AssetSubclassification__c ='General';
        assessmentLineItem3.Cost__c = 45000.00;
        assessmentLineItem3.AcquisitionYear__c = '2016';
        assessmentLineItem3.Case__c = previouscas.id;
        assessmentLineItem3.Description__c = 'Assessment Line Item for last year';
        assessmentLineItems.add(assessmentLineItem3);
        
        insert assessmentLineItems;
        TestDataUtility.disableAutomationCustomSettings(false);
		Test.stopTest();
    }

    @isTest
    private static void fetchAssetPickListValues(){
        Map<String,String> picklistOptions = ApplyReverseTrendingController.fetchAssetClassificationPicklists();
        System.assertEquals(true, picklistOptions != null);
        System.assert(picklistOptions.size()>0, 'Should have many picklist values');
    }

    @isTest
    private static void fetchSubAssetPickListValues(){
        Map<String,String> picklistOptions = ApplyReverseTrendingController.fetchSubAssetClassificationPicklists('Supplies');
        System.assertEquals(true, picklistOptions != null);
        System.assertEquals(picklistOptions.size(),1, 'Dependent picklist values for supplies should be one');
        picklistOptions = ApplyReverseTrendingController.fetchSubAssetClassificationPicklists('Machinery & Equipment');
        System.assert(picklistOptions.size()>0, 'Dependent picklist values for Machinery & Equipment should be more');
    }

    @isTest
    private static void fetchRelatedAssessmentLineItems(){
        RollYear__c roll = [select id, year__c from RollYear__c where status__c = 'Roll Closed' order by Year__c desc limit 1];

        Case cas = [select id, property__c, AccountId from case where Roll__c =: roll.Id];
        Test.startTest();
        List<AssessmentLineItem__c> assessmentLineItems = 
            ApplyReverseTrendingController.fetchRelatedAssessmentLineItems(cas.Id, 'Supplies', 'General');
        System.assertEquals(2, assessmentLineItems.size(), 'Expected 2 line items');
        assessmentLineItems = 
            ApplyReverseTrendingController.fetchRelatedAssessmentLineItems(cas.Id, 'Machinery & Equipment', 'Billiard');
        System.assertEquals(1, assessmentLineItems.size(), 'Expected 1 line items');
        Test.stopTest();
    }
    
    @isTest
    private static void saveAssessmentLineItems(){
        RollYear__c roll = [select id, year__c from RollYear__c where status__c = 'Roll Closed' order by Year__c desc limit 1];

        Case cas = [select id, property__c, AccountId from case where Roll__c =: roll.id];
        List<AssessmentLineItem__c> assessmentLineItems = 
            [select id, Name, AcquisitionYear__c, Cost__c, ReverseTrendingAffectedYear__c, ReverseTrendingCostRemoved__c, 
                 ReverseTrendingAdjustedCost__c from AssessmentLineItem__c where subcategory__c = 'Supplies'];
        System.assertEquals(2, assessmentLineItems.size(), 'Expected 2 line items');
        for(AssessmentLineItem__c lineitem : assessmentLineItems){
            lineitem.ReverseTrendingAffectedYear__c = '2019';
            lineitem.ReverseTrendingCostRemoved__c = 10000.00;
            lineitem.ReverseTrendingAdjustedCost__c = lineitem.Cost__c - lineitem.ReverseTrendingCostRemoved__c;
        }
        TestDataUtility.disableAutomationCustomSettings(true);
        //Updating last years case so disabling validation rules, triggers PB 
        Test.startTest();
        ApplyReverseTrendingController.saveLineItemsWithReverseTrendingValues(assessmentLineItems);
        for(AssessmentLineItem__c lineitem : 
            [select ReverseTrendingAffectedYear__c, ReverseTrendingCostRemoved__c, Cost__c, 
             ReverseTrendingAdjustedCost__c from AssessmentLineItem__c where subcategory__c = 'Supplies']){
                 System.assertEquals('2019', lineitem.ReverseTrendingAffectedYear__c);
                 System.assertEquals(10000.00, lineitem.ReverseTrendingCostRemoved__c);
        }
        Test.stopTest();
        TestDataUtility.disableAutomationCustomSettings(false);
    }
    
    @isTest
    private static void testGetFactorForAcquistionYear(){
        Map<String,Factor__c> dafaultFactorByYear = new Map<String,Factor__c>();
        Factor__c factor = new Factor__c();
        ApplyReverseTrendingController.defaultFactor = dafaultFactorByYear;
        Map<String, Map<String, Factor__c>> factorByAcquistionYears = new Map<String, Map<String, Factor__c>>();
        Double factorPercent = ApplyReverseTrendingController.getFactorForAcquistionYear('2020', '2014', factorByAcquistionYears);
        System.assertEquals(1.0, factorPercent);
        factor.FactorPercent__c = 200;
        factor.AcquisitionYear__c = '2005';
        factor.IsFallback__c = true;
        dafaultFactorByYear.put('2020',factor);
		factorPercent = ApplyReverseTrendingController.getFactorForAcquistionYear('2020', '2005', factorByAcquistionYears);
        System.assertEquals(2.0, factorPercent);
        factor = new Factor__c();
        factor.FactorPercent__c = 300;
        factor.AcquisitionYear__c = '2018';
        dafaultFactorByYear.put('2018',factor);
        factorByAcquistionYears.put('2020',dafaultFactorByYear);
        factorPercent = ApplyReverseTrendingController.getFactorForAcquistionYear('2020', '2018', factorByAcquistionYears);
        System.assertEquals(3.0, factorPercent);
    }
    
    @isTest
    private static void testGetFactorsForReverseTrending(){
        FactorSource__c factorSource = new FactorSource__c();
        factorSource.Name = 'Test Source';
		insert factorSource;
        BusinessAssetMapping__c businessAssetMapping = new BusinessAssetMapping__c();
        businessAssetMapping.AssetClassification__c = 'Supplies';
        businessAssetMapping.AssetSubclassification__c = 'General';
        businessAssetMapping.FactorSource__c = factorSource.id;
        businessAssetMapping.FixturesAllocationPercent__c = 0;
        businessAssetMapping.FactorType__c =Label.ReverseTrendingFactor;
        businessAssetMapping.Subcomponent__c = 'Average';
        insert businessAssetMapping;
        
        Id recordTypeId = Schema.SObjectType.Factor__c.getRecordTypeInfosByName().get(Label.ReverseTrendingFactor).getRecordTypeId();
        List<Factor__c> factors = new List<Factor__c>();
        for(Integer index = 0; index<5;index++){
            Factor__c factor = new Factor__c();
            factor.RecordTypeId = recordTypeId;
            factor.FactorSource__c = factorSource.id;
            factor.AcquisitionYear__c = String.valueOf(2015+index);
            factor.AssessmentYear__c = '2020';
            factor.FactorPercent__c = 107;
            factor.IsFallback__c = index == 0 ? true : false;
            factor.Subcomponent__c = 'Average';
            factors.add(factor);
        }
        insert factors;
        Test.startTest();
        Set<string> years = new Set<String>();
        years.add('2020');
        Map<String, Map<String, Factor__c>> factorsByAcquistionYear = ApplyReverseTrendingController.getFactorsForReverseTrending('Test','Test',years);
        System.assertEquals(1, factorsByAcquistionYear.size());
        System.assertEquals(0, factorsByAcquistionYear.get('2020').size());
        factorsByAcquistionYear = ApplyReverseTrendingController.getFactorsForReverseTrending('Supplies','General',years);
        System.assertEquals(1, factorsByAcquistionYear.size());
        System.assertEquals(5, factorsByAcquistionYear.get('2020').size());
        System.assert(ApplyReverseTrendingController.defaultFactor != null);
        System.assertEquals(1, ApplyReverseTrendingController.defaultFactor.size());
        System.assert(ApplyReverseTrendingController.defaultFactor.containsKey('2020'));
        Test.stopTest();
    }
    
    @isTest
    private static void testGetAssessmentLineItemsInRange(){
        List<AssessmentLineItem__c> assessmentLineItems = [select id, AcquisitionYear__c from AssessmentLineItem__c where Subcategory__c =:'Supplies'];
        System.assertEquals(2, assessmentLineItems.size(), 'Expected 2 line items');
        Test.startTest();
        List<AssessmentLineItem__c> testAssessmentLineItems = 
            ApplyReverseTrendingController.getAssessmentLineItemsInRange(assessmentLineItems, 2016, 2017);
        System.assertEquals(2, testAssessmentLineItems.size(), 'Expected 2 line items');
        testAssessmentLineItems = 
            ApplyReverseTrendingController.getAssessmentLineItemsInRange(assessmentLineItems, 2016, 2016);
        System.assertEquals(1, testAssessmentLineItems.size(), 'Expected 1 line items');
        testAssessmentLineItems = 
            ApplyReverseTrendingController.getAssessmentLineItemsInRange(assessmentLineItems, 2017, 2017);
        System.assertEquals(1, testAssessmentLineItems.size(), 'Expected 1 line items');
        testAssessmentLineItems = 
            ApplyReverseTrendingController.getAssessmentLineItemsInRange(assessmentLineItems, 2014, 2014);
        System.assertEquals(0, testAssessmentLineItems.size(), 'Expected 0 line items');
        Test.stopTest();
    }
    
    @isTest
    private static void testGetLastYearsReverseTrendedLineItem(){
        AssessmentLineItem__c lastYearItem = [select id, ReverseTrendingAffectedYear__c, ReverseTrendingCostRemoved__c 
                                              from AssessmentLineItem__c where subCategory__c =: 'Supplies' 
                                              order by AcquisitionYear__c asc limit 1];
        RollYear__c roll = [select id, year__c from RollYear__c where status__c = 'Roll Open' order by Year__c desc limit 1];
        //Updating last years lineitems, so disabling the validations, Triggers and PB.
        TestDataUtility.disableAutomationCustomSettings(true);
        Case cas = [select id, Property__c, AssessmentYear__c from case where Roll__c =: roll.id];
        AssessmentLineItem__c lineItem = ApplyReverseTrendingController.getLastYearsReverseTrendedLineItem(cas,'Supplies','General');
        System.assertEquals('2016', lineItem.AcquisitionYear__c, 'First acquistion year should be returned');
        lastYearItem.ReverseTrendingAffectedYear__c = '2018';
        lastYearItem.ReverseTrendingCostRemoved__c = 0;
        update lastYearItem;
        lineItem = ApplyReverseTrendingController.getLastYearsReverseTrendedLineItem(cas,'Supplies','General');
        System.assertEquals('2017', lineItem.AcquisitionYear__c, 'Second line item should be returned.');
        lastYearItem.ReverseTrendingCostRemoved__c = 100;
        update lastYearItem;
        lineItem = ApplyReverseTrendingController.getLastYearsReverseTrendedLineItem(cas,'Supplies','General');
        System.assertEquals('2016', lineItem.AcquisitionYear__c, 'Second line item should be returned.');
        TestDataUtility.disableAutomationCustomSettings(false);
    }

    @isTest
    private static void testfetchReverseTrendingDetails(){
        TestDataUtility.disableAutomationCustomSettings(true);
        //Updating last years ALI so disabling validation rules, triggers PB
        AssessmentLineItem__c lastYearItem = [select id, ReverseTrendingAffectedYear__c, ReverseTrendingCostRemoved__c 
                                              from AssessmentLineItem__c where subCategory__c =: 'Supplies' 
                                              order by AcquisitionYear__c asc limit 1];
        lastYearItem.ReverseTrendingAffectedYear__c = '2017';
        lastYearItem.ReverseTrendingCostRemoved__c = 100;
        lastYearItem.ReverseTrendingAdjustedCost__c = 100;
        update lastYearItem;
        TestDataUtility.disableAutomationCustomSettings(false);
        RollYear__c roll = [select id, Year__c from RollYear__c where status__c = 'Roll Open' order by Year__c desc limit 1];
        
        Case cas = [select id from case where Roll__c =: roll.id];
        FactorSource__c factorSource = new FactorSource__c();
        factorSource.Name = 'Test Source';
		insert factorSource;
        BusinessAssetMapping__c businessAssetMapping = new BusinessAssetMapping__c();
        businessAssetMapping.AssetClassification__c = 'Supplies';
        businessAssetMapping.AssetSubclassification__c = 'General';
        businessAssetMapping.FactorSource__c = factorSource.id;
        businessAssetMapping.FixturesAllocationPercent__c = 0;
        businessAssetMapping.FactorType__c =Label.ReverseTrendingFactor;
        businessAssetMapping.Subcomponent__c = 'Average';
        insert businessAssetMapping;
        Id recordTypeId = Schema.SObjectType.Factor__c.getRecordTypeInfosByName().get(Label.ReverseTrendingFactor).getRecordTypeId();
        List<AssessmentLineItem__c> lineItems = new List<AssessmentLineitem__c>();
        List<Factor__c> factors = new List<Factor__c>();
        AssessmentLineItem__c lineItem;
        Factor__c factor;

        for(integer index = 0; index<5; index++){
            lineItem = new AssessmentLineItem__c();
            factor = new Factor__c();
            factor.RecordTypeId = recordTypeId;
            factor.FactorSource__c = factorSource.id;
            factor.AcquisitionYear__c = String.valueOf(2015+index);
            factor.AssessmentYear__c = '2020';
            factor.FactorPercent__c = 110 - (index * 2);
            factor.Subcomponent__c = 'Average';
            factors.add(factor);
            
            lineItem.Category__c ='Supplies';
            lineItem.Subcategory__c ='Supplies';
            lineItem.AssetSubclassification__c = 'General';
            lineItem.Cost__c = 1500.00 - (10 * index);
            lineItem.AcquisitionYear__c = String.valueOf(2015+index);
            lineItem.Case__c = cas.id;
            lineItem.Description__c = 'Assessment Line Item for Current year';
			lineItems.add(lineItem);
        }
        insert lineItems;
        lineItems = ApplyReverseTrendingController.fetchReverseTrendingDetails(cas.Id,'Supplies','General','2015','2017');
        for(AssessmentLineItem__c item:lineItems){
            System.assertEquals('', item.ReverseTrendingAffectedYear__c, 'Items are out of range, so no calculation');
            System.assertEquals(0, item.ReverseTrendingCostRemoved__c, 'Items are out of range, so no calculation');
        }
        lineItems = ApplyReverseTrendingController.fetchReverseTrendingDetails(cas.Id,'Supplies','General','2018','2019');
        for(AssessmentLineItem__c item:lineItems){
            if(item.AcquisitionYear__c == '2016'){
                System.assertEquals('2018', item.ReverseTrendingAffectedYear__c, '2018 value is not taken for calculation');
                System.assertEquals(100, item.ReverseTrendingCostRemoved__c, 'some value will be there.');
                System.assertEquals(0, item.ReverseTrendingAdjustedCost__c, 'Entire value should be removed.');
            }
            if(item.AcquisitionYear__c == '2017'){
                System.assertEquals('2018, 2019', item.ReverseTrendingAffectedYear__c, '2018, 2019 value is not taken for calculation');
                System.assertEquals(1480, item.ReverseTrendingCostRemoved__c, 'some value will be there.');
                System.assertEquals(0, item.ReverseTrendingAdjustedCost__c, 'Entire value should be removed.');
            }
        }
    }
}