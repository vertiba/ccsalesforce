/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
global class SelfRegistrationHandler implements Auth.ConfigurableSelfRegHandler {

    private final static Long CURRENT_TIME            = Datetime.now().getTime();
    private final static String LOGGER_LOCATION       = 'SelfRegistrationHandler';
    private final static List<String> LOWERCASE_CHARS = 'abcdefghijklmnopqrstuvwxyz'.split('');
    private final static List<String> NUMBER_CHARS    = '1234567890'.split('');
    private final static List<String> SPECIAL_CHARS   = '!#$%-_=+<>'.split('');
    private final static List<String> UPPERCASE_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');


    // This method is called once after verification (if any was configured)
    // This method should create a user and insert it
    // Password can be null
    // Return null or throw an exception to fail creation
    global Id createUser(Id accountId, Id profileId, Map<SObjectField, String> registrationAttributes, String password) {
        try {
            Logger.addDebugEntry(LoggingLevel.FINEST, 'start createUser method', LOGGER_LOCATION);
            
            User user = new User();
            user.ProfileId = profileId;
            for (SObjectField field : registrationAttributes.keySet()) {
                String value = registrationAttributes.get(field);
                user.put(field, value);
            }

            user = handleUnsetRequiredFields(user);
            if(String.isBlank(password)) {
                password = generateRandomPassword();
            }
            
            if(!Test.isRunningTest()) {
                Site.validatePassword(user, password, password);
            }
            //Site.validatePassword(user, password, password);
            if(user.ContactId == null) {
                return Site.createExternalUser(user, accountId, password);
            }
            user.EmailEncodingKey  = 'UTF-8';
            user.LanguageLocaleKey = UserInfo.getLocale();
            user.LocaleSidKey      = UserInfo.getLocale();
            user.TimeZoneSidKey    = UserInfo.getTimezone().getId();

            Logger.addDebugEntry(LoggingLevel.FINEST, Json.serialize(user), LOGGER_LOCATION);

            insert user;

            System.setPassword(user.Id, password);

            Logger.addDebugEntry(LoggingLevel.FINEST, 'end createUser method', LOGGER_LOCATION);
            Logger.saveLog();

            return user.Id;
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, LOGGER_LOCATION);
            Logger.saveLog();
            throw new Auth.DiscoveryCustomErrorException(ex.getMessage());
        }

    }

    // Method to autogenerate a password if one was not passed in
    // By setting a password for a user, we won't send a welcome email to set the password
    private String generateRandomPassword() {
        List<String> characters = new List<String>(UPPERCASE_CHARS);
        characters.addAll(LOWERCASE_CHARS);
        characters.addAll(NUMBER_CHARS);
        characters.addAll(SPECIAL_CHARS);
        String newPassword = '';
        Boolean needsUpper = true, needsLower = true, needsNumber = true, needsSpecial = true;
        while (newPassword.length() < 50) {
            Integer randomInt = generateRandomInt(characters.size());
            String c = characters[randomInt];
            if(needsUpper && c.isAllUpperCase()) {
                needsUpper = false;
            } else if(needsLower && c.isAllLowerCase()) {
                needsLower = false;
            } else if(needsNumber && c.isNumeric()) {
                needsNumber = false;
            } else if(needsSpecial && !c.isAlphanumeric()) {
                needsSpecial = false;
            }
            newPassword += c;
        }
        newPassword = addMissingPasswordRequirements(newPassword, needsLower, needsUpper, needsNumber, needsSpecial);
        return newPassword;
    }

    private String addMissingPasswordRequirements(String password, Boolean addLowerCase, Boolean addUpperCase, Boolean addNumber, Boolean addSpecial) {
        if(addLowerCase) {
            password += LOWERCASE_CHARS[generateRandomInt(LOWERCASE_CHARS.size())];
        }
        if(addUpperCase) {
            password += UPPERCASE_CHARS[generateRandomInt(UPPERCASE_CHARS.size())];
        }
        if(addNumber) {
            password += NUMBER_CHARS[generateRandomInt(NUMBER_CHARS.size())];
        }
        if(addSpecial) {
            password += SPECIAL_CHARS[generateRandomInt(SPECIAL_CHARS.size())];
        }
        return password;
    }

    // Generates a random number from 0 up to, but not including, max.
    private Integer generateRandomInt(Integer max) {
        return Math.mod(Math.abs(Crypto.getRandomInteger()), max);
    }

    // Loops over required fields that were not passed in to set to some default value
    private User handleUnsetRequiredFields(User user) {
        if(String.isBlank(user.Username)) {
            user.Username = generateUsername();
        }
        if(String.isBlank(user.Alias)) {
            user.Alias = generateAlias();
        }
        if(String.isBlank(user.CommunityNickname)) {
            user.CommunityNickname = generateCommunityNickname();
        }
        return user;
    }

    // Default implementation to try to provide uniqueness
    private String generateAlias() {
        String timeString = String.valueOf(CURRENT_TIME);
        return timeString.substring(timeString.length() - 8);
    }

    // Default implementation to try to provide uniqueness
    private String generateUsername() {
        return 'externaluser' + CURRENT_TIME + '@company.com';
    }

    // Default implementation to try to provide uniqueness
    private String generateCommunityNickname() {
        return 'ExternalUser' + CURRENT_TIME;
    }

}