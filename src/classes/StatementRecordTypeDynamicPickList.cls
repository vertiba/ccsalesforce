/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/************************
 * Created date: 2-Dec-2019
 * Purpose: To provide RecordType Name Dynamically to statementCommunityCard Lightning Com
 * 
***************************/
global class StatementRecordTypeDynamicPickList extends VisualEditor.DynamicPickList{
    
    global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('None', '');
        return defaultValue;
    } 
    global override VisualEditor.DynamicPickListRows getValues() {
        
        VisualEditor.DynamicPickListRows  recordTypePickListValues  = new VisualEditor.DynamicPickListRows();
        List<Schema.RecordTypeInfo> RecordTypes = getRecordTypeId('Statement__c');
        for(RecordTypeInfo info: RecordTypes ){
            VisualEditor.DataRow value = new VisualEditor.DataRow(info.Name, info.Name);
            recordTypePickListValues.addRow(value);        
        }
        
        return recordTypePickListValues;
    }
    
    public static List<Schema.RecordTypeInfo> getRecordTypeId(String objectApiName) {
        
        List<Schema.RecordTypeInfo> recordTypeInfo = Schema.getGlobalDescribe().get(objectApiName).getDescribe().getRecordTypeInfosByName().values(); 
        
        if(recordTypeInfo == null) return null;
       
        return recordTypeInfo ;
    }
}