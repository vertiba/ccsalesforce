/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class LowValueAssessorScheduler extends EnhancedSchedulable {

    public override String getJobNamePrefix() {
        return 'Low Value Assessor Scheduler';
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : SchedulableContext
    // @description : Schedulable interface method. Schedules the LowValueAssessorBatch job
    // @return : void
    //-----------------------------
    public void execute(SchedulableContext schedulableContext) {
        // Salesforce has a limit of 5 running batch jobs
        // If there are already 5 jobs running, then don't run this job
        // Any records that need to be processed will be processed the next time the job executes
        if(EnhancedSchedulable.getNumberOfRunningBatchJobs() >= 5) return;

        Logger.addDebugEntry('After Entering LowValueAssessorBatch:', 'LowValueAssessorBatch.execute');
        Id batchProcessId = Database.executebatch(new LowValueAssessorBatch());
        Logger.addDebugEntry('After Scheduling bacth, jobid:'+batchProcessId, 'LowValueAssessorBatch.execute');
        Logger.saveLog();
    }

}