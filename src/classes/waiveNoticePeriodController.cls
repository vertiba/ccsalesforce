/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/

//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to display the records in waiveNoticePeriod component.
//---
public class waiveNoticePeriodController {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : recordId
    // @description : This method, returns the case object .
    // @return : Case
    //-----------------------------
    @AuraEnabled
    public static Case getWaiverValue(String recordId){
        Case cases = [Select Id,IntegrationStatus__c,Type,DeclarationName__c,Status,SubStatus__c from Case where Id =: recordId];
        return cases;
        
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : DeclarationName and recordId
    // @description : This method, updates the DeclarationName,IntegrationStatus and also locks the record.
    // @return : Case
    //-----------------------------
    @AuraEnabled
    public static Case updateWaiverNoticeDetails(String DeclarationName,String recordId){
        Case cases = [Select Id,IntegrationStatus__c,Type,DeclarationName__c,isLocked__c from Case where Id =: recordId];
        If(cases != Null)
        {
            cases.DeclarationName__c = DeclarationName;
            //Integration status for Escape cases will be updated as part of ASR-5669 using Batch job
            if(cases.Type!='Escape'){
                cases.IntegrationStatus__c = 'Ready to Send to TTX';
            }
            cases.WaiveNoticePeriod__c = true;
            //Exception handling
            try{
                update cases;
            }catch(Exception ex){
                Logger.addExceptionEntry(ex, 'WaiveNoticePeriodController');
                Logger.savelog();
                //Throw error to user
                throw new AuraHandledException(System.Label.GenericError);
            }
        }
        return cases;
    }        
}