/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to Send Exemption notices on Jan 1 based on exemption type. 
//-----------------------------
public without sharing class ExemptionNoticeGeneratorBatch implements Database.Batchable<SObject>, BatchableErrorHandler {
    private static final String LOG_LOCATION = 'ExemptionNoticeGeneratorBatch';
    private Map<String, TH1__Document_Setting__c> documentSettingsByFormName;
    private String exemptionNoticeType;

    /**
     * Constructor takes notice type as input, that helps in 
     * deciding which type of notices to generate. 
     */
    public ExemptionNoticeGeneratorBatch(String noticeType){
        exemptionNoticeType = noticeType;
        documentSettingsByFormName = SmartCOMMUtility.getDocumentSettingsBySObjectName('PropertyEvent__c');
    }

    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        DateTime noticeGenerationtime = DateTime.newInstanceGmt(FiscalYearUtility.getCurrentFiscalYear(), 01, 01, 08, 00, 00);
        Logger.addDebugEntry(noticeGenerationtime.format(), LOG_LOCATION);
        if(Label.InitialExemptionNotice.equalsIgnoreCase(exemptionNoticeType)){
            return Database.getQueryLocator([
                select id, LastNoticeToFileGenerated__c, ExemptionForm__c, PropertyContactEmail__c, FileDate__c,
                Type__c, Status__c, Substatus__c, Subtype__c from PropertyEvent__c 
                where RecordType.Name = 'Exemption' and Active__c != 0 and Property__r.Status__c = 'Active' and 
                ExemptionForm__c != '' and 
                (LastNoticeToFileGenerated__c = null or LastNoticeToFileGenerated__c <: noticeGenerationtime)]);
        } else if (Label.ReminderExemptionNotice.equalsIgnoreCase(exemptionNoticeType)) {
            return Database.getQueryLocator([
                select id, LastNoticeToFileGenerated__c, ExemptionForm__c, PropertyContactEmail__c, Type__c, Status__c, Substatus__c, Subtype__c from PropertyEvent__c 
                where RecordType.Name = 'Exemption' and Active__c != 0 and Property__r.Status__c = 'Active' and
                ExemptionForm__c  in ('260','262-AH', '264-AH', '267-A', '268-B','576-E') and 
                (FileDate__c = null or FileDate__c <: noticeGenerationtime.date())]);
        } else {
            //null, 0 and 1 are valid used for active__c. So Checking for 2.
            return Database.getQueryLocator([select id from PropertyEvent__c where Active__c=2]);
        }
    }

    public void execute(Database.BatchableContext batchableContext, List<PropertyEvent__c> scope) {
        try {
            DateTime lastYearsStartDate = DateTime.newInstanceGmt(FiscalYearUtility.getCurrentFiscalYear()-1, 01, 01, 08, 00, 00);
            TH1__Document_Setting__c documentSetting, emailDocumentSetting;
            if(Label.InitialExemptionNotice.equals(exemptionNoticeType)){
                documentSetting = documentSettingsByFormName.get(Label.NoticeToFileExemption);
                emailDocumentSetting = documentSettingsByFormName.get(Label.NoticeToFileExemptionEmail);
            } else if (Label.ReminderExemptionNotice.equals(exemptionNoticeType)){
                documentSetting = documentSettingsByFormName.get(Label.ReminderNoticeToFileExemption);
                emailDocumentSetting = documentSettingsByFormName.get(Label.ReminderNoticeToFileExemptionEmail);
            }
            //Document Settings not created properly.
            if(documentSetting == null || emailDocumentSetting == null) return;
            List<DocumentGenerationRequest__c> requestsToCreate = new List<DocumentGenerationRequest__c>();
            DateTime updatedDateTime = System.now();
            List<PropertyEvent__c> updatedEvents = new List<PropertyEvent__c>();
            DocumentGenerationRequest__c request, emailRequest;
            for(PropertyEvent__c propertyEvent : scope) {
                //If property event != vessel then add document generation request. 
                //if property event is vessel, subtype is 4% affidavit, substatus is approved or denied, last file date is 
                // in the past one year. then also add document generation request.
                if(propertyEvent.Type__c != 'Vessel' || 
                    (propertyEvent.Type__c == 'Vessel' && 
                    propertyEvent.SubType__c == '4% Affidavit' &&
                    propertyEvent.FileDate__c >= lastYearsStartDate &&
                    (propertyEvent.SubStatus__c == 'Approved' || propertyEvent.SubStatus__c == 'Denied' ))){
                    request = SmartCOMMUtility.createDocumentGenerationRequest(propertyEvent.Id, documentSetting.Id);
                    requestsToCreate.add(request);
                    if(String.isNotEmpty(propertyEvent.PropertyContactEmail__c)){
                        emailRequest = SmartCOMMUtility.createDocumentGenerationRequest(propertyEvent.Id, emailDocumentSetting.Id);
                        requestsToCreate.add(emailRequest);
                    }
                    propertyEvent.LastNoticeToFileGenerated__c = updatedDateTime;
                    updatedEvents.add(propertyEvent);
                }
            }
            if(!requestsToCreate.isEmpty()){
                insert requestsToCreate;
            }
            if(!updatedEvents.isEmpty()){
                update updatedEvents;
            }
        }catch(Exception ex) {
            Logger.addExceptionEntry(ex, LOG_LOCATION);
            throw ex;
        }
    }
    
    public void finish(Database.BatchableContext batchableContext) {
        string JobId = batchableContext.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'exemptionNoticeType' => this.exemptionNoticeType});

        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, ExemptionNoticeGeneratorBatch.class.getName(), asyncList[0].ParentJobId, 
                                           true,queryParams, asyncList[0].NumberOfErrors);
     }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
}