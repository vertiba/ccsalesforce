/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class UserUtility {
    
    //-----------------------------
    // @author : Publicis.Sapient    
    // @description :This method is created to give permission to BPP Chief for editing locked records based on certain conditions of the record.ASR-3468    
    // @return : Boolean
    //-----------------------------
    public static Boolean isBPPChief(){ 
        if(UserInfo.getUserRoleId() != null){
            String userRole = [Select id,Name from UserRole where id =: UserInfo.getUserRoleId()].Name;
            return userRole ==  CCSFConstants.ROLE_BPPCHIEF;
        }else{
            return false;
        }
 
    }
     //-----------------------------
    // @author : Publicis.Sapient    
    // @description :This method is created to check the profile of taxpayer and non-taxpayer.ASR-9334    
    // @return : Boolean
    //-----------------------------   
    
  public static Boolean isTaxPayer(){
       Profile profileName = [Select Name from Profile where Id =: userinfo.getProfileid()];
        String taxPayerProfile = profileName.name;
        if(taxPayerProfile == CCSFConstants.TAXPAYER_PROFILE) { 
            return true;
        } else { 
            return false;
        }
    }
     //-----------------------------
    // @author : Publicis.Sapient    
    // @description :This method is created to give permission to Admins for editing locked records.ASR-3319    
    // @return : Boolean
    //-----------------------------
    public static Boolean isNonCCSFEmployee(){
        String profileName = [Select id,Name from Profile where id =: UserInfo.getProfileId()].Name;
        
        return profileName == CCSFCONSTANTS.SYSTEMADMIN_PROFILE || profileName == CCSFCONSTANTS.SAPIENTADMIN_PROFILE;
    }
     //-----------------------------
    // @author : Publicis.Sapient    
    // @description :This method is created to give permission to Approvers for editing locked records.ASR-3404    
    // @return : Boolean
    //-----------------------------
    public static Map<Id,Id> getApprovers(Set<Id> recordIds){
        Map<Id,Id> approverIdByRecordid = new Map<Id,Id>();
        try{
            List<ProcessInstanceWorkitem> processInstanceRecords =[SELECT id,actorId,ProcessInstance.TargetObjectId
                                                                    FROM ProcessInstanceWorkitem
                                                                    Where ProcessInstance.TargetObjectId =: recordIds];
        if(processInstanceRecords.isEmpty()){
                return approverIdByRecordid;//processInstanceRecord.ActorId == userInfo.getUserId();
        }
        for(ProcessInstanceWorkitem processRecords : processInstanceRecords){
                    approverIdByRecordid.put(processRecords.ProcessInstance.TargetObjectId,processRecords.actorId);
        }
        }catch(Exception e){
            system.debug(e.getStackTraceString()+e.getMessage());
            logger.addDebugEntry(LoggingLevel.Error,e.getMessage()+' @lineNumber  '+e.getLineNumber()  ,'UserUtility-getApprovers');
            return null;
        }
        return approverIdByRecordid; 
    }

}