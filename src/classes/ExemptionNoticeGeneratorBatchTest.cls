/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test ExemptionNoticeGeneratorBatch
//-----------------------------
@isTest
private class ExemptionNoticeGeneratorBatchTest {

    @testSetup
    private static void dataSetup(){
        TH1__Schema_Set__c schemaSet = new TH1__Schema_Set__c();
        insert schemaSet;

        TH1__Schema_Object__c schemaObject = new TH1__Schema_Object__c();
        schemaObject.TH1__Is_Primary_Object__c = true;
        schemaObject.TH1__Object_Label__c = 'PropertyEvent__c';
        schemaObject.TH1__Schema_Set__c = schemaSet.Id;
        schemaObject.Name = 'PropertyEvent__c';
        insert schemaObject;

        schemaSet.TH1__Primary_Object__c = schemaObject.Id;
        update schemaSet;

        List<String> documentSettingNames = new List<String>{Label.ReminderNoticeToFileExemption,
            Label.NoticeToFileExemption, Label.ReminderNoticeToFileExemptionEmail, Label.NoticeToFileExemptionEmail};
        List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        TH1__Document_Setting__c documentSettting;
        for(integer i=0;i<4;i++) {
            documentSettting = new TH1__Document_Setting__c();
            documentSettting.TH1__Document_Data_Model__c = schemaSet.Id;
            documentSettting.Name = documentSettingNames.get(i);
            documentSettting.TH1__Storage_File_Name__c = documentSettingNames.get(i);
            documentSettting.TH1__Is_disabled__c=false;
            documentSettings.add(documentSettting);
        }
        insert documentSettings;
        
        Account account = new Account();
        account.Name='test account';
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert account;
    }

    @isTest
    private static void generateDocumentsForPropertyEvent(){
        Id bppRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Business Personal Property').getRecordTypeId();
        Account account = [select id from account limit 1];
        Property__C property = new Property__C();
        property.Name = 'test property'; 
        property.MailingCountry__c = 'US';
        property.Status__c = 'Active';
        property.Account__c = account.Id;
        property.RecordTypeId = bppRecordTypeId;
        insert property;
        
        Id exemptionRecordTypeId = Schema.SObjectType.PropertyEvent__c.getRecordTypeInfosByName().get('Exemption').getRecordTypeId();
        List<PropertyEvent__c> propertyEvents = new List<PropertyEvent__c>();
        PropertyEvent__c propertyEvent1 = new PropertyEvent__c();
        propertyEvent1.RecordTypeId = exemptionRecordTypeId;
        propertyEvent1.Property__c = property.Id;
        PropertyEvent1.Type__c='Institutional';
        propertyEvent1.ExemptionForm__c='261-G';
        propertyEvent1.ClaimantStatus__c='First time filer';
        propertyEvents.add(propertyEvent1);
        PropertyEvent__c propertyEvent2 = new PropertyEvent__c();
        propertyEvent2.RecordTypeId = exemptionRecordTypeId;
        propertyEvent2.Property__c = property.Id;
        PropertyEvent2.Type__c='Institutional';
        propertyEvent2.ExemptionForm__c='268-B';
        propertyEvent2.PropertyContactEmail__c='testforemail@test.com';
        propertyEvents.add(propertyEvent2);
        insert propertyEvents;
        
        //Create property Events
        Test.startTest();
        Database.executeBatch(new ExemptionNoticeGeneratorBatch(Label.InitialExemptionNotice));
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
        Test.stopTest();
        documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(3, documentGenerationRequests.size());
        propertyEvents = [select LastNoticeToFileGenerated__c from PropertyEvent__c];
        System.assertNotEquals(null, propertyEvents.get(0).LastNoticeToFileGenerated__c);
    }
    @isTest
    private static void doNotGenerateDocumentsForPropertyEvent(){
        Id bppRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Business Personal Property').getRecordTypeId();
        Account account = [select id from account limit 1];
        List<Property__c> propertyList = new List<Property__c>();
        Property__c property1 = new Property__C();
        property1.Name = 'test property'; 
        property1.MailingCountry__c = 'US';
        property1.Status__c = 'Inactive';
        property1.Account__c = account.Id;
        property1.RecordTypeId = bppRecordTypeId;
        propertyList.add(property1);
        
        Property__C property2 = new Property__C();
        property2.Name = 'test property 2'; 
        property2.MailingCountry__c = 'US';
        property2.Status__c = 'Active';
        property2.Account__c = account.Id;
        property2.RecordTypeId = bppRecordTypeId;
        propertyList.add(property2);
        insert propertyList;
        
        Id exemptionRecordTypeId = Schema.SObjectType.PropertyEvent__c.getRecordTypeInfosByName().get('Exemption').getRecordTypeId();
        Id calamityRecordTypeId = Schema.SObjectType.PropertyEvent__c.getRecordTypeInfosByName().get('Calamity').getRecordTypeId();
        List<PropertyEvent__c> propertyEvents = new List<PropertyEvent__c>();
        PropertyEvent__c propertyEvent1 = new PropertyEvent__c();
        propertyEvent1.RecordTypeId = exemptionRecordTypeId;
        propertyEvent1.Property__c = propertyList.get(0).Id;
        PropertyEvent1.Type__c='Institutional';
        propertyEvent1.ExemptionForm__c='261-G';
        propertyEvent1.ClaimantStatus__c='First time filer';
        propertyEvents.add(propertyEvent1);
        
        PropertyEvent__c propertyEvent2 = new PropertyEvent__c();
        propertyEvent2.RecordTypeId = calamityRecordTypeId;
        propertyEvent2.Property__c = propertyList.get(1).Id;
        PropertyEvent2.Type__c='Calamity';
        propertyEvent2.ExemptionForm__c='264-AH';
        propertyEvents.add(propertyEvent2);
        insert propertyEvents;
        
        Test.startTest();
        Database.executeBatch(new ExemptionNoticeGeneratorBatch(Label.InitialExemptionNotice));
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
        Test.stopTest();
        //For one property is inactive, another is of different record type. 
        documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
    }
    
    @isTest
    private static void doNotRunBatchForWrongValues(){
        Id bppRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Business Personal Property').getRecordTypeId();
        Account account = [select id from account limit 1];
        Property__C property = new Property__C();
        property.Name = 'test property'; 
        property.MailingCountry__c = 'US';
        property.Status__c = 'Active';
        property.Account__c = account.Id;
        property.RecordTypeId = bppRecordTypeId;
        insert property;
        PropertyEvent__c propertyEvent = new PropertyEvent__c();
        propertyEvent.RecordTypeId = Schema.SObjectType.PropertyEvent__c.getRecordTypeInfosByName().get('Exemption').getRecordTypeId();
        propertyEvent.Property__c = property.Id;
        PropertyEvent.Type__c='Institutional';
        propertyEvent.ClaimantStatus__c='First time filer';
        propertyEvent.ExemptionForm__c='264-AH';
        insert propertyEvent;
        Test.startTest();
        Database.executeBatch(new ExemptionNoticeGeneratorBatch('Wrong Value'));
        Test.stopTest();
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
    }
    
    @isTest
    private static void generateDocumentsForRemainderEvent(){
        Id bppRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Business Personal Property').getRecordTypeId();
        Account account = [select id from account limit 1];
        Property__C property = new Property__C();
        property.Name = 'test property'; 
        property.MailingCountry__c = 'US';
        property.Status__c = 'Active';
        property.Account__c = account.Id;
        property.RecordTypeId = bppRecordTypeId;
        insert property;
        
        Id exemptionRecordTypeId = Schema.SObjectType.PropertyEvent__c.getRecordTypeInfosByName().get('Exemption').getRecordTypeId();
        List<PropertyEvent__c> propertyEvents = new List<PropertyEvent__c>();
        PropertyEvent__c propertyEvent1 = new PropertyEvent__c();
        propertyEvent1.RecordTypeId = exemptionRecordTypeId;
        propertyEvent1.Property__c = property.Id;
        PropertyEvent1.Type__c='Institutional';
        propertyEvent1.ExemptionForm__c='262-AH';
        propertyEvent1.ClaimantStatus__c='First time filer';
        propertyEvents.add(propertyEvent1);
        PropertyEvent__c propertyEvent2 = new PropertyEvent__c();
        propertyEvent2.RecordTypeId = exemptionRecordTypeId;
        propertyEvent2.Property__c = property.Id;
        PropertyEvent2.Type__c='Institutional';
        propertyEvent2.ExemptionForm__c='268-B';
        propertyEvent2.PropertyContactEmail__c='testforemail@test.com';
        propertyEvents.add(propertyEvent2);
        insert propertyEvents;
        
        //Create property Events
        Test.startTest();
        Database.executeBatch(new ExemptionNoticeGeneratorBatch(Label.ReminderExemptionNotice));
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
        Test.stopTest();
        documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(3, documentGenerationRequests.size());
        propertyEvents = [select LastNoticeToFileGenerated__c from PropertyEvent__c];
        System.assertNotEquals(null, propertyEvents.get(0).LastNoticeToFileGenerated__c);
    }
   @isTest
    private static void doNotRunBatchForWrongVesselValues(){
        Id vesselRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Vessel').getRecordTypeId();
        Account account = [select id from account limit 1];
        Property__C property = new Property__C();
        property.Name = 'Test Vessel'; 
        property.Status__c = 'Active';
        property.Account__c = account.Id;
        property.RecordTypeId = vesselRecordTypeId;
        insert property;
        
        Id exemptionRecordType = Schema.SObjectType.PropertyEvent__c.getRecordTypeInfosByName().get('Exemption').getRecordTypeId();
        List<PropertyEvent__c> propertyEvents = new List<PropertyEvent__c>();
        PropertyEvent__c propertyEvent1 = new PropertyEvent__c();
        propertyEvent1.RecordTypeId = exemptionRecordType;
        propertyEvent1.Property__c = property.Id;
        PropertyEvent1.Type__c='Vessel';
        PropertyEvent1.SubType__c='50 Ton Barge';//Wrong value
        propertyEvent1.Status__c='New';
        propertyEvent1.SubStatus__c='Approved';
        propertyEvent1.ExemptionForm__c='576-E';
        propertyEvent1.FileDate__c = System.today().addDays(-360);
        propertyEvents.add(propertyEvent1);
        PropertyEvent__c propertyEvent2 = new PropertyEvent__c();
        propertyEvent2.RecordTypeId = exemptionRecordType;
        propertyEvent2.Property__c = property.Id;
        PropertyEvent2.Type__c='Vessel';
        PropertyEvent2.SubType__c='4% Affidavit';
        propertyEvent2.Status__c='New';
        propertyEvent2.SubStatus__c='Approved';
        propertyEvent2.ExemptionForm__c='576-E';
        propertyEvent2.FileDate__c = System.today().addyears(-2);//Wrong value
        propertyEvents.add(propertyEvent2);
        PropertyEvent__c propertyEvent3 = new PropertyEvent__c();
        propertyEvent3.RecordTypeId = exemptionRecordType;
        propertyEvent3.Property__c = property.Id;
        PropertyEvent3.Type__c='Vessel';
        PropertyEvent3.SubType__c='4% Affidavit';
        propertyEvent3.Status__c='New';
        propertyEvent3.SubStatus__c='Other';//Wrong value
        propertyEvent3.ExemptionForm__c='576-E';
        propertyEvent3.FileDate__c = System.today().addDays(-20);
        propertyEvents.add(propertyEvent3);
        insert propertyEvents;
        Test.startTest();
        Database.executeBatch(new ExemptionNoticeGeneratorBatch(Label.InitialExemptionNotice));
        Test.stopTest();
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
    }
    
    @isTest
    private static void generateDocumentsForVesselPropertyEvent(){
        Id vesselRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Vessel').getRecordTypeId();
        Account account = [select id from account limit 1];
        Property__C property = new Property__C();
        property.Name = 'Test Vessel'; 
        property.Status__c = 'Active';
        property.Account__c = account.Id;
        property.RecordTypeId = vesselRecordTypeId;
        insert property;
        
        Id exemptionRecordType = Schema.SObjectType.PropertyEvent__c.getRecordTypeInfosByName().get('Exemption').getRecordTypeId();
        List<PropertyEvent__c> propertyEvents = new List<PropertyEvent__c>();
        PropertyEvent__c propertyEvent1 = new PropertyEvent__c();
        propertyEvent1.RecordTypeId = exemptionRecordType;
        propertyEvent1.Property__c = property.Id;
        PropertyEvent1.Type__c='Vessel';
        PropertyEvent1.SubType__c='4% Affidavit';
        propertyEvent1.Status__c='New';
        propertyEvent1.PropertyContactEmail__c='testforemail@test.com';
        propertyEvent1.SubStatus__c='Approved';
        propertyEvent1.ExemptionForm__c='576-E';
        propertyEvent1.FileDate__c = System.today().addDays(-360);//With in last one year. So document should be generated
        propertyEvents.add(propertyEvent1);
        PropertyEvent__c propertyEvent2 = new PropertyEvent__c();
        propertyEvent2.RecordTypeId = exemptionRecordType;
        propertyEvent2.Property__c = property.Id;
        PropertyEvent2.Type__c='Vessel';
        PropertyEvent2.SubType__c='4% Affidavit';
        propertyEvent2.Status__c='New';
        propertyEvent2.SubStatus__c='Approved';
        propertyEvent2.ExemptionForm__c='576-E';
        propertyEvent2.FileDate__c = System.today().addDays(-20);
        propertyEvent2.PropertyContactEmail__c='testforemail@test.com';
        propertyEvents.add(propertyEvent2);
        insert propertyEvents;
        Test.startTest();
        Database.executeBatch(new ExemptionNoticeGeneratorBatch(Label.InitialExemptionNotice));
        Test.stopTest();
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(2, documentGenerationRequests.size());
    }
}