/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
public class StatementRecordTypeDynamicPickListTest {
    
     @isTest
    private static void getDefaultValue() {
        VisualEditor.DataRow defaultValue = new StatementRecordTypeDynamicPickList().getDefaultValue();
        System.assertEquals('None', defaultValue.getLabel());
        System.assertEquals('', defaultValue.getValue());
    }
    
     @isTest
    private static void getValues() {
        
        Test.startTest();

        VisualEditor.DynamicPickListRows returnedPicklistValues = new StatementRecordTypeDynamicPickList().getValues();
        System.assertEquals(4, returnedPicklistValues.size());

        Test.stopTest();
    }

}