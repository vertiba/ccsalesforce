/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public abstract class EnhancedSchedulable implements System.Schedulable {

    private static final LoggingLevel LOGGING_LEVEL = LoggingLevel.INFO;
    private static final String LOGGER_LOCATION     = 'EnhancedSchedulable';
    private static final List<String> JOB_STATUSES  = new List<String>{'Processing','Preparing','Holding','Queued'};

    public static Integer getNumberOfRunningBatchJobs() {
        return [SELECT COUNT() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN :JOB_STATUSES];
    }

    public static Integer getNumberOfBatchJobsForClass(String className) {
        Integer numberOfRunningJobs = [
            SELECT COUNT()
            FROM AsyncApexJob
            WHERE ApexClass.Name = :className
            AND JobType = 'BatchApex'
            AND Status IN :JOB_STATUSES
        ];
        return numberOfRunningJobs;
    }

    public abstract String getJobNamePrefix();

    public void abortJobsWithJobNamePrefix() {
        List<CronTrigger> scheduledJobs = [
            SELECT Id, OwnerId, CronExpression, CronJobDetail.Name
            FROM CronTrigger
            WHERE CronJobDetail.JobType = '7' // 7 == Scheduled Apex
            AND CronJobDetail.Name LIKE :this.getJobNamePrefix() + '%'
        ];
        Logger.addDebugEntry(LoggingLevel.INFO, 'Existing scheduled Apex jobs: ' + Json.serialize(scheduledJobs), LOGGER_LOCATION);
        for(CronTrigger scheduledJob : scheduledJobs) {
            System.abortJob(scheduledJob.Id);
        }
        Logger.addDebugEntry(LoggingLevel.INFO, scheduledJobs.size() + ' scheduled Apex jobs aborted', LOGGER_LOCATION);
        Logger.saveLog();
    }

    public void scheduleEveryXMinutesInHour(Integer x) {
        for(Integer i = 0; i < 60; i += x) {
            scheduleHourly(i);
        }
    }

    public void scheduleHourly(Integer startingMinuteInHour) {
        String minuteString = String.valueOf(startingMinuteInHour).leftPad(2, '0');
        String jobName = this.getJobNamePrefix() + ': Every Hour at ' + minuteString;

        System.schedule(jobName, '0 ' + startingMinuteInHour + ' * * * ?', this);
    }

    public void scheduleDaily(Integer startingHourInDay) {
        String jobName = this.getJobNamePrefix() + ': Every Day at ' + startingHourInDay;

        System.schedule(jobName, '0 0 ' + startingHourInDay + ' * * ?', this);
    }

}