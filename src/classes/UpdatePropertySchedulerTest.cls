// @author : Publicis.Sapient
// 		Class to test UpdatePropertyScheduler
//-----------------------------
@isTest
public class UpdatePropertySchedulerTest {
     //verify that a job is scheduled successfully in a future date. 
    @isTest
    private static void jobShouldBeScheduledToRunInFuture() {
        Test.startTest();
        String jobId = System.schedule('UpdatePropertySchedulerTest',
                                       '0 0 0 15 3 ? 2032', 
                                       new UpdatePropertyScheduler()); 
        UpdatePropertyScheduler updatesch = new UpdatePropertyScheduler();
        updatesch.getJobNamePrefix();
        Test.stopTest();
        
        system.assertEquals(1, [SELECT count() FROM CronTrigger where id = :jobId],
                            'A job should be scheduled');
    }
	
}