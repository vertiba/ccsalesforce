/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//                            Class to test LowValueAssessorBatch
//-----------------------------
@isTest
public class LowValueAssessorBatchTest {
    
    @testSetup
    private static void dataSetup(){
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueOf(system.today().Year());
        rollYear.Year__c = String.valueOf(system.today().Year());
        rollYear.Status__c='Roll Open';
        rollYear.IsNonFilerBulkGeneratorProcessed__c = true;
        rollYear.IsVesselNonFilerGeneratorProcessed__c = true;
        insert rollYear;
        
        Account account = new Account();
        account.Name='test account';
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert account;
        
        Penalty__c penalty = new Penalty__c();
        penalty.PenaltyMaxAmount__c = 500;
        penalty.Percent__c = 10;
        penalty.RTCode__c = '463';
        insert penalty;
        
        Property__c property = new Property__C();
        property.Name = 'test property'; 
        property.Account__c = account.Id;
        insert property;
        
        FactorSource__c factorSource = new FactorSource__c();
        factorSource.Name = 'Commercial & Industrial (Untrended)';        
        insert factorSource;
        
        BusinessAssetMapping__c businessAssetMapping = new BusinessAssetMapping__c();
        businessAssetMapping.AssetClassification__c='Machinery & Equipment';
        businessAssetMapping.AssetSubclassification__c='Bakery (large)';
        businessAssetMapping.FactorSource__c = factorSource.id;
        businessAssetMapping.Subcomponent__c = '';
        businessAssetMapping.FixturesAllocationPercent__c = 75;
        businessAssetMapping.YearsOfUsefulLife__c = 15;
        businessAssetMapping.CompositeKey__c='Machinery & Equipment-Bakery (large)';
        businessAssetMapping.FactorType__c='Depreciation Factor';
        insert businessAssetMapping;
        
        Factor__c factor = new Factor__c();
        factor.FactorPercent__c=67;
        factor.AcquisitionYear__c='2018';
        factor.AssessmentYear__c='2020';
        factor.FactorSource__c=factorSource.Id;
        factor.RecordTypeId=Schema.SObjectType.Factor__c.getRecordTypeInfosByDeveloperName().get('DepreciationFactor').getRecordTypeId();
        insert factor;
    }

    @isTest
    private static void donotSetLowValueIfAccountHasSecuredProperty(){
        Property__c property = [select id, RollCode__c from Property__c];
        property.RollCode__c = 'Secured';
        update property;

        Test.startTest();
        Database.executeBatch(new LowValueAssessorBatch());
        Test.stopTest();
        Account account = [select NoticeType__c, LowValueAssessedYear__c from Account];
        System.assertNotEquals('Low Value', account.NoticeType__c);
        System.assertEquals(null, account.LowValueAssessedYear__c);
    }
  
    @isTest
    private static void donotSetLowValueIfPropertyValueIsHigh(){
        Property__c property = [select id, account__c from Property__c];
        Factor__c factor = [select id from Factor__c];
        Case cas = new Case();
        cas.AccountId = property.account__c;
        cas.Property__C = property.id;
        cas.MailingCountry__c = 'US';
        cas.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        cas.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        cas.RollYear__c=String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        cas.Type='Regular';
        cas.Status= 'Closed';
        cas.SubStatus__c = 'Completed';
        cas.IntegrationStatus__c = 'Ready to send to TTX';
        cas.TotalPersonalPropertyValueOverride__c = 100000;
        insert cas;
        
        SalesTax__c salesTax= new SalesTax__c();
        salesTax.Sales_Tax_Year__c = '2019';
        insert salesTax; 
        
        AssessmentLineItem__c assessmentLineItem = new AssessmentLineItem__c();
        assessmentLineItem.Category__c ='Equipment';
        assessmentLineItem.Subcategory__c ='Machinery & Equipment';
        assessmentLineItem.AcquisitionYear__c = '2018';
        assessmentLineItem.AssessmentYear__c = cas.RollYear__c;
        assessmentLineItem.Case__c = cas.Id;
        assessmentLineItem.SalesTax__c = salesTax.Id;
        assessmentLineItem.Description__c = 'Assessment Line Item';
        assessmentLineItem.DepreciationFactor__c = factor.Id;
        assessmentLineItem.AssetSubclassification__c = 'Bakery (large)';
        //ideally this has to be auto populated, from Factor but hardcoding now;
        assessmentLineItem.DepreciationFactorPercent__c= 67;
        assessmentLineItem.FixturesAllocationPercent__c =75;
        //insert assessmentLineItem;
        assessmentLineItem.Cost__c = 50000;
        insert assessmentLineItem;
        Test.startTest();
        Database.executeBatch(new LowValueAssessorBatch());
        Test.stopTest();
        Account account = [select NoticeType__c, LowValueAssessedYear__c from Account];       
        cas = [select id, NoticeType__c from case];
        System.assertNotEquals('Low Value', cas.NoticeType__c);
    }
    
    @isTest
    private static void donotsetLowValueIfCaseStatusIsNotReadyToSendToTTX(){
        Property__c property = [select id, account__c from Property__c];
        Factor__c factor = [select id from Factor__c];
        Case cas = new Case();
        cas.AccountId = property.account__c;
        cas.Property__C = property.id;
        cas.MailingCountry__c = 'US';
        cas.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        cas.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        cas.RollYear__c=String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        cas.Type='Regular';
        insert cas;
        
        AssessmentLineItem__c assessmentLineItem = new AssessmentLineItem__c();
        assessmentLineItem.Category__c ='Supplies';
        assessmentLineItem.Subcategory__c ='Supplies';
        assessmentLineItem.Cost__c = 3000.00;
        assessmentLineItem.AcquisitionYear__c = '2017';
        assessmentLineItem.Case__c = cas.Id;
        assessmentLineItem.Description__c = 'Assessment Line Item';
        assessmentLineItem.DepreciationFactor__c = factor.Id;
        //ideally this has to be auto populated but hardcodino now;
        assessmentLineItem.DepreciationFactorPercent__c=98;
        insert assessmentLineItem;

        Test.startTest();
        Database.executeBatch(new LowValueAssessorBatch());
        Test.stopTest();
        Account account = [select NoticeType__c, LowValueAssessedYear__c from Account];
        System.assertEquals(null, account.LowValueAssessedYear__c);
        cas = [select id, NoticeType__c from case];
        System.assertNotEquals('Low Value', cas.NoticeType__c);
    }
    
    @isTest
    private static void setLowValueIfCaseStatusIsSetAndPropertyHasLowValue(){
        Property__c property = [select id, account__c from Property__c];
        Factor__c factor = [select id from Factor__c];
        Case cas = new Case();
        cas.AccountId = property.account__c;
        cas.Property__C = property.id;
        cas.MailingCountry__c = 'US';
        cas.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        cas.IntegrationStatus__c = 'Ready to send to TTX';
        cas.RollYear__c=String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        cas.Type='Regular';
        cas.Status= 'Closed';
        cas.SubStatus__c = 'Completed';
        cas.TotalPersonalPropertyValueOverride__c = 1000;
        cas.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        insert cas;
        
        AssessmentLineItem__c assessmentLineItem = new AssessmentLineItem__c();
        assessmentLineItem.Category__c ='Supplies';
        assessmentLineItem.Subcategory__c ='Supplies';
        assessmentLineItem.Cost__c = 3000.00;
        assessmentLineItem.AcquisitionYear__c = '2017';
        assessmentLineItem.Case__c = cas.Id;
        assessmentLineItem.Description__c = 'Assessment Line Item';
        assessmentLineItem.DepreciationFactor__c = factor.Id;
        //ideally this has to be auto populated but hardcodino now;
        assessmentLineItem.DepreciationFactorPercent__c=98;
        insert assessmentLineItem;
        
        Test.startTest();
        Database.executeBatch(new LowValueAssessorBatch());
        Test.stopTest();
        Account account = [select NoticeType__c, LowValueAssessedYear__c from Account];
        System.assertNotEquals(null, account.LowValueAssessedYear__c);
        cas = [select id, SubType__c from case];
        System.assertEquals('Low Value', cas.SubType__c);
    }
    
    @isTest
    static void errorFrameworkTest(){
         Account bppAccount = [Select Id, TotalValueofProperties__c, NumberofSecuredProperties__c,LowValueAssessedYear__c from Account];
        	 Id testJobId = '707S000000nKE4fIAG';
        	BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
            testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testJob.JobCreatedDate__c = System.today();
            testJob.JobId__c = testJobId;
            database.insert(testJob);        
           
        	BatchApexError__c testError = new BatchApexError__c();
            testError.AsyncApexJobId__c = testJobId;
            testError.BatchApexErrorLog__c = testJob.Id;
            testError.DoesExceedJobScopeMaxLength__c = false;
            testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
            testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testError.JobScope__c = bppAccount.Id;
            testError.Message__c = 'Test exception';
            testError.RequestId__c = null;
            testError.StackTrace__c = '';
            database.insert(testError);
        
        BatchableError batchError = BatchableError.newInstance(testError);
        LowValueAssessorBatch batchRetry= new LowValueAssessorBatch();
        batchRetry.handleErrors(batchError);
    }
}