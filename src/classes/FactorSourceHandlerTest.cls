/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class FactorSourceHandlerTest {

    @testSetup
    static void setupData() {
        FactorSource__c factorSource = new FactorSource__c();
        new TestDataFactory(factorSource).populateRequiredFields();
        insert factorSource;
    }

    @isTest
    static void it_should_set_uuid_on_new_record() {
        FactorSource__c factorSource = new FactorSource__c();
        new TestDataFactory(factorSource).populateRequiredFields();
        factorSource.Uuid__c = null;

        Test.startTest();
        insert factorSource;
        Test.stopTest();

        factorSource = [SELECT Id, Uuid__c FROM FactorSource__c WHERE Id = :factorSource.Id];
        System.assertNotEquals(null, factorSource.Uuid__c);
        System.assert(Uuid.isValid(factorSource.Uuid__c));
    }

    @isTest
    static void it_should_set_uuid_on_updated_record_without_uuid() {
        FactorSource__c factorSource = [SELECT Id, Uuid__c FROM FactorSource__c];
        factorSource.Uuid__c = 'invalid-uuid';
        System.assertEquals(false, Uuid.isValid(factorSource.Uuid__c));

        Test.startTest();
        update factorSource;
        Test.stopTest();

        factorSource = [SELECT Id, Uuid__c FROM FactorSource__c WHERE Id = :factorSource.Id];
        System.assertNotEquals(null, factorSource.Uuid__c);
        System.assert(Uuid.isValid(factorSource.Uuid__c));
    }

    @isTest
    static void it_should_not_set_uuid_on_updated_record_with_uuid() {
        FactorSource__c factorSource = [SELECT Id, Uuid__c FROM FactorSource__c];
        String existingUuid = factorSource.Uuid__c;

        Test.startTest();
        update factorSource;
        Test.stopTest();

        factorSource = [SELECT Id, Uuid__c FROM FactorSource__c WHERE Id = :factorSource.Id];
        System.assertNotEquals(null, factorSource.Uuid__c);
        System.assertEquals(existingUuid, factorSource.Uuid__c);
    }

}