/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test PrintMailerBatch
//-----------------------------
@isTest
private class PrintMailerBatchTest {
    //Create Accounts, properties, cases, documentSettings, print attributes, Attachments
    //Once exectued. verify for the contentVersions records are updated.
    @isTest
    private static void contentVersionsShouldBeSentInBulkToJavaLayer() {
        
        List<Account> accounts = new List<Account>();
         Account accountRecord = new Account();
        accountRecord.Name='New Test Account';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord.EntityId__c = '654321';
        accountRecord.BusinessStatus__c='active';
        accountRecord.MailingCountry__C='US';
        accountRecord.MailingStreetName__c ='355';
        accountRecord.MailingCity__c ='Oakland';
        accountRecord.NoticeType__c='Notice to File';
        accountRecord.RequiredToFile__c='Yes';
        accountRecord.NoticeLastGenerated__c = system.now();
        accounts.add(accountRecord);
        insert accounts;

        List<Property__c> properties = new List<Property__c>();
        Property__c propertyRecord = new Property__c();
        propertyRecord.Name = 'new test property';
        propertyRecord.PropertyId__c = '153536';
        propertyRecord.MailingCountry__c = 'US';
        propertyRecord.Account__c = accountRecord.Id;
        properties.add(propertyRecord);
        insert properties;

        List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        for(integer i=0;i<5;i++) {
            documentSettings.add(new TH1__Document_Setting__c(Name = 'Test '+i, TH1__Storage_File_Name__c ='Test'+i));
        }
        insert documentSettings;

        List<PrintAttribute__c> printAttributes = new List<PrintAttribute__c>();
        PrintAttribute__c printAttribute;
        for(integer i=0;i<5;i++) {
            printAttribute = new PrintAttribute__c();
            //Many thing are there to set.
            printAttribute.AccountNumber__c = '20200';
            printAttribute.Address__c = '1155 Market Street, San Francisco, CA';
            printAttribute.Department__c = 'ASR';
            printAttribute.Description__c = 'Testiwng Desc '+i;
            printAttribute.DocumentSetting__c = documentSettings.get(i).id;
            printAttribute.EmailAddress__c = 'test@test.com';
            printAttribute.EnvelopePicklist__c = '#10, window';
            printAttribute.FoldPicklist__c = 'letterfold';
            printAttribute.Inbound_Outbound__c = 'Outbound';
            printAttribute.InkPicklist__c = 'black';
            printAttribute.PaperPicklist__c = '20# text';
            printAttribute.PaperColorPicklist__c = 'White';
            printAttribute.PeopleSoftChartfields__c = '10000|229014|10000|10001634|1';
            printAttribute.PhoneNo__c = '1233456789';
            printAttribute.ProofRequired__c = 'No';
            //TODO populate email field through new custom setting
            //printAttribute.ReproNotificationEmailAddress__c = 'test@test.com';
            printAttribute.Sides__c = 'Two';
            printAttribute.SizePicklist__c = '8.5x14';
            printAttribute.SpecialPrintInstruction__c='No';
            printAttribute.InternationalMail__c='No';
            printAttribute.ReturnEnvelopeType__c='BPP';
            printAttribute.MailingIncludeReturnEnvelope__c='No';
            printAttributes.add(printAttribute);
        }
        insert printAttributes;
        
        List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty1 = new Penalty__c();
        penalty1.PenaltyMaxAmount__c = 500;
        penalty1.Percent__c = 10;
        penalty1.RTCode__c = '463';
        penalties.add(penalty1);

        Penalty__c penalty2 = new Penalty__c();
        penalty2.PenaltyMaxAmount__c = 500;
        penalty2.Percent__c = 25;
        penalty2.RTCode__c = '214.13';
        penalties.add(penalty2);

        Penalty__c penalty3 = new Penalty__c();
        penalty3.PenaltyMaxAmount__c = 500;
        penalty3.Percent__c = 25;
        penalty3.RTCode__c = '504';
        penalties.add(penalty3);
		insert penalties;
        
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Status__c='Roll Open';
        insert rollYear;

        RollYear__c prevrollYear = new RollYear__c();
        prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Status__c='Roll Closed';
        insert prevrollYear;

        RollYear__c nextrollYear = new RollYear__c();
        nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Status__c='Pending';
        insert nextrollYear;
        
        List<Case> cases = new List<Case>();
        Id bppAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Case caseRecord1 = new Case();
        caseRecord1.AccountName__c='new test account';
        caseRecord1.type = 'Regular';
        caseRecord1.AccountId = propertyRecord.account__c;
        caseRecord1.ApplyFraudPenalty__c = true;
        caseRecord1.RecordTypeId = bppAssessmentRecordTypeId;
        caseRecord1.Property__c = propertyRecord.Id;
        caseRecord1.Status = 'In Progress';
        caseRecord1.AssessmentYear__c ='2017';
        caseRecord1.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        caseRecord1.MailingCountry__c = 'US';
        cases.add(caseRecord1);
        insert cases;

        List<ContentVersion> contentVersions = new List<ContentVersion>();
        ContentVersion version;
        for(Integer i=0;i<17;i++){
            version = new ContentVersion();
            version.ContentLocation = 'S';
            version.VersionData = EncodingUtil.base64Decode('Test '+i);
            version.Title = 'Test'+i+'.pdf';
            version.PathOnClient = version.Title;
            version.DocumentSetting__c = documentSettings.get(Integer.valueof((Math.random() * documentSettings.size()-1))).id;
            contentVersions.add(version);
        }
        insert contentVersions;
        contentVersions = [select id, contentDocumentId from contentversion];
        List<ContentDocumentLink> documentLinks = new List<ContentDocumentLink>();
        ContentDocumentLink documentLink;
        for(Integer i=0;i<12;i++){
            documentLink= new ContentDocumentLink();
            documentLink.ContentDocumentId = contentVersions.get(i).contentDocumentId;
            documentLink.LinkedEntityId = caseRecord1.Id;
            documentLink.ShareType='V';
            documentLinks.add(documentLink);
        }
        for(Integer i=0;i<3;i++){
            documentLink= new ContentDocumentLink();
            documentLink.ContentDocumentId = contentVersions.get(12+i).contentDocumentId;
            documentLink.LinkedEntityId = propertyRecord.Id;
            documentLink.ShareType='V';
            documentLinks.add(documentLink);
        }
		for(Integer i=0;i<2;i++){
            documentLink= new ContentDocumentLink();
            documentLink.ContentDocumentId = contentVersions.get(15+i).contentDocumentId;
            documentLink.LinkedEntityId = accountRecord.Id;
            documentLink.ShareType='V';
            documentLinks.add(documentLink);
        }
		insert documentLinks;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class,new MockHttpResponseGenerator('Case',true));
        Database.executeBatch(new PrintMailerBatch());
        // Verify the scheduled job has not run yet.
        contentVersions = [SELECT Id, Status__c FROM ContentVersion WHERE SentToIntegrationLayer__c = null];
        System.assertEquals(17, contentVersions.size(), 'Content Versions are not updated');
        System.assertEquals('Created',contentVersions.get(0).Status__c);
        // Stopping the test will run the job synchronously
        Test.stopTest();

        // Now that the scheduled job has executed,
        // check that our tasks were created
        contentVersions = [SELECT Id, Status__c FROM ContentVersion WHERE SentToIntegrationLayer__c != null];
        System.assertEquals(17, contentVersions.size(), 'Content Versions are updated');
        System.assertEquals('Sent To Integration Layer',contentVersions.get(0).Status__c);
    }

    //test that when content version is for different type or
    //document setting is not set, then dont send to Java layer.
    @isTest
    private static void contentVersionsShouldNotBeSentToJavaLayer() {
         Account account = new Account();
        account.Name='New Test Account';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        account.EntityId__c = '654321';
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.MailingStreetName__c ='355';
        account.MailingCity__c ='Oakland';
        account.NoticeType__c='Notice to File';
        account.RequiredToFile__c='Yes';
        account.NoticeLastGenerated__c = system.now();
        insert account;

        TH1__Document_Setting__c documentSetting = new TH1__Document_Setting__c(Name = 'Test Document Setting');
        insert documentSetting;

        PrintAttribute__c printAttribute = new PrintAttribute__c();
        printAttribute.AccountNumber__c = '20200';
        printAttribute.Address__c = '1155 Market Street, San Francisco, CA';
        printAttribute.Department__c = 'ASR';
        printAttribute.Description__c = 'Testing Desc ';
        printAttribute.DocumentSetting__c = documentSetting.id;
        printAttribute.EmailAddress__c = 'test@test.com';
        printAttribute.EnvelopePicklist__c = '#10, window';
        printAttribute.FoldPicklist__c = 'letterfold';
        printAttribute.Inbound_Outbound__c = 'Outbound';
        printAttribute.InkPicklist__c = 'black';
        printAttribute.PaperPicklist__c = '20# text';
        printAttribute.PaperColorPicklist__c = 'White';
        printAttribute.PeopleSoftChartfields__c = '10000|229014|10000|10001634|1';
        printAttribute.PhoneNo__c = '1233456789';
        printAttribute.ProofRequired__c = 'No';
        //TODO populate email field through new custom setting
        //printAttribute.ReproNotificationEmailAddress__c = 'test@test.com';
        printAttribute.Sides__c = 'Two';
        printAttribute.SizePicklist__c = '8.5x14';
        printAttribute.SpecialPrintInstruction__c='No';
        printAttribute.InternationalMail__c='No';
        printAttribute.ReturnEnvelopeType__c='BPP';
        printAttribute.MailingIncludeReturnEnvelope__c='No';
        insert printAttribute;

        List<ContentVersion> contentVersions = new List<ContentVersion>();
        ContentVersion version1 = new ContentVersion();
        version1.ContentLocation = 'S';
        version1.VersionData = EncodingUtil.base64Decode('Test 1');
        version1.Title = 'Test1.txt';
        version1.PathOnClient = version1.Title;
        contentVersions.add(version1);
        ContentVersion version2 = new ContentVersion();
        version2.ContentLocation = 'S';
        version2.VersionData = EncodingUtil.base64Decode('Test 2');
        version2.Title = 'Test2.txt';
        version2.PathOnClient = version2.Title;
        version2.DocumentSetting__c = documentSetting.Id;
        contentVersions.add(version2);
        insert contentVersions;

        contentVersions = [select id, DocumentSetting__c, contentDocumentId from ContentVersion];
        List<ContentDocumentLink> documentLinks = new List<ContentDocumentLink>();
        ContentDocumentLink documentLink;
        for(contentversion version : contentVersions){
            documentLink= new ContentDocumentLink();
            documentLink.ContentDocumentId = version.ContentDocumentId;
            if(version.DocumentSetting__c == null){
                //Account document has to be ignored, as Document setting is not there
                documentLink.LinkedEntityId = account.Id;
            } else {
                //Document for Any other type (contact here) should not be sent to Java.
                documentLink.LinkedEntityId = documentSetting.Id;
            }
            documentLink.ShareType='V';
            documentLinks.add(documentLink);
        }
        insert documentLinks;

        Test.startTest();

        Database.executeBatch(new PrintMailerBatch());
        // Verify the scheduled job has not run yet.
        contentVersions = [SELECT Id, Status__c FROM ContentVersion WHERE SentToIntegrationLayer__c = null];
        System.assertEquals(2, contentVersions.size(), 'Content Versions are not updated');
        System.assertEquals('Created',contentVersions.get(0).Status__c);
        // Stopping the test will run the job synchronously
        Test.stopTest();

        // Now that the scheduled job has executed,
        // check that our tasks were created
        contentVersions = [SELECT Id, Status__c FROM ContentVersion WHERE SentToIntegrationLayer__c = null];
        System.assertEquals(2, contentVersions.size(), 'Content Versions are not updated');
        System.assertEquals('Created',contentVersions.get(0).Status__c);
    }

    @isTest
    private static void verifyPDFMetaDataIsCorrectlyCreated() {
        TH1__Document_Setting__c documentSetting = new TH1__Document_Setting__c(Name = 'Test Document Setting');
        insert documentSetting;

        PrintAttribute__c printAttribute = new PrintAttribute__c();
        printAttribute.AccountNumber__c = '20200';
        printAttribute.Address__c = '1155 Market Street, San Francisco, CA';
        printAttribute.Department__c = 'ASR';
        printAttribute.Description__c = 'Testing Desc ';
        printAttribute.DocumentSetting__c = documentSetting.Id;
        printAttribute.EmailAddress__c = 'test@test.com';
        printAttribute.EnvelopePicklist__c = '#10, window';
        printAttribute.FoldPicklist__c = 'letterfold';
        printAttribute.Inbound_Outbound__c = 'Outbound';
        printAttribute.InkPicklist__c = 'black';
        printAttribute.PaperPicklist__c = '20# text';
        printAttribute.PaperColorPicklist__c = 'White';
        printAttribute.PeopleSoftChartfields__c = '10000|229014|10000|10001634|1';
        printAttribute.PhoneNo__c = '1233456789';
        printAttribute.ProofRequired__c = 'No';
        //TODO populate email field through new custom setting
        //printAttribute.ReproNotificationEmailAddress__c = 'test@test.com';
        printAttribute.Sides__c = 'Two';
        printAttribute.SizePicklist__c = '8.5x14';
        printAttribute.SpecialPrintInstruction__c='No';
        printAttribute.InternationalMail__c='No';
        printAttribute.ReturnEnvelopeType__c='BPP';
        printAttribute.MailingIncludeReturnEnvelope__c='Yes';
        insert printAttribute;

        ContentVersion version = new ContentVersion();
        version.ContentLocation = 'S';
        version.VersionData = EncodingUtil.base64Decode('Test 1');
        version.Title = 'Test1';
        version.PathOnClient = version.Title;
        insert version;

        Test.startTest();
        //Most of data is set as it is from printattribtue to pdf metadata.
        //Testing the data that has some manupulation
		PrintMailerBatch.PDFMetaData pdfMetaData = new PrintMailerBatch.PDFMetaData(printAttribute, version, 'US');
        System.assertEquals(false, pdfMetaData.international);
        System.assertEquals(false, pdfMetaData.proofRequired);
        System.assertEquals(true, pdfMetaData.returnEnvelope);
        Test.stopTest();
    }
}