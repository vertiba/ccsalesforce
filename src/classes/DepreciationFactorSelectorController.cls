/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class DepreciationFactorSelectorController {

    @AuraEnabled
    public static MatchingFactorsResult getMatchingFactors(Id assessmentLineItemId) {
        AssessmentLineItem__c assessmentLineItem = [
            SELECT Id, Name, FactorCompositeKey__c, FallbackFactorCompositeKey__c, Category__c, Subcategory__c, AcquisitionYear__c, DepreciationFactor__c, UsefulLife__c
            FROM AssessmentLineItem__c
            WHERE Id = :assessmentLineItemId
        ];

        Map<String, List<Factor__c>> matchingFactorsByCompositeKeys = FactorUtility.getMatchingFactorsByCompositeKeys(assessmentLineItem.FactorCompositeKey__c, assessmentLineItem.FallbackFactorCompositeKey__c);
        List<Factor__c> matchingFactors = matchingFactorsByCompositeKeys.get(assessmentLineItem.FactorCompositeKey__c);

        MatchingFactorsResult result = new MatchingFactorsResult();
        result.matchingFactors = matchingFactors;
        if(assessmentLineItem.DepreciationFactor__c != null && matchingFactors != null) {
            result.currentFactor   = new Map<Id, Factor__c>(matchingFactors).get(assessmentLineItem.DepreciationFactor__c);
        }

        return result;
    }

    @AuraEnabled
    public static void setFactor(Id assessmentLineItemId, Id factorId) {
        System.debug('assessmentLineItemId=' + assessmentLineItemId);
        System.debug('factorId=' + factorId);

        AssessmentLineItem__c assessmentLineItem = [
            SELECT Id, Name, FactorCompositeKey__c, FallbackFactorCompositeKey__c,  Category__c, Subcategory__c, AcquisitionYear__c, DepreciationFactor__c, UsefulLife__c
            FROM AssessmentLineItem__c
            WHERE Id = :assessmentLineItemId
        ];

        Factor__c factor = [
            SELECT Id, FactorPercent__c, YearsOfUsefulLife__c
            FROM Factor__c
            WHERE Id = :factorId
        ];

        FactorUtility.applyFactor(assessmentLineItem, factor);
        update assessmentLineItem;
    }

    public class MatchingFactorsResult {
        @AuraEnabled public Factor__c currentFactor         {get; set;}
        @AuraEnabled public List<Factor__c> matchingFactors {get; set;}
    }

}