/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: BatchableError
 * Description: This class represents an error that occured processing a given chunk of items from a Batch Apex job.
 */ 
public class BatchableError {

    public String ApexClassName;
    public Id Id;
    public Id AsyncApexJobId;
    public boolean DoesExceedJobScopeMaxLength;
    public String ExceptionType;
    public String JobScope;
    public String Message;
    public String RequestId;
    public String StackTrace;
    
    /**
     * Method Name : newInstance
     * Description: Decomposes a persisted batch error into a BatchableError
     **/
    public static BatchableError newInstance(BatchApexError__c error) {
        BatchableError newError = new BatchableError(); 
        newError.ApexClassName = error.JobApexClass__c;
        newError.Id = error.Id;
        newError.AsyncApexJobId = error.AsyncApexJobId__c;
        newError.DoesExceedJobScopeMaxLength = error.DoesExceedJobScopeMaxLength__c;
        newError.ExceptionType = error.ExceptionType__c;
        newError.JobScope = error.JobScope__c;
        newError.Message = error.Message__c;
        newError.RequestId = error.RequestId__c;
        newError.StackTrace = error.StackTrace__c;
        return newError;
    }

    /**
     * Method: toString
     * Description: BatchableErrors are used by the BatchableRetryJob which implements RaisesPlatformEvents.
     *              If errors occur during retry (execute method) this ensures this state is 
     *              passed on (via the JobScope field on BatchApexErrorEvent) so that the another retry can occur.
     **/      
    public override String toString() {
        return JSON.serialize(this, true);
    }
    
}