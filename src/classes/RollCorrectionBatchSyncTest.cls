/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test RollCorrectionBatchSync
//-----------------------------
@isTest
public class RollCorrectionBatchSyncTest {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    private static void dataSetup(){
        TestDataUtility.disableAutomationCustomSettings(true);
        Test.setMock(HttpCalloutMock.class, new RollCorrectionBatchSyncMockCallout());
        //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        Property__c property = TestDataUtility.getVesselProperty();
        property.Account__c = businessAccount.Id;
        property.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        property.Status__c= CCSFConstants.STATUS_ACTIVE;
        insert property;
        
        String fiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        String prevfiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        
        List<RollYear__c> rollYears = new List<RollYear__c>();
        RollYear__c rollYear = TestDataUtility.buildRollYear(fiscalYear,fiscalYear,CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYears.add(rollYear);
        
        RollYear__c rollYear1 = TestDataUtility.buildRollYear(prevfiscalYear,prevfiscalYear,CCSFConstants.ROLLYEAR.ROLL_YEAR_Close_STATUS);
        rollYears.add(rollYear1);
        insert rollYears;
        
        
        //Insert Cases 
        List<Case> cases = new List<Case>();
        Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),property.Id,
                                                           rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(fiscalYear), 1, 1));
        assessment.Status =  CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX;
        assessment.AdjustmentType__c= CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_ROLLCORRECTION;
        assessment.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        assessment.AccountId = property.account__c;
        assessment.Property__c = property.Id;
        cases.add(assessment);
        
        Case assessment2  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),property.Id,
                                                            rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(fiscalYear), 1, 1));
        assessment2.Status =  CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment2.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX;
        assessment2.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        assessment2.AccountId = property.account__c;
        assessment2.Property__c = property.Id;
        cases.add(assessment2);
        
        Case assessment3  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property.Id,
                                                            rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(fiscalYear), 1, 1));
        assessment3.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment3.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
        assessment3.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
        assessment3.SubType__c = 'Closeout';
        assessment3.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
        assessment3.WaiveNoticePeriod__c = true;
        assessment3.Billable__c = 'No';
        assessment3.AccountId = property.account__c;
        assessment3.Property__c = property.Id;
        cases.add(assessment3);
        
        insert cases;
        TestDataUtility.disableAutomationCustomSettings(false);
        
        List<Case> newCases = [SELECT  id,IntegrationStatus__c FROM Case];
        List<ID> cases1=new List<ID>();
        for(Case inputCase:newCases){
            cases1.add(inputCase.id);
        }
        Test.startTest();
        Database.executeBatch(new RollCorrectionBatchSync());
        Test.stopTest();
    }   
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to check if batch class is triggering Regular cases
    //----------------------------- 
    @isTest
    private static void TestBatchClass() {
        Case newCase = [SELECT  id,IntegrationStatus__c,TriggerBatchId__c FROM Case where Type='Regular' and AdjustmentType__c ='Roll Correction'];
        System.assertEquals(CCSFConstants.IN_TRANSIT_TO_TTX, newCase.IntegrationStatus__c);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to check if batch class is triggering Escape cases
    //----------------------------- 
    @isTest
    private static void TestBatchClassForEscapeCases() {
        Case newCase = [SELECT  id,IntegrationStatus__c,TriggerBatchId__c FROM Case where Type='ESCAPE' and SubType__c!= 'Closeout'];
        System.assertEquals(CCSFConstants.IN_TRANSIT_TO_TTX, newCase.IntegrationStatus__c);
    }
}