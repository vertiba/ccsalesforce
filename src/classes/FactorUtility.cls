/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class FactorUtility {

    public static Map<String, BusinessAssetMapping__c> getBusinessAssetMappingsByCompositeKeys(String businessAssetMappingCompositeKey, String factorType) {
        return getBusinessAssetMappingsByCompositeKeys(new List<String>{businessAssetMappingCompositeKey}, factorType);
    }

    public static Map<String, BusinessAssetMapping__c> getBusinessAssetMappingsByCompositeKeys(List<String> businessAssetMappingCompositeKeys, String factorType) {
        Map<String, BusinessAssetMapping__c> businessAssetMappingByCompositeKey = new Map<String, BusinessAssetMapping__c>();

        for(BusinessAssetMapping__c businessAssetMapping : [SELECT Id, CompositeKey__c, AssetClassification__c, AssetSubclassification__c, FactorSource__c, Subcomponent__c, FixturesAllocationPercent__c, YearsOfUsefulLife__c, FactorSource__r.Name, FactorType__c
            FROM BusinessAssetMapping__c
            WHERE FactorType__c = :factorType and CompositeKey__c IN :businessAssetMappingCompositeKeys
        ]) {
            businessAssetMappingByCompositeKey.put(businessAssetMapping.CompositeKey__c, businessAssetMapping);
        }

        return businessAssetMappingByCompositeKey;
    }

    public static Map<Id, BusinessAssetMapping__c> getBusinessAssetMappingsById(List<Id> businessAssetMappingIds) {
        /* Map<String, BusinessAssetMapping__c> businessAssetMappingByCompositeKey = new Map<String, BusinessAssetMapping__c>();

        for(BusinessAssetMapping__c businessAssetMapping : [SELECT Id, CompositeKey__c, AssetClassification__c, AssetSubclassification__c, FactorSource__c, FixturesAllocationPercent__c, YearsOfUsefulLife__c, FactorSource__r.Name
            FROM BusinessAssetMapping__c
            WHERE CompositeKey__c IN :businessAssetMappingIds
        ]) {
            businessAssetMappingByCompositeKey.put(businessAssetMapping.CompositeKey__c, businessAssetMapping);
        } */

        return new Map<Id, BusinessAssetMapping__c>([
            SELECT Id, CompositeKey__c, AssetClassification__c, AssetSubclassification__c, FactorSource__c, FixturesAllocationPercent__c, YearsOfUsefulLife__c, FactorSource__r.Name, Subcomponent__c
            FROM BusinessAssetMapping__c
            WHERE Id IN :businessAssetMappingIds
        ]);
    }

    public static Map<String, List<Factor__c>> getMatchingFactorsByCompositeKeys(String factorCompositeKey, String fallbackFactorCompositeKey) {
        return getMatchingFactorsByCompositeKeys(new List<String>{factorCompositeKey}, new List<String>{fallbackFactorCompositeKey});
    }

    public static Map<String, List<Factor__c>> getMatchingFactorsByCompositeKeys(List<String> factorCompositeKeys, List<String> fallbackFactorCompositeKeys) {
        Map<String, List<Factor__c>> matchingFactorsByCompositeKey = new Map<String, List<Factor__c>>();

        for(Factor__c factor : [
            SELECT Id, Name, CalculatedName__c, FactorSource__c, Subcomponent__c, AssessmentYear__c, FactorPercent__c, YearsOfUsefulLife__c, IsFallback__c, CalculatedGroupCompositeKey__c, CalculatedFallbackCompositeKey__c
            FROM Factor__c
            WHERE CalculatedGroupCompositeKey__c IN :factorCompositeKeys
            OR CalculatedFallbackCompositeKey__c IN :fallbackFactorCompositeKeys
            ORDER BY AcquisitionYear__c DESC, YearsOfUsefulLife__c ASC
        ]) {
            List<Factor__c> matchingFactors = matchingFactorsByCompositeKey.get(factor.CalculatedGroupCompositeKey__c);
            if(matchingFactors == null) matchingFactors = new List<Factor__c>();

            matchingFactors.add(factor);
            matchingFactorsByCompositeKey.put(factor.CalculatedGroupCompositeKey__c, matchingFactors);
            if(factor.IsFallback__c != null && factor.CalculatedFallbackCompositeKey__c != null) {
                matchingFactorsByCompositeKey.put(factor.CalculatedFallbackCompositeKey__c, new List<Factor__c>{factor});
            }
        }

        return matchingFactorsByCompositeKey;
    }

    public static Factor__c findDefaultFactor(AssessmentLineItem__c assessementLineItem ,BusinessAssetMapping__c businessAssetMapping, List<Factor__c> matchingFactors, List<Factor__c> matchingFallbackFactors) {
        Factor__c defaultFactor;
        if(businessAssetMapping == null) return defaultFactor;
        if(matchingFactors == null && matchingFallbackFactors == null ) return defaultFactor;

        for(Factor__c factor : matchingFactors) {
            if(businessAssetMapping.FactorSource__c != factor.FactorSource__c) continue;
            if(businessAssetMapping.Subcomponent__c != factor.Subcomponent__c) continue;
            if(businessAssetMapping.YearsOfUsefulLife__c != factor.YearsOfUsefulLife__c) continue;

            defaultFactor = factor;
        }

        if(defaultFactor == null && matchingFactors.size() == 1) defaultFactor = matchingFactors[0];

        if(defaultFactor == null && matchingFallbackFactors != null && matchingFallbackFactors.size()>1) {
            logger.addRecordDebugEntry(assessementLineItem, 'Multiple fall Back factors. Invalid Data', 'factor Utility: findDefaultFactor Method');
        }
        else if(defaultFactor == null && matchingFallbackFactors != null && matchingFallbackFactors.size() ==1) {
            defaultFactor =  matchingFallbackFactors[0];
        }
        return defaultFactor;
    }

    public static void applyFactor(AssessmentLineItem__c assessmentLineItem, Factor__c factor) {
        assessmentLineItem.LastCalculationDate__c = System.now();

        if(factor == null) {
            assessmentLineItem.DepreciationFactor__c        = null;
            assessmentLineItem.DepreciationFactorPercent__c = null;
            assessmentLineItem.UsefulLife__c                = null;
        } else {
            assessmentLineItem.DepreciationFactor__c        = factor.Id;
            assessmentLineItem.DepreciationFactorPercent__c = factor.FactorPercent__c;
            assessmentLineItem.UsefulLife__c                = factor.YearsOfUsefulLife__c;
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient    
    // @param : Set<String>  factorCompositeKeys  
    // @description : This method takes factorCompositeKeys as input and return a map of matching factors.         
    // @return : Map<String, Factor__c>
    //-----------------------------
    public static Map<String, Factor__c> getMatchingVesselFactorsByCompositeKeys(Set<String> factorCompositeKeys) {

        Map<String, Factor__c> matchingFactorsByCompositeKey = new Map<String, Factor__c>();
        
        for(Factor__c factor : [SELECT Id, FactorPercent__c, CalculatedGroupCompositeKey__c FROM Factor__c 
                                WHERE CalculatedGroupCompositeKey__c IN :factorCompositeKeys            
                                ORDER BY AcquisitionYear__c DESC, YearsOfUsefulLife__c ASC]) {

           
            matchingFactorsByCompositeKey.put(factor.CalculatedGroupCompositeKey__c, factor);
            
        }

        return matchingFactorsByCompositeKey;
    }
}