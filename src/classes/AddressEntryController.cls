/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class AddressEntryController {

    // TODO move to named credential or custom setting
    private static final String USPS_USER_ID = '512PUBLI4209';

    /**
     * getAddressMetadata description
     * @param  sobjectApiName sobjectApiName description
     * @return                return description
     */
    @AuraEnabled(cacheable=true)
    public static AddressMetadata getAddressMetadata(String sobjectApiName, String addressType ) {
        return new AddressMetadata(sobjectApiName, addressType);
    }

    /**
     * lookupAddress description
     * @param  street  street description
     * @param  city    city description
     * @param  state   state description
     * @param  zipCode zipCode description
     * @return         return description
     */
    @AuraEnabled
    public static AddressLookupResult lookupAddress(String street, String city, String state, String zipCode) {
    
        CityStateLookupResult cityStateLookupResult = lookupCityState(zipCode);
        city  = cityStateLookupResult.City;
        state = cityStateLookupResult.State.right(2);

        String xmlString = '<AddressValidateRequest USERID="' + USPS_USER_ID + '">'
            + '<Address ID="0">'
            + '<Address1></Address1>'
            + '<Address2>' + street + '</Address2>'
            + '<City>' + city + '</City>'
            + '<State>' + state + '</State>'
            + '<Zip5>' + zipCode + '</Zip5>'
            + '<Zip4></Zip4>'
            + '</Address>'
            + '</AddressValidateRequest>';
		
        
        String endpoint = 'http://production.shippingapis.com/ShippingAPI.dll?'
            + 'API=Verify'
            + '&XML=' + EncodingUtil.urlEncode(xmlString, 'UTF-8');
        HttpResponse response = new Callout(endpoint).get();
      
        Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(response.getBody());
       
        Dom.XmlNode searchResultsNode = xmlDocument.getRootElement().getChildElement('Address', null);
        System.debug(searchResultsNode);
		AddressLookupResult result = new AddressLookupResult();
        
        
        if(searchResultsNode.getChildElement('Error', null) != NULL && searchResultsNode.getChildElement('Error', null).getName() == 'Error'){
        	  String errorMsg= searchResultsNode.getChildElement('Error', null).getChildElement('Description',null).getText();

            throw new AuraHandledException(errorMsg);
      
        }
        else{
        result.Street = searchResultsNode.getChildElement('Address2', null).getText();
        result.City   = searchResultsNode.getChildElement('City', null).getText();
        result.State  = 'US-' + searchResultsNode.getChildElement('State', null).getText();
        result.Zip5   = searchResultsNode.getChildElement('Zip5', null).getText();
        result.Zip4   = searchResultsNode.getChildElement('Zip4', null).getText();
           
        }
          return result;
    }

    /**
     * lookupCityState description
     * @param  zipCode zipCode description
     * @return         return description
     */
    @AuraEnabled
    public static CityStateLookupResult lookupCityState(String zipCode) {

        String xmlString = '<CityStateLookupRequest USERID="' + USPS_USER_ID + '">'
            + '<ZipCode ID="0">'
            + '<Zip5>' + zipCode + '</Zip5>'
            + '</ZipCode>'
            + '</CityStateLookupRequest>';

        String endpoint = 'http://production.shippingapis.com/ShippingAPI.dll?'
            + 'API=CityStateLookup'
            + '&XML=' + EncodingUtil.urlEncode(xmlString, 'UTF-8');
   
        HttpResponse response = new Callout(endpoint).get();
        System.debug('response.getBody()=' + response.getBody());

        Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(response.getBody());

        Dom.XmlNode searchResultsNode = xmlDocument.getRootElement().getChildElement('ZipCode',null);
        System.debug('searchResultsNode');
        System.debug(searchResultsNode);

        CityStateLookupResult result = new CityStateLookupResult();
        if(searchResultsNode.getChildElement('Error', null) != NULL && searchResultsNode.getChildElement('Error', null).getName() == 'Error'){
            String errorMsg= searchResultsNode.getChildElement('Error', null).getChildElement('Description',null).getText();
            system.debug('Error Msg City and Zip Code-->'+ errorMsg);
            throw new AuraHandledException(errorMsg);
            
        }else{
            result.City  = searchResultsNode.getChildElement('City', null).getText();
            result.State = 'US-' + searchResultsNode.getChildElement('State', null).getText();
            result.Zip5  = searchResultsNode.getChildElement('Zip5', null).getText();
        }
       

        return result;
    }

    /**
     * lookupZipCode description
     * @param  street street description
     * @param  city   city description
     * @param  state  state description
     * @return        return description
     */
    /* If needed in future for Zip code
    @AuraEnabled
    public static ZipCodeLookupResult lookupZipCode(String street, String city, String state) {
        System.debug('street=' + street);
        System.debug('city=' + city);
        System.debug('state=' + state);

        String xmlString = '<ZipCodeLookupRequest USERID="' + USPS_USER_ID + '">'
            + '<Address ID="0">'
            + '<Address1></Address1>'
            + '<Address2>' + street + '</Address2>'
            + '<City>' + city + '</City>'
            + '<State>' + state.right(2) + '</State>'
            + '<Zip5></Zip5>'
            + '<Zip4></Zip4>'
            + '</Address>'
            + '</ZipCodeLookupRequest>';

        String endpoint = 'http://production.shippingapis.com/ShippingAPI.dll?'
            + 'API=ZipCodeLookup'
            + '&XML=' + EncodingUtil.urlEncode(xmlString, 'UTF-8');

        System.debug('endpoint=' + endpoint);

        HttpResponse response = new Callout(endpoint).get();
        System.debug('response.getBody()=' + response.getBody());

        Dom.Document xmlDocument = new Dom.Document();
        xmlDocument.load(response.getBody());

        Dom.XmlNode searchResultsNode = xmlDocument.getRootElement().getChildElement('Address',null);
        System.debug('searchResultsNode');
        System.debug(searchResultsNode);

        ZipCodeLookupResult result = new ZipCodeLookupResult();
        
        if(searchResultsNode.getChildElement('Error', null) != NULL && searchResultsNode.getChildElement('Error', null).getName() == 'Error'){
            String errorMsg= searchResultsNode.getChildElement('Error', null).getChildElement('Description',null).getText();
            system.debug('Error Msg Search Zip Code-->'+ errorMsg);
            throw new AuraHandledException(errorMsg);
            
        }
        else{
            result.City  = searchResultsNode.getChildElement('City', null).getText();
            result.State = 'US-' + searchResultsNode.getChildElement('State', null).getText();
            result.Zip5  = searchResultsNode.getChildElement('Zip5', null).getText();
            result.Zip4  = searchResultsNode.getChildElement('Zip4', null).getText();
        }
        return result;
    }
*/
    
    /**
     * getData description
     * @param  sobjectApiName  description- Sobject Name
     * @param  addressType    description- addressType Mailing,Location etc
     * @param  recID   description- Record Id
     *  @return         return sObject
     */
    // To Load data 
	@AuraEnabled
    public static sobject getData(String sobjectApiName, String addressType , string recID){
        List<String> fieldsNames= new List<String>();
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(sobjectApiName).getDescribe().SObjectType.getDescribe().fields.getMap();
        for(Schema.SObjectField field : fields.values())       
        {
            Schema.DescribeFieldResult fieldResult = field.getDescribe();
            String fieldName= field.getDescribe().getName();
            if(fieldName.startsWith(addressType)){ 
                fieldsNames.add(fieldName); 
             }               
        }
                
       string sQuery = 'Select Id,RecordTypeId, '  + String.join(fieldsNames,',') +' From ' + sobjectApiName + ' WHERE ID = :recID LIMIT 1';
         return database.query(sQuery);
       	
    }
    
   public class AddressMetadata {
        @AuraEnabled public List<String> StreetPreDirections {get; private set;}
        @AuraEnabled public List<String> StreetTypes         {get; private set;}
        @AuraEnabled public List<String> UnitTypes           {get; private set;}
        @AuraEnabled public List<String> StreetFractions     {get; private set;}

        public AddressMetadata(String sobjectApiName, String addressType) {
            Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(sobjectApiName);

            
            this.StreetTypes         = this.getPicklistValues(sobjectType, addressType+'StreetType__c');
            this.UnitTypes           = this.getPicklistValues(sobjectType, addressType+'UnitType__c');
            this.StreetFractions     = this.getPicklistValues(sobjectType, addressType+'StreetFraction__c');
            this.StreetPreDirections = this.getPicklistValues(sobjectType, addressType+'StreetPreDirection__c');
            
        }

        private List<String> getPicklistValues(Schema.SObjectType sobjectType, String fieldApiName) {
            List<String> picklistValues = new List<String>();

            Schema.DescribeFieldResult fieldDescribe = sobjectType.getDescribe().fields.getMap().get(fieldApiName).getDescribe();
            for(Schema.PicklistEntry picklistEntry : fieldDescribe.getPicklistValues()) {
                picklistValues.add(picklistEntry.getValue());
            }

            return picklistValues;
        }
   }   
    

    public class AddressLookupResult {
        @AuraEnabled public String City   {get; private set;}
        @AuraEnabled public String Street {get; private set;}
        @AuraEnabled public String State  {get; private set;}
        @AuraEnabled public String Zip5   {get; private set;}
        @AuraEnabled public String Zip4   {get; private set;}
    }

    public class CityStateLookupResult {
        @AuraEnabled public String City  {get; private set;}
        @AuraEnabled public String State {get; private set;}
        @AuraEnabled public String Zip5  {get; private set;}
    }
/* If needed in Function for Zip Code Lookup function
    public class ZipCodeLookupResult {
        @AuraEnabled public String City   {get; private set;}
        @AuraEnabled public String Street {get; private set;}
        @AuraEnabled public String State  {get; private set;}
        @AuraEnabled public String Zip5   {get; private set;}
        @AuraEnabled public String Zip4   {get; private set;}
    }
*/

}