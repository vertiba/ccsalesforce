@isTest
public class RoutingUtilityTest {
    @TestSetup
    static void makeData(){
        insert new LoggerSettings__c();
        String rollYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        String lastYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        
        RollYear__c rollYear1 = new RollYear__c();
        rollYear1.Name = rollYear;
        rollYear1.Year__c = rollYear;
        rollYear1.Status__c='Roll Open';
        insert rollYear1;

        insert new Penalty__c(RTCode__c = System.Label.Late_Filer_Penalty_R_T_Code);
    }

    @IsTest
    public static void testRouting(){
        List<AttributeSkillMapping__mdt> mappings = [SELECT SkillName__c, Field__r.QualifiedAPIName, Value__c, Object__r.QualifiedAPIName
                                                    FROM AttributeSkillMapping__mdt];

        SObject testObject;
        AttributeSkillMapping__mdt finalMapping;
        for(AttributeSkillMapping__mdt mapping : mappings){
            finalMapping = mapping;
            Type t = Type.forName(mapping.Object__r.QualifiedAPIName);

            testObject = (SObject) t.newInstance();
            Schema.DescribeFieldResult field = testObject.getSObjectType().getDescribe().fields.getMap().get(mapping.Field__r.QualifiedAPIName).getDescribe();
            
            if(field.isRestrictedPicklist() || !field.isUpdateable()){
                testObject = null;
            }else{
                System.debug('Writing ' + mapping.Value__c + ' to ' + Mapping.Object__r.QualifiedAPIName + '.' + mapping.Field__r.QualifiedAPIName);
                testObject.put(mapping.Field__r.QualifiedAPIName, mapping.Value__c);
                break;
            }
        }

        if(testObject == null){
            return;
        }
        
        
        insert testObject;
        
        Test.startTest();

        RoutingUtility.doSkillBasedRouting(new List<SObject>{testObject});

        Test.stopTest();

        List<SkillRequirement> srs = [SELECT Skill.DeveloperName 
                                    FROM SkillRequirement
                                    WHERE Skill.DeveloperName = :finalMapping.SkillName__c];
        
        System.assert(!srs.isEmpty(), 'No Skill Requirement created for OmniChannel');
        
    }
}