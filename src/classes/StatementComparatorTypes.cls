/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : When Statement Comparator is added to lightning page, a value has 
// 		to be passed and this method helps in standardizing those paramenter values.
//-----------------------------
public class StatementComparatorTypes extends VisualEditor.DynamicPickList{
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns if any value has to be set 
    // 		as default while adding component to page.
    // @return : VisualEditor.DataRow
    //-----------------------------
    public override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultRow = new VisualEditor.DataRow('None', '');
        return defaultRow;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns all the expected values  
    // 		that can be used while adding component to page.
    // @return : VisualEditor.DynamicPickListRows
    //-----------------------------
    public override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DynamicPickListRows comparatorTypes = new VisualEditor.DynamicPickListRows();
        comparatorTypes.addRow(new VisualEditor.DataRow('LineItem To LineItem Comparator', 'LineItemToLineItemComparator'));
        comparatorTypes.addRow(new VisualEditor.DataRow('LineItem To Statement Comparator', 'LineItemToStatementComparator'));
        comparatorTypes.addRow(new VisualEditor.DataRow('Statement To Statement Comparator', 'StatementToStatementComparator'));
        comparatorTypes.addRow(new VisualEditor.DataRow('Statement To LineItem Comparator', 'StatementToLineItemComparator'));
        return comparatorTypes;
    }
}