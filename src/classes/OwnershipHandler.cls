/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Trigger handler class for the object Ownership__c
//      Currently no logic has been added, but this handler class triggers DLRS field calculations (via TriggerHandler)
//-----------------------------
public without sharing class OwnershipHandler extends TriggerHandler {}