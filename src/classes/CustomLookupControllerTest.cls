/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : test Class for Apex Controller CustomLookupController . 
//-----------------------------
@isTest
public class CustomLookupControllerTest {
    
      //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method to create Testdata.
    // @return : void
    //-----------------------------

    @testSetup
    private static void dataSetup(){
            
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin); 
        User bppStaff = TestDataUtility.getBPPAuditorUser();
        users.add(bppStaff);
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
           
        }
    }

     
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is test the resluts fromm Custom Controller Method findRecords
    // @return : void
    //-----------------------------

    @isTest
    private static void findRecordsTest() { 

        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Test.startTest();
           List<Sobject> recordList =  CustomLookupController.findRecords('Test Business Acc','Account');
           system.assert(recordList !=null, 'Successfully Found records');
        Test.stopTest();

    }
}