/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to store all constants values which will be used in other classes.
public class CCSFConstants {

    // Custom getters handle lazy-loading of each constant, which should help reduce the overall heap size
    public static final AccountConstants ACCOUNT                                   { get { if(ACCOUNT == null) { ACCOUNT = new AccountConstants(); } return ACCOUNT; } private set; }
    public static final PropertyConstants PROPERTY                                 { get { if(PROPERTY == null) { PROPERTY = new PropertyConstants(); } return PROPERTY; } private set; }
    public static final AssessmentConstants ASSESSMENT                             { get { if(ASSESSMENT == null) { ASSESSMENT = new AssessmentConstants(); } return ASSESSMENT; } private set; }
    public static final AssessmentLineItemsConstants ASSESSMENTLINEITEMS           { get { if(ASSESSMENTLINEITEMS == null) { ASSESSMENTLINEITEMS = new AssessmentLineItemsConstants(); } return ASSESSMENTLINEITEMS; } private set; }
    public static final StatementConstants STATEMENT                               { get { if(STATEMENT == null) { STATEMENT = new StatementConstants(); } return STATEMENT; } private set; }
    public static final StatementReportedAssetConstants STATEMENT_REPORTED_ASSETS  { get { if(STATEMENT_REPORTED_ASSETS == null) { STATEMENT_REPORTED_ASSETS = new StatementReportedAssetConstants(); } return STATEMENT_REPORTED_ASSETS; } private set; }
    public static final RollYearConstants ROLLYEAR                                 { get { if(ROLLYEAR == null) { ROLLYEAR = new RollYearConstants(); } return ROLLYEAR; } private set; }
    public static final BatchMessagesConstants BATCHMSG                            { get { if(BATCHMSG == null) { BATCHMSG = new BatchMessagesConstants(); } return BATCHMSG; } private set; }
    public static final ButtonMessagesConstants BUTTONMSG                          { get { if(BUTTONMSG == null) { BUTTONMSG = new ButtonMessagesConstants(); } return BUTTONMSG; } private set; }
    public static final OmniChannelConstants OMNICHANNEL                           { get { if(OMNICHANNEL == null) { OMNICHANNEL = new OmniChannelConstants(); } return OMNICHANNEL; } private set; }
    public static final MassApprovalConstants MASSAPPROVAL                         { get { if(MASSAPPROVAL == null) { MASSAPPROVAL = new MassApprovalConstants(); } return MASSAPPROVAL; } private set; }
    public static final AnnualRollClosureFileGenerationConstants AABFILE_CONSTANTS { get { if(AABFILE_CONSTANTS == null) { AABFILE_CONSTANTS = new AnnualRollClosureFileGenerationConstants(); } return AABFILE_CONSTANTS; } private set; }
    public static final AumentumProcessConstants AUMENTUM_PROCESS                  { get { if(AUMENTUM_PROCESS == null) { AUMENTUM_PROCESS = new AumentumProcessConstants(); } return AUMENTUM_PROCESS; } private set; }
    public static final EscapeCloseoutConstants ESCAPECLOSEOUTS                    { get { if(ESCAPECLOSEOUTS == null) { ESCAPECLOSEOUTS = new EscapeCloseoutConstants(); } return ESCAPECLOSEOUTS; } private set; }
    public static final OnbaseContentVersionConstants ONBASE_CONTENTVERSION        { get { if(ONBASE_CONTENTVERSION == null) { ONBASE_CONTENTVERSION = new OnbaseContentVersionConstants(); } return ONBASE_CONTENTVERSION; } private set; }
    public static final PropertyEventConstants PROPERTY_EVENT                      { get { if(PROPERTY_EVENT == null) { PROPERTY_EVENT = new PropertyEventConstants(); } return PROPERTY_EVENT; } private set; }

    public class AccountConstants {
        public final String ACCOUNT_API_NAME                    = 'Account';
        public final String BUSINESS_RECORD_TYPE_API_NAME       = 'BusinessAccount';
        public final String PERSON_ACCOUNT_RECORD_TYPE_API_NAME = 'PersonAccount';
        public final String BUSINESS_STATUS_INACTIVE            = 'Inactive';
        public final String BUSINESS_STATUS_API                 = 'BusinessStatus__c';
    }

    public class OmniChannelConstants {
        public final String ROUTING_MODEL                  = 'MostAvailable';
        public final String ROUTING_TYPE                   = 'SkillsBased';
        public final Double CAPACITY                       = 5;
        public final Integer ROUTING_PRIORITY              = 1;
        public final String BPP_STATEMENT_ROUTING          = 'BPP_Statement_Skill_Routing';
        public final String SERVICE_CHANNEL                = 'Exemption_Channel';
        public final String SKILL_NAME                     = 'BPPStatement';
        public final String SKILL_NAME_571R                = 'Form571R';
        public final String SKILL_NAME_571STR              = 'Form571STR';
        public final String AUDITOR_PROFILE_NAME           = 'BPP Staff';
        public final String AUDITOR_ROLE_NAME              = 'BPP Auditor';
        public final String SKILL_BPP_ASSESMENT            = 'BPPAssesment';
        public final String SERVICE_CHANNEL_CASE           = 'Case';
        public final String SKILL_VESSEL_ASSESMENT         = 'VesselAssessments';
        public final String BPP_AUDITOR                    = 'BPPAuditor';
        public final String BPP_PRINCIPAL_ROLE_NAME        = 'BPPPrincipal';
        public final Integer SKILL_LEVEL                   = 5;
        public final String BPP_OFFICE_ASSISTANT_ROLE_NAME = 'BPPOfficeAssistant';
    }

    public class PropertyConstants {
        public final String BPP_RECORD_TYPE_API_NAME                             = 'BusinessPersonalProperty';
        public final String VESSEL_RECORD_TYPE_API_NAME                          = 'Vessel';
        public final String LEASED_EQUIPMENT_RECORD_TYPE_API_NAME                = 'LeasedEquipment';
        public final String APARTMENT_RECORD_TYPE_API_NAME                       = 'ApartmentUnit';
        public final String VESSEL_BOAT_TYPE_POWER_BOAT                          = 'Powerboat';
        public final String VESSEL_BOAT_TYPE_SAIL_BOAT                           = 'Sailboat';
        public final String VESSEL_BOAT_TYPE_PERSONAL_WATERCRAFT                 = 'Personal Watercraft (Jet Ski)';
        public final String VESSEL_POWER_BOAT_UNDER_30                           = 'Power Boat (Under 30)';
        public final String VESSEL_POWER_BOAT_OVER_30                            = 'Power Boat (30 and Over)';
        public final String VESSEL_VALUATION_FACTOR_NEW_PURCHASE                 = 'New Purchase';
        public final String VESSEL_VALUATION_FACTOR_USED_PURCHASE                = 'Used Purchase';
        public final String VESSEL_VALUATION_FACTOR_ANNUAL_VALUATION             = 'Annual Valuation';
        public final String VESSEL_SERVICE_TYPE_BARGES                           = 'Barges';
        public final String PRIMARY_FORM_TYPE_571_L                              = '571-L';
        public final String PRIMARY_FORM_TYPE_571_R                              = '571-R';
        public final String PRIMARY_FORM_TYPE_571_STR                            = '571-STR';
        public final String REQUIRED_TO_FILE_YES                                 = 'Yes';
        public final String REQUIRED_TO_FILE_NO                                  = 'No';
        public final String REQUIRED_TO_FILE_OPTIONAL                            = 'OPTIONAL';
        public final String PROPERTY_TYPE_LESSOR                                 = 'Lessor';
        public final String PROPERTY_TYPE_LESSEE                                 = 'Lessee';
        public final String STATUS_ACTIVE                                        = 'Active';
        public final String STATUS_INACTIVE                                      = 'Inactive';
        public final String STATUS_API                                           = 'Status__c';
        public final String NOT_REQUIRED_TO_FILE_REASON_LEASED_EQUIPMENT         = 'Leased Equipment';
        public final String NOT_REQUIRED_TO_FILE_REASON_INACTIVE_ON_LIEN_DATE    = 'Inactive on Lien Date';
        public final String NOT_REQUIRED_TO_FILE_REASON_REFERENCE_PROPERTY       = 'Non-Assessable (Reference Property)';
        public final String NOT_REQUIRED_TO_FILE_REASON_GOVERNMENT_ENTITY        = 'Non-Assessable (Government Entity)';
        public final String NOT_REQUIRED_TO_FILE_REASON_FEDERAL_ENCLAVE_PROPERTY = 'Non-Assessable (Federal Enclave Property)';
        public final String NOT_REQUIRED_TO_FILE_REASON_STATE_ASSESSED_PROPERTY  = 'Non-Assessable (State Assessed Property)';
        public final String NOT_REQUIRED_TO_FILE_REASON_NON_ASSESSABLE           = 'Non-Assessable';
        public final String INACTIVE_REASON_API                                  = 'DeactivationReason__c';
        public final String INACTIVE_REASON_VALUE_FOR_AUMENTUM                   = 'Closed in Aumentum';
        public final String DISCOVERY_SOURCE_TTX_API                             = 'TTX';
    }

    public class AssessmentConstants {
        public final String VALIDATION_EXCEPTION_TEXT                        = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
        public final String CASE_API_NAME                                    = 'Case';
        public final String BPP_RECORD_TYPE_API_NAME                         = 'BPPAssessment';
        public final String LEGACY_BPP_RECORD_TYPE_API_NAME                  = 'LegacyBPPAssessment';
        public final String VESSEL_RECORD_TYPE_API_NAME                      = 'VesselAssessment';
        public final String LEGACY_VESSEL_RECORD_TYPE_API_NAME               = 'Legacy_Vessel_Assessment';
        public final String CUSTOMER_CARE_RECORD_TYPE_API_NAME               = 'CustomerService';
        public final String TYPE_REGULAR                                     = 'Regular';
        public final String TYPE_ESCAPE                                      = 'Escape';
        public final String STATUS_INPROGRESS                                = 'In Progress';
        public final String STATUS_NEW                                       = 'New';
        public final String STATUS_CLOSED                                    = 'Closed';
        public final String STATUTE_OF_LIMITATION_CONFIG                     = Label.StatuteofLimitations;
        public final String STATUTE_OF_LIMITATION                            = '48';
        public final String BILLABLE_YES                                     = 'Yes';
        public final String BILLABLE_NO                                      = 'No';
        public final String BPP_RECORD_LABEL_NAME                            = 'BPP Assessment';
        public final String LEGACY_BPP_RECORD_LABEL_NAME                     = 'Legacy BPP Assessment';
        public final String LEGACY_VESSEL_RECORD_LABEL_NAME                  = 'Legacy Vessel Assessment';
        public final String VESSEL_RECORD_LABEL_NAME                         = 'Vessel Assessment';
        public final String GENERATED_ASSESSMENT_NONFILER                    = 'NonFiler';
        public final String READY_TO_SEND_TO_TTX                             = 'Ready to Send to TTX';
        public final String SENT_TO_TTX                                      = 'Sent to TTX';
        public final String SENT_TO_RP                                       = 'Sent to RP';
        public final String IN_TRANSIT_TO_TTX                                = 'In Transit to TTX';
        public final String SUB_CATEGORY_LEASED_EQUIPMENT                    = 'Leased Equipment';
        public final String STATUS_ONHOLD                                    = 'On Hold';
        public final String SUB_STATUS_WAITINGFORINFO                        = 'Waiting on Information';
        public final String SUB_STATUS_PENDINGREVIEW                         = 'Pending Review';
        public final String SUBSTATUS_COMPLETED                              = 'Completed';
        public final String SUBSTATUS_NOTICING                               = 'Noticing';
        public final String SUBSTATUS_CLOSED                                 = 'Closed';
        public final String ROLLCODE_SECURED                                 = 'Secured';
        public final String ROLLCODE_UNSECURED                               = 'Unsecured';
        public final String INTEGRATIONSTATUS_READY_TO_SEND_TO_RP            = 'Ready to Send to RP';
        public final String SUBSTATUS_CANCELLED                              = 'Cancelled';
        public final String ADJUSTMENT_CANCEL                                = 'Cancel';
        public final String STATUS_READY_TO_SEND                             = 'Ready to Send';
        public final String INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND           = 'Not Eligible to Send';
        public final String INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX           = 'Ready to Send to TTX';
        public final String ADJUSTMENT_TYPE_NEW                              = 'New';
        public final String INTEGRATIONSTATUS_REJECTED_BY_TTX                = 'Rejected by TTX';
        public final String INTEGRATIONSTATUS_REJECTED_BY_RP                 = 'Rejected by RP';
        public final String INTEGRATIONSTATUS_SENT_TO_RP                     = 'Sent to RP';
        public final String INTEGRATIONSTATUS_IN_TRANSIT_TO_RP               = 'In Transit to RP';
        public final String ADJUSTMENTTYPE_ROLLCORRECTION                    = 'Roll Correction';
        public final String ADJUSTMENTTYPE_CANCEL                            = 'Cancel';
        public final String SUB_TYPE_LOWVALUE                                = 'Low Value';
        public final String SUB_TYPE_CLOSEOUT                                = 'Closeout';
        public final String SYSTEM_REMOVED                                   = 'System Removed';
        public final String SYSTEM_GENERATED                                 = 'System Generated';
        public final String FRAUD                                            = 'Fraud';
        public final String ERROR_MESSAGE_PENALTY                            = Label.ErrorMessagePenalty;
        public final String ERROR_MESSAGE_INVALID_PENALTY                    = Label.ErrorMessageInvalidPenalty;
        public final String SUB_TYPE_NON_ASSESSABLE_MOVED_WITHIN_SF			 = 'Non-Assessable (Moved Within SF)';
        public final String SUB_TYPE_NON_ASSESSABLE_CLOSED         			 = 'Non-Assessable (Closed)';
        public final String SUB_TYPE_NON_ASSESSABLE_SOLD         			 = 'Non-Assessable (Sold)';
        public final String SUB_TYPE_NON_ASSESSABLE_MOVED_OUTSIDE_SF         = 'Non-Assessable (Moved Outside SF)';
        public final String SUB_TYPE_NON_ASSESSABLE_REFERENCE_PROPERTY       = 'Non-Assessable (Reference Property)';
        public final String SUB_TYPE_NON_ASSESSABLE_GOVERNMENT_ENTITY        = 'Non-Assessable (Government Entity)';
        public final String SUB_TYPE_NON_ASSESSABLE_FEDERAL_ENCLAVE_PROPERTY = 'Non-Assessable (Federal Enclave Property)';
        public final String SUB_TYPE_NON_ASSESSABLE_STATE_ASSESSED_PROPERTY  = 'Non-Assessable (State Assessed Property)';
        public final String SUB_TYPE_NOTHING_TO_ASSESS						 = 'Nothing to Assess';
        public final String SUB_TYPE_NOTHING_TO_ASSESS_VESSEL_INOPERABLE	 = 'Nothing to Assess (Vessel Inoperable)';
        public final List<String> SUB_TYPES_FOR_PROPERTY_REQUIRED_TO_FILE_NON_ASSESSABLE  =  new List<String>{
                                                                                                'Non-Assessable (Moved Within SF)',
                                                                                                'Non-Assessable (Closed)',
                                                                                                'Non-Assessable (Sold)',
                                                                                                'Non-Assessable (Moved Outside SF)'
                                                                                            };
        public final List<String> SUB_TYPES_FOR_NOT_REQUIRED_TO_FILE  =  new List<String>{
                                                                                        'Non-Assessable (Moved Within SF)',
                                                                                        'Non-Assessable (Closed)',
                                                                                        'Non-Assessable (Sold)',
                                                                                        'Non-Assessable (Moved Outside SF)',
                                                                                        'Non-Assessable (Reference Property)',
                                                                                        'Non-Assessable (Government Entity)',
                                                                                        'Non-Assessable (Federal Enclave Property)',
                                                                                        'Non-Assessable (State Assessed Property)'
                                                                                };
        public final String  CUSTOM_PERMISSION_CANCEL                 = 'CancelledAssesmentsCreator';
        public final String  CUSTOM_PERMISSION_CANCEL_REISSUE         = 'CancelledReissuedAssesmentsCreator';
        public final String ADJUSTMENT_REASON_EXEMPTION				  = 'Exemption';
        public final String ADJUSTMENT_REASON_TRANSFER				  = 'Transfer to/from Secured/Unsecured Roll';
        public final String TEMPLATE_ROLLCANCELLATION				  = 'NoticeTypeRollCorrectionCancelled';
        public final String TEMPLATE_REDUCTIONWITHOUTAPPEAL			  = 'NoticeTypeRollCorrectionReductionWithoutAppeal';
        public final String TEMPLATE_REDUCTIONWITHAPPEAL			  = 'NoticeTypeRollCorrectionReductionWithAppeal';
        public final String TEMPLATE_PROPOSEDESCAPEASSESSMENT	      = 'NoticeofProposedEscapeAssessment';
        public final String ADJUSTMENTREASON_AUDIT					  = 'Audit';
        public final String ADJUSTMENTREASON_AAB_Decision 			  = 'AAB Decision (Controller\'s Letter)';
        public final String FINALCLOSEOUTVALUESOURCE_ESCROWMEMO       = 'Escrow Memo';
    }

    public class AssessmentLineItemsConstants{
        public final String SUBCATEGORY_LEASEHOLD_IMPROVEMENTS_FIXTURES = 'Leasehold Improvements - Fixtures';
        public final String SUBCATEGORY_MACHINERY_EQUIPMENT             = 'Machinery & Equipment';
        public final String SUBCATEGORY_OTHER_EQUIPMENT                 = 'Other Equipment';
        public final String DEFAULT_SUBCLASSIFICATION                   = 'General';
        public final Set<String> SUBCATEGORY_SET                        = new Set<String>{SUBCATEGORY_LEASEHOLD_IMPROVEMENTS_FIXTURES,SUBCATEGORY_MACHINERY_EQUIPMENT,SUBCATEGORY_OTHER_EQUIPMENT};
    }

    public class StatementConstants {
        public final String STATEMENT_API_NAME                  = 'Statement__c';
        public final String BPP_RECORD_TYPE_API_NAME            = 'BusinessPersonalPropertyStatement';
        public final String STATUS_SUBMITTED                    = 'Submitted';
        public final String STATUS_INPROGRESS                   = 'In Progress';
        public final String STATUS_PROCESSED                    = 'Processed';
        public final String FORM_571R_API_NAME                  = '571-R';
        public final String FORM_571STR_API_NAME                = '571-STR';
        public final String VESSEL_RECORD_TYPE_API_NAME         = 'VesselStatement';
        public final String PROPERTY_EVENT_RECORD_TYPE_API_NAME = 'PropertyEventStatement';
        public final String AMEND_STATEMENT_DATABASE_ERROR      = Label.ErrorAmendStatement;
        public final String FILING_METHOD                       = 'Hard Copy';
        public final String SUBMITTED_STATEMENT_ERROR           = 'Already Submitted Statements Cannot Be updated';
        public final String IN_PROGRESS_STATEMENTS_PRESENT      = 'In Progress Statements present';
        public final String NO_INPROGRESS_STATEMENTS			= 'No InProgress Statements';
    }

    public class StatementReportedAssetConstants {
        public final String OWNED_ASSETS_RECORD_TYPE_API_NAME      = 'Owned_Assets';
        public final String LEASED_ASSETS_RECORD_TYPE_API_NAME     = 'Leased_Assets';
        public final Set<String> SCHEDULE_D_ASSET_CLASSIFICATIONS  =  new Set<String>{'Land and Land Development','Land Improvements','Leasehold Improvements - Fixtures','Leasehold Improvements - Structure'};
    }

    public class RollYearConstants {
        public final String ROLL_YEAR_CLOSE_STATUS   = 'Roll Closed';
        public final String ROLL_YEAR_OPEN_STATUS    = 'Roll Open';
        public final String ROLL_YEAR_PENDING_STATUS = 'Pending';
        public final String IN_TRANSIT_TO_TTX        = 'In Transit to TTX';
        public final String SENT_TO_TTX              = 'Sent to TTX';
    }

    public class BatchMessagesConstants {
        public final String BATCH_ALREADY_RUNNING_MSG = Label.BatchAlreadyRunningMessage;
        public final String BATCH_STATUS_COMPLETED    = 'Completed';
        public final String BATCH_STATUS_FAILED       = 'Failed';
        public final Set<String> BATCH_STATUSES       = new Set<String>{'Queued','Preparing','Processing','Holding'};
    }

    public class ButtonMessagesConstants {
        public final String BATCH_STATUS_COMPLETED                           = 'Completed';
        public final String BATCH_STATUS_FAILED                              = 'Failed';
        public final String LOW_VALUE_ASSESMENT_ALREADY_GENERATED_RMSG       = Label.LowValueAssesmentGeneratorMsg;
        public final String DIRECT_BILL_ASSESMENT_ALREADY_GENERATED_MSG      = Label.DirectBillAssesmentGeneratorMsg;
        public final String NONFILER_ASSESMENT_ALREADY_GENERATED_MSG         = Label.NonFilerAssesmentGeneratorMsg;
        public final String DIRECT_BILL_ASSESMENT_GENERATOR_WARNING          = Label.DirectBillAssesmentGeneratorWarningMsg;
        public final String NONFILER_ASSESMENT_GENERATOR_WARNING             = Label.NonFilerAssesmentGeneratorWarningMsg;
        public final String CLOSED_ROLL_YEAR_BUTTON_WARNING                  = Label.RollYearClosed;
        public final String OPENED_ROLL_YEAR_BUTTON_WARNING                  = Label.RollYearOpened;
        public final String INTEGRATION_STATUS_WARNING                       = Label.RollYearIntegrationStatusWarningMsg;
        public final String CALENDAR_ROLL_YEAR_BUTTON_WARNING                = Label.CalendarRollYearWarningMsg;
        public final String NO_PERMISSION                                    = Label.NoPermission;
        public final String DIRECTBILLVESSEL_ASSESMENT_GENERATOR_WARNING     = Label.DirectBillVesselAssesmentGeneratorWarningMsg;
        public final String DIRECTBILLVESSEL_ASSESMENT_ALREADY_GENERATED_MSG = Label.DirectBillVesselAssesmentGeneratorMsg;
    }

     public class MassApprovalConstants{
        public final String SUCCESS_COUNT                  = 'Success record count ';
        public final String FAILURE_COUNT                  = 'Failure record count ';
        public final String BATCH_RUNNING_MSG              = 'As, for mass approval limits are set as '+ Label.LimitForMassApproval+'  So, the batch is in processing for Mass Approval';
        public final String NO_RECORDS                     = 'There are no records to process';
        public final String MASS_APPROVAL_PERMISSION       = Label.MassApprovalPermission;
        public final String MASS_APPROVAL_LIMITS_CONFIGED  = Label.LimitForMassApproval;
        public final String MASS_APPROVAL_LIMITS_HARDCODED = '1000';
        public final String MASS_APPROVAL_APPROVED_ACTION  = 'Approve';
        public final String MASS_APPROVAL_COMMENTS         = 'Approved by Mass Approval Action';
        public final String MASS_APPROVAL_LOGGER_LOCATION  = 'MassApprovalUtility';
        public final String STATUS_APPROVED                = 'Approved';
    }

    public class AnnualRollClosureFileGenerationConstants {
        public final String ROLLYEAR_INTEGRATIONSTATUS_SENT_TO_TTX = 'Sent to TTX';
        public final String ERROR_MSG_INTEGRATION_STATUS           = 'Process cannot be Started as Integration Status should be equal to Sent to TTX';
        public final String CONFIRM_MSG_INTEGRATION_STATUS         = 'Click on Confirm button to trigger the File Generation Job';
        public final String PROCESS_TRIGGERED_SUCCESSFULLY_MSG     = 'Files Generation triggered Successfully';
        public final String PROCESS_TRIGGERED_FAILED_MSG           = 'Server Error. Please Contact your Organisation Administrator for further details';
        public final String NAMED_CREDENTIAL_NAME                  = 'SMART_Annual_Roll_Close_File_Generation_Service';
    }

    public class AumentumProcessConstants{
        public final String POSTCODE_STREET_TYPE_STREET            = Label.PostCodeForStreetTypeStreet;
        public final String POSTCODE_STREET_TYPE_AVENUE            = Label.PostCodeForStreetTypeAvenue;
        public final String STREET_TYPE_STREET                     = 'ST';
        public final String STREET_TYPE_AVENUE                     = 'AVE';
        public final String PO_BOX_Variation                       = Label.ContactAddressPOBOXVariation;
        public final String CITY_SF                                = 'SAN FRANCISCO';
        public final String AUMENTUM_AUTO_CLEANUP_CONTROLLER       = 'AumentumAutoCleanupController';
        public final String AUMENTUM_AUTO_CLEANUP_BATCH            = 'AumentumAutoCleanupBatchr';
        public final String AUMENTUM_RECORD_REVIEW_LOCATION        = 'AumentumReviewProcessorController';
        public final String AUMENTUM_POTENTIAL_MATCH_LOCATION      = 'PotentialMatchController';
        public final String AUMENTUM_STATUS_API_NAME               = 'Status__c';
        public final String KEYWORD_TEST                           = 'test';
        public final String KEYWORD_SFGOV                          = 'sgov';
        public final String TAXI_VARIATION_DBA_NAME                = Label.TaxiVariationDBANames;
        public final String STATUS_REVIEW                          = 'In Review';
        public final String STATUS_WARNING                         = 'Warning';
        public final String LOCATION_AUMENTUM_DATA_PROCESS         = 'AumentumDataProcess';
        public final String MESSAGE_NEED_REVIEW                    = ' Record need review, as record details are different from SMART records';
        public final String MESSAGE_FOUND_BOTH_BY_KEY              = ' Found Location and Business by Key: ';
        public final String MESSAGE_NO_MATCH_FOUND                 = ' No matched Location or Business found in SMART';
        public final String MESSAGE_NO_LOCATION_FOUND_ONLY_ACCOUNT = ' No matched Location found, only Business has been identified by key: ';
        public final String MESSAGE_POTENTIAL_MATCHES_FOUND        = ' Potential Matches found - Key :';
        public final String MESSAGE_CONTACT_EMAIL_CONTAINS_SGOV    = ' Contact Email contains Sgov keyword.';
        public final String MESSAGE_OWNER_NAME_CONTAINS_TEST       = ' Owner Name contains Test keyword.';
        public final String MESSAGE_TRADE_NAME_CONTAINS_TEST       = ' Trade Name contains Test keyword.';
        public final String MESSAGE_STREET_NAME_CONTAINS_TEST      = ' Street Name contains Test keyword.';
        public final String MESSAGE_CITY_NOT_SF                    = ' Location City is outside of SANS FRANCISCO.';
        public final String MESSAGE_SITUS_ADDRESS_POBOX            = ' SITUS ADDRESS is PO Box';
        public final String MESSAGE_TNC_KEYWORDS                   = ' Records with has taxi or TNC-related keyword in the Trade name.';
        public final String MESSAGE_LOC_STREET_NUMBER_MISSING      = ' Location Street number is missing.';
        public final String MESSAGE_LOC_STREET_TYPE_MISSING        = ' Location Street type is missing.';
        public final String MESSAGE_CON_STREET_NUMBER_MISSING      = ' Contact Street number is missing.';
        public final String MESSAGE_CON_STREET_NAME_MISSING        = ' Contact Street name is missing.';
        public final String MESSAGE_CON_STREET_TYPE_MISSING        = ' Contact Street type is missing.';
        public final String MESSAGE_CON_CITY_MISSING               = ' Contact city is missing.';
        public final String MESSAGE_CON_POSTAL_MISSING             = ' Contact postal is missing.';
        public final String MESSAGE_CON_STATE_MISSING              = ' Contact state is missing.';
        public final String PROPERTY_API                           = 'Property__c';
        public final String FULL_STOP                              = '.';
        public final String MESSAGE_TRIGGER_MUTIPLE_ACCOUNT_EXIST  = 'Account with this information, already exist in SMART';
        public final String MESSAGE_TRIGGER_MUTIPLE_PROPERTY_EXIST = 'Property with this information, already exist in SMART';
        public final String MESSAGE_ADD_NEXT_LINE                  = '\r\n';
        public final String MESSAGE_IGNORE                         = 'Either Business or Location is inactive in SMART.';
        //The Picklist value is "Ignored" instead of Ignore in Aumentum Object
        public final String STATUS_IGNORE                          = 'Ignored';
        public final String AUMENTUM_PERMISSION                    = Label.AumentumProcessPermission;
        public final String IS_LOCKED_API                          = 'IsLocked__c';
        public final String MESSAGE_CREATE_PROPERTY                = 'Create property too';
        public final String STAGING_OBJECT_API_NAME                = 'StagingAumentumBusiness__c';
        public final String POTENTIAL_OBJECT_API_NAME              = 'PotentialMatchForBusinessAndLocation__c';
        public final String REVIEW_OBJECT_API_NAME                 = 'StagingBusinessCompareField__c';
        public final String MESSAGE_DML_ERROR                      = 'Error during creations ';
        public final String MEASSGE_DUPLICATE                      = 'Duplicate Aumentum Record Found. The Duplicate indicator key is ';
        public final String STATUS_FAILED                          = 'Failed';
        public final String MESSAGE_WRONG_FEDIN                    = 'Tax Identification Number (Federal ID) does not follow XX-XXXXXXX format. Also, It should be 9 digits';
        public final String POTENTIAL_MESSAGE_FOR_IN_REVIEW        = 'Please Review Records Tab to update details in SMART';
        public final String POTENTIAL_MESSAGE_FOR_PROCESSED        = 'There is no changes in details of Aumentum Record and SMART';
        public final String POTENTIAL_MESSAGE_FOR_IGNORE           = 'Either Location or Business are In-Active in SMART';
        public final String POTENTIAL_MESSAGE_ISSUE                = 'Some Issue Occured During Potential Matches';

    }

    public class EscapeCloseoutConstants{
        public final String TYPE_SUBTYPE_MISMATCH_ERROR_MESSAGE = 'Escape Closeouts can only be created from Regular-Closeout Assessment';
        public final String NULL_FINALCLOSEOUT_ERROR            = 'Final Close Out value cannot be blank for creation of Escape Closeouts';
        public final String MISSING_YEARS_ERROR_MESSAGE_PART1   = 'Escape Closeout not created for Years : ';
        public final String MISSING_YEARS_ERROR_MESSAGE_PART2   = ' as there were no Past Assesments present for these Years';
        public final String SUCCESS_MESSAGE                     = 'Escape Closeouts Created Successfully';
        public final String SYSTEM_ERROR                        = 'System Error. Please contact your Organisation Admin for it';
        public final String SYSTEM_SUCCESS                      = 'Success';
        public final String SYSTEM_FAILED                       = 'failed';
        public final String CREATE_CLOSEOUTS_MANUALLY_MESSAGE   = 'No Past Assessments found, please create escapes manually';
    }

    public class OnbaseContentVersionConstants{
        public final String RECORD_TYPE_FOR_DOCUMENT_TYPE_BPP_STATMENTS                 = Label.DocumentTypeBPPSupportDocuments ;
        public final String RECORD_TYPE_FOR_DOCUMENT_TYPE_EX_SUPPORT                    = Label.DocumentTypeEXSupportDocuments ;
        public final String RECORD_TYPE_FOR_DOCUMENT_TYPE_BPP_INCOMING_TAXPAYER_REQUEST = Label.DocumentTypeBPPIncomingTaxpayerRequest;
        public final String DOCUMENT_TYPE_BPP_STATMENTS                                 = 'BPP-Statements-Support Document';
        public final String DOCUMENT_TYPE_EX_SUPPORT                                    = 'EX-Support Documents';
        public final String DOCUMENT_TYPE_BPP_INCOMING_TAXPAYER_REQUEST                 = 'BPP-Incoming-Taxpayer-Request';
        public final String AIMS_UPLOAD_STATUS_READY_FOR_UPLOAD                         = 'Ready to upload';
        public final String ORIGIN_LOCATION                                             = 'ContentDocumentLinkHandler';

    }

    public class PropertyEventConstants{
        public final String EXEMPTION_RECORD_TYPE_API_NAME               = 'Exemption';
        public final String APPROVAL_STATUS_COMPLETE                     = 'Complete';
        public final String SUB_STATUS_APPROVED                          = 'Approved';

    }

    public static final String NOTICE_TYPE_LOW_VALUE                     = 'Low Value';
    public static final String NOTICE_TYPE_DIRECT_BILL                   = 'Direct Bill';
    public static final String NOTICE_TYPE_NOTICE_TO_FILE                = 'Notice to File';
    public static final String NOTICE_TYPE_NOT_APPLICABLE                = 'Not Applicable';
    public static final String NOTICE_TYPE_SDR                           = 'SDR';
    public static final String NON_FILER_GENERATOR_BATCH_NAME            = 'NonFilerAssessmentBulkGenerator';
    public static final String STATUS_ACTIVE                             = 'Active';
    public static final String STATUS_INPROGRESS                         = 'In Progress';
    public static final String PENALTY_RT_CODE_463                       = '463';
    public static final String ESCAPE_TYPE_ASSESSMENT                    = 'Escape';
    public static final String VESSEL_NON_FILER_GENERATOR_BATCH_NAME     = 'NonFilerVesselAssessmentBulkGenerator';
    public static final String DIRECTBILL_VESSEL_GENERATOR_BATCH_NAME    = 'DirectVesselAssessmentGenerator';
    public static final String ESCAPE_GENERATOR_BATCH_NAME               = 'EscapeAssessmentBatchGenerator';
    public static final String REOPEN_ANNUAL_ROLL_BATCH_NAME             = 'ReOpenAnnualRollBatch';
    public static final String UPDATE_CASE_INTEGRATIONSTATUS_BATCH_NAME  = 'UpdateCaseIntegrationStatusBatch';
    public static final String UPDATE_ROLLCLOSE_CASE_STATUS_BATCH_NAME   = 'UpdateRollCloseCaseStatusBatch';
    public static final String UPDATE_PROPERTY_ASSESSED_VALUE_BATCH_NAME = 'UpdatePropertyAssessedCost';
    public static final String ROLLCORRECTION_BATCH_SYNC_BATCH_NAME      = 'RollCorrectionBatchSync';
    public static final String AUMENTUM_STATUS_NEW                       = 'New';
    public static final String AUMENTUM_STATUS_PROCESSED                 = 'Processed';
    public static final String AUMENTUM_STATUS_FAILED                    = 'Failed';
    public static final String AUMENTUM_BATCH_NAME                       = 'AumentumDataNormalizeBatch';
    public static final String READY_TO_SENT_TO_TTX                      = 'Ready to send to TTX';
    public static final String SENT_TO_TTX                               = 'Sent to TTX';
    public static final String IN_TRANSIT_TO_TTX                         = 'In Transit to TTX';
    public static final String WHITE_SPACE                               = ' ';
    public static final String AMEND_STATEMENT_LOCATION                  = 'AmendStatementController';
    public static final String ROLE_BPPCHIEF                             = 'BPP Chief';
    public static final String SYSTEMADMIN_PROFILE                       = 'System Administrator';
    public static final String SAPIENTADMIN_PROFILE                      = 'Sapient Admin';
    public static final String ASSESSMENT_AJUSTMENTS                     = 'AssessmentAdjustmentsApproval';
    public static final String TAXPAYER_PROFILE                          = 'Taxpayer';
    public static final String OWNER                          			 = 'Owner';
    public static final String INTEREST_CODE                             = 'R&T Code 506';
    public static final Integer ACCESSPIN_MAXCOUNT                       = 999999;
    public static final Integer ACCESSPIN_MINCOUNT                       = 125000;
    public static final String EXEMPTIONSTAFF_PROFILE                    = 'Exemptions Staff';
    public static final String AURA_EXCEPTION 							 = 'Script-thrown exception';
    public static final String PENALTY_RT_CODE_506                       = '506';
    public static final string typeQueue 								 = 'Queue';
    public static final string eFile 								 	 = 'eFile';
    public static final string MINIMAL_NO_VALUE_CHANGE 				     = 'Minimal No Value Change';
    public static final string Leasehold_Improvements_Structure			 = 'Leasehold Improvements - Structure';
    public static final string Prior_2013_Acquisition			         = 'Prior 2013 Acquisition (Depreciate)';
    public static final string Post_2013_Acquisition			         = '>= 2013 Acquisition (CPI)';
    public static final string Not_Assessesed_by_BPP			         = 'Not Assessed by BPP';
    public static final string Post_2020_Acquisition			         = '2020';
    public static final string customerSupportQueueName 			     = 'Customer Support';
    public static final string statementQueueDevName 					 = 'Assignment_Queue';
    public static final string errorInsertingRecord 			         = 'Error in inserting record : ';
    public static final string errorInsertingCloneRecord 			     = 'Error in inserting clone record : ';
    public static final string errorUpdatingRecord 			             = 'Error in updating record : ';
}