/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name : CustomEmailService
 * Description: This class will run in system context, and remove the approval processes when the status is 
 * 				'Closed' and substatus is 'Minimal/No value change'.
 */ 
global class CustomEmailService implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Map<String,Object> emailParams = new Map<String, Object>();
        
        emailParams = (Map<String, Object>)JSON.deserializeUntyped(email.plainTextBody); 
        
        if(emailParams.get('errorMessage') == 'No Error'){
            String workItemId = (String)emailParams.get('workItemId');
            String caseId = (String)emailParams.get('caseId');
            Approval.ProcessWorkitemRequest workItem = new Approval.ProcessWorkitemRequest(); 
            workItem.setAction('Removed');
            workItem.setWorkItemId(workItemId);
            Approval.ProcessResult processResults = Approval.process(workItem);
            
            if(String.isNotBlank(caseId)){
               updateCaseRecord(caseId);
            }
            
        }
        
        return result;
    }

	/*
	 * Method Name: updateCaseRecord
	 * Description :Reupdating the case to Closed and Cancelled as the Approval Process 
            		has recall actions, which makes the status as Inprogress.
	 */     
    @future
    private static void updateCaseRecord(Id caseId) {
        if(String.isNotBlank(caseId)){
            Case caseRecord = new Case();
            caseRecord.Id = caseId;
            caseRecord.Status = CCSFConstants.ASSESSMENT.STATUS_CLOSED;
            caseRecord.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_CANCELLED;
            database.update(caseRecord);
        }
    }
    
}