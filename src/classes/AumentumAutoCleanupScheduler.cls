/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @story number : ASR-2366 and Task ASR-9371
// @description : This class is used to schedule AumentumAutoCleanupBatch.
//-----------------------------
public with sharing class AumentumAutoCleanupScheduler extends EnhancedSchedulable {
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @description : This method override the set jobName as 'AumentumAutoCleanupScheduler' extending the method from EnhancedSchedulable 
    // @return : String
    // @ASR- 9371
    //-----------------------------
    public override String getJobNamePrefix() {
        return 'AumentumAutoCleanupScheduler';
    }  

    //-----------------------------
    // @author : Publicis Sapient 
    // @param : SchedulableContext sc
    // @description : This method takes schedule the Batch class AumentumAutoCleanupBatch
    // @return : void
    // @ASR- 9371
    //-----------------------------
    
    public void execute(SchedulableContext sc){
        try{            
            Logger.addDebugEntry('AumentumAutoCleanupBatch Batch is scheduled','AumentumAutoCleanupBatch.Schedulable Execute');
            Id batchProcessId = Database.executeBatch(new AumentumAutoCleanupBatch());
            Logger.addDebugEntry('After Scheduling batch, jobid:'+batchProcessId, 'AumentumAutoCleanupBatch.execute');
            
        }catch (Exception ex){
           Logger.addExceptionEntry(ex, 'AumentumAutoCleanupBatch.execute');
        }
        Logger.saveLog();
    }  
}