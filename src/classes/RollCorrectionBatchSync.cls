/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR-5801. The SMART system must send Roll Correction assessment data to the TaxSys system.
// This Batch job is to collect Roll Correction cases (assessment) data and send it to integration layer.
//-----------------------------
public class RollCorrectionBatchSync implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts,
												BatchableErrorHandler 
{
    private String originLocation;
    private String currentYear;
    private String previousYear;
    public RollCorrectionBatchSync(){
        this.originLocation = CCSFConstants.ROLLCORRECTION_BATCH_SYNC_BATCH_NAME;
        currentYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        previousYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // Takes all the updated cases and check the below condition before sending it to integration layer
    //Added one extra condition adjustment type = regular/escape and assessment subtype= closeout adjustment subtype = new as part of ASR-10145
    // @return : Database.QueryLocator
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext context) {
        String query='SELECT ';
    	List<Schema.FieldSetMember> fieldList=SObjectType.Case.FieldSets.TaxSysRollCorrectionIntegrationFields.getFields();
     	for(Schema.FieldSetMember f : fieldList) {
        	query = query+f.getFieldPath() +','; 
        }
        if(query.contains(',Penalty__c')) {
            query = query+'Penalty__r.RTCode__c,';
        }
        if(query.contains(',ExemptionPenalty__c')) {
            query = query+'ExemptionPenalty__r.RTCode__c,';
        }
        if(query.contains(',Roll__c')) {
            query = query+'Roll__r.Year__c,';
        }
        if(query.contains(',Appeal__c')) {
            query = query+'Appeal__r.AABFormNumber__c,';
            query = query+'Appeal__r.RefundClaimFormFiled__c,';
        }
        
        query = query.substring(0, query.length()-1);
        
        query += ' from Case where  isAssessment__c = true';
        query += ' AND Status = \''+CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED+'\'';
        query += ' AND IntegrationStatus__c = \''+CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX+'\'';
        query += 'AND ((Type=\''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\' AND AdjustmentType__c = \''+CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_ROLLCORRECTION+'\')';
		query += '	OR (Type=\''+CCSFConstants.ASSESSMENT.TYPE_ESCAPE+'\' AND SubType__c!= \''+CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT+'\')';
		query += '	OR ((Type=\''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\' OR Type=\''+CCSFConstants.ASSESSMENT.TYPE_ESCAPE+'\')';
		query += '  AND SubType__c = \''+CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT+'\' AND AdjustmentType__c= \''+CCSFConstants.ASSESSMENT.STATUS_NEW+'\')';
		query += '	OR (Type=\''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\' AND AdjustmentType__c= \''+CCSFConstants.ASSESSMENT.ADJUSTMENT_CANCEL+'\' AND Status=\''+CCSFConstants.ASSESSMENT.STATUS_CLOSED+'\' AND SubStatus__c=\''+CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED+'\')';
		query += '	OR (Type=\''+CCSFConstants.ASSESSMENT.TYPE_ESCAPE+'\' AND AdjustmentType__c= \''+CCSFConstants.ASSESSMENT.ADJUSTMENT_CANCEL+'\' AND Status=\''+CCSFConstants.ASSESSMENT.STATUS_CLOSED+'\' AND SubStatus__c=\''+CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED+'\'))';
		query += ' AND (RollYear__c = :currentYear or RollYear__c = :previousYear)';
        return Database.getQueryLocator(query);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : List<Case>
    // @description : Get all the case and prepare a JSON. Send to Integration
    // layer and check if its success or failure.
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext context, List<Case> cases) {
        boolean isCalloutSuccess = makeCallout(cases);
        Logger.addDebugEntry('RollCorrectionBatchSync callout for passing assessments Success ?'+String.valueOf(isCalloutSuccess), originLocation);
        if(isCalloutSuccess){
            //update cases with Integration status
            updateEligibleCases(cases, CCSFConstants.IN_TRANSIT_TO_TTX);
        }
        Logger.saveLog();    
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : containes code used for retry batch mechanism
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext context) {
        // Check the status of the batch and store the values for retry batch.
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
       
        DescribeUtility.createApexBatchLog(JobId, RollCorrectionBatchSync.class.getName(), asyncList[0].ParentJobId,
                                           false, '', asyncList[0].NumberOfErrors);
    }
    
    //include this method to use retry batch mechanism
    public void handleErrors(BatchableError error){}
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Case>
    // @description : gets a JWT token and send the data to Java layer.
    // 		returns if callout is failed or not.
    // @return : boolean
    //-----------------------------
    private boolean makeCallout(List<Case> cases){
        try{
            	//Can't re-use same JWT token, as generated JTI should be unique per http request
                String jwtToken = CreateJWTToken.generateJWTToken();
                Map<String,String> headers = new Map<String,String>();
                headers.put('Authorization', 'Bearer '+jwtToken);
                headers.put('Content-Type', 'application/json');
                String url = Test.isRunningTest() ? 'http://dummyClassName.com' :[Select Endpoint from NamedCredential where developerName ='SMART_Job_Taxsys_Roll_Correction_Endpoint' limit 1].Endpoint;
                Callout callout= new Callout(url).setHeaders(headers);
                HttpResponse response = callout.post(cases);
            	return response.getStatusCode()==202;
        }
        catch(Exception e){
            Logger.addExceptionEntry(e, originLocation);
        	Logger.saveLog();
            return false;
		}
    }
      
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Case>
    // @param : String
    // @description : update cases with integration status
    // @return : void
    //-----------------------------
    public static void updateEligibleCases(List<Case> cases, String integrationStatus){
        Logger.addDebugEntry(LoggingLevel.ERROR, 'updateEligibleCases method called for update Case.IntegrationStatus', 'RollCorrectionBatchSync', null);
        for(Case inputCase : cases){
            inputCase.IntegrationStatus__c = integrationStatus;
        }
        update (cases);
        Logger.saveLog();
    }   
}