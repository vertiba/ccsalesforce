/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Batch class to set Notice type on Account
//-----------------------------
public class UpdateVesselNoticeType implements Database.Batchable<sObject>, BatchableErrorHandler {
    private String lastYear;
    private String currentYear;

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Constructor method.
    //-----------------------------
    public UpdateVesselNoticeType() {
        lastYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        currentYear = String.valueOf(system.today().year());
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : Database.QueryLocator
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext context) {
        string query='SELECT Id, MostRecentAssessedValue__c, NoticeType__c FROM Property__c ';
        query += 'WHERE Status__c = \'Active\' and RecordType.DeveloperName = \'Vessel\'';

        Logger.addDebugEntry(query,'UpdateVesselNoticeType - start method query');
        Logger.saveLog();

        return Database.getQueryLocator(query);         
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : List<sObject> 
    // @description : Validating all the criterias and assigning notice type to account
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext context, List<Property__c> properties) {
        Set<Id> allPropertyIds = new Set<Id>();
        Set<Id> filteredPropertyIds = new Set<Id>();
        List<Property__C> propertiesToUpdate = new List<Property__C>();
        for(Property__c property : properties){
            allPropertyIds.add(property.Id);
        }
        
        for(Property__C propertyResult : [SELECT Id, 
                                          (SELECT Id, type, penalty__c FROM Cases__r WHERE RollYear__c =:lastYear and 
                                           type = 'Regular' and IsAssessment__c = true order by EventDate__c desc)
                                          FROM Property__c where id = :allPropertyIds]){
            List<Case> assessments = propertyResult.Cases__r;
            if(assessments.size() > 0 && assessments[0].penalty__c == null){
                //Add those properties which have Regular Case with no Penalty for last year
                filteredPropertyIds.add(propertyResult.id);
            }
        }
        
        if(!filteredPropertyIds.isEmpty()){
            for(AggregateResult aggregateResult : [select Property__r.id propertyResult from Case where Property__r.id 
                                                   in :filteredPropertyIds and (type = 'Escape' or type = 'Roll Correction') 
                                                   and RollYear__c =:lastYear and IsAssessment__c = true 
                                                   group by Property__r.id]){
                // discart those properties which have case with Escape or Roll Correction in prior year
                filteredPropertyIds.remove((Id)aggregateResult.get('propertyResult'));
            }
        }
        
        if(!filteredPropertyIds.isEmpty()){
            for(Property__C property : [SELECT Id, MostRecentAssessedValue__c 
                                        FROM Property__c where id = :filteredPropertyIds and 
                                        RequiredToFileYear__c!= :currentYear]){
                if(property.MostRecentAssessedValue__c >= 1 && 
                   property.MostRecentAssessedValue__c <=Integer.valueOf(System.Label.VesselPropertyLowCostCeilingAmount)){
                    property.NoticeType__c = 'Low Value';
                    allPropertyIds.remove(property.Id);
                    propertiesToUpdate.add(property);
                }else if(property.MostRecentAssessedValue__c > Integer.valueOf(System.Label.VesselPropertyLowCostCeilingAmount) && 
                         property.MostRecentAssessedValue__c <=Integer.valueOf(System.Label.VesselPropertyDirectBillCeilingAmount)){
                    property.NoticeType__c = 'Direct Bill';
                    allPropertyIds.remove(property.Id);
                    propertiesToUpdate.add(property);
                }
            }
        }
        
        if(!allPropertyIds.isEmpty()){
            for(property__c property : [select id, NoticeType__c from Property__c where id in :allPropertyIds]){
                property.NoticeType__c = 'Notice to File';
                propertiesToUpdate.add(property);
            }
        }
        if(!propertiesToUpdate.isEmpty()) update propertiesToUpdate;
    }

    //-----------------------------
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext context) {
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, UpdateVesselNoticeType.class.getName(), 
                                           asyncList[0].ParentJobId, false, '', asyncList[0].NumberOfErrors);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
}