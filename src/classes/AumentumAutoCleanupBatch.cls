/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR- 2366, task -8502
// @description : This Batch class is used to cleanup Address data coming from Integration layer in StagingAumentumBusiness object
//-----------------------------
public with sharing class AumentumAutoCleanupBatch implements Database.Batchable<sObject>, BatchableErrorHandler{
 
    public static final String originLocation = CCSFConstants.AUMENTUM_PROCESS.AUMENTUM_AUTO_CLEANUP_BATCH;

    public Database.QueryLocator start(Database.BatchableContext context) {
        // collect the batches of records or objects to be passed to execute
        String STAGEQUERY ='SELECT Id,ContactCity__c,ContactPostalCode__c,ContactPostDirection__c,ContactPreDirection__c,ContactState__c,';
        STAGEQUERY +='ContactStreetName__c,ContactStreetNumber__c,ContactStreetType__c,ContactUnitNumber__c,ContactUnitType__c,LocationCity__c,';
        STAGEQUERY += 'LocationPostalCode__c,LocationPostDirection__c,LocationPreDirection__c,LocationState__c,LocationStreetName__c,';
        STAGEQUERY += 'LocationStreetNumber__c,LocationStreetType__c,LocationUnitNumber__c,LocationUnitType__c,Status__c, TaxIdentificationNumber__c ';
        STAGEQUERY += ' FROM StagingAumentumBusiness__c WHERE IsDataClean__c = false And Status__c = \'New\' ';
        Logger.addDebugEntry(STAGEQUERY,originLocation+'- start method query');
        Logger.saveLog(); 
        
        return Database.getQueryLocator(STAGEQUERY);
    }

    public void execute(Database.BatchableContext context, List<StagingAumentumBusiness__c> businessRecords){
        
        try{
            // Calling controller 
            List<StagingAumentumBusiness__c> recordsToUpdate = AumentumAutoCleanupController.cleanUpRecords(businessRecords);
        
            // Dml operation
            if(!recordsToUpdate.isEmpty()){
                updateCurrentRecords(recordsToUpdate);
            }
           
        }catch(Exception ex){     
            Logger.addExceptionEntry(ex, originLocation);
            throw ex;
        }
        Logger.saveLog();
    }

    public void finish(Database.BatchableContext context){
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, AumentumAutoCleanupBatch.class.getName(), asyncList[0].ParentJobId, 
                                           false,'', asyncList[0].NumberOfErrors);     
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : List<sObject>
    // @description : This method takes List<sobject> to update and return error if record failed
    // @return : void
    //-----------------------------
    public static void updateCurrentRecords(List<sObject> recordsToBeUpdate){        
        List<String> databaseErrors = new List<String>();
        Map<String,Object> updateResult = DescribeUtility.updateRecords(recordsToBeUpdate,null);
        List<Database.SaveResult> saveUpdateResults= (List<Database.SaveResult>)updateResult.get('updateResults');        
        for(Integer i=0;i< saveUpdateResults.size();i++){
            if (!saveUpdateResults.get(i).isSuccess()) {
                Database.Error error = saveUpdateResults.get(i).getErrors().get(0);
                databaseErrors.add(String.valueof(error));
            }
        }
        Logger.addDebugEntry(String.valueof(databaseErrors), originLocation);
        Logger.saveLog();
    }
}