public with sharing class RecordDetailFieldsController {

    @AuraEnabled
    public static List<String> getFieldSetFields(Id recordId, String fieldSetName) {
        SObjectType recordSObjectType = recordId.getSObjectType();
        Schema.FieldSet fs = recordSObjectType.getDescribe().FieldSets.getMap().get(fieldSetName);
        List<String> fieldList = getFieldStringListFromFieldSet(fs);
        
        return fieldList;
    }

    static List<String> getFieldStringListFromFieldSet(Schema.FieldSet fs) {
        List<String> fieldList = new List<String>();
        
        for(Schema.FieldSetMember f : fs.getFields()) {
            fieldList.add(f.getFieldPath());
        }
        
        return fieldList;
    }

}