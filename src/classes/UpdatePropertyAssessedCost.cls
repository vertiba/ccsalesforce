/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : 
//-----------------------------
public class UpdatePropertyAssessedCost implements Database.Batchable<sObject>,Database.Stateful, BatchableErrorHandler {
    private RollYear__c currentClosedRollYear;
    public static final String originLocation = CCSFConstants.UPDATE_PROPERTY_ASSESSED_VALUE_BATCH_NAME;

    public UpdatePropertyAssessedCost() {
        
        this.currentClosedRollYear = [SELECT Id, Name, Status__c,Year__c
                                          	FROM RollYear__c 
                                            WHERE Status__c =: CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS ORDER BY Year__c DESC LIMIT 1]; 
    }
    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        String  query;
        //Buld dynamic query for batch        
        query = ' SELECT Id,Name,MostRecentAssessedValue__c,AssessedCost__c FROM Property__c ';
        query += ' WHERE Status__c = \''+CCSFConstants.STATUS_ACTIVE+'\'';	
        
        Logger.addDebugEntry(query, 'UpdatePropertyAssessedCost-start method' );
        Logger.saveLog();

        return database.getQueryLocator(query);

    }
     //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : propertyAssessedValue (List of Properties)
    // @description : This method update assessed value on property from assessments
    // @return : void
    //------------------------
    public void execute(Database.BatchableContext bc, List<Property__c> propertyAssessedValue) {
        try {
                Set<Property__c>  propertiesToUpdate = new Set<Property__c>();
                for(Property__c property : propertyAssessedValue){  
                    propertiesToUpdate.add(property);
                }
                   
           		RollYear__c lastClosedRollYear =  this.currentClosedRollYear;
                RollUpAssessedValue updateAssessedValue = new RollUpAssessedValue();
                updateAssessedValue.rollUpTotalAssessedvalues(propertiesToUpdate,lastClosedRollYear);   
           }
            catch(Exception ex) {
                Logger.addExceptionEntry(ex, originLocation);
                throw ex;
            }
        Logger.saveLog();	
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method updates the roll year record that the Direct Bill Vessel batch has been completed.
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext bc) {
        Logger.addDebugEntry('Batch Class for Update property Value', originLocation);
        Logger.saveLog();
     
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, UpdatePropertyAssessedCost.class.getName(), asyncList[0].ParentJobId, false, '', asyncList[0].NumberOfErrors);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
     
}