/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// Batch ->Generation of Missing Escape Assessments when roll year is closing
//-----------------------------

@isTest
public class EscapeAssessmentBatchGeneratorTest {
    
    
    private static Integer currentYear 		            = FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastRollYear 	            = currentYear-1;
    private static Integer lastToLastYear 	            = lastRollYear-1;
    private static Integer lastFromLastToLast			= lastRollYear-2;
    private static Integer nextYear 		            = currentYear+1;
    private static Date eventDate                       = Date.newInstance(lastRollYear, 1, 1);
    private static Date eventDateLastToLastYear         = Date.newInstance(lastToLastYear, 1, 1);
    private static Date eventDateLastFromLastToLastYear = Date.newInstance(lastFromLastToLast, 1, 1);
    
    @testSetup
    private static void dataSetup(){
        
        //insert roll years
        List<RollYear__c> rollYears = new List<RollYear__c>();
        RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYearCurrentFiscalYear.GrosslyUnderreported__c = 8;
        rollYearCurrentFiscalYear.Threshold__c = 1000;       
        rollYears.add(rollYearCurrentFiscalYear);
        
        RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastRollYear),String.valueof(lastRollYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYearLastFiscalYear.GrosslyUnderreported__c = 5;
        rollYearLastFiscalYear.Threshold__c = 3000;  
        rollYears.add(rollYearLastFiscalYear);
        
        RollYear__c rollYearLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastToLastYear),String.valueof(lastToLastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS);
        rollYearLastToLastFiscalYear.GrosslyUnderreported__c = 4;
        rollYearLastToLastFiscalYear.Threshold__c = 4000;  
        rollYears.add(rollYearLastToLastFiscalYear);
        
        RollYear__c rollYearFromLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastFromLastToLast),String.valueof(lastFromLastToLast),CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS);
        rollYearFromLastToLastFiscalYear.GrosslyUnderreported__c = 4;
        rollYearFromLastToLastFiscalYear.Threshold__c = 4000;  
        rollYears.add(rollYearFromLastToLastFiscalYear);
        
        RollYear__c rollYearNextFiscalYear = TestDataUtility.buildRollYear(String.valueof(nextYear),String.valueof(nextYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_PENDING_STATUS);
        rollYearNextFiscalYear.GrosslyUnderreported__c = 6;
        rollYearNextFiscalYear.Threshold__c = 5000;  
        rollYears.add(rollYearNextFiscalYear);
        
        insert rollYears;
        
       
        List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463); 
        penalties.add(penalty);
        
        Penalty__c penalty1 = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_506);
        penalties.add(penalty1);
        insert penalties;
        
        List<Account> accounts = new List<Account>();
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAccount = TestDataUtility.buildAccount('Johansson&Jon', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, businessAccountRecordTypeId);
        Account objAccount1 = TestDataUtility.buildAccount('Milton', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, businessAccountRecordTypeId);
        Account objAccount2 = TestDataUtility.buildAccount('Jack Jill', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, businessAccountRecordTypeId);
        Account objAccount3 = TestDataUtility.buildAccount('Mars', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, businessAccountRecordTypeId);
        Account objAccount4 = TestDataUtility.buildAccount('Juptier', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, businessAccountRecordTypeId);
        Account objAccount5 = TestDataUtility.buildAccount('Moon', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, businessAccountRecordTypeId);
        accounts.add(objAccount);
        accounts.add(objAccount1);
        accounts.add(objAccount2);
        accounts.add(objAccount3);
        accounts.add(objAccount4);
        accounts.add(objAccount5);
        insert accounts;
        
        List<Property__c> properties = new List<Property__c>();
        Id PersonalPropRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Property__c objProperty  = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount.Id, '11-123456', 'Johansson&Jon', null);
        objProperty.BusinessLocationOpenDate__c = Date.newInstance(2017, 3, 4);
        Property__c objProperty1 = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount1.Id, '11-123457', 'Milton', null);
        objProperty1.BusinessLocationOpenDate__c = Date.newInstance(2017, 3, 4);
        Property__c objProperty2 = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount2.Id, '11-123458', 'Jack Jill', null);
        objProperty2.BusinessLocationOpenDate__c = Date.newInstance(2017, 3, 4);
        Property__c objProperty3 = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount3.Id, '11-123459', 'Mars', null);
        objProperty3.BusinessLocationOpenDate__c = Date.newInstance(2017, 3, 4);
        Property__c objProperty4 = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount4.Id, '11-123450', 'Juptier', null);
        objProperty4.BusinessLocationOpenDate__c = Date.newInstance(2017, 3, 4);
        Property__c objProperty5 = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount5.Id, '11-123452', 'Moon', null);
        objProperty5.BusinessLocationOpenDate__c = Date.newInstance(2017, 3, 4);
        
        properties.add(objProperty);
        properties.add(objProperty1);
        properties.add(objProperty2);
        properties.add(objProperty3);
        properties.add(objProperty4);
        properties.add(objProperty5);
        insert properties;
        
        Date filingDate = Date.newInstance(lastRollYear, 5, 30); 
        Id BPPrecordTypeId=  Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalPropertyStatement').getRecordTypeId();
        Date startofBussinessDate = Date.newInstance(2017, 3, 4);
        List<Statement__c> statements = new List<Statement__c>();
        Statement__c statement = TestDataUtility.buildStatement(objAccount1.Id, objProperty1.Id, '571-L', 'Existing', 'Existing', String.valueof(lastRollYear), BPPrecordTypeId, startofBussinessDate, filingDate);
        Statement__c statement1 = TestDataUtility.buildStatement(objAccount.Id, objProperty.Id, '571-L', 'Existing', 'Existing', String.valueof(lastRollYear), BPPrecordTypeId, startofBussinessDate, filingDate);
        Statement__c statement2 = TestDataUtility.buildStatement(objAccount2.Id, objProperty2.Id, '571-L', 'Existing', 'Existing', String.valueof(lastRollYear), BPPrecordTypeId, startofBussinessDate, filingDate);
        Statement__c statement3 = TestDataUtility.buildStatement(objAccount3.Id, objProperty3.Id, '571-L', 'Existing', 'Existing', String.valueof(lastRollYear), BPPrecordTypeId, startofBussinessDate, filingDate);
        Statement__c statement4 = TestDataUtility.buildStatement(objAccount4.Id, objProperty4.Id, '571-L', 'Existing', 'Existing', String.valueof(lastRollYear), BPPrecordTypeId, startofBussinessDate, filingDate);
        Statement__c statement5 = TestDataUtility.buildStatement(objAccount5.Id, objProperty5.Id, '571-L', 'Existing', 'Existing', String.valueof(lastRollYear), BPPrecordTypeId, startofBussinessDate, filingDate);
        statements.add(statement);
        statements.add(statement1);
        statements.add(statement2);
        statements.add(statement3);
        statements.add(statement4);
        statements.add(statement5);
        insert statements;
    }
    
    @istest
    static void whenNoEarlierAssessmentFound(){
        // last year roll closed
        // Last year Regular        
        
        Property__c bppProperty = [Select Id,Account__c,Account__r.Name,BusinessLocationOpenDate__c from Property__c where Account__r.Name ='Milton' limit 1];
        RollYear__c closingRollYear =[Select Id, Name,Year__c,GrosslyUnderreported__c,Threshold__c,Status__c from RollYear__c where
                                      Year__c=: String.valueOf(lastRollYear) limit 1];
        
        
        List<Case> assessments = new List<Case>();
        Case assessment = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                          closingRollYear.Id,closingRollYear.Name,closingRollYear.Name,
                                                          CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        assessments.add(assessment);        
        List<AssessmentLineItem__c> LineItems= new List<AssessmentLineItem__c>();
        
        insert assessments;
        AssessmentLineItem__c objAssessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('2017', 12000.00,assessment.id);
        AssessmentLineItem__c objAssessmentLineItem2 = TestDataUtility.buildAssessmentLineItem('2016', 2000.00,assessment.id);
        LineItems.add(objAssessmentLineItem1);
        LineItems.add(objAssessmentLineItem2);        
        insert LineItems;
        
        
        closingRollYear.Status__c= CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS;
        update closingRollYear;
        test.startTest();
        EscapeAssessmentBatchGenerator batch = new EscapeAssessmentBatchGenerator(closingRollYear);
        database.executeBatch(batch); 
        test.stopTest();  
        
        List<Case> cases=[Select Id,AssessmentYear__c,Type,Billable__c,Penalty__c,TotalAssessedCost__c from Case where Property__c =: bppProperty.Id and Type ='Escape'];
        // Escape 
        Integer missingYearCount = (lastRollYear-Integer.valueof(bppProperty.BusinessLocationOpenDate__c.Year()))-1; // Minus 1 regular case        
        system.assertEquals(missingYearCount, cases.size(),'Difference between closing year and start location open'); 
        // Billable depending on Statue of Limitation 4 Years
        system.assertEquals('Yes',cases[0].Billable__c );
        
    }
    
    @isTest
    static void whenPreviousYearHasLowValueNoticeType(){  
        // 2020 - Closing Year
        // 2019- Regular with Low Value Notice Type
        
        List<RollYear__c> rollYears =[Select Id, Name,Year__c,GrosslyUnderreported__c,Threshold__c,Status__c from RollYear__c order by Name desc];
        
        Property__c bppProperty = [Select Id,Account__c,Account__r.Name,BusinessLocationOpenDate__c from Property__c where Account__r.Name ='Johansson&Jon' limit 1];
        
        Id penaltyId = [Select Id,RTCode__c  from Penalty__c where RTCode__c =: CCSFConstants.PENALTY_RT_CODE_463].Id;
        
        
        List<Case> assessments = new List<Case>();
        Case assessment = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                          rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                          CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,penaltyId,eventDate);
        assessment.ApplyPenaltyReason__c='Test Class';
        Case assessment1 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                           rollYears[3].Id,rollYears[3].Name,rollYears[3].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,3,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,eventDateLastToLastYear);
        assessment1.SubType__c = CCSFConstants.NOTICE_TYPE_LOW_VALUE;
        
        assessments.add(assessment);
        assessments.add(assessment1);        
        List<AssessmentLineItem__c> LineItems= new List<AssessmentLineItem__c>();
        test.startTest();
        insert assessments;
        AssessmentLineItem__c objAssessmentLineItem  = TestDataUtility.buildAssessmentLineItem('2015', 2000,assessment.id);           
        AssessmentLineItem__c objAssessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('2017', 1200,assessment.id);
        AssessmentLineItem__c objAssessmentLineItem2 = TestDataUtility.buildAssessmentLineItem('2017', 200,assessment1.id); 
        LineItems.add(objAssessmentLineItem);
        LineItems.add(objAssessmentLineItem1);
        LineItems.add(objAssessmentLineItem2);        
        insert LineItems;
        
        rollYears[2].Status__c='Roll Closed';
        update rollYears[2];
        
        // Calling escape Batch - When roll closed
        EscapeAssessmentBatchGenerator batch = new EscapeAssessmentBatchGenerator(rollYears[2]);
        database.executeBatch(batch); 
        test.stopTest();  
        
        // formula- Closing Year minus Start of Business Location minus Regular Case -closing year
        Integer missingYearCount = (lastRollYear-Integer.valueof(bppProperty.BusinessLocationOpenDate__c.Year()))-1;
        List<Case> cases = [Select Id,AssessmentYear__c,Type,Penalty__c,TotalAssessedCost__c from Case 
                            where Property__c =: bppProperty.Id and Type = 'Escape'];
        // Closing year - 2020
        // Start of Business -2017
        // Escape 2019(2019 regular with Low Notice Type),2018 (missing year)      
        system.assertEquals(missingYearCount, cases.size());
        
        // Escape should not have Penalty as Regular of Last to Last Year didnt have
        Case caseYearWithLowNoticeType=[SELECT Id, AssessmentYear__c, Type,Penalty__c,AdjustmentType__c
                                        FROM Case where AssessmentYear__c=: rollYears[3].Name
                                        AND Type='Escape' 
                                        AND Property__c =: bppProperty.Id 
                                        limit 1]; 
        system.assert(caseYearWithLowNoticeType.Penalty__c == null);
        system.assertEquals('New',caseYearWithLowNoticeType.AdjustmentType__c);

    }  
    
    @isTest
    static void whenCloseAndPreviousYearsAsLowValue(){
        //Closing year 2020 - Low Value
        //Previous year 2019 - Low Value
        
        List<RollYear__c> rollYears =[Select Id, Name,Year__c,GrosslyUnderreported__c,Threshold__c,Status__c from RollYear__c order by Name desc];
        
        Property__c bppProperty = [Select Id,Account__c,Account__r.Name,BusinessLocationOpenDate__c from Property__c where Account__r.Name ='Jack Jill' limit 1];
        
        
        Id penaltyId = [Select Id,RTCode__c  from Penalty__c where RTCode__c =: CCSFConstants.PENALTY_RT_CODE_463].Id;
        
        List<Case> assessments = new List<Case>();
        Case assessment = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                          rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                          CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,penaltyId,eventDate);
        assessment.ApplyPenaltyReason__c='Test Class';
        assessment.SubType__c = CCSFConstants.NOTICE_TYPE_LOW_VALUE;
        
        Case assessment1 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                           rollYears[3].Id,rollYears[3].Name,rollYears[3].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,3,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,eventDateLastToLastYear);
        assessment1.SubType__c = CCSFConstants.NOTICE_TYPE_LOW_VALUE;
        
        assessments.add(assessment);
        assessments.add(assessment1);        
        List<AssessmentLineItem__c> LineItems= new List<AssessmentLineItem__c>();
        test.startTest();
        insert assessments;
        AssessmentLineItem__c objAssessmentLineItem = TestDataUtility.buildAssessmentLineItem('2016', 2000,assessment.id); 
        AssessmentLineItem__c objAssessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('2016', 2000,assessment1.id);
        LineItems.add(objAssessmentLineItem);
        LineItems.add(objAssessmentLineItem1);             
        insert LineItems;
        
        rollYears[2].Status__c='Roll Closed';
        update rollYears[2];
        
        // Calling escape Batch - When roll closed
        EscapeAssessmentBatchGenerator batch = new EscapeAssessmentBatchGenerator(rollYears[2]);
        database.executeBatch(batch); 
        test.stopTest();  
        
        // formula- Closing Year minus Start of Business Location minus Regular Case with Low value Notice type
        Integer missingYearCount = (lastRollYear-Integer.valueof(bppProperty.BusinessLocationOpenDate__c.Year()))-2;
        List<Case> cases=[Select Id,AssessmentYear__c,Type,Penalty__c,TotalAssessedCost__c from Case where Property__c =: bppProperty.Id and Type ='Escape'];
        // No Escape should generate as closing year has regular with Low Notice Type
        // If Closing year 2020 with Regular Low Value
        // Only Missing year Escape will be generated 
        system.assertEquals(missingYearCount,cases.size());        
        
    } 
    
    
    @isTest
    static void whenPreviousHasPenalty(){
        //Closing year  - No Penalty
        //Previous year  Low Value -with Penalty
        // Escape will have Penalty
        
        List<RollYear__c> rollYears =[Select Id, Name,Year__c,GrosslyUnderreported__c,Threshold__c,Status__c from RollYear__c order by Name desc];
        
        Property__c bppProperty = [Select Id,Account__c,Account__r.Name,BusinessLocationOpenDate__c from Property__c where Account__r.Name ='Mars' limit 1];
        
        
        Id penaltyId = [Select Id,RTCode__c  from Penalty__c where RTCode__c =: CCSFConstants.PENALTY_RT_CODE_463].Id;
        
        List<Case> assessments = new List<Case>();
        Case assessment = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                          rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                          CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        
        Case assessment1 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                           rollYears[3].Id,rollYears[3].Name,rollYears[3].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,3,CCSFConstants.NOTICE_TYPE_LOW_VALUE,penaltyId,eventDateLastToLastYear);
        assessment1.SubType__c = CCSFConstants.NOTICE_TYPE_LOW_VALUE;
        assessment1.ApplyPenaltyReason__c='Test Class';
        
        assessments.add(assessment);
        assessments.add(assessment1);
        List<AssessmentLineItem__c> LineItems= new List<AssessmentLineItem__c>();
        test.startTest();
        insert assessments;
        AssessmentLineItem__c objAssessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 2000,assessment.id); 
        AssessmentLineItem__c objAssessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('2016', 2000,assessment1.id);
        LineItems.add(objAssessmentLineItem);
        LineItems.add(objAssessmentLineItem1);
        insert LineItems;
        
        rollYears[2].Status__c='Roll Closed';
        update rollYears[2];
        
        // Calling escape Batch - When roll closed
        EscapeAssessmentBatchGenerator batch = new EscapeAssessmentBatchGenerator(rollYears[2]);
        database.executeBatch(batch); 
        test.stopTest(); 
        
        List<Case> cases=[Select Id,AssessmentYear__c,Type,Penalty__c,TotalAssessedCost__c from Case 
                          where Property__c =: bppProperty.Id and Type = 'Escape' and   AssessmentYear__c =: rollYears[3].Name           
                         ];
       
        // Escape should generate with Penalty for Previous Year
        system.assert(cases[0].Penalty__c != null);
        
    } 
    
    
    @isTest
    static void valuesLessThanThresholdMarkNoticeTypeLow(){
        //Closing year 2019 
        
        List<RollYear__c> rollYears =[Select Id, Name,Year__c,GrosslyUnderreported__c,Threshold__c,Status__c from RollYear__c order by Name desc];
        
        Property__c bppProperty = [Select Id,Account__c,Account__r.Name,BusinessLocationOpenDate__c from Property__c where Account__r.Name ='Mars' limit 1];
        
        
        List<Case> assessments = new List<Case>();
        Case assessment =TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                         rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                         CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        
        assessments.add(assessment);
        List<AssessmentLineItem__c> LineItems= new List<AssessmentLineItem__c>();
        test.startTest();
        insert assessments;
        AssessmentLineItem__c objAssessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 200,assessment.id);
        AssessmentLineItem__c objAssessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('2018', 1200,assessment.id);
        LineItems.add(objAssessmentLineItem);
        LineItems.add(objAssessmentLineItem1);
        
        insert LineItems;
        
        rollYears[2].Status__c='Roll Closed';
        update rollYears[2];
        
        // Calling escape Batch - When roll closed
        EscapeAssessmentBatchGenerator batch = new EscapeAssessmentBatchGenerator(rollYears[2]);
        database.executeBatch(batch); 
        test.stopTest(); 
        
        List<Case> cases=[Select Id,AssessmentYear__c,Type,Penalty__c,TotalAssessedCost__c, SubType__c from Case 
                          where Property__c =: bppProperty.Id 
                          and Type ='Escape'];
        // TotalAssessedCost__c is low from Threshold so Escape will be Low Value as Sub type
        system.assertEquals('Low Value', cases[0].SubType__c);
        
    } 
    
    @isTest
    static void ifEscapeAlreadyExist(){
        //Closing year 2019, 2018 has 1 Escape
        
        List<RollYear__c> rollYears =[Select Id, Name,Year__c,GrosslyUnderreported__c,Threshold__c,Status__c from RollYear__c order by Name desc];
        
        Property__c bppProperty = [Select Id,Account__c,Account__r.Name,BusinessLocationOpenDate__c from Property__c where Account__r.Name ='Moon' limit 1];
        
        
        List<Case> assessments = new List<Case>();
        Case assessment = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                          rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                          CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        
        Case assessment1 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                           rollYears[3].Id,rollYears[3].Name,rollYears[3].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDateLastToLastYear);
        
        
        Case assessment2 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                           rollYears[3].Id,rollYears[3].Name,rollYears[3].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,eventDateLastToLastYear );
        
        Case assessment3 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                           rollYears[4].Id,rollYears[4].Name,rollYears[4].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDateLastFromLastToLastYear);
        
        assessments.add(assessment);
        assessments.add(assessment1);
        assessments.add(assessment2);
        assessments.add(assessment3);
        
        List<AssessmentLineItem__c> LineItems= new List<AssessmentLineItem__c>();
        
        test.startTest();
        
        insert assessments;
        
        AssessmentLineItem__c objAssessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 2200,assessment.id);
        AssessmentLineItem__c objAssessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('2016', 1200,assessment1.id);
        AssessmentLineItem__c objAssessmentLineItem2 = TestDataUtility.buildAssessmentLineItem('2016', 1200,assessment3.id);
        AssessmentLineItem__c objAssessmentLineItem3 = TestDataUtility.buildAssessmentLineItem('2016', 1200,assessment3.id);
        
        LineItems.add(objAssessmentLineItem);
        LineItems.add(objAssessmentLineItem1); 
        LineItems.add(objAssessmentLineItem2);  
        LineItems.add(objAssessmentLineItem3);              
        insert LineItems;
        
        rollYears[2].Status__c='Roll Closed';
        update rollYears[2];
        
        // Calling escape Batch - When roll closed
        EscapeAssessmentBatchGenerator batch = new EscapeAssessmentBatchGenerator(rollYears[2]);
        database.executeBatch(batch); 
        test.stopTest(); 
        
        List<Case> cases= [Select Id,AssessmentYear__c,Type,Penalty__c,TotalAssessedCost__c, NoticeType__c from Case where Property__c =: bppProperty.Id and Type ='Escape'];
        // There should not be extra Escape Generated for Previous Year
        // No Missing Year - No Escape 
        // Previous Year Regular as Well Escape
        system.assert(cases.size() != 2);
        
    } 
    
     @isTest
    static void errorFrameworkTest(){
         RollYear__c closingRollYear =[Select Id, Name,Year__c,GrosslyUnderreported__c,Threshold__c,Status__c from RollYear__c where
                                      Year__c=: String.valueOf(lastRollYear) limit 1];
        closingRollYear.Status__c= CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS;
        update closingRollYear;
        
        Id testJobId = '707S000000nKE4fIAG';
        BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
        testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testJob.JobCreatedDate__c = System.today();
        testJob.JobId__c = testJobId;
        database.insert(testJob);        
        
        BatchApexError__c testError = new BatchApexError__c();
        testError.AsyncApexJobId__c = testJobId;
        testError.BatchApexErrorLog__c = testJob.Id;
        testError.DoesExceedJobScopeMaxLength__c = false;
        testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
        testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testError.JobScope__c = closingRollYear.Id;
        testError.Message__c = 'Test exception';
        testError.RequestId__c = null;
        testError.StackTrace__c = '';
        database.insert(testError);
        
        
        BatchableError batchError = BatchableError.newInstance(testError);
        EscapeAssessmentBatchGenerator batchRetry= new EscapeAssessmentBatchGenerator(closingRollYear);
        batchRetry.handleErrors(batchError);
        system.assertEquals('707S000000nKE4fIAG', testError.AsyncApexJobId__c);
    }
    
    
}