/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR- 7736; ASR-8605
// @description : This class is used to clone statement which can be amended by taxpayer on community.
//-----------------------------
public without sharing class AmendStatementController {
    
    private Static String originLocation = CCSFConstants.AMEND_STATEMENT_LOCATION;
    private Static final String STATEMENT_QUERY= DescribeUtility.buildQueryToFetchAllDetailsOfParentandChild(Statement__c.sObjectType, StatementReportedAsset__c.sObjectType);     
    private Static final String STATEMENT_REPORTEDASSETS_QUERY=DescribeUtility.buildQueryToFetchAllDetailsOfParentandChild(StatementReportedAsset__c.sObjectType, ReportedAssetSchedule__c.sObjectType);
    public Static final String INPROGRESS = CCSFConstants.STATEMENT.STATUS_INPROGRESS;
    public Static final String IN_PROGRESS_STATEMENTS_PRESENT = CCSFConstants.STATEMENT.IN_PROGRESS_STATEMENTS_PRESENT;
    public Static final String NO_INPROGRESS_STATEMENTS = CCSFConstants.STATEMENT.NO_INPROGRESS_STATEMENTS; 
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : statementId
    // @description : This method to validates if there inprogress statements for the property
    // @return : String
    //-----------------------------    
    @AuraEnabled
    public static string validateInProgressStatements(String statementId){
        List<Statement__c> currentStatement = new List<Statement__c>();
        List<Statement__c> inProgressStatements = new List<Statement__c>();
        String response = '';
        
        if(String.isnotBlank(statementId)){
            currentStatement = [Select Id, Property__c, Form__c from Statement__c where Id=:statementId LIMIT 1];
        }
        
        if(!currentStatement.isEmpty()){
            inProgressStatements = [Select Id, Property__c, Form__c from Statement__c 
                                    where Status__c=: INPROGRESS and Property__c =: currentStatement[0].Property__c
                                    and Form__c=: currentStatement[0].Form__c 
                                    Limit 50000];
        }

        if(inProgressStatements.size() > 0){
            response = IN_PROGRESS_STATEMENTS_PRESENT;
        }else{
            response = NO_INPROGRESS_STATEMENTS;
        }
        return response;
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : none
    // @description : This method to return Current Roll Year details
    // @return : RollYear__c record
    //-----------------------------    
    @AuraEnabled(cacheable=true)
    public static RollYear__c rollYearStatus(){
        Integer currentRollYear = FiscalYearUtility.getCurrentFiscalYear();
        List<RollYear__c> rollYear = [Select Id, Year__c, Name, Status__c from RollYear__c where Name =: String.valueOf(currentRollYear) LIMIT 1];
        //check query result and return first returned result's id
        if(rollYear.size()>0)
        {
            return rollYear[0];
        }
        //default retun null
        return null;
        
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : record id String type
    // @description : This method takes recordId to be cloned and return new cloned statement
    // @return : Statement__c record 
    //-----------------------------    
    @AuraEnabled
    public static Statement__c cloneStatement(String statementIdForCloning){
        
        // variable declaration 
        Statement__c newRecord= new Statement__c();
        
        Map<String, List<StatementReportedAsset__c>> clonedStatementToLIBySourceId = new Map<String, List<StatementReportedAsset__c>>();
        Map<String, List<ReportedAssetSchedule__c>> clonedReportedAssestsBySourceId = new Map<String, List<ReportedAssetSchedule__c>>();
        List<StatementReportedAsset__c> clonedReportedAssets = new List<StatementReportedAsset__c>();
        List<ReportedAssetSchedule__c> clonedReportedAssetsSchedule = new List<ReportedAssetSchedule__c>();
        Map<String,List<ReportedAssetSchedule__c>> statementReportedAssetIdByChild = new Map<String, List<ReportedAssetSchedule__c>>();
        
        //If cloned Id is not available- return Null    
        if(String.isBlank(statementIdForCloning)){return null;}
        Savepoint tansactionSavePoint;
        try {
            tansactionSavePoint = Database.setSavepoint();
            String query = STATEMENT_QUERY + ' WHERE Id =: statementIdForCloning LIMIT 1';
            String queryReportedAssests = STATEMENT_REPORTEDASSETS_QUERY + ' WHERE Statement__c =: statementIdForCloning';
            Statement__c recordToClone = Database.query(query);
            // Clone should not be allowed  
            //Bug - 10428 - Status can be Submitted as Processed
            if(recordToClone.Status__c == CCSFConstants.STATEMENT.STATUS_INPROGRESS ){return null;}
            // If Statment is already amended then clone not allowed
            if(recordToClone.AlreadyAmended__c == true){ return null;}
            List<StatementReportedAsset__c> childAndGrandChildToBeClone = Database.query(queryReportedAssests);
            
            if(!childAndGrandChildToBeClone.isEmpty()){
                for(StatementReportedAsset__c statementReportedAssetRecords:childAndGrandChildToBeClone){
                    List<ReportedAssetSchedule__c> childReportedAssets = new List<ReportedAssetSchedule__c>();
                    if(statementReportedAssetRecords.ReportedAssetSchedules__r.isEmpty()){ continue;}
                    statementReportedAssetIdByChild.put(statementReportedAssetRecords.Id,statementReportedAssetRecords.ReportedAssetSchedules__r);
                } 
            }             
            
            newRecord = recordToClone.clone(false, true, false, false);
            // Setting fields value for cloned Statement
            newRecord = resetAmendStatementFields(newRecord);
            newRecord.Status__c = CCSFConstants.STATEMENT.STATUS_INPROGRESS;
            newRecord.AmendedFrom__c = recordToClone.Id;
            // if Statement has child
            if(!recordToClone.StatementReportedAssets__r.isEmpty()) {
                List<StatementReportedAsset__c> clonedLineItems = new List<StatementReportedAsset__c>();
                for(StatementReportedAsset__c lineItemToClone : recordToClone.StatementReportedAssets__r) {
                    // Cloning StatementReportedAsset related
                    StatementReportedAsset__c newLineItem = lineItemToClone.clone(false, true, false, false);
                    List<ReportedAssetSchedule__c> newReportedAssetSchedules = new List<ReportedAssetSchedule__c>();
                    clonedLineItems.add(newLineItem); // added Statement Reported Assets
                    if(!statementReportedAssetIdByChild.containsKey(lineItemToClone.Id)){ continue;}                                     
                    for(ReportedAssetSchedule__c reportedAssetToClone: statementReportedAssetIdByChild.get(lineItemToClone.Id)){
                        // ReportedAssetSchedule to be clone if any  
                        ReportedAssetSchedule__c newReportedAssetSchedule= reportedAssetToClone.clone(false,true,false,false);
                        newReportedAssetSchedules.add(newReportedAssetSchedule); // added Statement Reported Schduled                         	
                    }                
                    clonedReportedAssestsBySourceId.put(newLineItem.getCloneSourceId(),newReportedAssetSchedules) ;
                }
                clonedStatementToLIBySourceId.put(newRecord.getCloneSourceId(), clonedLineItems);
            } 
            
            List<Statement__c> statmentsToBeCloned= new List<Statement__c>{newRecord};
            AsyncDlrsCalculator.getInstance().setQueueToRun(false);
            // Insert statement record
            insertClonedRecords(statmentsToBeCloned);

                 
            // updating cloned record information. Once Statement is cloned, setting the AlreadyAmended Check to true.
            recordToClone.AlreadyAmended__c = true;  // The statement will not be cloned again
            List<Statement__c> statementToBeupdate = new List<Statement__c>{recordToClone};
            updateCurrentRecords(statementToBeupdate); 
            
            
            //No Child
            if(clonedStatementToLIBySourceId.isEmpty()){ return newRecord;}
            
            // Setting new cloned Statement Id to StatementReportedAssets
            if(clonedStatementToLIBySourceId.containsKey(newRecord.getCloneSourceId())){
                for(StatementReportedAsset__c lineItem : clonedStatementToLIBySourceId.get(newRecord.getCloneSourceId())) {
                    lineItem.Statement__c = newRecord.Id;
                    clonedReportedAssets.add(lineItem);
                }
            }
            
            // Inserting Child records- StatementReportedAssets
            if(clonedReportedAssets.isEmpty()){ return newRecord;}
            insertClonedRecords(clonedReportedAssets);        
            
            // If no grandChilds
            if(clonedReportedAssestsBySourceId.isEmpty()){ return newRecord;}
            
            // Setting new cloned StatementReportedAsset Ids to ReportedAssetSchedule
            for(StatementReportedAsset__c lineItem: clonedReportedAssets){
                if(!clonedReportedAssestsBySourceId.containsKey(lineItem.getCloneSourceId())){ continue;}
                for(ReportedAssetSchedule__c reportedAssetScheduleRecord: clonedReportedAssestsBySourceId.get(lineItem.getCloneSourceId()) ){
                    reportedAssetScheduleRecord.StatementReportedAsset__c = lineItem.Id;
                    clonedReportedAssetsSchedule.add(reportedAssetScheduleRecord);
                }  
            }
            
            // Inserted Grand Childs- ReportedAssetReported 
            if(clonedReportedAssetsSchedule.isEmpty()){ return newRecord;}
            insertClonedRecords(clonedReportedAssetsSchedule);        
            AsyncDlrsCalculator.getInstance().setQueueToRun(true);
        } catch (Exception ex) {
           
            Database.rollback(tansactionSavePoint);
            Logger.addExceptionEntry(ex, originLocation);
            Logger.saveLog();           
            throw new AuraHandledException(CCSFConstants.STATEMENT.AMEND_STATEMENT_DATABASE_ERROR); 
        } 
       
        return newRecord;        
    }
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : List<sObject>
    // @description : This method takes List<sobject> to insert and return error if record failed
    // @return : void
    //-----------------------------
    public static void insertClonedRecords(List<sObject> recordsToBeInserted){        
        List<String> databaseErrors = new List<String>();
        Map<String,Object> insertResult = DescribeUtility.insertRecords(recordsToBeInserted,null);
        List<Database.SaveResult> saveInsertResult= (List<Database.SaveResult>)insertResult.get('insertResults');
        for(Integer i=0;i< saveInsertResult.size();i++){
            if (!saveInsertResult.get(i).isSuccess()) {
                Database.Error error = saveInsertResult.get(i).getErrors().get(0);
                databaseErrors.add(String.valueof(error));
            }
        }
        if(databaseErrors.size() >0 ){  
            Logger.addDebugEntry(String.valueof(databaseErrors), originLocation);
            Logger.saveLog();        
            throw new AuraHandledException(CCSFConstants.STATEMENT.AMEND_STATEMENT_DATABASE_ERROR);
        }
        
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : List<sObject>
    // @description : This method takes List<sobject> to update and return error if record failed
    // @return : void
    //-----------------------------
    public static void updateCurrentRecords(List<sObject> recordsToBeUpdate){        
        List<String> databaseErrors = new List<String>();
        Map<String,Object> updateResult = DescribeUtility.updateRecords(recordsToBeUpdate,null);
        List<Database.SaveResult> saveUpdateResults= (List<Database.SaveResult>)updateResult.get('updateResults');        
        for(Integer i=0;i< saveUpdateResults.size();i++){
            if (!saveUpdateResults.get(i).isSuccess()) {
                Database.Error error = saveUpdateResults.get(i).getErrors().get(0);
                databaseErrors.add(String.valueof(error));
            }
        }
      
        if(databaseErrors.size() >0 ){     
            Logger.addDebugEntry(String.valueof(databaseErrors), originLocation);
            Logger.saveLog();      
            throw new AuraHandledException(CCSFConstants.STATEMENT.AMEND_STATEMENT_DATABASE_ERROR);
        }
        
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : Statement__c record
    // @description : This method takes new record which is to be cloned and set field set fields as null
    // and false based on type and returns that record
    // @return : Statement__c record
    //-----------------------------
    public static Statement__c resetAmendStatementFields(Statement__c newClonedRecord){
        
        List<Schema.FieldSetMember> statementAmendFieldSetMembers= DescribeUtility.getFieldSet('ResetAmendedStatementFields', 'Statement__c');
        for(Schema.FieldSetMember fieldSetMember: statementAmendFieldSetMembers){
            
            if(String.valueof(fieldSetMember.getType()) != 'BOOLEAN'){
                newClonedRecord.put(String.valueof(fieldSetMember.getSObjectField()),null);
            }else{
                newClonedRecord.put(String.valueof(fieldSetMember.getSObjectField()),false);
            }   
        }
        return newClonedRecord;
    }
    
}