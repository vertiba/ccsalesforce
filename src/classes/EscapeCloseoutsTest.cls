//Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Test Class for Methods of CaseHandler Method for Closeouts
//-----------------------------
@isTest
public class EscapeCloseoutsTest {
    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
    private static Integer nextYear 		= currentYear+1;
    
    @testSetup
    private static void dataSetup(){
        
        Id systemAdminProfileId = [Select Id from Profile where Name='System Administrator'].Id;
        User managerUser = TestDataUtility.buildUser('Manager','manager@test.com','manag',systemAdminProfileId,null);
        insert managerUser;
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        systemAdmin.ManagerId = managerUser.Id;
        users.add(systemAdmin);       
        insert users;
        
        //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        Property__c property = TestDataUtility.getVesselProperty();
        property.Account__c = businessAccount.Id;
        property.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        property.Status__c= CCSFConstants.STATUS_ACTIVE;
        insert property;
        
        Property__c property1 = TestDataUtility.getBPPProperty();
        property1.Account__c = businessAccount.Id;
        property1.Status__c= CCSFConstants.STATUS_ACTIVE;
        insert property1;
        
        
        String fiscalYear = String.valueof(currentYear);
        Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear), 5, 30);
        String prevfiscalYear = String.valueof(lastYear);
        
        RollYear__c prevRollYear = new RollYear__c();
        prevRollYear.Name = prevfiscalYear;
        prevRollYear.Year__c = prevfiscalYear;
        prevRollYear.Status__c='Roll Closed';
        insert prevRollYear;
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = fiscalYear;
        rollYear.Year__c = fiscalYear;
        rollYear.Status__c='Roll Open';
        insert rollYear;
        
        
        Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,
                                                           null,null,null,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(fiscalYear), 01, 01));
        assessment.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT;
        assessment.AccountId = property.account__c;
        assessment.Property__c = property.Id;
        assessment.SequenceNumber__c = 1;     
        assessment.NumberOfMonthsFor506Interest__c = 4;              
        insert assessment;       
        Case assessment1  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,
                                                            prevRollYear.Id,prevRollYear.Name,prevRollYear.Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(prevfiscalYear), 01, 01));
        assessment1.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT;
        assessment1.ParentRegularAssessment__c =assessment.id;
        assessment1.AccountId = property.account__c;
        assessment1.Property__c = property.Id;
        assessment1.SequenceNumber__c = 1;     
        assessment1.NumberOfMonthsFor506Interest__c = 4;              
        insert assessment1;  
        System.runAs(systemAdmin){ 
        Approval.ProcessSubmitRequest req =new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval via form');
        req.setObjectId(assessment.Id);            
        //Step 3:  Submit on behalf of a specific submitter
        req.setSubmitterId(UserInfo.getUserId());            
        //Step 4:  Submit the record to specific process and skip the criteria evaluation
        req.setProcessDefinitionNameOrId('BPP_Assessment_Route_to_Manager');
        req.setSkipEntryCriteria(true) ;
      
        Approval.ProcessResult result = Approval.process(req);  
        }    
        
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Regular Closeout is Submitted For Approval
    //-----------------------------
    @isTest
    private static void ecapesApprovals() { 
        List<ProcessInstanceWorkitem> processInstanceRecords = new List<ProcessInstanceWorkitem>();
        String currentfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear());  
        
        Test.startTest();
        
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,TotalPersonalPropertyValueOverride__c,
                                  TotalAssessedCost__c,CaseNumber,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE Type =: 'Regular' 
                                  ORDER BY SequenceNumber__c DESC];  
        
        processInstanceRecords =[SELECT id,actorId,ProcessInstance.TargetObjectId
                                 FROM ProcessInstanceWorkitem
                                 Where ProcessInstance.TargetObjectId =: assessments[0].id];
        system.assertNotEquals(0, processInstanceRecords.size());
        Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
        req1.setComments('Approving request.');
        req1.setAction('Approve');
        req1.setWorkitemId(processInstanceRecords[0].Id);
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        
        System.runAs(systemAdminUser){ 
        Approval.ProcessResult result1 = Approval.process(req1);   
        }    
        Test.stopTest();
        List<Case> validateTest = [Select Id,Status,SubStatus__c,isLocked__c FROM CASE where id =:assessments[0].id OR ParentRegularAssessment__c =: assessments[0].id];       
        Boolean isClosed= false;
        for(Case cs :validateTest){
            if(cs.status == 'Closed'){
                isClosed= true;
            }else{
                isClosed = false;
            }
        }
        System.assertEquals(true, isClosed);
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : get Approvers(User Utility Method)Regular Closeout is Submitted For Approval
    //-----------------------------
    @isTest
    private static void getApproversTest() { 
       
        List<ProcessInstanceWorkitem> processInstanceRecords = new List<ProcessInstanceWorkitem>();
        String currentfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear());  
        
        Test.startTest();
        
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,TotalPersonalPropertyValueOverride__c,
                                  TotalAssessedCost__c,CaseNumber,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE Type =: 'Regular' 
                                  ORDER BY SequenceNumber__c DESC];  
        Set<Id> Ids = new Set<Id>();
        for(Case cs: assessments){
            Ids.add(cs.id);
        }
    	Map<Id,Id> returnApprover = UserUtility.getApprovers(Ids);
        system.assertNotEquals(null, returnApprover.get(assessments[0].id));            
        Test.stopTest();       
    }    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : get Approver Method for Escape closeout which are not sent to actual approval process
    //-----------------------------
    @isTest
    private static void getApproversForEscapesCloseout() { 
       
        List<ProcessInstanceWorkitem> processInstanceRecords = new List<ProcessInstanceWorkitem>();
        String currentfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear());  
        
        Test.startTest();
       
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,TotalPersonalPropertyValueOverride__c,
                                  TotalAssessedCost__c,CaseNumber,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE Type =: 'Escape' 
                                  ORDER BY SequenceNumber__c DESC];  
        Set<Id> Ids = new Set<Id>();
        for(Case cs: assessments){
            Ids.add(cs.id);
        }
        //Covered if there is no Process Instance record present for the assessment Id
    	Map<Id,Id> returnApprover = UserUtility.getApprovers(Ids);
        system.assertEquals(null, returnApprover.get(assessments[0].id));            
        Test.stopTest();       
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Regular Closeout is Submitted For Approval
    //-----------------------------
    @isTest
    private static void ecapesEditedWhileParentIsInAproval() {
        
        Id profileId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
        Id profileBPPId = [SELECT Id FROM Profile WHERE Name='BPP Staff'].Id;
        Id cheifRoleId = [select Id,Name from UserRole where Name='BPP Chief'].Id;
        Id auditorRoleId= [select Id,Name from UserRole where Name='BPP Auditor'].Id;
        User mang = TestDataUtility.buildUser('Testing1', 'newuser@testccs1.com', 'newUse1', profileId, cheifRoleId);
        insert mang;
        User user1 = TestDataUtility.buildUser('Testing', 'newuser@testccs.com', 'newUser', profileBPPId, auditorRoleId);
        user1.ManagerID = mang.id;
        insert user1;
        
        List<ProcessInstanceWorkitem> processInstanceRecords = new List<ProcessInstanceWorkitem>();
        String currentfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear());  
         List<Case> assessments =new  List<Case>();
        
        Test.startTest();
        system.runAs(user1){
            assessments 	=[SELECT Property__c,OwnerId,TotalAssessedValue__c,AdjustmentReason__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,TotalPersonalPropertyValueOverride__c,
                                      TotalAssessedCost__c,IsLocked__c,ParentRegularAssessment__c,CaseNumber,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                      FROM Case 
                                      WHERE Type =: 'Escape' 
                                      ORDER BY SequenceNumber__c DESC];  
           Map<Id,Case>  oldAssessment = new Map<Id,Case>();
            oldAssessment.put(assessments[0].Id,assessments[0]);
            CaseHandler cs = new CaseHandler();
            cs.escapeCloseoutorRollCorrectionLocked(assessments,oldAssessment,true);
        }       
        Test.stopTest();
        Case validateTest = [Select Id,Status,SubStatus__c,isLocked__c,ParentRegularAssessment__c FROM CASE where id =:assessments[0].id];       
      	//Parent Record is in Approval
        System.assertEquals(true, Approval.isLocked(validateTest.ParentRegularAssessment__c));
    }
}