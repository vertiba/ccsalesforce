/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR-2366
// @description : This class is used to Trigger Handler
//-----------------------------
public with sharing class StagingAumentumBusinessHandler extends TriggerHandler {
    
    
    protected override void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById){
        List<StagingAumentumBusiness__c> businessRecords = (List<StagingAumentumBusiness__c>)updatedRecords;
        Map<Id,StagingAumentumBusiness__c> oldAumetumRecordsById=(Map<Id,StagingAumentumBusiness__c>)oldRecordsById;
        if(!system.isBatch()){
            // No need to call if Batch is running
            this.statusChanged(businessRecords, oldAumetumRecordsById);        
            this.createNewAccountOrProperty(businessRecords,oldAumetumRecordsById);   
        }    
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : List<StagingAumentumBusiness__c>,Map<Id, StagingAumentumBusiness__c>
    // @description : This method update the status to Ignore and Reprocessed based on UserAction selection 
    // @return : void
    //-----------------------------
    private void statusChanged(List<StagingAumentumBusiness__c> recordToupdate,Map<Id, StagingAumentumBusiness__c> oldAumetumRecordsById){
        for(StagingAumentumBusiness__c aumentum: recordToupdate ){
            if(aumentum.UserAction__c == 'Ignore Warning' && oldAumetumRecordsById.get(aumentum.Id).UserAction__c !='Ignore Warning'){
                aumentum.Status__c='Reprocessed';                
            }else if(aumentum.UserAction__c == 'Ignore Record' && oldAumetumRecordsById.get(aumentum.Id).UserAction__c !='Ignore Record'){
                aumentum.Status__c='Ignored';            
            }
        }
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : List<StagingAumentumBusiness__c>,Map<Id, StagingAumentumBusiness__c> 
    // @description : This method for Creation of Account and Property based on user action on checkbox and return Failed Status if 
    //                 insert failed, else return processed status and locked the record.
    // @return : Void
    //-----------------------------

    private void createNewAccountOrProperty(List<StagingAumentumBusiness__c> businessRecords,Map<Id, StagingAumentumBusiness__c> oldAumetumRecordsById) {
        // Variable for Inserts
        Map<String,Account> accountToInsert = new Map<String,Account> ();      
        Map<String,Property__c> propertyToInsert= new Map<String,Property__c>();     
        
        // Used for tracking errors on StagingAumentumBusiness__c, Account and Property__c records
        // This determines if a StagingAumentumBusiness__c is flagged as 'processed' or 'failed'      
        Map<String,List<String>> errorsByKey = new Map<String,List<String>>();
        
        
        // Compare/Search Keys
        Set<String> keyByBAN = new Set<String>();
        Set<String> keyByFEIN = new Set<String>();
        Set<String> keyByName = new Set<String>();
        Set<String> keyByLIN = new Set<String>();
        Set<String> keyByDBA = new Set<String>();
        Set<String> keyBySitusAddress = new Set<String>(); 
        boolean createPropertyWithNewAccount = false;
        
        //for storing compare results
        Map<String,List<Account>> accountByCompositeKeys = new Map<String,List<Account>>();
        Map<String,List<Property__c>> locationByCompositeKeys = new Map<String,List<Property__c>>();  
        try{
        for(StagingAumentumBusiness__c record: businessRecords){
            
            if(String.isNotBlank(record.BusinessAccountNumber__c)){
                keyByBAN.add(record.BusinessAccountNumber__c); // added BAN for Compare with Account
            }
            if(String.isNotBlank(record.TaxIdentificationNumber__c)){
                keyByFEIN.add(record.TaxIdentificationNumber__c); // added FEIN for Compare with Account
            }
            if(String.isNotBlank(record.BusinessName__c)){
                keyByName.add(record.BusinessName__c); // added Business Name
            }
            if(String.isNotBlank(record.LocationIdentificationNumber__c)){
                keyByLIN.add(record.LocationIdentificationNumber__c); // added LIN for Compare with Property
            }
            if(String.isNotBlank(record.TradeName__c)){
                keyByDBA.add(record.TradeName__c); // added DBA Name
            }
            if(String.isNotBlank(record.PropertyStageAddressComp__c)){
                keyBySitusAddress.add(record.PropertyStageAddressComp__c); // added Situs Address for compare with Location Address of Property
            }                    
        }
        
        // get Account data for Search        
        accountByCompositeKeys= AumentumDataProccess.getAllAccountForCompare(keybyBAN, keyByFEIN, keyByName);        
        // get Property data for Search
        locationByCompositeKeys=AumentumDataProccess.getAllLocationForCompare(keyByLIN, keyByDBA, keyBySitusAddress); 
        
        for(StagingAumentumBusiness__c record:businessRecords){
            // Keys to compare
            String compositeKey= record.BusinessAccountNumber__c+'-'+record.TaxIdentificationNumber__c+'-'+record.BusinessName__c+'-'+record.LocationIdentificationNumber__c+'-'+record.TradeName__c+'-'+record.PropertyStageAddressComp__c ;    
            String accountCompositeKey=record.BusinessAccountNumber__c+'-'+record.TaxIdentificationNumber__c+'-'+record.BusinessName__c;
            
            if(record.CreateNewProperty__c && !oldAumetumRecordsById.get(record.Id).CreateNewProperty__c){
                // create property is checked
                if(locationByCompositeKeys.containsKey(compositeKey)){ 
                    // Record Exist
                    List<Property__c> matchedProperties= locationByCompositeKeys.get(compositeKey);                        
                    if(matchedProperties.size() == 1){
                      // Unique record exist, Map it                       
                        record.MappedProperty__c = locationByCompositeKeys.get(compositeKey)[0].Id;
                        record.MappedAccount__c = locationByCompositeKeys.get(compositeKey)[0].Account__c;
                        record.Status__c= CCSFConstants.AUMENTUM_STATUS_PROCESSED;
                        record.IsLocked__c = true;
                        record.Messages__c ='';
                    }else if(matchedProperties.size() > 1){
                        // Multiple Property exist. 
                        record.addError(CCSFConstants.AUMENTUM_PROCESS.MESSAGE_TRIGGER_MUTIPLE_PROPERTY_EXIST);
                    }                   
                } else if(!locationByCompositeKeys.containsKey(compositeKey)){
                    // Allow Insert
                    if(String.isNotBlank(record.MappedAccount__c) && String.isNotEmpty(record.MappedAccount__c)){  
                        // Mapped Account will be Property Parent
                        Property__c newProperty=AumentumDataProccess.populateProperty(record,record.MappedAccount__c);
                        propertyToInsert.put(compositeKey,newProperty);                       
                    }else{
                        if(accountByCompositeKeys.containsKey(accountCompositeKey)){                       
                            // Checking if Account already exist in SMART
                            List<Account> matchAccount=accountByCompositeKeys.get(accountCompositeKey);                          
                            if(matchAccount.size() == 1){// Only Unique records 
                                record.MappedAccount__c = accountByCompositeKeys.get(accountCompositeKey)[0].Id;
                                Property__c newProperty=AumentumDataProccess.populateProperty(record,accountByCompositeKeys.get(accountCompositeKey)[0].Id);
                                propertyToInsert.put(compositeKey,newProperty);                              
                            }else if(matchAccount.size()>1){
                                // If more records are there and user is trying to create show error 
                                record.addError(CCSFConstants.AUMENTUM_PROCESS.MESSAGE_TRIGGER_MUTIPLE_ACCOUNT_EXIST);
                            }
                        }else if(!accountByCompositeKeys.containsKey(accountCompositeKey)){
                            // Allow insert                        
                            Account newAccount= AumentumDataProccess.populateAccount(record);                            
                            accountToInsert.put(accountCompositeKey,newAccount);                       
                                         
                        }                        
                    }
                }
            }
           if(record.CreateNewAccount__c && !oldAumetumRecordsById.get(record.Id).CreateNewAccount__c){
                // Create Account is checked
                if(accountByCompositeKeys.containsKey(accountCompositeKey)){
                    List<Account> matchAccount=accountByCompositeKeys.get(accountCompositeKey);
                    if(matchAccount.size() == 1){
                        // Map unique record
                        record.MappedAccount__c = accountByCompositeKeys.get(accountCompositeKey)[0].Id;
                        record.Status__c = CCSFConstants.AUMENTUM_PROCESS.STATUS_WARNING; 
                        record.Messages__c =CCSFConstants.AUMENTUM_PROCESS.MESSAGE_CREATE_PROPERTY +CCSFConstants.AUMENTUM_PROCESS.MESSAGE_ADD_NEXT_LINE;                
                    }else if(matchAccount.size()>1){
                        // Multiple Account exist
                        record.addError(CCSFConstants.AUMENTUM_PROCESS.MESSAGE_TRIGGER_MUTIPLE_ACCOUNT_EXIST);
                    }
                }else if(!accountByCompositeKeys.containsKey(accountCompositeKey)){
                    // Allow insert
                    Account newAccount=AumentumDataProccess.populateAccount(record);
                    accountToInsert.put(accountCompositeKey,newAccount);  
                }
            }            
        }
       // DML
        if(!accountToInsert.isEmpty()){
            List<Database.SaveResult> insertAccountResults = Database.insert(accountToInsert.values(),false);
            for(Integer i = 0; i < insertAccountResults.size(); i++){
                if(insertAccountResults.get(i).isSuccess()) continue;
                // Storing Error 
                Database.Error error = insertAccountResults.get(i).getErrors().get(0);
                List<String> errors= new List<String>();
                errors.add(error.getMessage()); 
                errorsByKey.put(new List<String>(accountToInsert.keySet())[i],errors);   
            }
        } 
        if(!accountToInsert.isEmpty()  ){
            // When new Account need to created and property is checked- populate property list
            for(StagingAumentumBusiness__c record: businessRecords){
                if(record.CreateNewProperty__c && !oldAumetumRecordsById.get(record.Id).CreateNewProperty__c){
                String accountCompositeKey=record.BusinessAccountNumber__c+'-'+record.TaxIdentificationNumber__c+'-'+record.BusinessName__c; 
                String compositeKey= record.BusinessAccountNumber__c+'-'+record.TaxIdentificationNumber__c+'-'+record.BusinessName__c+'-'+record.LocationIdentificationNumber__c+'-'+record.TradeName__c+'-'+record.PropertyStageAddressComp__c ;     
                if(accountToInsert.containsKey(accountCompositeKey)){                   
                    Property__c newPropertyRecord = AumentumDataProccess.populateProperty(record,accountToInsert.get(accountCompositeKey).Id);  
                    propertyToInsert.put(compositeKey,newPropertyRecord);
                }                         
                
                }
            } 
        }        
      
        if(!propertyToInsert.isEmpty()){
            List<Database.SaveResult> insertPropertyResults = Database.insert(propertyToInsert.values(),false);
            for(Integer i = 0; i < insertPropertyResults.size(); i++){
                if(insertPropertyResults.get(i).isSuccess()) continue;
                Database.Error error = insertPropertyResults.get(i).getErrors().get(0);
                // Storing Error
                List<String> errors= new List<String>();
                errors.add(error.getMessage()); 
                errorsByKey.put(new List<String>(propertyToInsert.keySet())[i],errors);                  
            }
        }  
       for(StagingAumentumBusiness__c record:businessRecords){
            // Compare key
            String compositeKey= record.BusinessAccountNumber__c+'-'+record.TaxIdentificationNumber__c+'-'+record.BusinessName__c+'-'+record.LocationIdentificationNumber__c+'-'+record.TradeName__c+'-'+record.PropertyStageAddressComp__c ;    
            String accountCompositeKey=record.BusinessAccountNumber__c+'-'+record.TaxIdentificationNumber__c+'-'+record.BusinessName__c;           
            
            if(accountToInsert.containsKey(accountCompositeKey)){
                // New Account Mapping
                record.MappedAccount__c = accountToInsert.get(accountCompositeKey).Id;   
                if(record.MappedProperty__c == null){
                    record.Status__c = CCSFConstants.AUMENTUM_PROCESS.STATUS_WARNING;                  
                    record.Messages__c =CCSFConstants.AUMENTUM_PROCESS.MESSAGE_CREATE_PROPERTY +CCSFConstants.AUMENTUM_PROCESS.MESSAGE_ADD_NEXT_LINE;                
                }                    
                if(errorsByKey.containsKey(accountCompositeKey)){
                        record.Status__c =CCSFConstants.AUMENTUM_STATUS_FAILED;
                        record.Messages__c = String.valueof(errorsByKey.get(accountCompositeKey)) + CCSFConstants.AUMENTUM_PROCESS.MESSAGE_ADD_NEXT_LINE;                       
                }        
            }
         
            if(propertyToInsert.containsKey(compositeKey)){   
                // New Property Mapping              
                record.MappedProperty__c =propertyToInsert.get(compositeKey).Id;
                if(record.MappedAccount__c == null){
                    record.MappedAccount__c= propertyToInsert.get(compositeKey).Account__c;                    
                }               
                if(propertyToInsert.get(compositeKey).Id != null && propertyToInsert.get(compositeKey).Account__c != null ){
                    record.Status__c =CCSFConstants.AUMENTUM_STATUS_PROCESSED;
                    record.isLocked__c= true;
                    record.Messages__c ='';
                }else{        
                    // Status failed if we have errors                     
                    if(errorsByKey.containsKey(compositeKey)){
                        record.Status__c =CCSFConstants.AUMENTUM_STATUS_FAILED;
                        record.Messages__c += String.valueof(errorsByKey.get(compositeKey)) + CCSFConstants.AUMENTUM_PROCESS.MESSAGE_ADD_NEXT_LINE;                       
                    }                     
                }                
            }
        }  
    }catch(Exception ex){
        logger.addExceptionEntry(ex, 'Staging trigger');
       
    }   
    }     
}