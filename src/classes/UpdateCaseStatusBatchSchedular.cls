/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR-5669. This Schedulable class is used to Schedule the UpdateCaseIntegrationStatusBatch job
//-----------------------------
public class UpdateCaseStatusBatchSchedular extends EnhancedSchedulable {

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : SchedulableContext
    // @description : Returns the Job name prefix
    // @return : String
    //-----------------------------
    public override String getJobNamePrefix() {
        return 'UpdateCaseIntegrationStatusBatch Schedular';
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : SchedulableContext
    // @description : Schedulable interface method. Schedules the UpdateCaseIntegrationStatusBatch job
    // @return : void
    //-----------------------------
    public void execute(SchedulableContext schedulableContext) {
        // Salesforce has a limit of 5 running batch jobs
        // If there are already 5 jobs running, then don't run this job
        // Any records that need to be processed will be processed the next time the job executes
        if(EnhancedSchedulable.getNumberOfRunningBatchJobs() >= 5) return;
        
        Logger.addDebugEntry('After Entering UpdateCaseStatusBatchSchedular:', 'UpdateCaseStatusBatchSchedular.execute');
        Id batchProcessId = Database.executebatch(new UpdateCaseIntegrationStatusBatch(), Integer.valueOf(Label.UpdateCaseIntegrationStatusBatchSize));
        Logger.addDebugEntry('After Scheduling bacth, jobid:'+batchProcessId, 'UpdateCaseStatusBatchSchedular.execute');
        Logger.saveLog();
    }

}