/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Trigger handler class for updating Content Version details- ASR 8201
//    
//-----------------------------
public without sharing class ContentDocumentLinkHandler extends TriggerHandler {
    
    public final String ORIGIN_LOCATION = CCSFConstants.ONBASE_CONTENTVERSION.ORIGIN_LOCATION;

    protected override void executeAfterInsert(List<SObject> newRecords, Map<Id, SObject> newRecordsById) {       
        Profile taxPayerProfile  = Profiles.TAXPAYER; 
        if(UserInfo.getProfileId() == taxPayerProfile.id || Test.isRunningTest()) {         
            // Run only for Taxpayer profile 
            List<ContentDocumentLink> contentDocumentLinkCreated = (List<ContentDocumentLink>) newRecords;           
            this.setDocumentTypeForOnbaseArchiving(contentDocumentLinkCreated);  
        }
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : 
    // @description : This method takes 
    // @return : void
    // @ASR-8201 - Provide Attachments for Archiving in OnBase (Flow 1)
    //-----------------------------
    
    private void setDocumentTypeForOnbaseArchiving(List<ContentDocumentLink> newContentDocumentLink){
        
        Set<Id> contentDocumentIds= new Set<Id>();       
        Map<String,String> devolperNameByContentDocumentId = new Map<String,String>();
        List<ContentVersion> updatedRecords= new List<ContentVersion>();
        
        try{

            for(contentDocumentlink contentDocumentLink: newContentDocumentLink){
                contentDocumentIds.add(contentDocumentLink.ContentDocumentId); // All ContentDcoumentLink Ids
            }
            
            for(ContentDocumentLink contentDocumentlink : [SELECT ContentDocumentId, LinkedEntityId,Visibility,LinkedEntity.RecordType.DeveloperName
                                                           FROM ContentDocumentLink 
                                                           WHERE ContentDocumentId IN: contentDocumentIds] ) {        
                if(contentDocumentlink.LinkedEntityId.getsobjecttype() == Statement__c.sObjectType ||contentDocumentlink.LinkedEntityId.getsobjecttype() == Case.sObjectType ) {
                    // allow ContentDocumentId for Statement and Case Object type
                    if(String.isNotBlank(contentDocumentlink.LinkedEntity.RecordType.DeveloperName)){
                        // DeveloperName of Linked Entity should not blank
                        devolperNameByContentDocumentId.put(contentDocumentlink.ContentDocumentId,contentDocumentlink.LinkedEntity.RecordType.DeveloperName); 
                    }
                }
            }                
            for(ContentVersion contentversion : [SELECT Id,AIMSUploadStatus__c,ContentDocumentId,DocumentType__c 
                                                 FROM ContentVersion 
                                                 WHERE ContentDocumentId In: devolperNameByContentDocumentId.keySet()]){
                // Content version status and document type update
                contentversion.AIMSUploadStatus__c= CCSFConstants.ONBASE_CONTENTVERSION.AIMS_UPLOAD_STATUS_READY_FOR_UPLOAD;
                contentversion.DocumentType__c= devolperNameByContentDocumentId.containsKey(contentversion.ContentDocumentId)? this.returnDocumentType(devolperNameByContentDocumentId.get(contentversion.ContentDocumentId)):CCSFConstants.WHITE_SPACE;
                updatedRecords.add(contentversion);
            }          
            if(!updatedRecords.isEmpty()){
              //DML
                updateCurrentRecords(updatedRecords);          
            }
            
        } catch(Exception ex){
            logger.addExceptionEntry(ex, ORIGIN_LOCATION);
        }  
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String
    // @description : This method takes Developername of recordtype and return correct DocumentType
    // @return : String
    // @ASR-8201
    //-----------------------------
    public String returnDocumentType(String recordTypeDeveloperName){       
        
        String documentType;
        String[] bppStatementSupport = CCSFConstants.ONBASE_CONTENTVERSION.RECORD_TYPE_FOR_DOCUMENT_TYPE_BPP_STATMENTS.split(',');
        String[] exSupport = CCSFConstants.ONBASE_CONTENTVERSION.RECORD_TYPE_FOR_DOCUMENT_TYPE_EX_SUPPORT.split(',');
        String[] bppIncomingRequest = CCSFConstants.ONBASE_CONTENTVERSION.RECORD_TYPE_FOR_DOCUMENT_TYPE_BPP_INCOMING_TAXPAYER_REQUEST.split(',');
        
        if(bppStatementSupport.contains(recordTypeDeveloperName)){
            // Setting Docment Type as 'BPP-Statements-Support Document'
            documentType= CCSFConstants.ONBASE_CONTENTVERSION.DOCUMENT_TYPE_BPP_STATMENTS;
        }else if(exSupport.contains(recordTypeDeveloperName)){
            // Setting Docment Type as 'EX-Support Documents'
            documentType= CCSFConstants.ONBASE_CONTENTVERSION.DOCUMENT_TYPE_EX_SUPPORT;
        }else if(bppIncomingRequest.contains(recordTypeDeveloperName)){
            // Setting Docment Type as 'BPP-Incoming-Taxpayer-Request'
            documentType= CCSFConstants.ONBASE_CONTENTVERSION.DOCUMENT_TYPE_BPP_INCOMING_TAXPAYER_REQUEST;
        }else{
            // Setting blank
            documentType=CCSFConstants.WHITE_SPACE;
        }
        return documentType;
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : List<sObject>
    // @description : This method takes List<sobject> to update and return error if record failed
    // @return : void
    //-----------------------------
    public static void updateCurrentRecords(List<sObject> recordsToBeUpdate){        
        List<String> databaseErrors = new List<String>();
        try{
            Map<String,Object> updateResult = DescribeUtility.updateRecords(recordsToBeUpdate,null);
            List<Database.SaveResult> saveUpdateResults= (List<Database.SaveResult>)updateResult.get('updateResults');        
            for(Integer i=0;i< saveUpdateResults.size();i++){
                if (!saveUpdateResults.get(i).isSuccess()) {
                    Database.Error error = saveUpdateResults.get(i).getErrors().get(0);
                    databaseErrors.add(String.valueof(error));
                }
            }
            logger.addDebugEntry(string.valueof(databaseErrors), CCSFConstants.ONBASE_CONTENTVERSION.ORIGIN_LOCATION);
        }catch(Exception ex){
            logger.addExceptionEntry(ex, CCSFConstants.ONBASE_CONTENTVERSION.ORIGIN_LOCATION);
        }
    }   
}