/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test ExemptionNoticeGeneratorScheduler
//-----------------------------
@isTest
private class ExemptionNoticeGeneratorSchedulerTest {

    //verify that a job is scheduled successfully in a future date.
    @isTest
    private static void jobShouldBeScheduledToRunInFuture() {
        Test.startTest();
        ExemptionNoticeGeneratorScheduler scheduler = new ExemptionNoticeGeneratorScheduler();
        system.assertEquals(scheduler.getJobNamePrefix(), 'Exemption Notice Generator - ' + Label.InitialExemptionNotice,
                            'Job name should be matched');
       
        String jobId = System.schedule('ExemptionNoticeGeneratorScheduler',
                                       '0 0 0 15 3 ? 2032', scheduler);
        Test.stopTest();
        system.assertEquals(1, [SELECT count() FROM CronTrigger where id = :jobId],
                            'A job should be scheduled');
    }
}