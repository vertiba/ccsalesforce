/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Batch class to approve records 
//-----------------------------
public class MassApprovalBatch implements Database.Batchable<sObject>, BatchableErrorHandler{

    private String fixedQuery; 	
    public MassApprovalBatch(String fixedQuery){
        this.fixedQuery= fixedQuery;
        
    }
    public Database.QueryLocator start(Database.BatchableContext bc){
        Logger.addDebugEntry(fixedQuery,'MassApprovalBatch - Start method query');
        Logger.saveLog();
        return database.getQueryLocator(fixedQuery);
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> recordsForProcessing){
        //Batch record update depends on the sObject Type
        String sObjectType = String.valueOf(recordsForProcessing.getSObjectType());
        if(sObjectType == 'DocumentGenerationRequest__c'){
            MassApprovalUtility.updateGenerateNotices(recordsForProcessing, true);
        }else{
            // Calling MassApprovalUtility for approval
            MassApprovalUtility.approvalProcess(recordsForProcessing, true);
        }
    }
    
     public void finish(Database.BatchableContext bc){        
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'fixedQuery' => this.fixedQuery});                                
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, MassApprovalBatch.class.getName(), asyncList[0].ParentJobId, true, queryParams, asyncList[0].NumberOfErrors);

    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {
        
    }
}