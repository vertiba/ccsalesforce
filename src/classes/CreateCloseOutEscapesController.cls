/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// Batch ->Generation of  Escape Closeout Assessments When the auditor clicks button from regular closeout case. Build as per the Story 3505 & 8980

public class CreateCloseOutEscapesController {

    public String CASE_QUERY;
    public string recordStatus {get;set;} 
    public string baseURL {get;set;}
    Map<Id,Id> recordTypeId = new Map<Id,Id>();
    List<Case> regularCloseoutAssessments = new List<Case>();
    Case currentCase = new Case();
    Set<String> escapeYears = new Set<String>();
    //As part of ASR-10611, Map for Assessment and LineItems
    Map<Id,List<AssessmentLineItem__c>> caseByLineItems = new Map<Id,List<AssessmentLineItem__c>>();
    private String customPermisionName ='CreateCloseOutEscapesController';
    
    public CreateCloseOutEscapesController(ApexPages.StandardController standardController){
       currentCase = (Case) standardController.getRecord();
    }
    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is redirecting to VF page CreateCloseoutEscapes
    // @return : void
    //-----------------------------
    
    public void redirect(){

        // Checking if the logged in user has the permission to create Escape Closeouts.
        Boolean hasPermission = FeatureManagement.checkPermission(customPermisionName); 

        if(!hasPermission) { 
            // If permission is not granted then we will return and show the error message as required.
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + currentCase.Id;
            recordStatus = System.Label.NoPermission;
            return;
        }

        // Getting regular closeout Case all fields data with line items with Business Open Date 
       //ASR-10611
       CASE_QUERY = DescribeUtility.getDynamicQueryString('Case',null);
        String recordId = currentCase.id;
        List<String> splitedQueryStings = CASE_QUERY.split('FROM Case');
        CASE_QUERY = splitedQueryStings[0] + ',Property__r.BusinessLocationOpenDate__c';
       
        String query = CASE_QUERY + ' from Case WHERE Id =: recordId';
        regularCloseoutAssessments = new List<Case>((List<Case>)Database.query(query));
        //Addedd for ASR-10611
        caseByLineItems = AssessmentsUtility.retrieveAssessmentAndLineItems(regularCloseoutAssessments);
        baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + regularCloseoutAssessments[0].Id;


        // checking the Pre- Conditions for the creation of Escape closeouts. 
        if(regularCloseoutAssessments[0].Subtype__c != CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT && regularCloseoutAssessments[0].type == CCSFConstants.ASSESSMENT.TYPE_REGULAR) {

            recordStatus = CCSFConstants.ESCAPECLOSEOUTS.TYPE_SUBTYPE_MISMATCH_ERROR_MESSAGE;
            return;
           
        }else if(regularCloseoutAssessments[0].type != CCSFConstants.ASSESSMENT.TYPE_REGULAR) {

            recordStatus = CCSFConstants.ESCAPECLOSEOUTS.TYPE_SUBTYPE_MISMATCH_ERROR_MESSAGE;
            return;

        }else if(regularCloseoutAssessments[0].FinalCloseOutValue__c == null) { // Final Close Out Value Check

            recordStatus = CCSFConstants.ESCAPECLOSEOUTS.NULL_FINALCLOSEOUT_ERROR;
            return;
        }

        // If all the preconditions satisfy then call the method to calculate the escape years and then create escape closeouts accordingly.
        calculateEscapeCloseoutyears();

    }

    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is used calculate Escape Years for which escape closeouts hae tobe created and afer that finally calls 
    //                  the method to create Escape Closeout assessments.
    // @return : void
    //-----------------------------

    public void calculateEscapeCloseoutyears(){

        // Getting business open date from Property of the regular closeout case.
        Date businessOpenDate = regularCloseoutAssessments[0].Property__r.BusinessLocationOpenDate__c;
        Integer currentCaseAssessmentYear = Integer.valueOf(regularCloseoutAssessments[0].AssessmentYear__c);
        Id propertyID = regularCloseoutAssessments[0].property__c;

        Set<String> escapeYearsBasedOnStatueOfLimitation = new Set<String>();
        // Logic to get valid escape years on the basis of Statue of Limitation
        Integer statuteOfLimitation = String.isNotBlank(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION_CONFIG) ? Integer.valueOf(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION_CONFIG) : Integer.valueOf(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION);
        Integer numberOfLimitationYears = Integer.ValueOf(statuteOfLimitation/12);
      
        // Logic to calculate escape years based on the Statue Of Limitation.
        if(businessOpenDate == null || (currentCaseAssessmentYear -  Integer.valueOf(businessOpenDate.year()) >= numberOfLimitationYears)) {
            escapeYearsBasedOnStatueOfLimitation = calculateEscapeYears(Integer.valueOf(regularCloseoutAssessments[0].AssessmentYear__c), numberOfLimitationYears);
        }else {
            escapeYearsBasedOnStatueOfLimitation = calculateEscapeYears(currentCaseAssessmentYear, currentCaseAssessmentYear -  Integer.valueOf(businessOpenDate.year()));
        }

        set<String> missingAssessmentYears = escapeYearsBasedOnStatueOfLimitation;
        // Looping on Past Assessments
        for(Case assessment : [SELECT Id ,AssessmentYear__c,TotalAssessedValue__c FROM Case WHERE Property__c =:propertyID AND AdjustmentType__c !=: CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_CANCEL AND SubStatus__c !=: CCSFConstants.ASSESSMENT.SUBSTATUS_CANCELLED AND IsCorrectedOrCancelled__c = false  Order By EventDate__c,sequencenumber__c desc nulls last] ) {
            
            // Check if the assessment Year is present in the set of years collected using statue of limitaion.
            if(escapeYearsBasedOnStatueOfLimitation.contains(assessment.AssessmentYear__c) && !escapeYears.contains(assessment.AssessmentYear__c)  ) {
                
                missingAssessmentYears.remove(assessment.AssessmentYear__c);
                // Check if Assessed value is less than the Final Close Out Value to create Escape Closeouts.
                if(assessment.TotalAssessedValue__c < regularCloseoutAssessments[0].FinalCloseOutValue__c) {
                    escapeYears.add(assessment.AssessmentYear__c);
                }
            }
        }

        // Exception Handling done if in case creation of assessments/line items fail. A logger record will be created with an custom error message for UI
        try {
            AsyncDlrsCalculator.getInstance().setQueueToRun(false);
            createEscapeCloseouts(escapeYears);
            AsyncDlrsCalculator.getInstance().setQueueToRun(true);
        }catch (Exception ex) {
            // Adding an exception logger entry if in case the DML fails for escape closeouts. 
            recordStatus = CCSFConstants.ESCAPECLOSEOUTS.SYSTEM_ERROR;
            Logger.addExceptionEntry(ex, customPermisionName);
            Logger.saveLog();

        }

        // If no years are eligible then we will show the message to create escape closeouts manually.
        if(escapeYears.isEmpty() && !missingAssessmentYears.isEmpty() ) {
            recordStatus = CCSFConstants.ESCAPECLOSEOUTS.CREATE_CLOSEOUTS_MANUALLY_MESSAGE;

        }// If we got years for missing past assessments then we need to show message telling that escapes closeouts not created for Missing past assessments years
        else if(!missingAssessmentYears.isEmpty()) {
            recordStatus = CCSFConstants.ESCAPECLOSEOUTS.MISSING_YEARS_ERROR_MESSAGE_PART1 +JSON.serialize(missingAssessmentYears) + CCSFConstants.ESCAPECLOSEOUTS.MISSING_YEARS_ERROR_MESSAGE_PART2;
        }
       
    }

    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : Set<String> escapeYears - Collecion of years for which escape has to be created. 
    // @description : This method is used to create escape closeouts assessments and their line items. 
    // @return : void
    //-----------------------------

    public void createEscapeCloseouts(set<String> escapeYears) {

        List<Case> escapeCloseouts = new List<Case>();
        
        Id rollyearId = FiscalYearUtility.getCloseoutRollYear();
        // Looping for all the years for which escape Closeouts has to be created .
        for(String currentEscapedYear : escapeYears) {

            Case escapeCloseout = new Case();
            escapeCloseout.ParentRegularAssessment__c = currentCase.Id;
            escapeCloseout.EventDate__c = Date.newInstance(Integer.valueOf(currentEscapedYear), 1, 1); // What should be the event date for escape closeouts
            escapeCloseout.adjustmentType__c  = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
            escapeCloseout.Status = CCSFConstants.ASSESSMENT.STATUS_NEW;
            escapeCloseout.type = CCSFConstants.ASSESSMENT.TYPE_ESCAPE;
            escapeCloseout.subtype__c = CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT;
            escapeCloseout.property__c = regularCloseoutAssessments[0].property__c;
            escapeCloseout.AssessmentYear__c = currentEscapedYear;
            escapeCloseout.TotalPersonalPropertyValueOverride__c = regularCloseoutAssessments[0].FinalCloseOutValue__c;
            escapeCloseout.TotalFixturesValueOverride__c = 0;
            escapeCloseout.Roll__c = rollyearId;
            escapeCloseouts.add(escapeCloseout);
        }

         // Inserting the escape closeouts.
        insert escapeCloseouts;

        // Cloning Line Items for the escpe closeouts based on the Acquisition year and Assessment year
        //Updated as part of ASR-10611
        List<AssessmentLineItem__c> lineItems = new List<AssessmentLineItem__c>(); 
        for(Case escape : escapeCloseouts) {
            if(caseByLineItems.containsKey(regularCloseoutAssessments[0].id)) {
                for(AssessmentLineItem__c assessmentLineItem : caseByLineItems.get(regularCloseoutAssessments[0].id)) { 
                    if(assessmentLineItem.AcquisitionYear__c < escape.assessmentYear__c) {
                        AssessmentLineItem__c newAssessmentLineItem = assessmentLineItem.clone(false, true, false, false);
                        newAssessmentLineItem.Case__c = escape.Id;
                        lineItems.add(newAssessmentLineItem);
                    }
                }
            }
        }

        // Inserting line items for the escapes created.
        if(!lineItems.isEmpty()) {
            insert lineItems;
        }
        
        // Setting the success message.
        if(!escapeCloseouts.isEmpty()) {
            recordStatus = CCSFConstants.ESCAPECLOSEOUTS.SUCCESS_MESSAGE;
            return;
        }
    }


    //-----------------------------.
    // @author : Publicis.Sapient
    // @param : currentAssessmentYear -  Asessment Year in which the closeout happens, 
    //          subtractor - number of iterations to get the escape year by subtracting unity in each iteration.
    // @description : This method does the calculations for the years based on Statue of Limitation.
    // @return : Set<String> desired Escape years on the basis of Statue oF Limitation
    //-----------------------------

    public Set<String> calculateEscapeYears(integer currentAssessmentYear, Integer subtractor) {

        // returning if any of the paramter is null.
        if(currentAssessmentYear == null || subtractor == null) { 
            return null;
        }
        // Getting Escape years by subtracting unity form the current assessment year as per the Statue of limitation 
        Set<String> desiredYears = new Set<String>();
        for(Integer i = 1; i<= subtractor; i++){
            Integer desiredYear = currentAssessmentYear -i;
          
            desiredYears.add(String.valueOf(desiredYear));
        }
       
        return desiredYears;
    }
   

}