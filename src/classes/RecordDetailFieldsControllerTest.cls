@isTest
public class RecordDetailFieldsControllerTest {

    @isTest
    static void testGetFieldSetFields() {
        Account testProject = new Account(Name = 'test');
        insert testProject;
        String fieldSetName = new List<String>(Schema.SObjectType.Account.fieldSets.getMap().keySet())[0];
        
        Test.startTest();
        List<String> fields = RecordDetailFieldsController.getFieldSetFields(testProject.Id, fieldSetName);
        Test.stopTest();
        
        System.assert(fields.size() > 0);
    }

}