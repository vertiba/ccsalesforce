/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @story number : ASR- 8552.
// @description : This test class is used to  Controller claas to be called after Adjust Assessment button pressed
//-----------------------------
@isTest

public with sharing class AdjustAssessmentControllerTest {
    @testSetUp
    static void createTestData(){
        
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin); 
        User officeAssistant = TestDataUtility.getOfficeAssistantUser();
        users.add(officeAssistant);
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            
            String currentFiscalyear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());            
            String previousFiscalyear = String.valueOf(Integer.valueOf(currentFiscalyear)-1);
            List<RollYear__c> rollyears = new List<RollYear__c>();
            RollYear__c currentRollyear = TestDataUtility.buildRollYear(currentFiscalyear,currentFiscalyear,'Roll Open');
            RollYear__c closedRollyear = TestDataUtility.buildRollYear(previousFiscalyear,previousFiscalyear,'Roll Closed');
            rollyears.add(currentRollyear);
            rollyears.add(closedRollyear);
            insert rollyears;
            
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;
            
            Case cas = new Case();
            cas.AccountId = bppProperty.account__c;
            cas.Property__C = bppProperty.id;
            cas.MailingCountry__c = 'US';
            cas.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            cas.Type='Regular';
            cas.RecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
            
            insert cas; 
            
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,cas.id);
            insert assessmentLineItem; 
            
            //insert Vessel property
            Property__c vesselProperty = TestDataUtility.getVesselProperty();
            vesselProperty.Account__c = businessAccount.Id;
            insert vesselProperty;
        }
    }
    @isTest
    private static void testProfileHasNoAccess(){
        User officeAssistantUser = [SELECT Id FROM User WHERE Profile.Name !=: 'System Administrator' AND Profile.Name !=: 'BPP Staff' LIMIT 1]; 
        
        Case cs = [Select Id from Case];
        
        System.runAs(officeAssistantUser){
            AdjustAssessmentController extension = new AdjustAssessmentController(new ApexPages.standardController(cs));           
            Test.startTest();
            extension.redirect(); 
            Test.stopTest();
            
            System.assertEquals(System.Label.NoPermission, extension.recordStatus,'No permission to this profile');
        }
    }
     @isTest
    private static void testRedirectAdjustAssessmentCreatedNotAllowed(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,integrationstatus__c ,Status,SubStatus__c,SequenceNumber__c from Case];
        cs.Status= CCSFConstants.ASSESSMENT.STATUS_CLOSED;
        cs.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        cs.SequenceNumber__c= 1;                 
        update cs;
        System.runAs(sysAdminUser){
            AdjustAssessmentController extension = new AdjustAssessmentController(new ApexPages.standardController(cs));             
            Test.startTest();
            extension.redirect();            
            Test.stopTest();
            System.assertEquals( System.Label.AdjustAssessmentCreatedNotAllowed, extension.recordStatus,'Adjustment Success');           
            
        }
        
    } 
    @isTest
    private static void testRedirectAdjustAssessmentCreatedAllowed(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,integrationstatus__c ,Status,SubStatus__c,SequenceNumber__c from Case];
        cs.Status= CCSFConstants.ASSESSMENT.STATUS_CLOSED;
        cs.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        cs.SequenceNumber__c= 1;
        cs.IntegrationStatus__c= CCSFConstants.ASSESSMENT.SENT_TO_TTX;                 
        update cs;
        System.runAs(sysAdminUser){
            AdjustAssessmentController extension = new AdjustAssessmentController(new ApexPages.standardController(cs));             
            Test.startTest();
            extension.redirect();            
            Test.stopTest();
            System.assertEquals( System.Label.AdjustAssessmentCreated, extension.recordStatus,'Adjustment Success');  
            List<Case> cases=[Select Id,AdjustmentType__c,IntegrationStatus__c from Case];    
            system.assertEquals(2,cases.size(),'1 Orginial, 1 Adjusted');
            
        }
        
    } 
    
}