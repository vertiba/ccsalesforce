/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
public class GenerateLowValueAssessmentTest {

    @testSetup
    static void createTestData() {
        User nonAdmin = TestDataUtility.getOfficeAssistantUser();
        User sysAdmin = TestDataUtility.getSystemAdminUser();
        insert new List<User>{nonAdmin, sysAdmin};
            System.runas(sysAdmin){
                RollYear__c currentRollYear = new RollYear__c(
                    IsLowValueBulkGeneratorProcessed__c = false,
                    Name                                = String.valueof(FiscalYearUtility.getCurrentFiscalYear()),
                    Status__c                           = 'Roll Open',
                    Year__c                             = String.valueof(FiscalYearUtility.getCurrentFiscalYear())
                );
                RollYear__c previousRollYear = new RollYear__c(
                    IsLowValueBulkGeneratorProcessed__c = false,
                    Name                                = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1),
                    Status__c                           = 'Roll Closed',
                    Year__c                             = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1)
                );
                List<RollYear__c> rollYears = new List<RollYear__c>();
                rollYears.add(currentRollYear);
                rollYears.add(previousRollYear);
                insert rollYears;
            }
    }

    // TEST METHODS
    @isTest
    static void RantestGenerateLowValueAssessmentForSysAdmin(){
        User systemAdmin     = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c ='Roll Open'];

        Test.startTest();
        GenerateLowValueAssessment.Response response;
        GenerateLowValueAssessment.Response response1;

        System.runAs(systemAdmin) {
            response = GenerateLowValueAssessment.checkPermission(rollYear.Id);
            response1= GenerateLowValueAssessment.runLowValueAssessmentBulkGenerator();
        }
        Test.stopTest();

        // Assert the functionality i.e users with System Administrator profile has custom permission
        System.assertEquals(response.isSuccess, true);
        system.assertEquals(response1.isSuccess, true);
    }

    @isTest
    static void  checkBatch() {
        Integer currentYear = FiscalYearUtility.getCurrentFiscalYear();
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c roll=[Select Id,IsLowValueBulkGeneratorProcessed__c from RollYear__c where year__c =:String.valueof(currentYear) and Status__c ='Roll Open'];
        roll.IsLowValueBulkGeneratorProcessed__c = true;
        update roll;

        Test.startTest();
        GenerateLowValueAssessment.Response response;

        System.runAs(systemAdmin) {
          response = GenerateLowValueAssessment.runLowValueAssessmentBulkGenerator();
        }
        Test.stopTest();

        system.assertEquals(response.isSuccess, false);
    }

    @isTest
    static void testGenerateLowValueAssessmentForNonAdmin() {
        User nonAdmin        = [SELECT Id FROM User WHERE Email = 'testOfficeAssistantUser@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c ='Roll Open'];

        Test.startTest();
        GenerateLowValueAssessment.Response response;
        System.runAs(nonAdmin) {
            response = GenerateLowValueAssessment.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e users without System Administrator profile i.e.Office Assistant in this case
        // has not assigned the custom permission
        System.assertEquals(response.isSuccess, false);
    }

    @isTest
    static void RantestGenerateLowValueAssessmentForClosedYear(){
        User systemAdmin     = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c ='Roll Closed'];

        Test.startTest();
        GenerateLowValueAssessment.Response response;      

        System.runAs(systemAdmin) {
            response = GenerateLowValueAssessment.checkPermission(rollYear.Id);            
        }
        Test.stopTest();

        // Assert the functionality RollYear is Closed 
        System.assertEquals(response.isSuccess, false);
        system.assertEquals(System.Label.RollYearClosed, response.message);
        
    }
    
    @isTest
    static void RantestGenerateLowValueAssessmentForIsLowValueBulkGeneratorProcessedTrue(){
        User systemAdmin     = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c ='Roll Open'];
		rollYear.IsLowValueBulkGeneratorProcessed__c = true;
        update rollYear;
        Test.startTest();
        GenerateLowValueAssessment.Response response;       

        System.runAs(systemAdmin) {
            response = GenerateLowValueAssessment.checkPermission(rollYear.Id);            
        }
        Test.stopTest();

        // Assert the functionality IsLowValueBulkGeneratorProcessed__c is True 
        System.assertEquals(response.isSuccess, false);
        system.assertEquals(CCSFConstants.BUTTONMSG.LOW_VALUE_ASSESMENT_ALREADY_GENERATED_RMSG, response.message);
        
    }

}