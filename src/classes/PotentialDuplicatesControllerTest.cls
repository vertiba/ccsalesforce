/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for PotentialDuplicatesController
 * @Date: 01/30/2020
 * @Author: Srini Aluri(A1)
 
 * ModifiedBy         ModifiedDate   Description
 * Srini Aluri(A1)    01/31/2020     Initial development
*/

@isTest
public class PotentialDuplicatesControllerTest {
    
    @testSetup
    static void createTestData() {
     	Account act = new Account(Name = 'Star Company', Website = 'www.starcompany.com');
        INSERT act;
        
        Account act2 = new Account(Name = 'Star Company', Website = 'www.starcompany.tv');
        INSERT act2;
    }

    static testMethod void sobjectTypeHasDuplicateRulesTest() {
        Account act = new Account(Name = 'Star Company', Website = 'www.starcompany.org');
        INSERT act;
        
        Boolean hasDuplicateRule;
        
        Test.startTest();
        
        hasDuplicateRule = PotentialDuplicatesController.sobjectTypeHasDuplicateRules(act.Id);
        
        Test.stopTest();
        
        // Assert the functionality - We should get atleast one Duplicate rule i.e. true in the hasDuplicateRule
        System.assertEquals(true, hasDuplicateRule != null);
    }
    
    static testMethod void getPotentialDuplicatesTest() {
        // Create duplicate record set manually
        DuplicateRule dupRule = [SELECT Id 
                                 FROM DuplicateRule 
                                 WHERE DeveloperName = 'Account_Same_Name' 
                                 LIMIT 1];
        DuplicateRecordSet dupRs = new DuplicateRecordSet(DuplicateRuleId = dupRule.Id);
        INSERT dupRs;
        
        // Create duplicate accounts
        List<Account> dupActList = new List<Account>();
        for (Integer i = 0; i < 5; i++) {
        	dupActList.add(new Account(Name = 'Star Company', Website = 'www.starcompany' + i + '.uk'));
        }        
        INSERT dupActList;
        
        // Create DuplicateRecordItem's manually
        List<DuplicateRecordItem> dupRecItemList = new List<DuplicateRecordItem>();
        for (Account act : dupActList) {
        	dupRecItemList.add(new DuplicateRecordItem(DuplicateRecordSetId = dupRs.Id, RecordId = act.Id));
        }
        INSERT dupRecItemList;
        
        // Test the functionality
        List<SObject> dupRecList;
        
        Test.startTest();
        
        dupRecList = PotentialDuplicatesController.getPotentialDuplicates(dupActList[0].Id, 'Account');
        
        Test.stopTest();        
        
        // Assert the functionality - We should get 5 duplicate records, i.e. list must not be empty
        System.assertEquals(false, dupRecList.isEmpty());        
    }
}