/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Conroller for Cerification VF pages
//-----------------------------
public class CertificationDisplayController {

    public Statement__c getStatement() {
        return [SELECT Id, AccountName__c, FileDate__c, toLabel(Form__c), PropertyId__c, Name
            FROM Statement__c
            WHERE Id = :ApexPages.currentPage().getParameters().get('recordId')
        ];
    }

    //Redirecting to Navigation page to download as PDF
    public PageReference redirect(){
    	System.PageReference pageRef = new System.PageReference('/apex/CertificateNavigation?recordId='+ApexPages.currentPage().getParameters().get('recordId'));
        return pageRef;
    }

}