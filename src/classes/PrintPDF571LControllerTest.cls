/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @story number : ASR- 8469.
// @description : This test class for PrintPDF571LController
//-----------------------------
@isTest
public class PrintPDF571LControllerTest {
    
    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
	
    @testSetup
    private static void commonDataSetupForTesting() {
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin); 
        User officeAssistant = TestDataUtility.getOfficeAssistantUser();
        users.add(officeAssistant);
        insert users;
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        System.runAs(systemAdminUser) {
            
            String currentFiscalyear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());            
            String previousFiscalyear = String.valueOf(Integer.valueOf(currentFiscalyear)-1);
            List<RollYear__c> rollyears = new List<RollYear__c>();
            RollYear__c currentRollyear = TestDataUtility.buildRollYear(currentFiscalyear,currentFiscalyear,'Roll Open');
            RollYear__c closedRollyear = TestDataUtility.buildRollYear(previousFiscalyear,previousFiscalyear,'Roll Closed');
            rollyears.add(currentRollyear);
            rollyears.add(closedRollyear);
            insert rollyears;
            
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;
            
            Case cas = new Case();
            cas.AccountId = bppProperty.account__c;
            cas.Property__C = bppProperty.id;
            cas.MailingCountry__c = 'US';
            cas.EventDate__c = System.today();
            cas.Type='Regular';
            cas.RecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
            
            insert cas; 
            
            
            //Create Assessment Line Items
            List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
            AssessmentLineItem__c assessmentLineItem1 = new AssessmentLineItem__c();
            assessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('Alternate Schedule A', 'ATMs', '2017', 100.00, cas.id, 'Assessment Line Item for Current year');
            assessmentLineItems.add(assessmentLineItem1);
            AssessmentLineItem__c assessmentLineItem2 = new AssessmentLineItem__c();
            assessmentLineItem2 = TestDataUtility.buildAssessmentLineItem('Construction in Progress', 'Construction in Progress', '2016', 100.00, cas.id, 'Assessment Line Item for Current year');
            assessmentLineItems.add(assessmentLineItem2);
            //Insert Assessment Line Items        
            insert assessmentLineItems;
            
            //Create Statement         
            Map<String,String> inputParamsStatement = new Map<String,String>();            
            inputParamsStatement.put('accountId',bppProperty.account__c);
            inputParamsStatement.put('propertyId',bppProperty.id);
            inputParamsStatement.put('formType','571-L');
            inputParamsStatement.put('businessScenario','Existing');
            inputParamsStatement.put('propertyScenario','Existing');
            inputParamsStatement.put('assessmentYear',String.valueOf(lastYear)); 
            inputParamsStatement.put('recordTypeId',DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME));
            inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/'+lastToLastYear));
            inputParamsStatement.put('filingDate',String.valueOf('01/03/'+lastYear));            
            Statement__c statement = TestDataUtility.buildStatement(inputParamsStatement);
            //Insert Statement  
            insert statement;
            
            //Create Statement Reported Asset
            Id statementReportedOwnedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.OWNED_ASSETS_RECORD_TYPE_API_NAME);   
            List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
            StatementReportedAsset__c statementReportedAsset1 = new StatementReportedAsset__c();
            statementReportedAsset1 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Bldg/Bldg Impr/Leasehold Impr/Land/Land Impr', 'Land and Land Development', '2017', 100, null, statement.id, 'Statement Reported Asset for last year');
            statementReportedAssets.add(statementReportedAsset1);        
            StatementReportedAsset__c statementReportedAsset2 = new StatementReportedAsset__c();
            statementReportedAsset2 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Bldg/Bldg Impr/Leasehold Impr/Land/Land Impr', 'Land Improvements', '2016', 100, null, statement.id, 'Statement Reported Asset for last year');        
            statementReportedAssets.add(statementReportedAsset2);
            //Insert Statement Reported Asset
            insert statementReportedAssets;

            
            ReportedAssetSchedule__c reportedAssetSchedule1 = 
                TestDataUtility.buildReportedAssetSchedule(statementReportedAsset1.id,'January','2017',10000,
                                                          'New Report Asset Schedule', 'Addition');
            insert reportedAssetSchedule1;
        }
    }
    
    @isTest
    static void getStatementReportedAssetsTest() {        
        Statement__c statement = [select id, Status__c from Statement__c limit 1];
        apexpages.currentpage().getparameters().put('recordID' , statement.id);
        List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
        PrintPDF571LController printPDF571LController = new PrintPDF571LController();
        statementReportedAssets = printPDF571LController.getStatementReportedAssets();        
        System.assertEquals(2,statementReportedAssets.size());
    }
    
    @isTest
    static void getStatementTest() {        
        Statement__c statement = [select id, Status__c from Statement__c limit 1];
        apexpages.currentpage().getparameters().put('recordID' , statement.id);
        Statement__c inputStatement = new Statement__c();
        PrintPDF571LController printPDF571LController = new PrintPDF571LController();
        inputStatement = printPDF571LController.getStatement();        
        System.assertNotEquals(null,inputStatement);
    }
        
    @isTest
    static void getLeasedStatementReportedAssetsTest() {        
        Statement__c statement = [select id, Status__c from Statement__c limit 1];
        apexpages.currentpage().getparameters().put('recordID' , statement.id);
        List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
        PrintPDF571LController printPDF571LController = new PrintPDF571LController();
        statementReportedAssets = printPDF571LController.getLeasedStatementReportedAssets();        
        System.assertEquals(0,statementReportedAssets.size());
    }
    
    @isTest
    static void getReportedAssetScheduleTest() {        
        Statement__c statement = [select id, Status__c from Statement__c limit 1];
        apexpages.currentpage().getparameters().put('recordID' , statement.id);
        List<StatementReportedAsset__c> reportedAssetsWithSchedules = new List<StatementReportedAsset__c>();
        PrintPDF571LController printPDF571LController = new PrintPDF571LController();
        reportedAssetsWithSchedules = printPDF571LController.getReportedAssetsWithSchedule();        
        System.assertEquals(2,reportedAssetsWithSchedules.size());
    } 

}