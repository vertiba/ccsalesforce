/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class DocumentGenerationRequestHandlerTest {

    @testSetup
    static void setupData() {
		Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account account = TestDataUtility.buildAccount('test account', 'Notice to File', recordTypeId);
        insert account;

        // Create SmartCOMM data so there is a document setting record to use
        TH1__Schema_Set__c schemaSet = new TH1__Schema_Set__c();
        insert schemaSet;

        List<TH1__Schema_Object__c> schemaObjects = new List<TH1__Schema_Object__c>();
        TH1__Schema_Object__c accountSchemaObject = new TH1__Schema_Object__c(
            Name                      = 'Account',
            TH1__Is_Primary_Object__c = true,
            TH1__Object_Label__c      = 'Account',
            TH1__Schema_Set__c        = schemaSet.Id
        );
        insert accountSchemaObject;

        List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        TH1__Document_Setting__c documentSettting = new TH1__Document_Setting__c(
            Name                        = 'Account Document Setting',
            TH1__Document_Data_Model__c = schemaSet.Id,
            TH1__Is_disabled__c         = false,
            TH1__Storage_File_Name__c   = 'Account Document Setting'
        );
        documentSettings.add(documentSettting);
        insert documentSettings;
    }

    @isTest
    static void it_should_set_set_related_record_fields_on_new_record() {
        Account account = [SELECT Id FROM Account];
        TH1__Document_Setting__c documentSetting = [SELECT Id FROM TH1__Document_Setting__c];

        Test.startTest();
        DocumentGenerationRequest__c documentGenerationRequest = new DocumentGenerationRequest__c(
            DocumentSetting__c = documentSetting.Id,
            RelatedRecordId__c = account.Id
        );
        insert documentGenerationRequest;
        Test.stopTest();

        documentGenerationRequest = [
            SELECT Id, Account__c, DocumentSetting__c, RelatedRecordId__c, RelatedRecordType__c
            FROM DocumentGenerationRequest__c
            WHERE Id = :documentGenerationRequest.Id
        ];
        System.assertEquals(account.Id, documentGenerationRequest.Account__c);
        System.assertEquals(account.Id, documentGenerationRequest.RelatedRecordId__c);
        System.assertEquals('Account', documentGenerationRequest.RelatedRecordType__c);
    }
}