/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**************
 * @Author: Publicis Sapient
 * @Apex Class: AddressEntryController
 * @Apex Test Class Name: AddressEntryControllerTest
 * @Date : 26th Nov 2019 
***************/
@isTest
public class AddressEntryControllerTest {
    
        @isTest
    	private static void manageAddressEntry(){
        Account account = new Account();
        account.Name='test account';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.MailingStreetName__c ='355';
        account.MailingCity__c ='Oakland';
        account.NoticeType__c='Notice to File';
        account.RequiredToFile__c='Yes';
        insert account;
       
        Test.startTest();
         sobject sobj= AddressEntryController.getData('Account', 'Mailing', account.Id);
         system.assertEquals('Account', String.valueOf(sobj.getSObjectType()));        
         AddressEntryController.AddressMetadata addressMetadata = AddressEntryController.getAddressMetadata('Account','Mailing');
         system.assert(addressMetadata.StreetFractions != Null);
         
        Test.stopTest();
    }

	@isTest
    private  static void lookupCallouts(){
        String street = '355 Bush';
        String zipCode = '94104';
        String city='SAN FRANCISCO';
        String state='CA';
        
        Test.setMock(HttpCalloutMock.class, new AddressEntryControllerMockCallout());
        AddressEntryController.AddressLookupResult addressLookupResult = AddressEntryController.lookupAddress(street,city,state,zipCode);
        system.assertEquals('SAN FRANCISCO', addressLookupResult.City);
        
        
    }
}