@isTest
public class ConversationControllerTest {
    
    @isTest static void ConversationTestMethod(){
        String extname ='summary/2020-1-21';
        Map<String, Object> filter = new Map<String, Object>();
        filter.put('paginationOffset','0');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConversationControllerMockCallout(true));
        String record = ConversationController.conversationCallString(extName,filter);
        system.assert(record == 'Conversion');
        Test.stopTest();
    }
    
    @isTest
    private static void someTestMockFailure() {
        String extname ='summary/2020-1-21';
        Map<String, Object> filter = new Map<String, Object>();
        filter.put('paginationOffset','0');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ConversationControllerMockCallout(false));  // mock with response = 400
        String record = ConversationController.conversationCallString(extName,filter);       
        System.assert(record == null);
        Test.stoptest();
    }  

}