/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class StatementCommunityCardController {

    @AuraEnabled
    public static Id getRecordTypeId(String objectApiName, String recordTypeLabel) {
        if(recordTypeLabel == null) return null;

        Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(objectApiName);
        RecordTypeInfo recordTypeInfo = sobjectType.getDescribe().getRecordTypeInfosByName().get(recordTypeLabel);

        if(recordTypeInfo == null) return null;

        return recordTypeInfo.getRecordTypeId();
    }

}