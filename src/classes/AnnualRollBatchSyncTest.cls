/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test AnnualRollBatchSync
//-----------------------------
@isTest
public class AnnualRollBatchSyncTest {

    @testSetup
    private static void dataSetup(){
	    //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
                
        //insert account
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        List<Property__c> properties = new List<Property__c>();

        Property__c property = TestDataUtility.getVesselProperty();
        property.Account__c = businessAccount.Id;
        property.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        properties.add(property);

        Property__c property2 = TestDataUtility.getVesselProperty();
        property2.Account__c = businessAccount.Id;
        property2.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        properties.add(property2);
        insert properties;
        
        String fiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
		Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear), 5, 30);
        
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = fiscalYear;
        rollYear.Year__c = fiscalYear;
        rollYear.Status__c='Roll Open';
        insert rollYear;
        
        List<Property__c>  vesselProperties = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
        
        List<Case> assessments = new List<Case>();
        Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),vesselProperties[0].Id,
                                                                    rollYear.Id,rollYear.Name,rollYear.Name,
                                                                    CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(fiscalYear), 1, 1));
        assessment.Status = 'Closed';
        assessment.IntegrationStatus__c = CCSFConstants.READY_TO_SENT_TO_TTX;
        assessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
        assessment.AccountId = property.account__c;
        assessment.Property__c = property.Id;
        assessments.add(assessment);
        

        Case assessment2  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),vesselProperties[1].Id,
                                                                    rollYear.Id,rollYear.Name,rollYear.Name,
                                                                    CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(fiscalYear), 1, 1));
        assessment2.Status = 'Closed';
        assessment2.IntegrationStatus__c = CCSFConstants.READY_TO_SENT_TO_TTX;
        assessment2.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
        assessment2.AccountId = property2.account__c;
        assessment2.Property__c = property2.Id;
        assessment2.TriggerBatchId__c = '111111fe-11b1-1c11-11b1-ed1111111a22';
        assessments.add(assessment2);
        insert assessments;
    }

    
    @isTest
    private static void TestBatchClass() {
        RollYear__c rollYear = [SELECT  id,Year__c FROM RollYear__c];
       
        Test.setMock(HttpCalloutMock.class, new AnnualRollBatchSyncMockCallout());
        Test.startTest();
        Database.executeBatch(new AnnualRollBatchSync(rollYear));
        Test.stopTest();
        Case newCase = [SELECT  id,IntegrationStatus__c,TriggerBatchId__c FROM Case Limit 1];
        System.assertEquals(CCSFConstants.IN_TRANSIT_TO_TTX, newCase.IntegrationStatus__c);
        System.assertEquals('111111fe-11b1-1c11-11b1-ed1111111a11', newCase.TriggerBatchId__c);
    }
    
    @isTest
    private static void TestBatchClassWithReplay() {
        RollYear__c rollYear = [SELECT  id,Year__c FROM RollYear__c];
        Test.setMock(HttpCalloutMock.class, new AnnualRollBatchSyncMockCallout());
        Test.startTest();
        Database.executeBatch(new AnnualRollBatchSync(rollYear,'111111fe-11b1-1c11-11b1-ed1111111a22'));
        Test.stopTest();
        Case newCase = [SELECT  id,IntegrationStatus__c,TriggerBatchId__c FROM Case Where TriggerBatchId__c != null];
        System.assertEquals(CCSFConstants.IN_TRANSIT_TO_TTX, newCase.IntegrationStatus__c);
     
    }
}