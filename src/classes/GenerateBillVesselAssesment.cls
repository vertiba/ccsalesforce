/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used as controller for GenerateBillVesselAssesment aura component.
//-----------------------------
public class GenerateBillVesselAssesment {

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method checks permissions before allowing user to generate assessments.          
    // @return : Response wrapper object
	@AuraEnabled
    public static Response checkPermission(Id recordId) {
		
        Response response = new Response();    
        Boolean hasPermission = FeatureManagement.checkPermission(System.Label.MassGenerateDirectBillVesselAssessments); 
        RollYear__c rollYear = [Select Status__c,IsLowValueVesselAssessmentgenerator__c,IsDirectBillVesselAssessmentProcess__c from RollYear__c where id =: recordId];    
        
        //Check if roll year is not closed, if closed return error message
        if(rollYear.Status__c == CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS)
        {
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.CLOSED_ROLL_YEAR_BUTTON_WARNING;        
            return Response;    
        }else if(!rollYear.IsLowValueVesselAssessmentgenerator__c) {
        //check if low value vessel batch already ran, if not ran return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.DIRECTBILLVESSEL_ASSESMENT_GENERATOR_WARNING ;
            return Response; 
        }else if (rollYear.IsDirectBillVesselAssessmentProcess__c) {
        //check if Direct Bill Vessel batch has not already ran, if ran return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.DIRECTBILLVESSEL_ASSESMENT_ALREADY_GENERATED_MSG ;
            return Response; 
        }else if(!hasPermission) {
        //check if user has Custom Permission to run this batch, if not return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.NO_PERMISSION;      
            return Response;       
        }else {
        //if all the above condtions are false, means user can run this batch
            response.isSuccess = true;
            response.message = System.Label.HasPermissionVesselDirectBill;     
            return Response;  
        }             
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method runs the DirectVesselAssessmentGenerator batch.          
    // @return : Response wrapper object
    @AuraEnabled 
    public static Response runDirectBillVesselAssessmentBulkGenerator() {
        
        Response response = new Response();        
        
        List<AsyncApexJob> apexJob = [Select Id, Status,ApexClass.name from AsyncApexJob 
											 where ApexClass.name =: CCSFConstants.DIRECTBILL_VESSEL_GENERATOR_BATCH_NAME
											 and Status IN : CCSFConstants.BATCHMSG.BATCH_STATUSES order by CreatedDate desc limit 1];
		
		//Check if the same batch is not already running.
        if(!apexJob.isEmpty())
        {
            response.isSuccess = false;
            response.message = CCSFConstants.BATCHMSG.BATCH_ALREADY_RUNNING_MSG;
        }else {            
            //calling Batch
            DirectVesselAssessmentGenerator directBillVesselbatch = new DirectVesselAssessmentGenerator();
            Id jobId = Database.executeBatch(directBillVesselbatch,Integer.valueOf(Label.DirectBillVesselAssessmentGeneratorBatchSize));

            AsyncApexJob apexJobs = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                     TotalJobItems, CreatedBy.Email, ExtendedStatus
                                     from AsyncApexJob where Id = :jobId];
            
            if(apexJobs.Status != CCSFConstants.BATCHMSG.BATCH_STATUS_FAILED) {
                response.isSuccess = true;
                response.message= System.Label.BatchQueued + apexJobs.Status; 
            } else { 
                response.isSuccess = false;
                response.message = System.Label.BatchErrors + apexJobs.NumberOfErrors;
            }  
        }        
        return response;
    }
	
	//wrapper class for response
    public class Response {
        @AuraEnabled
        public Boolean isSuccess;
        @AuraEnabled
        public String message;
        
        public Response() {
            isSuccess =false;
            message='';
        }  
    }
}