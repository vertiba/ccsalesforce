/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * CreateJWTToken is class that creates a JWT token on the fly based on the paramenters provided by 
 * the caller method or default values provided in custom labels JWT_*. Null pointer exception and 
 * No Data Found exception are the 2 exceptions that can be thrown when certificate is not found in 
 * the org, or null is passed as certificate name. New overloaded methods can be added as required.
 */
public class CreateJWTToken {
    /**
     * An overridden for actual implementation, where caller sends without any parameters.
     */
    @AuraEnabled
	public static string generateJWTToken(){
        map<string,object> claims = new map<string,object>();
        claims.put('scope', new List<String>{'read','write'});
        //If method is called in batch then userinfo will not be there, so hardcoding, can be moved to custom label
        //but as of now, not sure what would be the profile to use.
        String profileName = System.isBatch() ? 'System Administrator' : 
        					[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
        claims.put('authorities', new List<String>{profileName});
        return generateJWTToken(Label.JWT_Token_Subject,Label.JWT_Token_Audience,Label.JWT_Token_Issuer,
                                Integer.valueOf(Label.JWT_Token_Expiry_Seconds),Label.JWT_Token_Certificate_Name,claims);
    }
    public static string generateJWTToken(string subject, string audience, string issuer, integer expiryInSeconds,
                                          string certificateName, map<string,object> claims){
        Auth.JWT jwt = new Auth.JWT();
        jwt.setSub(subject);
        jwt.setAud(audience);
        jwt.setIss(issuer);
        jwt.setAdditionalClaims(claims);
        if(expiryInSeconds != null && expiryInSeconds>0){
            jwt.setValidityLength(expiryInSeconds);
        }
        if(Test.isRunningTest() && !String.isEmpty(certificateName)){
            return 'abcdefgh.ijklemnop.qrstuvwxyz1234567890';
        }
        return new Auth.JWS(jwt, certificateName).getCompactSerialization();
    }
}