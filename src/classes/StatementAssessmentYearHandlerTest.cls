/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// Class to test StatementAssessmentYearHandler
//-----------------------------
@isTest
public class StatementAssessmentYearHandlerTest {

	//-----------------------------
    // @author : Publicis.Sapient
    // @description : Tests that StatementAssessmentYearHandler.getStatementAssessmentYearTest updates the Assessment Year
    //                                        Based on the VIPDateTime field.
    // @return : void
    //-----------------------------

    @isTest
    static void getStatementAssessmentYearTest() {
        List<StatementAssessmentYearHandler.wrapperClass> response = new List<StatementAssessmentYearHandler.wrapperClass>();
        StatementAssessmentYearHandler.wrapperClass wr = new StatementAssessmentYearHandler.wrapperClass();

        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User 5',alias='tuser5',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser5@test.com',UserName='testuser5@test.com.ccsf');

        database.insert(userInstance);
        System.runAs(userInstance){
            date myDate = date.newInstance(2018, 7, 15);
            String businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            Account accountData= TestDataUtility.buildAccount('Test Account', 'Low Value', businessAccountRecordTypeId);
            insert accountData;
            List<Property__c> properties = new List<Property__c>();
            Id proRecordTypeId  = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
            Property__c testProperty =  new Property__c();
            testProperty.RecordTypeId = proRecordTypeId;
            testProperty.Account__c = accountData.Id;
            testProperty.DoingBusinessAs__c = 'Chemicals';
            testProperty.Status__c ='Active';
            testProperty.RollCode__c ='Unsecured';
            testProperty.AssessorsParcelNumber__c ='1234A567';
            testProperty.DiscoverySource__c ='Audit';
            insert testProperty;
            Statement__c statement = new Statement__c();
            statement.Property__c = testProperty.Id;
            statement.VIP_Date_Time_Submitted__c =  system.Now();
            String fiscal = String.valueof(FiscalYearUtility.getFiscalYear(statement.VIP_Date_Time_Submitted__c));
            statement.Status__c ='submitted';
            statement.AssessmentYear__c = fiscal;
            insert statement;

            wr.statementId = statement.Id;
            wr.vipDate = system.Now();
            response.add(wr);

            System.assertEquals( statement.AssessmentYear__c, String.valueOf(FiscalYearUtility.getFiscalYear(statement.VIP_Date_Time_Submitted__c)));
            StatementAssessmentYearHandler.getStatementAssessmentYear(response);
        }
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Tests that StatementAssessmentYearHandler.setAmendedForm updates the AmendedFrom field
    //                                        Based on the Latest File Date entry.
    // @return : void
    //-----------------------------

    @isTest
    static void getStatementAmendedFormTest() {
        List<Id> statementIds = new List<Id>();
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.BusinessStatus__c = 'Active';
        testAccount.NoticeType__c ='Notice to File';
        testAccount.RequiredToFile__c ='Yes';
        testAccount.RecordTypeId = accRecordTypeId;
        insert testAccount;

        List<Property__c> properties = new List<Property__c>();
        Id proRecordTypeId  = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Property__c testProperty =  new Property__c();
        testProperty.RecordTypeId = proRecordTypeId;
        testProperty.Account__c = testAccount.Id;
        testProperty.DoingBusinessAs__c = 'Chemicals';
        testProperty.Status__c ='Active';
        testProperty.RollCode__c ='Unsecured';
        testProperty.AssessorsParcelNumber__c ='1234B567';
        testProperty.DiscoverySource__c ='Audit';
        insert testProperty;

        Id statementRecordTypeId  = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalPropertyStatement').getRecordTypeId();
        List<Statement__c> statementList = new List<Statement__c>();
        Statement__c testStatement = new Statement__c();
        testStatement.RecordTypeId = statementRecordTypeId;
        testStatement.BusinessAccount__c = testProperty.Account__c;
        testStatement.Property__c = testProperty.Id;
        testStatement.FileDate__c = System.today();
        testStatement.VIP_Date_Time_Submitted__c =  system.Now();
        testStatement.Status__c ='submitted';
        testStatement.AssessmentYear__c = String.valueOf(Date.today().year());
        testStatement.Form__c = '571-L';
        insert testStatement;

        Statement__c testStatement2 = new Statement__c();
        testStatement2.RecordTypeId = statementRecordTypeId;
        testStatement2.BusinessAccount__c = testProperty.Account__c;
        testStatement2.Property__c = testProperty.Id;
        testStatement2.FileDate__c = System.today()-1;
        testStatement2.VIP_Date_Time_Submitted__c =  system.Now();
        testStatement2.Status__c ='submitted';
        testStatement2.AssessmentYear__c = String.valueOf(Date.today().year());
        testStatement2.Form__c = '571-L';
        insert testStatement2;
        system.assert(testStatement != Null);
        Test.startTest();
        StatementAssessmentYearHandler.setAmendedForm(testStatement.Id);
        System.assertEquals(null, testStatement2.AmendedFrom__c);
        Test.stopTest();
    }
    
    @isTest
    static void getStatementAmendedFormTestForProcessed() {
        List<Id> statementIds = new List<Id>();
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.BusinessStatus__c = 'Active';
        testAccount.NoticeType__c ='Notice to File';
        testAccount.RequiredToFile__c ='Yes';
        testAccount.RecordTypeId = accRecordTypeId;
        insert testAccount;

        List<Property__c> properties = new List<Property__c>();
        Id proRecordTypeId  = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Property__c testProperty =  new Property__c();
        testProperty.RecordTypeId = proRecordTypeId;
        testProperty.Account__c = testAccount.Id;
        testProperty.DoingBusinessAs__c = 'Chemicals';
        testProperty.Status__c ='Active';
        testProperty.RollCode__c ='Unsecured';
        testProperty.AssessorsParcelNumber__c ='1234B567';
        testProperty.DiscoverySource__c ='Audit';
        insert testProperty;

        Id statementRecordTypeId  = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalPropertyStatement').getRecordTypeId();
        List<Statement__c> statementList = new List<Statement__c>();
        Statement__c testStatement = new Statement__c();
        testStatement.RecordTypeId = statementRecordTypeId;
        testStatement.BusinessAccount__c = testProperty.Account__c;
        testStatement.Property__c = testProperty.Id;
        testStatement.FileDate__c = System.today();
        testStatement.VIP_Date_Time_Submitted__c =  system.Now();
        testStatement.Status__c ='Processed';
        testStatement.AssessmentYear__c = String.valueOf(Date.today().year());
        testStatement.Form__c = '571-L';
        insert testStatement;

        Statement__c testStatement2 = new Statement__c();
        testStatement2.RecordTypeId = statementRecordTypeId;
        testStatement2.BusinessAccount__c = testProperty.Account__c;
        testStatement2.Property__c = testProperty.Id;
        testStatement2.FileDate__c = System.today()-1;
        testStatement2.VIP_Date_Time_Submitted__c =  system.Now();
        testStatement2.Status__c ='Processed';
        testStatement2.AssessmentYear__c = String.valueOf(Date.today().year());
        testStatement2.Form__c = '571-L';
        insert testStatement2;
        system.assert(testStatement != Null);
        Test.startTest();
        StatementAssessmentYearHandler.setAmendedForm(testStatement.Id);
        System.assertEquals(null, testStatement2.AmendedFrom__c);
        Test.stopTest();
    }
}