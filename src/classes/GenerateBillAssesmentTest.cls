/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for GenerateBillAssesment APEX Controller which has been used on GenerateDirectBillAssessment Lightning Component
 *
 * @Author: Publicis.Sapient
 *
 * ModifiedBy           ModifiedDate   Description
 * Publicis.Sapient     2020-01-07     Initial development
*/


@isTest
public class GenerateBillAssesmentTest {

    @testSetup
    static void createTestData() {
        User nonAdmin = TestDataUtility.getOfficeAssistantUser();
        User sysAdmin = TestDataUtility.getSystemAdminUser();
        insert new List<User>{ nonAdmin, sysAdmin };
            System.runas(sysAdmin){
                insert new RollYear__c(
                    IsLowValueBulkGeneratorProcessed__c = true,
                    Name                    = String.valueof(FiscalYearUtility.getCurrentFiscalYear()),
                    Status__c               = 'Roll Open',
                    Threshold__c            = 10,
                    Year__c                 = String.valueof(FiscalYearUtility.getCurrentFiscalYear())
                );
                insert new RollYear__c(
                    IsLowValueBulkGeneratorProcessed__c = false,
                    Name                    = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-2),
                    Status__c               = 'Roll Open',
                    Threshold__c            = 11,
                    Year__c                 = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-2)
                );
                insert new RollYear__c(
                    IsLowValueBulkGeneratorProcessed__c = true,
                    Name                    = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1),
                    Status__c               = 'Roll Closed',
                    Threshold__c            = 10,
                    Year__c                 = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1)
                );
            }
        
    }

    // TEST METHODS
    @isTest
    static void testGenerateBillForSysAdmin() {
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c =  'Roll Open' and IsLowValueBulkGeneratorProcessed__c = true];
        RollYear__c rollYear1 = [SELECT Id FROM RollYear__c where IsLowValueBulkGeneratorProcessed__c = false];

        Test.startTest();
        GenerateBillAssesment.Response response;
        GenerateBillAssesment.Response response1;
        GenerateBillAssesment.Response response2;

        System.runAs(systemAdmin) {
            response  = GenerateBillAssesment.generateBill(rollYear.Id);
            response1 = GenerateBillAssesment.runDirectBillAssessmentGenerator();
            response2 = GenerateBillAssesment.generateBill(rollYear1.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e users with System Administrator profile
        // has custom permission "Mass_Generate_Direct_Bill_Assessments"
        System.assertEquals(response.isSuccess, true);
        System.assertEquals(response1.isSuccess, true);
        System.assertEquals(response2.message, CCSFConstants.BUTTONMSG.DIRECT_BILL_ASSESMENT_GENERATOR_WARNING);
    }

    @isTest
    static void testGenerateBillForNonAdmin() {
        User nonAdmin = [SELECT Id FROM User WHERE Email = 'testOfficeAssistantUser@asrtest.com'];
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c =  'Roll Open' and IsLowValueBulkGeneratorProcessed__c = true];
        RollYear__c closedRollYear = [SELECT Id FROM RollYear__c where Status__c= 'Roll Closed'];
        Test.startTest();
        GenerateBillAssesment.Response response;
        GenerateBillAssesment.Response response1;
        System.runAs(nonAdmin) {
            response = GenerateBillAssesment.generateBill(rollYear.Id);
            response1 = GenerateBillAssesment.generateBill(closedRollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e users without System Administrator profile i.e.Office Assistant in this case
        // has not assigned the custom permission "Mass_Generate_Direct_Bill_Assessments"
        System.assertEquals(response.isSuccess, false);
        System.assertEquals(response1.message, CCSFConstants.BUTTONMSG.CLOSED_ROLL_YEAR_BUTTON_WARNING);
    }
}