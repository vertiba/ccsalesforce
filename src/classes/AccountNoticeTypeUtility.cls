/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Utility for calculating notice types on accounts
//-----------------------------
public with sharing class AccountNoticeTypeUtility {
    private static String rollYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
    private static String lastYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
    private static String currentYear = String.valueOf(System.today().year());

    public static Map<Id, String> getNoticeTypesForAccounts(List<Account> accounts){
        return getNoticeTypesForAccounts(accounts, true);
    }

    public static Map<Id, String> getNoticeTypesForAccounts(List<Account> accounts, Boolean processSDRNoticeType){
        Map<Id, Account> accountMap = new Map<Id, Account>(accounts);
        Set<Id> filteredAccountIdsWithDirectBill = new Set<Id>();
        List<Account> accountsToProcess = new List<Account>();
        Map<Id, String> returnMap = new Map<Id, String>();
        
        Set<Id> accountIdsWithNoProperty = accountMap.keySet().clone();//Removing entries from the reference to the keySet removes the entries from the map
        Set<Id> accountsWithPropertyFound = new Set<Id>(); 
        //Added as part of  ASR-9967 
        Set<Id> accountsWithoutPropertyAssessedCost = new Set<Id>(); 
        for(AggregateResult aggregateResult : [SELECT Account__c accountId
                                                FROM Property__c 
                                                WHERE Account__c IN :accountIdsWithNoProperty 
                                                GROUP BY Account__c]){
            
                accountsWithPropertyFound.add((Id)aggregateResult.get('accountId'));
                accountIdsWithNoProperty.remove((Id)aggregateResult.get('accountId'));
        }

        
        Set<Id> accountsWithPropertiesRequiredToFile = new Set<Id>();
        Set<Id> filteredAccountIds = new Set<Id>();
        //this query gives accounts with properties that have `RequiredToFile` as YES or optional
        //We can't query directly for `RequiredToFile = 'No'` as its running on group of Account and return account eventhough only
        //one property on that account having requiretofile as No(which we dont want)
        for(AggregateResult aggregateResult : [SELECT Account__c accountId 
                                                FROM Property__c 
                                                WHERE RequiredToFile__c != 'No' 
                                                AND Account__c in :accountsWithPropertyFound 
                                                GROUP BY Account__c]){
            //send these accounts to further processing and remove from the main recieved account and set it not applicable
            accountsWithPropertiesRequiredToFile.add((Id)aggregateResult.get('accountId'));
            filteredAccountIds.add((Id)aggregateResult.get('accountId'));
            accountsWithPropertyFound.remove((Id)aggregateResult.get('accountId'));
        }
        Set<Id> accountsWithNoPopertyRequiredToFile = accountsWithPropertyFound;
        
        //set notice type for accountIdsWithNoProperty
        if(!accountIdsWithNoProperty.isEmpty())
        {
            for(Id accountId : accountIdsWithNoProperty){
                if(accountMap.containsKey(accountId)){
                    returnMap.put(accountId, CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE);
                }
            }
        }
        
        //set notice type for accountIdsWithNA
        if(!accountsWithNoPopertyRequiredToFile.isEmpty())
        {
            for(Id accountId : accountsWithPropertyFound){
                if(accountMap.containsKey(accountId)){
                    returnMap.put(accountId, CCSFConstants.NOTICE_TYPE_NOT_APPLICABLE);
                }
            }
        }
                
        if(!filteredAccountIds.isEmpty()) {            
            for(AggregateResult aggregateResult : [SELECT Property__r.Account__c accountId 
                                                    FROM Case 
                                                    WHERE Property__r.Account__c IN :filteredAccountIds 
                                                    AND RollYear__c = :lastYear
                                                    AND (Type = :CCSFConstants.ESCAPE_TYPE_ASSESSMENT OR Penalty__c != null OR Adjustmenttype__c = :CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_ROLLCORRECTION) 
                                                    GROUP BY Property__r.Account__c]){
                //discard those account which is having regular case with Penalty
                filteredAccountIds.remove((Id)aggregateResult.get('accountId'));
            }           
        }
                
        filteredAccountIdsWithDirectBill = filteredAccountIds.clone();

        if(!filteredAccountIds.isEmpty())
        {
            for(AggregateResult aggregateResult : [SELECT Account__c accountId 
                                                    FROM Property__c 
                                                    WHERE Account__c in :filteredAccountIds
                                                    AND (PrimaryFormType__c NOT IN ('571-L', '571-LA')
                                                    OR RollCode__c != :CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED) GROUP BY Account__c]){
                //discard those account whose property dont follow the critera supplimental form or roll code
                filteredAccountIds.remove((Id)aggregateResult.get('accountId'));
            }
        }
        
        if(!filteredAccountIdsWithDirectBill.isEmpty())
        {
            for(AggregateResult aggregateResult : [SELECT Account__c accountId 
                                                    FROM Property__c 
                                                    WHERE Account__c IN :filteredAccountIdsWithDirectBill
                                                    AND PrimaryFormType__c NOT IN ('571-L', '571-LA')
                                                    GROUP BY Account__c]) {
                //discard those account whose property dont follow the criteria supplimental form
                filteredAccountIdsWithDirectBill.remove((Id)aggregateResult.get('accountId'));
            }
        }
        
        if(!filteredAccountIds.isEmpty())
        {
            for(Id accountId : filteredAccountIds){
                if(accountMap.containsKey(accountId)){
                    Account account = accountMap.get(accountId);
                    if(account.RequiredToFileYear__c != currentYear && account.TotalValueOfProperties__c >= 1 && account.TotalValueOfProperties__c <= 4000){
                        accountsWithPropertiesRequiredToFile.remove(account.Id);
                        filteredAccountIdsWithDirectBill.remove(accountId);
                        returnMap.put(accountId, CCSFConstants.NOTICE_TYPE_LOW_VALUE);
                    }
                }
            }
        }
        // get the account id for direct bill if Assessed Cost is less than or Zero
        for(Property__c property : [SELECT ID,AssessedCost__c,Account__r.id
                                    FROM Property__c
                                    WHERE Account__c IN: filteredAccountIdsWithDirectBill]){
            if(property.AssessedCost__c == null ||property.AssessedCost__c<= 0 ){
                accountsWithoutPropertyAssessedCost.add(property.Account__r.id);
            }
        }
        if(!filteredAccountIdsWithDirectBill.isEmpty())
        {
            for(Id accountId : filteredAccountIdsWithDirectBill){
                if(accountMap.containsKey(accountId)){
                    Account account = accountMap.get(accountId); 
                    if(account.RequiredToFileYear__c != currentYear && account.TotalDirectBillCost__c >= 1 && account.TotalDirectBillCost__c <= 100000 && !accountsWithoutPropertyAssessedCost.contains(account.id)){
                        accountsWithPropertiesRequiredToFile.remove(account.Id);
                        returnMap.put(accountId, CCSFConstants.NOTICE_TYPE_DIRECT_BILL);
                    }
                }
            }
        }
        if(processSDRNoticeType){
            Set<Id> accountIdsWithSDR = new Set<Id>();
            if(!accountsWithPropertiesRequiredToFile.isEmpty())
            {
                for(AggregateResult aggregateResult : [SELECT Account__c accountId 
                                                        FROM Property__c 
                                                        WHERE Account__c 
                                                        IN :accountsWithPropertiesRequiredToFile
                                                        AND LastFilingMethod__c = :CCSFConstants.NOTICE_TYPE_SDR 
                                                        GROUP BY Account__c]){
                        // taking SDR accounts separately
                        accountIdsWithSDR.add((Id)aggregateResult.get('accountId'));
                        accountsWithPropertiesRequiredToFile.remove((Id)aggregateResult.get('accountId'));
                    }
            }
            
            if(!accountIdsWithSDR.isEmpty())
            {
                for(Id accountId : accountIdsWithSDR){
                    if(accountMap.containsKey(accountId)){
                        returnMap.put(accountId, CCSFConstants.NOTICE_TYPE_SDR);
                    }
                }
            }
        }            
        if(!accountsWithPropertiesRequiredToFile.isEmpty())
        {
            for(Id accountId : accountsWithPropertiesRequiredToFile){
                if(accountMap.containsKey(accountId)){
                    Account acc = accountMap.get(accountId);
                    if(acc.PropertyLocations__r != null && 
                        acc.PropertyLocations__r.size() == 1 && 
                        acc.PropertyLocations__r[0].RecordType.DeveloperName == CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME){
                            returnMap.put(accountId, CCSFConstants.NOTICE_TYPE_NOT_APPLICABLE);
                    }else{
                        returnMap.put(accountId, CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE);
                    }
                }
            }
        }

        return returnMap;
    }
}