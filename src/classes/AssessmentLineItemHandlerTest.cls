/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//                                                                                                                       
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######  
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##    
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##    
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##    
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##    
//                                                                                                                       
//=======================================================================================================================




/***************************************************
 *          ASSESSMENTLINEITEMHANDLERTEST          *
 *   TEST CLASS FOR ASSESSMENT LINE ITEM TRIGGER   *
 * AND ITS HANDLER CLASS ASSESSMENTLINEITEMHANDLER *
 ***************************************************/

@isTest
private class AssessmentLineItemHandlerTest {
   @testSetup
    private static void dataSetup(){
        List<Penalty__c> penalties = new List<Penalty__c>();
        
        Penalty__c penalty1 = new Penalty__c();
        penalty1.PenaltyMaxAmount__c = 500;
        penalty1.Percent__c = 10;
        penalty1.RTCode__c = '463';
        insert penalty1;
        
        Penalty__c penalty2 = new Penalty__c();
        penalty2.PenaltyMaxAmount__c = 500;
        penalty2.Percent__c = 25;
        penalty2.RTCode__c = '214.13';
        penalties.add(penalty2);
            
        Penalty__c penalty3 = new Penalty__c();
        penalty3.PenaltyMaxAmount__c = 500;
        penalty3.Percent__c = 25;
        penalty3.RTCode__c = '504';
        penalties.add(penalty3);
        insert penalties;
        
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Status__c='Roll Open';
        insert rollYear;
        
        
        RollYear__c prevrollYear = new RollYear__c();
        prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Status__c='Roll Closed';
        insert prevrollYear;
        
        RollYear__c nextrollYear = new RollYear__c();
        nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Status__c='Pending';
        insert nextrollYear;
        
        Account accountRecord = new Account();
        accountRecord.Name='New Test Account';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord.EntityId__c = '654321';
        accountRecord.BusinessStatus__c='active';
        accountRecord.MailingCountry__C='US';
        accountRecord.MailingStreetName__c ='355';
        accountRecord.MailingCity__c ='Oakland';
        accountRecord.NoticeType__c='Notice to File';
        accountRecord.RequiredToFile__c='Yes';
        accountRecord.NoticeLastGenerated__c = system.now();
        insert accountRecord;
        
        Property__c propertyRecord = new Property__c();
        propertyRecord.Name = 'new test property';
        propertyRecord.PropertyId__c = '153536';
        propertyRecord.MailingCountry__c = 'US';
        propertyRecord.Account__c = accountRecord.Id;
        insert propertyRecord;
        
        Case caseRecord1 = new Case(); 
        caseRecord1.AccountName__c='new test account';
        caseRecord1.type = 'Regular';
        caseRecord1.AdjustmentType__c = 'Roll Correction';    
        caseRecord1.AccountId = propertyRecord.account__c;
        caseRecord1.ApplyFraudPenalty__c = true;
        caseRecord1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        caseRecord1.Property__c = propertyRecord.Id;
        caseRecord1.Status = 'Closed';
        caseRecord1.SubStatus__c = 'Completed';
        caseRecord1.AssessmentYear__c ='2021';
        caseRecord1.EventDate__c = Date.valueOf('2021-11-26');
        caseRecord1.MailingCountry__c = 'US';
        caseRecord1.Billable__c = 'Yes';
        caseRecord1.Roll__c = prevrollYear.Id;
        caseRecord1.IsLocked__c = true;
        database.insert(caseRecord1);
        
        Case caseRecord2 = new Case();
        caseRecord2.AccountName__c='new test account 1';
        caseRecord2.type = 'Regular';
        caseRecord2.AccountId = propertyRecord.account__c;
        caseRecord2.ApplyFraudPenalty__c = true;
        caseRecord2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        caseRecord2.Property__c = propertyRecord.Id;
        caseRecord2.Status = 'In Progress';
        caseRecord2.AssessmentYear__c ='2021';
        caseRecord2.EventDate__c = Date.valueOf('2021-11-26');
        caseRecord2.MailingCountry__c = 'US';
        database.insert(caseRecord2);
        
        AssessmentLineItem__c lineItem = new AssessmentLineItem__c();
        lineItem.Case__c = caseRecord1.Id;
        lineItem.AcquisitionYear__c = '2017';
        lineItem.AssessmentYear__c = '2018';
        lineItem.Subcategory__c = 'Leasehold Improvements - Structure';
        lineItem.AssetSubclassification__c = '>= 2013 Acquisition (CPI)';
        lineItem.Cost__c = 2700;    
        database.insert(lineItem); 
        
        AssessmentLineItem__c lineItem2 = new AssessmentLineItem__c();
        lineItem2.Case__c = caseRecord2.Id;
        lineItem2.AcquisitionYear__c = '2020';
        lineItem2.AssessmentYear__c = '2021';
        lineItem2.Subcategory__c = 'Leasehold Improvements - Structure';
        lineItem2.AssetSubclassification__c = CCSFConstants.Not_Assessesed_by_BPP;
        lineItem2.Cost__c = 2700;
        database.insert(lineItem2);
        System.debug('@@'+lineItem2);
        
		FactorSource__c factorSource = new FactorSource__c();
        factorSource.Name = 'RP CPI';
        database.insert(factorSource);
            
        Factor__c factor = new Factor__c();
        factor.Name = 'Real Property CPI';
        factor.FactorSource__c = factorSource.Id;
        factor.AcquisitionYear__c = '2017';
        factor.AssessmentYear__c = '2018';
        factor.FactorPercent__c = Decimal.valueOf(106);
        database.insert(factor);
            
        BusinessAssetMapping__c businessAssetMapping = new BusinessAssetMapping__c();
        businessAssetMapping.AssetClassification__c = 'Leasehold Improvements - Structure';
        businessAssetMapping.AssetSubclassification__c = '>= 2013 Acquisition (CPI)';
        businessAssetMapping.FactorSource__c = factorSource.Id;
        businessAssetMapping.FactorType__c = 'Depreciation Factor';
        businessAssetMapping.FixturesAllocationPercent__c = 100;
        database.insert(businessAssetMapping);
    }
    
    /*
     * Method Name:setDepreciationFactorTest
     * Description: Test Method to cover the CPI factor Changes as part of ASR-10027
     */ 
    public static TestMethod void setDepreciationFactorTest(){
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='New Admin',alias='nadmin',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='newadmin@test.com',UserName='newadmin@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){

            AssessmentLineItem__c lineItem2 = [Select Id, SubCategory__c from AssessmentLineItem__c where AcquisitionYear__c = '2020' ];
            AssessmentLineItem__c closedLineItem = [Select Id,Value__c,Sent_to_RP__c,Category__c from AssessmentLineItem__c where Id=:lineItem2.Id];
            system.assertEquals(0,closedLineItem.Value__c );
            PropertyCategoryMapping__mdt propertyCategoryMdt = [SELECT ID,SubCategory__c,PropertyCategory__c FROM PropertyCategoryMapping__mdt WHERE SubCategory__c=: lineItem2.Subcategory__c ];
            system.assertEquals(closedLineItem.Category__c,propertyCategoryMdt.PropertyCategory__c );
            system.assertEquals(true,closedLineItem.Sent_to_RP__c );
        }
        
    }
    
    /*
	// positive test
    @isTest
    private static void should_SetLineItemDepreciationFactor_When_RelevantDataExists(){

        Id accRecordTypeId                              = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Id propertyRecordTypeId                         = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Id caseRecordTypeId                             = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Id factorRecordTypeId                           = Schema.SObjectType.Factor__c.getRecordTypeInfosByDeveloperName().get('DepreciationFactor').getRecordTypeId();

        Account testAccount                             = new Account();
        testAccount.Name                                = 'Test Account';
        testAccount.RecordTypeId                        = accRecordTypeId;
        testAccount.BusinessStatus__c                   = 'Active';
        new TestDataFactory(testAccount).populateRequiredFields();
        insert testAccount;


        Property__c testProperty                        = new Property__c();
        testProperty.Name                               = 'Test Property';
        testProperty.RecordTypeId                       = propertyRecordTypeId;
        testProperty.Account__c                         = testAccount.Id;
        new TestDataFactory(testProperty).populateRequiredFields();
        insert testProperty;

       
        Case testCase                                   = new Case();
        testCase.RecordTypeId                           = caseRecordTypeId;
        testCase.AccountId                              = testAccount.Id;
        testCase.Property__c                            = testProperty.Id;
        testCase.AssessmentYear__c                      = String.valueOf(Date.today().year()); 
        new TestDataFactory(testCase).populateRequiredFields();
        insert testCase;
            

        FactorCategory__c testFactorCat                 = new FactorCategory__c();
        testFactorCat.Name                              = 'Test Category';
        new TestDataFactory(testFactorCat).populateRequiredFields();
        insert testFactorCat;


        FactorSubcategory__c testFactorSubCat           = new FactorSubcategory__c();
        testFactorSubCat.Name                           = 'test Sub Category';
        testFactorSubCat.FactorCategory__c              = testFactorCat.Id;
        new TestDataFactory(testFactorSubCat).populateRequiredFields();
        insert testFactorSubCat;


        Factor__c testFactor                            = new Factor__c();
        testFactor.RecordTypeId                         = factorRecordTypeId;
        testFactor.FactorPercent__c                     = 80;
        testFactor.FactorSubcategory__c                 = testFactorSubCat.Id; 
        testFactor.YearsOfUsefulLife__c                 = 5; 
        testFactor.AssessmentYear__c                    = [SELECT AssessmentYear__c FROM Case WHERE Id =:testCase.Id].AssessmentYear__c;
        testFactor.AcquisitionYear__c                   = String.valueOf(Date.today().addYears(-3).year()); 
        new TestDataFactory(testFactor).populateRequiredFields();
        insert testFactor;
        
       
        AssessmentLineItem__c testAssmtLineItem         = new AssessmentLineItem__c();
        testAssmtLineItem.AcquisitionYear__c            = String.valueOf(Date.today().addYears(-3).year());
        testAssmtLineItem.UsefulLife__c                 = 5;
        testAssmtLineItem.DepreciationCategory__c       = testFactorCat.Id;
        testAssmtLineItem.DepreciationSubcategory__c    = testFactorSubCat.Id;
        testAssmtLineItem.Case__c                       = testCase.Id;
        new TestDataFactory(testAssmtLineItem).populateRequiredFields();

        
        Test.startTest();
        insert testAssmtLineItem;
        Test.stopTest();

        AssessmentLineItem__c result                    = [SELECT DepreciationFactor__c FROM AssessmentLineItem__c WHERE Id =:testAssmtLineItem.Id];
        System.assert(result.DepreciationFactor__c != null);
        System.assertEquals(testFactor.Id, result.DepreciationFactor__c);


    }

    //negative case
    @isTest
    private static void shouldNot_SetLineItemDepreciationFactor_When_RelevantDataNotExists(){

        Id accRecordTypeId                              = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Id propertyRecordTypeId                         = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Id caseRecordTypeId                             = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Id factorRecordTypeId                           = Schema.SObjectType.Factor__c.getRecordTypeInfosByDeveloperName().get('DepreciationFactor').getRecordTypeId();

        Account testAccount                             = new Account();
        testAccount.Name                                = 'Test Account';
        testAccount.RecordTypeId                        = accRecordTypeId;
        testAccount.BusinessStatus__c                   = 'Active';
        new TestDataFactory(testAccount).populateRequiredFields();
        insert testAccount;


        Property__c testProperty                        = new Property__c();
        testProperty.Name                               = 'Test Property';
        testProperty.RecordTypeId                       = propertyRecordTypeId;
        testProperty.Account__c                         = testAccount.Id;
        new TestDataFactory(testProperty).populateRequiredFields();
        insert testProperty;

       
        Case testCase                                   = new Case();
        testCase.RecordTypeId                           = caseRecordTypeId;
        testCase.AccountId                              = testAccount.Id;
        testCase.Property__c                            = testProperty.Id;
        testCase.AssessmentYear__c                      = String.valueOf(Date.today().year()); 
        new TestDataFactory(testCase).populateRequiredFields();
        insert testCase;
            

        FactorCategory__c testFactorCat                 = new FactorCategory__c();
        testFactorCat.Name                              = 'Test Category';
        new TestDataFactory(testFactorCat).populateRequiredFields();
        insert testFactorCat;


        FactorSubcategory__c testFactorSubCat           = new FactorSubcategory__c();
        testFactorSubCat.Name                           = 'test Sub Category';
        testFactorSubCat.FactorCategory__c              = testFactorCat.Id;
        new TestDataFactory(testFactorSubCat).populateRequiredFields();
        insert testFactorSubCat;


        Factor__c testFactor                            = new Factor__c();
        testFactor.RecordTypeId                         = factorRecordTypeId;
        testFactor.FactorPercent__c                     = 80;
        testFactor.FactorSubcategory__c                 = testFactorSubCat.Id; 
        testFactor.YearsOfUsefulLife__c                 = 4; 
        testFactor.AssessmentYear__c                    = [SELECT AssessmentYear__c FROM Case WHERE Id =:testCase.Id].AssessmentYear__c;
        testFactor.AcquisitionYear__c                   = String.valueOf(Date.today().addYears(-3).year()); 
        new TestDataFactory(testFactor).populateRequiredFields();
        insert testFactor;
        
       
        AssessmentLineItem__c testAssmtLineItem         = new AssessmentLineItem__c();
        testAssmtLineItem.AcquisitionYear__c            = String.valueOf(Date.today().addYears(-3).year());
        testAssmtLineItem.UsefulLife__c                 = 5;
        testAssmtLineItem.DepreciationCategory__c       = testFactorCat.Id;
        testAssmtLineItem.DepreciationSubcategory__c    = testFactorSubCat.Id;
        testAssmtLineItem.Case__c                       = testCase.Id;
        new TestDataFactory(testAssmtLineItem).populateRequiredFields();

        
        Test.startTest();
        insert testAssmtLineItem;
        Test.stopTest();

        AssessmentLineItem__c result                    = [SELECT DepreciationFactor__c FROM AssessmentLineItem__c WHERE Id =:testAssmtLineItem.Id];
        System.assert(result.DepreciationFactor__c == null);
       
    }

    //Unit test when there are multiple assessment line items that should be assigned the same factor
    @isTest
    private static void should_SetAllLineItemDepreciationFactorSame_When_RelevantDataExists(){

        Id accRecordTypeId                              = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Id propertyRecordTypeId                         = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Id caseRecordTypeId                             = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Id factorRecordTypeId                           = Schema.SObjectType.Factor__c.getRecordTypeInfosByDeveloperName().get('DepreciationFactor').getRecordTypeId();

        Account testAccount                             = new Account();
        testAccount.Name                                = 'Test Account';
        testAccount.RecordTypeId                        = accRecordTypeId;
        testAccount.BusinessStatus__c                   = 'Active';
        new TestDataFactory(testAccount).populateRequiredFields();
        insert testAccount;


        Property__c testProperty                        = new Property__c();
        testProperty.Name                               = 'Test Property';
        testProperty.RecordTypeId                       = propertyRecordTypeId;
        testProperty.Account__c                         = testAccount.Id;
        new TestDataFactory(testProperty).populateRequiredFields();
        insert testProperty;

       
        Case testCase                                   = new Case();
        testCase.RecordTypeId                           = caseRecordTypeId;
        testCase.AccountId                              = testAccount.Id;
        testCase.Property__c                            = testProperty.Id;
        testCase.AssessmentYear__c                      = String.valueOf(Date.today().year()); 
        new TestDataFactory(testCase).populateRequiredFields();
        insert testCase;
            

        FactorCategory__c testFactorCat                 = new FactorCategory__c();
        testFactorCat.Name                              = 'Test Category';
        new TestDataFactory(testFactorCat).populateRequiredFields();
        insert testFactorCat;


        FactorSubcategory__c testFactorSubCat           = new FactorSubcategory__c();
        testFactorSubCat.Name                           = 'test Sub Category';
        testFactorSubCat.FactorCategory__c              = testFactorCat.Id;
        new TestDataFactory(testFactorSubCat).populateRequiredFields();
        insert testFactorSubCat;


        Factor__c testFactor                            = new Factor__c();
        testFactor.RecordTypeId                         = factorRecordTypeId;
        testFactor.FactorPercent__c                     = 80;
        testFactor.FactorSubcategory__c                 = testFactorSubCat.Id; 
        testFactor.YearsOfUsefulLife__c                 = 5; 
        testFactor.AssessmentYear__c                    = [SELECT AssessmentYear__c FROM Case WHERE Id =:testCase.Id].AssessmentYear__c;
        testFactor.AcquisitionYear__c                   = String.valueOf(Date.today().addYears(-3).year()); 
        new TestDataFactory(testFactor).populateRequiredFields();
        insert testFactor;
        
        List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
        AssessmentLineItem__c testAssmtLineItem ;
        for(Integer count =0 ; count<5 ; count++){
            testAssmtLineItem                               = new AssessmentLineItem__c();
            testAssmtLineItem.AcquisitionYear__c            = String.valueOf(Date.today().addYears(-3).year());
            testAssmtLineItem.UsefulLife__c                 = 5;
            testAssmtLineItem.DepreciationCategory__c       = testFactorCat.Id;
            testAssmtLineItem.DepreciationSubcategory__c    = testFactorSubCat.Id;
            testAssmtLineItem.Case__c                       = testCase.Id;
            new TestDataFactory(testAssmtLineItem).populateRequiredFields();
            assessmentLineItems.add(testAssmtLineItem);
        }
        

        
        Test.startTest();
        insert assessmentLineItems;
        Test.stopTest();

        List<AssessmentLineItem__c> result                    = [SELECT DepreciationFactor__c FROM AssessmentLineItem__c WHERE Id IN:assessmentLineItems];
        System.assert(result.size() == 5);
        system.debug(result);
        for(Integer count =0 ; count<5 ; count++){
            System.assertEquals(testFactor.Id, result.get(count).DepreciationFactor__c);
        }
       
    }

    //Unit test when there are multiple assessment line items that should be assigned different factors
    @isTest
    private static void should_SetAllLineItemDepreciationFactorDifferent_When_RelevantDataExists(){

        Id accRecordTypeId                              = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Id propertyRecordTypeId                         = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Id caseRecordTypeId                             = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Id factorRecordTypeId                           = Schema.SObjectType.Factor__c.getRecordTypeInfosByDeveloperName().get('DepreciationFactor').getRecordTypeId();

        Account testAccount                             = new Account();
        testAccount.Name                                = 'Test Account';
        testAccount.RecordTypeId                        = accRecordTypeId;
        testAccount.BusinessStatus__c                   = 'Active';
        new TestDataFactory(testAccount).populateRequiredFields();
        insert testAccount;


        Property__c testProperty                        = new Property__c();
        testProperty.Name                               = 'Test Property';
        testProperty.RecordTypeId                       = propertyRecordTypeId;
        testProperty.Account__c                         = testAccount.Id;
        new TestDataFactory(testProperty).populateRequiredFields();
        insert testProperty;

       
        Case testCase                                   = new Case();
        testCase.RecordTypeId                           = caseRecordTypeId;
        testCase.AccountId                              = testAccount.Id;
        testCase.Property__c                            = testProperty.Id;
        testCase.AssessmentYear__c                      = String.valueOf(Date.today().year()); 
        new TestDataFactory(testCase).populateRequiredFields();
        insert testCase;
            

        FactorCategory__c testFactorCat                 = new FactorCategory__c();
        testFactorCat.Name                              = 'Test Category';
        new TestDataFactory(testFactorCat).populateRequiredFields();
        insert testFactorCat;


        FactorSubcategory__c testFactorSubCat           = new FactorSubcategory__c();
        testFactorSubCat.Name                           = 'test Sub Category';
        testFactorSubCat.FactorCategory__c              = testFactorCat.Id;
        new TestDataFactory(testFactorSubCat).populateRequiredFields();
        insert testFactorSubCat;

        List<Factor__c> factors                         = new List<Factor__c>();
        Factor__c testFactor;
        for(Integer count =0 ; count<5 ; count++){
            testFactor                                  = new Factor__c();
            testFactor.RecordTypeId                     = factorRecordTypeId;
            testFactor.FactorPercent__c                 = 80;
            testFactor.FactorSubcategory__c             = testFactorSubCat.Id; 
            testFactor.YearsOfUsefulLife__c             = count+5; 
            testFactor.AssessmentYear__c                = [SELECT AssessmentYear__c FROM Case WHERE Id =:testCase.Id].AssessmentYear__c;
            testFactor.AcquisitionYear__c               = String.valueOf(Date.today().addYears(-3).year()); 
            new TestDataFactory(testFactor).populateRequiredFields();
            factors.add(testFactor);
        }                           
        insert factors;
        
        List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
        AssessmentLineItem__c testAssmtLineItem ;
        for(Integer count =0 ; count<5 ; count++){
            testAssmtLineItem                               = new AssessmentLineItem__c();
            testAssmtLineItem.AcquisitionYear__c            = String.valueOf(Date.today().addYears(-3).year());
            testAssmtLineItem.UsefulLife__c                 = count+5;
            testAssmtLineItem.DepreciationCategory__c       = testFactorCat.Id;
            testAssmtLineItem.DepreciationSubcategory__c    = testFactorSubCat.Id;
            testAssmtLineItem.Case__c                       = testCase.Id;
            new TestDataFactory(testAssmtLineItem).populateRequiredFields();
            assessmentLineItems.add(testAssmtLineItem);
        }
        

        
        Test.startTest();
        insert assessmentLineItems;
        Test.stopTest();

        List<AssessmentLineItem__c> result                    = [SELECT DepreciationFactor__c FROM AssessmentLineItem__c WHERE Id IN:assessmentLineItems];
        System.assert(result.size() == 5);
        system.debug(result);
        for(Integer count =0 ; count<5 ; count++){
            System.assertEquals(factors.get(count).Id, result.get(count).DepreciationFactor__c);
        }
       
    }
*/
 
    @isTest
    private static void testpreventToDeleteLockedLineItems(){
        
        List<User> users = new List<User>();
        User auditorUser = TestDataUtility.getBPPAuditorUser();
        User sysadmin = TestDataUtility.getSystemAdminUser();
        
        Account testAccount = [Select id,Name from account where Name = 'New Test Account' LIMIT 1];
        Property__c testproperty = [Select id, Account__c from Property__c where Account__c =:testAccount.id LIMIT 1];
        Penalty__c testpenalty = [Select id, PenaltyMaxAmount__c, Percent__c from Penalty__c where RTCode__c =:'463'];
        
        Case cs = [Select Id,Type from Case where Type = 'Regular' and AdjustmentType__c = 'Roll Correction' LIMIT 1];
        List<AssessmentLineItem__c> lineItem = [Select Id,AcquisitionYear__c,isLocked__c from AssessmentLineItem__c where AcquisitionYear__c = '2017' LIMIT 1];
        
        System.runAs(auditorUser){
            Test.startTest();
            try{
                AssessmentLineItemHandler lineHandler = new AssessmentLineItemHandler();
                lineHandler.preventToDeleteLockedLineItems(lineItem);
                System.assert(true,System.Label.Locked_Record_Error_Message);
            }catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            Test.stopTest();
        }    
    } 
}