/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR 2366
// @description : This class is used to test class for StagingAumentumBusiness trigger handler
//-----------------------------
@isTest
public with sharing class StagingAumentumBusinessHandlerTest {
   
    @TestSetup   
    static void createTestData() {

        List<Account> accounts= new List<Account>();       
        Account account1 =TestDataUtility.buildAccountAumentum('Jill JK', 'BAN0002', '11-2345678', Date.newInstance(1998,10,01), null);
        Account account2 = TestDataUtility.buildAccountAumentum('MK Ahmed', 'BAN0003', '11-2345679', null, null);
        Account account3 = TestDataUtility.buildAccountAumentum('Miss Cotton Candies', 'BAN0004', '11-2345670', Date.newInstance(1998,10,01), null);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);          
        insert accounts;
        
        List<Property__c> properties = new List<Property__c>();       
        Property__c property1=TestDataUtility.buildAumentumProperty('LIN003','DBA003','571-L',  Date.newInstance(1998,04,01),null,account2.Id, 'Mango Valley1151ST35FLNW');
        property1.LocationPostalCode__c = '94103';         
        properties.add(property1);      
        insert properties;

        List<StagingAumentumBusiness__c> stagings= new List<StagingAumentumBusiness__c>();
        StagingAumentumBusiness__c staging1 = TestDataUtility.buildStagingAumentumBusiness('BAN0001','LIN001','11-2345671','DBA001', 'Jack & Jill','CA','SAN FRANCISCO','0','0','0','1155 Market','1998-10-10',null,'1998-10-10',null);
        StagingAumentumBusiness__c staging2 = TestDataUtility.buildStagingAumentumBusiness('BAN0002','LIN002','11-2345678','DBA002', 'Jill JK','CA','SAN FRANCISCO','0','1','1','1176 Mango','1998-10-10',null,'1998-10-10',null);
        StagingAumentumBusiness__c staging3 = TestDataUtility.buildStagingAumentumBusiness('BAN0003','LIN003','11-2345679','DBA003', 'MK Ahmed','CA','SAN FRANCISCO','1','0','0','Mango Valley1151ST35FLNW','1998-10-10',null,'1998-10-10',null);
        StagingAumentumBusiness__c staging4=TestDataUtility.buildStagingAumentumBusiness('BAN0004','LIN004','11-2345670','DBA004','Miss Cotton Candies','CA','SAN FRANCISCO','1','1','0','1155 Market Street','1998-10-10',null,'1998-10-10',null);
        StagingAumentumBusiness__c staging5=TestDataUtility.buildStagingAumentumBusiness('BAN0005','LIN005','11-2345676','DBA005','Miss Cotton','CA','SAN FRANCISCO','1','1','0','1155 Market Street','10/10/1998',null,'1998-10-10',null);
        staging4.MappedAccount__c= account3.Id;
        stagings.add(staging1);
        stagings.add(staging2);
        stagings.add(staging3);
        stagings.add(staging4);
        stagings.add(staging5);
        insert stagings;        

    }
     @isTest
    static void createNewAccount(){
        StagingAumentumBusiness__c staging=[Select Id, CreateNewAccount__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0001'];
        staging.CreateNewAccount__c = true;
        test.startTest();       
        update staging;       
        test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, MappedAccount__c,CreateNewAccount__c, Status__c from StagingAumentumBusiness__c where Id =: staging.Id];
      
         Account accountRecord=[Select Id from Account where BusinessAccountNumber__c='BAN0001'];
         system.assertEquals('Warning',stageResult.Status__c,'Property not created' );
         system.assertEquals(accountRecord.Id, stageResult.MappedAccount__c,'Account created');

    }

    @isTest
    static void mapAccountIfSmartHasAccount(){
        StagingAumentumBusiness__c staging=[Select Id, CreateNewAccount__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0002'];
        staging.CreateNewAccount__c = true;
        test.startTest();
        update staging;
        test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, MappedAccount__c, Status__c from StagingAumentumBusiness__c where Id =: staging.Id];
        Account accountRecord=[Select Id from Account where BusinessAccountNumber__c='BAN0002'];
        system.assertEquals('Warning',stageResult.Status__c,'Property should be created by createNewProperty checkbox' );
        system.assertEquals(accountRecord.Id, stageResult.MappedAccount__c,'Account Mapped');

    }
  
    @isTest
    static void createProperty(){
        StagingAumentumBusiness__c staging=[Select Id, CreateNewProperty__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0001'];
        staging.CreateNewProperty__c = true;
        test.startTest();
        update staging;
        test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, MappedAccount__c, MappedProperty__c, Status__c from StagingAumentumBusiness__c where Id =: staging.Id];
        Account accountRecord=[Select Id from Account where BusinessAccountNumber__c='BAN0001'];
        Property__c property=[Select Id,Account__c, LocationIdentificationNumber__c from Property__c where LocationIdentificationNumber__c = 'LIN001' ];
        system.assertEquals('Processed',stageResult.Status__c ,'Processed with property and Account');
        system.assertEquals(accountRecord.Id, stageResult.MappedAccount__c,'Account Created');  
        system.assertEquals(property.Id, stageResult.MappedProperty__c,'Property Created');

    }
    @isTest
    static void createPropertyWhenMappedAccountIsNotNull(){
        StagingAumentumBusiness__c staging=[Select Id, CreateNewProperty__c,MappedAccount__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0004'];
        staging.CreateNewProperty__c = true;
        test.startTest();
        update staging;
        test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, MappedAccount__c, MappedProperty__c, Status__c from StagingAumentumBusiness__c where Id =: staging.Id];
        Account accountRecord=[Select Id from Account where BusinessAccountNumber__c='BAN0004'];
        Property__c property=[Select Id,Account__c, LocationIdentificationNumber__c from Property__c where LocationIdentificationNumber__c = 'LIN004' ];
        system.assertEquals('Processed',stageResult.Status__c ,'Processed with property and Account');
        system.assertEquals(accountRecord.Id, stageResult.MappedAccount__c,'Account Mapped');  
        system.assertEquals(property.Id, stageResult.MappedProperty__c,'Property Created');
        system.assertEquals(accountRecord.Id, property.Account__c,'Child of same account');

    }

    @isTest
    static void createPropertyAccountAlreadyInSmart (){
        StagingAumentumBusiness__c staging=[Select Id, CreateNewProperty__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0002'];
        staging.CreateNewProperty__c = true;
        test.startTest();
        update staging;
        test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, MappedAccount__c, MappedProperty__c, Status__c from StagingAumentumBusiness__c where Id =: staging.Id];
        Account accountRecord=[Select Id from Account where BusinessAccountNumber__c='BAN0002'];
        Property__c property=[Select Id,Account__c, LocationIdentificationNumber__c from Property__c where LocationIdentificationNumber__c = 'LIN002' ];
        system.assertEquals('Processed',stageResult.Status__c ,'Processed with property and Account');
        system.assertEquals(accountRecord.Id, stageResult.MappedAccount__c,'Account Not Inserted but Mapped');  
        system.assertEquals(property.Id, stageResult.MappedProperty__c,'Property Created');
        system.assertEquals(accountRecord.Id, property.Account__c,'Property is mapped with Account');

    } 

    @isTest
    static void propertyAndAccountMappedAlreadyInSmart(){
        StagingAumentumBusiness__c staging=[Select Id, CreateNewProperty__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0003'];
        staging.CreateNewProperty__c = true;
        test.startTest();
        update staging;
        test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, MappedAccount__c, MappedProperty__c, Status__c,Messages__c from StagingAumentumBusiness__c where Id =: staging.Id];
        Account accountRecord=[Select Id from Account where BusinessAccountNumber__c='BAN0003'];
        Property__c property=[Select Id,Account__c, LocationIdentificationNumber__c from Property__c where LocationIdentificationNumber__c = 'LIN003' ];
        system.debug('stageResult'+ stageResult.messages__c);
        system.assertEquals('Processed',stageResult.Status__c ,'Processed with property and Account');
        system.assertEquals(accountRecord.Id, stageResult.MappedAccount__c,'Account Not Inserted but Mapped');  
        system.assertEquals(property.Id, stageResult.MappedProperty__c,'Property Not created but Mapped');
        system.assertEquals(accountRecord.Id, property.Account__c,'Property is mapped with Account');

    }

    @isTest
    static void statusIgnored(){
        StagingAumentumBusiness__c staging=[Select Id, UserAction__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0003'];  
        staging.UserAction__c='Ignore Record';
        test.startTest();
        update staging;
        test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, Status__c from StagingAumentumBusiness__c where Id =: staging.Id];
        system.assertEquals('Ignored', stageResult.Status__c,'Status Ignored');

    }

    @isTest
    static void statusReprocessed(){
        StagingAumentumBusiness__c staging=[Select Id, UserAction__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0003'];  
        staging.UserAction__c='Ignore Warning';
        test.startTest();
        update staging;
        test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, Status__c from StagingAumentumBusiness__c where Id =: staging.Id];
        system.assertEquals('Reprocessed', stageResult.Status__c,'Status Reprocessed');

    }

    @isTest
    static void DMLFailedTest(){
        StagingAumentumBusiness__c staging=[Select Id, CreateNewProperty__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0005'];  
        staging.CreateNewProperty__c =true;
        test.startTest();
        try{
            update staging;            
        }catch(Exception e){
            System.assert(e.getMessage().contains('invalid date'));
        }        
       test.stopTest();
        StagingAumentumBusiness__c stageResult= [Select Id, Status__c from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0005'];  
		system.assertEquals('Failed', stageResult.Status__c);
    } 

}