/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//          test Class for ErrorChatterPostNotifications Apex class
// 

@istest
public with sharing class ErrorChatterPostNotificationsTest {
   
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method to create Test Data
   
    @testSetup
    private static void dataSetup(){
        TestDataUtility.disableAutomationCustomSettings(true);

        List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty1 = new Penalty__c();
        penalty1.PenaltyMaxAmount__c = 500;
        penalty1.Percent__c = 10;
        penalty1.RTCode__c = '463';
        penalties.add(penalty1);

        insert penalties;
        Account account = new Account();
        account.Name='new test account';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.MailingStreetName__c ='355';
        account.MailingCity__c ='Oakland';
        account.NoticeType__c='Notice to File';
        account.RequiredToFile__c='Yes';
        account.NoticeLastGenerated__c = system.now();
        insert account;

        Property__C property = new Property__C();
        property.Name = 'new test property';
        property.MailingCountry__c = 'US';
        property.Account__c = account.Id;
        insert property;


        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Status__c='Roll Open';
        insert rollYear;

        TestDataUtility.disableAutomationCustomSettings(false);
		
        ChatterNotifications__c settings = ChatterNotifications__c.getOrgDefaults();
        settings.EnableChatterNotitfication__c = True;
        upsert settings ChatterNotifications__c.Id;
        
        List<Case> casesToInsert = new List<Case>();
        Id bppAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Case cas1 = new Case();
        cas1.AccountName__c='new test account';
        cas1.type = 'Regular';
        cas1.AdjustmentType__c = 'New';
        cas1.AccountId = property.account__c;
        cas1.ApplyFraudPenalty__c = true;
        cas1.ApplyPenaltyReason__c = 'Test Class';
        cas1.RecordTypeId = bppAssessmentRecordTypeId;
        cas1.Property__c = property.Id;
        cas1.Status = 'New';
        cas1.EventDate__c =  Date.newInstance(FiscalYearUtility.getCurrentFiscalYear(), 1, 1);
        cas1.MailingCountry__c = 'US';
        casesToInsert.add(cas1);
        insert casesToInsert;
    }
        
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method to check the failures 
    @isTest
    private static void checkDMLfailuresTestForChatterNotifications(){
    	List<Case> assessmentList  = new List<Case>();
        Case cas= [SELECT id,eventDate__c from Case Limit 1];
        cas.EventDate__c = Date.newInstance(FiscalYearUtility.getCurrentFiscalYear() - 1, 1, 1);
        assessmentList.add(cas);
        
        
        
        Test.StartTest();
        	List<Database.SaveResult> dmlResults  = Database.update(assessmentList,false);
        	ErrorChatterPostNotifications.createFailedDMLChatterNotification(dmlResults,assessmentList);
        	ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
        	List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
        	testItemList.add(new ConnectApi.FeedItem());
        	testPage.elements = testItemList;
            
        	// Set the test data
            ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null,ConnectApi.FeedType.Record, cas.id, testPage);
            ConnectApi.FeedElementPage elements = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(null,ConnectApi.FeedType.Record, cas.Id);
        	
        	System.assertEquals(1,elements.elements.size());

        Test.StopTest();
    }
}