/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test PermissionSetUtility
//-----------------------------
@isTest
private class  PermissionSetUtilityTest {

    @isTest
    private static void createPermissionSetTest() {
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User user = new User(
            Alias             = 'standt',
            Email             = 'standarduser@testorg.com',
            EmailEncodingKey  = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LastName          = 'Testing',
            LocaleSidKey      = 'en_US',
            ProfileId         = profile.Id,
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName          = 'test.permissionset.user@testorg.com'
        );
        insert user;

        PermissionSet permissionSet = [SELECT Id,name FROM PermissionSet WHERE Name = 'CRMUserPsl'];

        //setting up invocableVariables
        List<PermissionSetUtility.PermissionSetInput> invocableVariables = new List<PermissionSetUtility.PermissionSetInput>();
        PermissionSetUtility.PermissionSetInput invocableVariable = new PermissionSetUtility.PermissionSetInput();
        invocableVariable.userId = user.id;
        invocableVariable.permissionSetName = permissionSet.Name;
        invocableVariables.add(invocableVariable);

        Test.startTest();
        PermissionSetUtility.createPermissionSet(invocableVariables);
        Test.stopTest();

        List<PermissionSetAssignment> assignments = [
            SELECT Id, AssigneeId, PermissionSetId
            FROM PermissionSetAssignment
            WHERE AssigneeId = :user.id
            AND PermissionSetId = :permissionSet.id
        ];
        System.assertEquals(1, assignments.size(), 'Exactly one entry should be there inside PermissionSetAssignment');
    }

}