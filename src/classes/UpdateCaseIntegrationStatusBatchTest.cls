/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test UpdateCaseIntegrationStatusBatch
//-----------------------------
@isTest
private class UpdateCaseIntegrationStatusBatchTest {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
        private static void dataSetup(){
            TestDataUtility.disableAutomationCustomSettings(true);
            //insert penalty
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
                    
            //insert account
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            List<Property__c> properties = new List<Property__c>();
            Property__c property = TestDataUtility.getBPPProperty();
            property.Account__c = businessAccount.Id;
            properties.add(property);
            
            Property__c property2 = TestDataUtility.getBPPProperty();
            property2.Account__c = businessAccount.Id;
            property2.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            properties.add(property2);
            insert properties;
            
            String currentYear 		= String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            String lastYear 		= String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            List<RollYear__c> rollYears = new List<RollYear__c>();
            RollYear__c currentRollYear = TestDataUtility.buildRollYear(currentYear,currentYear,CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            currentRollYear.IntegrationStatus__c=CCSFConstants.IN_TRANSIT_TO_TTX;
            rollYears.add(currentRollYear);
            RollYear__c lastRollYear = TestDataUtility.buildRollYear(lastYear,lastYear,CCSFConstants.ROLLYEAR.ROLL_YEAR_Close_STATUS);
            lastRollYear.IntegrationStatus__c=CCSFConstants.IN_TRANSIT_TO_TTX;
            rollYears.add(lastRollYear);
            insert rollYears;
            
            Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME) LIMIT 1];
            List<case> cases = new List<case>();   
            Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(
                Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,
                Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
            assessment.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment.SubType__c = 'Non-Assessable (Sold)';
            assessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment.AccountId = property2.account__c;
            assessment.Property__c = property2.Id;
            cases.add(assessment);
            Case assessment2  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment2.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment2.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
            assessment2.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment2.SubType__c = 'Low Value';
            assessment2.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment2.AccountId = property.account__c;
            assessment2.Property__c = property.Id;
            cases.add(assessment2);
            
            Case assessment3  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment3.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment3.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
            assessment3.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment3.SubType__c = 'Closeout';
            assessment3.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment3.AccountId = property2.account__c;
            assessment3.Property__c = property2.Id;
            cases.add(assessment3);
            
            Case assessment4  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment4.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment4.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
            assessment4.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment4.SubType__c = 'Closeout';
            assessment4.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment4.AccountId = property.account__c;
            assessment4.Property__c = property.Id;
            cases.add(assessment4);
            
            Case assessment5  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment5.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment5.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
            assessment5.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment5.AdjustmentType__c = 'Roll Correction';
            assessment5.AccountId = property2.account__c;
            assessment5.Property__c = property2.Id;
            cases.add(assessment5);
            
            Case assessment6  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment6.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment6.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
            assessment6.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment6.TotalSignsCameraTVetcValue__c = 100;
            assessment6.AdjustmentType__c = 'Roll Correction';
            assessment6.AccountId = property.account__c;
            assessment6.Property__c = property.Id;
            cases.add(assessment6);
            
            //taking escape cases
            Case assessment7  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment7.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment7.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment7.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment7.WaiveNoticePeriod__c = false;
            assessment7.Billable__c = 'Yes';
            assessment7.TotalSignsCameraTVetcValue__c = 100;
            assessment7.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment7.AccountId = property2.account__c;
            assessment7.Property__c = property2.Id;
            cases.add(assessment7);
            
            Case assessment8  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment8.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment8.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment8.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment8.WaiveNoticePeriod__c = false;
            assessment8.Billable__c = 'Yes';
            assessment8.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment8.AccountId = property.account__c;
            assessment8.Property__c = property.Id;
            cases.add(assessment8);
            
            Case assessment9  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment9.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment9.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment9.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment9.WaiveNoticePeriod__c = true;
            assessment9.Billable__c = 'Yes';
            assessment9.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment9.AccountId = property2.account__c;
            assessment9.Property__c = property2.Id;
            cases.add(assessment9);
            
            Case assessment10  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment10.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment10.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment10.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment10.WaiveNoticePeriod__c = true;
            assessment10.Billable__c = 'Yes';
            assessment10.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment10.AccountId = property.account__c;
            assessment10.Property__c = property.Id;
            cases.add(assessment10);
            
            Case assessment11  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment11.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment11.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment11.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment11.SubType__c = 'Closeout';
            assessment11.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment11.WaiveNoticePeriod__c = true;
            assessment11.Billable__c = 'Yes';
            assessment11.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment11.AccountId = property2.account__c;
            assessment11.Property__c = property2.Id;
            cases.add(assessment11);
            
            Case assessment12  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment12.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment12.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment12.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment12.SubType__c = 'Closeout';
            assessment12.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment12.WaiveNoticePeriod__c = true;
            assessment12.Billable__c = 'Yes';
            assessment12.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment12.AccountId = property.account__c;
            assessment12.Property__c = property.Id;
            cases.add(assessment12);
            
            Case assessment13  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment13.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment13.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment13.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment13.AdjustmentType__c = 'Cancel';
            assessment13.AccountId = property2.account__c;
            assessment13.Property__c = property2.Id;
            cases.add(assessment13);
            
            Case assessment14  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment14.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment14.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment14.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment14.AdjustmentType__c = 'Roll Correction';
            assessment14.AccountId = property.account__c;
            assessment14.Property__c = property.Id;
            cases.add(assessment14);
            
            Case assessment15  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment15.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment15.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment15.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment15.SubType__c = 'Closeout';
            assessment15.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment15.WaiveNoticePeriod__c = false;
            assessment15.Billable__c = 'Yes';
            assessment15.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment15.AccountId = property2.account__c;
            assessment15.Property__c = property2.Id;
            cases.add(assessment15);
            
            Case assessment16  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment16.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment16.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment16.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment16.SubType__c = 'Closeout';
            assessment16.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment16.WaiveNoticePeriod__c = false;
            assessment16.Billable__c = 'Yes';
            assessment16.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment16.AccountId = property.account__c;
            assessment16.Property__c = property.Id;
            cases.add(assessment16);
            
            Case assessment17  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment17.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment17.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment17.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            assessment17.SubType__c = 'Closeout';
            assessment17.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment17.WaiveNoticePeriod__c = true;
            assessment17.Billable__c = 'No';
            assessment17.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment17.AccountId = property2.account__c;
            assessment17.Property__c = property2.Id;
            cases.add(assessment17);
            
            Case assessment18  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment18.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment18.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment18.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            assessment18.SubType__c = 'Closeout';
            assessment18.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment18.WaiveNoticePeriod__c = true;
            assessment18.Billable__c = 'No';
            assessment18.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment18.AccountId = property.account__c;
            assessment18.Property__c = property.Id;
            cases.add(assessment18);
            
            Case assessment19  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment19.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment19.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment19.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
            //assessment19.SubType__c = 'Closeout';
            assessment19.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment19.WaiveNoticePeriod__c = false;
            assessment19.Billable__c = 'No';
            assessment19.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment19.AccountId = property2.account__c;
            assessment19.Property__c = property2.Id;
            cases.add(assessment19);
            
            Case assessment20  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                        rollYears[1].Id,rollYears[1].Name,rollYears[1].Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
            assessment20.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
            assessment20.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
            assessment20.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
            //assessment20.SubType__c = 'Closeout';
            assessment20.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment20.WaiveNoticePeriod__c = false;
            assessment20.Billable__c = 'No';
            assessment20.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
            assessment20.AccountId = property.account__c;
            assessment20.Property__c = property.Id;
            cases.add(assessment20);
            insert cases;
            TestDataUtility.disableAutomationCustomSettings(false);
        }
   
    	//-----------------------------
    	// @author : Publicis.Sapient
    	// @description : Test Method to check the integration status based on muliple filters
    	//-----------------------------
        @isTest
        static void testIntegrationStatus() {
             
             Test.startTest();
             UpdateCaseIntegrationStatusBatch  updateCaseIntegrationStatusBatch = new UpdateCaseIntegrationStatusBatch();
             TestDataUtility.disableAutomationCustomSettings(true);	
             Database.executeBatch(updateCaseIntegrationStatusBatch);
        	 Test.stopTest();
        	 // Assert the functionality
        	 for(Case updatedCase : [select id,TotalSignsCameraTVetcValueDiff__c,SubStatus__c,
                                     IntegrationStatus__c,TotalAssessedValue__c,RollCode__c,SubType__c,
                                     AdjustmentType__c,Type,WaiveNoticePeriod__c,Billable__c from case])
             {
                 if(updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED && updatedCase.SubType__c == 'Closeout' && updatedCase.Type==CCSFConstants.ASSESSMENT.TYPE_REGULAR){
                     system.debug(updatedCase.TotalAssessedValue__c+''+updatedCase.IntegrationStatus__c);
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.TotalSignsCameraTVetcValueDiff__c > 0){
                     // Validate if there are any Embedded Escapes
					System.assertEquals(CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING, updatedCase.SubStatus__c);                    
                 } else if(updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED && updatedCase.SubType__c == 'Closeout' && updatedCase.Type==CCSFConstants.ASSESSMENT.TYPE_REGULAR){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED && updatedCase.AdjustmentType__c == 'Roll Correction'){
                     System.assertNotEquals(CCSFConstants.ASSESSMENT.SUBSTATUS_CANCELLED, updatedCase.SubStatus__c);
                 } else if(updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED && updatedCase.AdjustmentType__c == 'Roll Correction'){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_RP, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && (updatedCase.SubType__c==null ||updatedCase.SubType__c=='') && updatedCase.WaiveNoticePeriod__c == false
            			&& updatedCase.Billable__c == 'Yes' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_RP, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && (updatedCase.SubType__c==null ||updatedCase.SubType__c=='') && updatedCase.WaiveNoticePeriod__c == false
            			&& updatedCase.Billable__c == 'Yes' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && (updatedCase.SubType__c==null ||updatedCase.SubType__c=='') && updatedCase.WaiveNoticePeriod__c == true
            			&& updatedCase.Billable__c == 'Yes' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_RP, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && (updatedCase.SubType__c==null ||updatedCase.SubType__c=='') && updatedCase.WaiveNoticePeriod__c == true
            			&& updatedCase.Billable__c == 'Yes' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.WaiveNoticePeriod__c == true
            			&& updatedCase.Billable__c == 'Yes' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED
                        && updatedCase.SubType__c == 'Closeout' && updatedCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.WaiveNoticePeriod__c == true
            			&& updatedCase.Billable__c == 'No' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED
                        && updatedCase.SubType__c == 'Closeout' && updatedCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE  && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED
                        && updatedCase.AdjustmentType__c == 'Cancel'){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_RP, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED
                        && updatedCase.AdjustmentType__c == 'Roll Correction'){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.WaiveNoticePeriod__c == false
            			&& updatedCase.Billable__c == 'No' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED
                        && updatedCase.SubType__c == 'Closeout' && updatedCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.WaiveNoticePeriod__c == false
            			&& updatedCase.Billable__c == 'Yes' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED
                        && updatedCase.SubType__c == 'Closeout' && updatedCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.WaiveNoticePeriod__c == false
            			&& updatedCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED
                        && updatedCase.SubType__c == null && updatedCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                 } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.WaiveNoticePeriod__c == false
            			&& updatedCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED
                        && updatedCase.SubType__c == null && updatedCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                     System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                 }
             }
            TestDataUtility.disableAutomationCustomSettings(false);
        }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method to check the integration status for closeout cases
    //-----------------------------
    @isTest
    static void testIntegrationStatusForCloseOut() {
        TestDataUtility.disableAutomationCustomSettings(true);
        String currentYear 		= String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        String lastYear 		= String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        Property__c property = new Property__c();
        Property__c property2 = new Property__c();
        Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME) LIMIT 1];
        RollYear__c rollYear=[select id,Name from RollYear__C where Year__c=:lastYear];
        List<Property__c> properties =[select id,account__c,rollcode__c from property__c];
        for(Property__c inputProperty : properties){
            if(inputProperty.rollcode__c ==CCSFConstants.ASSESSMENT.ROLLCODE_SECURED){
                property2 = inputProperty;
            } else{
                property = inputProperty;
            }
        }
        List<case> cases = new List<case>();
        Case assessment1  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                            rollYear.Id,rollYear.Name,rollYear.Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
        assessment1.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment1.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
        assessment1.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
        assessment1.SubType__c = 'Closeout';
        assessment1.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
        assessment1.WaiveNoticePeriod__c = false;
        assessment1.Billable__c = 'NO';
        assessment1.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
        assessment1.AccountId = property2.account__c;
        assessment1.Property__c = property2.Id;
        cases.add(assessment1);
        
        Case assessment2  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                            rollYear.Id,rollYear.Name,rollYear.Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(currentYear), 1, 1));
        assessment2.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment2.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
        assessment2.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
        assessment2.SubType__c = 'Closeout';
        assessment2.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
        assessment2.WaiveNoticePeriod__c = false;
        assessment2.Billable__c = 'NO';
        assessment2.NoticeSentDate__c = system.today()-(Integer.valueOf(Label.RequiredPostNoticeDays)+1);
        assessment2.AccountId = property.account__c;
        assessment2.Property__c = property.Id;
        cases.add(assessment2);
        insert cases;
        TestDataUtility.disableAutomationCustomSettings(false);
        
        Test.startTest();         
        UpdateCaseIntegrationStatusBatch  updateCaseIntegrationStatusBatch = new UpdateCaseIntegrationStatusBatch();
        Database.executeBatch(updateCaseIntegrationStatusBatch);
        Test.stopTest();
        
        // Assert the functionality
        for(Case updatedCase : [select id,IntegrationStatus__c,RollCode__c,SubType__c,AdjustmentType__c,Type,WaiveNoticePeriod__c,Billable__c from case]){
            if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.WaiveNoticePeriod__c == false
               && updatedCase.Billable__c == 'NO' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED
               && updatedCase.SubType__c == 'Closeout' && updatedCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                   System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
               } else if(updatedCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && updatedCase.WaiveNoticePeriod__c == false
                         && updatedCase.Billable__c == 'NO' && updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED
                         && updatedCase.SubType__c == 'Closeout' && updatedCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW){
                             System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
                         }
        }
    }
}