/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: RecordChangeValidatorTest 
 * Description : Test Class for RecordChangeValidator Class
*/
@isTest
public class RecordChangeValidatorTest {

    /*
     * Method Name: recordChangeTest
     * Description: This method tests all the functions in the RecordChangeValidator class 
     */ 
    public static TestMethod void recordChangeTest(){
        Account accountRecord = new Account();
        accountRecord.Name='New Test Account';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord.EntityId__c = '654321';
        accountRecord.BusinessStatus__c='active';
        database.insert(accountRecord);
        
        Account accountRecordOriginal = new Account();
        accountRecordOriginal.Name='New Test Account';
        accountRecordOriginal.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecordOriginal.EntityId__c = '654320';
        accountRecordOriginal.BusinessStatus__c='active';
        database.insert(accountRecordOriginal);
        
        // Declaring the Schema fields and fieldSets
        Schema.SobjectField entityField = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().get('EntityId__c');
        List<Schema.SobjectField> fields = new List<Schema.SobjectField>{entityField};
        Schema.FieldSet accountFieldSet = Schema.SObjectType.Account.fieldSets.getMap().get('AccountInfo');
        
        Test.startTest();
        RecordChangeValidator recordChange = new RecordChangeValidator(accountRecord,accountRecordOriginal);
        recordChange.hasFieldChanged('EntityId__c');
        recordChange.hasFieldChanged(entityField);
        recordChange.hasAnyFieldChanged(fields);
        recordChange.hasAnyFieldChanged(accountFieldSet);
        system.assertEquals(true, accountRecord.Id!=null);
        Test.stopTest();
    }
}