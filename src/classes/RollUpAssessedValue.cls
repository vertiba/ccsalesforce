public class RollUpAssessedValue {
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method calculate Total Assessed value on the Property
    // @return : NA
    //-----------------------------
    public void rollUpTotalAssessedvalues(Set<Property__c> properties,RollYear__c currentClosedRollYear) {
        Map<Id,Case> assessmentWithProperty = new Map<ID,Case>();
        Set<String> duplicateAssessmentNumber = new Set<String>();       
        Set<String> assessmentNumber = new Set<String>();        
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,
                                           TotalAssessedCost__c,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,
                                           TotalPersonalPropertyValue__c,TotalFixturesValue__c
                                  FROM Case 
                                  WHERE AssessmentYear__c   =: currentClosedRollYear.Year__c 
                                  AND   Status               =: CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED 
                                  AND (SubStatus__c         =: CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED                                                                                                                                                                                                                                  
                                       OR SubStatus__c      =: CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING)
                                  AND (IntegrationStatus__c =: CCSFConstants.ASSESSMENT.SENT_TO_TTX
                                       OR  IntegrationStatus__c =: CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_SENT_TO_RP
                                       OR (SubType__c =: CCSFConstants.NOTICE_TYPE_LOW_VALUE
                                           AND  IntegrationStatus__c =: CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND))
                                  AND Property__c  IN: properties 
                                  ORDER BY SequenceNumber__c DESC]; 
        for(Case assessment :assessments){
            if(assessment.AdjustmentType__c == 'Cancel'){ // update name 
                assessmentNumber.add(assessment.AssessmentNumber__c); 
            }
        }        

        //Map of Property with non cancelled Assesmenets
        for(Case assessment : assessments){            
            if(assessmentNumber.contains(assessment.AssessmentNumber__c) && assessment.AdjustmentType__c != 'Roll Correction') continue; // Correction as part of 5361
            if(!assessmentWithProperty.containsKey(assessment.Property__c)){                          
                assessmentWithProperty.put(assessment.Property__c,assessment);           
            }
        }             

        updatePropertyAssessedValue(assessmentWithProperty,properties);
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Sorts the assessment according to highest Sequennce Number and update its value on property,
    // 				  If no assessment set property value to zero
    // @return : NA
    //-----------------------------
    public void updatePropertyAssessedValue(Map<Id,Case> assessmentWithProperty,Set<Property__c> properties){ 
        List<Property__c> updateProperty = new List<Property__c>();
        for(Property__c property : [SELECT Id,Name,MostRecentAssessedValue__c,AssessedCost__c,PersonalPropertyValue__c,FixtureValue__c FROM Property__c Where ID IN : properties]){
            if(assessmentWithProperty.containsKey(property.id)){
                if(assessmentWithProperty.get(property.id).TotalAssessedValue__c != null){
                    property.MostRecentAssessedValue__c = assessmentWithProperty.get(property.id).TotalAssessedValue__c;
                }
                //ASR-9305 Chnages Starts
                property.PersonalPropertyValue__c = assessmentWithProperty.get(property.id).TotalPersonalPropertyValue__c;
                property.FixtureValue__c = assessmentWithProperty.get(property.id).TotalFixturesValue__c;
                 //ASR-9305 Chnages Ends
                if(assessmentWithProperty.get(property.id).TotalAssessedCost__c != null){
                    property.AssessedCost__c = assessmentWithProperty.get(property.id).TotalAssessedCost__c;
                }
                updateProperty.add(property);    
            }else if(property.MostRecentAssessedValue__c != null || property.AssessedCost__c != null){
                    property.MostRecentAssessedValue__c = null;
                    property.AssessedCost__c = null;
                updateProperty.add(property);
            }
        }
        if(updateProperty.isEmpty()){return;}
        List<String> databaseErrors = new List<String>();
        Map<String,Object> updateResult = DescribeUtility.updateRecords(updateProperty,null);
        List<Database.SaveResult> saveUpdateResults= (List<Database.SaveResult>)updateResult.get('updateResults');    
        for(Integer i=0;i< saveUpdateResults.size();i++){
            if (!saveUpdateResults.get(i).isSuccess()) {
                Database.Error error = saveUpdateResults.get(i).getErrors().get(0);
                databaseErrors.add(String.valueof(error));
            }
        }
        Logger.addDebugEntry(String.valueof(databaseErrors), 'RollUpAssessedValue-Class');
        Logger.saveLog();
    }
    
}