/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##
//
//=======================================================================================================================


/***************************************
 *      APPLYSALESTAXWRAPPERTEST       *
 * TEST CLASS FOR APPLYSALESTAXWRAPPER *
 ***************************************/

@isTest
private class ApplySalesTaxWrapperTest {

    @isTest
    private static void should_setInstance_When_RelevantDataExists(){
        
		List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty1 = new Penalty__c();
        penalty1.PenaltyMaxAmount__c = 500;
        penalty1.Percent__c = 10;
        penalty1.RTCode__c = '463';
        penalties.add(penalty1);

        Penalty__c penalty2 = new Penalty__c();
        penalty2.PenaltyMaxAmount__c = 500;
        penalty2.Percent__c = 25;
        penalty2.RTCode__c = '214.13';
        penalties.add(penalty2);

        Penalty__c penalty3 = new Penalty__c();
        penalty3.PenaltyMaxAmount__c = 500;
        penalty3.Percent__c = 25;
        penalty3.RTCode__c = '504';
        penalties.add(penalty3);
		insert penalties;
        
          Account accountRecord = new Account();
        accountRecord.Name='New Test Account';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord.EntityId__c = '654321';
        accountRecord.BusinessStatus__c='active';
        accountRecord.MailingCountry__C='US';
        accountRecord.MailingStreetName__c ='355';
        accountRecord.MailingCity__c ='Oakland';
        accountRecord.NoticeType__c='Notice to File';
        accountRecord.RequiredToFile__c='Yes';
        accountRecord.NoticeLastGenerated__c = system.now();
        insert accountRecord;

        Account accountRecord2 = new Account();
        accountRecord2.Name='New Test Account 2';
        accountRecord2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord2.BusinessStatus__c='active';
        accountRecord2.EntityId__c = '451123';
        accountRecord2.MailingCountry__C='US';
        accountRecord2.MailingStreetName__c ='355';
        accountRecord2.MailingCity__c ='Oakland';
        accountRecord2.NoticeType__c='Notice to File';
        accountRecord2.RequiredToFile__c='Yes';
        insert accountRecord2;
        
        Property__c propertyRecord = new Property__c();
        propertyRecord.Name = 'new test property';
        propertyRecord.PropertyId__c = '153536';
        propertyRecord.MailingCountry__c = 'US';
        propertyRecord.Account__c = accountRecord.Id;
        insert propertyRecord;

        Property__c propertyRecord2 = new Property__c();
        propertyRecord2.Name = 'new test property 2';
        propertyRecord2.PropertyId__c = '531536';
        propertyRecord2.MailingCountry__c = 'US';
        propertyRecord2.Account__c = accountRecord2.Id;
        insert propertyRecord2;
        
        String fiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        //Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear), 5, 30);
		Date filingDate = System.today();
        
        Statement__c statement = new Statement__c();
        statement.FileDate__c = filingDate;
        statement.Property__c = propertyRecord.id;
        statement.PropertyScenario__c = 'Existing';
        statement.RecordTypeId = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalPropertyStatement').getRecordTypeId();
        insert statement;

        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Status__c='Roll Open';
        insert rollYear;

        RollYear__c prevrollYear = new RollYear__c();
        prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Status__c='Roll Closed';
        insert prevrollYear;

        RollYear__c nextrollYear = new RollYear__c();
        nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Status__c='Pending';
        insert nextrollYear;

        List<Case> casesToInsert = new List<Case>();
        Id bppAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Case caseRecord1 = new Case();
        caseRecord1.AccountName__c='new test account';
        caseRecord1.type = 'Regular';
        caseRecord1.AccountId = propertyRecord.account__c;
        caseRecord1.ApplyFraudPenalty__c = true;
        caseRecord1.RecordTypeId = bppAssessmentRecordTypeId;
        caseRecord1.Property__c = propertyRecord.Id;
        caseRecord1.Status = 'In Progress';
        caseRecord1.AssessmentYear__c ='2017';
        caseRecord1.EventDate__c = Date.newInstance(2020, 1, 1);
        caseRecord1.MailingCountry__c = 'US';
        casesToInsert.add(caseRecord1);

        Case caseRecord2 = new Case();
        caseRecord2.AccountName__c='new test account';
        caseRecord2.type = 'Escape';
        caseRecord2.AccountId = propertyRecord.account__c;
        caseRecord2.RecordTypeId = bppAssessmentRecordTypeId;
        caseRecord2.Property__c = propertyRecord.Id;
        caseRecord2.Status = 'In Progress';
        caseRecord2.AssessmentYear__c ='2017';
        caseRecord2.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        caseRecord2.MailingCountry__c = 'US';
        caseRecord2.ApplyFraudPenalty__c = false;
        insert caseRecord2;
        
        SalesTax__c testSalesTax = new SalesTax__c();
        	new TestDataFactory(testSalesTax).populateRequiredFields();
        	insert testSalesTax;
        
         List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
         AssessmentLineItem__c testAssmtLineItem ;
         for(Integer count =0 ; count<5 ; count++){
            testAssmtLineItem                               = new AssessmentLineItem__c();
            testAssmtLineItem.AcquisitionYear__c            = String.valueOf(Date.today().addYears(-3).year());
            testAssmtLineItem.UsefulLife__c                 = 5;
            testAssmtLineItem.Case__c                       = caseRecord2.Id;
            new TestDataFactory(testAssmtLineItem).populateRequiredFields();
            assessmentLineItems.add(testAssmtLineItem);
         }

         insert assessmentLineItems;

        Test.startTest();
        ApplySalesTaxWrapper testApplySalesTaxWrapper1      = new ApplySalesTaxWrapper();
        System.assert(testApplySalesTaxWrapper1 != null);
        ApplySalesTaxWrapper testApplySalesTaxWrapper2      = new ApplySalesTaxWrapper(assessmentLineItems[0]);
        System.assert(testApplySalesTaxWrapper2 != null);
        ApplySalesTaxWrapper testApplySalesTaxWrapper3      = new ApplySalesTaxWrapper(assessmentLineItems[0],true);
        System.assertEquals(false,String.isBlank(assessmentLineItems[0].AcquisitionYear__c));
        Test.stopTest();
        
        AssessmentLineItem__c result = [SELECT SalesTax__c FROM AssessmentLineItem__c WHERE Id=:assessmentLineItems[0].Id];
       	    System.assertEquals(true,String.isBlank(result.SalesTax__c));

    }
}