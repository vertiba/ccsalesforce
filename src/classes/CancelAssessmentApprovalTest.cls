/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
* 
* Class Name : CancelAssessmentApprovalTest
* Description : This class is used to test CancelAssessmentApprovalRequest and CustomEmailService.
*/ 
@isTest
public class CancelAssessmentApprovalTest {
    
    public static TestMethod void cancelAssessmentTest(){
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User 64',alias='tuse2r4',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testuser34@test.com.ccsf');
        
        database.insert(userInstance);
        User userInstance2 = new User(FirstName = 'Test',LastName='Admin User 54',alias='tus4er4',ProfileId = profileId,
                                      TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                      EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testuser53@test.com.ccsf',
                                      managerId = userInstance.Id);
        
        database.insert(userInstance2);
        system.runAs(userInstance2){
            List<Penalty__c> penalties = new List<Penalty__c>();
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            penalties.add(penalty1);
            
            Penalty__c penalty2 = new Penalty__c();
            penalty2.PenaltyMaxAmount__c = 500;
            penalty2.Percent__c = 25;
            penalty2.RTCode__c = '214.13';
            penalties.add(penalty2);
            
            Penalty__c penalty3 = new Penalty__c();
            penalty3.PenaltyMaxAmount__c = 500;
            penalty3.Percent__c = 25;
            penalty3.RTCode__c = '504';
            penalties.add(penalty3);
            insert penalties;
            
            Account accountRecord = new Account();
            accountRecord.Name='New Test Account';
            accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            accountRecord.EntityId__c = '654321';
            accountRecord.BusinessStatus__c='active';
            accountRecord.MailingCountry__C='US';
            accountRecord.MailingStreetName__c ='355';
            accountRecord.MailingCity__c ='Oakland';
            accountRecord.NoticeType__c='Notice to File';
            accountRecord.RequiredToFile__c='Yes';
            accountRecord.NoticeLastGenerated__c = system.now();
            insert accountRecord;
            
            Property__c propertyRecord = new Property__c();
            propertyRecord.Name = 'New Test property';
            propertyRecord.PropertyId__c = '153536';
            propertyRecord.MailingCountry__c = 'US';
            propertyRecord.Account__c = accountRecord.Id;
            insert propertyRecord;
            
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c='Roll Open';
            insert rollYear;
            
            RollYear__c prevrollYear = new RollYear__c();
            prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Status__c='Roll Closed';
            insert prevrollYear;
            
            RollYear__c nextrollYear = new RollYear__c();
            nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Status__c='Pending';
            insert nextrollYear;
            
            Case caseRecord = new Case();
            caseRecord.AccountName__c='New Test Account';
            caseRecord.type = 'Regular';
            caseRecord.AccountId = propertyRecord.account__c;
            caseRecord.ApplyFraudPenalty__c = true;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            caseRecord.Property__c = propertyRecord.Id;
            caseRecord.Status = 'New';
            caseRecord.AssessmentYear__c ='2017';
            caseRecord.EventDate__c = Date.valueOf('2019-11-26');
            caseRecord.MailingCountry__c = 'US';
            database.insert(caseRecord);
            
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setObjectId(caseRecord.id);
            Approval.ProcessResult result = Approval.process(request);
            
            List<ProcessInstanceWorkitem> workItems = [Select Id From ProcessInstanceWorkitem
                                                       Where ProcessInstance.TargetObjectId = :caseRecord.Id ];
            
            Messaging.InboundEmail email = new Messaging.InboundEmail() ;
            Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
            Map<String, Object> emailParams = new Map<String, Object>();
            emailParams.put('workItemId', workItems[0].Id);
            emailParams.put('caseId', caseRecord.Id);
            emailParams.put('errorMessage','No Error');
            email.plainTextBody = JSON.serialize(emailParams);
            
            Test.startTest();
            CancelAssessmentApprovalRequest.RemoveAssessmentApproval(new List<Id>{caseRecord.id});
            CustomEmailService service = new CustomEmailService();
            service.handleInboundEmail(email, envelope);
            Case validateTest = [Select Id,Status,SubStatus__c FROM CASE where id =:caseRecord.Id];
            System.assertNotEquals('New', validateTest.Status);
            Test.stopTest();
        }
    }
}