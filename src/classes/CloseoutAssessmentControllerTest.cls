//Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Test Class for CloseoutAssessmentController
//-----------------------------
@isTest
public class CloseoutAssessmentControllerTest {
    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
    private static Integer nextYear 		= currentYear+1;
    
    @testSetup
    private static void dataSetup(){       
        
        Id systemAdminProfileId = [Select Id from Profile where Name='System Administrator'].Id;
        User managerUser = TestDataUtility.buildUser('Manager','manager@test.com','manag',systemAdminProfileId,null);
        insert managerUser;
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        systemAdmin.ManagerId = managerUser.Id;
        users.add(systemAdmin);       
        insert users;
        //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        Property__c property = TestDataUtility.getVesselProperty();
        property.Account__c = businessAccount.Id;
        property.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        property.Status__c= CCSFConstants.STATUS_ACTIVE;
        insert property;
        
        Property__c property1 = TestDataUtility.getBPPProperty();
        property1.Account__c = businessAccount.Id;
        property1.Status__c= CCSFConstants.STATUS_ACTIVE;
        insert property1;
        
        
        String fiscalYear = String.valueof(currentYear);
        Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear), 5, 30);
        String prevfiscalYear = String.valueof(lastYear);
        
        RollYear__c prevRollYear = new RollYear__c();
        prevRollYear.Name = prevfiscalYear;
        prevRollYear.Year__c = prevfiscalYear;
        prevRollYear.Status__c=CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS;
        insert prevRollYear;
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = fiscalYear;
        rollYear.Year__c = fiscalYear;
        rollYear.Status__c=CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS;
        insert rollYear;
        
        
        Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,
                                                           null,null,null,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(fiscalYear), 01, 01));
        assessment.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT;
        assessment.AccountId = property.account__c;
        assessment.Property__c = property.Id;
        assessment.SequenceNumber__c = 1;     
        assessment.NumberOfMonthsFor506Interest__c = 4;
        assessment.AdjustmentType__c= CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        assessment.AdjustmentReason__c = CCSFConstants.ASSESSMENT.ADJUSTMENTREASON_AAB_Decision;
        insert assessment;       
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Regular Closeout is Submitted For Approval
    //-----------------------------
    @isTest
    private static void regularCloseoutSentForApprovalTest() { 
        
        String lastfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        RollYear__c rollYears = [Select id,Name,Status__c,Year__c FROM RollYear__c  Where Name=: lastfiscalYear order by Name desc LIMIT 1];
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,TotalPersonalPropertyValueOverride__c,
                                  TotalAssessedCost__c,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE AssessmentYear__c   =: rollYears.Name 
                                  ORDER BY SequenceNumber__c DESC]; 
        
        
        Case assessment1  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),assessments[0].Property__r.Id,
                                                            rollYears.Id,rollYears.Name,rollYears.Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(lastfiscalYear), 01, 01));
        assessment1.Status =  CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        assessment1.ParentRegularAssessment__c =assessments[0].id;
        assessment1.Property__c = assessments[0].Property__r.Id;
        assessment1.NumberOfMonthsFor506Interest__c = 4;
        assessment1.SequenceNumber__c = 2;
        assessment1.TotalPersonalPropertyValueOverride__c = 100;
        assessment1.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT;
        assessment1.AdjustmentReason__c = CCSFConstants.ASSESSMENT.ADJUSTMENTREASON_AAB_Decision;
        insert	assessment1;         
        AssessmentDataWrapper dataWrapper = new AssessmentDataWrapper();  
        dataWrapper=CloseoutAssessmentController.getCloseoutAssessment(String.valueOf(assessments[0].id));
        dataWrapper.caseRecord = assessments[0];
        String serializedata = JSON.serialize(dataWrapper);
        Test.startTest();
 		User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        
        System.runAs(systemAdminUser){
        CloseoutAssessmentController.updateAndApprove(serializedata);
        }
        Test.stopTest();
        Case validateTest = [Select Id,Status,SubStatus__c FROM CASE where id =:assessments[0].id];
        System.assertEquals(CCSFConstants.ASSESSMENT.STATUS_INPROGRESS, validateTest.Status);
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Escape Closeout is Submitted For Approval
    //-----------------------------
    @isTest
    private static void escapeCloseoutSentForApprovalTest() { 
        
        String lastfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        RollYear__c rollYears = [Select id,Name,Status__c,Year__c FROM RollYear__c  Where Name=: lastfiscalYear order by Name desc LIMIT 1];
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,TotalPersonalPropertyValueOverride__c,
                                  TotalAssessedCost__c,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE AssessmentYear__c   =: rollYears.Name 
                                  ORDER BY SequenceNumber__c DESC]; 
        
        
        Case assessment1  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),assessments[0].Property__r.Id,
                                                            rollYears.Id,rollYears.Name,rollYears.Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(lastfiscalYear), 01, 01));
        assessment1.Status =  CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        assessment1.ParentRegularAssessment__c =assessments[0].id;
        assessment1.Property__c = assessments[0].Property__r.Id;
        assessment1.NumberOfMonthsFor506Interest__c = 4;
        assessment1.SequenceNumber__c = 2;
        assessment1.TotalPersonalPropertyValueOverride__c = 100;
        assessment1.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT;
        assessment1.AdjustmentReason__c = CCSFConstants.ASSESSMENT.ADJUSTMENTREASON_AAB_Decision;
        insert	assessment1;         
        AssessmentDataWrapper dataWrapper = new AssessmentDataWrapper();  
        dataWrapper=CloseoutAssessmentController.getCloseoutAssessment(String.valueOf(assessment1.id));
        dataWrapper.caseRecord = assessments[0];
        String serializedata = JSON.serialize(dataWrapper);
        Test.startTest();
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        
        System.runAs(systemAdminUser){
        CloseoutAssessmentController.updateAndApprove(serializedata);
        }
        Test.stopTest();
         List<Case> validateTest = [Select Id,Status,SubStatus__c FROM CASE where id =:assessment1.id OR id =: assessments[0].id];
       	Boolean isInProgress= false;
        for(Case cs :validateTest){
            if(cs.status == 'In Progress'){
                isInProgress= true;
            }else{
                isInProgress = false;
            }
        }
        System.assertEquals(true, isInProgress);
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : field Validation Failure "No. of Of Month & Rt Code"
    //-----------------------------
    @isTest
    private static void fieldValidationTest() { 
        
        String currentfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        String lastfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        RollYear__c rollYears = [Select id,Name,Status__c,Year__c FROM RollYear__c  Where Name=: currentfiscalYear order by Name desc LIMIT 1];
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,TotalPersonalPropertyValueOverride__c,
                                  TotalAssessedCost__c,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE AssessmentYear__c   =: rollYears.Name 
                                  ORDER BY SequenceNumber__c DESC]; 
        
        
        Case assessment1  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),assessments[0].Property__r.Id,
                                                            null,null,null,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(lastfiscalYear), 01, 01));
        assessment1.Status =  CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        assessment1.ParentRegularAssessment__c =assessments[0].id;
        assessment1.Property__c = assessments[0].Property__r.Id;
        assessment1.SequenceNumber__c = 2;
        assessment1.TotalPersonalPropertyValueOverride__c = 100;
        assessment1.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT;
        assessment1.AdjustmentReason__c = CCSFConstants.ASSESSMENT.ADJUSTMENTREASON_AAB_Decision;
        insert	assessment1;         
        AssessmentDataWrapper dataWrapper = new AssessmentDataWrapper();  
        dataWrapper=CloseoutAssessmentController.getCloseoutAssessment(String.valueOf(assessment1.id));
        dataWrapper.caseRecord = assessments[0];
        String serializedata = JSON.serialize(dataWrapper);
        Test.startTest();
        try{
            CloseoutAssessmentController.updateAndApprove(serializedata);
        }catch(Exception e){
            System.assertEquals(CCSFConstants.AURA_EXCEPTION, e.getMessage());
        }
        Test.stopTest();
        List<Case> validateTest = [Select Id,Status,SubStatus__c FROM CASE where id =:assessment1.id OR id =: assessments[0].id];
       	Boolean isInProgress= false;
        for(Case cs :validateTest){
            if(cs.status == 'In Progress'){
                isInProgress= true;
            }else{
                isInProgress = false;
            }
        }
        System.assertEquals(false, isInProgress);
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description :  Validation Failure "Adjustment Reason"
    //-----------------------------
    @isTest
    private static void adjustmentReasonValidationTest() { 
        
        String currentfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        String lastfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        RollYear__c rollYears = [Select id,Name,Status__c,Year__c FROM RollYear__c  Where Name=: currentfiscalYear order by Name desc LIMIT 1];
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,TotalPersonalPropertyValueOverride__c,
                                  TotalAssessedCost__c,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE AssessmentYear__c   =: rollYears.Name 
                                  ORDER BY SequenceNumber__c DESC]; 
        
        
        Case assessment1  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),assessments[0].Property__r.Id,
                                                            null,null,null,
                                                            CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(lastfiscalYear), 01, 01));
        assessment1.Status =  CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        assessment1.ParentRegularAssessment__c =assessments[0].id;
        assessment1.Property__c = assessments[0].Property__r.Id;
        assessment1.SequenceNumber__c = 2;
        assessment1.TotalPersonalPropertyValueOverride__c = 100;
        assessment1.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT;
        insert	assessment1;         
        AssessmentDataWrapper dataWrapper = new AssessmentDataWrapper();  
        dataWrapper=CloseoutAssessmentController.getCloseoutAssessment(String.valueOf(assessment1.id));
        dataWrapper.caseRecord = assessments[0];
        String serializedata = JSON.serialize(dataWrapper);
        Test.startTest();
        try{
            CloseoutAssessmentController.updateAndApprove(serializedata);
        }catch(Exception e){
            System.assertEquals(CCSFConstants.AURA_EXCEPTION, e.getMessage());
        }
        Test.stopTest();
        List<Case> validateTest = [Select Id,Status,SubStatus__c FROM CASE where id =:assessment1.id OR id =: assessments[0].id];
       	Boolean isInProgress= false;
        for(Case cs :validateTest){
            if(cs.status == 'In Progress'){
                isInProgress= true;
            }else{
                isInProgress = false;
            }
        }
        System.assertEquals(false, isInProgress);
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Aura Component map for the Picklist Fields
    //-----------------------------
    @isTest static void getPicklistValue(){        
        Test.startTest();
        Map<String, String> adjustmentReason = CloseoutAssessmentController.fetchAdjustmentReason();
        Map<String, String> subType = new Map<String, String>();
        subType= CloseoutAssessmentController.fetchSubType('Escape','Vessel');
        subType=CloseoutAssessmentController.fetchSubType('Regular','Vessel');
        subType=CloseoutAssessmentController.fetchSubType('Regular','BPP');
        subType=CloseoutAssessmentController.fetchSubType('Escape','BPP');
        system.assertNotEquals(null, subType.values());
        system.assertNotEquals(null, adjustmentReason.values());        
        Test.stopTest();
    }
}