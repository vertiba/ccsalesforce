/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test AumentumDataNormalizeBatch class
//-----------------------------
@isTest
public class AumentumDataNormalizeBatchTest {
    
    @TestSetup
    static void dataForTest(){
        List<Account> accounts= new List<Account>();
        Account account1= TestDataUtility.buildAccountAumentum('FCL LOUISE','2340028','45-4766086', Date.newInstance(1990,09,30), null);
        Account account2= TestDataUtility.buildAccountAumentum('SG Indutries', '2340029','45-4766087',Date.newInstance(1993,09,30),null);
        Account account3=TestDataUtility.buildAccountAumentum( 'DKSHVN ELEC CORP','2340030','45-4766088',Date.newInstance(1968,10,10), Date.newInstance(1993,09,30));
        Account account4= TestDataUtility.buildAccountAumentum('BYJUS INC', '2340038',	'45-4766096',Date.newInstance(1908,10,10),null);
        Account account5= TestDataUtility.buildAccountAumentum('UNI COLORS INC','2340044','45-4766101',Date.newInstance(1968,10,10),null);		
        Account account6= TestDataUtility.buildAccountAumentum('UNI COLORS','2340045','45-4766100',Date.newInstance(1998,10,10),null);		
        Account account7= TestDataUtility.buildAccountAumentum('FIJITSU UNITED CO','2304505','45-4766100',Date.newInstance(1918,10,10),null);		
        Account account8= TestDataUtility.buildAccountAumentum('TIGI PROPERTIES','2304507','45-4766909',Date.newInstance(1978,10,10),null);		
        Account account9= TestDataUtility.buildAccountAumentum('SKCROWN BUILDING CO','2340041','45-4766099',Date.newInstance(1928,10,10),null);		
        Account account10= TestDataUtility.buildAccountAumentum('SKCROWN BUILDING','2340041','45-4766099',Date.newInstance(1938,10,10),null);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        accounts.add(account4);
        accounts.add(account5);
        accounts.add(account6);
        accounts.add(account7);
        accounts.add(account8);
        accounts.add(account9);
        accounts.add(account10);
        insert accounts;

        List<Property__c> properties= new List<Property__c>();
        Property__c property1 =	TestDataUtility.buildAumentumProcessProperty('2340028-00-000','FCL WAREHOUSE','571-L',Date.newInstance(1968,10,10)	,null	,account1.Id	,'1 CHASE MANHATTAN PLZ 23TH FLR'	,null	,null	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94115'	,'2513');
        Property__c property2 =	TestDataUtility.buildAumentumProcessProperty('2340029-02-001','SG INDUSTRIES HEADOFFICE'	,'571-L',Date.newInstance(1968,10,10)	,null	,account2.Id	,'Market '	,'115'	,'ST'	,null	,null	,null	,'SAN FRANCISCO','US-CA','94115','2513');
        Property__c property3 =	TestDataUtility.buildAumentumProcessProperty('2340029-02-707','DKSHVN ELEC CORP OFFICE','571-L'	,Date.newInstance(1968,10,10)	,null	,account3.Id	,'Market '	,'1157'	,'ST'	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94115'	,'2513');
        Property__c property4=	TestDataUtility.buildAumentumProcessProperty('2340029-02-900','ELEC CORP OFFICE','571-L'	,Date.newInstance(1968,10,10)	,null	,account4.Id 	,'6700 LAWRENCE ST STOP 21'	,null	,null	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94110'	,'4107');
        Property__c property5=	TestDataUtility.buildAumentumProcessProperty('2340045-06-171','TIGI AGENCY','571-L',Date.newInstance(1978,10,10),	null	,account8.Id	,'4310 Montgomery HWY 67 BSMT'	,null	,null	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94107'	,null);
        Property__c property6=	TestDataUtility.buildAumentumProcessProperty('2340045-06-172','TIGI AGENCY','571-L',Date.newInstance(1978,10,10),	null	,account8.Id	,'4310 Montgomery HWY 67 BSMT'	,null	,null	,null	,null	,null	,'SAN FRANCISCO'	,'US-CA'	,'94107'	,null);
        Property__c property7=	TestDataUtility.buildAumentumProcessProperty('2340042-02-001','FIJITSU CORPORATE OFFICE'	,'571-STR',Date.newInstance(1968,10,10)	,null	,account7.id	,'3700 HWY 280-431 N'	,null	,'ST'	,null	,null	,null	,'SAN FRANCISCO','US-CA'	,'94107'	,'1123');
        properties.add(property1);
        properties.add(property2);
        properties.add(property3);
        properties.add(property4);
        properties.add(property5);
        properties.add(property6);
        properties.add(property7);
        insert properties;

    }
    @isTest
    static void aumentumProcessTest1(){
        List<StagingAumentumBusiness__c> records = Test.loadData(StagingAumentumBusiness__c.sObjectType, 'AumentumProcessTestData');
        List<StagingAumentumBusiness__c> cleanedRecords= new List<StagingAumentumBusiness__c>();
        List<Id> stagingIds= new List<Id>();
        for(StagingAumentumBusiness__c record:records){
            record.IsDataClean__c = true;
            cleanedRecords.add(record);
            stagingIds.add(record.Id);
        }       
        update cleanedRecords;
        Test.startTest();
        AumentumDataNormalizeBatch batchTest= new AumentumDataNormalizeBatch();
        Id batchid = database.executeBatch(batchTest);    
        Test.stopTest();  

        List<StagingAumentumBusiness__c> withNewStatus= new List<StagingAumentumBusiness__c>();
        List<StagingAumentumBusiness__c> withWarnStatus= new List<StagingAumentumBusiness__c>();
        List<StagingAumentumBusiness__c> withProcessedStatus= new List<StagingAumentumBusiness__c>(); 
        List<StagingAumentumBusiness__c> withReviewStatus= new List<StagingAumentumBusiness__c>();     
        List<StagingAumentumBusiness__c> processedResult=[Select id, Status__c from StagingAumentumBusiness__c where Id IN: stagingIds];
        for(StagingAumentumBusiness__c record:processedResult){           
            if(record.Status__c =='New'){
                withNewStatus.add(record);
            }else if(record.Status__c =='Processed'){
                 withProcessedStatus.add(record);
            }else if(record.Status__c =='Warning'){
                 withWarnStatus.add(record);
            }else if(record.Status__c =='In Review'){
                 withReviewStatus.add(record);
            }
        }
      
        system.assert(withNewStatus.size() ==0,'No record with New Status');
        system.assert(withProcessedStatus.size() >0,'Record with Processed Status');
        system.assert(withWarnStatus.size()>0,'Record with Warning Status');
        system.assert(withReviewStatus.size()>0,'Record with in Review Status');
        }
    
    @isTest
    static void errorFrameworkTest(){
        	 Id testJobId = '707S000000nKE4fIAG';
        	BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
            testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testJob.JobCreatedDate__c = System.today();
            testJob.JobId__c = testJobId;
            database.insert(testJob);
        
            List<StagingAumentumBusiness__c> records = Test.loadData(StagingAumentumBusiness__c.sObjectType, 'AumentumProcessTestData');
        	BatchApexError__c testError = new BatchApexError__c();
            testError.AsyncApexJobId__c = testJobId;
            testError.BatchApexErrorLog__c = testJob.Id;
            testError.DoesExceedJobScopeMaxLength__c = false;
            testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
            testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testError.JobScope__c = records[0].Id;
            testError.Message__c = 'Test exception';
            testError.RequestId__c = null;
            testError.StackTrace__c = '';
            database.insert(testError);
        
        BatchableError batchError = BatchableError.newInstance(testError);
        AumentumDataNormalizeBatch batchRetry= new AumentumDataNormalizeBatch();
        batchRetry.handleErrors(batchError);
    }

}