/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class EnhancedSchedulableTest {

    public class MockEnhancedSchedulable extends EnhancedSchedulable {
        public override String getJobNamePrefix() {
            return 'Mock Enhanced Schedulable';
        }
        public void execute(SchedulableContext schedulableContext) {
            System.debug('Execute method of mock enhanced-schedulable class');
        }
    }

    @isTest
    static void it_should_return_the_number_of_running_batch_jobs() {
        Integer expectedCount = [SELECT COUNT() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN ('Processing', 'Preparing', 'Queued')];

        Test.startTest();
        Integer returnedCount = EnhancedSchedulable.getNumberOfRunningBatchJobs();
        Test.stopTest();

        System.assertEquals(expectedCount, returnedCount);
    }

    @isTest
    static void it_should_schedule_job() {
        EnhancedSchedulable mockSchedulableClass = new MockEnhancedSchedulable();

        Test.startTest();
        mockSchedulableClass.scheduleEveryXMinutesInHour(10);
        Test.stopTest();

        Integer numberOfScheduledJobs = [SELECT COUNT() FROM CronTrigger];
        System.assertNotEquals(0, numberOfScheduledJobs, 'A job should be scheduled');
    }

    @isTest
    static void it_should_abort_jobs_with_the_same_job_name_prefix() {
        EnhancedSchedulable mockSchedulableClass = new MockEnhancedSchedulable();

        List<CronTrigger> matchingScheduledJobs = [
            SELECT Id, OwnerId, CronExpression, CronJobDetail.Name
            FROM CronTrigger
            WHERE CronJobDetail.JobType = '7' // 7 == Scheduled Apex
            AND CronJobDetail.Name LIKE :mockSchedulableClass.getJobNamePrefix() + '%'
        ];
        System.assertEquals(0, matchingScheduledJobs.size());

        mockSchedulableClass.scheduleHourly(1);

        Test.startTest();
        mockSchedulableClass.abortJobsWithJobNamePrefix();
        Test.stopTest();

        matchingScheduledJobs = [
            SELECT Id, OwnerId, CronExpression, CronJobDetail.Name
            FROM CronTrigger
            WHERE CronJobDetail.JobType = '7' // 7 == Scheduled Apex
            AND CronJobDetail.Name LIKE :mockSchedulableClass.getJobNamePrefix() + '%'
        ];
        System.assertEquals(0, matchingScheduledJobs.size());
    }

}