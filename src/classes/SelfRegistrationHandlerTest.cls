/**
 * @Business: Test class for SelfRegistrationHandler
 * @Date: 01/30/2020
 * @Author: Srini Aluri(A1)
 
 * ModifiedBy         ModifiedDate   Description
 * Srini Aluri(A1)    01/30/2020     Initial development
*/

@isTest
public class SelfRegistrationHandlerTest {
    
    @testSetup
    static void createTestData() {
        
    }
    
    static testMethod void createUserTest() {
        // Create a Account(Person Account Type) record
        Id personActRecTypId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        System.debug('personActRecTypId - ' + personActRecTypId);
        
        // Create one contact that can be used in next method for user creation
        Account personAct = new Account(
            RecordTypeId = personActRecTypId,
            FirstName = 'Garry', 
            LastName = 'Longhart', 
            PersonEmail = 'garry.longhart@taxpayertestuser.com'
        );
        INSERT personAct;
        
        // Community User profile must be 'Taxpayer'
        Profile taxpayerProfile = [SELECT Id FROM Profile WHERE Name = 'Taxpayer'];
        
        // Create registrationAttributes Map as required by SelfRegistrationHandler.createUser() method
        // As per settings defined Community Administration's in L & R part, we need to provide
        // LastName, FirstName, Username & Email
        Map<SObjectField, String> userData = new Map<SObjectField, String>();        
        userData.put(Schema.User.FirstName, personAct.FirstName);
        userData.put(Schema.User.LastName, personAct.LastName);
        userData.put(Schema.User.Username, personAct.PersonEmail);
        userData.put(Schema.User.Email, personAct.PersonEmail);
        
        Test.startTest();
        
        SelfRegistrationHandler handler = new SelfRegistrationHandler();
        Id userId = handler.createUser(personAct.Id, taxpayerProfile.Id, userData, 'TesT12axP012$$$');
        
        Test.stopTest();
    }
}