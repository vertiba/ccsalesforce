/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @story number : ASR-2366 and Task ASR-9371
// @description : This class is used to test AumentumAutoCleanupScheduler.
//-----------------------------
@isTest
public with sharing class AumentumAutoCleanupSchedulerTest {
    @isTest
    private static void AumentumAutoCleanupTest() {
    Test.startTest();
    AumentumAutoCleanupScheduler sched = new AumentumAutoCleanupScheduler();
        String jobId = System.schedule('AumentumAutoCleanupScheduler',
                                       '0 0 0 15 3 ? 2032', 
                                       sched); 
        sched.execute(null);       
     	String s =sched.getJobNamePrefix();
        Test.stopTest();
              
        system.assertEquals(1, [SELECT count() FROM CronTrigger where id = :jobId],
                            'A job should be scheduled');
    }
}