/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class ContentDistributionUtility {
    @InvocableMethod
    public static List<String> getPublicUrl(List<String> lstDocumentNames) {
        
        //accepts list of file names along with their library path
        //for example {"Assets Library/doc1","Any Library Name/document name"}
        
        Set<String> setLibraryNames = new Set<String>();
        Set<String> setDocumentNames = new Set<String>();
        
        for(String docPath :lstDocumentNames){
            
            //split the input list entry to get library name and document name
            List<String> docDetails = docPath.split('/');
            
            //make sure we got the expected names for library and doc
            if(docDetails.size() == 2){
                
                setLibraryNames.add(docDetails[0]); //get library name from first location of split result
                setDocumentNames.add(docDetails[1]); //get document name from 2nd location of split result
            }
        }
        
        //now get all content document ids for supplied list
        if(!setLibraryNames.isEmpty() && !setDocumentNames.isEmpty()){
            
            List<ContentWorkspaceDoc> lstContentLibraryDocs = [SELECT ContentDocumentId,ContentWorkSpace.Name,ContentDocument.Title 
                                                                FROM ContentWorkspaceDoc 
                                                                WHERE ContentWorkSpace.Name IN :setLibraryNames
                                                                AND ContentDocument.Title IN :setDocumentNames];
            
            if(!lstContentLibraryDocs.isEmpty()){
                //prepare map of file paths and their document ids, it will be used to order the return result
                Map<String,Id> mapDocumentPath_DocId = new Map<String,Id>();
                
                for(ContentWorkspaceDoc contentDoc :lstContentLibraryDocs){
                    String filePath = contentDoc.ContentWorkSpace.Name + '/' + contentDoc.ContentDocument.Title;
                    mapDocumentPath_DocId.put(filePath, contentDoc.ContentDocumentId);
                }
                
                //invoke method t get public URLs for each content document
                Map<Id,String> mapContentDocId_PublicURL = getPublicURLsForDocuments(mapDocumentPath_DocId.values());
                
                //sort the URLs to match the input order
                if(!mapContentDocId_PublicURL.isEmpty()){
                    
                    List<String> lstPublicURLs = new List<String>();
                    
                    for(String filePath :lstDocumentNames){
                        
                        lstPublicURLs.add(mapContentDocId_PublicURL.get(mapDocumentPath_DocId.get(filePath)));
                    }
                    
                    if(!lstPublicURLs.isEmpty())
                        return lstPublicURLs;
                }
            }
        }
        
        return new List<String>{'Operation not allowed.'};
    }
    
    public static Map<Id,String> getPublicURLsForDocuments(List<Id> lstContentDocIds){
        
        Map<Id,String> mapContentDocId_PublicURL = new Map<Id,String>();
        
        //query existing ContentDistribution records (if any) for given document ids
        List<ContentDistribution> lstExistingContentDistributions = [SELECT Id,DistributionPublicUrl,ContentDocumentId 
                                                                        FROM ContentDistribution
                                                                        WHERE ContentDocumentId IN :lstContentDocIds
                                                                        AND ( ExpiryDate = null OR ExpiryDate >= TODAY )];
        
        if(!lstExistingContentDistributions.isEmpty()){
            
            for(ContentDistribution distribution :lstExistingContentDistributions){
                
                if(distribution.DistributionPublicUrl != null && !mapContentDocId_PublicURL.containsKey(distribution.ContentDocumentId))
                    mapContentDocId_PublicURL.put(distribution.ContentDocumentId, distribution.DistributionPublicUrl);
            }
        }
        
        //query all latest content version records for given documents
        List<ContentVersion> lstContentVersions = [SELECT Id,ContentDocumentId 
                                                    FROM ContentVersion 
                                                    WHERE ContentDocumentId IN :lstContentDocIds 
                                                    AND IsLatest = true];
        
        Map<Id,Id> mapContentDocId_VersionId = new Map<Id,Id>();
        
        for(ContentVersion contVersion : lstContentVersions){
            
            mapContentDocId_VersionId.put(contVersion.ContentDocumentId, contVersion.Id);
        }
        
        List<ContentDistribution> lstNewContentDistributions = new List<ContentDistribution>();
        
        //create new ContentDistribution records if not found
        for(Id docId :lstContentDocIds){
            
            if(!mapContentDocId_PublicURL.containsKey(docId)){
                
                lstNewContentDistributions.add(new ContentDistribution(
                    
                    Name = 'Public Link',
                    ContentVersionId = mapContentDocId_VersionId.get(docId),
                    PreferencesAllowViewInBrowser = true,
                    PreferencesLinkLatestVersion = true,
                    PreferencesNotifyOnVisit = false,
                    PreferencesPasswordRequired = false,
                    PreferencesAllowOriginalDownload = true
                ));
            }
        }
        
        if(!lstNewContentDistributions.isEmpty()){
            
            try{
                INSERT lstNewContentDistributions;
            }
            catch(Exception e){
                system.debug(e.getMessage());
            }
        }
        
        //query updated ContentDistribution records for given document ids
        List<ContentDistribution> lstUpdatedContentDistributions = [SELECT Id,DistributionPublicUrl,ContentDocumentId 
                                                                        FROM ContentDistribution
                                                                        WHERE ContentDocumentId IN :lstContentDocIds
                                                                        AND ( ExpiryDate = null OR ExpiryDate >= TODAY )];
        
        if(!lstUpdatedContentDistributions.isEmpty()){
            
            for(ContentDistribution distribution :lstUpdatedContentDistributions){
                
                if(distribution.DistributionPublicUrl != null && !mapContentDocId_PublicURL.containsKey(distribution.ContentDocumentId))
                    mapContentDocId_PublicURL.put(distribution.ContentDocumentId, distribution.DistributionPublicUrl);
            }
        }
        
        return mapContentDocId_PublicURL;
    }
}