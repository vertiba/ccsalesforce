/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class FiscalYearUtility {

    private static final List<FiscalYearSettings> FISCAL_YEAR_SETTINGS;
    private static final Organization ORGANIZATION;
    private static Integer currentFiscalYear;
    private static Id rollyearId; // Added as per 10128

    static {
        FISCAL_YEAR_SETTINGS =  [
            SELECT Id, Name, StartDate, EndDate, IsStandardYear, PeriodId, YearType
            FROM FiscalYearSettings
        ];

        ORGANIZATION = [SELECT Id, FiscalYearStartMonth FROM Organization];
    }
    /**
     * Fiscal Year settings will always gives year based on Fsical year settings. But we have 
     * a scenario where Closed year can be reopened and in that case we need Roll year from
     * System and not from Fiscal year settings. 
     */
    public static Integer getCurrentFiscalYear() {
        if(currentFiscalYear!=null) return currentFiscalYear;
        List<RollYear__c> rollyears = [Select Id,Status__c,Year__c from RollYear__c where Status__c = 'Roll Open' ORDER BY Year__c ASC];
        if(rollyears != null && rollyears.size()==1){
            currentFiscalYear = Integer.valueOf(rollyears.get(0).year__c);
        } else {
            currentFiscalYear = getFiscalYear(System.today());
        }
        return currentFiscalYear;
    }

    public static Integer getFiscalYear(Datetime dt) {
        if(dt == null) throw new DateException('Datetime is required');
        return getFiscalYear(dt.date());
    }

    public static Integer getFiscalYear(Date d) {
        if(d == null) throw new DateException('Date is required');

        return calculateStandardFiscalYear(d);
    }

    private static Integer calculateStandardFiscalYear(Date d) {
        if(d.month() >= ORGANIZATION.FiscalYearStartMonth) return d.year() + 1;
        else return d.year();
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : String fiscalYear
    // @description : This method takes the year, and returns that years May 7 as date format.
    //         If May 7 is a weekend, then the next working day is returned.
    // @return : Date
    //-----------------------------
    public static Date getLastWorkingDayToFileStatementWithoutPenalty(String fiscalYear) {
       
        List<RollYear__c> rollyears = [SELECT Id, Name, Year__c, BPPLateFilingDate__c FROM RollYear__c WHERE Status__c = 'Roll Open' and Year__c =:fiscalYear];
        //7:00 AM in GMT = 00:00 in PDT on May 7, Using GMT to make sure that we can avoid TimeZone issues.
        DateTime lastDateWithoutPenalty = rollyears != null && !rollyears.isEmpty() && rollyears[0].BPPLateFilingDate__c != null ? Datetime.newInstanceGMT(rollyears[0].BPPLateFilingDate__c.year(), rollyears[0].BPPLateFilingDate__c.month(), rollyears[0].BPPLateFilingDate__c.day()).addHours(7) : DateTime.newInstanceGMT(Integer.ValueOf(fiscalYear), 5, 7, 7, 0, 0);
        while (lastDateWithoutPenalty.format('EEE').startsWithIgnoreCase('Sat')
               || lastDateWithoutPenalty.format('EEE').startsWithIgnoreCase('Sun')) {
            lastDateWithoutPenalty = lastDateWithoutPenalty.addDays(1);
        }
        return lastDateWithoutPenalty.date();
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : String fiscalYear
    // @description : get Input Year Fiscal Settings
    // @return : FiscalYearSettings for the asked fiscal year
    //-----------------------------
    public static FiscalYearSettings getFiscalYearSettings(String fiscalYear) {
        for(FiscalYearSettings fiscalyearSetting : FISCAL_YEAR_SETTINGS) {
            if( fiscalyearSetting.Name == fiscalYear){
                return fiscalyearSetting;
            }
        }
        return null;
    }
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param : None
    // @description : This method gets the Roll year as per ASR 8980.(Addedd on FicalYearUtility as per ASR-10128)
    // @return : Id of the Roll Year
    //-----------------------------
    public static ID getCloseoutRollYear() {
        Integer currentFiscalYear = FiscalYearUtility.getCurrentFiscalYear();
        FiscalYearSettings fiscalYearSettings = FiscalYearUtility.getFiscalYearSettings(String.ValueOf(currentFiscalYear));
        Date fiscalYearEndDate = fiscalYearSettings !=null ? fiscalYearSettings.EndDate : null;
        Date previousFiscalyearEndDate = fiscalYearEndDate!=null ? fiscalYearEndDate.addYears(-1) : null;
        RollYear__c closedRollRollYear;
        RollYear__c currentrollYear;
        for(RollYear__c rollYear : [SELECT Id,Status__c,Year__c from RollYear__c ORDER BY Year__c DESC]) {
            if(rollYear.Year__c == String.valueOf(currentFiscalYear) && rollYear.status__c != CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS ) {
                currentrollYear =  rollYear;
            }else if(rollYear.status__c == CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS && rollYear.Year__c == String.valueOf(currentFiscalYear -1)) {
                closedRollRollYear = rollYear;
            }
        }        
        // As lien Date is Fixed to 01/01/XXXX we explicitly used 01/01.
        if(System.today() <= fiscalYearEndDate && System.today() >= Date.newInstance(currentFiscalYear, 01, 01)) {
            rollyearId = currentrollYear.Id;
        
        // As lien Date is Fixed to 01/01/XXXX we explicitly used 01/01.
        }else if(System.today() > previousFiscalyearEndDate && System.today() < Date.newInstance(currentFiscalYear, 01, 01) ) {
            rollyearId = closedRollRollYear.Id;
        }
        return rollyearId;

    }

    private class DateException extends Exception {}

}