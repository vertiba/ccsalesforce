/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Conroller for UpdateVesselNoticeType VF page
//-----------------------------
public with sharing class UpdateVesselNoticeTypeController {
    
    private ApexPages.StandardSetController setController;
    public String recordStatus {get;set;} 
    
    public UpdateVesselNoticeTypeController(ApexPages.StandardSetController setController) {
        this.setController = setController;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This class is responsible to call Batch class
    // @return : void
    //-----------------------------
    public void redirect(){
        // 1 - Check whether user has permission to perform the action or not
        Boolean hasPermission = FeatureManagement.checkPermission('AccessToUpdateVesselNoticeType'); 
        
        
        // 2 - If user has permission then execute the batch job else, send error
        if (hasPermission) {
            Database.executeBatch(new UpdateVesselNoticeType());
        	recordStatus = System.Label.UpdateVesselNoticeType;
        } else {
            recordStatus =System.Label.NoPermission;
        }
	}

}