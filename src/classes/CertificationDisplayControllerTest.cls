/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to test CertificationDisplayController class.
//-----------------------------
@isTest (seealldata=false)
public class CertificationDisplayControllerTest {

    @testSetup
    static void createTestData() {
        
            //insert account
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            //insert property
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;
            
            //insert statement            
            Map<String,String> inputParamsStatement = new Map<String,String>();
            Date filingDate = Date.newInstance(2019, 2, 25);  
            Date startofBussinessDate = Date.newInstance(2016, 3,4);
            inputParamsStatement.put('accountId',businessAccount.Id);
            inputParamsStatement.put('propertyId',bppProperty.Id);
            inputParamsStatement.put('formType','571-L');
            inputParamsStatement.put('businessScenario','Existing');
            inputParamsStatement.put('propertyScenario','Existing');
            inputParamsStatement.put('assessmentYear','2020'); 
            inputParamsStatement.put('recordTypeId',DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME));
            inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/2016'));
            
            Statement__c bppStatement = TestDataUtility.buildStatement(inputParamsStatement);
            insert bppStatement;
            
        }
    
    
    @isTest
    private static void testStatementAvailability(){
        Test.startTest();
        List<statement__c> statements = [select id from statement__c];
        apexpages.currentpage().getparameters().put('recordID' , statements.get(0).id);
        CertificationDisplayController certificationDisplayController = new CertificationDisplayController();
        Statement__c statement = CertificationDisplayController.getStatement();
        Test.stopTest();
        system.assertNotEquals(null, statement);
    }
    
    @isTest
    private static void TestRedirectionPage(){
        Test.startTest();
        List<statement__c> statements = [select id from statement__c];
        apexpages.currentpage().getparameters().put('recordID' , statements.get(0).id);
        CertificationDisplayController certificationDisplayController = new CertificationDisplayController();
        PageReference reference = CertificationDisplayController.redirect();
        system.debug('reference'+reference.getUrl());
        Test.stopTest();
        system.assertEquals('/apex/CertificateNavigation?recordId='+statements.get(0).id, reference.getUrl());
    }
    
}