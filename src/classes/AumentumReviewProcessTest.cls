/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR: 2366
// @description : This class is used to AumentumReviewProcessController Test class.
//-----------------------------
@istest
public with sharing class AumentumReviewProcessTest {
    
    @testSetup
    static void createTestData() {
        List<StagingAumentumBusiness__c> stagings= new List<StagingAumentumBusiness__c>();
        StagingAumentumBusiness__c staging1 = TestDataUtility.buildStagingAumentumBusiness('BAN0001','LIN001','11-2345678','DBA-001', 'Jack & Jill','CA','SAN FRANCISCO','0','1','l-1','1155 Market','1998-10-10','2000-10-10','1998-10-10','2000-10-10');
        StagingAumentumBusiness__c staging2 = TestDataUtility.buildStagingAumentumBusiness('BAN0002','LIN002','11-2345679','DBA-002', 'Jill JK','CA','SAN FRANCISCO','0','1','l-2','1176 Mango','1998-10-10','2000-10-10','1998-10-10','2020-01-01');
        StagingAumentumBusiness__c staging3 = TestDataUtility.buildStagingAumentumBusiness('BAN0003','LIN003','11-2345677','DBA-003', 'MK Ahmed','CA','SAN FRANCISCO','1','0','l-13','1176 MangoValley','1998-10-10','2000-10-10','1998-10-10','2020-01-01');
        stagings.add(staging1);
        stagings.add(staging2);
        stagings.add(staging3);
        
        insert stagings;
        
        List<Account> accounts= new List<Account>();
        Account account1=TestDataUtility.buildAccountAumentum('Jack & Jill', 'BAN0001', '11-2345678', Date.newInstance(1998,04,01), null);
        Account account2=TestDataUtility.buildAccountAumentum('Jill JK', 'BAN0002', '11-2345679', Date.newInstance(1998,10,01), null);
        Account account3= TestDataUtility.buildAccountAumentum('MK Ahmed', 'BAN0003', '11-2345677', null, null);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        
        insert accounts;
        
        List<Property__c> properties = new List<Property__c>();
        Property__c property1=TestDataUtility.buildAumentumProperty('LIN001','DBA-001','571-STR',  Date.newInstance(1998,04,01),null,account1.Id, '1155 Market');
        Property__c property2=TestDataUtility.buildAumentumProperty('LIN002','DBA-002','571-L',  Date.newInstance(1998,04,01),null,account2.Id, '1176 Mango');
        Property__c property3=TestDataUtility.buildAumentumProperty('LIN003','DBA-003','571-L',  Date.newInstance(1998,04,01),null,account3.Id, '1176 MangoValley');
        properties.add(property1);
        properties.add(property2);
        properties.add(property3);
        
        insert properties;
        
        List<StagingBusinessCompareField__c> compareFields= new List<StagingBusinessCompareField__c>();
        StagingBusinessCompareField__c compare1=TestDataUtility.buildStagingCompareField(staging1.Id, account1.Id,property1.Id, '571-STR');
        StagingBusinessCompareField__c compare2=TestDataUtility.buildStagingCompareField(staging2.Id, account2.Id,property2.Id, '571-L');
        StagingBusinessCompareField__c compare3=TestDataUtility.buildStagingCompareField(staging3.Id, account2.Id,property3.Id, '571-L');
        compareFields.add(compare1);
        compareFields.add(compare2);
        compareFields.add(compare3);
        
        insert compareFields;
        
    }
    
    @istest
    static void reviewComponentTest(){
        Id  parentRecordId=[Select Id from StagingAumentumBusiness__c where BusinessAccountNumber__c='BAN0001' limit 1].Id;
        test.startTest();
        List<AumentumReviewProcessController.ResultWrapper> result = AumentumReviewProcessController.getChildTableValues(parentRecordId);
        AumentumReviewProcessController.ignoreChanges(parentRecordId);
        test.stopTest();
        StagingAumentumBusiness__c returnResult=[Select Id, Status__c from StagingAumentumBusiness__c where Id =:parentRecordId limit 1];
        system.assertEquals('Processed', returnResult.Status__c);
        system.assert(result !=null);
        
    }
    
    @istest
    static void updateChangesTest(){
        Id  parentRecordId=[Select Id from StagingAumentumBusiness__c where BusinessAccountNumber__c='BAN0002' limit 1].Id;
        test.startTest();
        
        List<AumentumReviewProcessController.ResultWrapper> result = AumentumReviewProcessController.getChildTableValues(parentRecordId);
        List<Object> resultObject=new List<Object>();
        for(AumentumReviewProcessController.ResultWrapper resultWrap: result ){
            innerWrapperTest innerWrap= new innerWrapperTest();
            innerWrap.Name= resultWrap.fieldLabel;
            innerWrap.Id =resultWrap.recordId;
            innerWrap.AccountId = resultWrap.mappedAccountId;
            innerWrap.ObjectName= resultWrap.objectName;
            innerWrap.PropertyId = resultWrap.mappedPropertyId;
            innerWrap.NewValue = resultWrap.integrationValue;           
            innerWrap.FieldAPI = resultWrap.fieldAPINameForUpdate;
            innerWrap.FieldStage= resultWrap.fieldAPINameOfStage;
            innerWrap.ParentId= resultWrap.parentId;            
            resultObject.add(innerWrap);
        }
        
        String recordsToBeUpdate = JSON.serialize(resultObject);       
        AumentumReviewProcessController.updateNewChanges(recordsToBeUpdate);
        test.stopTest();
        StagingAumentumBusiness__c returnResult=[Select Id, Status__c from StagingAumentumBusiness__c where Id =:parentRecordId limit 1];
        system.assertEquals('Processed', returnResult.Status__c);
        Id propertyUpdatedId=[Select MappedProperty__c from StagingBusinessCompareField__c where ParentStagingAumentumBusiness__c=: parentRecordId limit 1].MappedProperty__c;
        Property__c property=[Select Id, PrimaryFormType__c,Status__c,BusinessLocationCloseDate__c,BusinessLocationOpenDate__c
							  from Property__c 
                              where Id =: propertyUpdatedId limit 1 ];
        system.assertEquals('Inactive', property.Status__c);       
        
    }
    
    public class innerWrapperTest{
        String Name;
        String Id;
        String AccountId;
        String PropertyId;
        String NewValue;
        String ObjectName;
        String FieldAPI;
        String FieldStage;
        String ParentId;
        
        
    }
}