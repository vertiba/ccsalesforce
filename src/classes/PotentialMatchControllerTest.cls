/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR-2366 
// @description : This class test PotentialMatchController
//-----------------------------
@isTest
public with sharing class PotentialMatchControllerTest {
    @testSetup
    static void createTestData() {
      
        StagingAumentumBusiness__c staging1 = TestDataUtility.buildStagingAumentumBusiness('BAN0001','LIN001','11-2345678','DBA-001', 'Jack & Jill','CA','SAN FRANCISCO','0','1','l-1','1155 Market','1998-10-10','2000-10-10','1998-10-10','2000-10-10');
        StagingAumentumBusiness__c staging2 = TestDataUtility.buildStagingAumentumBusiness('BAN0002','LIN002','11-2345679','DBA-002', 'Jill JK','CA','SAN FRANCISCO','0','1','l-2','1176 Mango','1998-10-10','2000-10-10','1998-10-10','2020-01-01');
        StagingAumentumBusiness__c staging3 = TestDataUtility.buildStagingAumentumBusiness('BAN0003','LIN003','11-2345670','DBA-003', 'Jill','CA','SAN FRANCISCO','0','1','l-2','1176 Mango','1998-1-1','2000-1-1','1998-10-10','2020-01-01');
        StagingAumentumBusiness__c staging4 = TestDataUtility.buildStagingAumentumBusiness('BAN0001','LIN004','11-2345676','DBA-004', 'Jack & Jill','CA','SAN FRANCISCO','0','0','0','1176 Mango',null,null,null,null);
       
        insert new List<StagingAumentumBusiness__c> {staging1,staging2,staging3,staging4};
        
       
        Account account1=TestDataUtility.buildAccountAumentum('Jack & Jill', 'BAN0001', '11-2345678', Date.newInstance(1998,04,01), null);
        Account account2=TestDataUtility.buildAccountAumentum('Jill JK', 'BAN0002', '11-2345679', Date.newInstance(1998,10,01), null);
        Account account3= TestDataUtility.buildAccountAumentum('MK Ahmed', 'BAN0003', '11-2345677', null, null);
                
        insert new List<Account> {account1,account2,account3 };
        
       
        Property__c property1 = TestDataUtility.buildAumentumProperty('LIN001','DBA-001','571-STR',  Date.newInstance(1998,04,01),null,account1.Id, '1155 Market');
        Property__c property2 = TestDataUtility.buildAumentumProperty('LIN002','DBA-002','571-L',  Date.newInstance(1998,04,01),null,account2.Id, '1176 Mango');
        Property__c property3 = TestDataUtility.buildAumentumProperty('LIN003','DBA-003','571-L',  Date.newInstance(1998,04,01),null,account3.Id, '1176 MangoValley');
        property3.Status__c = CCSFConstants.PROPERTY.STATUS_INACTIVE;
        property3.BusinessLocationCloseDate__c = Date.newInstance(2020,04,01);
        Property__c property4 = TestDataUtility.buildAumentumProperty('LIN004','DBA-004','571-L',  Date.newInstance(1998,04,01),null,account1.Id, '1176 MangoValley');
               
        insert new  List<Property__c>{property1,property2,property3,property4};
        
        PotentialMatchForBusinessAndLocation__c match1=TestDataUtility.buildPotentialMatchAumentum(staging2.Id,account2.Id, null);      
        PotentialMatchForBusinessAndLocation__c match2=TestDataUtility.buildPotentialMatchAumentum(staging1.Id,null, property2.Id);
        PotentialMatchForBusinessAndLocation__c match3=TestDataUtility.buildPotentialMatchAumentum(staging3.Id,null, property3.Id);
        PotentialMatchForBusinessAndLocation__c match4=TestDataUtility.buildPotentialMatchAumentum(staging4.Id,null, property4.Id);
          
        insert new List<PotentialMatchForBusinessAndLocation__c>{match1, match2,match3,match4 };       
        
    }
    
    @isTest
    static void potentialMatchPropertyReviewTest(){
        Id  parentRecordId=[Select Id from StagingAumentumBusiness__c where LocationIdentificationNumber__c ='LIN001' limit 1].Id;
        test.startTest();
        List<PotentialMatchController.ResultWrapper> result = PotentialMatchController.getDataForSelection(parentRecordId);
        system.assert(result != null);      
        List<Object> resultObject=new List<Object>();
        for(PotentialMatchController.ResultWrapper resultWrap: result ){
            innerWrapperTest innerWrap= new innerWrapperTest();
            innerWrap.ObjectId= resultWrap.objectId;
            innerWrap.MatchId = resultWrap.matchId;
            innerWrap.ObjectName = resultWrap.objectName;                      
            resultObject.add(innerWrap);
        }        
        String recordsToBeUpdate = JSON.serialize(resultObject);       
        PotentialMatchController.updateNewChanges(recordsToBeUpdate,parentRecordId);
        test.stopTest();
        StagingAumentumBusiness__c returnResult=[Select Id, MappedProperty__c,Status__c, MappedAccount__c, Messages__c
                                                 from StagingAumentumBusiness__c
                                                 where Id =:parentRecordId];        
        system.assertEquals(CCSFConstants.AUMENTUM_PROCESS.STATUS_REVIEW, returnResult.Status__c);        
        system.assert(returnResult.MappedProperty__c != null);
        system.assert(returnResult.MappedAccount__c != null);  
        system.assertEquals(CCSFConstants.AUMENTUM_PROCESS.POTENTIAL_MESSAGE_FOR_IN_REVIEW, returnResult.Messages__c );     
       
        
    }
    @isTest
    static void potentialMatchAccountTest(){
        Id  parentRecordId=[Select Id from StagingAumentumBusiness__c where BusinessAccountNumber__c ='BAN0002' limit 1].Id;
       
        test.startTest();
        List<PotentialMatchController.ResultWrapper> result = PotentialMatchController.getDataForSelection(parentRecordId);
        system.assert(result != null);     
        List<Object> resultObject=new List<Object>();
        for(PotentialMatchController.ResultWrapper resultWrap: result ){
            innerWrapperTest innerWrap= new innerWrapperTest();
            innerWrap.ObjectId= resultWrap.objectId;
            innerWrap.MatchId = resultWrap.matchId;
            innerWrap.ObjectName = resultWrap.objectName;                      
            resultObject.add(innerWrap);
        }        
        String recordsToBeUpdate = JSON.serialize(resultObject);       
        PotentialMatchController.updateNewChanges(recordsToBeUpdate,parentRecordId);
        test.stopTest();
        StagingAumentumBusiness__c returnResult=[Select Id,Status__c, MappedAccount__c,MappedProperty__c
                                                 from StagingAumentumBusiness__c
                                                 where Id =:parentRecordId];

        system.assertEquals('Warning', returnResult.Status__c);       
        system.assert(returnResult.MappedAccount__c != null);
        
    }
    @isTest
    static void potentialMatchPropertyStatusIgnoreTest(){
        Id  parentRecordId=[Select Id from StagingAumentumBusiness__c where BusinessAccountNumber__c='BAN0003' limit 1].Id;      
        test.startTest();
        List<PotentialMatchController.ResultWrapper> result = PotentialMatchController.getDataForSelection(parentRecordId);
        system.assert(result != null);      
        List<Object> resultObject=new List<Object>();
        for(PotentialMatchController.ResultWrapper resultWrap: result ){
            innerWrapperTest innerWrap= new innerWrapperTest();
            innerWrap.ObjectId= resultWrap.objectId;
            innerWrap.MatchId = resultWrap.matchId;
            innerWrap.ObjectName = resultWrap.objectName;                      
            resultObject.add(innerWrap);
        }        
        String recordsToBeUpdate = JSON.serialize(resultObject);       
        PotentialMatchController.updateNewChanges(recordsToBeUpdate,parentRecordId);
        test.stopTest();
        StagingAumentumBusiness__c returnResult=[Select Id, MappedProperty__c,Status__c, MappedAccount__c, Messages__c
                                                 from StagingAumentumBusiness__c
                                                 where Id =:parentRecordId];        
        system.assertEquals(CCSFConstants.AUMENTUM_PROCESS.STATUS_IGNORE, returnResult.Status__c);
        system.assert(returnResult.MappedProperty__c != null);
        system.assert(returnResult.MappedAccount__c != null);
        system.assertEquals(CCSFConstants.AUMENTUM_PROCESS.POTENTIAL_MESSAGE_FOR_IGNORE, returnResult.Messages__c );
        
    }

    @isTest
    static void potentialMatchPropertyProcessedTest(){
        Id  parentRecordId=[Select Id from StagingAumentumBusiness__c where LocationIdentificationNumber__c ='LIN004' limit 1].Id;
        test.startTest();
        List<PotentialMatchController.ResultWrapper> result = PotentialMatchController.getDataForSelection(parentRecordId);
        system.assert(result != null);      
        List<Object> resultObject=new List<Object>();
        for(PotentialMatchController.ResultWrapper resultWrap: result ){
            innerWrapperTest innerWrap= new innerWrapperTest();
            innerWrap.ObjectId= resultWrap.objectId;
            innerWrap.MatchId = resultWrap.matchId;
            innerWrap.ObjectName = resultWrap.objectName;                      
            resultObject.add(innerWrap);
        }        
        String recordsToBeUpdate = JSON.serialize(resultObject);       
        PotentialMatchController.updateNewChanges(recordsToBeUpdate,parentRecordId);
        test.stopTest();
        StagingAumentumBusiness__c returnResult=[Select Id, MappedProperty__c,Status__c, MappedAccount__c,Messages__c
                                                 from StagingAumentumBusiness__c
                                                 where Id =:parentRecordId];        
        system.assertEquals(CCSFConstants.AUMENTUM_STATUS_PROCESSED, returnResult.Status__c);
        system.assert(returnResult.MappedProperty__c != null);
        system.assert(returnResult.MappedAccount__c != null);
        system.assertEquals(CCSFConstants.AUMENTUM_PROCESS.POTENTIAL_MESSAGE_FOR_PROCESSED, returnResult.Messages__c );
        
    }
    
    public class innerWrapperTest{
        String ObjectId;
        String MatchId;
        String ObjectName;     
        
    }
    
}