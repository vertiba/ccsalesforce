/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//                            Class to test UpdateVesselNoticeType
//-----------------------------
@isTest
public class UpdateVesselNoticeTypeTest {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    private static void dataSetup(){
        TestDataUtility.disableAutomationCustomSettings(true);
        String rollYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        String lastYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        
        List<RollYear__c> rollYears = new List<RollYear__c>();
        RollYear__c rollYear1 = TestDataUtility.buildRollYear(lastYear,lastYear,CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYears.add(rollYear1);
        RollYear__c rollYear2 = TestDataUtility.buildRollYear(rollYear,rollYear,CCSFConstants.ROLLYEAR.ROLL_YEAR_Close_STATUS);
        rollYears.add(rollYear2);
        insert rollYears;
        
        //insert account
        List<Account> accounts = new List<Account>();
        Account account1 = TestDataUtility.getBusinessAccount('test account1',CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE);
        account1.TotalValueofProperties__c = 2000;
        accounts.add(account1);
        Account account2 = TestDataUtility.getBusinessAccount('test account2',CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE);
        account2.TotalValueofProperties__c = 50000;
        accounts.add(account2);
        Account account3 = TestDataUtility.getBusinessAccount('test account3',CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE);
        account3.TotalValueofProperties__c = 2000;
        accounts.add(account3);
        Account account4 = TestDataUtility.getBusinessAccount('test account4',CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE);
        account4.TotalValueofProperties__c = 2000;
        accounts.add(account4);
        insert accounts;
        
        Penalty__c penalty = new Penalty__c();
        penalty.PenaltyMaxAmount__c = 500;
        penalty.Percent__c = 10;
        penalty.RTCode__c = '463';
        insert penalty;
        
        List<Property__c> properties = new List<Property__c>();        
        Id vesselRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Vessel').getRecordTypeId();
        Property__c property1 = TestDataUtility.buildProperty('test property1',vesselRecordTypeId);
        property1.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
        property1.Account__c = account1.Id;
        properties.add(property1);
        Property__c property2 = TestDataUtility.buildProperty('test property2',vesselRecordTypeId);
        property2.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
        property2.Account__c = account2.Id;
        properties.add(property2);
        Property__c property3 = TestDataUtility.buildProperty('test property3',vesselRecordTypeId);
        property3.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
        property3.Account__c = account3.Id;
        properties.add(property3);
        Property__c property4 = TestDataUtility.buildProperty('test property4',vesselRecordTypeId);
        property4.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
        property4.Account__c = account4.Id;
        properties.add(property4);
        insert properties;
        
        List<Case> casestests = new List<Case>();
        Case cas  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(
            Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,
                                                           rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,null,null,
                                                           Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1));
        cas.Property__c = property1.Id;
        cas.IntegrationStatus__c = 'Ready to send to TTX';
        casestests.add(cas);
        
        Case cas2  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(
            Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,
                                                           rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,null,null,
                                                           Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1));
        cas2.Property__c = property2.Id;
        casestests.add(cas2);
        
        Case cas3  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(
            Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,
                                                           rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,null,null,
                                                           Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1));
        cas3.Property__c = property3.Id;
        casestests.add(cas3);
        
        Case cas4  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(
            Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,
                                                           rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,null,null,
                                                           Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1));
        cas4.Property__c = property3.Id;
        casestests.add(cas4);
        
        Case cas5  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(
            Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,
                                                           rollYears[0].Id,rollYears[0].Name,rollYears[0].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_ESCAPE,4,null,null,
                                                           Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1));
        cas5.Property__c = property3.Id;
        casestests.add(cas5);
        insert casestests; 
        TestDataUtility.disableAutomationCustomSettings(false);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to check if Notice type is getting updated
    //----------------------------- 
    @isTest
    private static void SetNoticeTypeForAllKindsOfProperty(){
        TestDataUtility.disableAutomationCustomSettings(true);
        List<Case> cases = [select id, Penalty__c,RollYear__c,Property__r.name from Case];
        for(Case case1 : cases){
            case1.Penalty__c = null;
            case1.RollYear__c =String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        }
        update cases;
        
        List<Property__c> allProperty = [select name, MostRecentAssessedValue__c from Property__c];
        for(Property__c property : allProperty){
            if(property.Name=='test property1'){
                property.MostRecentAssessedValue__c = Integer.valueOf(System.Label.VesselPropertyLowCostCeilingAmount)-10;
            }else{
                property.MostRecentAssessedValue__c = Integer.valueOf(System.Label.VesselPropertyDirectBillCeilingAmount)-10;
            }                      
        }
        update allProperty;
        TestDataUtility.disableAutomationCustomSettings(false);
        Test.startTest();
        Database.executeBatch(new UpdateVesselNoticeType());
        Test.stopTest();
        
        List<Property__c> properties = [select name, NoticeType__c from Property__c];
        for(Property__c property : properties){
            if(property.name == 'test property1'){
                System.assertEquals('Low Value', property.NoticeType__c);
            }else if(property.name == 'test property2'){
                System.assertEquals('Direct Bill', property.NoticeType__c);
            }else if(property.name == 'test property3'){
                System.assertEquals('Notice to File', property.NoticeType__c);
            }else if(property.name == 'test property4'){
                System.assertEquals('Notice to File', property.NoticeType__c);
            }
        }
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to check if Notice type is getting updated for Regular with Penalty cases
    //----------------------------- 
    @isTest
    private static void checkNoticeTypeForRegularWithPenaltyCase(){
        TestDataUtility.disableAutomationCustomSettings(true);
        Penalty__c penalty = [select id from Penalty__c];
        Case caseTest = [select id, Penalty__c,RollYear__c,Property__r.name from Case where Property__r.name Like 'test property1%'];
        caseTest.Penalty__c=penalty.id;
        caseTest.RollYear__c =String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        update caseTest;
        
        List<Property__c> allProperty = [select name, MostRecentAssessedValue__c from Property__c];
        for(Property__c property : allProperty){
            property.MostRecentAssessedValue__c = Integer.valueOf(System.Label.VesselPropertyDirectBillCeilingAmount)-10;
        }
        update allProperty;
        TestDataUtility.disableAutomationCustomSettings(false);
        
        Test.startTest();
        Database.executeBatch(new UpdateVesselNoticeType());
        Test.stopTest();
        
        List<Property__c> properties = [select name, NoticeType__c from Property__c];
        for(Property__c property : properties){
            if(property.name == 'test property1'){
                System.assertEquals('Notice to File', property.NoticeType__c);
            }
        }
    }


}