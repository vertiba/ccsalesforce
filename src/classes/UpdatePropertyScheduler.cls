/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description :  UpdatePropertyScheduler schedulable cost This Batch will un 30th June every year
//-----------------------------
public class UpdatePropertyScheduler extends EnhancedSchedulable {      
    
     public override String getJobNamePrefix() {
        return 'Update Property Assessed Value Scheduler';
    }  
    // For Scheduling Batch class 
    public void execute(SchedulableContext sc){
        try{            
             Logger.addDebugEntry('After Entering UpdatePropertyScheduler:', 'UpdatePropertyAssessedCost.execute');
            Id batchProcessId = Database.executeBatch(new UpdatePropertyAssessedCost());
            Logger.addDebugEntry('After Scheduling batch, jobid:'+batchProcessId, 'UpdatePropertyAssessedCost.execute');
            
        }catch (Exception ex){
           Logger.addExceptionEntry(ex, 'UpdatePropertyAssessedCost.execute');
        }
        Logger.saveLog();
    }  
    
}