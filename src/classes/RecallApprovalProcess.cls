/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient    
// @param  : assessment
// @description : This is to Recall the Approval Process.
// @return : void
//------------------------------
public class RecallApprovalProcess {
    
    public class RecallApproval{
        @InvocableVariable(required = true)
        public List<Id> recordIds;
    }
    // Called in flow Create Assessment from Statement and Create Vessel Assessment From Statements
    @InvocableMethod(label='Recall Approval Process')
    public static void RecallApproval(List<Id> recordIds){
        ProcessInstanceWorkitem[] workItems = [SELECT Id
                                               FROM ProcessInstanceWorkitem
                                               WHERE ProcessInstance.TargetObjectId = :recordIds];
       //Return if workItem is null
       if(workItems.size() == 0){ 
            return; 
        } 
        Approval.ProcessWorkItemRequest processworkItemRequest = new Approval.ProcessWorkItemRequest(); 
        //Set the action
        processworkItemRequest.setAction('Reject');
        //Set the comments
        processworkItemRequest.setComments(System.Label.ApprovalRejected);
        processworkItemRequest.setWorkItemId(workItems[0].id);
        //Process the Approval Request
        try{
            Approval.ProcessResult result = Approval.process(processworkItemRequest);
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, 'RecallApprovalProcess');
            Logger.savelog();
            throw ex;
        }
    }
}