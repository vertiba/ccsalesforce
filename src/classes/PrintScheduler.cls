/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class PrintScheduler extends EnhancedSchedulable {

    public override String getJobNamePrefix() {
        return 'Print Scheduler';
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : SchedulableContext
    // @description : Schedulable interface method. Schedules the PrintMailerBatch job,
    //         by sending a list of print attributes.
    // @return : void
    //-----------------------------
    public void execute(SchedulableContext schedulableContext) {
        Logger.addDebugEntry('After Entering PrintScheduler:', 'PrintScheduler.execute');
        Id batchProcessId = Database.executebatch(new PrintMailerBatch());
        Logger.addDebugEntry('After Scheduling bacth, jobid:'+batchProcessId, 'PrintScheduler.execute');
        Logger.saveLog();
    }

}