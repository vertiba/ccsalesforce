@IsTest
/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class AgentWorkUtilityTest {
    @TestSetup
    static void makeData(){
        RollYear__c closedRollYear = TestDataUtility.buildRollYear('Closed Year', '2018', CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS);
        insert closedRollYear;

        RollYear__c rollyear = TestDataUtility.buildOpenRollYear('Test Year', '2019');
        insert rollyear;

        Account testAccount = TestDataUtility.buildBusinessAccount('Test Account');
        insert testAccount;

        Property__c testProperty = TestDataUtility.buildBusinessPersonalProperty('TestBPP', testAccount.Id);
        insert testProperty;

        Penalty__c testPenalty = TestDataUtility.buildPenalty(1000.0, 4, System.Label.Late_Filer_Penalty_R_T_Code);
        insert testPenalty;

        Case testAssessment =  TestDataUtility.buildRegularBPPAssessment(testProperty.Id, rollyear, '2019', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, testPenalty.Id, Date.newInstance(2019,1,1));
        testAssessment.ApplyPenaltyReason__c = 'Reason';
        insert testAssessment;

        Statement__c testStatement = TestDataUtility.buildStatement(testAccount.Id, testProperty.Id, '571-L', '', '', '2019', Schema.SObjectType.Statement__c.getRecordTypeInfosByName().get('Business Personal Property Statement').getRecordTypeId(), Date.newInstance(2019,1,1), Date.newInstance(2019, 6, 15));
        testStatement.Assessment__c = testAssessment.Id;
        insert testStatement;
    }
    @isTest
    private static List<User> buildUsers(){
        Profile auditorProfile = [SELECT Id FROM Profile WHERE Name = :CCSFConstants.OMNICHANNEL.AUDITOR_PROFILE_NAME];

        User testUser1 = TestDataUtility.buildUser('Test', 'Test@Test.dsfdafsdsdf', 'TT', auditorProfile.Id, null);
        User testUser2 = TestDataUtility.buildUser('NewGuy', 'newGuy@new.gtrfcdsbnyuy', 'NG', auditorProfile.Id, null);
        List<User> users = new List<User>{testUser1, testUser2};

        return users;
    }

    @isTest
    public static void testReassignement(){
        List<User> testUsers = buildUsers();
        insert testUsers;
        User user1 = testUsers[0];
        User user2 = testUsers[1];

        System.runAs(user1){

            Case testAssessment = [SELECT Id, OwnerId FROM Case];
            testAssessment.OwnerId = user1.Id;
            update testAssessment;

            Statement__c testStatement = [SELECT Id, OwnerId FROM Statement__c];
            testStatement.OwnerId = user2.Id;
            update testStatement;

            testStatement = [SELECT Id, OwnerId, Assessment__r.OwnerId FROM Statement__c];
            System.assertNotEquals(testStatement.OwnerId, testStatement.Assessment__r.OwnerId, 'Test only succeeds if Statement and Assessment are originally owned by different people.');
            
            Test.startTest();
            
            AgentWorkUtility.assignParentAssessmentToStatementOwner(new List<Id>{testStatement.id});
            
            Test.stopTest();
        }
        
        Statement__c testStatement = [SELECT Id, OwnerId, Assessment__r.OwnerId FROM Statement__c];
        System.assertEquals(testStatement.OwnerId, testStatement.Assessment__r.OwnerId, 'Assessment was not reassigned to Statement Owner');



    }
}