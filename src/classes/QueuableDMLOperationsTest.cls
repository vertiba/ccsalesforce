/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//----------------------------------------------------------
// @author : Publicis.Sapient
// @description: test class for QueuableDMLOperations class
//----------------------------------------------------------
@isTest
private class QueuableDMLOperationsTest {

    @testSetup
    static void setupData() {
		Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account account = TestDataUtility.buildAccount('test account', 'Notice to File', recordTypeId);
        insert account;
    }

    @isTest
    private static void testQueuableCallWithInsertion(){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account account = TestDataUtility.buildAccount('Test Queue Account', 'Notice to File', recordTypeId);
        QueuableDMLOperations queueable = new QueuableDMLOperations(new List<Account>{account}, 'Insert');
        Test.startTest();
        System.enqueueJob(queueable);
        Test.stopTest();
        List<Account> accounts = [select id from account];
        System.assertEquals(2, accounts.size(),'1 account from setup and another from the queuable class');
    }
    
    @isTest
    private static void testOtherCallActions(){
        Account account = [select id, name from account];
        account.name = 'Updated in Call action';
        
        QueuableDMLOperations queueable = new QueuableDMLOperations(new List<Account>{account}, 'Update');
        //Queue can be called only once in test method, to test others calling call method directly.
        queueable.call('Update',new Map<String,Object>());
        Account accountToVerify = [select id, name from account];
        System.assertEquals(account.name, accountToVerify.name);
        
        Object returnObject = queueable.call('Test',new Map<String,Object>());
        System.assertEquals(null, returnObject);
        
        queueable.call('Delete',new Map<String,Object>());
        List<Account> accounts = [select id from account];
        System.assertEquals(0, accounts.size(), 'Account should be deleted');
    }
}