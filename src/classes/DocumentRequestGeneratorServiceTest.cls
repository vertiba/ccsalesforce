/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
* @Business: Test class for DocumentRequestGeneratorService
* @Date: 01/28/2020
* @Author: Srini Aluri(A1)

* ModifiedBy         ModifiedDate   Description
* Srini Aluri(A1)    01/31/2020     Initial development
*/

@isTest
public class DocumentRequestGeneratorServiceTest {
    
    @testSetup
    static void createTestData() {
        Account act = new Account();
        act.Name = 'Sample Property Account #101';
        act.MailingCountry__C='US';
        act.MailingStreetName__c ='355';
        act.MailingCity__c ='Oakland';
        INSERT act;
        
        Ownership__c ownership = new Ownership__c();
        ownership.StartDate__c = system.today();
        ownership.Account__c = act.Id;
        database.insert(ownership);
        
        Property__c prop = new Property__c();
        prop.Account__c = act.Id;
        prop.PrimaryOwnership__c = ownership.Id;
        prop.Name = 'Test Property #300';
        prop.MailingStreetName__c = '7800 Blackwood';
        prop.MailingCity__c = 'Portland';
        prop.MailingCountry__c = 'US';
        prop.Status__c = 'Active';
        prop.PrimaryFormType__c = '571-L';
        INSERT prop;
        
        List<TH1__Schema_Set__c> schemaSets = new List<TH1__Schema_Set__c>();
        TH1__Schema_Set__c schemaSet1 = new TH1__Schema_Set__c();
        schemaSets.add(schemaSet1);
        TH1__Schema_Set__c schemaSet2 = new TH1__Schema_Set__c();
        schemaSets.add(schemaSet2);
        INSERT schemaSets;
        
        List<TH1__Schema_Object__c> schemaObjects = new List<TH1__Schema_Object__c>();
        TH1__Schema_Object__c schemaObject1 = new TH1__Schema_Object__c();
        schemaObject1.TH1__Is_Primary_Object__c = true;
        schemaObject1.TH1__Object_Label__c = 'Account';
        schemaObject1.TH1__Schema_Set__c = schemaSets.get(0).Id;
        schemaObject1.Name = 'Account';
        schemaObjects.add(schemaObject1);
        
        TH1__Schema_Object__c schemaObject2 = new TH1__Schema_Object__c();
        schemaObject2.TH1__Is_Primary_Object__c = true;
        schemaObject2.TH1__Object_Label__c = 'Property__c';
        schemaObject2.TH1__Schema_Set__c = schemaSets.get(1).Id;
        schemaObject2.Name = 'Property__c';
        schemaObjects.add(schemaObject2);
        INSERT schemaObjects;
        
        schemaSets.get(0).TH1__Primary_Object__c = schemaObjects.get(0).Id;
        schemaSets.get(1).TH1__Primary_Object__c = schemaObjects.get(1).Id;
        UPDATE schemaSets;
        
        List<String> documentSettingNames = new List<String>{'Account Document Setting', 'Notice of Requirement to File 571-L'};
            List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        
        // Create Document Setting for Account
        TH1__Document_Setting__c docSetting1 = new TH1__Document_Setting__c();
        docSetting1.TH1__Document_Data_Model__c = schemaSets.get(0).Id;
        docSetting1.Name = documentSettingNames.get(0);
        docSetting1.TH1__Storage_File_Name__c = documentSettingNames.get(0);
        docSetting1.TH1__Is_disabled__c = false;
        docSetting1.TH1__Generate_document__c = true;
        docSetting1.TH1__Filter_field_name__c = 'NoticeTemplate__c';
        docSetting1.TH1__Filter_field_value__c = 'Notice to File';
        documentSettings.add(docSetting1);        
        
        // Create Document Setting for Property
        TH1__Document_Setting__c docSetting2 = new TH1__Document_Setting__c();
        docSetting2.TH1__Document_Data_Model__c = schemaSets.get(1).Id;
        docSetting2.Name = documentSettingNames.get(1);
        docSetting2.TH1__Storage_File_Name__c = documentSettingNames.get(1);
        docSetting2.TH1__Is_disabled__c = false;
        docSetting2.TH1__Generate_document__c = true;
        docSetting2.TH1__Filter_field_name__c = 'RecordTypeName__c';
        docSetting2.TH1__Filter_field_value__c = 'Business Personal Property';
        documentSettings.add(docSetting2);
        
        INSERT documentSettings;
    }
    
    static testMethod void testBatchJob() {
        Test.startTest();
        string query = 'SELECT Id,Status__c,HasMailingAddress__c,PrimaryFormType__c FROM Property__c WHERE CreatedDate = TODAY';
        DocumentRequestGeneratorService batch = new DocumentRequestGeneratorService(query);
        Database.executeBatch(batch);
        
        Test.stopTest();
        
        // Assert the functionality - We should expect DocumentGenerationRequest__c record to be created for Property
        Property__c prop = [SELECT Id, Name, PrimaryFormType__c, MailingStreetName__c FROM Property__c WHERE PrimaryFormType__c = '571-L' 
                             AND MailingStreetName__c = '7800 Blackwood'];
        
        List<DocumentGenerationRequest__c> docGenReqList = [SELECT Id, Name, Status__c FROM DocumentGenerationRequest__c 
                                                            WHERE RelatedRecordId__c = :prop.Id];
        System.assertEquals(true, prop.id!=null);
    }
    
    
}