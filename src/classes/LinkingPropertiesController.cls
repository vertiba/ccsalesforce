/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to display the records in comparartor component.
//-----------------------------
public class LinkingPropertiesController {

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : recordId
    // @param : parentId
    // @param : FEINStatus__c
    // @description : This method sets current account as a child account and to parent account. FEIN status 
    // 			is set to 'Use the parent account's FEIN', will deactivate the child record and clones 
    // 			all the properties under it to the parent's properties. Childs properties are deactivated. If
    // 			FEIN status is set to Keep the current FEIN, update the child account and leave everything else.
    // @return : void
    //-----------------------------
    @auraEnabled
    public static void updateParentIdWithFeinStatus(String accountId, String parentAccountId, String feinStatus) {
		Account childAccount = new Account(Id = accountId, parentId = parentAccountId, FEINStatus__c = feinStatus);
        Savepoint savePoint = Database.setSavepoint();
        try {
            if(feinStatus == 'Use the parent account\'s FEIN'){
            	childAccount.BusinessStatus__c = 'Inactive';
                childAccount.BusinessCloseDate__c = System.today();
                cloneAndReparentProperties(accountId,parentAccountId);
        	}
        	update childAccount;
        } catch (Exception e){
            Database.rollback(savePoint);
            throw new AuraHandledException(e.getMessage());
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : childAccountId
    // @param : parentAccountId
    // @description : Get all the properties for child account Id and update them as inactive. 
    // 			Clone all the properties and set account as parent account it and insert.
    // @return : Void
    //-----------------------------
    private static void cloneAndReparentProperties (Id childAccountId, Id parentAccountId) {
        String query = DescribeUtility.getCreatableFieldsSOQL('Property__c');
        query += ' where Account__c =: childAccountId and Status__c = \'Active\'';
        List<Property__c> existingChildProperties = Database.query(query);
        List<Property__c> clonedProperties = new List<Property__c>();
        Property__c clonedProperty;
        for(Property__c existingChildProperty : existingChildProperties){
            clonedProperty = existingChildProperty.clone(false,true);
            clonedProperty.Account__c = parentAccountId;
            clonedProperty.PropertyId__c = null;
            clonedProperty.LegacyCompanyId__c = null;
            clonedProperty.LocationIdentificationNumber__c = null;
            clonedProperty.VesselId__c = null;
            clonedProperty.OriginalProperty__c = existingChildProperty.Id;
            clonedProperties.add(clonedProperty);
            existingChildProperty.Status__c = 'Inactive';
        }
        update existingChildProperties;
        insert clonedProperties;
    }
}