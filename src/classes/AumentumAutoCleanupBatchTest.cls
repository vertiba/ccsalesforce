/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR- 2366, task -8502
// @description : This Batch class is used to cleanup Address data coming from Integration layer in StagingAumentumBusiness object
//-----------------------------
@isTest
public class AumentumAutoCleanupBatchTest {
    
    
     @isTest
    static void cleanUpTest(){
        List<StagingAumentumBusiness__c> records = Test.loadData(StagingAumentumBusiness__c.sObjectType, 'AumentumProcessTestData');

        test.startTest();
        Id batchId = database.executeBatch(new AumentumAutoCleanupBatch());
        List<StagingAumentumBusiness__c> processRcords = AumentumAutoCleanupController.cleanUpRecords(records);
        test.stopTest();
        List<Boolean> isDateCleaned = new List<Boolean>();
        List<Boolean> notCleaned = new List<Boolean>();
        for(StagingAumentumBusiness__c record:processRcords){
            if(record.IsDataClean__c == true){
                isDateCleaned.add(record.IsDataClean__c); 
            }else{
                notCleaned.add(record.IsDataClean__c);
            }           
        }
        
        system.assertEquals(records.size(),isDateCleaned.size(),'All data is cleaned up' );
        system.assert(notCleaned.size()==0,'No data left uncleaned');
        
    }
    
    @isTest
    static void errorFrameworkTest(){
        	 Id testJobId = '707S000000nKE4fIAG';
        	BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
            testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testJob.JobCreatedDate__c = System.today();
            testJob.JobId__c = testJobId;
            database.insert(testJob);
        
            List<StagingAumentumBusiness__c> records = Test.loadData(StagingAumentumBusiness__c.sObjectType, 'AumentumProcessTestData');
        	BatchApexError__c testError = new BatchApexError__c();
            testError.AsyncApexJobId__c = testJobId;
            testError.BatchApexErrorLog__c = testJob.Id;
            testError.DoesExceedJobScopeMaxLength__c = false;
            testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
            testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testError.JobScope__c = records[0].Id;
            testError.Message__c = 'Test exception';
            testError.RequestId__c = null;
            testError.StackTrace__c = '';
            database.insert(testError);
        
        BatchableError batchError = BatchableError.newInstance(testError);
        AumentumAutoCleanupBatch batchRetry= new AumentumAutoCleanupBatch();
        batchRetry.handleErrors(batchError);
    }

}