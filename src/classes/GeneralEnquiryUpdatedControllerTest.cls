/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: GeneralEnquiryUpdatedControllerTest 
 * Description : Test Class to validate the GeneralEnquiryUpdatedController
*/
@isTest
public with sharing class GeneralEnquiryUpdatedControllerTest {
    // Creating a new Enquiry
    public static testMethod void newEnquiry(){
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = TestDataUtility.createUserRecord('Test','Admin User 6','tuser6',profileId,'America/Los_Angeles', 'en_US','en_US',
                                                            'UTF-8','testuser6@test.com','testuser6@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            // Inserting Roll Year
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c='Roll Open';
            insert rollYear;
            // Inserting Penalty
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            database.insert(penalty1);
            
            // Inserting ContentDocument
            ContentVersion contentVersionInsert = new ContentVersion(Title = 'Test',
                                                                     PathOnClient = 'Test.jpg',
                                                                     VersionData = Blob.valueOf('Test Content Data'),
                                                                     IsMajorVersion = true);
            insert contentVersionInsert;
            
            ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE 
                                                   Id = :contentVersionInsert.Id LIMIT 1];
            List<ContentDocument> contentDocumentList = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            
            // Creating separate Wrappers for all types
            GeneralEnquiryUpdatedController.enquiryWrapper wrapper1 = new  GeneralEnquiryUpdatedController.enquiryWrapper();
            wrapper1.accountName = 'Test Account';
            wrapper1.doingBusiness = 'Test Account';
            wrapper1.subject = 'Test';
            wrapper1.description = 'Test';
            wrapper1.selectedType = 'Request Replacement Notice';
            if(!contentDocumentList.isEmpty()){
            	wrapper1.contentDocumentId = contentDocumentList[0].Id;    
            }else{
                wrapper1.contentDocumentId = null;
            }
            
            String wrapper1String = JSON.serialize(wrapper1);
            
            GeneralEnquiryUpdatedController.enquiryWrapper wrapper2 = new  GeneralEnquiryUpdatedController.enquiryWrapper();
            wrapper2.accountName = 'Test Account';
            wrapper2.doingBusiness = 'Test Account';
            wrapper2.subject = 'Test';
            wrapper2.description = 'Test';
            wrapper2.businessStartDate = system.today();
            wrapper2.newBusinessLocation = '350 Bush Street, CA, 94104';
            wrapper2.selectedType = 'Request New Business';
            if(!contentDocumentList.isEmpty()){
            	wrapper2.contentDocumentId = contentDocumentList[0].Id;    
            }else{
                wrapper2.contentDocumentId = null;
            }
            
            String wrapper2String = JSON.serialize(wrapper2);
            
            GeneralEnquiryUpdatedController.enquiryWrapper wrapper3 = new  GeneralEnquiryUpdatedController.enquiryWrapper();
            wrapper3.accountName = 'Test Account';
            wrapper3.vesselName = 'Test Account';
            wrapper3.vesselOwner = 'Account';
            wrapper3.vesselId = '123456';
            wrapper3.subject = 'Test';
            wrapper3.description = 'Test';
            wrapper3.vesselIdType = 'DMV#';
            wrapper3.fishGameBoatNumber = '123';
            wrapper3.vesselLocation = 'California';
            wrapper3.dateBusinessMoved = system.today();
            wrapper3.selectedType = 'Request New Vessel';
            if(!contentDocumentList.isEmpty()){
            	wrapper3.contentDocumentId = contentDocumentList[0].Id;    
            }else{
                wrapper3.contentDocumentId = null;
            }
            
            String wrapper3String = JSON.serialize(wrapper3);
            
            GeneralEnquiryUpdatedController.enquiryWrapper wrapper4 = new  GeneralEnquiryUpdatedController.enquiryWrapper();
            wrapper4.subject = 'Test';
            wrapper4.description = 'Test';
            wrapper4.selectedType = 'Other';
            if(!contentDocumentList.isEmpty()){
            	wrapper4.contentDocumentId = contentDocumentList[0].Id;    
            }else{
                wrapper4.contentDocumentId = null;
            }
            
            String wrapper4String = JSON.serialize(wrapper4);
            
            System.Test.startTest();
            GeneralEnquiryUpdatedController.createEnquiry(wrapper1String);
            GeneralEnquiryUpdatedController.createEnquiry(wrapper2String);
            GeneralEnquiryUpdatedController.createEnquiry(wrapper3String);
            GeneralEnquiryUpdatedController.createEnquiry(wrapper4String);
            System.Test.stopTest();

            System.assertEquals(contentDocumentList.size(), 1);
        }
    }
}