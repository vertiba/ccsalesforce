/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is required for automation testing from Selenium.
//-----------------------------
@RestResource(urlMapping='/Initiatejobs/*')
global with sharing class InitiateBatchJobs{
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method executes batch classes based on provided input in URL
    // sample URL : /services/apexrest/Initiatejobs?jobName=Print
    // @return : String
    //-----------------------------
    @HttpGet
    global static String initiateJobs() {
        String jobName = RestContext.request.params.get('jobName');
        String message = 'Wrong job name provided';
        if(String.isBlank(jobName)){
            return message;
        }
        try {
            // Get the Type 'token' for the Class
            System.Type classType = System.Type.forName(jobName);
            if (classType != null) {
                // In future this has to be changed to abstract class which is being created as part of 
                // ASR - 8328, so that we can pass params also. If required in future.
                // as well for any class that requires parameter.
                // Database.Batchable<sObject> batch = (Database.Batchable<sObject>) JSON.deserialize('{}',classType);
                Database.Batchable<sObject> batch = (Database.Batchable<sObject>) classType.newInstance();
                Database.executebatch(batch);
                return 'Job initiated';
            }
        } catch (Exception ex) {
            System.debug('Error while creating batch job:'+jobName);
        }
        return message;
    }
}