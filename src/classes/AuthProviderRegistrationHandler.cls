/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
global without sharing class AuthProviderRegistrationHandler implements Auth.RegistrationHandler{

    private static final String LOG_ORIGIN_LOCATION = 'AuthProviderRegistrationHandler';
    private static final String ORG_SUFFIX          = 'ccsf.community'; // TODO make dynamic/blank for production

    /**
    * Let anyone register as long as the required fields are supplied
    *
    * We require email, first name and last name
    *
    * @userData - the user's info from the Auth Provider
    **/
    global Boolean canCreateUser(Auth.UserData userData) {
        Logger.addDebugEntry('canCreateUser was called for ' + (userData != null ? userData.email : 'null'), LOG_ORIGIN_LOCATION);

        Boolean canCreateUser = (userData != null && userData.email != null && userData.lastName != null && userData.firstName != null);
        Logger.addDebugEntry(LoggingLevel.FINE, 'canCreateUser=' + canCreateUser, LOG_ORIGIN_LOCATION);

        Logger.addDebugEntry(LoggingLevel.FINER, 'userData.username=' + userData.username, LOG_ORIGIN_LOCATION);
        Logger.addDebugEntry(LoggingLevel.FINER, 'userData.email=' + userData.email, LOG_ORIGIN_LOCATION);
        Logger.addDebugEntry(LoggingLevel.FINER, 'userData.lastName=' + userData.lastName, LOG_ORIGIN_LOCATION);
        Logger.addDebugEntry(LoggingLevel.FINER, 'userData.firstName=' + userData.firstName, LOG_ORIGIN_LOCATION);

        Logger.saveLog();

        return canCreateUser;
    }

    /**
    * Create the User - A required method to implement the handler interface
    *
    * @param portalId  - Id of the Community
    * @param userData - Auth provider's user data describing the user to create
    *
    * @return User that has been initialized
    **/
    global User createUser(Id portalId, Auth.UserData userData){
        // Returning null signals the auth framework we can't create the user
        if(!canCreateUser(userData)) return null;

        User user;
        // Is this a Community Context?
        if(userData.attributeMap.containsKey('sfdc_networkid')) {
            Logger.addDebugEntry('Registering Community user: ' + userData.email, LOG_ORIGIN_LOCATION);

            // To keep things modular, we're creating the Contact in a separate method
            Id personAccountContactId = createPersonAccount(userData);
            Logger.addDebugEntry('Created person account, contact ID is: '+ personAccountContactId, LOG_ORIGIN_LOCATION);

            // Keeping it modular, we initialize the user in another method
            user = createUser(userData, Profiles.DEFAULT_EXTERNAL_USER_PROFILE);

            user.ContactId = personAccountContactId;
        } else {
            //This is not a community, so we assign the default internal profile
            user = createUser(userData, Profiles.DEFAULT_INTERNAL_USER_PROFILE);
        }

        Logger.saveLog();
        return user;
    }

    /**
    * Update the user
    * @param portalId  - Id of the Community
    * @param userData - Auth provider's user data describing the user to create
    **/
    global void updateUser(Id userId, Id portalId, Auth.UserData userData) {
        Logger.addDebugEntry('updateUser method called for: ' + userData.email + ', portalId: ' + portalId, LOG_ORIGIN_LOCATION);

        User user = new User(
            Email     = userData.email,
            FirstName = userData.firstName,
            Id        = userId,
            LastName  = userData.lastName
        );
        update user;

        Logger.saveLog();
    }

    /**
     * Create a Person Account
     *
     * @param data - Auth provider's context for the user
     **/
    private Id createPersonAccount(Auth.UserData userData) {
        Logger.addDebugEntry('createPersonAccount method called for : ' + userData.email, LOG_ORIGIN_LOCATION);
        Logger.addDebugEntry(LoggingLevel.FINE, Json.serialize(userData), LOG_ORIGIN_LOCATION);

        Account personAccount = new Account(
            FirstName   = userData.firstName,
            LastName    = userData.lastName,
            PersonEmail = userData.email
        );

        try {
            insert personAccount;
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, LOG_ORIGIN_LOCATION);
            Logger.saveLog();
            throw ex;
        }
        // Requery to the person account so we can return the contact ID
        personAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :personAccount.Id];

        String personAccountDebugMessage = 'Person Account created for ' + userData.email
            + ', id=' + personAccount.id
            + ', contactId=' + personAccount.PersonContactId;
        Logger.addDebugEntry(personAccountDebugMessage, LOG_ORIGIN_LOCATION);

        return personAccount.PersonContactId;
    }


    /**
     * Create and initialize the User
     *
     * @param userData - the provided User context from FaceBook
     * @param profile - the Profile we are going to assign to this user
     *
     * @return User that has been initialized but not saved
     **/
    private User createUser(Auth.UserData userData, Profile profile) {
        String alias = userData.firstName + userData.lastName;
        //Alias must be 8 characters or less
        if(alias.length() > 8) alias = alias.substring(0, 8);

        User user = new User(
            Alias             = alias,
            Email             = userData.email,
            EmailEncodingKey  = 'UTF-8',
            FirstName         = userData.firstName,
            LanguageLocaleKey = UserInfo.getLocale(),
            LastName          = userData.lastName,
            Localesidkey      = UserInfo.getLocale(),
            ProfileId         = profile.Id,
            TimeZoneSidKey    = 'America/Los_Angeles',
            Username          = userData.email + '.' + userData.provider + '.' + ORG_SUFFIX
        );

        return user;
    }

}