/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class PropertyHandler extends TriggerHandler {
    
    protected override void executeBeforeInsert(List<SObject> newRecords) {
        List<Property__c> newProperties = (List<Property__c>)newRecords;
        
        this.setName(newProperties);
        this.setAddressFields(newProperties);
        // Populate Property Id
        this.populateCompanyIdInProperty(newProperties);
    }
    
    protected override void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {
        List<Property__c> updatedProperties = (List<Property__c>)updatedRecords;
        
        this.setName(updatedProperties);
        this.setAddressFields(updatedProperties);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Property__c>
    // @description : This method sets the record name based on a formula field. This is being done in code due a bug with process builder.
    // @return : void
    //-----------------------------
    private void setName(List<Property__c> properties) {
        for(Property__c property : properties) {
            property.Name = property.CalculatedName__c;
        }
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Property__c>
    // @description : This method sets address fields based on formula fields. This allows field history tracking on the address fields.
    // @return : void
    //-----------------------------
    private void setAddressFields(List<Property__c> properties) {
        for(Property__c property : properties) {
            property.AccountingAddress__c = String.isBlank(property.CalculatedAccountingAddress__c) ? null : property.CalculatedAccountingAddress__c.replace('_BR_ENCODED_', '\n');
            property.LocationAddress__c   = String.isBlank(property.CalculatedLocationAddress__c) ? null : property.CalculatedLocationAddress__c.replace('_BR_ENCODED_', '\n');
            property.MailingAddress__c    = String.isBlank(property.CalculatedMailingAddress__c) ? null : property.CalculatedMailingAddress__c.replace('_BR_ENCODED_', '\n');
        }
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Property__c>
    // @param : Map<Id, Property__c>
    // @description : This method, sets the PropertyId for all property
    // @return : void
    //-----------------------------
    private void populateCompanyIdInProperty(List<Property__c> properties) {
        List<Property__c> propertiesToBeUpdated = new List<Property__c>();
        for(Property__c property:properties){
            if(String.isBlank(property.PropertyId__c)) propertiesToBeUpdated.add(property);
        }
        if(propertiesToBeUpdated.isEmpty()) return;
        
        // Logic changed by ASR 8056
        // ASR: 8056 : PropertyId format should start 'A50000000' onwards 
        // Label stored the sequence   - PropertyIdStartingNumber     
        String SequencePrefix = System.Label.PropertyIdStartingNumber;
        // First Two Character to search in Query if its exists in SMART 'A5'
        String StartingSeriesToSearchInQuery = SequencePrefix.subString(0,2) + '%';       
        String latestPropertySequenceNumber;
        
        
        List<Property__c> latestSequenceProperty = [SELECT PropertyId__c 
                                                    FROM Property__c 
                                                    WHERE PropertyId__c != null 
                                                    AND PropertyId__c LIKE: StartingSeriesToSearchInQuery 
                                                    ORDER BY PropertyId__c DESC limit 1];
        
        if(!latestSequenceProperty.isEmpty()){
            // if Latest Property Id exist with Sequence Prefix
            latestPropertySequenceNumber = latestSequenceProperty[0].PropertyId__c;
        }else{
            // Assigning Label as 'A50000000' sequence doesnot exist in Smart 
            latestPropertySequenceNumber = System.Label.PropertyIdStartingNumber;
        }
        
        // Setting new porperty id 
        if(String.isNotBlank(latestPropertySequenceNumber)){  
            // Sequence start should not be Blank         
            for(Property__c property : propertiesToBeUpdated){
                // getNewPropertyId will returns new Property Id for setting
                String newSequence = getNewPropertyId(latestPropertySequenceNumber); 
                // Assigned new number to PropertyId             
                property.PropertyId__c = newSequence;
                // Assigning newsequence for considering latestPropertySequenceNumber 
                // next number will be send in function : getNewPropertyId
                latestPropertySequenceNumber = newSequence;               
            }
        }        
        
    }
    //-----------------------------
    // @author : Publicis Sapient 
    // @param :  String latestPropertySequenceNumber eg- A50000000
    // @description : This method takes latest Property Id sequence and increament and return new number
    // @return : String
    // @ASR- 8056
    //-----------------------------
    public static String getNewPropertyId(String latestPropertySequenceNumber ){
        
        String  newSequence;   
        // Breaks the String by its Character type eg- A50000000 will Be [A,50000000]
        String [] SequenceAndNumbers = latestPropertySequenceNumber.splitByCharacterType();  
        // Converting 50000000 into Intger so can be inceremented by 1        
        Integer newNumber = Integer.valueof(SequenceAndNumbers[1]) + 1; 
        // Concatenate A with new integers eg -A+50000001 = A50000001     
        newSequence = SequenceAndNumbers[0]+String.valueof(newNumber);        
        return newSequence;
    }
    
}