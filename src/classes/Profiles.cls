/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class Profiles {

    public static List<Profile> ALL_PROFILES {
        get {
            if(ALL_PROFILES == null) ALL_PROFILES = getAllProfiles();
            return ALL_PROFILES;
        }
        private set;
    }

    public static Map<Id, Profile> ALL_PROFILES_BY_ID {
        get {
            if(ALL_PROFILES_BY_ID == null) {
                ALL_PROFILES_BY_ID = new Map<Id, Profile>(ALL_PROFILES);
            }
            return ALL_PROFILES_BY_ID;
        }
        private set;
    }

    public static Map<String, Profile> ALL_PROFILES_BY_NAME {
        get {
            if(ALL_PROFILES_BY_NAME == null) {
                ALL_PROFILES_BY_NAME = new Map<String, Profile>();
                for(Profile profile : ALL_PROFILES) ALL_PROFILES_BY_NAME.put(profile.Name, Profile);
            }
            return ALL_PROFILES_BY_NAME;
        }
        private set;
    }

    public static final Profile DEFAULT_EXTERNAL_USER_PROFILE = ALL_PROFILES_BY_NAME.get('Taxpayer');
    public static final Profile DEFAULT_INTERNAL_USER_PROFILE = ALL_PROFILES_BY_NAME.get(CCSFConstants.OMNICHANNEL.AUDITOR_PROFILE_NAME);

    public static final Profile ADMIN            = ALL_PROFILES_BY_NAME.get('System Administrator');
    public static final Profile AUDITOR          = ALL_PROFILES_BY_NAME.get(CCSFConstants.OMNICHANNEL.AUDITOR_PROFILE_NAME);
    public static final Profile EXEMPTIONs_STAFF = ALL_PROFILES_BY_NAME.get('Exemptions Staff');
    public static final Profile TAXPAYER         = ALL_PROFILES_BY_NAME.get('Taxpayer');

    private static List<Profile> getAllProfiles() {
        return [
            SELECT Id, Name, UserLicenseId, UserLicense.Name, UserType
            FROM Profile
            ORDER BY Name
        ];
    }

}