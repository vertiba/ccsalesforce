/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class BusinessTypeMappingUtility {

    public static Map<String, BusinessTypeMapping__c> getBusinessTypeAssetMappingsByCompositeKey(String businessTypeAssetMappingCompositeKey) {
        return getBusinessTypeAssetMappingsByCompositeKey(new List<String>{businessTypeAssetMappingCompositeKey});
    }

    public static Map<String, BusinessTypeMapping__c> getBusinessTypeAssetMappingsByCompositeKey(List<String> businessTypeAssetMappingCompositeKeys) {
        Map<String, BusinessTypeMapping__c> businessTypeAssetMappingsByCompositeKey = new Map<String, BusinessTypeMapping__c>();

        for(BusinessTypeMapping__c businessTypeMapping : [SELECT Id, CalculatedCompositeKey__c, BusinessType__c, BusinessSubtype__c, AssetClassification__c, DefaultAssetSubclassification__c
            FROM BusinessTypeMapping__c
            WHERE CalculatedCompositeKey__c IN :businessTypeAssetMappingCompositeKeys
        ]) {
            businessTypeAssetMappingsByCompositeKey.put(businessTypeMapping.CalculatedCompositeKey__c, businessTypeMapping);
        }

        return businessTypeAssetMappingsByCompositeKey;
    }

}