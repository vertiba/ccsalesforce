/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##
//
//=======================================================================================================================

/******************************************
 *       ASSESSMENTLINEITEMHANDLER        *
 * HANDLER FOR ASSESSMENTLINEITEM TRIGGER *
 ******************************************/
public without sharing class AssessmentLineItemHandler extends TriggerHandler {

    protected override void executeBeforeInsert(List<SObject> newRecords) {
        List<AssessmentLineItem__c> newAssessmentLineItems = (List<AssessmentLineItem__c>)newRecords;

        /* Ignore processing for any legacy records */
        List<AssessmentLineItem__c> assessmentLineItemsToProcess = new List<AssessmentLineItem__c>();
        for(AssessmentLineItem__c assessmentLineItem : newAssessmentLineItems) {
            if(!assessmentLineItem.IsLegacy__c) assessmentLineItemsToProcess.add(assessmentLineItem);
        }

        this.setAssessmentYear(assessmentLineItemsToProcess);
        //As part of ASR-10611, moved Process Builder logic here
        this.propertyCategoryMapping(assessmentLineItemsToProcess);
        this.setBusinessTypeMappingDefaults(assessmentLineItemsToProcess, new Map<Id, String>());
        this.setBusinessAssetMappingDefaults(assessmentLineItemsToProcess);
        this.setDepreciationFactor(assessmentLineItemsToProcess);
    }

    protected override void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {
        List<AssessmentLineItem__c> updatedAssessmentLineItems    = (List<AssessmentLineItem__c>)updatedRecords;
        Map<Id, AssessmentLineItem__c> oldAssessmentLineItemsById = (Map<Id, AssessmentLineItem__c>)oldRecordsById;

        /* Ignore processing for any legacy records */
        List<AssessmentLineItem__c> assessmentLineItemsToProcess = new List<AssessmentLineItem__c>();

         /* Ignore setting of Depriciation Factor for Leasehold Improvements - Fixtures / General, Machinery & Equipment / General, Other Equipment / General as per ASR-8529*/
         List<AssessmentLineItem__c> assessmentLineItemsForDFCalculation = new List<AssessmentLineItem__c>();

        for(AssessmentLineItem__c assessmentLineItem : updatedAssessmentLineItems) {
            if(!assessmentLineItem.IsLegacy__c) {
                assessmentLineItemsToProcess.add(assessmentLineItem);
                if(!depreciationDataChangedManually(assessmentLineItem,(AssessmentLineItem__c)oldRecordsById.get(assessmentLineItem.Id))) {
                    assessmentLineItemsForDFCalculation.add(assessmentLineItem);
                }
            }
        }


        this.setAssessmentYear(assessmentLineItemsToProcess);
        //As part of ASR-10611, moved Process Builder logic here
        this.propertyCategoryMapping(assessmentLineItemsToProcess);
        this.setBusinessTypeMappingDefaults(assessmentLineItemsToProcess, oldAssessmentLineItemsById);
        this.setBusinessAssetMappingDefaults(assessmentLineItemsToProcess, oldAssessmentLineItemsById);
        this.setDepreciationFactor(assessmentLineItemsForDFCalculation, oldAssessmentLineItemsById);
    }

    protected override void executeAfterInsert(List<SObject> newRecords, Map<Id, SObject> newRecordsById) {
        List<AssessmentLineItem__c> newAssessmentLineItems = (List<AssessmentLineItem__c>)newRecords;
        List<AssessmentLineItem__c> assessmentLineItemsToProcess = new List<AssessmentLineItem__c>();
        for(AssessmentLineItem__c assessmentLineItem : newAssessmentLineItems) {
            if(!assessmentLineItem.IsLegacy__c) assessmentLineItemsToProcess.add(assessmentLineItem);
        }
        this.updateParentEscapeAssessmentsOnTotalAdjustedCost(assessmentLineItemsToProcess,null,false);
    }

    protected override void executeAfterUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {

        List<AssessmentLineItem__c> updatedAssessmentLineItems    = (List<AssessmentLineItem__c>)updatedRecords;

        List<AssessmentLineItem__c> assessmentLineItemsToProcess = new List<AssessmentLineItem__c>();
        for(AssessmentLineItem__c assessmentLineItem : updatedAssessmentLineItems) {
            if(!assessmentLineItem.IsLegacy__c) assessmentLineItemsToProcess.add(assessmentLineItem);
        }
        this.updateParentEscapeAssessmentsOnTotalAdjustedCost(assessmentLineItemsToProcess, oldRecordsById, true);
    }

    protected override void executeAfterDelete(List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {

        List<AssessmentLineItem__c> deletedRecords  = (List<AssessmentLineItem__c>)oldRecords;

		this.preventToDeleteLockedLineItems(deletedRecords);
        List<AssessmentLineItem__c> assessmentLineItemsToProcess = new List<AssessmentLineItem__c>();
        for(AssessmentLineItem__c assessmentLineItem : deletedRecords) {
            if(!assessmentLineItem.IsLegacy__c) assessmentLineItemsToProcess.add(assessmentLineItem); 
        }
        this.updateParentEscapeAssessmentsOnTotalAdjustedCost(assessmentLineItemsToProcess, oldRecordsById, false);
        
    }

    private void setAssessmentYear(List<AssessmentLineItem__c> assessmentLineItemsToProcess) {
        for(AssessmentLineItem__c assessmentLineItem : assessmentLineItemsToProcess){
            assessmentLineItem.AssessmentYear__c = assessmentLineItem.CaseAssessmentYear__c;
        }
    }

    private void setBusinessTypeMappingDefaults(List<AssessmentLineItem__c> updatedAssessmentLineItems, Map<Id, AssessmentLineItem__c> oldAssessmentLineItemsById) {
        List<AssessmentLineItem__c> assessmentLineItemsToProcess = new List<AssessmentLineItem__c>();
        Map<Id, String> newAssetSubclassificationByLineItemId = new Map<Id, String>();
        for(AssessmentLineItem__c assessmentLineItem : updatedAssessmentLineItems) {
            AssessmentLineItem__c oldAssessmentLineItem = oldAssessmentLineItemsById.get(assessmentLineItem.Id);

            // Check if any key fields related to asset classification have changed
            Boolean keyFieldChanged = false;
            if(assessmentLineItem.Subcategory__c != oldAssessmentLineItem.Subcategory__c) {
                keyFieldChanged = true;
                if(assessmentLineItem.AssetSubclassification__c != null || assessmentLineItem.AssetSubclassification__c == CCSFConstants.ASSESSMENTLINEITEMS.DEFAULT_SUBCLASSIFICATION) {
                    newAssetSubclassificationByLineItemId.put(assessmentLineItem.Id, assessmentLineItem.AssetSubclassification__c);
                }
            }
            // On the Insert scenario we want to apply the default Business type mapping whin the case when the Default value "General" is selected in asset sabcassification. 
            if(assessmentLineItem.AssetSubclassification__c == null) keyFieldChanged = true;
            // If a user changed the classification (subcategory) and subclassification, then we need to keep their chosen value
            if(assessmentLineItem.AssetSubclassification__c != null && assessmentLineItem.AssetSubclassification__c != oldAssessmentLineItem.AssetSubclassification__c) {
                keyFieldChanged = true;
                newAssetSubclassificationByLineItemId.put(assessmentLineItem.Id, assessmentLineItem.AssetSubclassification__c);
            }

            if(keyFieldChanged) assessmentLineItemsToProcess.add(assessmentLineItem);
        }

        this.setBusinessTypeMappingDefaults(assessmentLineItemsToProcess, newAssetSubclassificationByLineItemId);
    }

    private void setBusinessTypeMappingDefaults(List<AssessmentLineItem__c> assessmentLineItemsToProcess, Map<Id, String> newAssetSubclassificationByLineItemId) {
        // Build a list of composite keys for the line item's business type, subtype and asset classification
        List<String> businessTypeAssetMappingCompositeKeys = new List<String>();
        for(AssessmentLineItem__c assessmentLineItem : assessmentLineItemsToProcess) {
            businessTypeAssetMappingCompositeKeys.add(assessmentLineItem.BusinessTypeAssetCompositeKey__c);
        }

        // Get the matching business type mappings & apply the default subclassification
        Map<String, BusinessTypeMapping__c> businessTypeAssetMappingsByCompositeKeys = BusinessTypeMappingUtility.getBusinessTypeAssetMappingsByCompositeKey(businessTypeAssetMappingCompositeKeys);
        for(AssessmentLineItem__c assessmentLineItem : assessmentLineItemsToProcess) {
            BusinessTypeMapping__c mapping = businessTypeAssetMappingsByCompositeKeys.get(assessmentLineItem.BusinessTypeAssetCompositeKey__c);

            if(mapping == null) mapping = new BusinessTypeMapping__c();

            // If a user changed the classification (subcategory) and subclassification, then we need to keep their chosen value
            Boolean subclassificationSetOnInsert = assessmentLineItem.Id == null && assessmentLineItem.AssetSubclassification__c != CCSFConstants.ASSESSMENTLINEITEMS.DEFAULT_SUBCLASSIFICATION;
            if(newAssetSubclassificationByLineItemId != null && newAssetSubclassificationByLineItemId.containsKey(assessmentLineItem.Id)) {
                assessmentLineItem.AssetSubclassification__c = newAssetSubclassificationByLineItemId.get(assessmentLineItem.Id);
            } else if(subclassificationSetOnInsert == false && mapping.DefaultAssetSubclassification__c != null) {
                assessmentLineItem.AssetSubclassification__c = mapping.DefaultAssetSubclassification__c;
            }
        }
    }

    private void setBusinessAssetMappingDefaults(List<AssessmentLineItem__c> updatedAssessmentLineItems, Map<Id, AssessmentLineItem__c> oldAssessmentLineItemsById) {
        List<AssessmentLineItem__c> assessmentLineItemsToProcess = new List<AssessmentLineItem__c>();
        for(AssessmentLineItem__c assessmentLineItem : updatedAssessmentLineItems) {
            AssessmentLineItem__c oldAssessmentLineItem = oldAssessmentLineItemsById.get(assessmentLineItem.Id);

            // Check if any key fields related to asset classification have changed
            Boolean keyFieldChanged = false;
            if(assessmentLineItem.BusinessAssetMapping__c == null) keyFieldChanged = true;
            if(assessmentLineItem.AssetSubclassification__c == null) keyFieldChanged = true;
            if(assessmentLineItem.Subcategory__c != oldAssessmentLineItem.Subcategory__c) keyFieldChanged = true;
            if(assessmentLineItem.AssetSubclassification__c != oldAssessmentLineItem.AssetSubclassification__c) keyFieldChanged = true;
			if(!exclusiveClassficationCombinationsCheck(assessmentLineItem) && checkUsefulLifeChanges(assessmentLineItem,oldAssessmentLineItem) ) keyFieldChanged = true;

            if(keyFieldChanged) assessmentLineItemsToProcess.add(assessmentLineItem);
        }

        this.setBusinessAssetMappingDefaults(assessmentLineItemsToProcess);
    }

    private void setBusinessAssetMappingDefaults(List<AssessmentLineItem__c> assessmentLineItemsToProcess) {
        // Build a set of depreciation factor & business asset mapping composite keys
        List<String> businessAssetMappingCompositeKeys = new List<String>();

        for(AssessmentLineItem__c assessmentLineItem : assessmentLineItemsToProcess) {
            String subclassification = assessmentLineItem.AssetSubclassification__c == null ? CCSFConstants.ASSESSMENTLINEITEMS.DEFAULT_SUBCLASSIFICATION : assessmentLineItem.AssetSubclassification__c;
            String businessAssetCompositeKey = Label.DepreciationFactor+'-'+assessmentLineItem.Subcategory__c + '-' + subclassification;
            businessAssetMappingCompositeKeys.add(businessAssetCompositeKey);
        }

        // Query for the matching records
        Map<String, BusinessAssetMapping__c> businessAssetMappingsByCompositeKeys = FactorUtility.getBusinessAssetMappingsByCompositeKeys(businessAssetMappingCompositeKeys, Label.DepreciationFactor);

        // Apply business asset mapping & depreciation factor
        for(AssessmentLineItem__c assessmentLineItem : assessmentLineItemsToProcess) {
            String subclassification = assessmentLineItem.AssetSubclassification__c == null ? CCSFConstants.ASSESSMENTLINEITEMS.DEFAULT_SUBCLASSIFICATION : assessmentLineItem.AssetSubclassification__c;
            String businessAssetCompositeKey = Label.DepreciationFactor+'-'+assessmentLineItem.Subcategory__c + '-' + subclassification;
            BusinessAssetMapping__c matchingBusinessAssetMapping = businessAssetMappingsByCompositeKeys.get(businessAssetCompositeKey);

            if(matchingBusinessAssetMapping != null) {
                assessmentLineItem.BusinessAssetMapping__c             = matchingBusinessAssetMapping.Id;
                assessmentLineItem.BusinessAssetMappingFactorSource__c = matchingBusinessAssetMapping.FactorSource__r.Name;
                assessmentLineItem.FixturesAllocationPercent__c        = matchingBusinessAssetMapping.FixturesAllocationPercent__c;
                assessmentLineItem.UsefulLife__c                       = matchingBusinessAssetMapping.YearsOfUsefulLife__c;
                assessmentLineItem.Subcomponent__c                     = matchingBusinessAssetMapping.Subcomponent__c;
            } else {
                assessmentLineItem.BusinessAssetMapping__c             = null;
                assessmentLineItem.BusinessAssetMappingFactorSource__c = null;
                assessmentLineItem.FixturesAllocationPercent__c        = null;
                assessmentLineItem.UsefulLife__c                       = null;
            }
        }

    }

    private void setDepreciationFactor(List<AssessmentLineItem__c> updatedAssessmentLineItems, Map<Id, AssessmentLineItem__c> oldAssessmentLineItemsById) {
        List<AssessmentLineItem__c> assessmentLineItemsToProcess = new List<AssessmentLineItem__c>();
        for(AssessmentLineItem__c assessmentLineItem : updatedAssessmentLineItems) {
            AssessmentLineItem__c oldAssessmentLineItem = oldAssessmentLineItemsById.get(assessmentLineItem.Id);

            // Check if any key fields related to depreciation factors have changed
            Boolean keyFieldChanged = false;
            if(assessmentLineItem.DepreciationFactor__c == null) keyFieldChanged = true;
            if(assessmentLineItem.AssessmentYear__c != oldAssessmentLineItem.AssessmentYear__c) keyFieldChanged = true;
            if(assessmentLineItem.AcquisitionYear__c != oldAssessmentLineItem.AcquisitionYear__c) keyFieldChanged = true;
            if(assessmentLineItem.Category__c != oldAssessmentLineItem.Category__c) keyFieldChanged = true;
            if(assessmentLineItem.Subcategory__c != oldAssessmentLineItem.Subcategory__c) keyFieldChanged = true;
            if(assessmentLineItem.AssetSubclassification__c != oldAssessmentLineItem.AssetSubclassification__c) keyFieldChanged = true;
            if(assessmentLineItem.DepreciationFactor__c != oldAssessmentLineItem.DepreciationFactor__c) keyFieldChanged = true;

            if(keyFieldChanged) assessmentLineItemsToProcess.add(assessmentLineItem);
        }

        this.setDepreciationFactor(assessmentLineItemsToProcess);
    }

    private void setDepreciationFactor(List<AssessmentLineItem__c> assessmentLineItemsToProcess) {
        // Build a set of depreciation factor & business asset mapping composite keys
        List<String> factorCompositeKeys               = new List<String>();
        List<String> fallbackFactorCompositeKeys       = new List<String>();
        List<Id> businessAssetMappingIds = new List<Id>();
        String subComponent, usefulLife;
        for(AssessmentLineItem__c assessmentLineItem : assessmentLineItemsToProcess) {
            String businessAssetCompositeKey = assessmentLineItem.Subcategory__c + '-' + assessmentLineItem.AssetSubclassification__c;
            businessAssetMappingIds.add(assessmentLineItem.BusinessAssetMapping__c);

            String factorCompositeKey = assessmentLineItem.AssessmentYear__c
                + '-' + assessmentLineItem.AcquisitionYear__c
                + '-' + assessmentLineItem.BusinessAssetMappingFactorSource__c;
            factorCompositeKeys.add(factorCompositeKey);
            factorCompositeKeys.add(assessmentLineItem.FactorCompositeKey__c);
                subComponent = assessmentLineItem.Subcomponent__c !=null ? assessmentLineItem.Subcomponent__c : '';
                usefulLife = assessmentLineItem.UsefulLife__c !=null ? String.valueOf(assessmentLineItem.UsefulLife__c): '';
            String fallBackFactorCompositeKey = assessmentLineItem.AssessmentYear__c
                                                + '-' + 'FALLBACK'
                                                + '-' + assessmentLineItem.BusinessAssetMappingFactorSource__c
                                                + '-' + subComponent
                                                    + '-' + usefulLife;
            fallbackFactorCompositeKeys.add(fallBackFactorCompositeKey);                             
            fallbackFactorCompositeKeys.add(assessmentLineItem.FallbackFactorCompositeKey__c);
        }

        // Query for the matching records
        Map<Id, BusinessAssetMapping__c> businessAssetMappingsById  = FactorUtility.getBusinessAssetMappingsById(businessAssetMappingIds);
        Map<String, List<Factor__c>> matchingFactorsByCompositeKeys = FactorUtility.getMatchingFactorsByCompositeKeys(factorCompositeKeys, fallbackFactorCompositeKeys);
        // Apply business asset mapping & depreciation factor
        for(AssessmentLineItem__c assessmentLineItem : assessmentLineItemsToProcess) {
            Boolean post2020 = false;
                        
            if(assessmentLineItem.Subcategory__c == CCSFConstants.Leasehold_Improvements_Structure && 
               assessmentLineItem.AssetSubclassification__c == CCSFConstants.Not_Assessesed_by_BPP &&
               assessmentLineItem.AcquisitionYear__c >= CCSFConstants.Post_2020_Acquisition)
            {
                post2020 = true;
            }
            
            BusinessAssetMapping__c matchingBusinessAssetMapping = businessAssetMappingsById.get(assessmentLineItem.BusinessAssetMapping__c);
            //Because of the formula not getting populated, we are populating it manually - ASR-7607
            String factorCompositeKey = assessmentLineItem.AssessmentYear__c + '-' + assessmentLineItem.AcquisitionYear__c + '-' + 
                						assessmentLineItem.BusinessAssetMappingFactorSource__c;
            subComponent = assessmentLineItem.Subcomponent__c !=null ? assessmentLineItem.Subcomponent__c : '';
            usefulLife = assessmentLineItem.UsefulLife__c !=null ? String.valueOf(assessmentLineItem.UsefulLife__c): '';
            
            String fallBackFactorCompositeKey = assessmentLineItem.AssessmentYear__c + '-' + 'FALLBACK' + '-' +
                								assessmentLineItem.BusinessAssetMappingFactorSource__c + '-' +
                								subComponent + '-' + usefulLife;
    
            List<Factor__c> matchingFactors = matchingFactorsByCompositeKeys.get(factorCompositeKey);
            matchingFactors = matchingFactors == null ? new List<Factor__c>(): matchingFactors;
            
            List<Factor__c> matchingFallbackFactors = matchingFactorsByCompositeKeys.get(fallBackFactorCompositeKey);
			Factor__c defaultFactor;
            
            // Included logic as per ASR-10027
            if(post2020){
                assessmentLineItem.Sent_to_RP__c = true;
                assessmentLineItem.DepreciationFactor__c = null;
                assessmentLineItem.DepreciationFactorPercent__c = null;
            }else{
                defaultFactor = FactorUtility.findDefaultFactor(assessmentLineItem,matchingBusinessAssetMapping, 
                                                                      matchingFactors, matchingFallbackFactors); 
                FactorUtility.applyFactor(assessmentLineItem, defaultFactor);
            }
            
        }
    
    }


    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<AssessmentLineItem__c> newAssessmentLineItemsToProcess
    // @description : This method is used to add/remove penalty on BPP Assesment records on the Condition if total ,
    //         If Business Personal Property Assessment is having a type = Escape
    //       *****   Functionality is wriiten here because we cannot track the Roll up Summary field Changes on Parent(Case) record .******
    //         Created as per JIRA Story - 3486
    // @return : void
    //-----------------------------
    private void updateParentEscapeAssessmentsOnTotalAdjustedCost(List<AssessmentLineItem__c> newAssessmentLineItemsToProcess, Map<Id, SObject> oldRecordsById, Boolean isUpdate){
    
        Map<id,Case> escapeAssessmentsById; 
        Set<Id> assessmentIds = new Set<Id>();
        List<Case> escapeAssessmentsToUpdate = new List<Case>();
        Set<Id> propertyIds = new Set<Id>();
        Set<String> assessmentYears = new Set<String>();
        AssessmentLineItem__c oldAssessmentLineItem = new AssessmentLineItem__c();
        Decimal totalAdjustedCostCap = Decimal.valueOf(System.Label.TotalAdjustedCostCap);
        set<String> validRecordKeysToProcess = new set<String>();

        for(AssessmentLineItem__c assessmentLineItem : newAssessmentLineItemsToProcess) {
            
            if(isUpdate) {
                oldAssessmentLineItem = (AssessmentLineItem__c)oldRecordsById.get(assessmentLineItem.id);
                // Checking the old values versus new values (responsible for cost change) to make sure application or removal of penalty logic is executed only on these codiitons .
                if(assessmentLineItem.SalesTaxPercentage__c != oldAssessmentLineItem.SalesTaxPercentage__c
                    || assessmentLineItem.Cost__c != oldAssessmentLineItem.Cost__c
                    || assessmentLineItem.TradeLevelFactor__c != oldAssessmentLineItem.TradeLevelFactor__c
                    || assessmentLineItem.ReverseTrendingCostRemoved__c != oldAssessmentLineItem.ReverseTrendingCostRemoved__c) {

                    assessmentIds.add(assessmentLineItem.Case__c); 

                }
            }else {
                assessmentIds.add(assessmentLineItem.Case__c); 
            }
        }

        Id bppAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Id legacyBppAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('LegacyBPPAssessment').getRecordTypeId();
        escapeAssessmentsById = new Map<id,Case> ([SELECT  id,AssessmentYear__c,Property__c,TotalAssessedCost__c,Penalty__c,ApplyPenaltyReason__c,ApplyFraudPenalty__c FROM Case WHERE ID IN : assessmentIds AND Type = 'Escape' AND RecordTypeId =:bppAssessmentRecordTypeId]);

        if(escapeAssessmentsById == null || escapeAssessmentsById.isEmpty()) {return;}  

        for(Case assessment : escapeAssessmentsById.values()) {
            
            propertyIds.add(assessment.property__c);
            assessmentYears.add(assessment.AssessmentYear__c);
        }

            
        // This Set contains the keys which do not have any other assessments for the escape Assessment we are applying the penalty - Done as Per ASR 7813
        // Key  = Property Id  + '-' + Assessment Year
         validRecordKeysToProcess = getRecordsKeysForSingleBPPAssessmentForAYear(propertyIds,assessmentYears,bppAssessmentRecordTypeId,legacyBppAssessmentRecordTypeId);

           
        Map<String,Decimal> totalAdjustedCostByAssessmentId = new Map<String,Decimal> ();

        Penalty__c penalty = [SELECT id, PenaltyMaxAmount__c, Percent__c 
                                FROM Penalty__c 
                                WHERE RTCode__c =: System.Label.Late_Filer_Penalty_R_T_Code];
        
        List<AggregateResult> aggregateResults = [SELECT case__c, sum(AdjustedCost__c) totalAdjustedCost
                                                                    FROM AssessmentLineItem__c 
                                                                    WHERE case__c IN :escapeAssessmentsById.keyset() 
                                                                    GROUP BY case__c];
        
        for(AggregateResult aggregateResult : aggregateResults) {

            totalAdjustedCostByAssessmentId.put((String)aggregateResult.get('case__c'),(Decimal)aggregateResult.get('totalAdjustedCost') );

        }

        for(Case assessment : escapeAssessmentsById.values()) {

            Decimal totalAjustedCost = totalAdjustedCostByAssessmentId.get(assessment.id);
            Boolean penaltyOverride = false;
                if(assessment.ApplyPenaltyReason__c != CCSFConstants.ASSESSMENT.SYSTEM_GENERATED 
                        && !String.IsBlank(assessment.ApplyPenaltyReason__c)){
                        penaltyOverride = true;
                }                
            if(totalAjustedCost != null && totalAjustedCost > totalAdjustedCostCap &&  validRecordKeysToProcess.contains(assessment.property__c + '-'+assessment.AssessmentYear__c)
                && !assessment.ApplyFraudPenalty__c && assessment.Penalty__c == null && !penaltyOverride) { // Changed as part of 7916
                assessment.Penalty__c = penalty.Id;
                assessment.PenaltyMaxAmount__c = penalty.PenaltyMaxAmount__c;
                assessment.PenaltyPercent__c = penalty.Percent__c;
                assessment.ApplyPenaltyReason__c = CCSFConstants.ASSESSMENT.SYSTEM_GENERATED  ; // added ad part of  ASR-7916 
                escapeAssessmentsToUpdate.add(assessment);
            }else if(totalAjustedCost < totalAdjustedCostCap && assessment.penalty__c == penalty.Id   && !penaltyOverride) {// added as part of 7916
                assessment.penalty__c = null;
                assessment.PenaltyMaxAmount__c = null;
                assessment.PenaltyPercent__c = null;
                assessment.ApplyPenaltyReason__c =null ; // added ad part of  ASR-7916 
                //Though case is newly created, by the time case is updated here validation rule will trigger, so 
                //adding this line, 
                assessment.WaivePenaltyReason__c = CCSFConstants.ASSESSMENT.SYSTEM_REMOVED;
                escapeAssessmentsToUpdate.add(assessment);
            }
        }
        
        if(!escapeAssessmentsToUpdate.isEmpty()) {
            update escapeAssessmentsToUpdate;
        }
        
    }

     //-----------------------------
    // @author : Publicis.Sapient
    // @param : set<Id> propertyIds,Set<String> assessmentYears, Id bppAssessmentRecordTypeId, Id legacyBppAssessmentRecordTypeId
    // @description : This method is used to check whether multiple assessments are there for the Assessment year of the Record which is to process for penalty
    //                  This method is created as Part of ASR 7813 Point number 3 where we need to check if are any other assessmnets for the year for which Escape is created/updated.
    // @return : set<String> 
    //-----------------------------

    public static Set<String> getRecordsKeysForSingleBPPAssessmentForAYear(set<Id> propertyIds,Set<String> assessmentYears, Id bppAssessmentRecordTypeId, Id legacyBppAssessmentRecordTypeId) {

        //Changes done for Bug Penalty should not apply automatically to reissued case created from cancel and reissue 
        Set<String> recordKeysWithSingleBPPAssessmentForYear = new Set<String>();
        Set<String> uniqueKeys = new Set<String>();
        
        for(Case assessment :  [SELECT id,AssessmentYear__c,Property__c  FROM Case WHERE property__c IN :propertyIds 
                                AND AssessmentYear__c IN : assessmentYears 
                                AND (RecordTypeId =:bppAssessmentRecordTypeId OR RecordTypeId =:legacyBppAssessmentRecordTypeId)]) 
        {

            if(recordKeysWithSingleBPPAssessmentForYear.contains(assessment.property__c+'-'+assessment.AssessmentYear__c)) {
                uniqueKeys.add(assessment.property__c+'-'+assessment.AssessmentYear__c);
            }else {
                recordKeysWithSingleBPPAssessmentForYear.add(assessment.property__c+'-'+assessment.AssessmentYear__c);
            }                                                       
        }
        if(!recordKeysWithSingleBPPAssessmentForYear.isEmpty()){
            for(String str : recordKeysWithSingleBPPAssessmentForYear) {
                if(uniqueKeys.contains(str)) {
                   recordKeysWithSingleBPPAssessmentForYear.remove(str); 
                }
            }
        }
        return recordKeysWithSingleBPPAssessmentForYear;
    }
	
    /*-----------------------------
     @author : Publicis.Sapient
     @param : AssessmentLineItem__c -- newAssessmentLineItem,oldAssessmentLineItem
     @description : This method is used check if the Depreciation factor Data is changed manually for the Specific Asset Classifications
     				  mentioned in the story ASR 8529
     @return : Boolean
     -----------------------------*/
    private Boolean depreciationDataChangedManually(AssessmentLineItem__c newAssessmentLineItem, AssessmentLineItem__c oldAssessmentLineItem) {

        Boolean byPassDepreciationFactorOverride = false;
        if ( newAssessmentLineItem != null && oldAssessmentLineItem != null 
            && exclusiveClassficationCombinationsCheck(newAssessmentLineItem)
            && (newAssessmentLineItem.DepreciationFactor__c != oldAssessmentLineItem.DepreciationFactor__c
                || checkUsefulLifeChanges(newAssessmentLineItem,oldAssessmentLineItem))
        ) {
            
            byPassDepreciationFactorOverride =true;
            Factor__c selectedFactor = [SELECT Id, FactorPercent__c,YearsOfUsefulLife__c
            							FROM Factor__c
                                        WHERE id =: newAssessmentLineItem.DepreciationFactor__c];
            
            newAssessmentLineItem.DepreciationFactorPercent__c = selectedFactor.FactorPercent__c;
            
            // Update the UsefulLife as per the new factor Table if there is no explicit change in it.
            newAssessmentLineItem.UsefulLife__c = newAssessmentLineItem.UsefulLife__c == oldAssessmentLineItem.UsefulLife__c ? selectedFactor.YearsOfUsefulLife__c : newAssessmentLineItem.UsefulLife__c;   
		
        }
        return byPassDepreciationFactorOverride; 
    }
    
     /*-----------------------------
     @author : Publicis.Sapient
     @param : AssessmentLineItem__c -- newAssessmentLineItem
     @description : This method is used check if the updated record has the combination of Specific Asset Classification & sub-asset classification
     				as mentioned in the story ASR 8529
     @return : Boolean
     -----------------------------*/
    
    private Boolean exclusiveClassficationCombinationsCheck (AssessmentLineItem__c newAssessmentLineItem) {
        
        Boolean allowChangeClassificationsCheck = false;
        if(newAssessmentLineItem !=null && CCSFConstants.ASSESSMENTLINEITEMS.SUBCATEGORY_SET.contains(newAssessmentLineItem.Subcategory__c)
           && newAssessmentLineItem.AssetSubclassification__c == CCSFConstants.ASSESSMENTLINEITEMS.DEFAULT_SUBCLASSIFICATION) {
             allowChangeClassificationsCheck = true;  
           }
        return allowChangeClassificationsCheck;
    }
    
     /*-----------------------------
     @author : Publicis.Sapient
     @param : AssessmentLineItem__c -- newAssessmentLineItem,oldAssessmentLineItem
     @description : This method is used check if there is a manual Change in Useful life and Fixtures Allocation percentage in the updated record
     				  Created as part of the story ASR 8529
     @return : Boolean
     -----------------------------*/
    private Boolean checkUsefulLifeChanges(AssessmentLineItem__c newAssessmentLineItem, AssessmentLineItem__c oldAssessmentLineItem){
        
        Boolean isUsefulLifeChange = false;
            if(newAssessmentLineItem.UsefulLife__c != oldAssessmentLineItem.UsefulLife__c
               || newAssessmentLineItem.FixturesAllocationPercent__c != oldAssessmentLineItem.FixturesAllocationPercent__c) {
                       
                   isUsefulLifeChange =true;
               }
        return isUsefulLifeChange;
    }
    /*-----------------------------
     @author : Publicis.Sapient
     @param : AssessmentLineItem__c -- List<AssessmentLineItem__c>
     @description :This is set property Category on AssessmentLine Item
     -----------------------------*/
     private void propertyCategoryMapping(List<AssessmentLineItem__c> updatedAssessmentLineItems){
         List<PropertyCategoryMapping__mdt> propertyCategoryMdt = Database.query('SELECT ID,SubCategory__c,PropertyCategory__c FROM PropertyCategoryMapping__mdt');
         Map<String, PropertyCategoryMapping__mdt> subCategoryWithPropertyCategory = new Map<String, PropertyCategoryMapping__mdt>();
         
         if(!propertyCategoryMdt.isEmpty()){
             for(PropertyCategoryMapping__mdt asset : propertyCategoryMdt){
                 subCategoryWithPropertyCategory.put(asset.SubCategory__c,asset);
             }
         }
         
         for(AssessmentLineItem__c  lineItem: updatedAssessmentLineItems){
             // ASR-8596, if Category__c != Leases Equipment, set the Property Catepgory mapping.
             if(lineItem.Category__c != CCSFconstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_LEASED_EQUIPMENT){
                 if(!subCategoryWithPropertyCategory.isEmpty() && !String.isBlank(lineItem.Subcategory__c) &&
                    subCategoryWithPropertyCategory.containsKey(lineItem.Subcategory__c)){
                        lineItem.Category__c = subCategoryWithPropertyCategory.get(lineItem.Subcategory__c).PropertyCategory__c;
                    }
             }
             
         }
     }
    /*-----------------------------
     @author : Publicis.Sapient
     @param : AssessmentLineItem__c -- oldAssessmentLineItem
     @description : This method is to prevent the deletion of the line items once related assessment is approved.
     @return : void
     -----------------------------*/
    public void preventToDeleteLockedLineItems(List<AssessmentLineItem__c> oldAssessmentLineItemsToProcess){
        Boolean isNonCCSFEmployee = UserUtility.isNonCCSFEmployee();
        for(AssessmentLineItem__c lineItem : oldAssessmentLineItemsToProcess){
            if(lineItem.IsLocked__c && !isNonCCSFEmployee){
                Logger.addRecordExceptionEntry(lineItem, new SaveResultException(System.Label.Locked_Record_Error_Message),null);
                lineItem.addError(System.Label.Locked_Record_Error_Message); 
            } 
        }
    }
    private class SaveResultException extends Exception {}
}