/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//                            Class to test LowValueAssessmentBulkGenerator
//-----------------------------

@isTest
public class LowValueAssessmentBulkGeneratorTest {
    
    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
    private static Integer nextYear 		= currentYear+1;
    @testSetup
    static void createTestData() {
        //Create Users
        List<User> users = new List<User>();
        User principal = TestDataUtility.getPrincipalUser();
        users.add(principal);
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin);          
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            
            //insert roll years
            List<RollYear__c> rollYears = new List<RollYear__c>();
            
            RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYearCurrentFiscalYear.IsLowValueBulkGeneratorProcessed__c = true;
            rollYearCurrentFiscalYear.IsDirectBillBulkGeneratorProcessed__c = true;            
            rollYears.add(rollYearCurrentFiscalYear);
            
            RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastYear),String.valueof(lastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastFiscalYear);
            
            RollYear__c rollYearLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastToLastYear),String.valueof(lastToLastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastToLastFiscalYear);
            
            RollYear__c rollYearNextFiscalYear = TestDataUtility.buildRollYear(String.valueof(nextYear),String.valueof(nextYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearNextFiscalYear);
            
            insert rollYears;
            
            //insert penalty
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            //insert account
            Account businessAccount = TestDataUtility.getBusinessAccount();
            businessAccount.NoticeType__c = 'Low Value';
            insert businessAccount;
            
            //insert property
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;          
            
        }
    }
    @isTest
    static void cloneCasesWithLineItemsTest1() {
        Integer currentYear = FiscalYearUtility.getCurrentFiscalYear();       
        RollYear__c[] rollYears = [select id,Name FROM RollYear__c order by Name desc]; 
        system.debug('rollYears'+rollYears);
        test.startTest();
        Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME)];
        //insert assessment
        Date lastYearEventDate=  Date.newInstance(lastYear, 3, 11);           
        Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                           rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,lastYearEventDate);
        
        insert assessment;          
        //insert assessment line item
        AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,assessment.id);
        insert assessmentLineItem; 
        LowValueAssessmentBulkGenerator batchable= new LowValueAssessmentBulkGenerator();
        database.executeBatch(batchable);
        test.stopTest();
        List<Case> assessments = new List<Case>();
        List<Property__c> properties = [Select Id, name, Account__r.Name, (Select Id from Cases__r ) from Property__c];
        for( Property__c property : properties ){
            for(Case c: property.Cases__r){
                assessments.add(c);   
            }       
        }         
        system.assert(assessments.size() == 2);
        RollYear__c roll=[Select Id,IsLowValueBulkGeneratorProcessed__c from RollYear__c where year__c =:String.valueof(currentYear) and Status__c ='Roll Open'];
        system.assert(roll.IsLowValueBulkGeneratorProcessed__c);    
    }
    @isTest
    static void errorFrameworkTest(){
        Property__c bppProperty = [Select Id from Property__c];
        Id testJobId = '707S000000nKE4fIAG';
        BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
        testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testJob.JobCreatedDate__c = System.today();
        testJob.JobId__c = testJobId;
        database.insert(testJob);        
        
        BatchApexError__c testError = new BatchApexError__c();
        testError.AsyncApexJobId__c = testJobId;
        testError.BatchApexErrorLog__c = testJob.Id;
        testError.DoesExceedJobScopeMaxLength__c = false;
        testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
        testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testError.JobScope__c = bppProperty.Id;
        testError.Message__c = 'Test exception';
        testError.RequestId__c = null;
        testError.StackTrace__c = '';
        database.insert(testError);
        
        BatchableError batchError = BatchableError.newInstance(testError);
        LowValueAssessmentBulkGenerator batchRetry= new LowValueAssessmentBulkGenerator();
        batchRetry.handleErrors(batchError);
    }
}