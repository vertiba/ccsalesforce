/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR-8088. This Batch class is used to get all the required Assessments (Cases)
//          from the current calendar years and update their fields and then update Roll year fields also.
//-----------------------------
public class ReOpenAnnualRollBatch implements Database.Batchable<sObject>,Database.Stateful, BatchableErrorHandler {

    private String originLocation;
    private String currentCalendarYear;
    private boolean processSuccess;
    
    public ReOpenAnnualRollBatch() {
        originLocation = CCSFConstants.REOPEN_ANNUAL_ROLL_BATCH_NAME;
        currentCalendarYear = String.valueOf(system.today().year());
        processSuccess = true;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method gets all Assessment (Case) records which follow the below criteria.
    // @return : Database.QueryLocator (List of Cases)
    public Database.QueryLocator start(Database.BatchableContext bc) {
		
        //Build dynamic query for batch
        String query='SELECT IntegrationStatus__c, TriggerBatchId__c';
        query += ' from Case where IsClosed = true and isAssessment__c = true';
        query += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';
        query += ' and AdjustmentType__c = \''+CCSFConstants.ASSESSMENT.STATUS_NEW+'\'';
        query += ' and RollYear__c = :currentCalendarYear';
        Logger.addDebugEntry(query+'- start method query',originLocation);
        Logger.saveLog();
		return database.getQueryLocator(query);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : CasesList (List of Cases)
    // @description : This method update fields for all the Assessment (Case) records which
    //          are returned by start() method of the Batch class 
    // @return : void
    public void execute(Database.BatchableContext bc, List<Case> cases) {
        
        try {
                //update assessments
                processSuccess = updateAssessments(cases);
                
                Logger.addDebugEntry(String.valueof(processSuccess),'processSuccess'+originLocation);
           }
            catch(Exception ex) {
            	processSuccess = false;
                Logger.addExceptionEntry(ex, originLocation);
                throw ex;
            }
        Logger.saveLog();
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method updates the roll year fields record that the Reopen Annual batch has been completed.
    // @return : void
    public void finish(Database.BatchableContext bc) {

        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        DescribeUtility.createApexBatchLog(JobId, AnnualRollBatchSync.class.getName(), asyncList[0].ParentJobId, false, '', asyncList[0].NumberOfErrors);
        
        RollYear__c rollYear = getRollYearDetails(currentCalendarYear);

        //Update the roll Year values after successful batch
        //if the batch is successful and there are no failed records.
        if(processSuccess && rollYear !=null) {
            rollYear.IntegrationStatus__c = '';
            rollYear.FailureReason__c='';
            rollYear.IsLocked__c = false;
            rollYear.TriggerBatchId__c='';
            rollYear.Status__c='Roll Open';
            update rollYear;
        }
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method update assessments
    // @return : return boolean value based on the success/fail result
    //-----------------------------
    private Boolean updateAssessments(List<Case> cases) {
        
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        for(Case inputCase : cases)
        {
            inputCase.IntegrationStatus__c='';
            inputCase.TriggerBatchId__c='';
        }
        //update assessments
        Map<String,Object> updateResultMap = DescribeUtility.updateRecords(cases,dmlOptions);
        return (Boolean)updateResultMap.get('updateSuccess');
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns Roll_Year__c object record for requested year
    // @return : roll year record
    //-----------------------------
    private static RollYear__c getRollYearDetails(String year) {
        
        RollYear__c[] rollYear = [SELECT Id,Name,Status__c,IntegrationStatus__c,FailureReason__c,TriggerBatchId__c,isLocked__c
                                  FROM RollYear__c WHERE Year__c =: year  limit 1];
        //check query result and return first returned result's id
        if(rollYear.size()>0)
        {
            return rollYear[0];
        }
		return null;
    }

}