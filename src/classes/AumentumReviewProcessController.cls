/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR 2366 - TASK ASR 8503
// @description : This class is used to aumentumReviewProcessor LWC controller
//-----------------------------
public with sharing class AumentumReviewProcessController {
    
    public static final String  ORIGINLOCATION= CCSFConstants.AUMENTUM_PROCESS.AUMENTUM_RECORD_REVIEW_LOCATION ;
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String
    // @description : This method takes String StagingAumentumBusiness Record Id and return data table to display in component
    // @return : List<ResultWrapper>
    //-----------------------------
    @AuraEnabled(cacheable=true)
    public static List<ResultWrapper> getChildTableValues(String parentRecordId){
        List<StagingReviewRecordProcessor__mdt> stagingCompareFieldsMapping =[SELECT DeveloperName,ExistingValue__c,FieldLabel__c,Id,IntegrationValue__c,Label,ObjectName__c,FieldAPIForUpdate__c,FieldAPIValueFrom__c 
                                                                              FROM StagingReviewRecordProcessor__mdt];
        List<String> fieldsToBeQuery= new List<String>();       
        List<ResultWrapper> childRecordDetails= new List<ResultWrapper>();
        
        for(StagingReviewRecordProcessor__mdt record: stagingCompareFieldsMapping){
            fieldsToBeQuery.add(record.ExistingValue__c);
            fieldsToBeQuery.add(record.IntegrationValue__c);
        }
        String fieldsQuery =String.join(fieldsToBeQuery,',');     
        String query='SELECT ID, Name, ParentStagingAumentumBusiness__c,MappedBusiness__c,MappedProperty__c,'+ fieldsQuery + ' from StagingBusinessCompareField__c where ParentStagingAumentumBusiness__c =: parentRecordId';
        List<StagingBusinessCompareField__c> childRecords=Database.query(query);    
        If(childRecords.isEmpty()){return null;}  
        for(StagingBusinessCompareField__c record:childRecords){   
            
            for(StagingReviewRecordProcessor__mdt metaValue:stagingCompareFieldsMapping){
                ResultWrapper result = new ResultWrapper();
                result.fieldLabel=metaValue.FieldLabel__c;       
                if(metavalue.ExistingValue__c.contains('.')){
                    result.existingValue = String.valueOf(record.getSobject(metavalue.ExistingValue__c.subStringBefore('.')).get(metavalue.ExistingValue__c.subStringAfter('.')));
                }else{
                    result.existingValue=String.valueof(record.get(metaValue.ExistingValue__c));
                }
                if(metavalue.IntegrationValue__c.contains('.')){
                    result.integrationValue = String.valueOf(record.getSobject(metavalue.IntegrationValue__c.subStringBefore('.')).get(metavalue.IntegrationValue__c.subStringAfter('.')));               
                }else{
                    result.integrationValue=String.valueof(record.get(metaValue.IntegrationValue__c));
                }        
                result.objectName=metaValue.ObjectName__c;             
                result.recordId=record.Id; 
                result.mappedAccountId= record.MappedBusiness__c;
                result.mappedPropertyId= record.MappedProperty__c;
                result.fieldAPINameForUpdate=metaValue.FieldAPIForUpdate__c;
                result.fieldAPINameOfStage=metaValue.FieldAPIValueFrom__c;
                result.parentId=record.ParentStagingAumentumBusiness__c;
                childRecordDetails.add(result);
            }
        }
        
        return childRecordDetails;
    }
    // Wrapper for datatable to display in UI
    public class ResultWrapper{
        @AuraEnabled
        public String recordId{get;set;}
        @AuraEnabled
        public String objectName{get;set;}
        @AuraEnabled
        public String fieldLabel{get;set;}
        @AuraEnabled
        public String existingValue{get;set;}
        @AuraEnabled       
        public String integrationValue{get;set;}
        @AuraEnabled
        public Id mappedAccountId{get;set;}  
        @AuraEnabled
        public Id mappedPropertyId{get;set;} 
        @AuraEnabled
        public Id parentId{get;set;}   
        @AuraEnabled
        public String fieldAPINameForUpdate {get;set;}
        @AuraEnabled
        public String fieldAPINameOfStage {get;set;}
        
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String
    // @description : This method takes JSON string which contains new changes and update Account,Property and StagingAumentumBusiness
    // @return : none
    //-----------------------------
    @AuraEnabled
    public static void updateNewChanges(String newChanges){
        
        List<object> recordsForChanges= (List<object>)JSON.deserializeUntyped(newChanges); 
        Map<String,Object> metaFieldsDetails = new Map<String,Object>();
        Map<Id,Account> accountsToUpdateById = new Map<Id,Account>();
        Map<Id,Property__c> propertiesToUpdateById = new Map<Id,Property__c>();
        Map<Id,StagingAumentumBusiness__c> aumentumRecordsToUpdateById = new Map<Id,StagingAumentumBusiness__c>();
        try{
            if(!recordsForChanges.isEmpty()){
                for(Object record: recordsForChanges){
                    metaFieldsDetails = (Map<string,Object>)record;    
                    if(metaFieldsDetails.get('ObjectName') =='Account'){
                        // If changes are for Account
                        Account newAccount = new Account();
                        if(!accountsToUpdateById.containsKey(String.valueof(metaFieldsDetails.get('AccountId')))){
                            newAccount.Id= String.valueof(metaFieldsDetails.get('AccountId'));
                            newAccount=(Account)getFields(String.valueof(metaFieldsDetails.get('FieldAPI')),String.valueOf(metaFieldsDetails.get('NewValue')),(sobject)newAccount,String.valueof(metaFieldsDetails.get('ObjectName')));                    
                            accountsToUpdateById.put(newAccount.Id, newAccount);      
                        }else{
                            newAccount= accountsToUpdateById.get(String.valueof(metaFieldsDetails.get('AccountId')));
                            newAccount=(Account)getFields(String.valueof(metaFieldsDetails.get('FieldAPI')),String.valueOf(metaFieldsDetails.get('NewValue')),(sobject)newAccount,String.valueof(metaFieldsDetails.get('ObjectName')));                    
                            accountsToUpdateById.put(newAccount.Id, newAccount);
                        }
                        
                    } else{
                        Property__c newProperty = new Property__c();
                        if(!propertiesToUpdateById.containsKey(String.valueof(metaFieldsDetails.get('PropertyId')))){
                            newProperty.Id= String.valueof(metaFieldsDetails.get('PropertyId'));
                            newProperty=(Property__c)getFields(String.valueof(metaFieldsDetails.get('FieldAPI')),String.valueOf(metaFieldsDetails.get('NewValue')),(sobject)newProperty,String.valueof(metaFieldsDetails.get('ObjectName')));                    
                            propertiesToUpdateById.put(newProperty.Id,newProperty);      
                        }else{
                            newProperty= propertiesToUpdateById.get(String.valueof(metaFieldsDetails.get('PropertyId')));
                            newProperty=(Property__c)getFields(String.valueof(metaFieldsDetails.get('FieldAPI')),String.valueOf(metaFieldsDetails.get('NewValue')),(sobject)newProperty,String.valueof(metaFieldsDetails.get('ObjectName')));                    
                            propertiesToUpdateById.put(newProperty.Id,newProperty);
                        }
                    }
                    if(!aumentumRecordsToUpdateById.containsKey((String)metaFieldsDetails.get('ParentId'))){
                        StagingAumentumBusiness__c newAumentum= new StagingAumentumBusiness__c();
                        newAumentum.Id= String.valueof(metaFieldsDetails.get('ParentId'));
                        if(!String.valueof(metaFieldsDetails.get('FieldStage')).contains('FormType')){
                            // FormType doesnot exist in Staging Aumentum Record- So no update needed
                            newAumentum=(StagingAumentumBusiness__c)getFields(String.valueof(metaFieldsDetails.get('FieldStage')), String.valueOf(metaFieldsDetails.get('NewValue')),(sobject)newAumentum, 'Stage');
                        }           
                        newAumentum.put(CCSFConstants.AUMENTUM_PROCESS.AUMENTUM_STATUS_API_NAME,CCSFConstants.AUMENTUM_STATUS_PROCESSED);
                        newAumentum.put(CCSFConstants.AUMENTUM_PROCESS.IS_LOCKED_API,true);
                        aumentumRecordsToUpdateById.put(newAumentum.Id,newAumentum);
                    } 
                }
            }  
            
            List<sobject> sobjectToUpdate = new List<sobject>();
            // Assigning Account,Property and StagingAumentumRecord for Sobject to update
            if(!accountsToUpdateById.isEmpty()){
                sobjectToUpdate.addAll(accountsToUpdateById.values());
            }
            if(!propertiesToUpdateById.isEmpty()){
                sobjectToUpdate.addAll(propertiesToUpdateById.values());
            }
            if(!aumentumRecordsToUpdateById.isEmpty()){
                sobjectToUpdate.addAll(aumentumRecordsToUpdateById.values());
            } 
            // DML
            updateCurrentRecords(sobjectToUpdate);
        }catch(Exception ex){
            Logger.addExceptionEntry(ex, ORIGINLOCATION);
            Logger.saveLog();
            throw new AuraHandledException('Error while updating records, '+ex.getMessage());
        }
        
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String,String,sobject
    // @description : This method takes String fieldAPI name, value for updation and instance of sobject a
    // @return : sobject
    //-----------------------------
    public static sobject getFields(String fieldApiName, String value, sobject newsobject, String objectName){
        
        if(fieldApiName.contains('Date') && String.isNotBlank(value) && objectName !='Stage'){        
            // Field which are Date datatype
            String[] newDate=value.split('-');     
            newsobject.put(fieldApiName,date.newInstance(integer.valueof(newDate[0]), integer.valueof(newDate[1]), integer.valueof(newDate[2])));    
            if(fieldApiName.contains('Close')){
                // Making Status in-active if data is coming for Closing dates
                if(objectName == CCSFConstants.ACCOUNT.ACCOUNT_API_NAME){
                    newsobject.put(CCSFConstants.ACCOUNT.BUSINESS_STATUS_API,CCSFConstants.ACCOUNT.BUSINESS_STATUS_INACTIVE);
                }else{
                    newsobject.put(CCSFConstants.PROPERTY.STATUS_API,CCSFConstants.PROPERTY.STATUS_INACTIVE);
                    newsobject.put(CCSFConstants.PROPERTY.INACTIVE_REASON_API,CCSFConstants.PROPERTY.INACTIVE_REASON_VALUE_FOR_AUMENTUM);
                }
            }
        }else if(String.isNotBlank(value)){        
            newsobject.put(fieldApiName,value); 
        }
        return newsobject;
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : List<sObject>
    // @description : This method takes List<sobject> to update and return error if record failed
    // @return : void
    //-----------------------------
    public static void updateCurrentRecords(List<sObject> recordsToBeUpdate){        
        List<String> databaseErrors = new List<String>();
        Map<String,Object> updateResult = DescribeUtility.updateRecords(recordsToBeUpdate,null);
        List<Database.SaveResult> saveUpdateResults= (List<Database.SaveResult>)updateResult.get('updateResults');        
        for(Integer i=0;i< saveUpdateResults.size();i++){
            if (!saveUpdateResults.get(i).isSuccess()) {
                Database.Error error = saveUpdateResults.get(i).getErrors().get(0);
                databaseErrors.add(String.valueof(error));
            }
        }
        Logger.addDebugEntry(String.valueof(databaseErrors), ORIGINLOCATION);
        Logger.saveLog();
        if(databaseErrors.size() >0 ){           
            throw new AuraHandledException('Error while updating records, ' + databaseErrors);
        }        
    }
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : List<sObject>
    // @description : This method takes String Id and update the record status
    // @return : void
    //-----------------------------
    
    @AuraEnabled
    public static void ignoreChanges(String aumentumId){
        StagingAumentumBusiness__c newAumentum= new StagingAumentumBusiness__c();
        newAumentum.Id= aumentumId;
        newAumentum.put(CCSFConstants.AUMENTUM_PROCESS.AUMENTUM_STATUS_API_NAME,CCSFConstants.AUMENTUM_STATUS_PROCESSED);
        update newAumentum;
        
    }
}