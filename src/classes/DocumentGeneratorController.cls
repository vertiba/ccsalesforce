/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class DocumentGeneratorController {

    @AuraEnabled
    public static List<SObject> getAvailableTemplates(Id recordId) {
        return SmartCommUtility.getAvailableDocumentSettings(recordId);
    }
	//-----------------------------
    // @author : Publicis.Sapient
    // @param : Sobject record Id
    // @description : This method will return the boolean value based on the mailing address field value
    // @return : void
    //-----------------------------
    @AuraEnabled
    public static Boolean getMailingAddress(Id recordId) {
        Boolean isMailingAddressAvailable = false;
        Schema.SObjectType sobjectType = recordId.getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();
        //In case of Vessel Property we are populating from Calculated address and for others mailing addrss it's from account.
        if(sobjectName=='Property__c'){
            SObject objPropertyRecord = Database.query('Select Id, MailingAddress__c, RecordTypeId, Account__r.MailingAddress__c From ' + sobjectName + ' Where Id = :recordId');
            SObject objAccountRecord = objPropertyRecord.getSObject('Account__r');
            if(objPropertyRecord.get('RecordTypeId') != Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get(CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME).getRecordTypeId() && objAccountRecord.get('MailingAddress__c') != Null){
                isMailingAddressAvailable = true;
            } else if(objPropertyRecord.get('MailingAddress__c') != Null){
                isMailingAddressAvailable = true;
            }
            return isMailingAddressAvailable;
        } else {
            SObject objRecord = Database.query('Select Id, MailingAddress__c From ' + sobjectName + ' Where Id = :recordId');
            if(objRecord.get('MailingAddress__c') != Null){
                isMailingAddressAvailable = true;
            }
            return isMailingAddressAvailable;
        }
    }
    
    @AuraEnabled
    public static DocumentGenerationRequest__c generateDocument(Id recordId, Id documentTemplateId) {
        DocumentGenerationRequest__c request = SmartCommUtility.createDocumentGenerationRequest(recordId, documentTemplateId);
        request.Status__c = 'Approved';

        insert request;
        return request;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List of DocumentTemplateRequest which includes record ids and document Template Names
    // @description : When Exemption Status updated to Closed and Sub-Status is updated to Discontinued
    // (on Property Event record), then invoking the SmartCOMM Document Setting to generate the PDF notice
    // @return : void
    //-----------------------------
    @InvocableMethod(label = 'Generate Document')
    public static void generateDocument(List<DocumentTemplateRequest> DocumentTemplateRequests) {
        List<String> documentTemplateNames = new List<String>();
        for(DocumentTemplateRequest input : DocumentTemplateRequests) {
            documentTemplateNames.add(input.documentTemplateName);
        }
        Map<String, Id> documentSettingIdsByName = new Map<String, Id>();
        for(TH1__Document_Setting__c documentSetting : [SELECT Id, Name FROM TH1__Document_Setting__c WHERE Name IN :documentTemplateNames]) {
            documentSettingIdsByName.put(documentSetting.Name, documentSetting.Id);
        }

        List<DocumentGenerationRequest__c> requests = new List<DocumentGenerationRequest__c>();
        for(DocumentTemplateRequest input : DocumentTemplateRequests) {
            Id documentSettingId = documentSettingIdsByName.get(input.documentTemplateName);
            DocumentGenerationRequest__c request = SmartCommUtility.createDocumentGenerationRequest(input.recordId, documentSettingId);
            request.Status__c = 'Approved';
            requests.add(request);
        }
        insert requests;
    }

     public class DocumentTemplateRequest {

        @InvocableVariable(label = 'Record Id' required = true)
        public Id recordId;

        @InvocableVariable(label = 'Document Template Name' required = true)
        public String documentTemplateName;
  }

}