/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class RoutingUtility {
    private static Map<String, ServiceChannel> sObjectTypeToServiceChannelMap = getServiceChannelMap();
    private static Map<String, Skill> skillMap = getSkillMap();
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : workitems List of sObjects to be routed using skill-based routing
    // @description : Take a list of sObjects, create PendingServiceRouting records, create skill requirements
    //                  depending on sObject type, then mark the PSRs as 'ready for routing'
    // @return : void
    //-----------------------------
    @InvocableMethod(label='Dispatch for Skill Based Routing' description='Takes a list of SObjects and routes them through the Skill Based Routing Engine')
    public static void doSkillBasedRouting(List<Sobject> workItems){
        Map<Id, sObject> workItemMap = new Map<Id, sObject>();
        for(SObject workItem : workItems){
            workItemMap.put(workItem.Id, workItem);
        }

        List<PendingServiceRouting> existingPSRs = [SELECT Id, WorkItemId 
                                                    FROM PendingServiceRouting 
                                                    WHERE WorkItemId IN :workItemMap.keySet()];

        delete existingPSRs;

        List<PendingServiceRouting> psrs = new List<PendingServiceRouting>();
        for(Sobject workItem : workItems){
            ServiceChannel channel = sObjectTypeToServiceChannelMap.get(workItem.getSobjectType().getDescribe().getName());

            psrs.add(new PendingServiceRouting(
                CapacityWeight = CCSFConstants.OMNICHANNEL.CAPACITY,
                IsReadyForRouting = FALSE,
                RoutingModel = CCSFConstants.OMNICHANNEL.ROUTING_MODEL,
                RoutingPriority = 1,
                RoutingType = CCSFConstants.OMNICHANNEL.ROUTING_TYPE,
                ServiceChannelId = channel.Id,
                WorkItemId = workItem.Id
            ));
        }
        
        insert psrs;

        List<SkillRequirement> requiredSkills = new List<SkillRequirement>();
        for(PendingServiceRouting psr : psrs){
            Set<Id> skillIds = getSkillsForSObject(workItemMap.get(psr.WorkItemId));
            for(Id skillId : skillIds){
                if(skillId == null)continue;
                SkillRequirement srObj = new SkillRequirement(
                    RelatedRecordId = psr.id,
                    SkillId = skillId
                );
                requiredSkills.add(srObj);
            }
            psr.IsReadyForRouting = TRUE;
        }

        insert requiredSkills;
        update psrs;
    }
  
    private static Set<Id> getSkillsForSObject(SObject obj){
        Set<Id> toReturn = new Set<id>();
        String sObjectType = obj.getSObjectType().getDescribe().getName();
        
        for(AttributeSkillMapping__mdt mapping : [SELECT SkillName__c, Field__r.QualifiedAPIName, Value__c
                                                    FROM AttributeSkillMapping__mdt
                                                    WHERE Object__r.QualifiedAPIName = :sObjectType]){
            if((String)obj.get(mapping.Field__r.QualifiedAPIName) == mapping.Value__c){
                if(skillMap.containsKey(mapping.SkillName__c)){
                    toReturn.add(skillMap.get(mapping.SkillName__c).Id);
                }      
            }
        }

        return toReturn;
    }

  
    private static Map<String, ServiceChannel> getServiceChannelMap() {
        Map<String, ServiceChannel> toReturn = new Map<String, ServiceChannel>();
        
        List<ServiceChannel> channels = [SELECT Id, RelatedEntity FROM ServiceChannel];
        for(ServiceChannel channel : channels){
            toReturn.put(channel.RelatedEntity, channel);
        }
        
        return toReturn;
    }
  
    private static Map<String, Skill> getSkillMap() {
        Map<String, Skill> toReturn = new Map<String, Skill>();
        List<Skill> skills = [SELECT Id, DeveloperName
                                FROM Skill];
        
        for(Skill skillRecord : skills){
            toReturn.put(skillRecord.DeveloperName, skillRecord);
        }
        
        return toReturn;
    }
}