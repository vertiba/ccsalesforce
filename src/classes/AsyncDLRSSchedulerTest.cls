/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test AsyncDLRSScheduler
//-----------------------------
@isTest
private class AsyncDLRSSchedulerTest {

    //verify that a job is scheduled successfully in a future date. 
    @isTest
    private static void jobShouldBeScheduledToRunInFuture() {
        AsyncDlrsCalculator chainedInstance = AsyncDlrsCalculator.getInstance();
        Test.startTest();
        String jobId = System.schedule('AsyncDLRSSchedulerTest',
                                       '0 0 0 15 3 ? 2032', 
                                       new AsyncDLRSScheduler(chainedInstance)); 
        Test.stopTest();
        
        system.assertEquals(0, [SELECT count() FROM CronTrigger where id = :jobId],
                            'A job should not be scheduled, as it is aborted');
    }
}