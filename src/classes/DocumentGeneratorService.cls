/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class DocumentGeneratorService implements Database.Batchable<SObject>, Database.AllowsCallouts {

    private static final List<String> LOG_TOPICS = new List<String>{'SmartCOMM', 'DocumentGeneratorService', 'BatchApex'};

    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        final String LOG_LOCATION = 'DocumentGeneratorService.start()';

        // If there is already another instance of the job running, then abort this instance
        if(EnhancedSchedulable.getNumberOfBatchJobsForClass(this.getClassName()) > 1) {
            System.abortJob(batchableContext.getJobId());
            return null;
        }

        // If we have already exceeded the limit of allowed documents, abort job before any more processing occurs
        if(this.isProcessingLimitExceeded()) {
            Logger.addDebugEntry(LoggingLevel.DEBUG, 'Processing limit exceeded, aborting job ID=' + batchableContext.getJobId(), LOG_LOCATION, LOG_TOPICS);
            System.abortJob(batchableContext.getJobId());
        }

        return Database.getQueryLocator([
            SELECT Id, CreatedById, DocumentSetting__c, RelatedRecordId__c, Status__c
            FROM DocumentGenerationRequest__c
            WHERE Status__c = 'Approved'
            ORDER BY CreatedDate
        ]);
    }

    public void execute(Database.BatchableContext batchableContext, List<DocumentGenerationRequest__c> scope) {
        final String LOG_LOCATION = 'DocumentGeneratorService.execute()';

        // If we exceed the limit of allowed documents during a large batch job, abort job/don't process the remaining records
        if(this.isProcessingLimitExceeded()) {
            Logger.addDebugEntry(LoggingLevel.DEBUG, 'Processing limit exceeded, aborting job ID=' + batchableContext.getJobId(), LOG_LOCATION, LOG_TOPICS);
            Logger.saveLog();
            System.abortJob(batchableContext.getJobId());
        }

        try {
            Set<String> requestCombinedIds = new Set<String>();
            for(DocumentGenerationRequest__c documentRequest : scope) {
                requestCombinedIds.add(documentRequest.DocumentSetting__c + '|' + documentRequest.RelatedRecordId__c);
            }

            for(String requestCombinedId : requestCombinedIds) {
                Id documentTemplateId = requestCombinedId.split('\\|')[0];
                Id relatedRecordId    = requestCombinedId.split('\\|')[1];
                TH1.GLOBAL_API_V1.generateDocument(documentTemplateId, relatedRecordId);
            }

            for(DocumentGenerationRequest__c documentRequest : scope) {
                documentRequest.Status__c = 'Done';
            }
            update scope;
        } catch(Exception ex) {
            for(DocumentGenerationRequest__c documentRequest : scope) {
                documentRequest.ProcessingError__c = ex.getMessage();
                documentRequest.Status__c          = 'Failed';
            }
            update scope;
        }
    }

    public void finish(Database.BatchableContext batchableContext) { }

    private String getClassName() {
        return String.valueOf(this).split(':')[0];
    }

    // To avoid accidentally generating large volumes of documents...
    // the system should check how many requests have been processed, using configurable parameters
    private Boolean isProcessingLimitExceeded() {
        final String LOG_LOCATION = 'DocumentGenerationService.isProcessingLimitExceeded()';

        DocumentGeneratorServiceSettings__c settings = DocumentGeneratorServiceSettings__c.getInstance();
        Logger.addDebugEntry(LoggingLevel.DEBUG, 'Loaded DocumentGeneratorServiceSettings__c, settings=' + settings, LOG_LOCATION, LOG_TOPICS);

        Integer numberOfDays = Integer.valueOf(0 - Math.abs(settings.NumberOfDays__c));
        Datetime initialDate = System.today().addDays(numberOfDays);
        Logger.addDebugEntry(LoggingLevel.DEBUG, 'numberOfDays=' + numberOfDays, LOG_LOCATION, LOG_TOPICS);
        Logger.addDebugEntry(LoggingLevel.DEBUG, 'initialDate=' + initialDate, LOG_LOCATION, LOG_TOPICS);

        List<String> statusesToInclude = new List<String>();
        for(String status : settings.StatusesToInclude__c.split(',')) {
            statusesToInclude.add(status.trim());
        }
        Logger.addDebugEntry(LoggingLevel.DEBUG, 'statusesToInclude=' + statusesToInclude, LOG_LOCATION, LOG_TOPICS);

        Integer matchingDocumentCount = [
            SELECT COUNT()
            FROM DocumentGenerationRequest__c
            WHERE (CreatedDate >= :initialDate OR LastModifiedDate >= :initialDate)
            AND Status__c = :statusesToInclude
            ];

        Boolean isProcessingLimitExceeded = matchingDocumentCount >= settings.MaxNumberOfDocumentsAllowed__c;
        Logger.addDebugEntry(LoggingLevel.DEBUG, 'isProcessingLimitExceeded=' + isProcessingLimitExceeded, LOG_LOCATION, LOG_TOPICS);

        return isProcessingLimitExceeded;
    }

}