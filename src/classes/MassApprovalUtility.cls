/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class MassApprovalUtility {
    private static final String LOGGER_LOCATION= CCSFConstants.MASSAPPROVAL.MASS_APPROVAL_LOGGER_LOCATION;
    
    /*-----------------------------
     @author : Publicis Sapient
     @param : List<sobject> and Boolean
     @description : Approve records through Apex       
     @return : String                              
     ----------------------------- */
    public static String approvalProcess(List<sObject> recordsToBeApproved,Boolean callingFromBatch){
        Integer successCount=0;
        Integer errorCount=0;
        List<String> errorInformations= new List<String>();        
        String response;
        
        try{            
            String idkey = UserInfo.getUserId();     
            Boolean hasAccess = false;       
            List<Profile> pro = [SELECT Id, Name FROM Profile WHERE Name='System Administrator'];     
            if(UserInfo.getProfileId() == pro[0].id)hasAccess = true;   
            
            List<String> recordIds = new List<String>();  
            for(sObject record: recordsToBeApproved){    
                recordIds.add(record.Id);              
            }   
            
            List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();  
            List<ProcessInstanceWorkitem> workItems;   
            if(hasAccess)                    
                workItems = [SELECT Id, ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId IN :recordIds ];               
            else       
                workItems = [SELECT Id, ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ActorId = :idkey AND ProcessInstance.TargetObjectId IN :recordIds ];    
           
            for(ProcessInstanceWorkitem workItem : workItems){      
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();     
                req.setWorkitemId(workItem.Id);                 
                req.setAction(CCSFConstants.MASSAPPROVAL.MASS_APPROVAL_APPROVED_ACTION);                 
                req.setComments(CCSFConstants.MASSAPPROVAL.MASS_APPROVAL_COMMENTS);           
                requests.add(req);              
            }
            if(requests.isEmpty()) { return response='Error Occured'; }
            Approval.ProcessResult[] processResults = Approval.process(requests, false); 
                      
            for ( Integer x=0; x< processResults.size(); x++){
                
                if(!processResults[x].isSuccess()){
                    errorInformations.add(String.valueOf(processResults[x].getEntityId())+ String.valueOf(processResults[x].getErrors()));
                    errorCount++;
                }else{                 
                    successCount++;
                }
            }
            if(!callingFromBatch){
                response = CCSFConstants.MASSAPPROVAL.SUCCESS_COUNT + successCount +' and '+ CCSFConstants.MASSAPPROVAL.FAILURE_COUNT + errorCount;
            }
            Logger.addDebugEntry(String.valueOf(errorInformations), LOGGER_LOCATION);
            
        } catch ( Exception ex ){
            response = String.valueOf(ex.getMessage());
            Logger.addExceptionEntry(ex,LOGGER_LOCATION + response );          
        }  
        Logger.saveLog(); 
        return response;
    }
    /*-----------------------------
     @author : Publicis Sapient
     @param : List<sobject> and Boolean
     @description : Update the Status of the DocumentGenerationRequest__c  records selected. 
     @return : String                              
     ----------------------------- */
    public static String updateGenerateNotices(List<DocumentGenerationRequest__c> updateRequest,Boolean executBatch){
        String response;
        List<DocumentGenerationRequest__c> documentGenerateRequest = new List<DocumentGenerationRequest__c>();
        List<DocumentGenerationRequest__c> updateStatus 		   = new List<DocumentGenerationRequest__c>();
        List<String> recordIds = new List<String>();  
        try{
            for(DocumentGenerationRequest__c record: updateRequest){    
                recordIds.add(record.Id);              
            } 
            if(recordIds.isEmpty()){return CCSFConstants.MASSAPPROVAL.NO_RECORDS ;}
            documentGenerateRequest = [SELECT ID,Status__c 
                              			 FROM DocumentGenerationRequest__c
                             			WHERE ID IN:recordIds
                                          AND Status__c =: CCSFConstants.ASSESSMENT.STATUS_NEW];
            for(DocumentGenerationRequest__c updateRecord :documentGenerateRequest ){
                updateRecord.Status__c = CCSFConstants.MASSAPPROVAL.STATUS_APPROVED;
                updateStatus.add(updateRecord);               
            }
            UPDATE updateStatus;
             if(!executBatch){
                response = CCSFConstants.MASSAPPROVAL.SUCCESS_COUNT + updateStatus.size();
            }
        }catch(Exception e){
            response = String.valueOf(e.getMessage());
            Logger.addExceptionEntry(e,LOGGER_LOCATION + response );  
        }
        Logger.saveLog(); 
        return response;
    }

    
}