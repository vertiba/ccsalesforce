/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
public class ContentDistributionUtilitiesTest{
    
    static testMethod void testContentDistribution(){
        
        //add new document
        ContentVersion cv = new ContentVersion();
        cv.VersionData = Blob.valueOf('Pika');
        cv.Title = 'myfile';
        cv.PathOnClient = 'myfile.xml';
        INSERT cv;
        
        cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];
        
        //content workspace and content version are not allowed to insert in same apex transaction
        //but we can not separate them in our case for test class purpose
        //so we have to query and rely on existing library in salesforce org
        ContentWorkspace ws = [SELECT Id, RootContentFolderId FROM ContentWorkspace WHERE Name = 'Asset Library' LIMIT 1];
        
        //link it to library
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cv.ContentDocumentId;
        cdl.ShareType = 'I';
        cdl.Visibility = 'AllUsers';
        cdl.LinkedEntityId = ws.Id; 
        INSERT cdl;
        
        ContentDistributionUtility.getPublicUrl(new List<String>{'Asset Library/myfile'});
    }
}