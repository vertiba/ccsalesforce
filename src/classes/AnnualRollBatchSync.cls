/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR-4861.In order to finish processing annual assessments, the SMART system must send annual assessment data to the TaxSys system.
// This Batch job is to collect all the case (assessment) data and send it to integration layer.
//-----------------------------
public class AnnualRollBatchSync implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts, BatchableErrorHandler {
	private String triggerBatchID;
    private Boolean isChunkFailed;
    private String currentClosingRollYear;
    private final String originLocation ='AnnualRollBatchSync';
    public RollYear__c rollYearObject;

    public AnnualRollBatchSync(RollYear__c roll){
        this.rollYearObject = roll;
        this.currentClosingRollYear = roll.Year__c;
        this.isChunkFailed=false;
    }
    
    //Overrided constructor for replay functionality 
    public AnnualRollBatchSync(RollYear__c roll, String triggerBatchID){
        this.rollYearObject = roll;
        this.currentClosingRollYear = roll.Year__c;
        this.isChunkFailed=false;
        this.triggerBatchID = triggerBatchID;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : Database.QueryLocator
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext context) {
        List<RollYear__c> rollYears = new List<RollYear__c>();
		String query='SELECT ';
    	List<Schema.FieldSetMember> fieldList=SObjectType.Case.FieldSets.TaxSys_Integration_Fields.getFields();
     	for(Schema.FieldSetMember f : fieldList) {
        	query = query+f.getFieldPath() +','; 
        }
        if(query.contains(',Penalty__c')) {
            query = query+'Penalty__r.RTCode__c,';
        }
        if(query.contains(',ExemptionPenalty__c')) {
            query = query+'ExemptionPenalty__r.RTCode__c,';
        }
        
        query = query.substring(0, query.length()-1);
        query += ' from Case where IsClosed = true and isAssessment__c = true';
        query += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';
        query += ' AND IntegrationStatus__c = \''+CCSFConstants.READY_TO_SENT_TO_TTX+'\'';
        query += ' AND AdjustmentType__c = \''+CCSFConstants.ASSESSMENT.STATUS_NEW+'\'';
        query += ' AND SubType__c != \''+CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT+'\'';
        query += ' AND RollYear__c = :currentClosingRollYear';
        
        if(triggerBatchID==null || triggerBatchID ==''){
            //count expected record count
            String queryCount = 'SELECT COUNT() from Case where IsClosed = true and isAssessment__c = true';
            queryCount += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';
        	queryCount += ' AND IntegrationStatus__c = \''+CCSFConstants.READY_TO_SENT_TO_TTX+'\'';
        	queryCount += ' AND AdjustmentType__c = \''+CCSFConstants.ASSESSMENT.STATUS_NEW+'\'';
            queryCount += ' AND SubType__c != \''+CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT+'\'';
        	queryCount += ' AND RollYear__c = :currentClosingRollYear';
        	Integer expectedCount = Database.countQuery(queryCount);
        	triggerBatchID = makeStartcallout(currentClosingRollYear,expectedCount);
            //In case of call out failure, sending dummy empty records to execute so that it don't roll back the RollYear updates with errors
            if(triggerBatchID == null) return Database.getQueryLocator([SELECT Id FROM Case Limit 0]);
            //In case of call out success, update roll year with batch id and integration status
            updateRollYear(currentClosingRollYear, triggerBatchID, null, CCSFConstants.IN_TRANSIT_TO_TTX);            
        }
        else{
            //execute in case of replay
            query +=' and TriggerBatchId__c = :triggerBatchID';       	
        }
        
        Logger.addDebugEntry(query,originLocation+'- start method query');
        Logger.saveLog();

        return Database.getQueryLocator(query);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : List<Case>
    // @description : Get all the case and prepare a JSON. Send to Integration
    // layer and check if its success or failure.
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext context, List<Case> cases) {
        
        Map<string, object> mapToPass = new Map<string, object>();
        mapToPass.put('triggerBatchId', triggerBatchId);
        mapToPass.put('assessments', cases);
		string jsonstring = JSON.serializePretty(mapToPass);
        boolean isCalloutSuccess = makeCallout(mapToPass, triggerBatchId);
        if(!isCalloutSuccess){
            isChunkFailed = true;
        }
        Logger.addDebugEntry('Is callout for passing assessments Success ?', ''+isCalloutSuccess);
        Logger.saveLog();     
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Get roll year, triggerBatchId, record count and pass to Integration
    // layer.
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext context) {
        if(!isChunkFailed){
            boolean isCalloutSuccess = makeEndcallout(triggerBatchID, currentClosingRollYear);
            Logger.addDebugEntry('Is callout for end batch API Success ?', ''+isCalloutSuccess);
            Logger.saveLog();
        }
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'rollYearObject' => this.rollYearObject,'triggerBatchID' => this.triggerBatchID});                                
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, AnnualRollBatchSync.class.getName(), asyncList[0].ParentJobId, true, queryParams, asyncList[0].NumberOfErrors);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Case>
    // @param : String
    // @description : gets a JWT token and send the data to Java layer.
    // 		returns if callout is failed or not.
    // @return : boolean
    //-----------------------------
    private boolean makeCallout(Map<string, object> mapToPass, String triggerBatchId){
        Boolean finalResponse;
        try{
            //Can't re-use same JWT token, as generated JTI should be unique per http request
            String jwtToken = CreateJWTToken.generateJWTToken();
            Map<String,String> headers = new Map<String,String>();
            headers.put('Authorization', 'Bearer '+jwtToken);
            headers.put('Content-Type', 'application/json');
            String url = Test.isRunningTest() ? 'http://dummyClassNamePassAssessment.com' :[Select Endpoint from NamedCredential where developerName ='SMART_Job_Taxsys_Pass_Assessment_Endpoint' limit 1].Endpoint;
            Callout callout= new Callout(url).setHeaders(headers);
            HttpResponse response = callout.post(mapToPass);
            
            //update cases with Integration status and trigger batch id
            updateEligibleCases((List<Case>)mapToPass.get('assessments'), triggerBatchID, CCSFConstants.IN_TRANSIT_TO_TTX);
            finalResponse = response.getStatusCode() == 202;
        }
        catch(Exception e){
            //update Roll Year Object with Failure Reason
            updateRollYear(currentClosingRollYear, triggerBatchId, e.getMessage(), CCSFConstants.BATCHMSG.BATCH_STATUS_FAILED);
            Logger.addDebugEntry('AnnualRollBatchSync makeCallout failed', ''+e.getMessage());
            Logger.saveLog();
            finalResponse = false;
            throw e;
        }
        
        return finalResponse;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : String roll year
    // @param : Integer expected Count
    // @description : gets a JWT token and send the roll year to Java layer.
    // 		returns triggerBatchId which will be used for further processing.
    // @return : String
    //-----------------------------
    public static String makeStartcallout(String rollYear, Integer expectedCount){
        String finalResponse;
        try{
            String jwtToken = CreateJWTToken.generateJWTToken();
            Map<String,String> headers = new Map<String,String>();
            headers.put('Authorization', 'Bearer '+jwtToken);
            headers.put('Content-Type', 'application/json');
            String urlPath = Test.isRunningTest() ? 'http://dummyClassNameRollStart.com' : [Select Endpoint from NamedCredential where developerName ='SMART_Job_Taxsys_Annual_Roll_Start_Endpoint' limit 1].Endpoint;
                urlPath +=rollYear+'?expectedrecordcount='+expectedCount;
                Callout callout= new Callout(urlPath).setHeaders(headers);
                if(Test.isrunningtest()){
                    finalResponse = '111111fe-11b1-1c11-11b1-ed1111111a11';
                } else {
                     HttpResponse response = callout.get();
                     Map<String, String> values = (Map<String, String>) JSON.deserialize(response.getBody(), Map<String, String>.class);
                     finalResponse = values.get('triggerBatchId');
                }  
        }
        catch(Exception e){
            updateRollYear(rollYear, null, e.getMessage(), CCSFConstants.BATCHMSG.BATCH_STATUS_FAILED);
            Logger.addDebugEntry('AnnualRollBatchSync makeStartcallout failed', ''+e.getMessage());
            Logger.saveLog();
            finalResponse = null;
            throw e;
        }
        
        return finalResponse;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : String
    // @param : String
    // @description : gets a JWT token and send the roll year to Java layer.
    // returns if callout is failed or not.
    // @return : boolean
    //-----------------------------
    public static boolean makeEndcallout(String triggerBatchID, String rollYear){
        Boolean finalResponse;
        try{
            String jwtToken = CreateJWTToken.generateJWTToken();
            Map<String,String> headers = new Map<String,String>();
            headers.put('Authorization', 'Bearer '+jwtToken);
            headers.put('Content-Type', 'application/json');
            String urlPath = Test.isRunningTest() ? 'http://dummyClassNameRollEnd.com' :[Select Endpoint from NamedCredential where developerName ='SMART_Job_Taxsys_Annual_Roll_End_Endpoint' limit 1].Endpoint;
                urlPath +=rollYear+'?convbatchid='+triggerBatchID;
                Callout callout= new Callout(urlPath).setHeaders(headers);
                if(Test.isrunningtest()){
                    finalResponse = false;
                } else {
                    HttpResponse response = callout.get();
                    finalResponse = response.getStatusCode() == 202;
                }
            
        }
        catch(Exception e){
            Logger.addDebugEntry('AnnualRollBatchSync makeEndcallout failed', ''+e.getMessage());
            Logger.saveLog();
            finalResponse = false;
            throw e;
        }
        
        return finalResponse;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Case>
    // @param : String
    // @description : update cases with trigger batch id and integration status
    // @return : boolean
    //-----------------------------
    public static void updateEligibleCases(List<Case> cases, String triggerBatchID, String integrationStatus){
        for(Case inputCase : cases){
            inputCase.IntegrationStatus__c = integrationStatus;
            inputCase.TriggerBatchId__c = triggerBatchID;
        }
        Database.update(cases);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : String
    // @param : String
    // @param : String
    // @description : update Roll Year with batch id, failure reason and Ingetration status
    // @return : void
    //-----------------------------
    public static void updateRollYear(String currentClosingRollYear, String triggerBatchID, String failureReason, String integrationStatus){
        List<RollYear__c> rollYears = new List<RollYear__c>();
        for(RollYear__c rollYear : [SELECT TriggerBatchId__c, IntegrationStatus__c, FailureReason__c FROM RollYear__c where Year__c = :currentClosingRollYear]){
            rollYear.TriggerBatchId__c = triggerBatchID;
            rollYear.IntegrationStatus__c = integrationStatus;
            rollYear.FailureReason__c = failureReason;
            rollYears.add(rollYear);
        }
        update rollYears;
    }
}