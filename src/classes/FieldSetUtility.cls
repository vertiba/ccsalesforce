/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//                                                                                                                       
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######  
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##    
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##    
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##    
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##    
//                                                                                                                       
//=======================================================================================================================


/*****************************************
 *             FieldSetUtility              *
 * PROVIDE UTILITY METHODS FOR FIELD SET *
 *****************************************/

public without sharing class FieldSetUtility {

    
    public static List<FieldSetUtility.FieldSetMember> getFieldSetFields(String objectName, String fieldSetName){
        if(String.isBlank(objectName)) return new List<FieldSetUtility.FieldSetMember>();
        if(String.isBlank(fieldSetName)) return new List<FieldSetUtility.FieldSetMember>();

        getObjectFieldSets(objectName);
        
        if(fieldSetsByName == null ) return new List<FieldSetUtility.FieldSetMember>();
        if(fieldSetsByName.isEmpty()) return new List<FieldSetUtility.FieldSetMember>();
        
        return getFields(fieldSetName);
    }   
    
    /**************************
    * PRIVATE HELPER METHODS *
    **************************/

    private static Map<String, Schema.FieldSet> fieldSetsByName;
    
    private static void getObjectFieldSets(String objectName){
        Schema.SObjectType objectType               = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
        fieldSetsByName                             = objectDescribe.fieldSets.getMap();
    }

    private static List<FieldSetUtility.FieldSetMember> getFields( String fieldSetName) {
        Schema.FieldSet fieldSet                            = fieldSetsByName.get(fieldSetName);
        List<Schema.FieldSetMember> schemaFieldSetMembers   = fieldSet.getFields();
        
        List<FieldSetUtility.FieldSetMember> fieldSetMembers   = new List<FieldSetUtility.FieldSetMember>();
        FieldSetUtility.FieldSetMember fieldSetMember;

        for (Schema.FieldSetMember fsm : schemaFieldSetMembers) {
            fieldSetMember = new FieldSetUtility.FieldSetMember(fsm);
            fieldSetMembers.add(fieldSetMember);
        }
        return fieldSetMembers;
    }

    public without sharing class FieldSetMember {

        public FieldSetMember(Schema.FieldSetMember f) {
            this.isDBRequired   = f.DBRequired;
            this.fieldPath      = f.fieldPath;
            this.label          = f.label;
            this.isRequired     = f.required;
            this.type           = '' + f.getType();
        }
        
        @AuraEnabled
        public Boolean isDBRequired { get;set; }

        @AuraEnabled
        public String fieldPath { get;set; }

        @AuraEnabled
        public String label { get;set; }

        @AuraEnabled
        public Boolean isRequired { get;set; }

        @AuraEnabled
        public String type { get; set; }
    }

}