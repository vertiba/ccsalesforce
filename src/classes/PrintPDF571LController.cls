/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR- 8469 Conroller for PrintPDF571L VF page
//-----------------------------
public class PrintPDF571LController {

    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is sending Statement fields value to VF page
    // @return : void
    //-----------------------------
    public Statement__c getStatement() {
        return [SELECT Id,Supplies__c,Equipment__c,TotalLeasedEquipmentCost__c,LeasedEquipment__c,ConstructioninProgress__c,LeaseholdImprovements__c,TotalReportedAssets__c,Property__c,Property__r.name,
                PropertyId__c,AccountName__c,MailingAddress__c,RollCode__c,BusinessType__c,BusinessSubtype__c,BusinessPhone__c,ContactEmailAddress__c,FaxNumber__c,OwnLandatBusinessLocation__c,StartofBusinessatthisLocation__c,FormerBusinessName__c,AccountingAddress__c,AccountingContactName__c,AccountingContactNumber__c,RealPropertyAtTimeOfAcquisition__c,
            Signature__c,DateofSignature__c,LegalEntityName__c,FEINorEIN__c,PreparersName__c,PreparersTitle__c,PreparersPhoneNumber__c,TotalCounterlinesPartitionsetcCost__c,TotalSignsCameraTVEquipmentetcCost__c,TotalCarpetsDrapesCost__c,TotalATMsCost__c,TotalVaultDoorNightDepositoriesCost__c,TotalDriveupWalkupWindowKioskCost__c
                ,FileDate__c, Form__c, Name, Remarks__c, FilerName__c, NameonDeedRecordedasShown__c, ControllingInterest__c, AssesseeorAuthorizedAgentName__c, AssesseeorAuthorizedAgentTitle__c, NoPropertyBelongingToOthers__c, CertifiedNoAssetLineItems__c  FROM Statement__c
            WHERE Id = :ApexPages.currentPage().getParameters().get('recordId')
        ];
    }
    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is sending StatementReportedAsset fields value to VF page
    // @return : void
    //-----------------------------
    public List<StatementReportedAsset__c> getStatementReportedAssets() {      
        List<StatementReportedAsset__c> statementReportedAssets = [SELECT Id, Form__c, Subcategory__c, Category__c, AcquisitionYear__c, Cost__c,LessorsName__c,TaxObligation__c,LeasedAssetType__c,AnnualRent__c,
                                                                   NetValueChangedfromLastYear__c, TotalAdditionValue__c, TotalDisposalValue__c, IsMatchedAmount__c,Description__c,
                                                                   (SELECT Id, Name, Addition_Disposal_Month__c, Addition_Disposal_Year__c, Cost__c, Type__c, Description__c FROM ReportedAssetSchedules__r order by Type__c)
                                                                   FROM StatementReportedAsset__c WHERE Statement__c =:ApexPages.currentPage().getParameters().get('recordId') 
                                                                   AND Category__c != 'Leased Equipment'];
        return statementReportedAssets;
    }
    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is sending StatementReportedAsset fields value to VF page based on certain filters
    // @return : void
    //-----------------------------
    public List<StatementReportedAsset__c> getLeasedStatementReportedAssets() {      
        List<StatementReportedAsset__c> statementReportedAssets = [SELECT Id, Form__c, Subcategory__c, Category__c, AcquisitionYear__c, Cost__c,LessorsName__c,TaxObligation__c,LeasedAssetType__c,AnnualRent__c,
                                                                   NetValueChangedfromLastYear__c, TotalAdditionValue__c, TotalDisposalValue__c, IsMatchedAmount__c,Description__c,
                                                                   (SELECT Id, Name, Addition_Disposal_Month__c, Addition_Disposal_Year__c, Cost__c, Type__c, Description__c FROM ReportedAssetSchedules__r order by Type__c)
                                                                   FROM StatementReportedAsset__c WHERE Statement__c =:ApexPages.currentPage().getParameters().get('recordId') 
                                                                   AND NetValueChangedfromLastYear__c != 0 and Category__c = 'Leased Equipment'];
        return statementReportedAssets;
    }

    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is sending ReportedAssetSchedule fields value to VF page
    // @return : void
    //-----------------------------
    public List<StatementReportedAsset__c> getReportedAssetsWithSchedule() {      
        List<StatementReportedAsset__c> statementReportedAssets = [SELECT Id, Form__c, Subcategory__c, Category__c, AcquisitionYear__c, Cost__c,
                                                                   NetValueChangedfromLastYear__c, TotalAdditionValue__c, TotalDisposalValue__c, IsMatchedAmount__c,
                                                                   (SELECT Id, Name, Addition_Disposal_Month__c, Addition_Disposal_Year__c, Cost__c, Type__c, Description__c FROM ReportedAssetSchedules__r order by Type__c)
                                                                   FROM StatementReportedAsset__c WHERE Statement__c =:ApexPages.currentPage().getParameters().get('recordId')
                                                                   AND Subcategory__c IN : CCSFConstants.STATEMENT_REPORTED_ASSETS.SCHEDULE_D_ASSET_CLASSIFICATIONS AND NetValueChangedfromLastYear__c != 0];
        return statementReportedAssets;
    }
}