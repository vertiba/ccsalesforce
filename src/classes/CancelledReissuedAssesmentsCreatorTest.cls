/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
* @Business: Test class for CancelledReissuedAssesmentsCreator
* 
* @Author: Publicis.Sapient
*
*/
@isTest
public class CancelledReissuedAssesmentsCreatorTest {
    
    @testSetUp
    static void createTestData(){
        
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin); 
        User officeAssistant = TestDataUtility.getOfficeAssistantUser();
        users.add(officeAssistant);
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            
            String currentFiscalyear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());            
            String previousFiscalyear = String.valueOf(Integer.valueOf(currentFiscalyear)-1);
            List<RollYear__c> rollyears = new List<RollYear__c>();
            RollYear__c currentRollyear = TestDataUtility.buildRollYear(currentFiscalyear,currentFiscalyear,'Roll Open');
            RollYear__c closedRollyear = TestDataUtility.buildRollYear(previousFiscalyear,previousFiscalyear,'Roll Closed');
            rollyears.add(currentRollyear);
            rollyears.add(closedRollyear);
            insert rollyears;
            
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;
            
            Case cas = new Case();
            cas.AccountId = bppProperty.account__c;
            cas.Property__C = bppProperty.id;
            cas.MailingCountry__c = 'US';
            cas.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            cas.Type='Regular';
            cas.RecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
            
            insert cas; 
            
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,cas.id);
            insert assessmentLineItem; 
            
            //insert Vessel property
            Property__c vesselProperty = TestDataUtility.getVesselProperty();
            vesselProperty.Account__c = businessAccount.Id;
            insert vesselProperty;
        }
    }
    
    @isTest
    private static void testRedirectAsAdminCancelAndReissue(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,integrationstatus__c ,Status,SubStatus__c,SequenceNumber__c from Case];
        cs.Status= CCSFConstants.ASSESSMENT.STATUS_CLOSED;
        cs.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        cs.SequenceNumber__c= 1;
        cs.integrationstatus__c = 'Sent To TTX';             
        update cs;
        System.runAs(sysAdminUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs));
            
            Test.startTest();
            // Clicked on Cancel assesment and Reissue Assessment Button
            extension.calledFromOnlyCancel = false; 
            extension.redirect();            
            Test.stopTest();
            System.assertEquals(System.Label.CancelAndReissuedAssessmentsCreated, extension.recordStatus,'Cancel & Reissued Success');
            List<Case> cases=[Select CancelAttempted__c,Id,AdjustmentType__c from Case];            
            system.assertEquals(3,cases.size(),'1 Orginial, 1 Closed assessment and 1 Reissued Assessment');
            
        }
        
    } 
    @isTest
    private static void testRedirectAsAdminCancel(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,integrationstatus__c ,Status,SubStatus__c,SequenceNumber__c from Case];
        cs.Status= CCSFConstants.ASSESSMENT.STATUS_CLOSED;
        cs.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        cs.SequenceNumber__c= 1;
        cs.integrationstatus__c = 'Sent To TTX';             
        update cs;
        System.runAs(sysAdminUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs));
            
            Test.startTest();
            // Clicked on Cancel assesment Button
            extension.calledFromOnlyCancel = true; 
            extension.redirect();            
            Test.stopTest();
            System.assertEquals( System.Label.CancelAssessmentsCreated, extension.recordStatus,'Cancel Success');
            List<Case> cases=[Select CancelAttempted__c,Id,AdjustmentType__c from Case];            
            system.assertEquals(2,cases.size(),'1 Orginial, 1 Closed assessment');
            
        }
        
    } 
    
    @isTest
    private static void testRedirectAsOfficeAssisstant(){
        User officeAssistantUser = [SELECT Id FROM User WHERE Profile.Name !=: 'System Administrator' AND Profile.Name !=: 'BPP Staff' LIMIT 1]; 
        
        Case cs = [Select Id,RecordTypeId from Case];
        cs.integrationstatus__c = 'Sent To TTX';
        update cs;
        System.runAs(officeAssistantUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs));
            extension.calledFromOnlyCancel = true; 
            Test.startTest();
            extension.redirect(); 
            Test.stopTest();
            System.assertEquals(System.Label.NoPermission, extension.recordStatus);
        }
    } 
    
    @isTest
    private static void testRedirectAsLegacyVesselRecordNoAccess(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];         
        Case cs = [Select Id,RecordTypeId from Case];
        cs.integrationstatus__c = 'Sent To TTX';     
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_LABEL_NAME).getRecordTypeId();
        update cs;
        System.runAs(sysAdminUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs));
            extension.calledFromOnlyCancel = false; 
            Test.startTest();
            extension.redirect(); 
            Test.stopTest();
            System.assertEquals(System.Label.CancelReisueNotAllowed, extension.recordStatus);
        }    
    }
    @isTest
    private static void testRedirectAsLegacyVesselRecordCancelNoAccess(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];         
        Case cs = [Select Id,RecordTypeId from Case];
        cs.integrationstatus__c = 'Sent To TTX';     
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_LABEL_NAME).getRecordTypeId();
        update cs;
        System.runAs(sysAdminUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs));
            extension.calledFromOnlyCancel = true; 
            Test.startTest();
            extension.redirect(); 
            Test.stopTest();
            System.assertEquals(System.Label.CancelAssessmentNotAllowed, extension.recordStatus);
        }    
    } 
    
    @isTest
    private static void testRedirectAsLegacyVesselRecordCancelAccess(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Id vesselPropertyRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME);         
        Property__c property =[Select Id from Property__c where RecordTypeId =:vesselPropertyRecordTypeId limit 1];
        Case cs1= new Case(); 
        cs1.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);          
        cs1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_LABEL_NAME).getRecordTypeId();
        cs1.Status= CCSFConstants.ASSESSMENT.STATUS_CLOSED;
        cs1.Property__c= property.Id;
        cs1.Type ='Regular';
        cs1.AdjustmentType__c='New';
        cs1.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;      
        cs1.SequenceNumber__c= 2;
        cs1.integrationstatus__c = 'Sent To TTX';             
        insert cs1;
        System.runAs(sysAdminUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs1));
            
            Test.startTest();
            // Clicked on Cancel assesment and Reissue Assessment Button
            extension.calledFromOnlyCancel = false; 
            extension.redirect();            
            Test.stopTest();
            System.assertEquals(System.Label.CancelAndReissuedAssessmentsCreated, extension.recordStatus,'Cancel & Reissued Success');
            List<Case> cases=[Select CancelAttempted__c,Id,AdjustmentType__c from Case];            
            system.assertEquals(4,cases.size(),'1 Newly created Orginial, 1 Closed assessment, 1 Reissued Assessment and 1 already available old BPP assessment');
            
        }
        
    }     
    @isTest
    private static void testalreadyCancelled(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,integrationstatus__c ,Status,SubStatus__c,SequenceNumber__c from Case];
        cs.Status= CCSFConstants.ASSESSMENT.STATUS_CLOSED;
        cs.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        cs.SequenceNumber__c= 1;
        cs.CancelAttempted__c=true;
        cs.integrationstatus__c = 'Sent To TTX';             
        update cs;
        System.runAs(sysAdminUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs));
            
            Test.startTest();
            // Clicked on Cancel assesment and Reissue Assessment Button
            extension.calledFromOnlyCancel = true; 
            extension.redirect();            
            Test.stopTest();
            System.assertEquals( System.Label.AlreadyCancelMessage, extension.recordStatus,'Cancel Success');           
            
        }        
    } 
    
    @isTest
    private static void testCancelEscape(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Id businessPropertyRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);    
        Property__c property =[Select Id from Property__c where RecordTypeId =:businessPropertyRecordTypeId limit 1];
        Case cs= new Case(); 
        cs.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);          
        cs.RecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
        cs.Status= CCSFConstants.ASSESSMENT.STATUS_CLOSED;
        cs.Property__c= property.Id;
        cs.Type ='Escape';
        cs.AdjustmentType__c='New';
        cs.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;      
        cs.SequenceNumber__c= 2;
        cs.integrationstatus__c = 'Sent To TTX';             
        insert cs;
        System.runAs(sysAdminUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs));
            Test.startTest();
            // Clicked on Cancel & Reissue assesment Button
            extension.calledFromOnlyCancel = true; 
            extension.redirect(); 
            Test.stopTest();  
            System.assertEquals( System.Label.CancelAssessmentsCreated, extension.recordStatus,'Cancel Success');
        }
        
    } 
    
    @isTest
    private static void testCancelEscapeVessel(){
        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Id vesselPropertyRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME);         
        Property__c property =[Select Id from Property__c where RecordTypeId =:vesselPropertyRecordTypeId limit 1];
        Case cs= new Case(); 
        cs.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);          
        cs.RecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType,CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME);
        cs.Status= CCSFConstants.ASSESSMENT.STATUS_CLOSED;
        cs.Property__c= property.Id;
        cs.Type ='Escape';
        cs.AdjustmentType__c='New';
        cs.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;      
        cs.SequenceNumber__c= 2;
        cs.integrationstatus__c = 'Sent To TTX';             
        insert cs;
        System.runAs(sysAdminUser){
            CancelledReissuedAssesmentsCreator extension = new CancelledReissuedAssesmentsCreator(new ApexPages.standardController(cs));
            Test.startTest();
            // Clicked on Cancel & Reissue assesment Button
            extension.calledFromOnlyCancel = true; 
            extension.redirect(); 
            Test.stopTest();  
            System.assertEquals( System.Label.CancelAssessmentsCreated, extension.recordStatus,'Cancel Success');
        }
        
    } 
}