/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/*********************** 
 * @Author: Publicis Sapient
 * @Apex TestClassName: AddressEntryControllerMockCallout
 * @Date : 26th Nov 2019 
 * 
 **********************/
@isTest
global class AddressEntryControllerMockCallout implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        
        // Send a mock response for a specific endpoint
        String endpoint ='http://production.shippingapis.com';      
        System.assertEquals(true, req.getEndpoint().startsWith(endpoint));
        System.assertEquals('GET', req.getMethod());
         string sessionBody;
        
        // Create a response
        if (req.getEndpoint().contains('API=CityStateLookup')) {
                sessionBody = '<CityStateLookupResponse>'+'<ZipCode ID="0">'+
             '<Zip5>94104</Zip5><City>SAN FRANCISCO</City><State>CA</State></ZipCode></CityStateLookupResponse>';
            
        }else if (req.getEndpoint().contains('API=Verify')){
            
            sessionBody = '<AddressValidateRequest USERID="512PUBLI4209">'
            + '<Address ID="0">'
            + '<Address1></Address1>'
            + '<Address2>355 Bush</Address2>'
            + '<City>SAN FRANCISCO</City>'
            + '<State>CA</State>'
            + '<Zip5>94104</Zip5>'
            + '<Zip4></Zip4>'
            + '</Address>'
            + '</AddressValidateRequest>';
        }
        
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml; charset=utf-8');
        res.setBody(sessionBody);
        res.setStatusCode(200);
        system.debug('end points---->'+ req.getEndpoint());
        
      
        return res; 
        
    } 
    
    
}