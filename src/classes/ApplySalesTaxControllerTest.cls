/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##
//
//=======================================================================================================================


/******************************************
 *      APPLYSALESTAXCONTROLLERTEST       *
 * TEST CLASS FOR APPLYSALESTAXCONTROLLER *
 ******************************************/

@isTest
private class ApplySalesTaxControllerTest {

    @isTest
    private static void should_GetEmptySalesTaxWrappers_When_RelevantDataNotExists(){

        Test.startTest();
        List<ApplySalesTaxWrapper> testAppleySalesTaxWrappers   = ApplySalesTaxController.getAllAssessmentLineItems(null);
        List<ApplySalesTaxWrapper> testAppleySalesTaxWrappers1  = ApplySalesTaxController.getAllAssessmentLineItems('');
        Test.stopTest();

        System.assert(testAppleySalesTaxWrappers != null);
        System.assertEquals(0,testAppleySalesTaxWrappers.size());
        System.assert(testAppleySalesTaxWrappers1 != null);
        System.assertEquals(0,testAppleySalesTaxWrappers1.size());
    }

    @isTest
    private static void should_GetSalesTaxWrappers_When_RelevantDataExists(){
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User56',alias='tuse5r6',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testus56r@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            List<Penalty__c> penalties = new List<Penalty__c>();
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            penalties.add(penalty1);
            
            Penalty__c penalty2 = new Penalty__c();
            penalty2.PenaltyMaxAmount__c = 500;
            penalty2.Percent__c = 25;
            penalty2.RTCode__c = '214.13';
            penalties.add(penalty2);
            
            Penalty__c penalty3 = new Penalty__c();
            penalty3.PenaltyMaxAmount__c = 500;
            penalty3.Percent__c = 25;
            penalty3.RTCode__c = '504';
            penalties.add(penalty3);
            insert penalties;
            
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c='Roll Open';
            insert rollYear;
            
            RollYear__c prevrollYear = new RollYear__c();
            prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-2);
            prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-2);
            prevrollYear.Status__c='Roll Closed';
            insert prevrollYear;
            
            RollYear__c nextrollYear = new RollYear__c();
            nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Status__c='Pending';
            insert nextrollYear;
            
            Account accountRecord = new Account();
            accountRecord.Name='New Test Account';
            accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            accountRecord.EntityId__c = '654321';
            accountRecord.BusinessStatus__c='active';
            insert accountRecord;
            
            Property__c propertyRecord = new Property__c();
            propertyRecord.Name = 'new test property';
            propertyRecord.PropertyId__c = '153536';
            propertyRecord.MailingCountry__c = 'US';
            propertyRecord.Account__c = accountRecord.Id;
            insert propertyRecord;
            
            Case caseRecord = new Case();
            caseRecord.AccountName__c='new test account';
            caseRecord.type = 'Regular';
            caseRecord.AccountId = propertyRecord.account__c;
            caseRecord.ApplyFraudPenalty__c = true;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            caseRecord.Property__c = propertyRecord.Id;
            caseRecord.Status = 'In Progress';
            caseRecord.AssessmentYear__c ='2017';
            caseRecord.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            caseRecord.MailingCountry__c = 'US';
            database.insert(caseRecord);
            
            AssessmentLineItem__c assessmentLineItem = new AssessmentLineItem__c();
            assessmentLineItem.Case__c = caseRecord.Id;
            assessmentLineItem.AcquisitionYear__c = '2016';
            assessmentLineItem.Subcategory__c = 'ATMs';
            assessmentLineItem.AssetSubclassification__c = 'General';
            assessmentLineItem.Cost__c = 12000;
            database.insert(assessmentLineItem);
            
            FactorSource__c factorSource = new FactorSource__c();
            factorSource.Name = 'Sample';
            database.insert(factorSource);
            
            Factor__c factor = new Factor__c();
            factor.Name = 'F1';
            factor.FactorSource__c = factorSource.Id;
            factor.AssessmentYear__c = '2017';
            factor.AcquisitionYear__c = '2016';
            factor.FactorPercent__c = 2;
            factor.YearsOfUsefulLife__c = 3;
            database.insert(factor);
            
            BusinessAssetMapping__c businessMapping = new BusinessAssetMapping__c();
            businessMapping.AssetClassification__c = 'ATMs';
            businessMapping.AssetSubclassification__c = 'General';
            businessMapping.FactorSource__c = factorSource.Id;
            businessMapping.FactorType__c = 'Depreciation Factor';
            businessMapping.YearsOfUsefulLife__c = 3;
            businessMapping.FixturesAllocationPercent__c = 10;
            database.insert(businessMapping);
            Test.startTest();
            List<ApplySalesTaxWrapper> testApplySalesTaxWrappers = ApplySalesTaxController.getAllAssessmentLineItems(caseRecord.Id);
            Test.stopTest();
            
             System.assert(testApplySalesTaxWrappers != null);
            // System.assertEquals(5,testApplySalesTaxWrappers.size());
        }
    }

    @isTest
    private static void should_GetSalesTaxWrappers_When_RelevantDataExistsWithSalesTaxField(){

       Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User56',alias='tuse5r6',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testus56r@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            List<Penalty__c> penalties = new List<Penalty__c>();
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            penalties.add(penalty1);
            
            Penalty__c penalty2 = new Penalty__c();
            penalty2.PenaltyMaxAmount__c = 500;
            penalty2.Percent__c = 25;
            penalty2.RTCode__c = '214.13';
            penalties.add(penalty2);
            
            Penalty__c penalty3 = new Penalty__c();
            penalty3.PenaltyMaxAmount__c = 500;
            penalty3.Percent__c = 25;
            penalty3.RTCode__c = '504';
            penalties.add(penalty3);
            insert penalties;
            
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c='Roll Open';
            insert rollYear;
            
            RollYear__c prevrollYear = new RollYear__c();
            prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Status__c='Roll Closed';
            insert prevrollYear;
            
            RollYear__c nextrollYear = new RollYear__c();
            nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Status__c='Pending';
            insert nextrollYear;
            
            Account accountRecord = new Account();
            accountRecord.Name='New Test Account';
            accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            accountRecord.EntityId__c = '654321';
            accountRecord.BusinessStatus__c='active';
            insert accountRecord;
            
            Property__c propertyRecord = new Property__c();
            propertyRecord.Name = 'new test property';
            propertyRecord.PropertyId__c = '153536';
            propertyRecord.MailingCountry__c = 'US';
            propertyRecord.Account__c = accountRecord.Id;
            insert propertyRecord;
            
            Case caseRecord = new Case();
            caseRecord.AccountName__c='new test account';
            caseRecord.type = 'Regular';
            caseRecord.AccountId = propertyRecord.account__c;
            caseRecord.ApplyFraudPenalty__c = true;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            caseRecord.Property__c = propertyRecord.Id;
            caseRecord.Status = 'In Progress';
            caseRecord.AssessmentYear__c ='2017';
            caseRecord.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            caseRecord.MailingCountry__c = 'US';
            database.insert(caseRecord);
            
            FactorSource__c factorSource = new FactorSource__c();
            factorSource.Name = 'Sample';
            database.insert(factorSource);
            
            Factor__c factor = new Factor__c();
            factor.Name = 'F1';
            factor.FactorSource__c = factorSource.Id;
            factor.AssessmentYear__c = '2017';
            factor.AcquisitionYear__c = '2016';
            factor.FactorPercent__c = 2;
            factor.YearsOfUsefulLife__c = 3;
            database.insert(factor);
            
            BusinessAssetMapping__c businessMapping = new BusinessAssetMapping__c();
            businessMapping.AssetClassification__c = 'ATMs';
            businessMapping.AssetSubclassification__c = 'General';
            businessMapping.FactorSource__c = factorSource.Id;
            businessMapping.FactorType__c = 'Depreciation Factor';
            businessMapping.YearsOfUsefulLife__c = 3;
            businessMapping.FixturesAllocationPercent__c = 10;
            database.insert(businessMapping);
            
        	SalesTax__c testSalesTax = new SalesTax__c();
        	new TestDataFactory(testSalesTax).populateRequiredFields();
        	insert testSalesTax;

            AssessmentLineItem__c assessmentLineItem = new AssessmentLineItem__c();
            assessmentLineItem.Case__c = caseRecord.Id;
            assessmentLineItem.AcquisitionYear__c = '2016';
            assessmentLineItem.Subcategory__c = 'ATMs';
            assessmentLineItem.AssetSubclassification__c = 'General';
            assessmentLineItem.SalesTax__c = testSalesTax.Id;
            assessmentLineItem.Cost__c = 12000;
            database.insert(assessmentLineItem);
			
            Test.startTest();
        		List<ApplySalesTaxWrapper> testAppleySalesTaxWrappers = ApplySalesTaxController.getAllAssessmentLineItems(caseRecord.Id);
        	Test.stopTest();
            system.assertEquals(assessmentLineItem.ManualAdjustedCost__c, null);
        	System.assert(testAppleySalesTaxWrappers != null);
        // System.assertEquals(5,testAppleySalesTaxWrappers.size());
        }
    }

    @isTest
    private static void should_NoSalesTaxActionApply_When_RelevantDataNotExists(){
       
		Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User56',alias='tuse5r6',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testus56r@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            List<Penalty__c> penalties = new List<Penalty__c>();
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            penalties.add(penalty1);
            
            Penalty__c penalty2 = new Penalty__c();
            penalty2.PenaltyMaxAmount__c = 500;
            penalty2.Percent__c = 25;
            penalty2.RTCode__c = '214.13';
            penalties.add(penalty2);
            
            Penalty__c penalty3 = new Penalty__c();
            penalty3.PenaltyMaxAmount__c = 500;
            penalty3.Percent__c = 25;
            penalty3.RTCode__c = '504';
            penalties.add(penalty3);
            insert penalties;
            
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c='Roll Open';
            insert rollYear;
            
            RollYear__c prevrollYear = new RollYear__c();
            prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Status__c='Roll Closed';
            insert prevrollYear;
            
            RollYear__c nextrollYear = new RollYear__c();
            nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Status__c='Pending';
            insert nextrollYear;
            
            Account accountRecord = new Account();
            accountRecord.Name='New Test Account';
            accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            accountRecord.EntityId__c = '654321';
            accountRecord.BusinessStatus__c='active';
            insert accountRecord;
            
            Property__c propertyRecord = new Property__c();
            propertyRecord.Name = 'new test property';
            propertyRecord.PropertyId__c = '153536';
            propertyRecord.MailingCountry__c = 'US';
            propertyRecord.Account__c = accountRecord.Id;
            insert propertyRecord;
            
            Case caseRecord = new Case();
            caseRecord.AccountName__c='new test account';
            caseRecord.type = 'Regular';
            caseRecord.AccountId = propertyRecord.account__c;
            caseRecord.ApplyFraudPenalty__c = true;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            caseRecord.Property__c = propertyRecord.Id;
            caseRecord.Status = 'In Progress';
            caseRecord.AssessmentYear__c ='2017';
            caseRecord.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            caseRecord.MailingCountry__c = 'US';
            database.insert(caseRecord);
            
            FactorSource__c factorSource = new FactorSource__c();
            factorSource.Name = 'Sample';
            database.insert(factorSource);
            
            Factor__c factor = new Factor__c();
            factor.Name = 'F1';
            factor.FactorSource__c = factorSource.Id;
            factor.AssessmentYear__c = '2017';
            factor.AcquisitionYear__c = '2016';
            factor.FactorPercent__c = 2;
            factor.YearsOfUsefulLife__c = 3;
            database.insert(factor);
            
            BusinessAssetMapping__c businessMapping = new BusinessAssetMapping__c();
            businessMapping.AssetClassification__c = 'ATMs';
            businessMapping.AssetSubclassification__c = 'General';
            businessMapping.FactorSource__c = factorSource.Id;
            businessMapping.FactorType__c = 'Depreciation Factor';
            businessMapping.YearsOfUsefulLife__c = 3;
            businessMapping.FixturesAllocationPercent__c = 10;
            database.insert(businessMapping);
            
        	SalesTax__c testSalesTax = new SalesTax__c();
        	new TestDataFactory(testSalesTax).populateRequiredFields();
        	insert testSalesTax;

            AssessmentLineItem__c assessmentLineItem = new AssessmentLineItem__c();
            assessmentLineItem.Case__c = caseRecord.Id;
            assessmentLineItem.AcquisitionYear__c = '2016';
            assessmentLineItem.Subcategory__c = 'ATMs';
            assessmentLineItem.AssetSubclassification__c = 'General';
            assessmentLineItem.SalesTax__c = testSalesTax.Id;
            assessmentLineItem.Cost__c = 12000;
            database.insert(assessmentLineItem);

        	Test.startTest();
        	ApplySalesTaxController.applySalesTaxAction('');
        	Test.stopTest();

         AssessmentLineItem__c result = [SELECT SalesTax__c FROM AssessmentLineItem__c WHERE Id=:assessmentLineItem.Id];
            system.debug('result--'+result);
         System.assertEquals(true,String.isNotBlank(result.SalesTax__c));
        }
    }

    @isTest
    private static void should_SalesTaxApply_When_RelevantDataExists(){

        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User56',alias='tuse5r6',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testus56r@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            List<Penalty__c> penalties = new List<Penalty__c>();
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            penalties.add(penalty1);
            
            Penalty__c penalty2 = new Penalty__c();
            penalty2.PenaltyMaxAmount__c = 500;
            penalty2.Percent__c = 25;
            penalty2.RTCode__c = '214.13';
            penalties.add(penalty2);
            
            Penalty__c penalty3 = new Penalty__c();
            penalty3.PenaltyMaxAmount__c = 500;
            penalty3.Percent__c = 25;
            penalty3.RTCode__c = '504';
            penalties.add(penalty3);
            insert penalties;
            
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c='Roll Open';
            insert rollYear;
            
            RollYear__c prevrollYear = new RollYear__c();
            prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Status__c='Roll Closed';
            insert prevrollYear;
            
            RollYear__c nextrollYear = new RollYear__c();
            nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Status__c='Pending';
            insert nextrollYear;
            
            Account accountRecord = new Account();
            accountRecord.Name='New Test Account';
            accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            accountRecord.EntityId__c = '654321';
            accountRecord.BusinessStatus__c='active';
            insert accountRecord;
            
            Property__c propertyRecord = new Property__c();
            propertyRecord.Name = 'new test property';
            propertyRecord.PropertyId__c = '153536';
            propertyRecord.MailingCountry__c = 'US';
            propertyRecord.Account__c = accountRecord.Id;
            insert propertyRecord;
            
            Case caseRecord = new Case();
            caseRecord.AccountName__c='new test account';
            caseRecord.type = 'Regular';
            caseRecord.AccountId = propertyRecord.account__c;
            caseRecord.ApplyFraudPenalty__c = true;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            caseRecord.Property__c = propertyRecord.Id;
            caseRecord.Status = 'In Progress';
            caseRecord.AssessmentYear__c ='2017';
            caseRecord.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            caseRecord.MailingCountry__c = 'US';
            database.insert(caseRecord);
            
            FactorSource__c factorSource = new FactorSource__c();
            factorSource.Name = 'Sample';
            database.insert(factorSource);
            
            Factor__c factor = new Factor__c();
            factor.Name = 'F1';
            factor.FactorSource__c = factorSource.Id;
            factor.AssessmentYear__c = '2017';
            factor.AcquisitionYear__c = '2016';
            factor.FactorPercent__c = 2;
            factor.YearsOfUsefulLife__c = 3;
            database.insert(factor);
            
            BusinessAssetMapping__c businessMapping = new BusinessAssetMapping__c();
            businessMapping.AssetClassification__c = 'ATMs';
            businessMapping.AssetSubclassification__c = 'General';
            businessMapping.FactorSource__c = factorSource.Id;
            businessMapping.FactorType__c = 'Depreciation Factor';
            businessMapping.YearsOfUsefulLife__c = 3;
            businessMapping.FixturesAllocationPercent__c = 10;
            database.insert(businessMapping);
            
        	SalesTax__c testSalesTax = new SalesTax__c();
        	new TestDataFactory(testSalesTax).populateRequiredFields();
        	insert testSalesTax;
			
            List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
            AssessmentLineItem__c testAssmtLineItem ;
            for(Integer count =0 ; count<5 ; count++){
                testAssmtLineItem                               = new AssessmentLineItem__c();
                testAssmtLineItem.AcquisitionYear__c            = String.valueOf(Date.today().addYears(-3).year());
                testAssmtLineItem.UsefulLife__c                 = 5;
                testAssmtLineItem.Case__c                       = caseRecord.Id;
                new TestDataFactory(testAssmtLineItem).populateRequiredFields();
                assessmentLineItems.add(testAssmtLineItem);
            }
			insert assessmentLineItems;
            
        	List<ApplySalesTaxWrapper> testapplySalesTaxWrappers = new List<ApplySalesTaxWrapper>();
        	for(Integer count = 0 ; count<5 ; count++){
        	 testapplySalesTaxWrappers.add(new ApplySalesTaxWrapper(assessmentLineItems[count]));
        	}

             
			Test.startTest();
        	ApplySalesTaxController.applySalesTaxAction(JSON.serialize(testapplySalesTaxWrappers));
        	Test.stopTest();

        	AssessmentLineItem__c result = [SELECT SalesTax__c FROM AssessmentLineItem__c WHERE Id=:assessmentLineItems[0].Id];
       	    System.assertEquals(true,String.isBlank(result.SalesTax__c));
        }
    }

    @isTest
    private static void should_SalesTaxApply_When_RelevantDataExistsWithSalesTax(){
Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User56',alias='tuse5r6',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testus56r@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            List<Penalty__c> penalties = new List<Penalty__c>();
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            penalties.add(penalty1);
            
            Penalty__c penalty2 = new Penalty__c();
            penalty2.PenaltyMaxAmount__c = 500;
            penalty2.Percent__c = 25;
            penalty2.RTCode__c = '214.13';
            penalties.add(penalty2);
            
            Penalty__c penalty3 = new Penalty__c();
            penalty3.PenaltyMaxAmount__c = 500;
            penalty3.Percent__c = 25;
            penalty3.RTCode__c = '504';
            penalties.add(penalty3);
            insert penalties;
            
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c='Roll Open';
            insert rollYear;
            
            RollYear__c prevrollYear = new RollYear__c();
            prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Status__c='Roll Closed';
            insert prevrollYear;
            
            RollYear__c nextrollYear = new RollYear__c();
            nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Status__c='Pending';
            insert nextrollYear;
            
            Account accountRecord = new Account();
            accountRecord.Name='New Test Account';
            accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            accountRecord.EntityId__c = '654321';
            accountRecord.BusinessStatus__c='active';
            insert accountRecord;
            
            Property__c propertyRecord = new Property__c();
            propertyRecord.Name = 'new test property';
            propertyRecord.PropertyId__c = '153536';
            propertyRecord.MailingCountry__c = 'US';
            propertyRecord.Account__c = accountRecord.Id;
            insert propertyRecord;
            
            Case caseRecord = new Case();
            caseRecord.AccountName__c='new test account';
            caseRecord.type = 'Regular';
            caseRecord.AccountId = propertyRecord.account__c;
            caseRecord.ApplyFraudPenalty__c = true;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            caseRecord.Property__c = propertyRecord.Id;
            caseRecord.Status = 'In Progress';
            caseRecord.AssessmentYear__c ='2017';
            caseRecord.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            caseRecord.MailingCountry__c = 'US';
            database.insert(caseRecord);
            
            FactorSource__c factorSource = new FactorSource__c();
            factorSource.Name = 'Sample';
            database.insert(factorSource);
            
            Factor__c factor = new Factor__c();
            factor.Name = 'F1';
            factor.FactorSource__c = factorSource.Id;
            factor.AssessmentYear__c = '2017';
            factor.AcquisitionYear__c = '2016';
            factor.FactorPercent__c = 2;
            factor.YearsOfUsefulLife__c = 3;
            database.insert(factor);
            
            BusinessAssetMapping__c businessMapping = new BusinessAssetMapping__c();
            businessMapping.AssetClassification__c = 'ATMs';
            businessMapping.AssetSubclassification__c = 'General';
            businessMapping.FactorSource__c = factorSource.Id;
            businessMapping.FactorType__c = 'Depreciation Factor';
            businessMapping.YearsOfUsefulLife__c = 3;
            businessMapping.FixturesAllocationPercent__c = 10;
            database.insert(businessMapping);
            
        	SalesTax__c testSalesTax = new SalesTax__c();
        	new TestDataFactory(testSalesTax).populateRequiredFields();
        	insert testSalesTax;
			
            List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
            AssessmentLineItem__c assessmentLineItem = new AssessmentLineItem__c();
            assessmentLineItem.Case__c = caseRecord.Id;
            assessmentLineItem.AcquisitionYear__c = '2016';
            assessmentLineItem.Subcategory__c = 'ATMs';
            assessmentLineItem.AssetSubclassification__c = 'General';
            assessmentLineItem.SalesTax__c = testSalesTax.Id;
            assessmentLineItem.Cost__c = 12000;
            assessmentLineItems.add(assessmentLineItem);
            database.insert(assessmentLineItems);
            
        	assessmentLineItems[0].SalesTax__c = testSalesTax.Id;
        	update assessmentLineItems[0];
            List<ApplySalesTaxWrapper> testApplySalesTaxWrappers = new List<ApplySalesTaxWrapper>();
			testApplySalesTaxWrappers.add(new ApplySalesTaxWrapper(assessmentLineItems[0]));
            
        	Test.startTest();
            ApplySalesTaxController.getAllAssessmentLineItems(caseRecord.Id);
        	ApplySalesTaxController.applySalesTaxAction(JSON.serialize(testApplySalesTaxWrappers));
        	Test.stopTest();

        	AssessmentLineItem__c result = [SELECT SalesTax__c FROM AssessmentLineItem__c WHERE Id=:assessmentLineItems[0].Id];
        	System.assertEquals(true,String.isBlank(result.SalesTax__c));
        }
    }

}