/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class PotentialDuplicatesController {

    @AuraEnabled
    public static Boolean sobjectTypeHasDuplicateRules(Id recordId) {
        String sobjectName = String.valueOf(recordId.getSObjectType());
        Integer ruleCount = [SELECT COUNT() FROM DuplicateRule WHERE SObjectType = :sobjectName AND IsActive = true];

        return ruleCount > 0;
    }

    @AuraEnabled
    public static List<SObject> getPotentialDuplicates(Id recordId, String sObjectName) {
        List<DuplicateRecordSet> duplicateSets = [
            SELECT Id, DuplicateRuleId, Name, ParentId, RecordCount
            FROM DuplicateRecordSet
            WHERE Id IN (
                SELECT DuplicateRecordSetId FROM DuplicateRecordItem
                WHERE RecordId = :recordId
            )
            AND RecordCount > 1
        ];
System.debug('duplicateSets=' + duplicateSets);
        List<DuplicateRecordItem> duplicateRecords = [
			SELECT Id, RecordId, Record.Type, Record.Name
            FROM DuplicateRecordItem
            WHERE DuplicateRecordSetId = :duplicateSets
            AND RecordId != :recordId
            ORDER BY Record.Type, Record.Name
        ];
System.debug('duplicateRecords=' + duplicateRecords);
		return duplicateRecords;
	}

}