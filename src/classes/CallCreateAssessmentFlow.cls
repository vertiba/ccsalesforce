/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient
// @description : This class is used to call flow from Process Builder to create Assessment
// from Statement
// ASR- 9580 Bug
//-----------------------------


global class CallCreateAssessmentFlow {
    public final static Id BPP_RECORD_TYPE_ID = DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);
    public final static Id VESSEL_RECORD_TYPE_ID =  DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.VESSEL_RECORD_TYPE_API_NAME); 
    public final static String originLocation = 'CreateAsessmentFromStatement';
    
    
    @InvocableMethod
    public static void CreateAsessmentFromStatement(List<Statement__c> Statements ) {
        // Method getting called from Statement Process - Process Builder
        // When Statement is Submitted the Assessment will be created
        try{
            // List Empty return
            if(Statements.isEmpty()){return;}
            if(Statements.size() == 1 ){
                // Calling Flow Using Future Method
                callFlowByFutureMethod(Statements[0].Id,Statements[0].RecordTypeId);
            }
        }catch(Exception ex){
            Logger.addRecordExceptionEntry(Statements[0], ex, originLocation);
            Logger.saveLog();
        }
    }
    
    @Future
    public static void callFlowByFutureMethod(String statementId, String recordTypeId){
        try{
            Map<String,Object> parameters=new Map<String,Object>();
            parameters.put('varInputStatement',statementId);
            // Addedd for the skipped records processing fro DLRS rules,ASR-10663
            AsyncDlrsCalculator.getInstance().setQueueToRun(false);
            if(recordTypeId == BPP_RECORD_TYPE_ID){
                // If Bpp Statement Call BPP Assessment Flow - Create_Assessment_from_Statement            
               Flow.Interview createBPPAssessment = new Flow.Interview.Create_Assessment_from_Statement(parameters);  
                // Calling Flow to create Assessment for BPP statement
                createBPPAssessment.start();
            }else if(recordTypeId == VESSEL_RECORD_TYPE_ID){
                // If Vessel Statment Call Vessel Assessment Flow - Create_Vessel_Assessment_From_Statements
                Flow.Interview createVesselAssessment = new Flow.Interview.Create_Vessel_Assessment_From_Statements(parameters);  
                // Calling Flow to create Assessment for BPP statement
                createVesselAssessment.start();
            }
        }catch(Exception ex){
            Logger.addExceptionEntry(ex, originLocation);
            Logger.saveLog();
        }
        // Addedd for the skipped records processing fro DLRS rules,ASR-10663
        AsyncDlrsCalculator.getInstance().setQueueToRun(true);
    }
    
}