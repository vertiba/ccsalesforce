/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test AnnualRollCloseFileGenerationTest
//-----------------------------
@isTest
public with sharing class AnnualRollCloseFileGenerationTest {
    
    @isTest
    private static void triggerAABFileGenerationTest() {

        Rollyear__c rollYear = TestDataUtility.buildRollYear('2020','2020','Roll Open');
        insert rollYear;
        Test.setMock(HttpCalloutMock.class, new AnnualRollCloseFileGenerationMockCallout());
        Test.startTest();
        AnnualRollCloseFileGeneration.triggerAABCallout(rollYear.Name);
        Test.stopTest();
    }

    @isTest
    private static void checkPermissionTest() {

        Rollyear__c rollYear = TestDataUtility.buildRollYear('2020','2020','Roll Open');
        rollYear.IntegrationStatus__c = 'Sent To TTX';
        insert rollYear;
        Test.setMock(HttpCalloutMock.class, new AnnualRollCloseFileGenerationMockCallout());
        Test.startTest();
        AnnualRollCloseFileGeneration.checkRequiredPermissions(rollYear.Id);
        Test.stopTest();
    }

    @isTest
    private static void checFailedPermissionTest() {

        Rollyear__c rollYear = TestDataUtility.buildRollYear('2020','2020','Roll Open');
        rollYear.IntegrationStatus__c = '';
        insert rollYear;
        Test.setMock(HttpCalloutMock.class, new AnnualRollCloseFileGenerationMockCallout());
        Test.startTest();
        AnnualRollCloseFileGeneration.checkRequiredPermissions(rollYear.Id);
        Test.stopTest();
    }
}