/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This Batch class is to update Billable field on Case whenever the Waiver Expiration Date updated or
// 					Statute of Limitations is greater than 48 months from current date.
//-----------------------------

public class WaiverExpirationDateUpdated implements Database.Batchable<sObject>, BatchableErrorHandler{
    
    private String originLocation ='WaiverExpirationDateUpdated';
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method gets the Escape Case records based on conditions mentioned.
    // @return : Database.QueryLocator (List of Cases)
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        String query='Select Id, Status, Type, WaiverExpirationDate__c, Billable__c, StatuteOfLimitations__c, IntegrationStatus__c,CreatedDate ';
        query += ' FROM Case WHERE Type =: CCSFConstants.ASSESSMENT.TYPE_ESCAPE AND ';
        query += ' (Status =: CCSFConstants.ASSESSMENT.STATUS_INPROGRESS OR Status =: CCSFConstants.ASSESSMENT.STATUS_NEW) AND Billable__c != null';
        Logger.addDebugEntry(query, 'WaiverExpirationDateUpdated-start method' );
        Logger.saveLog();
        return Database.getQueryLocator([Select Id, Status, Type, WaiverExpirationDate__c,AssessmentYear__c, Billable__c, StatuteOfLimitations__c, IntegrationStatus__c,CreatedDate 
                                         FROM Case 
                                         WHERE Type =: CCSFConstants.ASSESSMENT.TYPE_ESCAPE
                                         AND (Status =: CCSFConstants.ASSESSMENT.STATUS_INPROGRESS OR Status =: CCSFConstants.ASSESSMENT.STATUS_NEW) AND Billable__c != null]);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : CasesList (List of Cases)
    // @description : This method changes the Billable field on conditions mentioned below.
    // @return : void
    public void execute(Database.BatchableContext bc, List<Case> assessments){
        List<Case> updatedAssessments = new List<Case>();
        Date currentDate = Date.valueOf(system.today());
        
        try{    
            for(Case record: assessments){
                Date createdDate = Date.valueOf(record.CreatedDate);
                Integer assessmentYear =  Integer.valueOf(record.AssessmentYear__c) ;
                //getting july of assessment year
                Date julyofAssessmentYear = Date.newInstance(assessmentYear, 07, 31);
                Integer monthsLeft = julyofAssessmentYear.monthsBetween(createdDate);
               
                if(record.WaiverExpirationDate__c >= currentDate && record.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.READY_TO_SEND_TO_TTX && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.SENT_TO_TTX && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.IN_TRANSIT_TO_TTX){
                    record.Billable__c = CCSFConstants.ASSESSMENT.BILLABLE_YES;
                    updatedAssessments.add(record);
                }else if(record.WaiverExpirationDate__c < currentDate && record.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_YES && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.READY_TO_SEND_TO_TTX && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.SENT_TO_TTX && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.IN_TRANSIT_TO_TTX)
                {
                    record.Billable__c = CCSFConstants.ASSESSMENT.BILLABLE_NO;
                    updatedAssessments.add(record);
                }else if(monthsLeft > record.StatuteOfLimitations__c && record.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_YES && record.WaiverExpirationDate__c == null && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.READY_TO_SEND_TO_TTX && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.SENT_TO_TTX && record.IntegrationStatus__c != CCSFConstants.ASSESSMENT.IN_TRANSIT_TO_TTX){
                    record.Billable__c = CCSFConstants.ASSESSMENT.BILLABLE_NO;
                    updatedAssessments.add(record);
                }
            }
            if(!updatedAssessments.isEmpty()){
                DescribeUtility.updateRecords(updatedAssessments, null);
            } 
            
        }catch(Exception ex){  
            Logger.addExceptionEntry(ex, originLocation);
            throw ex;
        }    
        Logger.saveLog();
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Placed for future purpose.
    // @return : void
    public void finish(Database.BatchableContext bc){
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, WaiverExpirationDateUpdated.class.getName(), asyncList[0].ParentJobId, false, '', asyncList[0].NumberOfErrors);
    } 
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
}