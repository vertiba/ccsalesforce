/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: RegenerateAccountAccessPIN_Test
 * Description: Test class for the batch RegenerateAccountAccessPIN
 */ 
@isTest
public class RegenerateAccountAccessPIN_Test {

    public static testmethod void updateAccessPIN(){
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User45',alias='tuse5r4',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testuse5r@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            Account accountRecord = new Account();
            accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            accountRecord.Name = 'Sample';
            database.insert(accountRecord);
            
           	Property__c propertyRecord = new Property__c();
            propertyRecord.Name = 'new test property';
            propertyRecord.PropertyId__c = '153536';
            propertyRecord.MailingCountry__c = 'US';
            propertyRecord.Account__c = accountRecord.Id;
            database.insert(propertyRecord);
            
            User taxpayer = TestDataUtility.getPortalUser();
            String fiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear)-1, 1, 1);
            
            Statement__c statement = new Statement__c();
            statement.FileDate__c = filingDate;
            statement.FilingMethod__c = 'eFile';
            statement.Property__c = propertyRecord.id;
            statement.PropertyScenario__c = 'Existing';
            statement.RecordTypeId = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalPropertyStatement').getRecordTypeId();
            statement.ownerId = taxpayer.Id;
            database.insert(statement);
            
            Integer initialNumber = 126538; 
            Test.startTest();
            Database.executeBatch(new RegenerateAccountAccessPIN(initialNumber));
            Statement__c validateRecord = [Select Id,PreviousStatementOwner__c,OwnerId from Statement__c where Id=:statement.Id];
            system.assertEquals(null, validateRecord.PreviousStatementOwner__c);
            Test.stopTest();
        }
    }
    
}