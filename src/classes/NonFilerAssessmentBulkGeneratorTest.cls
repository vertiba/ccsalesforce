/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to test NonFilerAssessmentBulkGenerator class.
//-----------------------------
@isTest
public class NonFilerAssessmentBulkGeneratorTest {

    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
    private static Integer nextYear 		= currentYear+1;
    
    @testSetup
    static void createTestData() {
        
        //Create Users
        List<User> users = new List<User>();
        User principal = TestDataUtility.getPrincipalUser();
        users.add(principal);
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin);  
        User auditor = TestDataUtility.getBPPAuditorUser();
        users.add(auditor); 
        insert users;
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        System.runAs(systemAdminUser) {
            
            //insert roll years
            List<RollYear__c> rollYears = new List<RollYear__c>();
            
            RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYearCurrentFiscalYear.IsLowValueBulkGeneratorProcessed__c = true;
            rollYearCurrentFiscalYear.IsDirectBillBulkGeneratorProcessed__c = true;            
            rollYears.add(rollYearCurrentFiscalYear);
            
            RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastYear),String.valueof(lastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastFiscalYear);

            RollYear__c rollYearLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastToLastYear),String.valueof(lastToLastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastToLastFiscalYear);
            
            RollYear__c rollYearNextFiscalYear = TestDataUtility.buildRollYear(String.valueof(nextYear),String.valueof(nextYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearNextFiscalYear);
            
            insert rollYears;
            
            //insert penalty
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            //insert account
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            //insert property
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;
            
            // insert validation rule custom setting
            ValidationRuleSettings__c setting = new ValidationRuleSettings__c();            
            setting.DisableValidationRules__c = true;
            insert setting;
            
            //insert statement            
            Map<String,String> inputParamsStatement = new Map<String,String>();
            Date filingDate = Date.newInstance(2019, 2, 25);  
            Date startofBussinessDate = Date.newInstance(2016, 3,4);
            inputParamsStatement.put('accountId',businessAccount.Id);
            inputParamsStatement.put('propertyId',bppProperty.Id);
            inputParamsStatement.put('formType','571-L');
            inputParamsStatement.put('businessScenario','Existing');
            inputParamsStatement.put('propertyScenario','Existing');
            inputParamsStatement.put('assessmentYear','2020'); 
            inputParamsStatement.put('recordTypeId',DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME));
            inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/2016'));
            inputParamsStatement.put('filingDate',String.valueOf('08/05/'+lastYear));
            
            Statement__c bppStatement = TestDataUtility.buildStatement(inputParamsStatement);
            insert bppStatement;
            
        }
    }
    
    @isTest
    static void testGenerateNonFilerAssessmentForBPPPrinicipal() {
        
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
       
        Test.startTest();        
                                  
            Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME)];
            
            //insert assessment
            Date lastYearEventDate=  Date.newInstance(lastYear, 3, 11);           
            Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                                CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,lastYearEventDate);
            
            insert assessment;
            
            //insert assessment line item
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,assessment.id);
            insert assessmentLineItem;                
            
            System.runAs(principalUser) 
            {             
                //run batch class
                GenerateNonFilerAssessment.runNonFilerAssessmentBulkGenerator();
                rollYears = [select id,Name, BPPLateFilingDate__c,IsNonFilerBulkGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1] ;      
                        
            }
        Test.stopTest();       
    }
	
	@isTest
    static void testForLastYearLegacyAssessment() {
        
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
       
        Test.startTest();        
                                  
            Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME)];
            bppProperty.LastFileDate__c = null;
        	update bppProperty;
            
            //insert assessment
            Date lastYearEventDate=  Date.newInstance(lastYear, 3, 11);           
            Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                                CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,lastYearEventDate);
            
            insert assessment;
            
            //insert assessment line item
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,assessment.id);
            insert assessmentLineItem;                
            
            System.runAs(principalUser) 
            {             
                //run batch class
                GenerateNonFilerAssessment.runNonFilerAssessmentBulkGenerator();
                rollYears = [select id,Name, BPPLateFilingDate__c,IsNonFilerBulkGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1] ;      
                        
            }
        Test.stopTest();       
    }

    @isTest
    static void testForCurrentYearAssesmentExists() {
        
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
        rollYears[1].BPPLateFilingDate__c = Date.parse(String.valueOf('01/05/'+currentYear));
        update rollYears[1];
        Test.startTest();        
                                  
            Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME)];
            bppProperty.LastFileDate__c = null;
        	update bppProperty;
            
            //insert assessment
            Date lastYearEventDate=  Date.newInstance(lastYear, 3, 11);           
            Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                                CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,lastYearEventDate);
            
            insert assessment;
            
            //insert assessment line item
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,assessment.id);
            insert assessmentLineItem;                
            
            System.runAs(principalUser) 
            {             
                //run batch class
                GenerateNonFilerAssessment.runNonFilerAssessmentBulkGenerator();
                rollYears = [select id,Name, BPPLateFilingDate__c,IsNonFilerBulkGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1] ;      
                        
            }
        Test.stopTest();       
    }


    @isTest
    static void testForNoPreviousYearAssesmentExists() {
        
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
        
        Test.startTest();        
                                  
            Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME)];
            bppProperty.LastFileDate__c = null;
        	update bppProperty;
                        
            //insert assessment
            Date currentYearEventDate=  Date.newInstance(currentYear, 3, 11);           
            Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                rollYears[1].Id,rollYears[1].Name,rollYears[2].Name,
                                                                CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,currentYearEventDate);
            
            insert assessment;
            
            //insert assessment line item
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,assessment.id);
            insert assessmentLineItem;     
            
            System.runAs(principalUser) 
            {
                //run batch class
                GenerateNonFilerAssessment.runNonFilerAssessmentBulkGenerator();
                rollYears = [select id,Name, BPPLateFilingDate__c,IsNonFilerBulkGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1] ;      
                        
            }
        Test.stopTest();       
    }
}