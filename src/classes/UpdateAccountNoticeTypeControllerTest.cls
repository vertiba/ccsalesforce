/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for UpdateAccountNoticeTypeController
 * 
 * @Author: Publicis.Sapient
 *
*/
@isTest
public class UpdateAccountNoticeTypeControllerTest {
    
    @testSetup
    static void createTestData() {
        
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User nonAdmin = TestDataUtility.getOfficeAssistantUser();
        User sysAdmin = TestDataUtility.getSystemAdminUser();
        INSERT new List<User>{ nonAdmin, sysAdmin };

        PermissionSetAssignment assignment = new PermissionSetAssignment(
        	AssigneeId = sysAdmin.Id,
        	PermissionSetId = permissionSet.Id
    	);
    	insert assignment;    
    }
    
    static testMethod void  checkPermissionForRollYearUpdatePermissionSet() {
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        
        Test.startTest();
        System.runAs(systemAdmin) {
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(new List<Account>());
          	UpdateAccountNoticeTypeController extension = new UpdateAccountNoticeTypeController(controller);
            extension.redirect();
          	system.assertEquals(System.Label.UpdateAccountNoticeType,extension.recordStatus);
        }
        Test.stopTest();
    }
    
    static testMethod void  checkPermissionForOthers() {
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testOfficeAssistantUser@asrtest.com'];
        
        Test.startTest();
        System.runAs(systemAdmin) {
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(new List<Account>());
          	UpdateAccountNoticeTypeController extension = new UpdateAccountNoticeTypeController(controller);
            extension.redirect();
          	system.assertEquals(System.Label.NoPermission,extension.recordStatus);
        }
        Test.stopTest();
    }

}