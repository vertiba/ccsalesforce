<apex:page docType="html-5.0" title="Notice to File: Mass Generation" standardController="Property__c" recordSetVar="properties" extensions="PropertyMassRequestDocumentsExtension" action="{!processSampleProperties}">

    <apex:slds />

    <script>
    window.onload=function() 
    { 
        generate(); 
    };
    function redirectToListView(recordStatus) {
        var resposne = recordStatus;           
        if (typeof sforce === 'undefined' ||  sforce === null || resposne.length >0) {
            sforce.one.back(true);  
            sforce.one.showToast({
                message : resposne,
                title   : 'Error!',
                type    : 'Error'
            });
        }
    }
     function showToast(){
         sforce.one.showToast({
                message : 'The requested documents have been submitted for generation.',
                title   : 'Success!',
                type    : 'success'
            });
     }
    </script>

    <div class="slds-scope">
        <apex:form >
            <div class="slds-page-header">
                <div class="slds-page-header__row">
                    <div class="slds-page-header__col-title">
                        <div class="slds-media">
                            <div class="slds-media__body">
                                <div class="slds-page-header__name">
                                    <div class="slds-page-header__name-title">
                                        <h1>
                                            <span>{!$ObjectType.Property__c.LabelPlural}</span>
                                            <span class="slds-page-header__title slds-truncate" title="Notice to File: Mass Generation">Notice to File: Mass Generation</span>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slds-page-header__col-actions">
                        <div class="slds-page-header__controls">
                            <div class="slds-page-header__control">
                                <ul class="slds-button-group-list">
                                    <li>
                                        <apex:actionFunction name="generate" action="{!processSampleProperties}" oncomplete="redirectToListView('{!recordStatus}');" reRender=""/>     
                                        <apex:commandButton action="{!generateNotices}" onclick="showToast();" value="Generate Notices" styleClass="slds-button slds-button_brand" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="slds-m-around_xx-large">
                <p class="slds-m-top_xx-large slds-text-color_success">Preview of new notices that will being generated, based on the selected list view: <strong>{!listView.label}</strong></p>
                <table class="slds-table slds-table_cell-buffer slds-table_bordered slds-table_striped slds-m-vertical_medium">
                    <tr>
                        <th scope="col" width="30%">Property Name</th>
                        <th scope="col" width="30%">Business Account Name</th>
                        <th scope="col" width="20%">Status</th>
                        <th scope="col" width="20%">Primary Form Type</th>
                        <th scope="col" width="20%">Mailing Address</th>
                    </tr>
                    <apex:repeat value="{!processedProperties}" var="property">
                        <tr>
                            <td scope="row">
                                <a href="{! '/' + property.Id}" target="_blank">{!property.Name}</a>
                            </td>
                            <td scope="row">
                                <a href="{! '/' + property.Account__c}" target="_blank">{!property.Account__r.Name}</a>
                            </td>
                            <td scope="row">{!property.Status__c}</td>
                            <td scope="row">{!property.PrimaryFormType__c}</td>
                            <td scope="row">
                                <apex:outputText value="{!property.MailingAddress__c}" escape="false" />
                            </td>
                        </tr>
                    </apex:repeat>
                </table>

                <p class="slds-m-top_xx-large slds-text-color_destructive">Preview of problematic records found due to missing or inaccurate information. No documents will be generated for these properties.</p>
                <table class="slds-table slds-table_cell-buffer slds-table_bordered slds-table_striped slds-m-vertical_medium">
                    <tr>
                        <th scope="col" width="30%">Property Name</th>
                        <th scope="col" width="30%">Business Account Name</th>
                        <th scope="col" width="20%">Primary Form Type</th> 
                        <th scope="col" width="20%">Record Type</th>                                      
                        <th scope="col" width="20%">Mailing Address</th>
                        <th scope="col" width="20%">Required To File</th>
                        <th scope="col" width="20%">Status</th>                        
                    </tr>
                    <apex:repeat value="{!problematicProperties}" var="property">
                        <tr>
                            <td scope="row">
                                <a href="{! '/' + property.Id}" target="_blank">{!property.Name}</a>
                            </td>
                            <td scope="row">
                                <a href="{! '/' + property.Account__c}" target="_blank">{!property.Account__r.Name}</a>
                            </td>
                            <td scope="row">{!property.PrimaryFormType__c}</td>
                            <td scope="row">{!property.RecordType.DeveloperName}</td>                           
                            <td scope="row">
                                <apex:outputText value="{!property.MailingAddress__c}" escape="false" />
                            </td>
                            <td scope="row">{!property.RequiredToFile__c}</td>
                            <td scope="row">{!property.Status__c}</td>                            
                        </tr>
                    </apex:repeat>
                </table>
            </div>
        </apex:form>
    </div>

</apex:page>