/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR: 7736 
// Utility function for Show Toast
// @description : This web component is generic web component can be used to showToast message in other components
//-----------------------------

import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

function showToast(cmp, title, message, variant, mode) {
    const event = new ShowToastEvent({
        title: title,
        message: message,
        variant: variant,
        mode: mode

    });
    cmp.dispatchEvent(event);
}

export {
    showToast
}