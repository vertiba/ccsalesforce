import { LightningElement } from 'lwc';
import currentUserId from '@salesforce/user/Id';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import createCase from '@salesforce/apex/GeneralEnquiryUpdatedController.createEnquiry';
import validateAddress from '@salesforce/apex/AddressEntryController.lookupAddress';

export default class GeneralEnquiryCaseCreation extends LightningElement {
    
    // Declaring all our variables.
    value;
    myUserId = currentUserId;
    replacementNotice = false;
    newBusiness = false;
    newVessel = false;
    otherSection = false;
    subjectFieldValue = '';
    descriptionFieldValue = '';
    accountNameValue = '';
    doingBusinessAsValue = '';
    vesselNameValue = '';
    vesselIdValue = '';
    vesselLocationValue = '';
    dateBusinessMovedValue = ''; 
    businessLocationStartDateValue = ''; 
    newBusinessLocationValue = '';
    contentDocumentRecordId = '';
    fileName = '';
    selectedType;
    disableButton = false;
    addressValue = '';
    zipCodeValue = '';
    searchResult = '';
    vesselOwnerValue = '';
    vesselIdTypeValue = '';
    fishGameBoatValue = '';

    // setting options for the combo-box
    get options() {
        return [
            { label: 'Request Replacement Notice', value: 'Request Replacement Notice' },
            { label: 'Request New Business', value: 'Request New Business' },
            { label: 'Request New Vessel', value: 'Request New Vessel' },
            { label: 'Other', value: 'Other' },
        ];
    }
    
    get vesselOptions() {
        return [
            { label: 'Coast Guard', value: 'Coast Guard' },
            { label: 'DMV#', value: 'DMV#' },
        ];
    }
    // Setting the accepted formats for the upload function
    get acceptedFormats() {
        return ['.pdf', '.png', '.jpeg', '.xlsx', '.xls', '.jpg', '.doc', '.docx'];
    }

    // Handling the comboxbox Change function, to render different sections.
    handleComboboxChange(event){
        let selectedValue = event.detail.value;
        this.selectedType = selectedValue;
        
        if(selectedValue == 'Request Replacement Notice'){
            this.genericBooleanFunction(true,false,false);
        }else if(selectedValue == 'Request New Business'){
            this.genericBooleanFunction(false,true,false);
        }else if(selectedValue == 'Request New Vessel'){
            this.genericBooleanFunction(false,false,true);
        }else if(selectedValue == 'Other'){
            this.genericBooleanFunction(false,false,false);
        }
    }

    // Handle change events for all the fields
    handleChange(event){
        var value = event.target.value;
        
        if(event.target.dataset.id === 'subjectField'){
            this.subjectFieldValue = value;
        }else if(event.target.dataset.id === 'descriptionField'){
            this.descriptionFieldValue = value;
        }else if(event.target.dataset.id === 'accountName'){
            this.accountNameValue = value;
        }else if(event.target.dataset.id === 'doingBusinessAs'){
            this.doingBusinessAsValue = value;
        }else if(event.target.dataset.id === 'vesselName'){
            this.vesselNameValue = value;
        }else if(event.target.dataset.id === 'vesselId'){
            this.vesselIdValue = value;
        }else if(event.target.dataset.id === 'vesselLocation'){
            this.vesselLocationValue = value;
        }else if(event.target.dataset.id === 'dateBusinessMoved'){
            this.dateBusinessMovedValue = value;
        }else if(event.target.dataset.id === 'addressInput'){
            this.addressValue = value;
        }else if(event.target.dataset.id === 'zipCode'){
            this.zipCodeValue = value;
        }else if(event.target.dataset.id === 'businessStartDate'){
            this.businessLocationStartDateValue = value;
        }else if(event.target.dataset.id === 'vesselOwner'){
            this.vesselOwnerValue = value;
        }else if(event.target.dataset.id === 'fishGameBoat'){
            this.fishGameBoatValue = value;
        }

    }

    // Handling the Vessel Type Combo-box change event
    handleVesselTypeChange(event){
        let selectedValue = event.detail.value;
        this.vesselIdTypeValue = selectedValue;
    }

    // Creating a general boolean function to handle the display of appropriate sections
    genericBooleanFunction(replacementNoticeAttribute,newBusinessAttribute,newVesselAttribute){
        this.replacementNotice = replacementNoticeAttribute;
        this.newBusiness = newBusinessAttribute;
        this.newVessel = newVesselAttribute;
        if (replacementNoticeAttribute == false && newBusinessAttribute == false && newVesselAttribute == false){
            this.otherSection = true;
        } else { this.otherSection = false;}
    }

    // Handle the update functionality.
    handleUploadFinished(event){
        var files = event.detail.files;
        this.fileName = files[0].name;
        this.contentDocumentRecordId = files[0].documentId;
    }

    // Set the New Business Address
    setValidationResult(){
        const resultValue = this.searchResult.Street +', '+this.searchResult.City +', '+this.searchResult.State +', '+this.searchResult.Zip5 +'-'+ this.searchResult.Zip4 ;
        this.newBusinessLocationValue = resultValue;
    }

    // Clear the searchResults
    clearResult(){
        this.searchResult = '';
    }

    // Handle Address Search
    handleSearch(){
        if(this.addressValue == '' || this.zipCodeValue == ''){
            this.toastFunction('Requied Fields Missing','Both Address, and Zip Code are required','warning','sticky');
        }else{
            validateAddress({street: this.addressValue, zipCode: this.zipCodeValue }).then(result => {
                console.log('result->',result);
                this.searchResult = result;
            }).catch(error => {
                console.log('error->',error);
                this.toastFunction('Unknown Error',error.body.message,'error','sticky');
            })
        }
    }

    // Submit Function
    handleSubmit(){
        if(this.selectedType == undefined){
            this.toastFunction('Requied Fields Missing','Please fill the mandatory fields before submitting','warning','sticky');
        }else{
            var input = this.template.querySelector(".businessMoved");
            this.disableButton = true;
        // Creating individual Sections based on type selection, and passing a JS object to the apex class to insert.
        if(this.selectedType == 'Request Replacement Notice'){
            if(this.accountNameValue == '' || this.doingBusinessAsValue == ''){
                    let displayMessage = 'Please ensure all required fields are filled before submitting.';
                    this.toastFunction('Requied Fields Missing',displayMessage,'warning','sticky'); 
                    this.disableButton = false;
            }else{
                const caseObject = {
                    accountName: this.accountNameValue,
                    subject: this.selectedType,
                    doingBusiness: this.doingBusinessAsValue,
                    contentDocumentId: this.contentDocumentRecordId,
                    selectedType: this.selectedType
                };

                createCase({ objectMap: JSON.stringify(caseObject) }).then(result => {
                    console.log('result-->',result)
                    const caseNumber = result.split('-')[1];
                    
                    if((result).includes('Case Created')){
                        let displayMessage = 'We have received your request. You can find your request '+caseNumber+' in Customer Service Cases under "My  Details" tab. We will review your request and proceed as needed. Thank you!';
                        this.toastFunction('Success',displayMessage,'success','dismissable ');     
                        this.disableButton = false;
                        this.callPageLoad();
                    }
                }).catch(error => {
                    console.log('error-->',error);
                    this.toastFunction('Error Creating Case',error.body.message,'error','sticky'); 
                });

            }
        }else if(this.selectedType == 'Request New Business'){
            var input = this.template.querySelector(".businessDate");
            if (this.accountNameValue == '' || this.doingBusinessAsValue == '' ||
                this.newBusinessLocationValue == '' || this.businessLocationStartDateValue == '') {
                let displayMessage = 'Please ensure all required fields are filled before submitting.';
                this.toastFunction('Requied Fields Missing', displayMessage, 'warning', 'sticky');
                this.disableButton = false;
            }else if(input.validity.valid == false) {
                let displayMessage = 'Please ensure the right date format is entered';
                this.toastFunction('Bad Date Format', displayMessage, 'error', 'sticky');
                this.disableButton = false;
            } else {
                const caseObject = {
                    accountName: this.accountNameValue,
                    subject: this.selectedType,
                    doingBusiness: this.doingBusinessAsValue,
                    contentDocumentId: this.contentDocumentRecordId,
                    selectedType: this.selectedType,
                    newBusinessLocation: this.newBusinessLocationValue,
                    businessStartDate: this.businessLocationStartDateValue
                };

                createCase({ objectMap: JSON.stringify(caseObject) }).then(result => {
                    console.log('result-->', result);
                    const caseNumber = result.split('-')[1];
                    if((result).includes('Case Created')){
                        let displayMessage = 'We have received your request. You can find your request, '+caseNumber+' in Customer Service Cases under "My  Details" tab. We will review your request and proceed as needed. Thank you!';
                        this.toastFunction('Success', displayMessage, 'success', 'dismissable ');
                        this.disableButton = false;
                        this.callPageLoad();
                    }
                }).catch(error => {
                    console.log('error-->', error);
                    this.toastFunction('Error Creating Case', error.body.message, 'error', 'sticky');
                });

            }

        }else if(this.selectedType == 'Request New Vessel'){
            var input = this.template.querySelector(".businessMoved");
            var inputVslID = this.template.querySelector(".cVesselID");
            if (this.vesselNameValue == '' ||
                this.vesselIdValue == '' || this.vesselLocationValue == '' || this.dateBusinessMovedValue == '' ||
                this.vesselIdTypeValue == '' || this.vesselOwnerValue == '') {
                let displayMessage = 'Please ensure all required fields are filled before submitting.';
                this.toastFunction('Requied Fields Missing', displayMessage, 'warning', 'sticky');
                this.disableButton = false;
            }else if(input.validity.valid == false) {
                let displayMessage = 'Please ensure the right date format is entered.';
                this.toastFunction('Bad Date Format', displayMessage, 'error', 'sticky');
                this.disableButton = false;
            }else if(inputVslID.validity.valid == false) {
                let displayMessage = 'Vessel ID can be 8 characters long. Special characters are not allowed.';
                this.toastFunction('Invalid Vessel ID', displayMessage, 'error', 'sticky');
                this.disableButton = false;
            }else {
                const caseObject = {
                    subject: this.selectedType,
                    vesselName: this.vesselNameValue,
                    vesselId: this.vesselIdValue,
                    vesselLocation: this.vesselLocationValue,
                    fishGameBoatNumber: this.fishGameBoatValue,
                    vesselIdType: this.vesselIdTypeValue,
                    vesselOwner: this.vesselOwnerValue,
                    dateBusinessMoved: this.dateBusinessMovedValue,
                    contentDocumentId: this.contentDocumentRecordId,
                    selectedType: this.selectedType
                };

                createCase({ objectMap: JSON.stringify(caseObject) }).then(result => {
                    console.log('result-->', result);
                    const caseNumber = result.split('-')[1];
                    if((result).includes('Case Created')){
                        let displayMessage = 'We have received your request. You can find your request '+caseNumber+' in Customer Service Cases under "My  Details" tab. We will review your request and proceed as needed. Thank you!';
                        this.toastFunction('Success',displayMessage,'success','dismissable ');   
                        this.disableButton = false;  
                        this.callPageLoad();
                    }
                }).catch(error => {
                    console.log('error-->', error);
                    this.toastFunction('Error Creating Case',error.body.message, 'error', 'sticky');
                });

            }
        }else if(this.selectedType == 'Other'){
            if (this.subjectFieldValue == '' || this.descriptionFieldValue == '') {
                let displayMessage = 'Please ensure all required fields are filled before submitting.';
                this.toastFunction('Requied Fields Missing', displayMessage, 'warning', 'sticky');
                this.disableButton = false;
            } else {
                const caseObject = {
                    subject: this.subjectFieldValue,
                    description: this.descriptionFieldValue,
                    contentDocumentId: this.contentDocumentRecordId,
                    selectedType: this.selectedType
                };

                createCase({ objectMap: JSON.stringify(caseObject) }).then(result => {
                    console.log('result-->', result);
                    const caseNumber = result.split('-')[1];
                    if((result).includes('Case Created')){
                        let displayMessage = 'We have received your request. You can find your request '+caseNumber+' in Customer Service Cases under "My  Details" tab. We will review your request and proceed as needed. Thank you!';
                        this.toastFunction('Success',displayMessage,'success','dismissable ');
                        this.disableButton = false;
                        this.callPageLoad();     
                    }
                }).catch(error => {
                    console.log('error-->', error);
                    this.toastFunction('Error Creating Case', error.body.message, 'error', 'sticky');
                });

            }
        }
        }
        
    }

    // Call page reload after 6 seconds, once a case is submitted
    callPageLoad(){
        setTimeout( () =>{
            location.reload();
        },6000);
    }

    // Generic Toast Function, for re-usability
    toastFunction(titleMessage, displayMessage, toastVariant, toastMode){
        this.dispatchEvent(
            new ShowToastEvent({
                title: titleMessage,
                message: displayMessage,
                variant: toastVariant,
                mode : toastMode
            })
        );
    }

    
}