/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR: 7736
// ASR:9438 - changed fileyear to assesment year.
// @description : This web component is used to allow taxpayer to Clone Statement and its child(StatementReportedAssets) and 
// grand child(ReportedAssetSchedule), so they can amend their current rollyear filing
// ASR-8605 - Included the logic to disable amendment when an another statement is in-progress.
//-----------------------------
import {
  LightningElement,
  api,
  wire,
  track
} from "lwc";
import {
  getRecord
} from "lightning/uiRecordApi";
// Navigation
import {
  NavigationMixin
} from "lightning/navigation";
//Apex Methods
import rollYearStatus from "@salesforce/apex/AmendStatementController.rollYearStatus";
import cloneStatement from "@salesforce/apex/AmendStatementController.cloneStatement";
import checkStatements from '@salesforce/apex/AmendStatementController.validateInProgressStatements';

// Generic Show Toast Utility Web Component
import {
  showToast
} from "c/showToastUtility";

import {
  reduceErrors
} from "c/errorHandlerUtility";
// labels
import errorCannotAmendStatement from '@salesforce/label/c.ErrorCannotAmendStatement';
import successAmendStatement from '@salesforce/label/c.SuccessAmendStatement';
import errorAmendRollYearClosed from '@salesforce/label/c.ErrorAmendRollYearClosed';
import errorAmendStatement from '@salesforce/label/c.ErrorAmendStatement';
import AmendFilingErrorMessage from '@salesforce/label/c.AmendFilingErrorMessage';

const FIELDS = [
  "Statement__c.Status__c",
  "Statement__c.AssessmentYear__c",
  "Statement__c.AlreadyAmended__c"
];

export default class AmendStatement extends NavigationMixin(LightningElement) {
  // properties

  label = {
      errorCannotAmendStatement,
      successAmendStatement,
      errorAmendStatement,
      errorAmendRollYearClosed,
      AmendFilingErrorMessage
  };

  @api recordId;
  @track statment;
  @track rollYear;
  @track newStatementRecord;
  assessmentYear;
  statementStatus;
  alreadyAmended;
  displayAmendStatement = false; // to display Amend Button
  disableAmendStatement = false; // to disabled Amend Button
  isCloning = false;
  cloneError;

  // Getting Statement Information Using LDS
  @wire(getRecord, {
    recordId: "$recordId",
    fields: FIELDS
  })
  wiredRecord({
    error,
    data
  }) {
    if (error) {
      let message = "Unknown error";
      if (Array.isArray(error.body)) {
        message = error.body.map((e) => e.message).join(", ");
      } else if (typeof error.body.message === "string") {
        message = error.body.message;
      }
      showToast(this, 'Unknown Error', message, 'error', 'pester');
    } else if (data) {
      this.statement = data;
      this.statementStatus = this.statement.fields.Status__c.value;
      this.assessmentYear = this.statement.fields.AssessmentYear__c.value;
      this.alreadyAmended = this.statement.fields.AlreadyAmended__c.value;
      // Show Amend Button if Statement Status is Submit
      //Bug -10428 - Status can be Submtted as well Processed
      if (this.statementStatus === "Submitted" || this.statementStatus === "Processed" ) {
        this.displayAmendStatement = true;
      }
    }
  }

  // Getting information of Roll year and storing in rollYear variable
  @wire(rollYearStatus)
  getrollYearInformation({
    error,
    data
  }) {
    if (error) {
      let message = "Unknown error";
      if (Array.isArray(error.body)) {
        message = error.body.map((e) => e.message).join(", ");
      } else if (typeof error.body.message === "string") {
        message = error.body.message;
      }
      showToast(this, 'Error', message, 'error', 'pester');
    } else if (data) {
      this.rollYear = data;
      // disabled the Amend button if Roll year is closed
      if (
        this.assessmentYear != this.rollYear.Year__c &&
        this.rollYear.Status__c === "Roll Closed"
      ) {
        this.disableAmendStatement = true;
      }
    }
  }

  // Clone Current Statement for Amendment
  cloneStatementForAmendment(event) {
      checkStatements({ statementId: this.recordId }).then(result => {
          const IN_PROGRESS_STATEMENTS_PRESENT = 'In Progress Statements present';
          const NO_INPROGRESS_STATEMENTS = 'No InProgress Statements';
    
          if (IN_PROGRESS_STATEMENTS_PRESENT.includes(result)) {
            showToast(this, 'Warning: Cannot Amend File', this.label.AmendFilingErrorMessage, 'warning', 'pester');
          } else if (NO_INPROGRESS_STATEMENTS.includes(result)) {
            this.cloneStatmentFunction();
          }
        }).catch(error => {    
          let errorMessage = reduceErrors(error);    
          showToast(this, 'Error while Amending Filing', errorMessage[0], 'warning', 'sticky');
        });
  }


  // Moving the clone functionality to separate Method.
  cloneStatmentFunction() {
    // Bug 10428 - statment status can be Submitted as well as Processed
      if (this.assessmentYear === this.rollYear.Year__c && this.statementStatus != "In Progress"  &&
          this.alreadyAmended === false) {
          this.isCloning = true;
          //server call
          cloneStatement({
              statementIdForCloning: this.recordId
          })
              .then((result) => {
                  this.newStatementRecord = result;
                  this.isCloning = false;
                  showToast(this, 'Success', this.label.successAmendStatement, 'success', 'pester');
                  // Navigation to new cloned statement
                  this[NavigationMixin.Navigate]({
                      type: "standard__recordPage",
                      attributes: {
                          objectApiName: "Statement__c",
                          recordId: result.Id,
                          actionName: "view"
                      }
                  });
                  this.cloneError = undefined;
              })
              .catch((error) => {
                  this.cloneError = error;
                  let errorMessage = reduceErrors(error);                  
                  this.isCloning = false;
                  showToast(this, 'Error while Amending Filing', errorMessage[0],'warning', 'sticky');
                  this.newStatementRecord = undefined;
              });
      } else {
          if (this.alreadyAmended === true) {
              // ASR-9438
              // Show message if they cannot amend the current statement
              showToast(this, 'Cannot Amend Filing', this.label.errorCannotAmendStatement, 'warning', 'sticky');
          } else if (this.assessmentYear != this.rollYear.Year__c) {
              // Show message roll year is closed
              showToast(this, 'Cannot Amend Filing', this.label.errorAmendRollYearClosed, 'warning', 'sticky');
          } else {
              // generic message 
        // generic message 
              // generic message 
              showToast(this, 'Cannot Amend Filing', this.label.errorAmendStatement, 'warning', 'sticky');
          }

      }
  }
}