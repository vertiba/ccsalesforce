/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @description : This class is used to show user correction records between StagingAumentum records, Account, Property .
//-----------------------------
import {
    LightningElement,
    api,
    wire,
    track
} from 'lwc';
// for record information
import {
    getRecord
} from 'lightning/uiRecordApi';
import {
    showToast
} from "c/showToastUtility";
//Apex Methods
import getChildTableValues from "@salesforce/apex/AumentumReviewProcessController.getChildTableValues";
import updateNewChanges from "@salesforce/apex/AumentumReviewProcessController.updateNewChanges";
import ignoreChanges from "@salesforce/apex/AumentumReviewProcessController.ignoreChanges";
//label
import successReviewProcessorMessage from '@salesforce/label/c.SuccessReviewProcessorMessage';
import ignoreReviewProcessorMessage from '@salesforce/label/c.IgnoreReviewProcessorMessage';
import noRecordReviewProcessorMessage from '@salesforce/label/c.NoRecordReviewProcessorMessage';
import noAccessMessage from '@salesforce/label/c.AumentumNoPermissionMessage';

// Custom permission - ASR -897
import hasPermission from '@salesforce/customPermission/AumentumProcessPermission'

// Error Handler Utility
import {
    reduceErrors
} from 'c/errorHandlerUtility';

const FIELDS = ['StagingAumentumBusiness__c.Status__c'];

export default class AumentumReviewProcessor extends LightningElement {
    @api recordId;
    @track stagingAumentumBusiness;
    @track stagingAumentumStatus;
    @track stagingCompareRecords;
    showComponent = true;
    showGenericMessage = false;
    @track changesForSaving;
    @track pageErrors;
    // Custom label
    label = {
        successReviewProcessorMessage,
        ignoreReviewProcessorMessage,
        noRecordReviewProcessorMessage,
        noAccessMessage
    }
    // ASR-8974- For Custom permission access 
    get accessPermission() {
        return hasPermission;
    }
    // Getting Staging Status Information Using LDS
    @wire(getRecord, {
        recordId: "$recordId",
        fields: FIELDS
    })
    wiredRecord({
        error,
        data
    }) {
        if (error) {
            let message = "Unknown Error";
            if (Array.isArray(error.body)) {
                message = error.body.map((e) => e.message).join(", ");
            } else if (typeof error.body.message === "string") {
                message = error.body.message;
            }
            showToast(this, 'Unknown Error', message, 'error', 'pester');
        } else if (data) {
            this.stagingAumentumBusiness = data;
            this.stagingAumentumStatus = this.stagingAumentumBusiness.fields.Status__c.value;
            if (this.stagingAumentumStatus === "Processed") {
                this.showGenericMessage = true;
                this.showComponent = false;
            }

        }
    }
    // On load get value of table for display
    connectedCallback() {
        getChildTableValues({
                parentRecordId: this.recordId
            })
            .then((data) => {
                this.stagingCompareRecords = JSON.parse(JSON.stringify(data));
                this.pageErrors = undefined;
            })
            .catch((error) => {
                this.pageErrors = error;
                showToast(this, 'Error while loading', error.message, 'sticky');
                this.stagingCompareRecords = undefined;
            });
    }
    // Saved checked option and send to Server
    saveChanges() {
        this.changesForSaving = [];
        let selectedRows = this.template.querySelectorAll('lightning-input');
        // based on selected row getting values of the changes
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                this.changesForSaving.push({
                    Name: selectedRows[i].value,
                    Id: selectedRows[i].dataset.id,
                    AccountId: selectedRows[i].dataset.accountid,
                    PropertyId: selectedRows[i].dataset.propertyid,
                    NewValue: selectedRows[i].dataset.integration,
                    ObjectName: selectedRows[i].dataset.objectname,
                    FieldAPI: selectedRows[i].dataset.fieldapi,
                    FieldStage: selectedRows[i].dataset.fieldstage,
                    ParentId: selectedRows[i].dataset.parentid
                })
            }
        }
        // Calling Server- Apex
        updateNewChanges({
                newChanges: JSON.stringify(this.changesForSaving)
            })
            .then((result) => {
                showToast(this, 'Success', 'Saved Changes successfully', 'success', 'pester');
                this.pageErrors = undefined;
                window.location.reload();
                if (this.stagingAumentumStatus === "Processed") {
                    this.showGenericMessage = true;
                    this.showComponent = false;
                }
            })
            .catch((error) => {
                this.pageErrors = error;
                let errorMessage = reduceErrors(error);
                showToast(this, 'Error occured while update', errorMessage[0],'error', 'sticky'); 
                this.showGenericMessage = false;
                this.showComponent = true;
            });
    }
    // Make checkbox checked
    storeChange(event) {
        let selectedRows = this.template.querySelector('lightning-input');
        for (let i = 0; i < selectedRows.length; i++) {
            console.log('selected' + selectedRows[i]);
            if (selectedRows[i].type === 'checkbox') {
                selectedRows[i].checked = event.target.checked;
            }
        }

    }
    // No Changes needed for record
    noChanges() {
        // Calling Server- Apex
        ignoreChanges({
                aumentumId: this.recordId
            })
            .then((result) => {
                showToast(this, 'Success', this.label.ignoreReviewProcessorMessage, 'info', 'pester');
                this.pageErrors = undefined;
                window.location.reload();
                if (this.stagingAumentumStatus === "Processed") {
                    this.showGenericMessage = true;
                    this.showComponent = false;
                }
            })
            .catch((error) => {
                this.pageErrors = error;
                let errorMessage = reduceErrors(error);
                showToast(this, 'Error occured while update', errorMessage[0],'error', 'sticky'); 
                this.showGenericMessage = false;
                this.showComponent = true;
            });
    }

}