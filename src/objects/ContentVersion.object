<?xml version="1.0" encoding="utf-8"?><CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>AIMSUploadDate__c</fullName>
        <description>This will be updated by integration layer to that date when document is sent to AIMS</description>
        <externalId>false</externalId>
        <label>AIMS Upload Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>AIMSUploadError__c</fullName>
        <description>This field will capture any error during upload, integration layer will use it to report error.</description>
        <externalId>false</externalId>
        <label>AIMS Upload Error</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AIMSUploadStatus__c</fullName>
        <description>This will capture integration status with AIMS/OnBase</description>
        <externalId>false</externalId>
        <label>AIMS Upload Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Ready to upload</fullName>
                    <default>false</default>
                    <label>Ready to upload</label>
                </value>
                <value>
                    <fullName>Received by Integration Layer</fullName>
                    <default>false</default>
                    <label>Received by Integration Layer</label>
                </value>
                <value>
                    <fullName>Failed</fullName>
                    <default>false</default>
                    <label>Failed</label>
                </value>
                <value>
                    <fullName>Sent</fullName>
                    <default>false</default>
                    <label>Sent</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ContentSize</fullName>
    </fields>
    <fields>
        <fullName>ConversationId__c</fullName>
        <description>Done as Part of 9382. Used in migration Track</description>
        <externalId>false</externalId>
        <label>ConversationId</label>
        <length>36</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>DocumentSetting__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Document Setting</label>
        <referenceTo>TH1__Document_Setting__c</referenceTo>
        <relationshipLabel>Content Versions</relationshipLabel>
        <relationshipName>Content_Versions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DocumentType__c</fullName>
        <description>This field will be set by salesforce to appropriate document type</description>
        <externalId>false</externalId>
        <label>Document Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>OnBaseDocumentType</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>DocumentUniqueId__c</fullName>
        <description>This will be set by salesforce to an appropriate unique id of document.</description>
        <displayFormat>CV-{0}</displayFormat>
        <externalId>false</externalId>
        <label>Document Unique Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>FileExtension</fullName>
    </fields>
    <fields>
        <fullName>FileType</fullName>
    </fields>
    <fields>
        <fullName>IsAssetEnabled</fullName>
    </fields>
    <fields>
        <fullName>Language</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>LastFailedAttempt__c</fullName>
        <externalId>false</externalId>
        <label>Last Failed Attempt</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>NumberOfFailedAttempts__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Number Of Failed Attempts</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OwnerId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SentToIntegrationLayer__c</fullName>
        <externalId>false</externalId>
        <label>Sent To Integration Layer</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>SharingOption</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>SharingPrivacy</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Created</fullName>
                    <default>true</default>
                    <label>Created</label>
                </value>
                <value>
                    <fullName>Sent To Integration Layer</fullName>
                    <default>false</default>
                    <label>Sent To Integration Layer</label>
                </value>
                <value>
                    <fullName>Sent To Repro</fullName>
                    <default>false</default>
                    <label>Sent To Repro</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>TagCsv</fullName>
    </fields>
    <fields>
        <fullName>Title</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fieldSets>
        <fullName>Track_Repro_Delivery</fullName>
        <description>Track Repro Delivery</description>
        <displayedFields>
            <field>Title</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>FileType</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Description</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>FileExtension</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>SentToIntegrationLayer__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Track Repro Delivery</label>
    </fieldSets>
    <searchLayouts>
        <searchResultsAdditionalFields>VERSION.TITLE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>VERSION.CONTENT_SIZE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>VERSION.FILE_EXTENSION</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>VERSION.LAST_UPDATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>VERSION.OWNER</searchResultsAdditionalFields>
    </searchLayouts>
</CustomObject>
