<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Signs, Cameras, TV Equipment, etc.</label>
    <protected>false</protected>
    <values>
        <field>PropertyCategory__c</field>
        <value xsi:type="xsd:string">Alternate Schedule A</value>
    </values>
    <values>
        <field>SubCategory__c</field>
        <value xsi:type="xsd:string">Signs, Cameras, TV Equipment, etc.</value>
    </values>
</CustomMetadata>
