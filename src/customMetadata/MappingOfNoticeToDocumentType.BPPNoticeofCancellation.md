<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BPP - Notice of Cancellation</label>
    <protected>false</protected>
    <values>
        <field>DocumentType__c</field>
        <value xsi:type="xsd:string">BPP-Outgoing-Adjustment-Notice</value>
    </values>
    <values>
        <field>TemplateName__c</field>
        <value xsi:type="xsd:string">Notice of Roll Correction-Cancellation,Notice of Roll Correction Cancellation - Direct</value>
    </values>
</CustomMetadata>
