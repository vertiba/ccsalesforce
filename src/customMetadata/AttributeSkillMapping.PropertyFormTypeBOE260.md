<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PropertyFormTypeBOE260</label>
    <protected>false</protected>
    <values>
        <field>Field__c</field>
        <value xsi:type="xsd:string">PropertyFormType__c</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Case</value>
    </values>
    <values>
        <field>SkillName__c</field>
        <value xsi:type="xsd:string">FormBOE260</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">BOE-260</value>
    </values>
</CustomMetadata>
