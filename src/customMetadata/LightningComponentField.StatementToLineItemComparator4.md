<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>StatementToLineItemComparator4</label>
    <protected>false</protected>
    <values>
        <field>API__c</field>
        <value xsi:type="xsd:string">ManualAdjustedCost__c</value>
    </values>
    <values>
        <field>ComponentName__c</field>
        <value xsi:type="xsd:string">StatementToLineItemComparator</value>
    </values>
    <values>
        <field>FieldType__c</field>
        <value xsi:type="xsd:string">Currency</value>
    </values>
    <values>
        <field>IsEditable__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsSavable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Manual Adjusted Cost</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
</CustomMetadata>
