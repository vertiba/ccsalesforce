<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LineItemToLineItemComparator3</label>
    <protected>false</protected>
    <values>
        <field>API__c</field>
        <value xsi:type="xsd:string">Cost__c</value>
    </values>
    <values>
        <field>ComponentName__c</field>
        <value xsi:type="xsd:string">LineItemToLineItemComparator</value>
    </values>
    <values>
        <field>FieldType__c</field>
        <value xsi:type="xsd:string">Currency</value>
    </values>
    <values>
        <field>IsEditable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsSavable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Last Year Cost</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
