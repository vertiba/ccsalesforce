<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Primary Form Type</label>
    <protected>false</protected>
    <values>
        <field>ExistingValue__c</field>
        <value xsi:type="xsd:string">MappedProperty__r.PrimaryFormType__c</value>
    </values>
    <values>
        <field>FieldAPIForUpdate__c</field>
        <value xsi:type="xsd:string">PrimaryFormType__c</value>
    </values>
    <values>
        <field>FieldAPIValueFrom__c</field>
        <value xsi:type="xsd:string">StagingPrimaryFormType__c</value>
    </values>
    <values>
        <field>FieldLabel__c</field>
        <value xsi:type="xsd:string">Primary Form Type</value>
    </values>
    <values>
        <field>IntegrationValue__c</field>
        <value xsi:type="xsd:string">StagingPrimaryFormType__c</value>
    </values>
    <values>
        <field>ObjectName__c</field>
        <value xsi:type="xsd:string">Property</value>
    </values>
</CustomMetadata>
