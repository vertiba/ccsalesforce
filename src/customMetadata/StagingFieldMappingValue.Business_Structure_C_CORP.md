<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Business Structure - C-CORP</label>
    <protected>false</protected>
    <values>
        <field>MappedFieldValue__c</field>
        <value xsi:type="xsd:string">Corporation</value>
    </values>
    <values>
        <field>MappedField__c</field>
        <value xsi:type="xsd:string">BusinessStructure__c</value>
    </values>
    <values>
        <field>MappedRecordLookupField__c</field>
        <value xsi:type="xsd:string">MappedAccount__c</value>
    </values>
    <values>
        <field>MappedSObject__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
    <values>
        <field>StagingFieldValue__c</field>
        <value xsi:type="xsd:string">C-CORP</value>
    </values>
    <values>
        <field>StagingField__c</field>
        <value xsi:type="xsd:string">BusinessStructure__c</value>
    </values>
    <values>
        <field>StagingSObject__c</field>
        <value xsi:type="xsd:string">StagingAumentumBusiness__c</value>
    </values>
</CustomMetadata>
