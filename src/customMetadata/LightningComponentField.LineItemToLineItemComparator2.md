<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LineItemToLineItemComparator2</label>
    <protected>false</protected>
    <values>
        <field>API__c</field>
        <value xsi:type="xsd:string">AssetSubclassification__c</value>
    </values>
    <values>
        <field>ComponentName__c</field>
        <value xsi:type="xsd:string">LineItemToLineItemComparator</value>
    </values>
    <values>
        <field>FieldType__c</field>
        <value xsi:type="xsd:string">Text</value>
    </values>
    <values>
        <field>IsEditable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsSavable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Asset Subclassification</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
</CustomMetadata>
