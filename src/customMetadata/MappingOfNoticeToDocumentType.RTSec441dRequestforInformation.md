<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>R&amp;T Sec 441(d) Request for Information</label>
    <protected>false</protected>
    <values>
        <field>DocumentType__c</field>
        <value xsi:type="xsd:string">BPP-Outgoing-Request-for-Information</value>
    </values>
    <values>
        <field>TemplateName__c</field>
        <value xsi:type="xsd:string">Request for Additional Information,Request for Additional Information - Taxpayer Req</value>
    </values>
</CustomMetadata>
