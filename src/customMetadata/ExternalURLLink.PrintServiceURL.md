<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Print Service URL</label>
    <protected>false</protected>
    <values>
        <field>ExternalURL__c</field>
        <value xsi:type="xsd:string">https://dev1-smart.sfgov.org/smart/print/smart/pdf/in</value>
    </values>
</CustomMetadata>
