<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Business Open Date</label>
    <protected>false</protected>
    <values>
        <field>ExistingValue__c</field>
        <value xsi:type="xsd:string">MappedBusiness__r.BusinessOpenDate__c</value>
    </values>
    <values>
        <field>FieldAPIForUpdate__c</field>
        <value xsi:type="xsd:string">BusinessOpenDate__c</value>
    </values>
    <values>
        <field>FieldAPIValueFrom__c</field>
        <value xsi:type="xsd:string">BusinessStartDate__c</value>
    </values>
    <values>
        <field>FieldLabel__c</field>
        <value xsi:type="xsd:string">Business Open Date</value>
    </values>
    <values>
        <field>IntegrationValue__c</field>
        <value xsi:type="xsd:string">ParentStagingAumentumBusiness__r.BusinessStartDate__c</value>
    </values>
    <values>
        <field>ObjectName__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
</CustomMetadata>
