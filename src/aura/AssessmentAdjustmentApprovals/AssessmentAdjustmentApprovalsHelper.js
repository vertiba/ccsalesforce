/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    getRecordsToDisplay: function (component) {
        var action = component.get("c.getAssessment");
        var actParams = {
            'recordId': component.get('v.recordId')
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results === null) {
                    component.set('v.showSpinner', false);
                    sforce.one.showToast({
                        "title": "Error",
                        "message": "Invalid data",
                        "type": "error"
                    });
                    sforce.console.getEnclosingTabId(
                        $A.getCallback(function (result) {
                            sforce.console.closeTab(result.id);
                        })
                    );
                } else if (results.errorMsg !== undefined) {
                    component.set('v.showSpinner', false);
                    sforce.one.showToast({
                        "message": results.errorMsg,
                        "type": "Warning"
                    });                    
                    sforce.console.getEnclosingTabId(
                        $A.getCallback(function (result) {
                            sforce.console.closeTab(result.id);
                        })
                    );

                } else if (results.caseRecord === undefined) {
                    component.set('v.showSpinner', false);
                    sforce.one.showToast({
                        "title": "Error",
                        "message": "Invalid data",
                        "type": "error"
                    });
                    sforce.console.getEnclosingTabId(
                        $A.getCallback(function (result) {
                            sforce.console.closeTab(result.id);
                        })
                    );
                } else {
                    component.set('v.dataToDisplay', results);
                    this.getSubTypePicklists(component);
                }
            } else {
                var errors = response.getError();
                component.set('v.showSpinner', false);
                sforce.one.showToast({
                    "title": "Error",
                    "message": JSON.stringify(errors[0].message),
                    "type": "error"
                });
                sforce.console.getEnclosingTabId(
                    $A.getCallback(function (result) {
                        sforce.console.closeTab(result.id);
                    })
                );
            }
        });
        $A.enqueueAction(action);
    },
    getAdjustmentReasonPicklists: function (component, event) {
        var action = component.get("c.fetchAdjustmentReason");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var adjustmentReason = [];
                for (var key in result) {
                    adjustmentReason.push({ key: key, value: result[key] });
                }
                component.set("v.adjustmentReason", adjustmentReason);
            }
            //  component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    getSubTypePicklists: function (component) {
        var assessmentDataWrapper = component.get("v.dataToDisplay");
        var typeValue = assessmentDataWrapper.caseRecord.Type;
        var recordTypeName = assessmentDataWrapper.caseRecord.RecordType.Name;
        var action = component.get("c.fetchSubType");
        action.setParams({
            'typeValue': typeValue,
            'recordTypeName': recordTypeName
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                var subType = [];
                if (!$A.util.isEmpty(results)) {
                    for (var key in results) {
                        subType.push({ key: key, value: results[key] });
                    }
                    component.set('v.showSpinner', false);

                }
                component.set("v.subType", subType);
            }
        });
        $A.enqueueAction(action);
    },
    doCancel: function (component) {
        var action = component.get("c.Cancel");
        var actParams = {
            'recordId': component.get('v.recordId')
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
            }
            else{
                var errors = response.getError();
                if(errors[0].message === $A.get("$Label.c.ErrorMessageForUser")){
                    component.set('v.showSpinner', false);
                    sforce.one.showToast({
                        "title": "Error",
                        "message": JSON.stringify(errors[0].message),
                        "type": "error"
                    });
                }
            }
            sforce.console.getEnclosingTabId(
                $A.getCallback(function (result) {
                    sforce.console.closeTab(result.id);
                })
            );
        });
        $A.enqueueAction(action);
    },
})