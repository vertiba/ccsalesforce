/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInitHelper : function(component) {
        var filters = component.get('v.filter');
        if(filters==='undefined' || filters===null){
            component.set('v.filter',{});
        }     
        this.loadData(component);
        var objectAttribute = component.get('v.objAttributeParam');
        component.set('v.jobName',objectAttribute.jobName);
        component.set('v.displayName',objectAttribute.displayName);
        component.set('v.jobId',objectAttribute.jobId);
        component.set('v.stageId',objectAttribute.stageId);
        component.set('v.batchId',objectAttribute.batchId);
        component.set('v.stageName',objectAttribute.stageName);
        component.set('v.selectedDate',objectAttribute.selectedDate);	
    },
    // to Handle Change in Date
    changeInDateHelper : function(component,event) {
        var filters= component.get('v.filter');
        filters['eventDate']= component.get('v.selectedDate');
        this.loadData(component);
    },
    // to handle Next navigation
    handleNextHelper : function(component,event) {
        var AttributeParam ={'paginationOffset' : component.get('v.paginationOffset')};
        AttributeParam['selectedDate']=component.get('v.selectedDate');
        AttributeParam['objectName']= component.get('v.objectName');
        AttributeParam['pageNumber']= component.get('v.pageNumber');
        this.handleNextLoad(component, AttributeParam);
    },
    // to handle Previous navigation
    handlePreviousHelper: function(component,event) {
        var AttributeParam ={'paginationOffset' : component.get('v.paginationOffset')};
        AttributeParam['selectedDate']=component.get('v.selectedDate');
        AttributeParam['objectName']= component.get('v.objectName');
        AttributeParam['pageNumber']= component.get('v.pageNumber');
        this.handlePreviousLoad(component, AttributeParam); 
    }
})