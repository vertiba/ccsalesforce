/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    getFieldsAndNewObject: function (component, event, helper) {
        var comparatorType = component.get("v.comparatorType");
        var acqYear = component.get("v.acqYear");
        component.set("v.defaultAcqYear",acqYear);
        var action = component.get("c.getLightningComponentFields");
        if ('LineItemToLineItemComparator' === comparatorType ||
            'StatementToLineItemComparator' === comparatorType) {
            action.setParam('comparatorType', 'CreateAssessmentLineItem');
            component.set('v.objectLabel', 'Assessment Line Item');
        } else if ('LeasedLineItemToStatementComparator' === comparatorType) {
            action.setParam('comparatorType', 'CreateLeasedStatementReportedAsset');
            component.set('v.objectLabel', 'Statement Reported Asset');
            component.set('v.assetClassificationType', 'Leased Equipment');
        } else if("CreateReportedAssetSchedule" === comparatorType) {
            action.setParam('comparatorType', comparatorType);
            component.set('v.objectLabel', 'Reported Asset Schedule');
        } else {
            action.setParam('comparatorType', 'CreateStatementReportedAsset');
            component.set('v.objectLabel', 'Statement Reported Asset');
        }
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var fields = response.getReturnValue();
                var fieldsArray = [];
                for (var index in fields) {
                    var metadata = fields[index];
                    fieldsArray.push(metadata.API__c);
                }
                //To show Asset Type for Part 2 New Statement Reported Assest Creation pop-up on commnunity
                if (component.get("v.objectAPI") === 'StatementReportedAsset__c' &&
                    component.get('v.formType') === '571-LA' && component.get("v.comparatorType") != 'LeasedLineItemToStatementComparator') {
                    fieldsArray.splice(1, 0, 'AssetType__c');
                }
                //To show Leased Asset Type for Part 3 New Statement Reported Assest Creation pop-up commnunity
                if(component.get("v.objectAPI") === 'StatementReportedAsset__c' && component.get('v.formType').includes('571')) {
                    if ('LeasedLineItemToStatementComparator' === component.get("v.comparatorType")) {                        
                        fieldsArray.splice(1, 0, 'LeasedAssetType__c');
                    }
                }
                component.set('v.apiFields', fieldsArray);
            } else {
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                this.showToast(component, 'ERROR', message, 'sticky', 'error');
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    showToast: function (component, title, messageToShow, mode, typeofMsg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": mode,
            "type": typeofMsg,
            "title": title,
            duration:' 3000',
            mode: 'pester',
            message: messageToShow
        });
        toastEvent.fire();
    },
    closeModal: function (component) {
        var componentEvent = component.getEvent("closeModal");
        componentEvent.fire();
    }
})