/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    // loading the component
    doInitHelper : function(component) {
        var filters = component.get('v.filter');
        if(filters==='undefined' || filters===null){
            component.set('v.filter',{});
        }
        component.set('v.selectedDate',$A.localizationService.formatDate(new Date(), "YYYY-MM-DD"));
        this.loadData(component);
        // to store value coming from other component 
        var objAttributeParam = component.get("v.objAttributeParam");
        component.set("v.selectedStatus" , objAttributeParam.status);
        component.set("v.selectedDate", objAttributeParam.selectedDate);
        component.set("v.jobId",objAttributeParam.jobId);
        component.set("v.displayName",objAttributeParam.displayName);
        component.set("v.jobName", objAttributeParam.jobName);
        component.set("v.batchId",objAttributeParam.batchId);
    },    
    // to load the Errors
    displayExceptionsHelper : function(component, event) {
        var row = event.currentTarget.title;
        var jobName = component.get('v.jobName');
        var filters = component.get('v.filter');
        var batchId = component.get('v.batchId');
        var displayName = component.get('v.displayName');
        var stageName = event.currentTarget.getAttribute('data-stageName');
        var jobId = component.get('v.jobId');
        var urlpath= 'failed';
        filters['batchId']= batchId;
        filters['batchStageId']= row;
        filters['eventDate']= component.get('v.selectedDate');
        filters['paginationOffset']= component.get('v.paginationOffset');
        filters['jobId']= jobId;
        filters['jobName']= jobName;
        filters['filterCriteria']={
            status : component.get('v.selectedStatus')
        }
        // to Pass values to ConversationExceptionLog
        var objAttributes = {'jobName' :jobName };
        objAttributes['displayName'] = displayName;
        objAttributes['batchId']= batchId;
        objAttributes['jobId']= jobId;
        objAttributes['stageId']= row;
        objAttributes['stageName']= stageName;
        objAttributes['status'] = component.get('v.selectedStatus');
        objAttributes['selectedDate'] = component.get('v.selectedDate');        
        this.gotoComponent(component, 'ConversationExceptionLog', filters, urlpath, objAttributes);
        
    },
    // to handle status changes
    statusChangeHelper : function(component, event) {
        var status = component.find("selectedJobStatus").get('v.value');
        var fil = component.get('v.filter');
        fil['filterCriteria'] = {
            status : status
        };
        this.loadData(component);
    },
    // to handle date changes 
    changeInDateHelper : function(component, event) {
        var selectedDate= component.get("v.selectedDate");
        var jobId = component.get("v.jobId");
        var batchId =component.get("v.batchId");
        var filters = component.get('v.filter');
        filters['jobId']= jobId ;
        filters['eventDate']= selectedDate;
        component.set('v.objectName','batch/'+batchId+'/stages');
        this.loadData(component);
    },
    // to handle navigation to Next Button
    handleNextHelper : function(component, event) {
        var AttributeParam ={'paginationOffset' : component.get('v.paginationOffset')};
        AttributeParam['status']= component.get('v.selectedStatus');
        AttributeParam['selectedDate']=component.get('v.selectedDate');
        AttributeParam['objectName']= component.get('v.objectName');
        AttributeParam['pageNumber']= component.get('v.pageNumber');
        this.handleNextLoad(component, AttributeParam);
    },
    // to handle navigation to Previous Button
    handlePreviousHelper : function(component, event) {
        var AttributeParam ={'paginationOffset' : component.get('v.paginationOffset')};
        AttributeParam['status']= component.get('v.selectedStatus');
        AttributeParam['selectedDate']=component.get('v.selectedDate');
        AttributeParam['objectName']= component.get('v.objectName');
        AttributeParam['pageNumber']= component.get('v.pageNumber');
        this.handlePreviousLoad(component, AttributeParam);	
    },
     // handle replay action
    replayAction : function(component,event){
        var jobId = component.get('v.jobId');
		var stageId = event.currentTarget.getAttribute('data-jobStageId');
		var batchId = event.currentTarget.getAttribute('data-batchId');
        component.set('v.filter',{});
        var filters = component.get('v.filter');
        filters['jobId']= jobId;
		filters['jobStageId']= stageId;
		filters['batchId']= batchId;
 		var urlpath ='replayJobStage';		
        component.set('v.objectName', urlpath);
        this.loadData(component);
    }
})