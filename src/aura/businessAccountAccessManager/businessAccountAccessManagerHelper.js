/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    validateEntityAndPin: function (component, event, helper) {
        var action = component.get('c.validateEntityIdAndPin');
        action.setParams({
            entityId : component.get('v.entityId'),
            pin      : component.get('v.pin')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var message = response.getReturnValue();
                if(message === 'Agent Pin Matched') {
                    // show file upload field
                    $A.util.removeClass(component.find('fileDiv'),'slds-hide');
                    component.set('v.IsAgentPinValidationPass',true);
                } else if(message === 'Access Pin Matched') {
                    helper.submitWithAccessPIN(component, event, helper);
                }
            } else if(state === 'ERROR') {
                // Hide file upload field
                $A.util.addClass(component.find('fileDiv'),'slds-hide');
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                if(errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    message : message,
                    mode    : 'sticky',
                    title   : 'Error!',
                    type    : 'error'
                });
                toastEvent.fire();
            }
    	});
        $A.enqueueAction(action);
    },

    submitWithAccessPIN: function (component, event, helper) {
        var action = component.get('c.submitWithAccessPIN');
        action.setParams({
            accessPIN: component.get('v.pin'),
            entityId: component.get('v.entityId')
        });
        action.setCallback(this, function (response) {
            if(response.getState() === 'SUCCESS') {
                var businessAccount = response.getReturnValue();
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    message : 'Found your business/account: ' + businessAccount.Name,
                    title   : 'Success!',
                    type    : 'success'
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();

                var navEvt = $A.get('e.force:navigateToSObject');
                navEvt.setParams({
                    recordId: businessAccount.Id
                });
               // Bug - 9783 - Reloading the Page to refersh the List View Component on Page
               window.location.reload();
            } else {
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if(errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;// + '\n' + errors[0].stackTrace;
                }
                // Display the message
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    message : message,
                    mode    : 'sticky',
                    title   : 'Error!',
                    type    : 'error'
                });
                toastEvent.fire();
            }
        });

        $A.enqueueAction(action);
    },

    save : function(component, event, helper) {
        component.set('v.spinner', true);
        $A.util.addClass(component.find('fileDiv'),'slds-hide');
        var fileInput = component.get('v.uploadedFiles')[0];
        for (var i = 0; i < fileInput.length; i++) {
        var file = fileInput[i];
        helper.readFile(component, file);
        }
    },

    readFile: function(component, file) {
        var fr = new FileReader();
        var self = this;
        var entityId = component.get('v.entityId');
        var agentPinToSend= component.get('v.pin');
       	fr.onload = function() {
            var fileContents = fr.result;
    	    var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
			fileContents = fileContents.substring(dataStart);
        	self.upload(component, ban, agentPinToSend, file, fileContents);
        };
		fr.readAsDataURL(file);
    },

    upload: function(component, entityId, agentPinToSend, file, fileContents) {
        var action = component.get('c.saveTheFile');
        action.setParams({
            agentPIN    : agentPinToSend,
            base64Data  : encodeURIComponent(fileContents),
            contentType : file.type,
            entityId    : entityId,
            fileName    : file.name
        });
		action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS') {
                component.set('v.spinner', false);
                var businessAccount = response.getReturnValue();
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    message : 'Found your business account: ' + businessAccount.Name,
                    title   : 'Success!',
                    type    : 'success'
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();

                var navEvt = $A.get('e.force:navigateToSObject');
                navEvt.setParams({
                    recordId: businessAccount.Id
                });
            } else {
                component.set('v.spinner', false);
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                if(errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;// + '\n' + errors[0].stackTrace;
                }
                // Display the message
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    message : message,
                    mode    : 'sticky',
                    title   : 'Error!',
                    type    : 'error'
                });
                toastEvent.fire();
            }

        });
        $A.enqueueAction(action);
    },
})