/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    enableModal: function (component, event, helper) {
        component.set('v.showModal', true);
    },

    /*handleFilesChange : function (component, event, helper) {
        var fileName = 'No File Selected..';
        component.set('v.UploadedfileCount', event.getSource().get('v.files').length);
        if (event.getSource().get('v.files').length > 0) {
            component.set('v.uploadedFiles', event.getSource().get('v.files'));
            component.set('v.fileName', event.getSource().get('v.files').length+' File selected');
            component.set('v.IsFileSelected',true);
        }
    },*/

    submit: function (component, event, helper) {
        if(component.get('v.IsAgentPinValidationPass') === false) {
            helper.validateEntityAndPin(component, event, helper);
        } else if(component.get('v.IsFileSelected') === true) {
            helper.save(component, event, helper);
        }
    },

})