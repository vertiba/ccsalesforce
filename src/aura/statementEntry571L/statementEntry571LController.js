/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        component.find("recordData").reloadRecord(true);
        component.set('v.currentYear', new Date().getFullYear());
        component.set('v.showAccountingAddressEntry', false);
        component.set('v.showMailingAddressEntry', false);
        helper.returnPropertyRecordType(component);
        //ASR-7869 - calling this method to get Statement Reported Assets and their Child Reported Asset Schdeules Records to show on Schedule-D
        helper.retrieveStatementReportedAssetsData(component, event);
        helper.retrieveTaxPayerInformation(component, event);
        helper.retrieveDefaultAcquistionYear(component, event);
    },
  
    back : function(component, event, helper) {
        helper.backHelper(component);
    },
    
    //ASR-8469 - this function get called when user clicks on Print PDF
    printClick : function(component, event, helper) {
         component.set('v.buttonlabel','printClick');
         if(component.get("v.record.Status__c") == "In Progress") {
         helper.setRequired(component,'bussinessType',false);
         helper.setRequired(component,'businessSubtype',false);
         helper.setRequired(component,'rollCode',false);
         helper.setRequired(component,'businessPhone',false);
         helper.setRequired(component,'contactEmailAddress',false);
         helper.setRequired(component,'ownLandatBusinessLocation',false);
         helper.setRequired(component,'nameonDeedRecordedasShown',false);
         helper.setRequired(component,'startofBusinessatthisLocation',false);
         helper.setRequired(component,'accountingContactName',false);
         helper.setRequired(component,'accountingContactNumber',false);
         helper.setRequired(component,'controllingInterest',false);
         helper.setRequired(component,'realPropertyAtTimeOfAcquisition',false);
         helper.setRequired(component,'signature',false);
         helper.setRequired(component,'dateofSignature',false);
         helper.setRequired(component,'assesseeorAuthorizedAgentName',false);
         helper.setRequired(component,'assesseeorAuthorizedAgentTitle',false);
         helper.setRequired(component,'legalEntityName',false);
         helper.setRequired(component,'preparersName',false);
         helper.setRequired(component,'preparersTitle',false);
         helper.setRequired(component,'preparersPhoneNumber',false);
         helper.setRequired(component,'signatureBio',false);
         helper.setRequired(component,'dateofSignatureBio',false);
         helper.setRequired(component,'assesseeorAuthorizedAgentNameBio',false);
         helper.setRequired(component,'assesseeorAuthorizedAgentTitleBio',false);
         helper.setRequired(component,'legalEntityNameBio',false);
         helper.setRequired(component,'preparersNameBio',false);
         helper.setRequired(component,'preparersTitleBio',false);
         helper.setRequired(component,'preparersPhoneNumberBio',false);
         } else {
            event.preventDefault();
            helper.print(component, event);
        }
    },
    
    //ASR-8469 - this function get called when user clicks on next
    nextClick : function(component, event, helper) {
         component.set('v.buttonlabel','nextClick');
         if(component.get("v.record.Status__c") == "In Progress") {        
         helper.setRequired(component,'bussinessType',true);
         helper.setRequired(component,'businessSubtype',true);
         helper.setRequired(component,'rollCode',true);
         helper.setRequired(component,'businessPhone',true);
         helper.setRequired(component,'contactEmailAddress',true);
         helper.setRequired(component,'ownLandatBusinessLocation',true);
         helper.setRequired(component,'nameonDeedRecordedasShown',true);
         helper.setRequired(component,'startofBusinessatthisLocation',true);
         helper.setRequired(component,'accountingContactName',true);
         helper.setRequired(component,'accountingContactNumber',true);
         helper.setRequired(component,'controllingInterest',true);
         helper.setRequired(component,'realPropertyAtTimeOfAcquisition',true);
         helper.setRequired(component,'signature',true);
         helper.setRequired(component,'dateofSignature',true);
         helper.setRequired(component,'assesseeorAuthorizedAgentName',true);
         helper.setRequired(component,'assesseeorAuthorizedAgentTitle',true);
         helper.setRequired(component,'legalEntityName',true);
         helper.setRequired(component,'preparersName',true);
         helper.setRequired(component,'preparersTitle',true);
         helper.setRequired(component,'preparersPhoneNumber',true);
         helper.setRequired(component,'signatureBio',true);
         helper.setRequired(component,'dateofSignatureBio',true);
         helper.setRequired(component,'assesseeorAuthorizedAgentNameBio',true);
         helper.setRequired(component,'assesseeorAuthorizedAgentTitleBio',true);
         helper.setRequired(component,'legalEntityNameBio',true);
         helper.setRequired(component,'preparersNameBio',true);
         helper.setRequired(component,'preparersTitleBio',true);
         helper.setRequired(component,'preparersPhoneNumberBio',true);
        } else {
            event.preventDefault();
            helper.nextHelper(component,event);
        }
    },
    
    next : function(component, event, helper) {
        var buttonval = event.target.value;
        if (!$A.util.isUndefinedOrNull(buttonval) && buttonval === 'next') {
            helper.nextHelper(component, event);
        }
        else if(component.get("v.record.Status__c") == "In Progress") {
            var button = component.get('v.buttonlabel');   
            if(button === 'printClick'){
                helper.print(component, event);
            }else {
                helper.nextHelper(component, event);
            }
        }
        else{
            var button = component.get('v.buttonlabel');   
            if(button === 'printClick'){
                helper.print(component, event);
            }else {
                helper.nextHelper(component, event);
            }
        }
    },
    
    success:function(component, event, helper) {
        var button = component.get('v.buttonlabel');   
        if(button === 'printClick'){
            helper.print(component, event);
            component.set('v.buttonlabel', '');
        }else if(button === 'nextClick'){
            component.find('recordData').reloadRecord(true);
        	helper.showToast('Success!','success','Statement successfully submitted','sticky');
        	component.set('v.currentStep', 1);
        	component.set('v.disableBack', true);
        	component.set('v.disableNext', false);
            component.set('v.underSubmition',false);
       		//This method will redirect the user to the page based on boolean value check.
        	helper.returnOnSubmitRedirection(component, event);
        }
        
    },
    handleFormError: function (component, event) {
        component.set("v.showSpinner", false);
        component.set("v.underSubmition",false);
        var errorResult = JSON.parse(JSON.stringify(event.getParams()));
        console.log('error ->'+errorResult);
    },
    editMailingAddress : function(component, event, helper) {
        component.set('v.showMailingAddressEntry', true);
    },
    editAccountingAddress : function(component,event,helper){
        component.set('v.showAccountingAddressEntry',true);
    },
    submit : function(component, event, helper) {
        
        var button = component.get('v.buttonlabel');
        
        if(button === 'nextClick'){
            event.preventDefault();
            helper.submitHelper(component,event);
        } else if(component.get("v.record.Status__c") !== "In Progress" && button == 'printClick') {
            //final page print PDF button click will call this code 
            event.preventDefault();
             helper.print(component, event);
        } else if(component.get("v.record.Status__c") == "In Progress" && button == 'printClick') {
            helper.submitPrintHelper(component,event);
        }
         
        
    },
    fileUploaderHandler: function(component,event,helper)
    {  
        helper.fileUploader(component,event);       
    },
    reloadTotalRelatedData : function(component, event, helper){
        component.find('recordData').reloadRecord(true);
        helper.retrieveStatementReportedAssetsData(component, event);
    },
    handleAddressEvent : function(component, event){
        
        var addressType = event.getParam("addressType");
        var addressSaved = event.getParam("isSaved");
        var addressRemoved = event.getParam("addressRemoved");
        if(addressType === 'Accounting') {
            if(!addressRemoved) {
            	component.set('v.isAccountingAddressSaved',addressSaved);
            }
            else {
                component.set('v.isAccountingAddressSaved',false);
            }
        }
        else if(addressType === 'Mailing') {
           if(!addressRemoved) {
            	component.set('v.isMailingAddressSaved',addressSaved);
            }
            else {
                component.set('v.isMailingAddressSaved',false);
            } 
        }
        component.find("recordData").reloadRecord(true);
    },
    handleLoad : function(component){
        component.set("v.showSpinner", true);
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.showSpinner", false);
            }), 1000
        );
    }
})