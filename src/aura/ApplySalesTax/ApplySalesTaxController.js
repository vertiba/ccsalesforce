/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	doInit : function(component, event, helper) {
        console.log('doInit started for ApplySalesTax');
        component.set('v.showSpinner', true);
        helper.getRelatedAssessmentLineItems(component);
        component.set('v.showSpinner', false);
	},

    updateSalesTax : function(component, event, helper) {
        component.set('v.showSpinner', true);
        var assessmentLineItems = component.get("v.assessmentLineItems");
        if(!$A.util.isEmpty(assessmentLineItems) && !$A.util.isUndefined(assessmentLineItems)){
            var action = component.get("c.applySalesTaxAction");
            var assessmentLnItemRecords = JSON.stringify(assessmentLineItems);
            action.setParams({assessmentLineItemRecords : assessmentLnItemRecords});
            action.setCallback(this,function(a){
                var state = a.getState();
                if(state == "SUCCESS"){
                    helper.showToast('Success','success','Sales Tax applied successfully');
                    component.set('v.showSpinner', false);
					//$A.get("e.force:closeQuickAction").fire();
					component.getEvent("closeModal").fire();
                } else if(state == "ERROR"){
                    helper.showToast('Error','error','Sales Tax not applied successfully, Please try after some time.');
                    component.set('v.showSpinner', false);
                }
            });
            $A.enqueueAction(action);
        }

    }

})