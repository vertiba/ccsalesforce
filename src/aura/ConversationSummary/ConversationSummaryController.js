/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
		component.set('v.data',[]);
        component.set('v.selectedDate',$A.localizationService.formatDate(new Date(), "YYYY-MM-DD"));
        helper.doInitHelper(component);       
	},
    displayJobDetails: function (component, event, helper) {
        helper.displayJobDetailsHelper(component, event);      
    },
    displayExceptions: function (component, event, helper) {
        helper.displayExceptionsHelper(component, event);       
    },
    statusChange : function(component,event,helper){
        component.set('v.data',[]);
        helper.statusChangeHelper(component,event);        
    },
    changeInDate :function (component,event,helper){
        component.set('v.data',[]);
        helper.changeInDateHelper(component,event);
    },
    handleNext : function(component,event, helper){
        helper.handleNextHelper(component,event);
    },    
    handlePrevious : function(component,event, helper){
        helper.handlePreviousHelper(component,event);
	}
})