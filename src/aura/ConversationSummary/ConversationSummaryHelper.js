/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    // Helper to Load Summary Component
    doInitHelper : function(component) {
       
        var filters = component.get('v.filter');
        if($A.util.isEmpty(filters)){
            filters['paginationOffset']=component.get('v.paginationOffset'); 
            filters['filterCriteria']=  {
                status : component.get('v.selectedStatus')
            };
        }
        component.set('v.filter',filters);
        component.set('v.objectName','summary/'+ component.get('v.selectedDate'));
        this.loadData(component);
    },
    // To display Job Details 
    displayJobDetailsHelper : function(component, event) {
        var jobName = event.currentTarget.title;
        var jobId = event.currentTarget.getAttribute('data-jobId');
        var displayName = event.currentTarget.getAttribute('data-displayName');
        var urlpath ='job/' +jobName +'/date/' + component.get('v.selectedDate');
        component.set('v.objectName', urlpath);
        var filters = component.get('v.filter');
        filters['jobId']= jobId;
        filters['paginationOffset']= component.get('v.paginationOffset');
        filters['filterCriteria']=  {
            status : component.get('v.selectedStatus')
        };
        var objAttributes = {'jobName' :jobName };
        objAttributes['displayName']= displayName;
        objAttributes['jobId']= jobId;
        objAttributes['status'] = component.get('v.selectedStatus');
        objAttributes['selectedDate'] = component.get('v.selectedDate');        
        this.gotoComponent(component, 'ConversationBatch', filters, urlpath, objAttributes); 
    },
    // to display the Failure Occured on Job Summary Level
    displayExceptionsHelper: function(component, event) {
        var row = event.currentTarget.getAttribute('data-jobId');
        var jobName = event.currentTarget.getAttribute('data-jobName');
        var displayName = event.currentTarget.getAttribute('data-displayName');
        var urlpath='failed';
        var filters = component.get('v.filter');
        filters['jobId']=row;
        filters['eventDate']= component.get('v.selectedDate');
        filters['jobName']= jobName;
        var objAttributes = {'jobName' :jobName };
        objAttributes['displayName']= displayName;
        objAttributes['batchId']= '';
        objAttributes['jobId']= row;
        objAttributes['stageId']= '';
        objAttributes['stageName']= '';
        objAttributes['status'] = component.get('v.selectedStatus');
        objAttributes['selectedDate'] = component.get('v.selectedDate');
        this.gotoComponent(component, 'ConversationExceptionLog', filters, urlpath, objAttributes);
    },
    // To handle status changes
    statusChangeHelper : function(component,event) {
        var status = component.find("selectedJobStatus").get('v.value');
        var selectedDate= component.get("v.selectedDate");
        var filters = component.get('v.filter');
        filters['filterCriteria'] = {
            status : status
        };
        component.set("v.selectedStatus",status);
        this.loadData(component);
    },
    // To handle date changes
    changeInDateHelper : function(component,event) {
        var selectedDate= component.get("v.selectedDate");
        component.set('v.objectName','summary/'+ component.get('v.selectedDate'));
        this.loadData(component);
    },
    // to navigate to next page if records are more than 10 records
    handleNextHelper : function(component,event) {
        var AttributeParam ={'paginationOffset' : component.get('v.paginationOffset')};
        AttributeParam['status']= component.get('v.selectedStatus');
        AttributeParam['selectedDate']=component.get('v.selectedDate');
        AttributeParam['objectName']= component.get('v.objectName');
        AttributeParam['pageNumber']= component.get('v.pageNumber');
        this.handleNextLoad(component, AttributeParam);
    },
    // to navigate to previous page
    handlePreviousHelper : function(component,event) {
        var AttributeParam ={'paginationOffset' : component.get('v.paginationOffset')};
        AttributeParam['status']= component.get('v.selectedStatus');
        AttributeParam['selectedDate']=component.get('v.selectedDate');
        AttributeParam['objectName']= component.get('v.objectName');
        AttributeParam['pageNumber']= component.get('v.pageNumber');
        this.handlePreviousLoad(component, AttributeParam);
    }
    
})