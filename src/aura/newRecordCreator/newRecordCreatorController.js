/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	doInit : function(component, event, helper) {

        var pageReference = component.get('v.pageReference');
        var recordTypeId = pageReference.state.recordTypeId;

        //var pageRef = cmp.get("v.pageReference");
        var state = pageReference.state; // state holds any query params
        var base64Context = pageReference.state ? state.inContextOfRef : null;
        console.log('base64Context=' + base64Context);
        // For some reason, the string starts with "1.", if somebody knows why,
        // this solution could be better generalized.
        if(base64Context && base64Context.startsWith("1\.")) {
            base64Context = base64Context.substring(2);
        }

        //salesforce passes parent record id in state if new record creation
        //is initiated from list view button
        var addressableContext = JSON.parse(window.atob(base64Context));
		console.log('addressableContext:');
		console.log(addressableContext);
		var accountId;

        //will look for account record id and validate initial context as Account
		if(addressableContext && addressableContext.attributes)// && addressableContext.attributes.objectApiName == 'Account')
		{
            accountId = addressableContext.attributes.recordId;
		}
        console.log('accountId=' + accountId);

		var createRecordEvent = $A.get('e.force:createRecord');
        createRecordEvent.setParams({
            'entityApiName': component.get('v.sObjectName'),
            'defaultFieldValues': {
                'Name' : 'New Record',
                'RecordTypeId' : recordTypeId,
				'Account__c' : accountId        //set Account lookup field to in state Account record Id
            },
            'recordTypeId':recordTypeId
        });
        createRecordEvent.fire();
	}
})