/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    downloadForm: function (component, event, helper) {
        var downloadLink = component.get('v.downloadLink');
        console.log('opening download link:' + downloadLink);
        window.open(downloadLink, '_blank');
    }
})