/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    getFieldSetFields : function(component,helper) {
        console.log('helper method');
        var action = component.get("c.getFieldSetFields");
        action.setParams({ 
            objectName      : 'ContentVersion' ,
            fieldSetName    : 'Track_Repro_Delivery' 
        });
        //Setting the Callback
            action.setCallback(this,function(a){
                //get the response state
                var state = a.getState();
                //check if result is successfull
                if(state == "SUCCESS"){
                    var result = a.getReturnValue();
                    if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                        component.set("v.fieldSetMembers",result);
                        helper.getRelatedContentVersions(component,helper);
                    }
                } else if(state == "ERROR"){
                    helper.showToast('Error','error','There is some issue in fetching details, Please try after some time.'); 
                }
            });
            
            //adds the server-side action to the queue        
            $A.enqueueAction(action);
    },

    getRelatedContentVersions : function(component,helper) {
        console.log('helper method2');
        var action = component.get("c.getAllContentVersionRecords");
        action.setParams({ 
            objectName      : 'ContentVersion' ,
            fieldSetName    : 'Track_Repro_Delivery' 
        });
        //Setting the Callback
            action.setCallback(this,function(a){
                //get the response state
                var state = a.getState();
                //check if result is successfull
                if(state == "SUCCESS"){
                    var result = a.getReturnValue();
                    if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                        component.set("v.contentVersions",result);
                    }
                } else if(state == "ERROR"){
                    helper.showToast('Error','error','There is some issue in fetching details, Please try after some time.'); 
                }
            });
            
            //adds the server-side action to the queue        
            $A.enqueueAction(action);
    },

    showToast : function(title,type,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title, 
            "message": message,
            "type":type
        });
        toastEvent.fire();
    }
})