({
    doInit : function(component, event, helper) {
        var recordId               = component.get('v.recordId');
        var vipFormWizardUrlPrefix = component.get('v.vipFormWizardUrlPrefix');

        console.log('vipFormWizardUrlPrefix=' + vipFormWizardUrlPrefix);
        if(vipFormWizardUrlPrefix == null) vipFormWizardUrlPrefix = '';

        var vipFormWizardUrl       = vipFormWizardUrlPrefix + '/apex/VIPForm__VIP_FormWizard?'
            + 'id=' + recordId
            + '&sfdcIFrameOrigin=' + window.location.origin
            + '&tour=&isdtp=p1&clc=0&sfdcIFrameHost=web';
        console.log('vipFormWizardUrl=' + vipFormWizardUrl);
        component.set('v.vipFormWizardUrl', vipFormWizardUrl);
    }
})