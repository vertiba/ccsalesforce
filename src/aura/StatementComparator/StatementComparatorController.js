/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit: function (component, event, helper) {
        //On page refresh, or on save, reset the field
        component.set('v.unSavedChangesExist', false);
        component.set('v.showSpinner', true);
        component.set("v.filter", '');
        component.set("v.isSearchable", false);
        var defaultAcqYear = component.get('v.defaultAcquistionYear');    
        var comparatorName = component.get('v.comparatorType');
        if ((comparatorName === 'StatementToStatementComparator' ||
             comparatorName === 'LineItemToStatementComparator')) {
            if ($A.util.isEmpty(component.get('v.formType'))) {
                helper.getFormType(component);
            }
        } else if (comparatorName === 'StatementToLineItemComparator' ||
                   comparatorName === 'LineItemToLineItemComparator') {
            component.set("v.isSearchable", true);
        }
        helper.getFieldsToDisplay(component);
        helper.getRecordsToDisplay(component);
        helper.assignstatementReportedAssetsRecordTypes(component);
    },
    saveRecords: function (component, event, helper) {
        var records = component.get('v.dataToDisplay');
        var recordsToSave = [];
        for (var index in records) {
            var record = records[index];
            if (record.isChanged || record.toBeDeleted) {
                //identify records that are changed and send only them.
                recordsToSave.push(record);
            }
        }
        if (recordsToSave.length > 0) {
            component.set('v.showSpinner', true);
            helper.saveRecords(component, recordsToSave);
        }
    },
    createNewRecord: function (component, event, helper) {
        component.set('v.showModal', true);
        var defaultAcquistionYear = component.get("v.defaultAcquistionYear");
        var objectApi = 'StatementReportedAsset__c';
        var recordTypeId = component.get("v.recordTypeIdOwnedAssets");
        if (component.get("v.comparatorType") === 'LineItemToLineItemComparator' ||
            component.get("v.comparatorType") === 'StatementToLineItemComparator') {
            objectApi = 'AssessmentLineItem__c';
        } else if (component.get("v.comparatorType") === 'LeasedLineItemToStatementComparator') {
            recordTypeId = component.get("v.recordTypeIdLeasedAssets");
        }
        var componentAttributes = {
            "comparatorType": component.get("v.comparatorType"),
            "parentId": component.get("v.recordId"),
            "isCommunityPage": component.get("v.isCommunityPage"),
            "objectAPI": objectApi,
            "recordTypeId": recordTypeId,
            "formType": component.get("v.formType"),
            "acqYear": defaultAcquistionYear
        };
        $A.createComponent(
            "c:CreateNewLineItem",
            componentAttributes,
            function (createRecordModal, status, errorMessage) {
                if (status === "SUCCESS") {
                    var targetCmp = component.find('recordCreatorModal');
                    var body = targetCmp.get("v.body");
                    body.push(createRecordModal);
                    targetCmp.set("v.body", body);
                    component.set('v.showModal', true);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    closeModal: function (component, event, helper) {
        helper.getRecordsToDisplay(component);
        var divComponent = component.find('recordCreatorModal');
        divComponent.set("v.body", []);
        component.get('v.showModal', false);
    },
    filterData: function (component, event, helper) {
        helper.filterRecords(component);
    },
    handleCertifyLineItems: function (component, event, helper) {
        component.set('v.showSpinner', true);
        var noLineItems = component.get("v.noLineItems");
        var comparatorName = component.get('v.comparatorType');
        var certifiedNoAssetLineItems = component.find("CertifyLineItems").get("v.checked");
        component.set("v.certifyLineItems",certifiedNoAssetLineItems);
        var certifyLineItems = component.get("v.certifyLineItems");
        var statementId = component.get("v.recordId");
        if (comparatorName === 'LineItemToStatementComparator') {
            if (noLineItems && certifyLineItems) {
                //ASR-1712 - if no line items are there on part II and user make CertifyNoLineItems checknox true, 
                //save the value on statement and allow user to go next screen
                helper.updateStatement(component,statementId,certifyLineItems,'certifiedNoAssetLineItems');
                component.set("v.unSavedChangesExist", false);
            } else if(noLineItems && !certifyLineItems) {
                //ASR-1712 - if no line items are there on part II and user make CertifyNoLineItems checknox false, 
                //save the value on statement and don't allow user to go next screen
                helper.updateStatement(component,statementId,certifyLineItems,'certifiedNoAssetLineItems')
                component.set("v.unSavedChangesExist", true)
            }
        }
    },
    handleCertifyNoPropertyBelongingToOthers: function (component, event, helper) {
        //ASR-10039 - show the NoPropertyBelongingToOthers checkbox on part-III and allow to user make this checknox true, 
        //save the value on statement and allow user to go next screen
        component.set('v.showSpinner', true);
        var certifiedNoPropertyBelongingToOthers = component.find("CertifyNoPropertyBelongingToOthers").get("v.checked");
        console.log('certifiedNoPropertyBelongingToOthers+++'+certifiedNoPropertyBelongingToOthers);
        var statementId = component.get("v.recordId");
        helper.updateStatement(component,statementId,certifiedNoPropertyBelongingToOthers,'noPropertyBelongingtoOthers');
    }
})