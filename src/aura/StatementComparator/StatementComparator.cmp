<!--Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d-->
<aura:component controller="StatementComparatorController" implements="force:appHostable,force:hasRecordId,force:lightningQuickAction,flexipage:availableForAllPageTypes,forceCommunity:availableForAllPageTypes" access="global">
    <!--aura attributes-->
    <aura:attribute name="dataToDisplay" type="Object[]" description="List of records to display"/>
    <aura:attribute name="comparatorType" type="String" description="Controller uses this to identify what records to be returned."/>
    <aura:attribute name="fields" type="LightningComponentField__mdt[]" description="List of fields to display" />
    <aura:attribute name="showModal" type="boolean" default="false" />
    <aura:attribute name="readOnlyRecord" type="boolean" default="true" description="By default, record is readonly and dont display buttons" />
    <aura:attribute name="unSavedChangesExist" type="boolean" default="false" />
    <aura:attribute name="isCommunityPage" type="boolean" default="false" />
    <aura:attribute name="tableStyle" type="String" />
    <aura:attribute name="headerStyle" type="String" />
    <aura:attribute name="columnStyle" type="String" />
    <aura:attribute name="formType" type="String" />
    <aura:attribute name="recordTypeIdOwnedAssets" type="String" description="to store statement reported assest record type for part 2" />
    <aura:attribute name="recordTypeIdLeasedAssets" type="String" description="to store statement reported assest record type for part 2" />
    <aura:attribute access="private" name="showSpinner" type="Boolean" default="true" />
    <aura:attribute name="filter" type="String" description="search filter keyword which will be used for search on the ui" />
    <aura:attribute name="filterData" type="List" description="list of all records matching, with the search keyword" default="[]" />
    <aura:attribute name="matchRecords" type="List" description="list of all records matching, with the search keyword" default="[]" />
    <aura:attribute name="listAllData" type="List" description="list of all records" default="[]" />
    <aura:attribute name="isSearchable" type="boolean" default="false" description="Used to enable/disable search filter." />
    <aura:attribute name="certifyLineItems" type="boolean" default="false" description="To certify user action of not having any line items"/>
    <aura:attribute name="noLineItems" type="boolean" default="false" description="To check if any line items exits or not"/>
    <aura:attribute name="showCertifyLineItemsCb" type="boolean" default="true" description="To show/hide certifyLineItems cb"/>
    <aura:attribute name="statementRecordTypeId" type="String" />
    <aura:attribute name="statementStatus" type="String" />
    <aura:attribute name="certifyNoPropertyBelongingtoOthers" type="boolean" default="false" description="To certify no property belonging to others"/>
    <aura:attribute name="defaultAcquistionYear" access="public" type="String" default="2020"/>
    <aura:attribute access="private" name="processed" type="String" default="Processed" />
    <aura:attribute access="private" name="submitted" type="String" default="Submitted" />
    <aura:registerEvent name="refreshDataEvent" type="c:RefreshDataEvent" />
    <!--aura handlers-->
    <aura:handler name="init" value="{! this }" action="{! c.doInit }" />
    <aura:handler event="force:refreshView" action="{!c.doInit}" />
    <aura:handler name="closeModal" event="c:NewLineItemCreatorEvent" action="{!c.closeModal}" />
    
    <aura:if isTrue="{!v.showModal}">
        <div aura:id="recordCreatorModal" />
    </aura:if>
    <aura:if isTrue="{!v.showSpinner}">
        <lightning:spinner />
    </aura:if>
    <div>
        <aura:if isTrue="{! !v.readOnlyRecord}">
            <lightning:buttonGroup>
                <lightning:button label="Cancel" onclick="{! c.doInit }" />
                <lightning:button label="Save" onclick="{! c.saveRecords }" />
                <lightning:button label="New" onclick="{! c.createNewRecord }" />
            </lightning:buttonGroup>
            <br/>
            <aura:if isTrue="{!or(v.formType == '571-L - Leasing',v.formType == '571-L - Billboard')}">
                <aura:if isTrue="{! v.comparatorType == 'LineItemToStatementComparator'}">
                    <br/>
                    {!$Label.c.PartIIBillboardorLeasingUploadVerbage}
                    <br/>
                </aura:if>
            </aura:if>
            <c:fileUploader recordId="{!v.recordId}" /> <br/>
        </aura:if>
        <!--ASR-1712 Changes Start - Show static text and certify line items checkbox for Part II of 571-L-->
        <aura:if isTrue="{! v.comparatorType == 'LineItemToStatementComparator'}">
            <div class="slds-text-heading_xsmall">{!$Label.c.StatementComparatorVerbagePart1}</div>
            <aura:if isTrue="{!v.showCertifyLineItemsCb}">
                <br /> 
                <div style="float:left;font-size: 20px;"><abbr class="slds-required" title="required">* </abbr> </div>
                <div style="float:left;">      
                    <lightning:input aura:id="CertifyLineItems" type="checkbox" checked="{!v.certifyLineItems}"
                                     label="{!$Label.c.StatementComparatorVerbagePart2}"
                                     onchange="{!c.handleCertifyLineItems}" disabled="{!or(v.statementStatus == v.submitted, v.statementStatus == v.processed)}"/>
                </div>
            </aura:if>
        </aura:if>
        <!--ASR-1712 Changes End-->
        <aura:if isTrue="{!v.isSearchable}">
            <div class="slds-clearfix slds-m-around_small">
                <div class="slds-clearfix">
                    <div class="slds-float_right slds-align_absolute-center">
                        <lightning:input aura:id="Filter" value="{!v.filter}" name="Filter" placeholder="Filter"
                                         onchange="{!c.filterData}" type="search" />
                    </div>
                </div>
            </div>
        </aura:if>
        <table class="slds-table slds-table_bordered slds-table_col-bordered slds-table_fixed-layout slds-table_striped"
               role="grid" style="{!v.tableStyle}">
            <thead>
                <tr class="slds-line-height_reset">
                    <aura:if isTrue="{! v.comparatorType == 'StatementToLineItemComparator'}">
                        <aura:if isTrue="{! !v.readOnlyRecord}">
                            <th aria-label="Delete" scope="row" class="deleteHeader" style="{!v.headerStyle}">
                                <div class="slds-grid slds-grid_vertical-align-center ">
                                    <span style="white-space:pre-wrap" title="Delete">Delete</span>
                                </div>
                            </th>
                        </aura:if>
                    </aura:if>
                    <aura:iteration items="{!v.fields}" var="field">
                        <th aria-label="{!field.Label__c}" class="" scope="col" style="{!v.headerStyle}">
                            <div class="slds-grid slds-grid_vertical-align-center ">
                                <span style="white-space:pre-wrap" title="{!field.Label__c}">{!field.Label__c}</span>
                            </div>
                        </th>
                    </aura:iteration>
                </tr>
            </thead>
            <tbody>
                <aura:iteration items="{!v.dataToDisplay}" var="record">
                    <tr class="{! record.Style +' slds-hint-parent'}" aria-selected="false">
                        <aura:if isTrue="{! v.comparatorType == 'StatementToLineItemComparator'}">
                            <aura:if isTrue="{! !v.readOnlyRecord}">
                                <td>
                                    <ui:inputCheckbox value="{!record.toBeDeleted}" disabled="{! record.readRecord != null}" />
                                </td>
                            </aura:if>
                        </aura:if>
                        <aura:iteration items="{!v.fields}" var="field">
                            <aura:if isTrue="{! v.comparatorType == 'LineItemToStatementComparator'}">
                                <c:StatementLineItem recordValuesByLabels="{!record}" fieldLabel="{!field.Label__c}"
                                                     unSavedChangesExist="{!v.unSavedChangesExist}" columnStyle="{!v.columnStyle}"
                                                     isEditable="{!field.IsEditable__c}" fieldType="{!field.FieldType__c}" />
                                <aura:set attribute="else">
                                    <aura:if isTrue="{!not(empty(record.changedColumnNames))}">
                                        <aura:iteration items="{!record.changedColumnNames}" var="cc">
                                            <c:auraIfContains items="{!cc}" element="{!field.Label__c}">
                                                <c:StatementLineItem recordValuesByLabels="{!record}" 
                                                                     fieldLabel="{!field.Label__c}"
                                                                     unSavedChangesExist="{!v.unSavedChangesExist}" 
                                                                     columnStyle="{!v.columnStyle}"
                                                                     isEditable="{!field.IsEditable__c}" 
                                                                     isColumnEdited="true"
                                                                     fieldType="{!field.FieldType__c}" />
                                            </c:auraIfContains>
                                        </aura:iteration>
                                        <aura:if isTrue="{!not(empty(record.nonchangedColumnNames))}">
                                            <aura:iteration items="{!record.nonchangedColumnNames}" var="ncc">
                                                <c:auraIfContains items="{!ncc}" element="{!field.Label__c}">
                                                    <c:StatementLineItem recordValuesByLabels="{!record}" 
                                                                         fieldLabel="{!field.Label__c}"
                                                                         unSavedChangesExist="{!v.unSavedChangesExist}" 
                                                                         columnStyle="{!v.columnStyle}"
                                                                         isEditable="{!field.IsEditable__c}" 
                                                                         fieldType="{!field.FieldType__c}" />
                                                </c:auraIfContains>
                                            </aura:iteration>
                                        </aura:if>
                                        <aura:set attribute="else">
                                            <aura:if isTrue="{!not(empty(record.nonchangedColumnNames))}">
                                                <aura:iteration items="{!record.nonchangedColumnNames}" var="ncc">
                                                    <c:auraIfContains items="{!ncc}" element="{!field.Label__c}">
                                                        <c:StatementLineItem recordValuesByLabels="{!record}" 
                                                                             fieldLabel="{!field.Label__c}"
                                                                             unSavedChangesExist="{!v.unSavedChangesExist}"
                                                                             columnStyle="{!v.columnStyle}" 
                                                                             isEditable="{!field.IsEditable__c}"
                                                                             fieldType="{!field.FieldType__c}" />
                                                    </c:auraIfContains>
                                                </aura:iteration>
                                            </aura:if>
                                        </aura:set>
                                    </aura:if>
                                </aura:set>
                            </aura:if>
                        </aura:iteration>
                    </tr>
                </aura:iteration>
            </tbody>
        </table>
        <aura:if isTrue="{! !v.readOnlyRecord}">
            <lightning:buttonGroup>
                <lightning:button label="Cancel" onclick="{!c.doInit}" />
                <lightning:button label="Save" onclick="{!c.saveRecords}" />
                <lightning:button label="New" onclick="{!c.createNewRecord}" />
            </lightning:buttonGroup>
        </aura:if>
        <br/>
        <aura:if isTrue="{! v.comparatorType == 'LeasedLineItemToStatementComparator'}">
                <br /><br />
                <div style="float:left;">      
                    <lightning:input aura:id="CertifyNoPropertyBelongingToOthers" type="checkbox" checked="{!v.certifyNoPropertyBelongingtoOthers}"
                                     label="{!$Label.c.StatementComparatorVerbagePart3}"
                                     onchange="{!c.handleCertifyNoPropertyBelongingToOthers}" disabled="{!or(v.statementStatus == v.submitted, v.statementStatus == v.processed)}"/>
                </div>
        </aura:if>
    </div>
</aura:component>