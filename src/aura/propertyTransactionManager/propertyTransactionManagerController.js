/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    handlePropertyRecordUpdated : function(component, event, helper) {
        var action = component.get('c.getCurrentOwners');
        var params = {
            propertyId : component.get('v.recordId')
        };

        action.setParams(params);
        action.setCallback(this, function(a) {
            if(a.getState() === 'SUCCESS') {
                var results = a.getReturnValue();
                component.set('v.currentOwners', results);
            } else {
                console.log('error');
            }
        });

        $A.enqueueAction(action);
    },
    selectSellers : function(component, event, helper) {
        component.set('v.stepName', 'selectSellers');
    },
    selectBuyers : function(component, event, helper) {
        component.set('v.stepName', 'selectBuyers');
    },
    search : function(component, event, helper) {
        var action = component.get('c.searchAccounts');
        var params = {
            propertyId : component.get('v.recordId'),
            searchTerm : component.get('v.searchTerm')
        };

        action.setParams(params);
        action.setCallback(this, function(a) {
            if(a.getState() === 'SUCCESS') {
                var results = a.getReturnValue();
                component.set('v.searchResults', results);
            } else {
                console.log('error');
            }
        });

        $A.enqueueAction(action);
    },
    save : function(component, event, helper) {
        var transaction = component.get('v.transaction');
        transaction.Property__c = component.get('v.recordId');

        var action = component.get('c.saveData');
        var params = {
            propertyTransaction : transaction,
            sellers             : component.get('v.currentOwners'),
            buyers              : component.get('v.searchResults')
        };

        action.setParams(params);
        action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS') {
                //alert('success, id=' + a.getReturnValue().Id);
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    title   : 'Success!',
                    type    : 'success',
                    message : 'Transaction has been added successfully.'
                });
                toastEvent.fire();
                //$A.get('e.force:closeQuickAction').fire();
                //$A.get('e.force:refreshView').fire();
                var navEvt = $A.get('e.force:navigateToSObject');
                navEvt.setParams({
                    recordId : response.getReturnValue().Id
                });
                navEvt.fire();
            } else {
                console.log('error');
                //alert(response.getError());
                console.log(response.getError());
                //alert('An error has occurred');

let errors = response.getError();
let message = 'Unknown error'; // Default error message
// Retrieve the error message sent by the server
if (errors && Array.isArray(errors) && errors.length > 0) {
    message = errors[0].message + '\n' + errors[0].stackTrace;
}
// Display the message
console.error(message);


                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    mode    : 'sticky',
                    title   : 'Error!',
                    type    : 'error',
                    message : message
                });
                toastEvent.fire();
            }
        });

        $A.enqueueAction(action);
    }
})