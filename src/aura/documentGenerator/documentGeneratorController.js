/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        var action = component.get('c.getAvailableTemplates');
        /* var params = {
            recordId : component.get('v.recordId')
        }; */
        action.setParams({
            recordId : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS') {
                var results = response.getReturnValue();

                component.set('v.availableTemplates', results);
            } else {
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if(errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message + '\n' + errors[0].stackTrace;
                }
				this.errorShowToast('Error!','error',message);
            }
        });

        $A.enqueueAction(action);
    },
    createDocumentRequest : function (component, event, helper) {
        //Calling checkMailingAddress() to verify the Mailing Address field value
		helper.checkMailingAddress(component, event, helper);
      
    }
})