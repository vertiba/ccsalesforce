/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    checkMailingAddress : function(component, event, helper) {
        var action = component.get('c.getMailingAddress');
        action.setParams({
            recordId : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var retval = response.getReturnValue();
            if(state === 'SUCCESS' && retval == true) {
                var action = component.get('c.generateDocument');
                action.setParams({
                    documentTemplateId : component.get('v.selectedTemplateId'),
                    recordId           : component.get('v.recordId')
                });
                action.setCallback(this, function(response) {
                    if(response.getState() === 'SUCCESS') {
                        this.showToast('Success!','success','Your document will be generated shortly.');
                        $A.get('e.force:closeQuickAction').fire();
                        $A.get('e.force:refreshView').fire();
                    } else {
                        let errors = response.getError();
                        let message = 'Unknown error'; 
                        if(errors && Array.isArray(errors) && errors.length > 0) {
                            message = errors[0].message + '\n' + errors[0].stackTrace;
                        }
                        this.showToast('Error!','error',message);
                    }
                });
                
                $A.enqueueAction(action);
            } else if(state === 'SUCCESS' && retval == false) {
                this.showToast('Error!','error','Mailing Address value is missing for the current record.');
            }else{
                console.log('error');
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(title,type,message) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            message : message,
            mode    : 'sticky',
            title   : title,
            type    : type
        });
        toastEvent.fire();
    }
})