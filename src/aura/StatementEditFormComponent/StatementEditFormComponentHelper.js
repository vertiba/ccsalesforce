({
    checkRecordTypeAccess : function(component, event, helper) {
        var action = component.get("c.getStatementRecordType");
        action.setParams({ statementId : component.get("v.recordId")});
        action.setCallback(this, $A.getCallback(function (response){
            var state = response.getState();
            var retval = response.getReturnValue();
            var recordTypeCheck = retval.RecordType.DeveloperName;
            if (state === 'SUCCESS'){
                if(recordTypeCheck === 'BusinessPersonalPropertyStatement' || recordTypeCheck === 'VesselStatement'){
                    component.set("v.propertyEventRecordTypeFlag",false);
                    component.set("v.openModal", true);
                }else{
                    component.set("v.openModal", true);
                }
                
            }else {
                console.log('error');
            }
        }));
        $A.enqueueAction(action);
        
    }

})