({
    doInit : function(component, event, helper) {
        var recordId               = component.get('v.recordId');
        var vipFormCertificateUrlPrefix = component.get('v.vipFormCertificateUrlPrefix');

        console.log('vipFormCertificateUrlPrefix=' + vipFormCertificateUrlPrefix);
        if(vipFormCertificateUrlPrefix == null) vipFormCertificateUrlPrefix = '';

        var vipFormCertificateUrl       = vipFormCertificateUrlPrefix + '/sfassessor/CertificateConfirmation?recordId=' + recordId;
        console.log('vipFormCertificateUrl=' + vipFormCertificateUrl);
        component.set('v.vipFormCertificateUrl', vipFormCertificateUrl);
    }
})