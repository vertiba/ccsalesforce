/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        var fieldLabel = component.get("v.fieldLabel");
        var recordValuesByLabels = component.get("v.recordValuesByLabels");
        if(!$A.util.isUndefined(recordValuesByLabels) && !$A.util.isEmpty(recordValuesByLabels)) {
            if(!$A.util.isUndefined(recordValuesByLabels[fieldLabel]) && !$A.util.isEmpty(recordValuesByLabels[fieldLabel])) {
                component.set("v.value" , recordValuesByLabels[fieldLabel]);
            }
        }
    },
    inlineEdit: function(component, event, helper) {
        component.set("v.editMode", true);
        setTimeout(function(){ 
            component.find("inputId").focus();
        }, 100);
    },
    checkFieldType: function(component, event, helper){
        var newValue = event.getSource().get("v.value");
        var fieldType = component.get('v.fieldType');
        if(fieldType==='Double' || fieldType==='Currency'){
            if($A.util.isEmpty(newValue)){
                component.find('inputId').set("v.value", null);
            } else if(isNaN(newValue)) {
                var recordValuesByLabels = component.get("v.recordValuesByLabels");
                var fieldLabel = component.get('v.fieldLabel');
                component.find('inputId').set("v.value", recordValuesByLabels[fieldLabel]); 
            }
        }
    },
    closeBox: function(component, event, helper) {
        var recordValuesByLabels = component.get("v.recordValuesByLabels");
        var fieldLabel = component.get('v.fieldLabel');
        var oldValue = recordValuesByLabels[fieldLabel];
        var newValue = event.getSource().get("v.value");
        var fieldType = component.get('v.fieldType');
        if(oldValue !== newValue && fieldLabel != undefined) {
            component.set("v.unSavedChangesExist", true);
            component.set("v.isColumnEdited",true);
            recordValuesByLabels['isChanged']=true;
            recordValuesByLabels[fieldLabel]=newValue;                           
            if(!recordValuesByLabels['changedColumnNames'].includes(fieldLabel)) { 
                recordValuesByLabels['changedColumnNames'].push(fieldLabel);     
            }
            if(recordValuesByLabels['nonchangedColumnNames'].includes(fieldLabel)) {
                var arr = recordValuesByLabels['nonchangedColumnNames'];
                var idx = arr.indexOf(fieldLabel);
                if (idx != -1) arr.splice(idx, 1);
            }           
        }
        component.set("v.editMode", false);        
    }
})