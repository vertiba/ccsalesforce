/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {        
        component.set('v.data',[]);
        // Setting up page reference for tab
        var pageReference = component.get("v.pageReference");
        component.set("v.objectName", pageReference.state.objectName);
        component.set("v.objAttributeParam", pageReference.state.objAttributeParam);
        component.set("v.filter", pageReference.state.filter); 
        helper.doInitHelper(component); 
        
    },
    
    displayBatchDetails: function (component, event, helper) {
        helper.displayBatchDetailsHelper(component, event);
        
    },
    displayExceptions: function (component, event, helper) {
        helper.displayExceptionsHelper(component, event);       
    },
    statusChange : function (component, event, helper) {
        component.set('v.data',[]);
        helper.statusChangeHelper(component, event);        
    },
    changeInDate :function (component,event,helper){
        component.set("v.data",[]);
        helper.changeInDateHelper(component, event);           
    },    
    handleNext : function(component,event, helper){
        helper.handleNextHelper(component, event);       
    },    
    handlePrevious : function(component,event, helper){
        helper.handlePreviousHelper(component, event);       
    },
    openFailurePopup : function(component,event, helper){
        var failureMessage= event.currentTarget.getAttribute('data-failureMessage');
        helper.showToast(component, 'Failure Message', failureMessage, 'sticky', 'warning');        
    }
})