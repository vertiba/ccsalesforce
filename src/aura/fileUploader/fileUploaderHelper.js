/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    handleUploadFinished : function(component,event)
    {
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name; 	
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message":uploadedFiles.length + " File(s) have been uploaded successfully."
        });
        toastEvent.fire();
        if(fileName != undefined){
            var fileEvent = $A.get("e.c:fileUploaderEvent");
            console.log('recordType'+ component.get("v.recordTypeName"));
            fileEvent.setParams({"UploadFinished": true});
            fileEvent.fire(); 
        }   
    }  
})