({
	doInit : function(component, event, helper) {
		helper.generateNonFilerAssessements(component);
	},
    confirmAction : function(component, event, helper) {
        helper.confirm(component);
    },
    cancel: function(component, event, helper) {
          $A.get("e.force:closeQuickAction").fire();
    }
})