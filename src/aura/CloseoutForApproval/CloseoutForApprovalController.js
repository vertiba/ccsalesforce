({
	doInit: function (component, event, helper) {
        component.set('v.showSpinner', true);
        helper.getRecordsToDisplay(component); 
	    helper.getAdjustmentReasonPicklists(component);
   }, 
    handleAdjustmentReasonChange : function(component, event, helper) {
        var assessmentDataWrapper = component.get("v.dataToDisplay");
        var selPickListValue = component.find("adjustmentReason").get("v.value");
        assessmentDataWrapper[0].caseRecord.AdjustmentReason__c = selPickListValue;
        var assessment = assessmentDataWrapper[0];       
        for(var i =0 ;i<assessment.escapeCloseouts.length;i++){
            assessment.escapeCloseouts[i].AdjustmentReason__c = selPickListValue;
        }
        component.set('v.dataToDisplay', assessmentDataWrapper);
        
    },
    handleSubTypeChange : function(component, event, helper) {
        var assessmentDataWrapper = component.get("v.dataToDisplay");
        var selPickListValue = component.find("subType").get("v.value");
        assessmentDataWrapper[0].caseRecord.SubType__c = selPickListValue;
        var assessment = assessmentDataWrapper[0];       
        for(var i =0 ;i<assessment.escapeCloseouts.length;i++){
            assessment.escapeCloseouts[i].AdjustmentReason__c = selPickListValue;
        }
        component.set('v.dataToDisplay', assessmentDataWrapper);
        
    },
    handleSubmit :function (component, event, helper){  
    component.set('v.showSpinner', true);
    var assessmentDataWrapper = component.get("v.dataToDisplay");
    var action = component.get("c.updateAndApprove");
      var actParams = {         
          'assessmentDataWrapper':  JSON.stringify(assessmentDataWrapper[0])
      }
      action.setParams(actParams);
      action.setCallback(this, function (response) {
          var state = response.getState();           
          if (component.isValid() && state === "SUCCESS") {
              var results = response.getReturnValue();                
              component.set('v.dataToDisplay', results);
              component.set('v.showSpinner', false);
              sforce.one.showToast({
                  "title": "Success",
                  "message": "Submitted For Approval",
                  "type" : "Success"
              });
              sforce.console.getEnclosingTabId(
                  $A.getCallback(function(result) {
                      sforce.console.closeTab(result.id);
                      
                  })
              );
          }else{
              var errors = response.getError();
              component.set('v.showSpinner', false);
              sforce.one.showToast({
                  "title": "Error",
                  "message": JSON.stringify(errors[0].message),
                  "type" : "error"
              });
              sforce.console.getEnclosingTabId(
                $A.getCallback(function (result) {
                    sforce.console.closeTab(result.id);
                })
            );
          }
      });       
      $A.enqueueAction(action);
    },
    handleCancel : function(component, event, helper){
        sforce.console.getEnclosingTabId(
            $A.getCallback(function(result) {
                sforce.console.closeTab(result.id);
                })
            );
    }
})