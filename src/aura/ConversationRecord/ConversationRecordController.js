/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
		component.set('v.data',[]);
        var fil = component.get('v.filter');
        if(fil==='undefined' || fil===null){
            fil={};
        }
        fil['pageNumber']=1;
        component.set('v.filter',fil);
        helper.loadData(component);
	},
    displayBatchDetails: function (component, event, helper) {
        var fil = component.get('v.filter');
        if(fil==='undefined' || fil===null){
            //component.set('v.filter',{});
        } else{
            //removing stageId, so, that initial conditions if any + Batchid, will still be applied.
            delete fil.stageId;
            delete fil.pageNumber;
        }
        helper.gotoComponent(component, 'ConversationStage', fil);
    },
    sortTable: function (component, event, helper) {
        helper.sortData(component, event);
    }
})