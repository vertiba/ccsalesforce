({
    doInit : function(component, event, helper) {

        helper.checkPermissions(component,event);
    },
    confirmAction : function(component, event, helper) {
        helper.confirm(component,event);
    },
    cancel: function(component, event, helper) {
          $A.get("e.force:closeQuickAction").fire();
    }

})