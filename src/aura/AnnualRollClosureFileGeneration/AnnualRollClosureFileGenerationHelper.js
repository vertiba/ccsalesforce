({
    checkPermissions : function(component,event) {
        var spinnerMain =  component.find("Spinner");
        var action = component.get("c.checkRequiredPermissions");
        action.setParams({
            'recordId' : component.get('v.recordId')
        }); 
        action.setCallback(this,function(serverResponse){
            var state = serverResponse.getState();

            if(state ==='SUCCESS'){
                
                var response = serverResponse.getReturnValue();
               
                component.set('v.serverResponse', response);
                component.set('v.rollYear', response.rollYear);
              
                $A.util.removeClass(spinnerMain, "slds-show");   
                $A.util.addClass(spinnerMain, "slds-hide"); 
                  
                          
            }else{
                var errorMsg = ''; 
                var errors = serverResponse.getError();
                if (errors && errors[0] && errors[0].message) {
                    errorMsg = errors[0].message;                    
                } else {
                    errorMsg = "Unknown error. Please try again after some time.";
                }
                component.find('notifLib').showNotice({
                    "variant": "error",
                    "header": "Error!",
                    "message": errorMsg                    
                });
                
                $A.get("e.force:closeQuickAction").fire();
            }
            
        });
        $A.enqueueAction(action);
    },

    confirm : function(component,event) {   
        var spinnerMain =  component.find("Spinner");
        $A.util.addClass(spinnerMain, "slds-show"); 

        //component.set('v.showSpinner', true);
        var action= component.get("c.triggerAABCallout");
        action.setParams({
            'rollYear' : component.get('v.rollYear')
        }); 
        action.setCallback(this,function(serverResponse){
            var state = serverResponse.getState();

            $A.util.removeClass(spinnerMain, "slds-show");   
            $A.util.addClass(spinnerMain, "slds-hide"); 

            if(state === "SUCCESS"){
                var response = serverResponse.getReturnValue();           
                if(response.isSuccess){
                    component.find('notifLib').showNotice({
                        "variant": 'info',
                        "header": 'Alert',
                        "message": response.message,
                        closeCallback: function() {  
                        }
                    });  
                }else{
                    
                    component.find('notifLib').showNotice({
                        "variant": 'error',
                        "header": 'Warning',
                        "message": response.message                        
                    }); 
                }
                $A.get("e.force:closeQuickAction").fire();                             
            }else{
                var errorMsg = ''; 
                var errors = serverResponse.getError();
                if (errors && errors[0] && errors[0].message) {
                    errorMsg = errors[0].message;                    
                } else {
                    errorMsg = "Unknown error. Please try again after some time.";
                }
                component.find('notifLib').showNotice({
                    "variant": "error",
                    "header": "Error!",
                    "message": errorMsg                    
                });
            }
        });
        
        $A.enqueueAction(action);        
    }
})