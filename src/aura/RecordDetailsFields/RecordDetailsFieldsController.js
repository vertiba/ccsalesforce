({
	doInit : function(component, event, helper) {
        helper.setDisplayFieldList(component);
        helper.showComponent(component);
		helper.getFieldSet(component);
	},
    
    showComponent : function(component, event, helper) {
        helper.showComponent(component);
    },
    
    reloadRecord : function(component, event, helper) {
    	helper.reloadRecord(component);
	}
})