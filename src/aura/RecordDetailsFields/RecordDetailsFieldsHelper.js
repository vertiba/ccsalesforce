({
	getFieldSet : function(component) {
		var recordId = component.get('v.recordId');
		var fieldSetName = component.get('v.fieldSetName');
		var getFieldSetFields = component.get('c.getFieldSetFields');
        getFieldSetFields.setParams({
            'recordId' : recordId,
            'fieldSetName' : fieldSetName
        });
        getFieldSetFields.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                var fieldSetFields = response.getReturnValue();
                console.log(fieldSetFields); 
                component.set('v.fieldSetFields', fieldSetFields);
            } else { 
                //this.showError(component);
                console.log(response.getError());
            }
         });
        $A.enqueueAction(getFieldSetFields);
	},
    
    setDisplayFieldList : function(component) {
        var displayField = component.get('v.displayField');
        if(!displayField)
            return;
        var displayFieldList = [];
        displayFieldList.push(displayField);
        component.set('v.displayFieldList', displayFieldList);        
    },
    
    showComponent : function(component) {
        var displayField = component.get('v.displayField');
        // if there is null value, show component
        if(!displayField) { 
            component.set('v.showComponent', true);
            return;
        }
        
        // if the record hasnt loaded yet, return and wait
		var record = component.get('v.displayFieldRecord');
        if(!record)
            return;
        
        // get the field value of the deciding field and show if true
        var showComponent = record[displayField];
        if(showComponent)
            component.set('v.showComponent', true);             
        
    },   
    
    reloadRecord : function(component) {
        component.find('displayFieldRecord').reloadRecord();
    }
})