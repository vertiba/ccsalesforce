({
    closeQuickAction : function(component, event, helper) {
     $A.get("e.force:closeQuickAction").fire();
    },

    doInit: function(component, event) {
       
        var action = component.get("c.checkPermission");
        action.setCallback(this, function(response) {
            var state = response.getState();
          
            if (state === "SUCCESS") {
                
               if(!response.getReturnValue()) {
                
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        message: $A.get("$Label.c.NoPermission"),
                        duration:' 5000',
                        type: 'error',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
            }else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Info',
                    message: 'Error occured. Please contact your system administrator for further details.',
                    duration:' 5000', 
                    type: 'error',
                    mode: 'dismissible'
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
    } ,
})