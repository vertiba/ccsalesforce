/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	doInit : function(component, event, helper) {
		var duplicateRuleAction = component.get('c.sobjectTypeHasDuplicateRules');
        var duplicateRuleActionParams = {
            recordId : component.get('v.recordId')
        };
        duplicateRuleAction.setParams(duplicateRuleActionParams);
        duplicateRuleAction.setCallback(this, function(a) {
			if(a.getState() === 'SUCCESS') {
            	var result = a.getReturnValue();
                component.set('v.hasDuplicateRules', result);
			}
		});
        
        $A.enqueueAction(duplicateRuleAction);
        
        var action = component.get('c.getPotentialDuplicates');
        var params = {
            recordId : component.get('v.recordId')
        };

        action.setParams(params);
        action.setCallback(this, function(a) {
			if(a.getState() === 'SUCCESS') {
            	var results = a.getReturnValue();
                component.set('v.potentialDuplicates', results);
			}
		});
        
        $A.enqueueAction(action);
	}
})