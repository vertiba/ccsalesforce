/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
   
    getAddressMetadata : function(component,addressType) {
      
        component.set('v.showSpinner', true);        
        var action = component.get('c.getAddressMetadata');
        var params = {
            sobjectApiName: component.get('v.sObjectName'), 
            addressType : addressType
        };
        action.setParams(params);
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
            
                component.set('v.addressMetadata', result);
            } else {
               
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message += errors[0].message + '\n' + errors[0].stackTrace;
                }
               
                 this.showToast('Error!','error',message);              
            }
        });
        
        $A.enqueueAction(action);
        
        component.set('v.showSpinner', false);
    },
    
    showToast : function(title,type,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title, 
            "message": message,
            "type":type
        });
        toastEvent.fire();
    },
    
    // For Getting addressType mapping with sobject
    addressTypeMappingWithSObject : function(component,record, addressType){
       
        var addresses = component.get("v.addressValues");
        
        addresses["CareOf"] = record[addressType + 'CareOf__c']; 
        addresses["StreetNumber"] = record[addressType + 'StreetNumber__c']; 
        addresses["StreetFraction"] = record[addressType + 'StreetFraction__c']; 
        addresses["StreetPreDirection"] = record[addressType + 'StreetPreDirection__c']; 
        addresses["StreetName"] = record[addressType + 'StreetName__c']; 
        addresses["StreetType"] = record[addressType + 'StreetType__c'];
        addresses["UnitType"] = record[addressType + 'UnitType__c'];
        addresses["UnitNumber"] = record[addressType + 'UnitNumber__c'];
        addresses["City"] = record[addressType + 'City__c'];
        addresses["State"] = record[addressType + 'State__c'];
        addresses["PostalCode"] = record[addressType + 'PostalCode__c'];
        addresses["PostalCodeExtension"] = record[addressType + 'PostalCodeExtension__c'];
        addresses["Country"] = record[addressType + 'Country__c'];
        
        component.set("v.addressValues", addresses);

        
    },
    clearParseData : function(component){
         // Resetting All Data to empty
         var addresses = component.get('v.addressValues');        
        addresses["StreetNumber"] = ""; 
        addresses["StreetFraction"] = ""; 
        addresses["StreetPreDirection"] = ""; 
        addresses["StreetName"] = ""; 
        addresses["StreetType"] = "";
        addresses["UnitType"] = "";
        addresses["UnitNumber"] = "";
        addresses["City"] = "";
        addresses["State"] = "";
        addresses["PostalCode"] = "";
        addresses["PostalCodeExtension"] = "";
        
        component.set("v.addressValues", addresses); 
        component.set('v.showSpinner', false);
        this.parseValidationResult(component);
        
    },
    // Paring validating the result
    parseValidationResult : function(component){
        
        var searchResult = component.get('v.searchResult');
        var addressInput = component.get('v.searchResult').Street.toUpperCase();
        var streetTypes = component.get('v.addressMetadata').StreetTypes;
        var unitTypes = component.get('v.addressMetadata').UnitTypes;
        //  var StreetFractions     = component.get('v.addressMetadata').StreetFractions;
        var StreetPreDirections = component.get('v.addressMetadata').StreetPreDirections;  
          // for PO BOX
        if(addressInput.replace(/\./g, '').startsWith('PO BOX')) {
            
            var addresses = component.get('v.addressValues');
                        
            addresses["StreetNumber"] = ''; 
            addresses["StreetName"] = addressInput.replace(/\./g, ''); 
            addresses["StreetType"] = '';
            addresses["UnitType"] = '';
            addresses["UnitNumber"] = '';   
            addresses["City"] = searchResult.City;
            addresses["State"] = searchResult.State;
            addresses["PostalCode"] = searchResult.Zip5;
            addresses["PostalCodeExtension"] = searchResult.Zip4;
            component.set('v.addressValues', addresses);
            return;
        }
        var streetPreDirection;
       // var streetFraction;
        var streetInfo;
        var streetNumber;
        var streetName ="";
        var streetType;
        var unitType;
        var unitNumber;
     
        var streetSplit = addressInput.split(" ");
        var strLength = streetSplit.length; 
        if(strLength<0) {
            return false;
        }       
        for ( var i=strLength; i> 0; i--){
            if(i == strLength-1 ){     
                isnum = /^\d+$/.test(streetSplit[i]);
                if(isnum){
                    unitNumber = streetSplit[i];
                    streetSplit.splice(i,1);
                    continue;               
                }
            } 
            var uniTyp = unitTypes.indexOf(streetSplit[i]);
            if(uniTyp >= 0){
                unitType = unitTypes[uniTyp];
                streetSplit.splice(i,1);
                continue;
            } 
        }
       var strLength = streetSplit.length;
       for( var i = 0; i < strLength; i++){
             var isnum=/^\d+$/.test(streetSplit[i]);
            if(isnum && i == 0){
                streetNumber = streetSplit[i];
                continue;
            }            
         /* If we want to populate fractions  
            var stFrac = StreetFractions.indexOf(streetSplit[i]);
            if(stFrac >= 0){
                streetFraction = StreetFractions[stFrac];
            }*/
            var stPreDir = StreetPreDirections.indexOf(streetSplit[i]);
            if( stPreDir >= 0){
                 streetPreDirection = StreetPreDirections[stPreDir];
                 continue;
            }
            var stType = streetTypes.indexOf(streetSplit[i]); 
            if( stType >= 0){
                 streetType = streetTypes[stType];
            }
           if( i !=0 && i != (strLength-1)){
               streetName = streetName +' ' + streetSplit[i];         
               continue;     
           }       
        }   
        var addresses = component.get('v.addressValues');        
        addresses["StreetNumber"] = streetNumber; 
       //addresses["StreetFraction"] = streetFraction; 
        addresses["StreetPreDirection"] = streetPreDirection; 
        addresses["StreetName"] = streetName; 
        addresses["StreetType"] = streetType;
        addresses["UnitType"] = unitType;
        addresses["UnitNumber"] = unitNumber;
        addresses["City"] = searchResult.City;
        addresses["State"] = searchResult.State;
        addresses["PostalCode"] = searchResult.Zip5;
        addresses["PostalCodeExtension"] = searchResult.Zip4;        
        component.set("v.addressValues", addresses);              
        component.set('v.showSpinner', false);
        
    },
    
    // To vaildate USPS address
    searchForAddressHelper : function( component, inputStreet,inputZipCode){
    
        if(inputStreet == null || inputStreet == "") {
            this.showToast('Field Required','error','To validate address Street address is required.');  
            component.set('v.showSpinner', false);
            return;
        }
        if(inputZipCode == null || inputZipCode == "") {
            this.showToast('Field Required','error','To validate address Zip code is required.');  
            component.set('v.showSpinner', false);
            return;
        }
        var isnum = /^\d+$/.test(inputZipCode);
        if(!isnum){
            this.showToast('Incorect Value','error','Please enter proper value for zipcode. It only accept digits ');  
            component.set('v.showSpinner', false);
            return;
        }
        
        var action = component.get('c.lookupAddress');
        var params = {
            street  : component.get('v.uspsAddressInput'),
            zipCode: component.get('v.uspsZipInput')
         
        };
  
        action.setParams(params);
        action.setCallback(this, function(response) {
            
            if(response.getState() === 'SUCCESS') {
                var searchResult = response.getReturnValue();

                component.set('v.searchResult', searchResult);
           
                
            } else {
               let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message + '\n' + errors[0].stackTrace;
                }
                message = message + 'Your address could not be validated with USPS';
                
                // Display the message
                
                this.showToast('Error','error',message); 
                
            }
        });
        
        $A.enqueueAction(action);    
        component.set('v.showSpinner', false);
  
    },
    
    // Clear the Address fields
    clearAddressHelper : function(component, addresses){
       
        addresses["CareOf"] = ''; 
        addresses["StreetNumber"] =''; 
        addresses["StreetFraction"] = ''; 
        addresses["StreetPreDirection"] =''; 
        addresses["StreetName"] =''; 
        addresses["StreetType"] = '';
        addresses["UnitType"] = '';
        addresses["UnitNumber"] = '';
        addresses["City"] = '';
        addresses["State"] = '';
        addresses["PostalCode"] = '';
        addresses["PostalCodeExtension"] = '';
        
        return addresses;
    },
    
    /* For Future Use If needed- Find ZipCode button***
     
    searchForZipCodeHelper : function (component,street,addresses){
        
        if(street == null || street == "") {
            this.showToast('Field Required','error','To populate zip code-> Street address is required.');  
            component.set('v.showSpinner', false);
            return;
        }        
        if(addresses["City"] == null || addresses["City"] == ""){
            this.showToast('Field Required','error','To populate zip code-> City is required.');  
            component.set('v.showSpinner', false);
            return;
        }      
        if(addresses["State"] == null || addresses["State"] == ""){
            this.showToast('Field Required','error','To populate zip code -> State is required.');  
            component.set('v.showSpinner', false);
            return;
        }
        
        var action = component.get('c.lookupZipCode');
        var params = {
            street : component.get('v.uspsAddressInput'),
            city   : addresses["City"],
            state  : addresses["State"] 
        }
      
        action.setParams(params);
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
  
                
                addresses["City"]               = result.City ? result.City : addresses["City"];
                addresses["State"]               = result.State ? result.State :addresses["State"];
                addresses["PostalCode"]          = result.Zip5 ? result.Zip5 : addresses["PostalCode"]
                addresses["PostalCodeExtension"] = result.Zip4 ? result.Zip4 :  addresses["PostalCodeExtension"];
                component.set('v.addressValues', addresses);
  
                
            } else{
               
                
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message + '\n' + errors[0].stackTrace;
                }
                message = message + 'Your address could not be validated with USPS';
                
                // Display the message
                this.showToast('Error','error',message);                            
                
            }
        });
        
        $A.enqueueAction(action);        
        component.set('v.showSpinner', false);        
    }, */
    
    searchForCityStateHelper : function(component,addresses){
        
        if(addresses["PostalCode"] == null || addresses["PostalCode"] == "") {
            this.showToast('Field Required','error','To populate City-> zip code is required.');  
            component.set('v.showSpinner', false);
            return;
        }
        
        var isnum = /^\d+$/.test(addresses["PostalCode"]);
        if(!isnum){
            this.showToast('Incorect Value','error','Please enter proper value for zipcode. It only accept digits ');  
            component.set('v.showSpinner', false);
            return;
        }
        
        var action = component.get('c.lookupCityState');
        var params = {
            zipCode : addresses["PostalCode"]
        };
        action.setParams(params);
        action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
              
                addresses["City"]               = result.City ? result.City : addresses["City"];
                addresses["State"]               = result.State ? result.State :addresses["State"];
                addresses["PostalCode"]          = result.Zip5 ? result.Zip5 : addresses["PostalCode"]
                addresses["PostalCodeExtension"] = result.Zip4 ? result.Zip4 :  addresses["PostalCodeExtension"];
                component.set('v.addressValues', addresses);
            
                
            } else {
             
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message + '\n' + errors[0].stackTrace;
                }
                message = message + 'Your address could not be validated with USPS';
                // Display the message
                
                this.showToast('Field Required','error', message);  
            }
        });
        
        $A.enqueueAction(action);
        component.set('v.showSpinner', false);
        
    },
    
    //Getting Fields Api name from Server with respect to sobject
    dataLoad: function(component){
    
        var action = component.get('c.getData');
        var addressType = component.get('v.addressType');
        var objectName= component.get('v.sObjectName');
        var record = null;
      
        action.setParams({
            sobjectApiName:objectName,
            addressType: addressType,
            recID : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
          
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.record", result);  
                record = component.get("v.record");
                                           
                this.addressTypeMappingWithSObject(component, record, addressType); // Calling for Maping address type with field

            }else{
                
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message + '\n' + errors[0].stackTrace;
                }
                message = message + 'Something went wrong while loading fields names';
                // Display the message
                
                this.showToast('Field Names','error', message);  
            }
        });
        
        $A.enqueueAction(action);    
        
        
    },
    
    
})