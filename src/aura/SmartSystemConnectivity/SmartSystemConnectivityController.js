/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	handleClick : function(component, event, helper) {
		var action = component.get("c.verifyUrl");
        var actParams = {
            'url': component.get('v.urltotest')
        }
        action.setParams(actParams);
 		action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                var result =response.getReturnValue();
                component.set('v.urlreturnedstring',result);
            } else {
                let errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    component.set('v.urlreturnedstring',errors[0].message);
                } else {
                    component.set('v.urlreturnedstring',errors);
                }
            }
        });
        $A.enqueueAction(action);  
	}
})