trigger RollYear on RollYear__c(before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    new RollYearHandler().execute();
}