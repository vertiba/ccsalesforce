trigger VIPTemplate on VIPForm__VIP_Template__c(before insert) {
    for(VIPForm__VIP_Template__c vipTemplate : Trigger.new) {
        if(!VIPFormRestoreService.isImporting()) vipTemplate.Uuid__c = null;
        if(Uuid.isValid(vipTemplate.Uuid__c)) continue;

        vipTemplate.Uuid__c = new Uuid().getValue();
    }
}