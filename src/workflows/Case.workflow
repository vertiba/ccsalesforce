<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AssessmentRejected</fullName>
        <description>Assessment Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SMART_Templates/AssessmentRejected</template>
    </alerts>
    <alerts>
        <fullName>Case_Request_for_Additional_Information</fullName>
        <description>Case Request for Additional Information</description>
        <protected>false</protected>
        <recipients>
            <field>TaxpayerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SMART_Templates/Case_Additional_Documentation_Requested</template>
    </alerts>
    <alerts>
        <fullName>CloseoutEscapesEmailAlert</fullName>
        <description>Closeout Escapes Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply.assessor-recorder@sfgov.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SMART_Templates/CloseoutEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>Customer_Service_Business_Info_Completed</fullName>
        <description>Customer Service Business Info Completed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.assessor-recorder@sfgov.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SMART_Templates/Customer_Service_Case_Completed</template>
    </alerts>
    <alerts>
        <fullName>Email_Case_Owner_Approval_Received</fullName>
        <description>Email Case Owner - Approval Received</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SMART_Templates/Cases_Approved</template>
    </alerts>
    <alerts>
        <fullName>Send_Customer_Service_Case_Submission_Received</fullName>
        <description>Send Customer Service Case Submission Received</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply.assessor-recorder@sfgov.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SMART_Templates/Case_Submission_Confirmation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Status_Rejected_As_InProgress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Case Status Rejected As InProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_SubStatus_Rejected_As_None</fullName>
        <field>SubStatus__c</field>
        <name>Case Sub Status Rejected As None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetCaseStatusasInProgress</fullName>
        <description>ASR-8552</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Set Case Status as In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetCaseSubstatusasNull</fullName>
        <description>ASR-8552</description>
        <field>SubStatus__c</field>
        <name>Set Case Sub-status as Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusasInProgress</fullName>
        <description>ASR-8552</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Set Status as In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetSubstatusasNull</fullName>
        <description>ASR-8552</description>
        <field>SubStatus__c</field>
        <name>Set Sub-status as Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Status_as_In_Progress</fullName>
        <description>On Approval Process starts set Case status to In Progress</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Set Case Status as In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Sub_Status_as_Pending_Review</fullName>
        <description>On the approval process submission, set Case sub status as Pending Review</description>
        <field>SubStatus__c</field>
        <literalValue>Pending Review</literalValue>
        <name>Set Case Sub Status as Pending Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IsLocked_c_true</fullName>
        <description>Set this field to true so a FieldSet can be used to lock a subset of fields on approval</description>
        <field>IsLocked__c</field>
        <literalValue>1</literalValue>
        <name>Set IsLocked__c = true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Minimum_Cap_Assessed_Value</fullName>
        <description>Set this field to null, in case of rejection or recall of approval process.</description>
        <field>MinimumCapAssessedValue__c</field>
        <name>Set Minimum Cap Assessed Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Substatus_Completed</fullName>
        <field>SubStatus__c</field>
        <literalValue>Completed</literalValue>
        <name>Set Substatus Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_As_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Update Case Status As Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_SubStatus_as_Noticing</fullName>
        <field>SubStatus__c</field>
        <literalValue>Noticing</literalValue>
        <name>Update Case Sub Status as Noticing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Sub_Status_as_Completed</fullName>
        <field>SubStatus__c</field>
        <literalValue>Completed</literalValue>
        <name>Update Case Sub Status as Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Reason</fullName>
        <field>ClosedReason__c</field>
        <literalValue>Requested Documentation Not Provided</literalValue>
        <name>Update Closed Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Request_for_Info_Sent</fullName>
        <field>RequestforInfoSent__c</field>
        <formula>Today()</formula>
        <name>Update Request for Info Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Statuses</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Update Statuses</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sub_Status</fullName>
        <field>SubStatus__c</field>
        <literalValue>Completed</literalValue>
        <name>Update Sub-Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Info Requested from Taxpayer</fullName>
        <actions>
            <name>Update_Request_for_Info_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.TypeofInfoRequested__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TaxpayerEmail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RequestforInfoReceived__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Closed_Reason</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Statuses</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Sub_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Email to Taxpayer on Additional Docs</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send an email to the Taxpayer when additional documentation is needed on a Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SendEmailForTaxLiability</fullName>
        <actions>
            <name>CloseoutEscapesEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Regular</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubType__c</field>
            <operation>equals</operation>
            <value>Closeout</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SubStatus__c</field>
            <operation>equals</operation>
            <value>Noticing,Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsLegacy__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>After the Regular/Escape Closeouts are approved, an email is sent to escro about the No Taxabale liability of the Taxpayer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
