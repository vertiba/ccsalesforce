<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Annual_Roll_Closed</fullName>
        <field>Status__c</field>
        <literalValue>Roll Closed</literalValue>
        <name>Annual Roll Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status</fullName>
        <field>Status__c</field>
        <literalValue>In Review for Roll Closure</literalValue>
        <name>Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Back_to_Working_Roll_Open</fullName>
        <field>Status__c</field>
        <literalValue>Roll Open</literalValue>
        <name>Back to Working Roll Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Annual_Roll_Closed</fullName>
        <field>Status__c</field>
        <literalValue>Roll Closed</literalValue>
        <name>Change to Annual Roll Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_Review_for_Annual_Roll_Closure</fullName>
        <field>Status__c</field>
        <literalValue>In Review for Roll Closure</literalValue>
        <name>In Review for Annual Roll Closure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IntegrationStatusasBlank</fullName>
        <field>IntegrationStatus__c</field>
        <name>Integration Status as Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lock_Roll_Year_Assessments_in_Review</fullName>
        <field>Status__c</field>
        <literalValue>In Review for Roll Closure</literalValue>
        <name>Lock Roll Year Assessments (in Review)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Roll_Closure</fullName>
        <field>Status__c</field>
        <literalValue>Roll Closed</literalValue>
        <name>Roll Closure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_IsLocked_c_true</fullName>
        <description>This is created as part of bug-10499 to set the isLocked flag.</description>
        <field>IsLocked__c</field>
        <literalValue>1</literalValue>
        <name>Set IsLocked__c = true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unlock_Roll_Year</fullName>
        <field>Status__c</field>
        <literalValue>Roll Open</literalValue>
        <name>Unlock Roll Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <tasks>
        <fullName>Please_Review_Roll_Close</fullName>
        <assignedTo>RPPrincipal</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>User.LastLoginDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Please Review Roll Close</subject>
    </tasks>
</Workflow>
