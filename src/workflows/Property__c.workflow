<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RP_Exemption_Notice_January_1</fullName>
        <description>RP Exemption Notice January 1</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SMART_Templates/Exemption_Email_Notice_January_1</template>
    </alerts>
    <alerts>
        <fullName>Vessel_Notice_January_1</fullName>
        <description>Vessel Notice January 1</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SMART_Templates/Vessel_Email_Notice_January_1</template>
    </alerts>
</Workflow>
