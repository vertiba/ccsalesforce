<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Audit_Status_Pending_Review</fullName>
        <description>Updates the Audit Status to 'Pending Review' when submitted for approval.</description>
        <field>AuditStatus__c</field>
        <literalValue>Pending Review</literalValue>
        <name>Change Audit Status - Pending Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
