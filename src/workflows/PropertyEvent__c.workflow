<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approval_Status_Complete</fullName>
        <field>ApprovalStatus__c</field>
        <literalValue>Complete</literalValue>
        <name>Approval Status Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Incomplete</fullName>
        <description>Approval Status is set to Incomplete</description>
        <field>ApprovalStatus__c</field>
        <literalValue>Incomplete</literalValue>
        <name>Approval Status Incomplete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Exempt_Percent_update</fullName>
        <description>update PercentExempt field based on filed date on exemption record</description>
        <field>PercentExempt__c</field>
        <formula>IF( AND( FileDate__c  &gt;= DATE(YEAR(FileDate__c),1,1),FileDate__c  &lt;= DATE(YEAR(FileDate__c),2,15)), 0.96,
IF( AND( FileDate__c  &gt;= DATE(YEAR(FileDate__c),2,16),FileDate__c  &lt;= DATE(YEAR(FileDate__c),8,1)), 0.80,
IF( AND( FileDate__c  &gt; DATE(YEAR(FileDate__c),8,1),FileDate__c  &lt;= DATE(YEAR(FileDate__c),12,31)), NULL,
NULL)))</formula>
        <name>Exempt Percent update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsLockedTrue</fullName>
        <field>IsLocked__c</field>
        <literalValue>1</literalValue>
        <name>IsLockedTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusPendingFurtherAction</fullName>
        <field>Status__c</field>
        <literalValue>Pending Further Action</literalValue>
        <name>Status = Pending Further Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Closed</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Status = Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_In_Progress</fullName>
        <field>Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Status = In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_status_Pending_Review</fullName>
        <field>SubStatus__c</field>
        <literalValue>Pending Review</literalValue>
        <name>Sub-status = Pending Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sub_Status</fullName>
        <description>Mar 4 KJB: Current functionality: Sub-Status = Approved.

Mar 4 KJB: Proposed functionality: If Sub-Status was 'Pending Approval', Sub-Status is set to 'Approved'.  If Sub-Status was 'Pending Denial', Sub-Status is set to 'Denied'.</description>
        <field>SubStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Sub-Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Example Construction Email Alert Workflow</fullName>
        <active>false</active>
        <criteriaItems>
            <field>PropertyEvent__c.ProjectStatus__c</field>
            <operation>equals</operation>
            <value>I will not be doing this work at all</value>
        </criteriaItems>
        <description>Example email alert for when a property event is change to project status: I will not be doing this work at all</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Exempt Percent update</fullName>
        <actions>
            <name>Exempt_Percent_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
