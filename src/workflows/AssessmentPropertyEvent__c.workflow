<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_Amount_Exempt</fullName>
        <description>Jonathan: quick POC way to handle setting a field that's used in a roll-up summary field on Case</description>
        <field>AmountExempt_Stored__c</field>
        <formula>AmountExempt__c</formula>
        <name>Copy Amount Exempt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Copy Amount Exempt</fullName>
        <actions>
            <name>Copy_Amount_Exempt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AmountExempt_Stored__c != AmountExempt__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
