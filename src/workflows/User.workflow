<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notification_when_profile_is_changed_from_Sapient_Admin</fullName>
        <ccEmails>5bf8d835.publicisgroupe.onmicrosoft.com@amer.teams.ms</ccEmails>
        <description>Email notification when profile is changed from 'Sapient Admin'</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>SMART_Templates/Profile_changed_from_Sapient_Admin</template>
    </alerts>
</Workflow>
