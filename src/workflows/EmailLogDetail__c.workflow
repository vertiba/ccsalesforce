<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ReproMail_Upload_Notification</fullName>
        <description>ReproMail Upload Notification</description>
        <protected>false</protected>
        <recipients>
            <field>RecipientEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SMART_Templates/ReproMail_Upload_Notification</template>
    </alerts>
</Workflow>
