<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SendFormSubmissionReceived</fullName>
        <description>Send Form Submission Received</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmailAddress__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.assessor-recorder@sfgov.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SMART_Templates/Statement_Submission_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>SendFormSubmissionReceivedBusinessEmail</fullName>
        <description>Send Form Submission Received - Business Email</description>
        <protected>false</protected>
        <recipients>
            <field>BusinessEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.assessor-recorder@sfgov.org</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SMART_Templates/Statement_Submission_Confirmation</template>
    </alerts>
</Workflow>
