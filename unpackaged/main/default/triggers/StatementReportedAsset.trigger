/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
trigger StatementReportedAsset on StatementReportedAsset__c(before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    new StatementReportedAssetHandler().execute();
}