trigger VIPCategory on VIPForm__VIP_Category__c(before insert) {
    for(VIPForm__VIP_Category__c vipCategory : Trigger.new) {
        if(!VIPFormRestoreService.isImporting()) vipCategory.Uuid__c = null;
        if(Uuid.isValid(vipCategory.Uuid__c)) continue;

        vipCategory.Uuid__c = new Uuid().getValue();
    }
}