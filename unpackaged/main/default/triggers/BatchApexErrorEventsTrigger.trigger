/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Trigger Name : BatchApexErrorEventsTrigger
 * Description : Subscribes to BatchApexErrorEvent events
 */
trigger BatchApexErrorEventsTrigger on BatchApexErrorEvent (after insert) {
    new BatchApexErrorEvents(Trigger.new).handle();
}