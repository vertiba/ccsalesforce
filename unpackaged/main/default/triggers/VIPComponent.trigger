trigger VIPComponent on VIPForm__VIP_Component__c(before insert) {
    for(VIPForm__VIP_Component__c vipComponent : Trigger.new) {
        if(!VIPFormRestoreService.isImporting()) vipComponent.Uuid__c = null;
        if(Uuid.isValid(vipComponent.Uuid__c)) continue;

        vipComponent.Uuid__c = new Uuid().getValue();
    }
}