trigger VIPFormBranding on VIPForm__VIP_Form_Branding__c(before insert) {
    for(VIPForm__VIP_Form_Branding__c vipFormBranding : Trigger.new) {
        if(!VIPFormRestoreService.isImporting()) vipFormBranding.Uuid__c = null;
        if(Uuid.isValid(vipFormBranding.Uuid__c)) continue;

        vipFormBranding.Uuid__c = new Uuid().getValue();
    }
}