/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/

public class WaiverExpirationDateUpdatedScheduler extends EnhancedSchedulable{
    public override String getJobNamePrefix() {
        return 'Waiver Expiration Date Updated Scheduler'; 
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : SchedulableContext
    // @description : Schedulable interface method. Schedules the WaiverExpirationDateUdated batch job
    // @return : void
    //-----------------------------
    public void execute(SchedulableContext schedulableContext) {
        // Salesforce has a limit of 5 running batch jobs
        // If there are already 5 jobs running, then don't run this job
        // Any records that need to be processed will be processed the next time the job executes
        if(EnhancedSchedulable.getNumberOfRunningBatchJobs() >= 5) return;
        
        Logger.addDebugEntry('After Entering WaiverExpirationDateUpdated:', 'WaiverExpirationDateUpdated.execute');
		Id batchProcessId = Database.executebatch(new WaiverExpirationDateUpdated());
        Logger.addDebugEntry('After Scheduling batch, jobid:'+batchProcessId, 'WaiverExpirationDateUpdated.execute');
        Logger.saveLog();
    }
}