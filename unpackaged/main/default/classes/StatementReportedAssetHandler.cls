/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Trigger handler class for the object StatementReportedAsset__c
//      Currently no logic has been added, but this handler class triggers DLRS field calculations (via TriggerHandler)
//-----------------------------
public without sharing class StatementReportedAssetHandler extends TriggerHandler {
    protected override void executeBeforeInsert(List<SObject> newRecords) {
        List<StatementReportedAsset__c> newStatementReportedAssets = (List<StatementReportedAsset__c>)newRecords;
        this.verifyFormTypeAndPopulate(newStatementReportedAssets, null, false);
    }

    protected override void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {
        List<StatementReportedAsset__c> updatedStatementReportedAssets = (List<StatementReportedAsset__c>)updatedRecords;
        Map<Id,StatementReportedAsset__c> oldStatementReportedAssetsByIds = (Map<Id,StatementReportedAsset__c> )oldRecordsById;
        this.verifyFormTypeAndPopulate(updatedStatementReportedAssets, oldStatementReportedAssetsByIds, true);
    }
    
	//-----------------------------
    // @author : Publicis.Sapient
    // @param : newStatementReportedAssets
	// @param : oldStatementReportedAssetsByIds
	// @param : isUpdate
    // @description : If statement reported asset dont have a form type then populate it.
    // @return : void
    //-----------------------------
    private void verifyFormTypeAndPopulate(List<StatementReportedAsset__c> newStatementReportedAssets, Map<Id, StatementReportedAsset__c> oldStatementReportedAssetsByIds, boolean isUpdate){
        StatementReportedAsset__c oldStatementReportedAsset;
        Set<Id> statementIds = new Set<Id>();
        
        for(StatementReportedAsset__c statementReportedAsset : newStatementReportedAssets){
            if(isUpdate && statementReportedAsset.form__c != oldStatementReportedAssetsByIds.get(statementReportedAsset.Id).form__c) {
                statementIds.add(statementReportedAsset.statement__c);
            } else {
                statementIds.add(statementReportedAsset.statement__c);
            }
        }
        Statement__c statement = null;
        Map<Id, Statement__c> statementsById = new Map<Id,Statement__c>([select id, form__c from Statement__c where id in :statementIds]);
        for(StatementReportedAsset__c statementReportedAsset : newStatementReportedAssets){
            statement = statementsById.get(statementReportedAsset.Statement__c);
            if(statement != null && String.isNotBlank(statement.form__c)){
                statementReportedAsset.form__c = statement.form__c;
            }
        }
    }
}