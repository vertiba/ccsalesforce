/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// Batch ->Generation of Missing Escape Assessments when roll year is closing
//-----------------------------

public class EscapeAssessmentBatchGenerator implements Database.Batchable<sObject>, Database.Stateful, BatchableErrorHandler{
    
    private String CASE_QUERY;
    private String currentClosingRollYear;
    private RollYear__c rollYear;
    private String originLocation = CCSFConstants.ESCAPE_GENERATOR_BATCH_NAME;
    private Id statementBPPRecordTypeId;
    private Integer statuteOfLimitation;
    private Id assessmentLegacyRecordTypeId;
    private Id assessmentBPPRecordTypeId;
    
    public EscapeAssessmentBatchGenerator(RollYear__c roll){
        this.rollYear = roll;
        this.currentClosingRollYear = rollYear.Year__c;
        statementBPPRecordTypeId = DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);
    	this.statuteOfLimitation = !String.isBlank(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION_CONFIG)?Integer.valueOf(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION_CONFIG):Integer.valueOf(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION);
        assessmentLegacyRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME);
        assessmentBPPRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
    }
    public Database.QueryLocator start(Database.BatchableContext bc){
        String batchQuery ='SELECT Id, StartofBusinessatthisLocation__c, AssessmentYear__c,Property__c, Property__r.Account__c, Form__c, Assignee__c'; 
        batchQuery += ' FROM Statement__c WHERE Form__c =\'571-L\' ';
        batchQuery += ' AND recordTypeId = \''+statementBPPRecordTypeId+'\'';
        batchQuery += ' AND AssessmentYear__c = \''+currentClosingRollYear+'\'';
        batchQuery += ' AND Property__r.Status__c = \''+CCSFConstants.STATUS_ACTIVE+'\'';
        batchQuery += ' AND Property__r.Account__r.BusinessStatus__c = \''+CCSFConstants.STATUS_ACTIVE+'\'';
        Logger.addDebugEntry(batchQuery,originLocation+'- start method query');
        Logger.saveLog();

        return database.getQueryLocator([SELECT Id, StartofBusinessatthisLocation__c, AssessmentYear__c,Property__c, Property__r.Account__c, Form__c, Assignee__c 
                                         FROM Statement__c
                                         WHERE Form__c ='571-L'
                                         AND recordTypeId =: statementBPPRecordTypeId
                                         AND AssessmentYear__c =: currentClosingRollYear // Updated for ASR-8546
                                         AND Property__r.Status__c =: CCSFConstants.STATUS_ACTIVE 
                                         AND Property__r.Account__r.BusinessStatus__c =: CCSFConstants.STATUS_ACTIVE]);
    }
    
    public void execute(Database.BatchableContext bc, List<Statement__c> statements){
        
        Map<Id,Id> propertyIdByStatementOwnerId = new Map<Id,Id>();
        Map<String,Boolean> previousYearHasAssessment = new Map<String,Boolean>();
        Map<String,List<Case>> propertyIdAssessmentByChild = new Map<String,List<Case>>();
        Map<String,List<String>> caseIdByEscapedYears = new Map<String,List<String>>();
        Map<String,Case> yearByCaseForPenalty = new Map<String,Case>();
        Map<String,Decimal> propertyIdByCurrentTotalAdjusted = new Map<String,Decimal>();
        List<Id> newlyCreatedAssessmentIds = new List<Id>();
        List<String> caseIdWithEscapeYears = new List<String>();
        List<Case> noticeUpdateForEscapes = new List<Case>();
        Set<String> cases = new Set<String>();
        
        Set<String> propertyIdYears = new Set<String>();
        Set<Id> propertyIds = new Set<Id>();
        Map<String,Id> previousYearIdByEscapeYear = new Map<String,Id>();
        
        // Setting All fields of Case and Assessment Line Item Dynamically in CASE_QUERY
        Schema.SObjectType caseSObjectType = Case.sObjectType;
        Schema.SObjectType lineItemSObjectType = AssessmentLineItem__c.sObjectType;
        CASE_QUERY = DescribeUtility.buildQueryToFetchAllDetailsOfParentandChild(caseSObjectType, lineItemSObjectType);  
     
        try{            
            for(Statement__c statement : statements){
                if(statement.Assignee__c != null ){
                    propertyIdByStatementOwnerId.put(statement.Property__c,statement.Assignee__c);
                } 
                propertyIds.add(statement.Property__c);
                if(statement.StartofBusinessatthisLocation__c != null && statement.AssessmentYear__c != null){
                    Set<Integer> yearsBetweenStartAndAssessment = returnYearsBetweenStartAndAssessementYear(Integer.valueof(statement.StartofBusinessatthisLocation__c.Year())+1, Integer.valueOf(statement.AssessmentYear__c) );
                    if(!yearsBetweenStartAndAssessment.isEmpty()){
                        for(Integer year : yearsBetweenStartAndAssessment){
                            propertyIdYears.add(statement.Property__c +'-'+year);
                        }  
                    }                                               
                }                
            }
          
            for(Case caseRecord : [SELECT Id, Type,RecordTypeId, Property__c,RollYear__c, NoticeType__c,SubType__c,StatuteOfLimitations__c,AssessmentYear__c,TotalAssessedCost__c, Penalty__c,PenaltyPercent__c,ExemptionType__c,ExemptionPenalty__c,ExemptionCode__c,ExemptionDate__c,PercentExempt__c,ExemptionSubtype__c,ExemptAmountManual__c,ApplyFraudPenalty__c,WaivePenaltyReason__c,AdjustmentType__c,ApplyPenaltyReason__c FROM Case WHERE Property__c IN: propertyIds AND (Type ='Regular' OR Type='Escape')  Order By AssessmentYear__c DESC]){
                if(!propertyIdAssessmentByChild.containsKey(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c) ){
                    propertyIdAssessmentByChild.put(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c, new List<Case>());
                }
                propertyIdAssessmentByChild.get(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c).add(caseRecord);                   
                if(currentClosingRollYear == caseRecord.AssessmentYear__c && caseRecord.TotalAssessedCost__c != null ){
                    propertyIdByCurrentTotalAdjusted.put(caseRecord.Property__c+'-'+currentClosingRollYear,caseRecord.TotalAssessedCost__c); 
                }   
            }             
            if(propertyIdYears.isEmpty()) return;
           
            for(String propertyWithYear : propertyIdYears){                        
                List<String> yearWithPropertyIdsToBeProcessed =propertyWithYear.split('-');
                String propertyId = yearWithPropertyIdsToBeProcessed[0];
                String assessedYear = yearWithPropertyIdsToBeProcessed[1];
                if(propertyIdAssessmentByChild.containsKey(propertyWithYear)){
                    for(Case caseRecord : propertyIdAssessmentByChild.get(propertyWithYear)){                       
                        if(assessedYear != currentClosingRollYear && caseRecord.Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR && propertyIdAssessmentByChild.get(propertyWithYear).size() == 1){
							boolean isIncreasedGrosslyUnderreported = returnPercentageChange(caseRecord.TotalAssessedCost__c != null ? caseRecord.TotalAssessedCost__c : 0,propertyIdByCurrentTotalAdjusted.containsKey(propertyId+'-'+currentClosingRollYear) ? propertyIdByCurrentTotalAdjusted.get(propertyId+'-'+currentClosingRollYear): 0,rollYear.GrosslyUnderreported__c != null ? rollYear.GrosslyUnderreported__c: 0);  
                            //ASR-8551 Changes Starts
                            if(caseRecord.SubType__c == null && !propertyIdAssessmentByChild.containsKey(propertyId+'-'+currentClosingRollYear)) continue; 
                            if(caseRecord.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_LOWVALUE && propertyIdAssessmentByChild.get(propertyId+'-'+currentClosingRollYear)[0].SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_LOWVALUE) continue;
                            if((caseRecord.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_LOWVALUE || isIncreasedGrosslyUnderreported ) && !caseIdWithEscapeYears.contains(propertyIdAssessmentByChild.get(propertyId+'-'+currentClosingRollYear)[0].Id +'-'+ assessedYear)){ 
                                caseIdWithEscapeYears.add(propertyIdAssessmentByChild.get(propertyId+'-'+currentClosingRollYear)[0].Id +'-'+ assessedYear);  
                            }
                            //ASR-8551 Ends 
                            if(!previousYearIdByEscapeYear.containsKey(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c)){
                                 previousYearIdByEscapeYear.put(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c,caseRecord.Id);
                            }
                            if(!previousYearHasAssessment.containsKey(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c)){
                                previousYearHasAssessment.put(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c,true);                                
                            } 
                            if(previousYearHasAssessment.get(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c) && (caseRecord.Penalty__c != null || caseRecord.ApplyFraudPenalty__c || caseRecord.ExemptionPenalty__c != null || caseRecord.ExemptionCode__c != null || caseRecord.ExemptionType__c != null || caseRecord.ExemptionDate__c != null || caseRecord.ExemptionSubtype__c != null || caseRecord.WaivePenaltyReason__c != null || caseRecord.ApplyPenaltyReason__c != null )){
                                if(!yearByCaseForPenalty.containsKey(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c)){
                                   yearByCaseForPenalty.put(caseRecord.Property__c+'-'+caseRecord.AssessmentYear__c, caseRecord);
                                }   
                            }
                        }
                    } 
                }else if(propertyIdAssessmentByChild.containsKey(propertyId+'-'+currentClosingRollYear) && propertyIdAssessmentByChild.get(propertyId+'-'+currentClosingRollYear).size() > 0){
                    caseIdWithEscapeYears.add(propertyIdAssessmentByChild.get(propertyId+'-'+currentClosingRollYear)[0].Id +'-'+ assessedYear); 
                }              
            }  
           
            if(caseIdWithEscapeYears.isEmpty()) return;
            for(String caseWithEscapeYear : caseIdWithEscapeYears){
                List<String> caseIdAndEscape = caseWithEscapeYear.split('-');
                String caseId= caseIdAndEscape[0];
                if(!caseIdByEscapedYears.containsKey(caseId)){
                    caseIdByEscapedYears.put(caseId,new List<String>());
                }
                caseIdByEscapedYears.get(caseId).add(caseIdAndEscape[1]);
            }    
            
                
            if(caseIdByEscapedYears.isEmpty()) return;
            cases = caseIdByEscapedYears.keyset();
            
            // Build query to get all fields of Case and Assessment Line Items
            String query = CASE_QUERY + ' WHERE Id IN: cases';
            
            Map<Id,Case> casesToBeClonedById= new Map<Id,Case>((List<Case>)Database.query(query));
            
            // Create new list of cloned cases
            List<Case> clonedCases = new List<Case>();
            // Create a map of original Case Id(Source Id) to cloned Assessment Line Items
            Map<String, List<AssessmentLineItem__c>> clonedCaseToAssessmentLIBySourceId = new Map<String, List<AssessmentLineItem__c>>();
            
            // Setting value for new clone
            for(Case caseToClone: casesToBeClonedById.values() ){
                
                for(Integer i=0; i< caseIdByEscapedYears.get(caseToClone.Id).size(); i++){
                    //  Clone the case, perform the deep clone here
                    String currentEscapedYear = String.valueof(caseIdByEscapedYears.get(caseToClone.Id)[i]);                       
                    Case newCase = caseToClone.clone(false, true, false, false);
                    //handling legacy record type
                    if(caseToClone.RecordTypeId == assessmentLegacyRecordTypeId) {
                        newCase = LegacyDataUtility.convertAssessment(newCase,assessmentBPPRecordTypeId);
                    }			
                    newCase.Type = CCSFConstants.ASSESSMENT.TYPE_ESCAPE;
                    newCase.Status = CCSFConstants.ASSESSMENT.STATUS_NEW;
                    newCase.SubStatus__c = null;
                    newCase.IsLegacy__c = false;
                    newCase.IsLocked__c = false;
                    newCase.InterestCode__c = System.Label.InterestCodeRTCode506;
                    newCase.AssessmentNumberSequence__c = null; 
                    newCase.AssessmentNumber__c = null;
                    newCase.EventDate__c= Date.newInstance(Integer.valueOf(currentEscapedYear), 1, 1);
                    newCase.Roll__c = rollYear.Id;
                    newCase.RollYear__c = currentClosingRollYear;
                    newCase.AssessmentYear__c = currentEscapedYear;
                    newCase.WaiverExpirationDate__c = null;
                    newCase.StatuteOfLimitations__c = statuteOfLimitation;
                    newCase.ParentRegularAssessment__c = caseToClone.Id;
                    //Addedd as per bug ASR-7853 Clone assessment from Auto Proccessed should not be auto Processed 
                    newCase.AutoProcessed__c = false; 
                    //ASR-9701. Enrolled Date should be blank after cloning an assessment
            		newCase.EnrolledDate__c = null;
                    // ASR-8548
                    newCase.CancelAttempted__c = false;                  
                    
                    // as part of ASR 10048	
                    newCase.IsCorrectedOrCancelled__c = false;
                    // Marking Billable depending on Statute of Limitation
                    if(returnBillability(Integer.valueof(caseToClone.AssessmentYear__c),Integer.valueOf(currentEscapedYear),statuteOfLimitation)){
                        newCase.Billable__c = CCSFConstants.ASSESSMENT.BILLABLE_YES;
                    }else{
                        newCase.Billable__c = CCSFConstants.ASSESSMENT.BILLABLE_NO;
                    }                    
                    // Setting Assessment Owner as Statement - Assignee
                    if(propertyIdByStatementOwnerId.containsKey(caseToClone.Property__c)){
                        newCase.OwnerId =propertyIdByStatementOwnerId.get(caseToClone.Property__c);
                    }  
                    // Setting ParentId as CaseId for Previous Escape Year
                    newCase.ParentId = previousYearIdByEscapeYear.containsKey(caseToClone.Property__c+'-'+currentEscapedYear)? previousYearIdByEscapeYear.get(caseToClone.Property__c+'-'+currentEscapedYear): null;      
                    
                    if(!previousYearHasAssessment.isEmpty() && previousYearHasAssessment.containsKey(caseToClone.Property__c+'-'+currentEscapedYear)){
                        // Updated as part of bug 10215, Missing Escape will always generate NEW
                        newCase.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
                        
                        // Penality and Exemption
                        if(previousYearHasAssessment.containsKey(caseToClone.Property__c+'-'+currentEscapedYear) && previousYearHasAssessment.get(caseToClone.Property__c+'-'+currentEscapedYear) && yearByCaseForPenalty.containsKey(caseToClone.Property__c+'-'+currentEscapedYear)){
                            
                            //Setting Penalty fields
                            newCase.Penalty__c = yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).Penalty__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).Penalty__c;
                            newCase.WaivePenaltyReason__c =yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).WaivePenaltyReason__c == null? null : yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).WaivePenaltyReason__c ;
                            newCase.ApplyFraudPenalty__c=yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ApplyFraudPenalty__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ApplyFraudPenalty__c;
                            newCase.PenaltyPercent__c =yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).PenaltyPercent__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).PenaltyPercent__c;
                            newCase.ApplyPenaltyReason__c =yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ApplyPenaltyReason__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ApplyPenaltyReason__c;
                            //Setting Exemption 
                            newCase.ExemptionPenalty__c = yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionPenalty__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionPenalty__c;
                            newCase.ExemptAmountManual__c= yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptAmountManual__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptAmountManual__c;
                            newCase.PercentExempt__c= yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).PercentExempt__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).PercentExempt__c;
                            newCase.ExemptionCode__c= yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionCode__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionCode__c;
                            newCase.ExemptionDate__c= yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionDate__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionDate__c;
                            newCase.ExemptionType__c= yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionType__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionType__c;
                            newCase.ExemptionSubtype__c= yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionSubtype__c == null? null :yearByCaseForPenalty.get(caseToClone.Property__c+'-'+currentEscapedYear).ExemptionSubtype__c;    
                        }else{   
                            //Make the Penalty fields to null
                            newCase.Penalty__c = null;
                            newCase.WaivePenaltyReason__c =null;
                            newCase.ApplyFraudPenalty__c= false;
                            newCase.PenaltyPercent__c =0;
                            newCase.ApplyPenaltyReason__c = null;
                            
                            //Make all the Exemption Fields to null
                            newCase.ExemptionPenalty__c = null;
                            newCase.ExemptAmountManual__c= null;
                            newCase.PercentExempt__c= null;
                            newCase.ExemptionCode__c= null;
                            newCase.ExemptionDate__c= null;
                            newCase.ExemptionType__c= null;
                            newCase.ExemptionSubtype__c= null;
                        }
                    } else{
                        // Penality setting Null for Missing Years
                           newCase.WaivePenaltyReason__c = null;
                           newCase.Penalty__c = null;
                           newCase.ApplyFraudPenalty__c = false;
                           newCase.ApplyPenaltyReason__c = null;
                           newCase.PenaltyPercent__c =0;
                    }            
                    clonedCases.add(newCase);
                    // related AssessmentLineItem
                    if(!caseToClone.AssessmentLineItems__r.isEmpty()) {
                        List<AssessmentLineItem__c> clonedLineItems = new List<AssessmentLineItem__c>();
                        for(AssessmentLineItem__c lineItemToClone : caseToClone.AssessmentLineItems__r) {
                            AssessmentLineItem__c newAssessmentLineItem = lineItemToClone.clone(false, true, false, false);
                            // LineItem Acquisition Year must not be greater from Assessment year
                            if(currentEscapedYear >= lineItemToClone.AcquisitionYear__c ){
                                newAssessmentLineItem.DepreciationFactor__c = null;
                                newAssessmentLineItem.ReverseTrendingCostRemoved__c = null;
                                newAssessmentLineItem.IsLegacy__c= false;
                                clonedLineItems.add(newAssessmentLineItem) ;
                            }
                        }
                        clonedCaseToAssessmentLIBySourceId.put(newCase.getCloneSourceId()+'-'+currentEscapedYear, clonedLineItems);
                    }
                }
            }    
            
            AsyncDlrsCalculator.getInstance().setQueueToRun(false);
            newlyCreatedAssessmentIds= insertRecords((List<Case>)clonedCases);
            
            // Before Insert line item - update Case__c(lookup) with newly cloned Case Id's on Assessment Line Item
            List<AssessmentLineItem__c> clonedAssessmentLItems = new List<AssessmentLineItem__c>();
            if(!clonedCases.isEmpty()){
                for(Case clonedCase : clonedCases) {
                    String assessedYear = clonedCase.AssessmentYear__c;
                    String sourceCaseId = clonedCase.getCloneSourceId();
                    if(clonedCaseToAssessmentLIBySourceId.containsKey(sourceCaseId+'-'+assessedYear)) {
                        for(AssessmentLineItem__c lineItem : clonedCaseToAssessmentLIBySourceId.get(sourceCaseId+'-'+assessedYear)) {
                            lineItem.Case__c = clonedCase.Id;
                            clonedAssessmentLItems.add(lineItem);
                        }
                    }
                }  
            }
            // Insertion of AssessmentLineItem
            List<Id> insertedlineItemsIds = insertRecords((List<AssessmentLineItem__c>)clonedAssessmentLItems);
            
            // To update Notice Type to Low value depending on Threshold field of Roll year
            if(!newlyCreatedAssessmentIds.isEmpty()){
                for(Case escapedRecord:[Select Id,TotalAssessedCost__c,TotalAssessedValue__c,AssessmentYear__c from Case where Id In:newlyCreatedAssessmentIds]){
                    if(escapedRecord.TotalAssessedValue__c == null) continue;
                    if(rollYear.Threshold__c == null) continue;
                    if(escapedRecord.TotalAssessedValue__c > rollYear.Threshold__c) continue;
                    escapedRecord.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_LOWVALUE; //ASR-8551 Changes
                    noticeUpdateForEscapes.add(escapedRecord);
                }     
            }
            updateRecords(noticeUpdateForEscapes);                       
            AsyncDlrsCalculator.getInstance().setQueueToRun(true);
        }catch(Exception ex){  
            Logger.addExceptionEntry(ex, originLocation);            
            throw ex;
        }    
        Logger.saveLog();
    }
    // for future can be used to - send mails
    public void finish(Database.BatchableContext bc){
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'rollYear' => this.rollYear});                                
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, EscapeAssessmentBatchGenerator.class.getName(), asyncList[0].ParentJobId, true, queryParams, asyncList[0].NumberOfErrors);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}

    //Method to insert records
    private static List<Id> insertRecords(List<sObject> records){
        List<Id> insertedRecordIds=new List<Id>();
        if(records.isEmpty()) return insertedRecordIds;
        List<Database.SaveResult> insertResults = Database.insert(records, false); 
        for(Integer i=0;i<insertResults.size();i++){
            if (insertResults.get(i).isSuccess()){
                insertedRecordIds.add(insertResults.get(i).getId()); continue; 
            }else{
                // DML operation failed
                Database.Error error = insertResults.get(i).getErrors().get(0);
                String failedDML = error.getMessage();
                Logger.addDebugEntry(failedDML,'failedDML');
            }                    
        }          
        return insertedRecordIds;
    }
    
    //Method to update records
    private static void updateRecords(List<Case> records){
        if(records.isEmpty()) return;
        List<Database.SaveResult> updateResults = Database.update(records,false);  
        for(Integer i=0;i<updateResults.size();i++){
            if (updateResults.get(i).isSuccess()){
                continue;
            }else if (!updateResults.get(i).isSuccess()){
                // DML operation failed
                Database.Error error = updateResults.get(i).getErrors().get(0);
                String failedDML = error.getMessage();
                Logger.addDebugEntry(failedDML,'failedDML');                        
            }                    
        }               
    }
    
    // Method to return Years difference between Start year of Business Location to Current Roll Closing Year
    private static Set<Integer> returnYearsBetweenStartAndAssessementYear(Integer startofBusinessYear, Integer currentAssessmentYear){
        Set<Integer> years = new Set<Integer>();
        for(Integer i = startofBusinessYear; i<= currentAssessmentYear; i++){
            years.add(i);
        }
        return years;
    }
    
    // Method to calculate Billable based on Statute of Limitation
    private static Boolean returnBillability(Integer closingYear, Integer escapeYear, Integer statuteOfLimiationValue){
        Boolean isYes;
        Integer differenceBetweenYearsInMonth=(closingYear-escapeYear)*12;
        if(differenceBetweenYearsInMonth < statuteOfLimiationValue){    
            isYes = true;
        }else{
            isYes = false;
        }        
        return isYes;
    }
    
    // Method to return if Assessment is grossly under reported
    private static Boolean returnPercentageChange(Decimal previousTotalAssessedCost, Decimal  currentTotalAssessedCost, Decimal GrosslyUnderreported){
        if(previousTotalAssessedCost == 0 && currentTotalAssessedCost == 0) return false;
        if(previousTotalAssessedCost == 0 ) return true;
        Boolean isIncreasedGrosslyUnderreported = false;
        Decimal changeInPercentage = ((currentTotalAssessedCost- previousTotalAssessedCost)/ previousTotalAssessedCost) * 100;
        if(changeInPercentage >= GrosslyUnderreported){
            isIncreasedGrosslyUnderreported = true;
        }
        return isIncreasedGrosslyUnderreported;
    } 
    
}