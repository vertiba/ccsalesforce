/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// Class to test StatementNewFormController
//-----------------------------
@isTest
public class StatementNewFormControllerTest {
    
     @TestSetup
    static void dataForTesting(){
       		List<Account> accounts = new List<Account>(); 
            Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
            Account testAccount = new Account();
            testAccount.Name = 'Test Account';
            testAccount.BusinessStatus__c = 'Active';
            testAccount.NoticeType__c ='Notice to File';
            testAccount.RequiredToFile__c ='Yes';
            testAccount.RecordTypeId = accRecordTypeId;
            accounts.add(testAccount);            
            Database.insert(accounts);
        
        List<Property__c> properties = new List<Property__c>();            
        Id proRecordTypeId  = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Property__c testProperty =  new Property__c();
        testProperty.RecordTypeId = proRecordTypeId;
        testProperty.Account__c = testAccount.Id;
        testProperty.DoingBusinessAs__c = 'Chemicals';
        testProperty.Status__c ='Active';
        testProperty.RollCode__c ='Unsecured';
        testProperty.AssessorsParcelNumber__c ='1234A567';
        testProperty.DiscoverySource__c ='Audit';
        properties.add(testProperty);
        Database.insert(properties); 
        
        Date filingDate = System.today();            
        Id statementRecordTypeId  = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('PropertyEventStatement').getRecordTypeId();
        
        Statement__c testStatement = new Statement__c();
        testStatement.RecordTypeId = statementRecordTypeId;
        testStatement.BusinessAccount__c = testProperty.Account__c;
        testStatement.Property__c = testProperty.Id;
        testStatement.FileDate__c = filingDate; 
        Database.insert(testStatement);           
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Tests that StatementNewFormController.getListViews list view of the Statement object.
    // @return : void
    //-----------------------------
    
    @isTest
    static void getStatementAssessmentYearTest() { 
    List<ListView> views = StatementNewFormController.getListViews();
        system.assert(views.size()>0);
        Boolean isAllListViewExist = false;
        for(ListView allListView : views){
            if(allListView.Name == 'All'){
                isAllListViewExist = true;
            }
        }
        system.assertEquals(true, isAllListViewExist,'All view exist');     
    }
    
    @isTest
    static void getDefaultRecordTypeTest() { 
        Statement__c statement = [SELECT Id, Name, RecordType.Developername FROM Statement__c];
        Test.startTest();
        system.assert(statement !=Null,'Statement records.');
        Id recId = StatementNewFormController.getDefaultRecordType();
        Statement__c statementRec = StatementNewFormController.getStatementRecordType(statement.Id);
        system.assertEquals('PropertyEventStatement', statementRec.RecordType.Developername); 
        Test.stopTest();
        Statement__c statement1 = [SELECT Id, Name, RecordTypeId FROM Statement__c];
        system.assertEquals(recId, statement1.RecordTypeId, 'Default RecordType of Statement Records.');
    }
}