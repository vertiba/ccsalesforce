/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Batch job to trigger DLRS-maintained fields to be recalculated. These fields are normally updated
//          via triggers, but we need to be able to rerun the DLRS rules during 2 scenarios:
//              1. Triggers are disabled during data migration (to avoid row locks), so the rules need to be recalculated after data migration
//              2. If/when we change the criteria for an existing DLRS rule, we need to recalculate the values using the updated rule's logic
//-----------------------------
public without sharing class RollupFieldsBatchCalculator implements Database.Batchable<SObject>, Database.Stateful {

    private static String LOG_LOCATION = 'RollupFieldsBatchCalculator';

    public static void startBatchCalculation(List<ChildSObject> childSObjects) {
        ChildSObject initialChildSObject = childSObjects.get(0);
        Database.executeBatch(new RollupFieldsBatchCalculator(childSObjects), initialChildSObject.batchSize);
    }

    public class ChildSObject {
        private Schema.SObjectType sobjectType;
        private Schema.SObjectField orderByField;
        private Integer batchSize;

        public ChildSObject(Schema.SObjectType sobjectType, Schema.SObjectField orderByField) {
            this(sobjectType, orderByField, 2000);
        }

        public ChildSObject(Schema.SObjectType sobjectType, Schema.SObjectField orderByField, Integer batchSize) {
            this.sobjectType  = sobjectType;
            this.orderByField = orderByField;
            this.batchSize    = batchSize;
        }
    }

    private List<ChildSObject> childSObjects;

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : childSObjects    List<RollupFieldsBatchCalculator.ChildSObject> This list controls the order of updating SObjects, starting with the lowest SObject in the hierarchy.
    // @description : Constructor with 1 parameter, used to indicate the order of SObjects to recalculate.
    //-----------------------------
    private RollupFieldsBatchCalculator(List<ChildSObject> childSObjects) {
        this.childSObjects = childSObjects;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : Database.QueryLocator
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        ChildSObject childSObject = this.childSObjects.get(0);
        this.childSObjects.remove(0);

        // Disable automation before updating records to minimize CPU time
        this.disableAutomationCustomSettings(true);

        return Database.getQueryLocator(this.getQuery(childSObject));
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : List<SObject>
    // @description : This method receives a set of records queried from the currentSObjectType.
    //      DLRS rules applied by calling dlrs.RollupService class. All DLRS rules on the object are then calculated.
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext batchableContext, List<SObject> scope) {
        Logger.addDebugEntry('scope=' + scope, LOG_LOCATION);

        // Run DLRS rules
        List<SObject> parentRecords = dlrs.RollupService.rollup(scope);
        Logger.addDebugEntry('parentRecords=' + parentRecords, LOG_LOCATION);
        Database.update(parentRecords);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method. If additional SObjects need to be recalculated, a new instance is executed
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext batchableContext) {
        // Re-enable automation after updating records
        this.disableAutomationCustomSettings(false);

        // If there are additional child SObjects to calculate, then start a new instance of the batch job
        if(this.childSObjects.isEmpty() == false) {
            startBatchCalculation(this.childSObjects);
        }
    }

    private String getQuery(ChildSObject childSObject) {
        String sobjectName      = String.valueOf(childSObject.sobjectType);
        String orderByFieldName = String.valueOf(childSObject.orderByField);

        Set<String> fieldNames = new Set<String>{'Id'};

        List<dlrs__LookupRollupSummary2__mdt> dlrsRules = [
            SELECT Id, dlrs__RelationshipField__c
            FROM dlrs__LookupRollupSummary2__mdt
            WHERE dlrs__ChildObject__c = :sobjectName
            AND dlrs__Active__c = true
            AND dlrs__CalculationMode__c = 'Developer'
        ];
        for(dlrs__LookupRollupSummary2__mdt dlrsRule : dlrsRules) {
            fieldNames.add(dlrsRule.dlrs__RelationshipField__c);
        }

        return 'SELECT ' + String.join(new List<String>(fieldNames), ',') + ' FROM ' + sobjectName;// + ' ORDER BY ' + orderByFieldName;
    }

    private void disableAutomationCustomSettings(Boolean disable) {
        TriggerHandlerSettings__c triggerHandlerSettings = TriggerHandlerSettings__c.getInstance(UserInfo.getUserId());
        triggerHandlerSettings.DisableAllTriggers__c             = disable;
        triggerHandlerSettings.DisableDLRSCalculationTriggers__c = disable;
        upsert triggerHandlerSettings;

        ProcessBuilderSettings__c processBuilderSettings = ProcessBuilderSettings__c.getInstance(UserInfo.getUserId());
        processBuilderSettings.DisableAllProcesses__c = disable;
        upsert processBuilderSettings;
    }

}