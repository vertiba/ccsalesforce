/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to Scheduler to run Exemption notices.
//-----------------------------
public class ExemptionNoticeGeneratorScheduler extends EnhancedSchedulable {

    private String exemptionNoticeType;

    public override String getJobNamePrefix() {
        return 'Exemption Notice Generator - ' + this.exemptionNoticeType;
    }

    public ExemptionNoticeGeneratorScheduler(String noticeType){
        this.exemptionNoticeType = noticeType;
    }

    public ExemptionNoticeGeneratorScheduler(){
        this(Label.InitialExemptionNotice);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : SchedulableContext
    // @description : Schedulable interface method. Schedules the ExemptionNoticeGeneratorBatch job,
    // @return : void
    //-----------------------------
    public void execute(SchedulableContext schedulableContext) {
        Logger.addDebugEntry('After Entering ExemptionNoticeGeneratorBatch:', 'ExemptionNoticeGeneratorBatch.execute');
        Id batchProcessId = Database.executebatch(new ExemptionNoticeGeneratorBatch(exemptionNoticeType));
        Logger.addDebugEntry('After Scheduling bacth, jobid:'+batchProcessId, 'ExemptionNoticeGeneratorBatch.execute');
        Logger.saveLog();
    }

}