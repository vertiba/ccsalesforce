/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: DataPurgeTest 
 * Description : Test Class for DataPurge and DataPurgeMarine Class
*/
@isTest
public class DataPurgeTest {

    /*
     * Method Name: disableAutomationCustomSettings
     * Description: This method disables the DLRS settings for test class validation purposes. 
     */ 
    private static void disableAutomationCustomSettings(Boolean disable) {
        TriggerHandlerSettings__c triggerHandlerSettings = TriggerHandlerSettings__c.getInstance(UserInfo.getUserId());
        triggerHandlerSettings.DisableAllTriggers__c             = disable;
        triggerHandlerSettings.DisableDLRSCalculationTriggers__c = disable;
        upsert triggerHandlerSettings;
        
        ProcessBuilderSettings__c processBuilderSettings = ProcessBuilderSettings__c.getInstance(UserInfo.getUserId());
        processBuilderSettings.DisableAllProcesses__c = disable;
        upsert processBuilderSettings;
        
        ValidationRuleSettings__c validationRuleSettings = ValidationRuleSettings__c.getInstance(UserInfo.getUserId());
        validationRuleSettings.DisableValidationRules__c = disable;
        upsert validationRuleSettings;
    }
    
    /*
     * Method Name: purgeTest
     * Description: This method tests all the functions in the DataPurge class 
     */ 
    public static TestMethod void purgeTest(){
        disableAutomationCustomSettings(true);

        List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty1 = new Penalty__c();
        penalty1.PenaltyMaxAmount__c = 500;
        penalty1.Percent__c = 10;
        penalty1.RTCode__c = '463';
        penalties.add(penalty1);

        Penalty__c penalty2 = new Penalty__c();
        penalty2.PenaltyMaxAmount__c = 500;
        penalty2.Percent__c = 25;
        penalty2.RTCode__c = '214.13';
        penalties.add(penalty2);

        Penalty__c penalty3 = new Penalty__c();
        penalty3.PenaltyMaxAmount__c = 500;
        penalty3.Percent__c = 25;
        penalty3.RTCode__c = '504';
        penalties.add(penalty3);
		insert penalties;

        Account accountRecord = new Account();
        accountRecord.Name='New Test Account';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord.EntityId__c = '654321';
        accountRecord.BusinessStatus__c='active';
        accountRecord.MailingCountry__C='US';
        accountRecord.MailingStreetName__c ='355';
        accountRecord.MailingCity__c ='Oakland';
        accountRecord.NoticeType__c='Notice to File';
        accountRecord.RequiredToFile__c='Yes';
        accountRecord.NoticeLastGenerated__c = system.now();
        insert accountRecord;

        Account accountRecord2 = new Account();
        accountRecord2.Name='New Test Account 2';
        accountRecord2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord2.BusinessStatus__c='active';
        accountRecord2.EntityId__c = '451123';
        accountRecord2.MailingCountry__C='US';
        accountRecord2.MailingStreetName__c ='355';
        accountRecord2.MailingCity__c ='Oakland';
        accountRecord2.NoticeType__c='Notice to File';
        accountRecord2.RequiredToFile__c='Yes';
        insert accountRecord2;
        
        Property__c propertyRecord = new Property__c();
        propertyRecord.Name = 'new test property';
        propertyRecord.PropertyId__c = '153536';
        propertyRecord.MailingCountry__c = 'US';
        propertyRecord.Account__c = accountRecord.Id;
        insert propertyRecord;

        Property__c propertyRecord2 = new Property__c();
        propertyRecord2.Name = 'new test property 2';
        propertyRecord2.PropertyId__c = '531536';
        propertyRecord2.MailingCountry__c = 'US';
        propertyRecord2.Account__c = accountRecord2.Id;
        insert propertyRecord2;
        
        String fiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear), 5, 30);

        Statement__c statement = new Statement__c();
        statement.FileDate__c = filingDate;
        statement.Property__c = propertyRecord.id;
        statement.PropertyScenario__c = 'Existing';
        statement.RecordTypeId = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalPropertyStatement').getRecordTypeId();
        insert statement;

        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Status__c='Roll Open';
        insert rollYear;

        RollYear__c prevrollYear = new RollYear__c();
        prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Status__c='Roll Closed';
        insert prevrollYear;

        RollYear__c nextrollYear = new RollYear__c();
        nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Status__c='Pending';
        insert nextrollYear;

        List<Case> casesToInsert = new List<Case>();
        Id bppAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        Case caseRecord1 = new Case();
        caseRecord1.AccountName__c='new test account';
        caseRecord1.type = 'Regular';
        caseRecord1.AccountId = propertyRecord.account__c;
        caseRecord1.ApplyFraudPenalty__c = true;
        caseRecord1.RecordTypeId = bppAssessmentRecordTypeId;
        caseRecord1.Property__c = propertyRecord.Id;
        caseRecord1.Status = 'In Progress';
        caseRecord1.AssessmentYear__c ='2017';
        caseRecord1.EventDate__c = Date.valueOf('2019-11-26');
        caseRecord1.MailingCountry__c = 'US';
        casesToInsert.add(caseRecord1);

        Case caseRecord2 = new Case();
        caseRecord2.AccountName__c='new test account';
        caseRecord2.type = 'Escape';
        caseRecord2.AccountId = propertyRecord.account__c;
        caseRecord2.RecordTypeId = bppAssessmentRecordTypeId;
        caseRecord2.Property__c = propertyRecord.Id;
        caseRecord2.Status = 'In Progress';
        caseRecord2.AssessmentYear__c ='2017';
        caseRecord2.EventDate__c = Date.valueOf('2019-11-26');
        caseRecord2.MailingCountry__c = 'US';
        caseRecord2.ApplyFraudPenalty__c = false;
        casesToInsert.add(caseRecord2);

        Id vesselAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VesselAssessment').getRecordTypeId();
        Case caseRecord = new Case();
        caseRecord.AccountId = propertyRecord.Account__c;
        caseRecord.Property__c = propertyRecord.id;
        caseRecord.MailingCountry__c = 'US';
        caseRecord.Type = 'Regular';
        caseRecord.penalty__c = penalty1.id;
        caseRecord.RecordTypeId= vesselAssessmentRecordTypeId;
        caseRecord.filedate__c = Date.newInstance(2020, 05, 08);
		casesToInsert.add(caseRecord);
		database.insert(casesToInsert);
		
        DocumentGenerationRequest__c documentGeneration = new DocumentGenerationRequest__c();
        documentGeneration.Account__c = accountRecord.Id;
        database.insert(documentGeneration);
        
        DocumentGenerationRequest__c documentGeneration2 = new DocumentGenerationRequest__c();
        documentGeneration2.property__c = propertyRecord.Id;
        database.insert(documentGeneration2);
        
        DocumentGenerationRequest__c documentGeneration3 = new DocumentGenerationRequest__c();
        documentGeneration3.Account__c = accountRecord2.Id;
        database.insert(documentGeneration3);
        
        StagingAumentumBusiness__c stagingBusiness = new StagingAumentumBusiness__c();
        stagingBusiness.ExtractDate__c = String.valueOf(system.today());
        stagingBusiness.MappedAccount__c = accountRecord.Id;
        database.insert(stagingBusiness);
        
        StagingAumentumBusiness__c stagingBusiness2 = new StagingAumentumBusiness__c();
        stagingBusiness2.ExtractDate__c = String.valueOf(system.today());
        stagingBusiness2.MappedProperty__c = propertyRecord.Id;
        database.insert(stagingBusiness2);
        
        StagingBusinessCompareField__c businessCompare = new StagingBusinessCompareField__c();
        businessCompare.ParentStagingAumentumBusiness__c = stagingBusiness.Id;
        businessCompare.MappedBusiness__c = accountRecord.Id;
        businessCompare.MappedProperty__c = propertyRecord.Id;
        database.insert(businessCompare);
        
        StagingBusinessCompareField__c businessCompare2 = new StagingBusinessCompareField__c();
        businessCompare2.ParentStagingAumentumBusiness__c = stagingBusiness2.Id;
        businessCompare2.MappedBusiness__c = accountRecord.Id;
        businessCompare2.MappedProperty__c = propertyRecord.Id;
        database.insert(businessCompare2);
        
        Ownership__c ownership = new Ownership__c();
        ownership.StartDate__c = system.today();
        ownership.Account__c = accountRecord.Id;
        database.insert(ownership);
        
        PropertyTransaction__c propertyTransaction = new PropertyTransaction__c();
        propertyTransaction.Property__c = propertyRecord.Id;
        propertyTransaction.PurchaseAmount__c = 200;
        database.insert(propertyTransaction);
        
        OwnershipTransaction__c ownershipTransaction = new OwnershipTransaction__c();
        ownershipTransaction.Ownership__c = ownership.Id;
        ownershipTransaction.PropertyTransaction__c = propertyTransaction.Id;
        ownershipTransaction.Role__c = 'Buyer';
        database.insert(ownershipTransaction);
        
        ContentVersion content = new ContentVersion(); 
        content.Title = 'Header_Picture1'; 
        content.PathOnClient='/' + content.Title + '.jpg'; 
        Blob bodyBlob = Blob.valueOf('Unit Test ContentVersion Body'); 
        content.VersionData = bodyBlob; 
        content.origin = 'H';
        database.insert(content);
        
        ContentNote note = new ContentNote();
        note.Title = 'Sample';
        note.Content = bodyBlob;
        database.insert(note);
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId = propertyRecord.Id;
        contentlink.contentdocumentid = note.Id;
        contentlink.ShareType = 'I';
        contentlink.Visibility = 'AllUsers'; 
        database.insert(contentlink);
        
        disableAutomationCustomSettings(false);
        
        Test.startTest();
        RestRequest request = new RestRequest(); 
        RestResponse response = new RestResponse();             
        request.requestURI = '/services/apexrest/AccountsPurgeByEntity/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('{"entityIds" :["654321"]}');
        RestContext.request = request;
        RestContext.response= response;
        Map<String, String> deletedObjectNameByCount = AccountPurgeByEntityIdRest.postMethod();
        
        
        RestRequest request2 = new RestRequest(); 
        RestResponse response2 = new RestResponse();             
        request2.requestURI = '/services/apexrest/PropertiesPurgeByLegacyProperty/';
        request2.httpMethod = 'POST';
        request2.requestBody = Blob.valueof('{"entityIds" :["531536"]}');
        RestContext.request = request2;
        RestContext.response = response2;
        Map<String, String> deletedObjectNameByCount2 = PropertiesPurgeByLegacyPropertyRest.postMethod();
        
        DataPurge purgeInstance = new DataPurge();
        purgeInstance.deleteDocumentGenerationRequestsByAccountIds(new Set<Id>{accountRecord2.Id});
        System.assertEquals(1, [Select Id From Account where EntityId__c='654321'].size());
        Test.stopTest();
        
    }
    
    /*
     * Method Name: purgeMarineTest
     * Description: This method tests all the functions in the DataPurgeMarine class 
     */ 
    public static TestMethod void purgeMarineTest(){
        disableAutomationCustomSettings(true);

        List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty1 = new Penalty__c();
        penalty1.PenaltyMaxAmount__c = 500;
        penalty1.Percent__c = 10;
        penalty1.RTCode__c = '463';
        penalties.add(penalty1);

        Penalty__c penalty2 = new Penalty__c();
        penalty2.PenaltyMaxAmount__c = 500;
        penalty2.Percent__c = 25;
        penalty2.RTCode__c = '214.13';
        penalties.add(penalty2);

        Penalty__c penalty3 = new Penalty__c();
        penalty3.PenaltyMaxAmount__c = 500;
        penalty3.Percent__c = 25;
        penalty3.RTCode__c = '504';
        penalties.add(penalty3);
		insert penalties;

        Account accountRecord = new Account();
        accountRecord.Name='New Test Account';
        accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord.EntityId__c = '654321';
        accountRecord.BusinessStatus__c='active';
        accountRecord.MailingCountry__C='US';
        accountRecord.MailingStreetName__c ='355';
        accountRecord.MailingCity__c ='Oakland';
        accountRecord.NoticeType__c='Notice to File';
        accountRecord.RequiredToFile__c='Yes';
        accountRecord.NoticeLastGenerated__c = system.now();
        insert accountRecord;

        Account accountRecord2 = new Account();
        accountRecord2.Name='New Test Account 2';
        accountRecord2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accountRecord2.BusinessStatus__c='active';
        accountRecord2.EntityId__c = '451123';
        accountRecord2.MailingCountry__C='US';
        accountRecord2.MailingStreetName__c ='355';
        accountRecord2.MailingCity__c ='Oakland';
        accountRecord2.NoticeType__c='Notice to File';
        accountRecord2.RequiredToFile__c='Yes';
        insert accountRecord2;
        
        Id vesselRecordTypeId= Schema.SObjectType.property__c.getRecordTypeInfosByDeveloperName().get('Vessel').getRecordTypeId(); 
        Property__c propertyRecord = new Property__c();
        propertyRecord.Name = 'new test property';
        propertyRecord.RecordTypeId = vesselRecordTypeId;
        propertyRecord.PropertyId__c = '153536';
        propertyRecord.MailingCountry__c = 'US';
        propertyRecord.Account__c = accountRecord.Id;
        insert propertyRecord;

        Property__c propertyRecord2 = new Property__c();
        propertyRecord2.Name = 'new test property 2';
        propertyRecord2.RecordTypeId = vesselRecordTypeId;
        propertyRecord2.PropertyId__c = '531536';
        propertyRecord2.MailingCountry__c = 'US';
        propertyRecord2.Account__c = accountRecord2.Id;
        insert propertyRecord2;
        
        PropertyEvent__c propertyEvent = new PropertyEvent__c();
        propertyEvent.Property__c = propertyRecord2.Id;
        propertyEvent.PropertyAccount__c = accountRecord2.Id;
        propertyEvent.Type__c = 'Vessel';
        database.insert(propertyEvent);
        
        String fiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear), 5, 30);

        Statement__c statement = new Statement__c();
        statement.FileDate__c = filingDate;
        statement.Property__c = propertyRecord2.id;
        statement.PropertyScenario__c = 'Existing';
        statement.RecordTypeId = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('VesselStatement').getRecordTypeId();
        insert statement;

        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Status__c='Roll Open';
        insert rollYear;

        RollYear__c prevrollYear = new RollYear__c();
        prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        prevrollYear.Status__c='Roll Closed';
        insert prevrollYear;

        RollYear__c nextrollYear = new RollYear__c();
        nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
        nextrollYear.Status__c='Pending';
        insert nextrollYear;

        List<Case> casesToInsert = new List<Case>();
        Id vesselAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VesselAssessment').getRecordTypeId();
        Case caseRecord = new Case();
        caseRecord.AccountId = propertyRecord.Account__c;
        caseRecord.Property__c = propertyRecord2.id;
        caseRecord.MailingCountry__c = 'US';
        caseRecord.Type = 'Regular';
        caseRecord.penalty__c = penalty1.id;
        caseRecord.RecordTypeId= vesselAssessmentRecordTypeId;
        caseRecord.filedate__c = Date.newInstance(2020, 05, 08);
		casesToInsert.add(caseRecord);
		database.insert(casesToInsert);
		
        DocumentGenerationRequest__c documentGeneration = new DocumentGenerationRequest__c();
        documentGeneration.Account__c = accountRecord.Id;
        database.insert(documentGeneration);
        
        DocumentGenerationRequest__c documentGeneration2 = new DocumentGenerationRequest__c();
        documentGeneration2.property__c = propertyRecord.Id;
        database.insert(documentGeneration2);
        
        DocumentGenerationRequest__c documentGeneration3 = new DocumentGenerationRequest__c();
        documentGeneration3.Account__c = accountRecord2.Id;
        database.insert(documentGeneration3);
        
        Ownership__c ownership = new Ownership__c();
        ownership.StartDate__c = system.today();
        ownership.Account__c = accountRecord2.Id;
        ownership.property__c = propertyRecord2.Id;
        database.insert(ownership);
        
        PropertyTransaction__c propertyTransaction = new PropertyTransaction__c();
        propertyTransaction.Property__c = propertyRecord2.Id;
        propertyTransaction.PurchaseAmount__c = 200;
        database.insert(propertyTransaction);
        
        OwnershipTransaction__c ownershipTransaction = new OwnershipTransaction__c();
        ownershipTransaction.Ownership__c = ownership.Id;
        ownershipTransaction.PropertyTransaction__c = propertyTransaction.Id;
        ownershipTransaction.Role__c = 'Buyer';
        database.insert(ownershipTransaction);
        
        ContentVersion content = new ContentVersion(); 
        content.Title = 'Header_Picture1'; 
        content.PathOnClient='/' + content.Title + '.jpg'; 
        Blob bodyBlob = Blob.valueOf('Unit Test ContentVersion Body'); 
        content.VersionData = bodyBlob; 
        content.origin = 'H';
        database.insert(content);
        
        ContentNote note = new ContentNote();
        note.Title = 'Sample';
        note.Content = bodyBlob;
        database.insert(note);
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId = propertyRecord2.Id;
        contentlink.contentdocumentid = note.Id;
        contentlink.ShareType = 'I';
        contentlink.Visibility = 'AllUsers'; 
        database.insert(contentlink);
        
        disableAutomationCustomSettings(false);
        
        Test.startTest();
        RestRequest request = new RestRequest(); 
        RestResponse response = new RestResponse();             
        request.requestURI = '/services/apexrest/AccountsPurgeForMarineByEntity/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('{"entityIds" :["451123"]}');
        RestContext.request = request;
        RestContext.response= response;
        Map<String, String> deletedObjectNameByCount = AccountPurgeByEntityIdForMarineRest.postMethod();
        
        DataPurgeForMarine purgeInstance = new DataPurgeForMarine();
        purgeInstance.deletePropertiesByLegacyProperty(new List<String>{'153536'});
        System.assertEquals(0, [Select Id From Account where EntityId__c='451123'].size());
        Test.stopTest();
    }
    
}