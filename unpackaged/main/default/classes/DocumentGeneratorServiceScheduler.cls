/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class DocumentGeneratorServiceScheduler extends EnhancedSchedulable {

    public override String getJobNamePrefix() {
        return 'Document Generator Service';
    }

    public void execute(SchedulableContext schedulableContext) {
        // Salesforce has a limit of 5 running batch jobs
        // If there are already 5 jobs running, then don't run this job
        // Any records that need to be processed will be processed the next time the job executes
        if(EnhancedSchedulable.getNumberOfRunningBatchJobs() >= 5) return;

        // The DocumentGeneratorService handles processing all requests, so only 1 instance should execute at a time
        // Multiple instances of the job running could result in duplicate documents being generated
        String batchClassName = DocumentGeneratorService.class.getName();
        if(EnhancedSchedulable.getNumberOfBatchJobsForClass(batchClassName) >= 1) return;

        Integer batchSize = Limits.getLimitCallouts();
        Database.executebatch(new DocumentGeneratorService(), 1);
    }

}