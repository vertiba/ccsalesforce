/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/*
* Class Name : GeneralEnquiryUpdatedController
* Author: Publicis Sapient
* Description: This class is the controller for the LWComponent 'GeneralEnquiryCaseCreation'. In this class we are inserting cases based on the type of selected.
*/
public with sharing class GeneralEnquiryUpdatedController {
    // Declaraing static Strings
    public static final string CASE_CREATED = 'Case Created';
    public static final string WEB = 'Web';
    public static final String originLocation = 'GeneralEnquiryUpdatedController';
    
    @AuraEnabled
    /*
* Method Name:createEnquiry
* Description : Create a Customer Service Case based on type selected by the User
*/
    public static string createEnquiry(String objectMap){
        string responseMessage = '';
        enquiryWrapper wrapper = (enquiryWrapper) JSON.deserialize(objectMap,enquiryWrapper.class);
        Id customerServiceRecordTypeId = [SELECT Id, Name, SobjectType FROM RecordType WHERE SobjectType='Case' AND name='Customer Service'].Id;
        Id csQueueID = [Select Id, Name From Group Where type = :CCSFConstants.typeQueue and Name = :CCSFConstants.customerSupportQueueName].Id;
        ID taxpayerContactId = [Select contactid from User where id =:Userinfo.getUserid()].contactId;
        List<Contact> taxpayerContactList  = [Select AccountID from Contact where id = :taxpayerContactId];
        ID taxpayerAccountID;
        if (taxpayerContactList.size() > 0) {
            taxpayerAccountID = taxpayerContactList[0].AccountID;
        }
        
        if(wrapper.selectedType == 'Request Replacement Notice'){
            try{
                Case caseRecord = createCaseInstance(wrapper, customerServiceRecordTypeId, csQueueID, taxpayerAccountID);
                database.insert(caseRecord);
                String caseNumber = getCaseNumber(caseRecord.Id);
                
                if(wrapper.contentDocumentId != null && wrapper.contentDocumentId != '' && caseRecord.Id != null){
                    insertContentDocument(wrapper,caseRecord.Id);
                }
                responseMessage = GeneralEnquiryUpdatedController.CASE_CREATED+'-'+caseNumber;
            }catch(Exception ex){
                Logger.addExceptionEntry(ex, originLocation);
                throw new AuraHandledException(ex.getMessage());
            }
        }else if(wrapper.selectedType == 'Request New Business'){
            try{
                Case caseRecord = createCaseInstance(wrapper, customerServiceRecordTypeId, csQueueID, taxpayerAccountID);
                database.insert(caseRecord);
                String caseNumber = getCaseNumber(caseRecord.Id);
                
                if(wrapper.contentDocumentId != null && wrapper.contentDocumentId != '' && caseRecord.Id != null){
                    insertContentDocument(wrapper,caseRecord.Id);
                }
                responseMessage = GeneralEnquiryUpdatedController.CASE_CREATED+'-'+caseNumber;
            }catch(Exception ex){
                Logger.addExceptionEntry(ex, originLocation);
                throw new AuraHandledException(ex.getMessage());
            }
        }else if(wrapper.selectedType == 'Request New Vessel'){
            try{
                Case caseRecord = createCaseInstance(wrapper, customerServiceRecordTypeId, csQueueID, taxpayerAccountID);
                database.insert(caseRecord);
                String caseNumber = getCaseNumber(caseRecord.Id);
                
                if(wrapper.contentDocumentId != null && wrapper.contentDocumentId != '' && caseRecord.Id != null){
                    insertContentDocument(wrapper,caseRecord.Id);
                }
                responseMessage = GeneralEnquiryUpdatedController.CASE_CREATED+'-'+caseNumber;
            }catch(Exception ex){
                Logger.addExceptionEntry(ex, originLocation);
                throw new AuraHandledException(ex.getMessage());
            }
            
        }else if(wrapper.selectedType == 'Other'){
            try{
                Case caseRecord = createCaseInstance(wrapper, customerServiceRecordTypeId, csQueueID, taxpayerAccountID);
                database.insert(caseRecord);
                String caseNumber = getCaseNumber(caseRecord.Id);
                
                if(wrapper.contentDocumentId != null && wrapper.contentDocumentId != '' && caseRecord.Id != null){
                    insertContentDocument(wrapper,caseRecord.Id);
                }
                responseMessage = GeneralEnquiryUpdatedController.CASE_CREATED+'-'+caseNumber;
            }catch(Exception ex){
                Logger.addExceptionEntry(ex, originLocation);
                throw new AuraHandledException(ex.getMessage());
            }
        }
        
        return responseMessage;
    }
    
    // Fetch Case Number
    public static String getCaseNumber(String caseRecordId){
        String caseNumber = '';
        
        if(String.isNotBlank(caseRecordId)){
            List<Case> cases = [Select Id, caseNumber from Case where Id=:caseRecordId];
            
            if(!cases.isEmpty()){
                caseNumber = cases[0].caseNumber;
            }
        }
        
        return caseNumber;
    }
    // Create Generic Case Instance
    public static Case createCaseInstance(enquiryWrapper wrapper, Id customerServiceRecordTypeId, Id qID, Id taxpayerAccountID){
        Case caseRecord = new Case();
        caseRecord.recordtypeId = customerServiceRecordTypeId;
        caseRecord.origin = GeneralEnquiryUpdatedController.WEB;
        caseRecord.subject = wrapper.subject;
        caseRecord.description = wrapper.description;
        caseRecord.Type = wrapper.selectedType;
        caseRecord.OwnerID = qID;
        if (taxpayerAccountID != null){
            caseRecord.AccountID = taxpayerAccountID;
        }
        
        if(wrapper.accountName != null && wrapper.accountName!=''){
            caseRecord.AccountName__c = wrapper.accountName;    
        }if(wrapper.doingBusiness != null && wrapper.doingBusiness!=''){
            caseRecord.DoingBusinessAs__c = wrapper.doingBusiness;
        }if(wrapper.newBusinessLocation != null && wrapper.newBusinessLocation!=''){
            caseRecord.NewSitusAddress__c = wrapper.newBusinessLocation;
        }if(wrapper.vesselName != null && wrapper.vesselName!=''){
            caseRecord.VesselName__c = wrapper.vesselName;
        }if(wrapper.vesselId != null && wrapper.vesselId!=''){
            caseRecord.VesselId__c = wrapper.vesselId;
        }if(wrapper.vesselLocation != null && wrapper.vesselLocation!=''){
            caseRecord.VesselLocation__c = wrapper.vesselLocation;
        }if(wrapper.dateBusinessMoved != null){
            caseRecord.DateBusinessMoved__c = wrapper.dateBusinessMoved;
        }if(wrapper.businessStartDate != null){
            caseRecord.Business_Location_Start_Date__c = wrapper.businessStartDate;
        }if(wrapper.vesselOwner != null && wrapper.vesselOwner!=''){
            caseRecord.VesselOwner__c = wrapper.vesselOwner;
        }if(wrapper.vesselIdType != null && wrapper.vesselIdType!=''){
            caseRecord.Vessel_ID_Type__c = wrapper.vesselIdType;
        }if(wrapper.fishGameBoatNumber != null && wrapper.fishGameBoatNumber!=''){
            caseRecord.Fish_Game_Boat_Number__c = wrapper.fishGameBoatNumber;
        }
        
        return caseRecord;
    }
    
    // Generic Content Document Method
    public static void insertContentDocument(enquiryWrapper wrapper, Id caseRecordId){
        ContentDocumentLink contentDocumentLinkInstance = new ContentDocumentLink();
        contentDocumentLinkInstance.ContentDocumentId = wrapper.contentDocumentId;
        contentDocumentLinkInstance.LinkedEntityId = caseRecordId;
        contentDocumentLinkInstance.ShareType = 'V';
        database.insert(contentDocumentLinkInstance);
    }
    
    // Wrapper class to hold all the values form the Form.
    public class enquiryWrapper{
        @auraEnabled public string accountName;
        @auraEnabled public string subject;
        @auraEnabled public string description;
        @auraEnabled public string doingBusiness;
        @auraEnabled public string contentDocumentId;
        @auraEnabled public string selectedType;
        @auraEnabled public string newBusinessLocation;
        @auraEnabled public string vesselName;
        @auraEnabled public string vesselId;
        @auraEnabled public string vesselLocation;
        @auraEnabled public date dateBusinessMoved;
        @auraEnabled public date businessStartDate;
        @auraEnabled public string vesselOwner;
        @auraEnabled public string vesselIdType;
        @auraEnabled public string fishGameBoatNumber;
    }
    
}