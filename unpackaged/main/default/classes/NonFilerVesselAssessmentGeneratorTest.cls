/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This is atest class to NonfilerVessellAssessmentBulkGeneratorClass

    @isTest
    public class NonFilerVesselAssessmentGeneratorTest {
    
        private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
        private static Integer lastYear 		= currentYear-1;
        private static Integer lastToLastYear 	= lastYear-1;
        private static Integer nextYear 		= currentYear+1;
        
        @testSetup
        static void createTestData() { 
            
            //Create Users
            List<User> users = new List<User>();
            User principal = TestDataUtility.getPrincipalUser();
            users.add(principal);
            User systemAdmin = TestDataUtility.getSystemAdminUser();
            users.add(systemAdmin);  
            User auditor = TestDataUtility.getBPPAuditorUser();
            users.add(auditor); 
            insert users;
            
            User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
            System.runAs(systemAdminUser) {
                
                //insert roll years
                List<RollYear__c> rollYears = new List<RollYear__c>();
                
                RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
                rollYearCurrentFiscalYear.IsLowValueVesselAssessmentgenerator__c = true;
                rollYearCurrentFiscalYear.IsDirectBillVesselAssessmentProcess__c = true;            
                rollYears.add(rollYearCurrentFiscalYear);
                
                RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastYear),String.valueof(lastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS);
                rollYears.add(rollYearLastFiscalYear);
    
                RollYear__c rollYearLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastToLastYear),String.valueof(lastToLastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS);
                rollYears.add(rollYearLastToLastFiscalYear);
                
                RollYear__c rollYearNextFiscalYear = TestDataUtility.buildRollYear(String.valueof(nextYear),String.valueof(nextYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_PENDING_STATUS);
                rollYears.add(rollYearNextFiscalYear);
                
                insert rollYears;
                
                //insert penalty
                Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
                insert penalty;
                
                //insert account
                Account businessAccount = TestDataUtility.getBusinessAccount();
                insert businessAccount;
                
                //insert property
                Property__c vesselProperty = TestDataUtility.getVesselProperty();
                vesselProperty.Account__c = businessAccount.Id;
                insert vesselProperty;
                
                // insert validation rule custom setting
                ValidationRuleSettings__c setting = new ValidationRuleSettings__c();            
                setting.DisableValidationRules__c = true;
                insert setting;
                
            }
        }
        
        @isTest
        static void testGenerateNonFilerAssessmentForBPPPrinicipal() {
            
            User principalUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
            RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
           
            Test.startTest();        
                                      
                Property__c  vesselProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
                
                //insert assessment
                Date lastYearEventDate=  Date.newInstance(lastYear, 3, 11);           
                Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),vesselProperty.Id,
                                                                    rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                                    CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,lastYearEventDate);
                
                insert assessment;
                
                System.runAs(principalUser) 
                {             
                    //run batch class
                    GenerateNonFilerVesselAssessments.Response  response = GenerateNonFilerVesselAssessments.checkPermission(rollYears[1].id);
                    System.assertEquals(true,response.isSuccess , 'Batch allowed to process');
                    GenerateNonFilerVesselAssessments.runNonFilerAssessmentBulkGenerator();
                            
                }
            Test.stopTest();
            rollYears = [select id,Name, BPPLateFilingDate__c,IsLowValueVesselAssessmentgenerator__c,IsDirectBillVesselAssessmentProcess__c,IsVesselNonFilerGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1] ; 
            System.assertEquals(true ,rollYears[0].IsVesselNonFilerGeneratorProcessed__c, 'Batch Completed Successfully'); 

        }
        
    	@isTest
        static void testForLastYearLegacyAssessment() {
            
            User principalUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
            RollYear__c[] rollYears = [select id,Name,IsDirectBillVesselAssessmentProcess__c,IsLowValueVesselAssessmentgenerator__c, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
           
            Test.startTest();        
                                      
                Property__c  vesselProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
                vesselProperty.LastFileDate__c = null;
                update vesselProperty;
                
                //insert assessment
                Date lastYearEventDate=  Date.newInstance(lastYear, 3, 11);           
                Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME),vesselProperty.Id,
                                                                    rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                                    CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,lastYearEventDate);
                
                insert assessment;
                                
                
                System.runAs(principalUser) 
                {             
                    //run batch class
                
                    GenerateNonFilerVesselAssessments.Response  response = GenerateNonFilerVesselAssessments.checkPermission(rollYears[1].id);
                    System.assertEquals(true,response.isSuccess , 'Batch allowed to process');

                    GenerateNonFilerVesselAssessments.runNonFilerAssessmentBulkGenerator();
                 
                }
            Test.stopTest();    
               
            rollYears = [select id,Name, BPPLateFilingDate__c,IsLowValueVesselAssessmentgenerator__c,IsDirectBillVesselAssessmentProcess__c,IsVesselNonFilerGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1] ; 
            System.assertEquals(true ,rollYears[0].IsVesselNonFilerGeneratorProcessed__c, 'Batch Completed Successfully'); 
                    
        }
    
        @isTest
        static void testForCurrentYearAssesmentExists() {
            
            User principalUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
            RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
            rollYears[1].BPPLateFilingDate__c = Date.parse(String.valueOf('01/05/'+currentYear));
            update rollYears[1];
            Test.startTest();        
                                      
                Property__c  vesselProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
                vesselProperty.LastFileDate__c = null;
                update vesselProperty;
                
                //insert assessment
                Date lastYearEventDate=  Date.newInstance(lastYear, 3, 11);           
                Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),vesselProperty.Id,
                                                                    rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                                    CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,lastYearEventDate);
                
                insert assessment;
                            
                
                System.runAs(principalUser) 
                {             
                    //run batch class
                    GenerateNonFilerVesselAssessments.Response  response = GenerateNonFilerVesselAssessments.checkPermission(rollYears[1].id);
                    System.assertEquals(true,response.isSuccess , 'Batch allowed to process');
                    GenerateNonFilerVesselAssessments.runNonFilerAssessmentBulkGenerator();
                            
                }
            Test.stopTest();  
            rollYears = [select id,Name, BPPLateFilingDate__c,IsLowValueVesselAssessmentgenerator__c,IsDirectBillVesselAssessmentProcess__c,IsVesselNonFilerGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1] ; 
            System.assertEquals(true ,rollYears[0].IsVesselNonFilerGeneratorProcessed__c, 'Batch Completed Successfully');      
        }

        @isTest
        static void testInvalidBatchRunOne() {
            
            User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
            RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
            rollYears[1].BPPLateFilingDate__c = Date.parse(String.valueOf('01/05/'+currentYear));
            rollYears[1].IsLowValueVesselAssessmentgenerator__c = false;
           // rollYears[0].IsDirectBillVesselAssessmentProcess__c = true;   
            update rollYears[1];
            Test.startTest();        
                                      
                Property__c  vesselProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
                vesselProperty.LastFileDate__c = null;
                update vesselProperty;
                
                System.runAs(systemAdminUser) 
                {             
                    //run batch class
                    GenerateNonFilerVesselAssessments.Response  response = GenerateNonFilerVesselAssessments.checkPermission(rollYears[1].id);
                    System.assertEquals(false,response.isSuccess , 'Batch Should not be allowed to process');
                    GenerateNonFilerVesselAssessments.runNonFilerAssessmentBulkGenerator();
                            
                }
            Test.stopTest();  
           
        }

        @isTest
        static void testInvalidBatchRunTwo() {
            
            User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
            RollYear__c[] rollYears = [select id,Name,Status__c, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
            Test.startTest();        
                                      
                Property__c  vesselProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
                vesselProperty.LastFileDate__c = null;
                update vesselProperty;
 
                System.runAs(systemAdminUser) 
                {             
                    //run batch class
                    GenerateNonFilerVesselAssessments.Response  response = GenerateNonFilerVesselAssessments.checkPermission(rollYears[2].id);
                    System.assertEquals('Roll Closed',rollYears[2].Status__c , 'Roll is already Closed');
                    System.assertEquals(false,response.isSuccess , 'Batch Should not be allowed to process');
                    GenerateNonFilerVesselAssessments.runNonFilerAssessmentBulkGenerator();
                            
                }
            Test.stopTest();  
           
        }

        @isTest
        static void testInvalidBatchRunThree() {
            
            User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
            RollYear__c[] rollYears = [select id,Name,Status__c, BPPLateFilingDate__c FROM RollYear__c order by Name desc]; 
            rollYears[1].IsVesselNonFilerGeneratorProcessed__c = true;  
            update rollYears[1];      
            Test.startTest();        
                                      
                Property__c  vesselProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
                vesselProperty.LastFileDate__c = null;
                update vesselProperty;
 
                System.runAs(systemAdminUser) 
                {             
                    //run batch class
                    GenerateNonFilerVesselAssessments.Response  response = GenerateNonFilerVesselAssessments.checkPermission(rollYears[1].id);
                    System.assertEquals(true,rollYears[1].IsVesselNonFilerGeneratorProcessed__c , 'Non Filer Vessel Assesment Bulk Job already proocessed');
                    System.assertEquals(false,response.isSuccess , 'Batch Should not be allowed to process');
                    GenerateNonFilerVesselAssessments.runNonFilerAssessmentBulkGenerator();
                            
                }
            Test.stopTest();  
           
        }

        @isTest
        static void testInvalidBatchRunFour() {
            
            User principalUser = [SELECT Id FROM User WHERE LastName = 'principaluser'];
            RollYear__c[] rollYears = [select id,Name,Status__c, BPPLateFilingDate__c FROM RollYear__c order by Name desc]; 
            rollYears[1].IsVesselNonFilerGeneratorProcessed__c = true;  
            update rollYears[1];  

            Test.startTest();        
                                      
                Property__c  vesselProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
                vesselProperty.LastFileDate__c = null;
                update vesselProperty;
 
                System.runAs(principalUser) 
                {             
                    //run batch class
                    GenerateNonFilerVesselAssessments.Response  response = GenerateNonFilerVesselAssessments.checkPermission(rollYears[1].id);
                    System.assertEquals(false,response.isSuccess , 'No Perssion for the logged in user');
                    GenerateNonFilerVesselAssessments.runNonFilerAssessmentBulkGenerator();
                            
                }
            Test.stopTest();  
           
        }
    
    
    
}