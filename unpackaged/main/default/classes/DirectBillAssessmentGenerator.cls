/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This Batch class is used to get all the Property related Assessments (Cases)
//          with Assesment Line Items from the previous years and clone the Assesment with its Line Items.
//-----------------------------

public class DirectBillAssessmentGenerator implements Database.Batchable<sObject>, Database.Stateful, BatchableErrorHandler{
    private final String CASE_QUERY;
    private final String originLocation ='DirectBillAssessmentGenerator';
    private boolean cloneSuccess = true;
    private Integer currentYear = FiscalYearUtility.getCurrentFiscalYear();
    private Integer lastRollYear = currentYear - 1;
    private Id assessmentLegacyRecordTypeId;
    private Id assessmentBPPRecordTypeId;
    private Id propertyBPPRecordTypeId;
    
    public DirectBillAssessmentGenerator() {
        assessmentLegacyRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME);
        assessmentBPPRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
        propertyBPPRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);
        CASE_QUERY = buildQueryToFetchAllDetailsOfCases();
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method gets all Property records and its child Assessment (Case) records.
    //          It only get those Properties whose account's notice type is equal to 'Direct Bill'.
    // @return : Database.QueryLocator (List of Properties)
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String  batchQuery;
        //Buld dynamic query for batch
        batchQuery =  'SELECT Id, Name,Account__c, ( SELECT Id,AdjustmentType__c, RecordType.DeveloperName,Generated_Assessment__c,Type,EventDate__c,Penalty__c,ExemptionPenalty__c,';
        batchQuery += 'ParentId, isAssessment__c, RollYear__c,AssessmentYear__c,NoticeType__c FROM Cases__r WHERE IsAssessment__c = true ';
        batchQuery += ' AND RollYear__c != \''+String.valueof(currentYear)+'\'';
        batchQuery += ' AND (RecordTypeId = \''+assessmentLegacyRecordTypeId+'\'';
        batchQuery += ' OR RecordTypeId = \''+assessmentBPPRecordTypeId+'\')';
        batchQuery += ' AND AdjustmentType__c != \''+String.escapeSingleQuotes('Cancel')+'\' ';
        batchQuery += ' AND SubStatus__c  != \''+String.escapeSingleQuotes('Cancelled')+'\' ';
        batchQuery += ' AND IsCorrectedOrCancelled__c = false ';
        batchQuery += ' Order By EventDate__c desc, sequencenumber__c desc nulls last limit 1) ';
        batchQuery += ' FROM Property__c WHERE Status__c = \''+CCSFConstants.STATUS_ACTIVE+'\' AND Account__r.BusinessStatus__c = \''+CCSFConstants.STATUS_ACTIVE+'\'';
        batchQuery += ' AND Account__r.NoticeType__c =\''+CCSFConstants.NOTICE_TYPE_DIRECT_BILL+'\' AND RecordTypeId = \''+propertyBPPRecordTypeId+'\'';        
        batchQuery += ' AND Id NOT IN ( SELECT Property__c FROM Case WHERE IsAssessment__c = true ';
        batchQuery += ' AND RollYear__c = \''+String.valueof(currentYear)+'\'';
        batchQuery += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';
        batchQuery += ' AND (RecordTypeId = \''+assessmentLegacyRecordTypeId+'\'';
        batchQuery += ' OR RecordTypeId = \''+assessmentBPPRecordTypeId+'\'))';

        Logger.addDebugEntry(batchQuery,originLocation+'- start method query');
        Logger.saveLog();
        return database.getQueryLocator(batchQuery);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : propertyWithCasesList (List of Properties)
    // @description : This method clones all the Assessment (Case) records of Property(s) which
    //          are returned by start() method of the Batch class with its child records i.e.
    //          Assesment Line Items. Then get only those Assessment records whose isAssessment
    //          eqals to TRUE, Roll year is less than or equal to last Assesment year and Assessment
    //          type is not equal to 'Regular'.
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext bc, List<Property__c> propertyWithCasesList) {
        // Process each batch of records, default batch size 200
        // Changed to Fiscal year from Current        
        RollYear__c currentRollYear = getRollYearDetails(String.valueOf(currentYear));
        String rollYear = currentRollYear.Id;
        
        Set<Id> caseIdsSet = new Set<Id>();       
        for(Property__c property : propertyWithCasesList) {
            List<Case> propertyAssessments = property.Cases__r;
            if(propertyAssessments.size() == 1) {  
                if(propertyAssessments[0].Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR && propertyAssessments[0].AdjustmentType__c == CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW) {
                    caseIdsSet.add(propertyAssessments[0].Id);
                }
            }           
        }  

        // Build query to get all fields of Case and Assessment Line Items
        String query = CASE_QUERY + ' WHERE Id IN: caseIdsSet';        
        
        // Execute query and fetch all cases to be cloned
        List<Case> casesToBeCloned = Database.query(query);
        
        // Create new list of cloned cases
        List<Case> clonedCases = new List<Case>();
        
        // Create a map of original Case Id(Source Id) to cloned Assessment Line Items
        Map<String, List<AssessmentLineItem__c>> clonedCaseToAssessmentLIMap = new Map<String, List<AssessmentLineItem__c>>();
        Date eventDate = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        try {
            if(!casesToBeCloned.isEmpty()) {
                
                for(Case caseToClone : casesToBeCloned) {
                    // 1 - Clone the case, perform the deep clone here
                    Case newCase = caseToClone.clone(false, true, false, false);
                    newCase.Status = 'In Progress';               
                    // 2 - ASR-8909 -Updating newCase, Event Date to today and Assessment Year year to current year etc.
                    //newCase.EventDate__c = System.today();
                    // ASR-10029 -Updating Event Date as 1/1/Calendar Current Year i.e.,Current Fiscal Year
                    newCase.EventDate__c = eventDate;
                    newCase.AssessmentYear__c = String.valueOf(FiscalYearUtility.getFiscalYear(newCase.EventDate__c));
                   //Addedd as per bug ASR-7853 Clone assessment from Auto Proccessed should not be auto Processed 
                    newCase.AutoProcessed__c = false;
                    //ASR-9701. Enrolled Date should be blank after cloning an assessment
            		newCase.EnrolledDate__c = null;
                    // ASR-8548
                    newCase.CancelAttempted__c = false;
                    // 3 - Updating newCase, updating roll year to current year etc.
                    newCase.RollYear__c = String.valueOf(currentYear);
                    newCase.Roll__c = rollYear;
                    
                    //ASR-8544 Changes Starts
                    newCase.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
                    newCase.SubType__c = null;
                    newCase.SubStatus__c= null;
                    newCase.IntegrationStatus__c = null;
                    newCase.IsLocked__c = false;
                    //ASR-8544 Changes Ends
                    
                    //Make the Penalty fields to null
                    newCase.Penalty__c = null;
                    newCase.WaivePenaltyReason__c =null;
                    newCase.ApplyFraudPenalty__c=false;
                    newCase.PenaltyPercent__c =0;
                    
                    //Make all the Exemption Fields to null
                    newCase.ExemptionPenalty__c = null;
                    //updated as part of ASR-10047
                    newCase.ExemptAmountManual__c= null;
                    newCase.PercentExempt__c= null;
                    newCase.ExemptionCode__c= null;
                    newCase.ExemptionDate__c= null;
                    newCase.ExemptionType__c= null;
                    newCase.ExemptionSubtype__c= null;
                    newCase.ExemptionReviewed__c = false;
                    newCase.ExemptAmountManual__c = null;
                    newCase.PercentExempt__c = null;
                    newCase.Exemption__c = null;
                    
                    // as part of ASR 10048	
                    newCase.IsCorrectedOrCancelled__c = false;
                    
                    // As part of 9070 Fix
                    newCase.ClosedDate = null;
                    newCase.Status = CCSFConstants.ASSESSMENT.STATUS_CLOSED;
                    newCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                    newCase.IsLocked__c = true;

                    //ASR-10768 Changes
                    newCase.AdjustmentReason__c = null;
                    
                    //newCase.NoticeType__c = 'Direct Bill';
                    
                    //call utility method to clone legacy assessment
                    if(newCase.RecordTypeId == assessmentLegacyRecordTypeId) {
                        newCase = LegacyDataUtility.convertAssessment(newCase,assessmentBPPRecordTypeId);
                    }
                    // 3 - Add new case to list
                    clonedCases.add(newCase);
                    
                    // 4 - Clone Assessment Line Items separately, if they are available only
                    if(!caseToClone.AssessmentLineItems__r.isEmpty()) {
                        List<AssessmentLineItem__c> clonedLineItemList = new List<AssessmentLineItem__c>();
                        for(AssessmentLineItem__c lineItemToClone : caseToClone.AssessmentLineItems__r) {
                            // 4.1 - Clone the Assessment Line Item, perform the deep clone again
                            clonedLineItemList.add(lineItemToClone.clone(false, true, false, false));
                        }
                        clonedCaseToAssessmentLIMap.put(newCase.getCloneSourceId(), clonedLineItemList);
                    }
                }
                
                Database.DMLOptions dmlOptions = new Database.DMLOptions();
                dmlOptions.DuplicateRuleHeader.allowSave = true;
                AsyncDlrsCalculator.getInstance().setQueueToRun(false);
                List<Database.SaveResult> insertResults, insertChildResults;
                // 1st insert cloned Assessments a.k.a Cases
                // insert cloned Cases
                if(!clonedCases.isEmpty()){
                    insertResults = Database.insert(clonedCases, dmlOptions); 
                }
                
                // 2nd insert cloned AssessmentListItems a.k.a Case Line Items,
                // but before update Case__c(lookup) with newly cloned Case Id's on Assessment Line Item
                List<AssessmentLineItem__c> clonedAssessmentLItems = new List<AssessmentLineItem__c>();
                if(!clonedCases.isEmpty()){
                    for(Case clonedCase : clonedCases) {
                        String sourceCaseId = clonedCase.getCloneSourceId();
                        if(clonedCaseToAssessmentLIMap.containsKey(sourceCaseId)) {
                            for(AssessmentLineItem__c lineItem : clonedCaseToAssessmentLIMap.get(sourceCaseId)) {
                                lineItem.Case__c = clonedCase.Id;
                                lineItem.isLegacy__c = false;
                                clonedAssessmentLItems.add(lineItem);
                            }
                        }
                    }   
                }

                if(!clonedAssessmentLItems.isEmpty()){
                    insertChildResults = Database.insert(clonedAssessmentLItems, dmlOptions);
                }
                AsyncDlrsCalculator.getInstance().setQueueToRun(true);
            }
        } catch(Exception ex){
            throw ex;
        }
        
    }
    
    public void finish(Database.BatchableContext bc) {
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, DirectBillAssessmentGenerator.class.getName(), asyncList[0].ParentJobId, 
                                           false,'', asyncList[0].NumberOfErrors);

        RollYear__c rollYear = getRollYearDetails(String.valueOf(currentYear));
        //Update the roll year IsDirectBillBulkGeneratorProcessed__c checkbox to true,
        //if the batch is successful and there are no failed records.
        if(cloneSuccess && rollYear !=null) {
            rollYear.IsDirectBillBulkGeneratorProcessed__c = true;
            update rollYear;
        }        
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {
        
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns the Query with all the fields
    //          of the Cases and also the AssesmentLineItem Object
    //          dynamically without hardcoding the fields
    // @return : Query with fields
    //-----------------------------
    public String buildQueryToFetchAllDetailsOfCases() {
        String queryTemplate = 'SELECT {0}, (SELECT {1} FROM AssessmentLineItems__r) FROM Case';
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        
        // 1 - Get all Case object fields
        Set<String> caseFieldsSet = globalDescribe.get('Case').getDescribe().fields.getMap().keyset();
        String caseFields = String.join(new List<String>(caseFieldsSet), ',');
        
        // 2 - Get all AssessmentLineItem fields
        Set<String> assessLineItemFieldsSet = globalDescribe.get('AssessmentLineItem__c').getDescribe().fields.getMap().keyset();
        String lineItemFields = String.join(new List<String>(assessLineItemFieldsSet), ',');
        
        // 3 - Build final query and return
        return String.format(queryTemplate, new List<String>{ caseFields, lineItemFields });
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns Roll_Year__c object record for requested year
    // @return : roll year record
    //-----------------------------
    private static RollYear__c getRollYearDetails(String year) {
        
        RollYear__c[] rollYear = [SELECT Id,Name,Status__c,IsDirectBillBulkGeneratorProcessed__c
                                  FROM RollYear__c WHERE Year__c =: year  limit 1];
        //check query result and return first returned result's id
        if(rollYear.size()>0)
        {
            return rollYear[0];
        }
        return null;
    }
}