/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient
// @description : This class is used to call flow from Process Builder to create Assessment
// from Statement
// ASR- 9580 Bug
//-----------------------------
@IsTest
public with sharing class CallCreateAssessmentFlowTest {

    public static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    public static Integer lastYear 		    = currentYear-1;
    public static Integer lastToLastYear 	= lastYear-1;
    public static Id BPP_RECORD_TYPE_ID     = DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);
    public static Id VESSEL_RECORD_TYPE_ID  =  DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.VESSEL_RECORD_TYPE_API_NAME); 

    @testSetUp
    static void dataSetup(){
        
        TestDataUtility.disableAutomationCustomSettings(true);
                   
            RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            insert rollYearCurrentFiscalYear;
            
            //Insert Penalty
            Penalty__c penalty463 = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty463;
            
            //insert account
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            //insert property
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            bppProperty.BusinessLocationOpenDate__c= Date.newInstance(2016, 3,4);                      
            
            Property__c vesselProperty = TestDataUtility.getVesselProperty();
            vesselProperty.Account__c = businessAccount.Id;
            vesselProperty.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;

            insert new List<Property__c> {bppProperty,vesselProperty};         
            
             //Create BPP Statement         
             Map<String,String> inputParamsStatement = new Map<String,String>();            
             inputParamsStatement.put('accountId',businessAccount.Id);
             inputParamsStatement.put('propertyId',bppProperty.Id);
             inputParamsStatement.put('formType','571-L');
             inputParamsStatement.put('businessScenario','Existing');
             inputParamsStatement.put('propertyScenario','Existing');
             inputParamsStatement.put('assessmentYear',String.valueOf(lastYear)); 
             inputParamsStatement.put('recordTypeId',BPP_RECORD_TYPE_ID);
             inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/'+lastToLastYear));
             inputParamsStatement.put('filingDate',String.valueOf('01/03/'+lastYear));            
             Statement__c bppStatement = TestDataUtility.buildStatement(inputParamsStatement);
             bppStatement.FilingMethod__c = 'eFile'; 
             
            //Vessel Create Statement         
            Map<String,String> inputParamsVesselStatement = new Map<String,String>();            
            inputParamsVesselStatement.put('accountId',businessAccount.Id);
            inputParamsVesselStatement.put('propertyId',vesselProperty.Id);
            inputParamsVesselStatement.put('formType','576-D');
            inputParamsVesselStatement.put('businessScenario','Existing');
            inputParamsVesselStatement.put('propertyScenario','Existing');
            inputParamsVesselStatement.put('assessmentYear',String.valueOf(lastYear)); 
            inputParamsVesselStatement.put('recordTypeId',VESSEL_RECORD_TYPE_ID);
            inputParamsVesselStatement.put('startofBussinessDate',String.valueOf('01/03/'+lastToLastYear));
            inputParamsVesselStatement.put('filingDate',String.valueOf('01/03/'+lastYear));            
            Statement__c vesselStatement = TestDataUtility.buildStatement(inputParamsVesselStatement);
            vesselStatement.FilingMethod__c = 'eFile';  

             insert new List<Statement__c>{bppStatement, vesselStatement};
            
            Id statementReportedOwnedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.OWNED_ASSETS_RECORD_TYPE_API_NAME);   
            StatementReportedAsset__c bppStatementReportedAsset = new StatementReportedAsset__c();
            bppStatementReportedAsset = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Alternate Schedule A', 'ATMs', '2017', 100, null, bppStatement.id, 'Statement Reported Asset for last year');
            insert bppStatementReportedAsset;              
            
            TestDataUtility.disableAutomationCustomSettings(false);  
      
        }

    @istest
    static void testBPPAssessment(){
        List<Statement__c> Statements = [Select id from Statement__c where RecordTypeId =: BPP_RECORD_TYPE_ID];
        Statements[0].Status__c = CCSFConstants.STATEMENT.STATUS_SUBMITTED;
        Statements[0].VIP_Date_Time_Submitted__c = System.now();
        update Statements[0];
        String BppAssesmentRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
        Test.startTest();
        CallCreateAssessmentFlow.CreateAsessmentFromStatement(Statements);
        Test.stopTest();
        List<Case> assessment = [Select Id,RecordTypeId From Case 
                                 Where RecordTypeId =: BppAssesmentRecordTypeId];
        system.assert(assessment.size() == 1,'Assessment is created by statement');
        system.assertEquals(BppAssesmentRecordTypeId, assessment[0].RecordTypeId, 'BPP Assessment generated');
        system.debug('assessment @@' +assessment);
    }

    @istest
    static void testVesselAssessment(){
        List<Statement__c> Statements = [Select id from Statement__c where RecordTypeId =: VESSEL_RECORD_TYPE_ID];
        Statements[0].Status__c = CCSFConstants.STATEMENT.STATUS_SUBMITTED;
        Statements[0].VIP_Date_Time_Submitted__c = System.now();
        update Statements[0];
        String vesselAssesmentRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME);
        Test.startTest();
        CallCreateAssessmentFlow.CreateAsessmentFromStatement(Statements);
        Test.stopTest();
        List<Case> assessment = [Select Id,RecordTypeId From Case 
                                 Where RecordTypeId =: vesselAssesmentRecordTypeId];
        system.assert(assessment.size() == 1,'Assessment is created by statement');
        system.assertEquals(vesselAssesmentRecordTypeId, assessment[0].RecordTypeId, 'Vessel Assessment generated');
        system.debug('assessment @@' +assessment);
    }
    
}