/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @description : This class is used to test class AumentumAutoCleanupController.
// ASR-2366
//-----------------------------
@isTest
public with sharing class AumentumAutoCleanupControllerTest {
   
    @isTest
    static void cleanUpTest(){
        List<StagingAumentumBusiness__c> records = Test.loadData(StagingAumentumBusiness__c.sObjectType, 'AumentumProcessTestData');

        test.startTest();
        List<StagingAumentumBusiness__c> processRcords = AumentumAutoCleanupController.cleanUpRecords(records);
        test.stopTest();
        List<Boolean> isDateCleaned = new List<Boolean>();
        List<Boolean> notCleaned = new List<Boolean>();
        for(StagingAumentumBusiness__c record:processRcords){
            if(record.IsDataClean__c == true){
                isDateCleaned.add(record.IsDataClean__c); 
            }else{
                notCleaned.add(record.IsDataClean__c);
            }           
        }
        
        system.assertEquals(records.size(),isDateCleaned.size(),'All data is cleaned up' );
        system.assert(notCleaned.size()==0,'No data left uncleaned');
        
    }
}