/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This is a test class to InitiateBatchJobs
@isTest
public class InitiateBatchJobsTest {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    private static void dataSetup(){
        RollYear__c rollYear = TestDataUtility.buildRollYear(String.valueof(system.today().year()),String.valueof(system.today().year()),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYear.IntegrationStatus__c=CCSFConstants.IN_TRANSIT_TO_TTX;
        insert rollYear;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to check if batch class is triggering in case Input given is wrong 
    //-----------------------------
    @isTest
    static void initiateJobsIfProvidedInputIsWrong(){
        InitiateBatchJobs request=new InitiateBatchJobs();
        String JsonMsg=JSON.serialize(request);
        Test.startTest();
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = '/services/apexrest/Initiatejobs?jobName=testing';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        req.params.put('jobName', 'testing');
        String message = InitiateBatchJobs.initiateJobs();
        System.assertEquals('Wrong job name provided', message);
        Test.stopTest();
   }
    
   //-----------------------------
   // @author : Publicis.Sapient
   // @description : Method to check if batch class is triggering in case Input given is blank
   //----------------------------- 
   @isTest
    static void initiateJobsIfProvidedInputIsBlank(){
        InitiateBatchJobs request=new InitiateBatchJobs();
        String JsonMsg=JSON.serialize(request);
        Test.startTest();
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = '/services/apexrest/Initiatejobs?jobName=testing';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        req.params.put('jobName', '');
        String message = InitiateBatchJobs.initiateJobs();
        System.assertEquals('Wrong job name provided', message);
        Test.stopTest();
   }
    
	//-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to check if batch class is triggering in case Input given as 'LowValueAssessorBatch' 
    //-----------------------------    
    @isTest
    static void initiateJobsForLowValueAssessor(){
        InitiateBatchJobs request=new InitiateBatchJobs();
        String JsonMsg=JSON.serialize(request);
        Test.startTest();
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = '/services/apexrest/Initiatejobs?jobName=LowValueAssessorBatch';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        req.params.put('jobName', 'LowValueAssessorBatch');
        String message = InitiateBatchJobs.initiateJobs();
        System.assertEquals('Job initiated', message);
        Test.stopTest();
   }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to check if batch class is triggering in case Input given as 'PrintMailerBatch' 
    //-----------------------------    
    @isTest
    static void initiateJobsForPrint(){
        InitiateBatchJobs request=new InitiateBatchJobs();
        String JsonMsg=JSON.serialize(request);
        Test.startTest();
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = '/services/apexrest/Initiatejobs?jobName=PrintMailerBatch';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        req.params.put('jobName', 'PrintMailerBatch');
        String message = InitiateBatchJobs.initiateJobs();
        System.assertEquals('Job initiated', message);
        Test.stopTest();
   }	   
}