/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test SmartCOMMUtility
//-----------------------------
@isTest
public class SmartCOMMUtilityTest {

    @testSetup
    private static void dataSetup(){
        List<TH1__Schema_Set__c> schemaSets = new List<TH1__Schema_Set__c>();
        TH1__Schema_Set__c schemaSet1 = new TH1__Schema_Set__c();
        schemaSets.add(schemaSet1);
        TH1__Schema_Set__c schemaSet2 = new TH1__Schema_Set__c();
        schemaSets.add(schemaSet2);
        insert schemaSets;
        
        List<TH1__Schema_Object__c> schemaObjects = new List<TH1__Schema_Object__c>();
        TH1__Schema_Object__c schemaObject1 = new TH1__Schema_Object__c();
        schemaObject1.TH1__Is_Primary_Object__c = true;
        schemaObject1.TH1__Object_Label__c = 'Account';
        schemaObject1.TH1__Schema_Set__c = schemaSets.get(0).Id;
        schemaObject1.Name = 'Account';
        schemaObjects.add(schemaObject1);
        
        TH1__Schema_Object__c schemaObject2 = new TH1__Schema_Object__c();
        schemaObject2.TH1__Is_Primary_Object__c = true;
        schemaObject2.TH1__Object_Label__c = 'Property__c';
        schemaObject2.TH1__Schema_Set__c = schemaSets.get(1).Id;
        schemaObject2.Name = 'Property__c';
        schemaObjects.add(schemaObject2);
        insert schemaObjects;
       
        schemaSets.get(0).TH1__Primary_Object__c = schemaObjects.get(0).Id;
        schemaSets.get(1).TH1__Primary_Object__c = schemaObjects.get(1).Id;
        update schemaSets;
        
        List<String> documentSettingNames = new List<String>{'Account Document Setting','Notice of Requirement to File 571-L'};
        List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        TH1__Document_Setting__c documentSettting;
        for(integer i=0;i<2;i++) {
            documentSettting = new TH1__Document_Setting__c();
            documentSettting.TH1__Document_Data_Model__c = schemaSets.get(i).Id;
            documentSettting.Name = documentSettingNames.get(i);
            documentSettting.TH1__Storage_File_Name__c = documentSettingNames.get(i);
            documentSettting.TH1__Is_disabled__c=false;
            documentSettings.add(documentSettting);
        }
        insert documentSettings;
    }
    
    /* Template Designer is not currently licensed for your organization. 
     * To enquire about this feature, please contact Smart Communications Support
     * @isTest
    private static void getAvailableDocumentSettings(){
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Account account = new Account();
        account.Name='test account';
        account.RecordTypeId=businessAccountRecordTypeId;
        account.BusinessStatus__c='active';
        account.MailingCity__c='San Franciso';
        account.MailingCountry__C='US';
        account.MailingStreetName__c = 'Bush Street';
        account.RequiredToFile__c = 'Yes';
        insert account;
        Test.startTest();
        List<SmartCommUtility.DocumentSettingDetail> templates = SmartCommUtility.getAvailableDocumentSettings(account.Id);
        System.assertEquals(1, templates.size());
        System.assertEquals('Account Document Setting', templates.get(0).Name);
        Test.stopTest();
    }*/
    
    @isTest
    private static void getDocumentSettingsByPrimaryFormType(){
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Account account = new Account();
        account.Name='test account';
        account.RecordTypeId=businessAccountRecordTypeId;
        account.BusinessStatus__c='active';
        account.MailingCity__c='San Franciso';
        account.MailingCountry__C='US';
        account.MailingStreetName__c = 'Bush Street';
        account.RequiredToFile__c = 'Yes';
        insert account;
        
        List<Property__c> properties = new List<Property__c>();
        
        Property__C property1 = new Property__C();
        property1.Name = 'test property'; 
        property1.MailingCountry__c = 'US';
        property1.Account__c = account.Id;
        property1.PrimaryFormType__c='571-L';
        properties.add(property1);
        Property__C property2 = new Property__C();
        property2.Name = 'test property'; 
        property2.MailingCountry__c = 'US';
        property2.Account__c = account.Id;
        properties.add(property2);
        insert properties;
        Test.startTest();
        Map<String, TH1__Document_Setting__c> documentSettingsByFormName = SmartCommUtility.getDocumentSettingsByPrimaryFormType(properties);
        System.assertEquals(1,documentSettingsByFormName.size());
        System.assertNotEquals(null,documentSettingsByFormName.get('571-L'));
        Test.stopTest();
    }
    
    @isTest
    private static void getDocumentSettingsBySObjectName(){
        Test.startTest();
        Map<String, TH1__Document_Setting__c> documentSettingsByName = SmartCommUtility.getDocumentSettingsBySObjectName('Account');
        System.assertEquals(1, documentSettingsByName.Size());
        documentSettingsByName = SmartCommUtility.getDocumentSettingsBySObjectName('Property__c');
        System.assertEquals(1, documentSettingsByName.Size());
        documentSettingsByName = SmartCommUtility.getDocumentSettingsBySObjectName('Case');
        System.assertEquals(0, documentSettingsByName.Size());
        Test.stopTest();
    }
    
    @isTest
    private static void createDocumentGenerationRequest(){
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        Account account = new Account();
        account.Name='test account';
        account.RecordTypeId=businessAccountRecordTypeId;
        account.BusinessStatus__c='active';
        account.MailingCity__c='San Franciso';
        account.MailingCountry__C='US';
        account.MailingStreetName__c = 'Bush Street';
        account.RequiredToFile__c = 'Yes';
        insert account;
        
        Property__C property = new Property__C();
        property.Name = 'test property'; 
        property.MailingCountry__c = 'US';
        property.Account__c = account.Id;
        insert property;
        TH1__Document_Setting__c documentSetting = [SELECT Id, Name FROM TH1__Document_Setting__c limit 1];
        Test.startTest();
        DocumentGenerationRequest__c documentGenerationRequest = SmartCommUtility.createDocumentGenerationRequest(account.Id, documentSetting.Id);
        System.assertEquals(account.Id, documentGenerationRequest.RelatedRecordId__c);
        documentGenerationRequest = SmartCommUtility.createDocumentGenerationRequest(property.Id, documentSetting.Id);
        System.assertEquals(property.Id, documentGenerationRequest.RelatedRecordId__c);
        Test.stopTest();
    }
}