/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/*********************** 
 * @Author: Publicis Sapient
 * @Apex TestClassName: AnnualRollBatchSyncMockCallout
 * @Date : 11th Feb 2019 
 * 
 **********************/
@isTest
global class AnnualRollBatchSyncMockCallout implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest request) {
        
        String passUrl = Test.isRunningTest() ? 'http://dummyClassNamePassAssessment.com' :[Select Endpoint from NamedCredential where developerName ='SMART_Job_Taxsys_Pass_Assessment_Endpoint' limit 1].Endpoint;
        String endUrl = Test.isRunningTest() ? 'http://dummyClassNameRollEnd.com' :[Select Endpoint from NamedCredential where developerName ='SMART_Job_Taxsys_Annual_Roll_End_Endpoint' limit 1].Endpoint;
        String startUrl = Test.isRunningTest() ? 'http://dummyClassNameRollStart.com' :[Select Endpoint from NamedCredential where developerName ='SMART_Job_Taxsys_Annual_Roll_Start_Endpoint' limit 1].Endpoint;
        
        HttpResponse response = new HttpResponse();
        String currentRollYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        // Send a mock response for a specific endpoint
        if(request.getEndpoint().contains(startUrl))
        {
            System.assertEquals('GET', request.getMethod());
            System.assertEquals(true, request.getEndpoint().contains(startUrl+currentRollYear+'?expectedrecordcount=2'));
            string sessionBody = '{"triggerBatchId":"111111fe-11b1-1c11-11b1-ed1111111a11"}';
            response.setHeader('Content-Type', 'text/json; charset=utf-8');
            response.setBody(sessionBody);
            response.setStatusCode(202);
        }
        else if(request.getEndpoint().contains(passUrl))
        {
            System.assertEquals('POST', request.getMethod());
            response.setHeader('Content-Type', 'application/json; charset=utf-8');
            response.setStatusCode(202);
        }
        else if(request.getEndpoint().contains(endUrl))
        {
            System.assertEquals('GET', request.getMethod());
            System.assertEquals(true, request.getEndpoint().contains('/111111fe-11b1-1c11-11b1-ed1111111a11/1/'+currentRollYear));
            response.setHeader('Content-Type', 'application/json; charset=utf-8');
            response.setStatusCode(202);
        }
        return response;
    } 

}