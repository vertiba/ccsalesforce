/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test WaiverExpirationDateUpdatedScheduler
//-----------------------------
@isTest
public class WaiverExpirationDateUpdatedSchedulerTest {
    //verify that a job is scheduled successfully in a future date. 
    @isTest
    private static void jobShouldBeScheduledToRunInFuture() {
        Test.startTest();
        WaiverExpirationDateUpdatedScheduler scheduler = new WaiverExpirationDateUpdatedScheduler();
        system.assertEquals(scheduler.getJobNamePrefix(),'Waiver Expiration Date Updated Scheduler', 
                            'Job name should be matched');
        
        
        String jobId = System.schedule('WaiverExpirationDateUpdatedSchedulerTest',
                                       '0 0 0 1 1 ? 2030', 
                                       new WaiverExpirationDateUpdatedScheduler()); 
        Test.stopTest();
        
        system.assertEquals(1, [SELECT count() FROM CronTrigger where id = :jobId]);
    }
}