/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class DocumentRequestGeneratorService implements Database.Batchable<SObject>,BatchableErrorHandler {

    private static final String LOG_LOCATION = 'DocumentRequestGeneratorService';

    private String query;

    public DocumentRequestGeneratorService(String query) {
        this.query = query;
    }

    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        Logger.addDebugEntry('starting to process query: ' + this.query, LOG_LOCATION);
        Logger.saveLog();
        return Database.getQueryLocator(this.query);
    }

    public void execute(Database.BatchableContext batchableContext, List<Property__c> scope) {
        try {
            // Requery the records to ensure we have the necessary fields
            scope = [SELECT Id, HasMailingAddress__c, MailingAddress__c, PrimaryFormType__c, Status__c FROM Property__c WHERE Id IN :scope];

            Map<String, TH1__Document_Setting__c> documentSettingsByPrimaryFormType = SmartCOMMUtility.getDocumentSettingsByPrimaryFormType(scope);
            Logger.addDebugEntry(String.valueOf(documentSettingsByPrimaryFormType), LOG_LOCATION);

            List<DocumentGenerationRequest__c> requestsToCreate = new List<DocumentGenerationRequest__c>();
            for(Property__c property : scope) {
                TH1__Document_Setting__c documentSetting = documentSettingsByPrimaryFormType.get(property.PrimaryFormType__c);

                if(documentSetting == null) continue;
                if(property.HasMailingAddress__c == false) continue;
                if(property.Status__c == 'Inactive') continue;

                DocumentGenerationRequest__c request = SmartCOMMUtility.createDocumentGenerationRequest(property.Id, documentSetting.Id);
                requestsToCreate.add(request);
            }
            insert requestsToCreate;
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, LOG_LOCATION);
            throw ex;
        }
    }

    public void finish(Database.BatchableContext batchableContext) { 
        string JobId = batchableContext.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'query' => this.query});                                
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, DocumentRequestGeneratorService.class.getName(), asyncList[0].ParentJobId, true, queryParams, asyncList[0].NumberOfErrors);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}

}