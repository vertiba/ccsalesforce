/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This Batch class is used to get all the Non-Filer Property related Assessments (Cases)
//          with Assesment Line Items from the previous years and clone the Assesment with its Line Items.
//-----------------------------
public class NonFilerAssessmentBulkGenerator implements Database.Batchable<sObject>,Database.Stateful, BatchableErrorHandler {

    private String originLocation;
    private Integer lastRollYear;
    private Integer currentYear;
    private final String CASE_QUERY;
    private boolean processSuccess;
    private Id assessmentLegacyRecordTypeId;
    private Id assessmentBPPRecordTypeId;
    private Id propertyBPPRecordTypeId;
    private Id statementBPPRecordTypeId;

    public NonFilerAssessmentBulkGenerator() {
        originLocation = CCSFConstants.NON_FILER_GENERATOR_BATCH_NAME;
        //get all objects records type ids
        assessmentLegacyRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME);
        assessmentBPPRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
        propertyBPPRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);
        statementBPPRecordTypeId = DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);

        //Get current year and last rollyear
        currentYear =   FiscalYearUtility.getCurrentFiscalYear();
        lastRollYear = currentYear - 1;
        processSuccess = true;
        CASE_QUERY = DescribeUtility.buildQueryToFetchAllDetailsOfParentandChild(Case.sObjectType, AssessmentLineItem__c.sObjectType);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method gets all Property records and its child Assessment (Case) records.
    // @return : Database.QueryLocator (List of Properties)
    public Database.QueryLocator start(Database.BatchableContext bc) {

        String  batchQuery;
        //Buld dynamic query for batch
        batchQuery = 'SELECT Id, Name,Account__c, ( SELECT Id, RecordType.DeveloperName,Generated_Assessment__c,Type FROM Cases__r WHERE IsAssessment__c = true ';
		batchQuery += ' AND RollYear__c != \''+String.valueof(currentYear)+'\'';
        batchQuery += ' AND (RecordTypeId = \''+assessmentLegacyRecordTypeId+'\'';
        batchQuery += ' OR RecordTypeId = \''+assessmentBPPRecordTypeId+'\')';
        batchQuery += ' AND AdjustmentType__c != \''+String.escapeSingleQuotes('Cancel')+'\' ';
        batchQuery += ' AND SubStatus__c  != \''+String.escapeSingleQuotes('Cancelled')+'\' ';
        batchQuery += ' AND IsCorrectedOrCancelled__c = false ';
        batchQuery += ' Order By EventDate__c desc, sequencenumber__c desc nulls last limit 1) ';
        batchQuery += ' FROM Property__c WHERE Status__c = \''+CCSFConstants.STATUS_ACTIVE+'\' AND Account__r.BusinessStatus__c = \''+CCSFConstants.STATUS_ACTIVE+'\'';
        batchQuery += ' AND RecordTypeId = \''+propertyBPPRecordTypeId+'\'';
		batchQuery += ' AND (LastFileDate__c = null OR FISCAL_YEAR(LastFileDate__c) < '+currentYear+')';
		batchQuery += ' AND Id NOT IN ( SELECT Property__c FROM Case WHERE IsAssessment__c = true ';
		batchQuery += ' AND RollYear__c = \''+String.valueof(currentYear)+'\'';
		batchQuery += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';		
		batchQuery += ' AND (RecordTypeId = \''+assessmentLegacyRecordTypeId+'\'';
		batchQuery += ' OR RecordTypeId = \''+assessmentBPPRecordTypeId+'\'))';
		
		Logger.addDebugEntry(batchQuery,originLocation+'- start method query');
        Logger.saveLog();
        return database.getQueryLocator(batchQuery);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : propertyWithCasesList (List of Properties)
    // @description : This method clones all the Assessment (Case) records of Property(s) which
    //          are returned by start() method of the Batch class with its child records i.e.
    //          Assesment Line Items.
    // @return : void
    public void execute(Database.BatchableContext bc, List<Property__c> matchingProperties) {
        try {            
            Set<Id> assessmentIds = new Set<Id>();
            List<Case> assessments = new List<Case>();
            Date eventDate = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            for(Property__c property : matchingProperties) {
                List<Case> propertyAssessments = property.Cases__r;
                if(propertyAssessments.size() == 0) {   
                    Case assessment = new Case(    
                                            //EventDate__c = System.today(),
                        					//ASR-10029 -Updating Event Date as 1/1/Calendar Current Year i.e.,Current Fiscal Year
                        					EventDate__c =  eventDate,
                                            Type         =  CCSFConstants.ASSESSMENT.TYPE_REGULAR,
                                            Property__c  =  property.Id,
                                            Generated_Assessment__c  =	CCSFConstants.ASSESSMENT.GENERATED_ASSESSMENT_NONFILER,
                                            AccountId    =  property.Account__c,
                                            RecordTypeId =  assessmentBPPRecordTypeId,
                                            //ASR-8544 Changes Starts
                                            AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW,
                                            Status = CCSFConstants.ASSESSMENT.STATUS_NEW,                                            
                                            SubStatus__c= null
                                            //ASR-8544 Changes Ends
                                            );
                    assessments.add(assessment);
                }else { //Check if previous year regular case exists and current year regular case doesn't exists.
                    assessmentIds.add(propertyAssessments[0].Id);
                }
            }

            // Build query to get all fields of Case and Assessment Line Items
            String query = CASE_QUERY + ' WHERE Id IN: assessmentIds';
            // Execute query and fetch all cases to be cloned
            Map<Id,Case> assessmentsToBeClonedById= new Map<Id,Case>((List<Case>)Database.query(query));

            AsyncDlrsCalculator.getInstance().setQueueToRun(false);

            //insert assessments and assessment line items
            assessmentsToBeClonedById = inserAssesmentsAndLineItems(assessmentsToBeClonedById,assessments,processSuccess);

            AsyncDlrsCalculator.getInstance().setQueueToRun(true);

            Logger.addDebugEntry(String.valueof(processSuccess),'processSuccess'+originLocation);

        }  catch(Exception ex) {
                Logger.addExceptionEntry(ex, originLocation);
        }

        Logger.saveLog();
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method uddates the roll year record that the Non-Filer batch has been completed.
    // @return : void
    public void finish(Database.BatchableContext bc) {

        RollYear__c rollYear = getRollYearDetails(String.valueOf(currentYear));

        //Update the roll year IsNonFilerBulkGeneratorProcessed checkbox to true,
        //if the batch is successful and there are no failed records.
        if(processSuccess && rollYear !=null) {
            rollYear.IsNonFilerBulkGeneratorProcessed__c = true;
            update rollYear;
        }
        
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncApexJobs = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                            TotalJobItems, CreatedBy.Email,ParentJobId
                                            FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, NonFilerAssessmentBulkGenerator.class.getName(), asyncApexJobs[0].ParentJobId, 
                                           false,'', asyncApexJobs[0].NumberOfErrors); 
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {
        
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns Roll_Year__c object record for requested year
    // @return : roll year record
    //-----------------------------
    private static RollYear__c getRollYearDetails(String year) {

        RollYear__c[] rollYear = [SELECT Id,Name,Status__c,IsLowValueBulkGeneratorProcessed__c,
                                  IsDirectBillBulkGeneratorProcessed__c,
                                  IsNonFilerBulkGeneratorProcessed__c,
                                  BPPLateFilingDate__c
                                  FROM RollYear__c WHERE Year__c =: year  limit 1];

        //check query result and return first returned result's id
        if(rollYear.size()>0)
        {
            return rollYear[0];
        }

        //default retun null
        return null;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method inserts assessments and assessments line items
    // @return : return map of records of old assessments form which new assessments are cloned
    //-----------------------------
    private Map<Id,Case> inserAssesmentsAndLineItems(Map<Id,Case> assessmentsToBeClonedById,List<Case> assessments,Boolean processSuccess) {

        // Create a map of original Case Id(Source Id) to cloned Assessment Line Items
        Map<String, List<AssessmentLineItem__c>> assessmentLIBySourceId = new Map<String, List<AssessmentLineItem__c>>();
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        Date eventDate = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        for(Case assessmentsToClone: assessmentsToBeClonedById.values()) {
            //  Clone the case, perform the deep clone here
            Case newAssessment = assessmentsToClone.clone(false, true, false, false);
            //ASR-8544 Changes Starts
            newAssessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
            newAssessment.Status = CCSFConstants.ASSESSMENT.STATUS_NEW; 
            newAssessment.IntegrationStatus__c = null;
            newAssessment.IsLocked__c = false;
            newAssessment.SubStatus__c= null;
            //ASR-8544 Changes Ends
            //newAssessment.EventDate__c = system.today();
            //ASR-10029 -Updating Event Date as 1/1/Calendar Current Year i.e.,Current Fiscal Year
            newAssessment.EventDate__c = eventDate;
            newAssessment.AccountNumber__c = null;
            newAssessment.AssessmentNumberSequence__c = null;
            newAssessment.AssessmentNumber__c = null;
            newAssessment.Roll__c= null;
            newAssessment.NoticeType__c = assessmentsToClone.NoticeType__c;
            newAssessment.Type = CCSFConstants.ASSESSMENT.TYPE_REGULAR;
            newAssessment.Generated_Assessment__c = CCSFConstants.ASSESSMENT.GENERATED_ASSESSMENT_NONFILER;
            //Addedd as per bug ASR-7853 Clone assessment from Auto Proccessed should not be auto Processed 
            newAssessment.AutoProcessed__c = false;
            //ASR-9701. Enrolled Date should be blank after cloning an assessment
            newAssessment.EnrolledDate__c = null;
            // Added as ASR-8548
            newAssessment.CancelAttempted__c = false;
			//call utility method to clone legacy assessment
            if(assessmentsToClone.RecordTypeId == assessmentLegacyRecordTypeId) {
				newAssessment = LegacyDataUtility.convertAssessment(newAssessment,assessmentBPPRecordTypeId);
            }

            //Make the Penalty fields to null
            newAssessment.Penalty__c = null;
            newAssessment.WaivePenaltyReason__c =null;
            newAssessment.ApplyFraudPenalty__c=false;
            newAssessment.PenaltyPercent__c =0;

            //Make all the Exemption Fields to null
            newAssessment.ExemptionPenalty__c = null;
            newAssessment.ExemptAmountManual__c= null;
            newAssessment.PercentExempt__c= null;
            newAssessment.ExemptionCode__c= null;
            newAssessment.ExemptionDate__c= null;
            newAssessment.ExemptionType__c= null;
            newAssessment.ExemptionSubtype__c= null;
            newAssessment.ExemptionReviewed__c = false;
            newAssessment.Exemption__c = null;

            // as part of ASR 10048	
            newAssessment.IsCorrectedOrCancelled__c = false;

            //ASR-10768 Changes
            newAssessment.AdjustmentReason__c = null;

              // as part of ASR 10596	
            newAssessment.ParentId = null;

            // add the assessment to the list for insertion
            assessments.add(newAssessment);

            if(!assessmentsToClone.AssessmentLineItems__r.isEmpty()) {
                List<AssessmentLineItem__c> clonedAssessmentLineItems = new List<AssessmentLineItem__c>();
                for(AssessmentLineItem__c lineItemToClone : assessmentsToClone.AssessmentLineItems__r) {
                    AssessmentLineItem__c newAssessmentLineItem = lineItemToClone.clone(false, true, false, false);
                    newAssessmentLineItem.DepreciationFactor__c = null;
                    newAssessmentLineItem.isLegacy__c= false;
                    clonedAssessmentLineItems.add(newAssessmentLineItem) ;
                }
                assessmentLIBySourceId.put(newAssessment.getCloneSourceId(), clonedAssessmentLineItems);
            }
        }

        try{
            //insert assessments
            insertRecords(assessments,processSuccess,dmlOptions);
            //insert assessment line items
            insertAssesmentLineItems(assessments,assessmentLIBySourceId,dmlOptions,processSuccess);
        }catch(Exception ex){
            throw ex;
        }
        

        return assessmentsToBeClonedById;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method inserts assessments line items
    // @return : return null
    //-----------------------------
    private void insertAssesmentLineItems(List<Case> assessments,Map<String, List<AssessmentLineItem__c>> assessmentLIBySourceId,Database.DMLOptions dmlOptions,Boolean processSuccess) {

        // insert cloned AssessmentListItems
        // but before update Case__c(lookup) with newly cloned Case Id's on Assessment Line Item
        List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();

        for(Case clonedCase : assessments) {
            String sourceCaseId = clonedCase.getCloneSourceId();
            if(assessmentLIBySourceId.containsKey(sourceCaseId)) {
                for(AssessmentLineItem__c lineItem : assessmentLIBySourceId.get(sourceCaseId)) {
                    lineItem.Case__c = clonedCase.Id;
                    assessmentLineItems.add(lineItem);
                }
            }
        }

		if(assessmentLineItems.isEmpty()) {return;}

        //insert assessment line items
        insertRecords(assessmentLineItems,processSuccess,dmlOptions);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method is used to insert sobject records.
    // @return : boolean , success or error on record insertion
    //-----------------------------
    private Boolean insertRecords(List<SObject> records,Boolean processSuccess,Database.DMLOptions dmlOptions) {
        Map<String,String> result = new Map<String,String>();
        
        //Get Database.insert results
        List<Database.SaveResult> insertResults = Database.insert(records, dmlOptions);
        for(Integer i=0;i<insertResults.size();i++) {
            if (insertResults.get(i).isSuccess()) {
                String recordId = String.valueof(insertResults.get(i).getId());
            } else {
                // DML operation failed
                Database.Error error = insertResults.get(i).getErrors().get(0);
                Logger.addExceptionEntry(new SaveResultException('error: ' + error.getMessage()), originLocation);
            }
        }

        return processSuccess;
    }
    
    // Inner class for exceptions
    private class SaveResultException extends Exception {}

}