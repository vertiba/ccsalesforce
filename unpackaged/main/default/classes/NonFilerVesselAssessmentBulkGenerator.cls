/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This Batch class is used to get all the Non-Filer Vessel Property related Assessments (Cases)
//          from the previous years and clone the Assesments.

public class NonFilerVesselAssessmentBulkGenerator implements Database.Batchable<sObject>, Database.Stateful, BatchableErrorHandler{

    private Integer currentYear;
    private final String CASE_QUERY;
    private boolean batchSuccess;
    private Id assessmentLegacyRecordTypeId;
    private Id assessmentVessselRecordTypeId;
    private Id propertyVesselRecordTypeId;
  

    public NonFilerVesselAssessmentBulkGenerator() {

        //get all objects records type ids
        assessmentLegacyRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME);
        assessmentVessselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME);
        propertyVesselRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME);

        //Get current year and last rollyear
        currentYear =   FiscalYearUtility.getCurrentFiscalYear();
        batchSuccess = true;
        CASE_QUERY = DescribeUtility.buildQueryToFetchAllDetailsOfParentandChild(Case.sObjectType, AssessmentLineItem__c.sObjectType);

    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method gets all Property records and its child Assessment (Case) records.
    // @return : Database.QueryLocator (List of Properties)
    public Database.QueryLocator start(Database.BatchableContext bc) {

        String  batchQuery;
        //Building dynamic query for batch
        batchQuery = 'SELECT Id, Name,Account__c, ( SELECT Id, RecordType.DeveloperName, Type FROM Cases__r WHERE RollYear__c != \''+String.valueof(currentYear)+'\'';
        batchQuery += ' AND (RecordTypeId = \''+assessmentLegacyRecordTypeId+'\'';
        batchQuery += ' OR RecordTypeId = \''+assessmentVessselRecordTypeId+'\')';
        batchQuery += ' AND AdjustmentType__c != \''+String.escapeSingleQuotes('Cancel')+'\' ';
        batchQuery += ' AND SubStatus__c  != \''+String.escapeSingleQuotes('Cancelled')+'\' ';
        batchQuery += ' AND IsCorrectedOrCancelled__c = false ';
        batchQuery += ' Order By EventDate__c desc, sequencenumber__c desc nulls last limit 1) ';
        batchQuery += ' FROM Property__c WHERE Status__c = \''+CCSFConstants.STATUS_ACTIVE+'\'';
        batchQuery += ' AND RecordTypeId = \''+propertyVesselRecordTypeId+'\'';
		batchQuery += ' AND (LastFileDate__c = null OR FISCAL_YEAR(LastFileDate__c) < '+currentYear+')';
		batchQuery += ' AND Id NOT IN ( SELECT Property__c FROM Case WHERE RollYear__c = \''+String.valueof(currentYear)+'\'';
		batchQuery += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';		
		batchQuery += ' AND (RecordTypeId = \''+assessmentLegacyRecordTypeId+'\'';
		batchQuery += ' OR RecordTypeId = \''+assessmentVessselRecordTypeId+'\'))';
		
		Logger.addDebugEntry(batchQuery,CCSFConstants.VESSEL_NON_FILER_GENERATOR_BATCH_NAME+'- start method query');
        Logger.saveLog();
        return database.getQueryLocator(batchQuery);
    }

     //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : propertyWithCasesList (List of Properties)
    // @description : This method clones all the Assessment (Case) records of Property(s) which
    //          are returned by start() method of the Batch class with its child records i.e.
    //          Assesment Line Items.
    // @return : void
    public void execute(Database.BatchableContext bc, List<Property__c> matchingProperties) {
        
        try {
            Set<Id> assessmentIds = new Set<Id>();
            List<Case> assessments = new List<Case>();
            Date eventDate = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            for(Property__c property : matchingProperties) {
                List<Case> propertyAssessments = property.Cases__r;
                if(propertyAssessments.size() == 0) {   
                    Case assessment = new Case(    
                                            //EventDate__c = System.today(), 
                                            //ASR-10029 -Updating Event Date as 1/1/Calendar Current Year i.e.,Current Fiscal Year
                        					EventDate__c =  eventDate,                    
                                            Type         =  CCSFConstants.ASSESSMENT.TYPE_REGULAR,
                                            Property__c  =  property.Id,
                                            AccountId    =  property.Account__c,
                                            RecordTypeId =  assessmentVessselRecordTypeId,
                                            //ASR-8544 Changes Starts
                                            AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW,
                                            Status = CCSFConstants.ASSESSMENT.STATUS_NEW,                                            
                                            SubStatus__c= null
                                            //ASR-8544 Changes Ends
                                            );
                    assessments.add(assessment);
                }else { //Check if previous year regular case exists and current year regular case doesn't exists.
                    assessmentIds.add(propertyAssessments[0].Id);
                }
            }

            // Build query to get all fields of Case and Assessment Line Items
            String query = CASE_QUERY + ' WHERE Id IN: assessmentIds';
            // Execute query and fetch all cases to be cloned
            Map<Id,Case> assessmentsToBeClonedById= new Map<Id,Case>((List<Case>)Database.query(query));

            //insert assessments and assessment line items
            if(!assessmentsToBeClonedById.isEmpty() || !assessments.isEmpty()) {
                batchSuccess = insertAssessments(assessmentsToBeClonedById,assessments);
            }

            Logger.addDebugEntry(String.valueof(batchSuccess),'batchSuccess'+CCSFConstants.VESSEL_NON_FILER_GENERATOR_BATCH_NAME);

        } catch(Exception ex) {
            batchSuccess = false;
            Logger.addExceptionEntry(ex, CCSFConstants.VESSEL_NON_FILER_GENERATOR_BATCH_NAME);
            throw ex;
        }

        Logger.saveLog();
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method uddates the roll year record that the Non-Filer batch has been completed.
    // @return : void
    public void finish(Database.BatchableContext bc) {

        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, NonFilerVesselAssessmentBulkGenerator.class.getName(), asyncList[0].ParentJobId, 
                                           false,'', asyncList[0].NumberOfErrors);

        RollYear__c rollYear = getRollYearDetails(String.valueOf(currentYear));
        //Update the roll year IsNonFilerBulkGeneratorProcessed checkbox to true,
        //if the batch is successful and there are no failed records.
        if(batchSuccess && rollYear !=null) {
            rollYear.IsVesselNonFilerGeneratorProcessed__c = true;
            update rollYear;
        }
    }

     //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method inserts assessments and assessments line items
    // @return : return map of records of old assessments form which new assessments are cloned
    //-----------------------------
    private Boolean insertAssessments(Map<Id,Case> assessmentsToBeClonedById,List<Case> assessments) {

        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        Map<Id,Id> recordtypeIdForLegacyRecord = new Map<Id,Id>{assessmentLegacyRecordTypeId => assessmentVessselRecordTypeId};

        Map<String,Object> clonedDataMapByListNames = AssessmentsUtility.getCloneAssessments(assessmentsToBeClonedById.values(),true,recordtypeIdForLegacyRecord);

        if(!clonedDataMapByListNames.isEmpty()) {
            assessments.addAll((List<Case>)clonedDataMapByListNames.get('assessments'));
        }
        // Added a loop for all  inserted and clone statement, to identify the Orign.
        for(Case assessment : assessments){
            assessment.Generated_Assessment__c = CCSFConstants.ASSESSMENT.GENERATED_ASSESSMENT_NONFILER;
            //ASR-8544 Changes Starts
            assessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
            assessment.Status = CCSFConstants.ASSESSMENT.STATUS_NEW;                                        
            assessment.SubStatus__c= null;
            assessment.IntegrationStatus__c = null;
            //ASR-8544 Changes Ends
            //Addedd as per bug ASR-7853 Clone assessment from Auto Proccessed should not be auto Processed 
            assessment.AutoProcessed__c = false;
            //ASR-9701. Enrolled Date should be blank after cloning an assessment
            assessment.EnrolledDate__c = null;

            //ASR-10768 Changes
            assessment.AdjustmentReason__c = null;

            // as part of ASR 10596	
            assessment.ParentId = null;
        }
        //insert assessments
        Map<String,Object> insertResultMap = DescribeUtility.insertRecords(assessments,dmlOptions);
        return (Boolean)insertResultMap.get('insertSuccess');

    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns Roll_Year__c object record for requested year
    // @return : roll year record
    //-----------------------------
    public static RollYear__c getRollYearDetails(String year) {

        RollYear__c[] rollYear = [SELECT Id,Name,Status__c,IsLowValueBulkGeneratorProcessed__c,
                                  IsDirectBillBulkGeneratorProcessed__c,
                                  IsNonFilerBulkGeneratorProcessed__c,
                                  BPPLateFilingDate__c,IsVesselNonFilerGeneratorProcessed__c
                                  FROM RollYear__c WHERE Year__c =: year  limit 1];

        //check query result and return first returned result's id
        if(rollYear.size()>0)
        {
            return rollYear[0];
        }

        //default retun null
        return null;
    }
}