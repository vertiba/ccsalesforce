/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for AuthProviderRegistrationHandler
 * @Date: 01/28/2020
 * @Author: Srini Aluri(A1)
 
 * ModifiedBy         ModifiedDate   Description
 * Srini Aluri(A1)    01/28/2020     Initial development
*/

@isTest
public class AuthProviderRegistrationHandlerTest {

    // Description: Test functionality of createUser() global function & updateUser function in general
    static testMethod void testRegistrationHandler1() {
        // 1st test createUser()
        AuthProviderRegistrationHandler handler = new AuthProviderRegistrationHandler();
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                     'testFirst testLast', 'testuser@example.org', 
                                                     null, 'testuserlong', 'en_US', 'srini',
                                                     null, new Map<String, String>{'language' => 'en_US'});
        User user = handler.createUser(null, sampleData);
        System.assertEquals('testuser@example.org.srini.ccsf.community', user.userName);
        INSERT user;
        
        // 2nd test updateUser()
        String userId = user.Id;        
        sampleData = new Auth.UserData('testNewId', 'testNewFirst', 'testNewLast',
                                       'testNewFirst testNewLast', 'testnewuser@example.org', 
                                       null, 'testnewuserlong', 'en_US', 'newsrini',
                                       null, new Map<String, String>{});
        handler.updateUser(userId, null, sampleData);
        
        User updatedUser = [SELECT UserName, Email, FirstName, LastName, Alias 
                            FROM User 
                            WHERE Id = :userId];
        System.assertEquals('testnewuser@example.org', updatedUser.Email);
    }
    
    
    // Description: Test functionality of createUser() as a Community User
    static testMethod void testRegistrationHandler2() {        
        AuthProviderRegistrationHandler handler = new AuthProviderRegistrationHandler();
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                     'testFirst testLast', 'testuser@example.org', 
                                                     null, 'testuserlong', 'en_US', 'srini',
                                                     null, new Map<String, String>{'language' => 'en_US', 'sfdc_networkid' => 'NT9889PLOKIJ9898'});
        User user = handler.createUser(null, sampleData);
        System.assertEquals(true, String.isNotBlank(user.ProfileId));
    }
}