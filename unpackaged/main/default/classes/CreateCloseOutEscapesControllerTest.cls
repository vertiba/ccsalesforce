/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// Batch ->Test class for CreateCloseOutEscapesController Apex class

@isTest
public class CreateCloseOutEscapesControllerTest {
    
    @testSetup
    private static void dataSetup(){

        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin); 
        User officeAssistant = TestDataUtility.getOfficeAssistantUser();
        users.add(officeAssistant);
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
          
            insert penalty1;
            
            Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            Id businessPPPropertyRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Business Personal Property').getRecordTypeId();
            Id bPPAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('BPP Assessment').getRecordTypeId();
            Account account = TestDataUtility.buildAccount('Escape Closeout Test Account','Notice to File',businessAccountRecordTypeId);
            insert account;
            
            List<Property__c> properties = new List<Property__c>();
            Property__c propertyWithNoBusinessOpendate = TestDataUtility.buildProperty('TestProperty1',businessPPPropertyRecordTypeId);
            Property__c propertyWithBusinessOpendate = TestDataUtility.buildProperty('TestProperty2',businessPPPropertyRecordTypeId);
            propertyWithBusinessOPendate.BusinessLocationOpenDate__c = Date.newInstance(system.today().year() -2, 01,01);
			propertyWithNoBusinessOPendate.Account__c = account.id;
            propertyWithBusinessOPendate.Account__c = account.id;
            properties.add(propertyWithNoBusinessOPendate);
            properties.add(propertyWithBusinessOPendate);

            insert properties;
            
            String currentFiscalyear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            String previousFiscalyear = String.valueOf(Integer.valueOf(currentFiscalyear)-1);
            Test.startTest();
            	
                RollYear__c closedRollyear = TestDataUtility.buildRollYear(previousFiscalyear,previousFiscalyear,'Roll Closed');
                insert closedRollyear;
            	List<Case> caseList = new List<Case>();
                Date eventDate1 = Date.newInstance(integer.valueof(closedRollyear.Year__c), 01, 01);
                RollYear__c rollYear2 = TestDataUtility.buildRollYear(currentFiscalyear,currentFiscalyear,'Roll Open');
                insert rollYear2;
                
                Case priorYearAssessment = TestDataUtility.buildAssessment(bPPAssessmentRecordTypeId,propertyWithNoBusinessOpendate.Id,closedRollyear.id,closedRollyear.Year__c,
                                                                            closedRollyear.Year__c,'Escape',48, 'Notice to File',null,eventDate1);
                priorYearAssessment.AdjustmentType__c = 'New';
               
            	caseList.add(priorYearAssessment);
                Date eventDate2 = Date.newInstance(integer.valueof(rollYear2.Year__c), 01, 01);
                Case currentYearAssessment = TestDataUtility.buildAssessment(bPPAssessmentRecordTypeId,propertyWithNoBusinessOpendate.Id,rollYear2.id,rollYear2.Year__c,
                                                                            rollYear2.Year__c,'Regular',48, 'Notice to File',null,eventDate2);
                                                                        
                currentYearAssessment.subtype__c = 'Closeout';
                currentYearAssessment.AdjustmentType__c = 'New';
               
                caseList.add(currentYearAssessment);
            
            	Case nonCloseoutregularCase = TestDataUtility.buildAssessment(bPPAssessmentRecordTypeId,propertyWithBusinessOpendate.Id,rollYear2.id,rollYear2.Year__c,
                                                                            rollYear2.Year__c,'Regular',48, 'Notice to File',null,eventDate2);
            	nonCloseoutregularCase.AdjustmentType__c = 'New';
            	caseList.add(nonCloseoutregularCase);
            	insert caseList;
    
                FactorSource__c factorSource = new FactorSource__c(name = 'Commercial CAA Factor Source');
                insert factorSource;
                BusinessAssetMapping__c businessAssetMapping = TestDataUtility.getBusinessAssetMapping('Machinery & Equipment','General',String.valueOf(factorSource.id),20.00,'Depreciation Factor');
                insert businessAssetMapping;
            	
                Factor__c factor = TestDataUtility.getfactor(previousFiscalyear,rollYear2.Year__c,factorSource.Id,95.00,4);
                insert factor;
                
            	ProcessBuilderSettings__c processBuilderSettings = ProcessBuilderSettings__c.getInstance(UserInfo.getUserId());
                processBuilderSettings.DisableAllProcesses__c = true;
                upsert processBuilderSettings;
            
            	List<AssessmentLineItem__c> lineItems = new list<AssessmentLineItem__c>();
                AssessmentLineItem__c lineItem = TestDataUtility.buildAssessmentLineItem('Equipment','Machinery & Equipment',previousFiscalyear,1000.00,currentYearAssessment.id,'Description');
               
                lineItems.add(lineItem);
                String previousToPreviousYear = String.valueOf(Integer.valueOf(currentFiscalyear)-2);
                AssessmentLineItem__c lineItem2 = TestDataUtility.buildAssessmentLineItem('Equipment','Machinery & Equipment',previousToPreviousYear,1000.00,currentYearAssessment.id,'Description');
               
                lineItems.add(lineItem2);
            	insert lineItems;
            
            	ProcessBuilderSettings__c processBuilderSettings1 = ProcessBuilderSettings__c.getInstance(UserInfo.getUserId());
                processBuilderSettings1.DisableAllProcesses__c = false;
                upsert processBuilderSettings1;
                 
                TriggerHandlerSettings__c settings = TriggerHandlerSettings__c.getOrgDefaults();
                settings.DisableDLRSCalculationTriggers__c = true;
                upsert settings TriggerHandlerSettings__c.Id;
            Test.stopTest();
        }

    }

    @isTest
    private static void createEscapesTest() { 

        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,FinalCloseOutValue__c,integrationstatus__c ,RecordTypeId from Case Where subtype__c =: 'Closeout'];
        cs.FinalCloseOutValue__c =5000;
       
            
        CreateCloseOutEscapesController extension = new CreateCloseOutEscapesController(new ApexPages.standardController(cs));
        System.runAs(sysAdminUser){
            Test.startTest();
            	update cs;
                extension.redirect(); 
            Test.stopTest();
            
            List<Case> escapeCloseouts = [SELECT id,TotalPersonalPropertyValueOverride__c,(Select id,Acquisitionyear__c from AssessmentLineItems__r) FROM Case WHERE type =: 'Escape' AND subtype__c =: 'Closeout' ];
           	System.assert(escapeCloseouts.size() == 1 , 'Escape Closeouts are created');
            System.assertEquals(escapeCloseouts[0].TotalPersonalPropertyValueOverride__c, cs.FinalCloseOutValue__c);
            
            List<AssessmentLineItem__c> lineItems = [Select id,AssessmentYear__c,Acquisitionyear__c from AssessmentLineItem__c WHERE Case__c !=: cs.Id];          
            system.assertEquals(lineItems.size(),escapeCloseouts[0].AssessmentLineItems__r.size());
        }
        
    }
    
     @isTest
    private static void noFinalCloseValueTest() { 

        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,integrationstatus__c ,FinalCloseOutValue__c,RecordTypeId from Case Where subtype__c =: 'Closeout'];
      
        CreateCloseOutEscapesController extension = new CreateCloseOutEscapesController(new ApexPages.standardController(cs));
        System.runAs(sysAdminUser){
            Test.startTest();
                extension.redirect(); 
            Test.stopTest();
            List<Case> escapeCloseouts = new List<Case>();
            escapeCloseouts = [SELECT id FROM Case WHERE type =: 'Escape' AND subtype__c =: 'Closeout' ];
            System.assert(escapeCloseouts.isEmpty(), 'Without Closeout value Escape Closeouts cannot be created');
            System.assertEquals(null, cs.FinalCloseOutValue__c, 'No Escape Closeout created as Final Closeout Value is Null on regular closeout');
        }
    }
    @isTest
    private static void testRedirectAsOfficeAssisstant(){
        User officeAssistantUser = [SELECT Id FROM User WHERE Profile.Name !=: 'System Administrator' AND Profile.Name !=: 'BPP Staff' LIMIT 1]; 
        
        Case cs = [Select Id,RecordTypeId from Case WHERE subtype__c =: 'Closeout' LIMIT 1];
        System.runAs(officeAssistantUser){
            CreateCloseOutEscapesController extension = new CreateCloseOutEscapesController(new ApexPages.standardController(cs));
            
            Test.startTest();
            extension.redirect(); 
            Test.stopTest();
            
            System.assertEquals(System.Label.NoPermission, extension.recordStatus);
        }
    }
    
    @isTest
    private static void testNonCloseoutRegularCase(){
         User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,integrationstatus__c ,FinalCloseOutValue__c,RecordTypeId from Case Where Type =: 'Regular' AND subtype__c !=: 'Closeout'];
        
        System.runAs(sysAdminUser){
            CreateCloseOutEscapesController extension = new CreateCloseOutEscapesController(new ApexPages.standardController(cs));
            
            Test.startTest();
            	extension.redirect(); 
            Test.stopTest();
            
            System.assertEquals(CCSFConstants.ESCAPECLOSEOUTS.TYPE_SUBTYPE_MISMATCH_ERROR_MESSAGE, extension.recordStatus);
        }
    }
    
      @isTest
    private static void testNonRegularCase(){
         User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,integrationstatus__c ,FinalCloseOutValue__c,RecordTypeId from Case Where Type =: 'Escape'];
        
        System.runAs(sysAdminUser){
            CreateCloseOutEscapesController extension = new CreateCloseOutEscapesController(new ApexPages.standardController(cs));
            
            Test.startTest();
            	extension.redirect(); 
            Test.stopTest();
            
            System.assertEquals(CCSFConstants.ESCAPECLOSEOUTS.TYPE_SUBTYPE_MISMATCH_ERROR_MESSAGE, extension.recordStatus);
        }
    }
    
}