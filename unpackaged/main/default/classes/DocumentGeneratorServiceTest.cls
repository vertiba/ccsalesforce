/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class DocumentGeneratorServiceTest {

    private final static String EXPECTED_SMARTCOMM_EXCEPTION = System.Label.SmartCOMM_License_Exception;

    @testSetup
    static void setupData() {
        insert DocumentGeneratorServiceSettings__c.getInstance();

        TH1__Document_Setting__c documentSetting = new TH1__Document_Setting__c(
            Name = 'Test document setting'
        );
        new TestDataFactory(documentSetting).populateRequiredFields();
        insert documentSetting;

        Account account = new Account(
            Name = 'Test account'
        );
        new TestDataFactory(account).populateRequiredFields();
        insert account;
    }

    @isTest
    static void it_should_generate_document_for_approved_request() {
        TH1__Document_Setting__c documentSetting = [SELECT Id FROM TH1__Document_Setting__c];
        Account account = [SELECT Id FROM Account];

        DocumentGenerationRequest__c request = new DocumentGenerationRequest__c(
            DocumentSetting__c = documentSetting.Id,
            RelatedRecordId__c = account.Id,
            Status__c          = 'Approved'
        );
        insert request;

        Test.startTest();

        DocumentGeneratorService documentGeneratorService = new DocumentGeneratorService();
        Database.executeBatch(documentGeneratorService);

        Test.stopTest();

        request = [SELECT Id, DocumentSetting__c, Status__c, ProcessingError__c FROM DocumentGenerationRequest__c WHERE Id = :request.Id];
        System.debug('request=' + request);
        // SmartCOMM does not allow us generate documents in a unit test, so check for their exception
        System.assert(request.ProcessingError__c.startsWith(EXPECTED_SMARTCOMM_EXCEPTION), request.ProcessingError__c);
    }

}