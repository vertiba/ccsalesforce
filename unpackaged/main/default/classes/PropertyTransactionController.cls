/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class PropertyTransactionController {

    @AuraEnabled
    public static List<Ownership__c> getCurrentOwners(Id propertyId) {
        return [
            SELECT Id, Name, account__c, account__r.Name, StartDate__c, EndDate__c, OwnershipPercent__c
            FROM Ownership__c
            WHERE property__c = :propertyId
            AND EndDate__c = null
            ORDER BY account__r.Name
        ];
    }

    @AuraEnabled
    public static List<Account> searchAccounts(Id propertyId, String searchTerm) {
        String cleanedSearchTerm = '%' + String.escapeSingleQuotes(searchTerm) + '%';

        return [
            SELECT Id, Name, RecordTypeId, RecordType.Name
            FROM Account
            WHERE Name LIKE :cleanedSearchTerm
            ORDER BY Name
        ];
    }

    @AuraEnabled
    public static PropertyTransaction__c saveData(PropertyTransaction__c propertyTransaction, List<Ownership__c> sellers, List<SObject> buyers) {
        System.debug('propertyTransaction=' + propertyTransaction);
        System.debug('sellers=' + sellers);
        System.debug('buyers=' + buyers);
        SavePoint savepoint = Database.setSavepoint();
        try {
            insert propertyTransaction;

            List<OwnershipTransaction__c> ownershipTransactions = new List<OwnershipTransaction__c>();

            Decimal percentBeingSold = 0.00;
            for(Ownership__c seller : sellers) {
                seller.EndDate__c         = propertyTransaction.TransactionDate__c;
                //seller.SellTransaction__c = propertyTransaction.Id;

                //percentBeingSold += seller.OwnershipPercent__c;

                ownershipTransactions.add(new OwnershipTransaction__c(
                    // TODO - temp way to make it a negative, but UI needs to specify the % being sold
                    //ChangeInOwnershipPercent__c = (seller.OwnershipPercent__c * -1.00).setScale(2, RoundingMode.HALF_UP),
                    Ownership__c                = seller.Id,
                    PropertyTransaction__c      = propertyTransaction.Id,
                    Role__c                   = 'Seller'
                ));
            }
            update sellers;

            System.assert(buyers.size() > 0);
            List<Ownership__c> buyerOwnerships = new List<Ownership__c>();
            for(Account buyer : (List<Account>)buyers) {
                buyerOwnerships.add(new Ownership__c(
                    account__c             = buyer.Id,
                    CompositeKey__c        = '' + buyer.Id + propertyTransaction.Property__c,
                    EndDate__c             = null,
                    //OwnershipPercent__c    = (percentBeingSold / buyers.size()).setScale(2, RoundingMode.HALF_UP),
                    Property__c            = propertyTransaction.Property__c,
                    //PurchaseTransaction__c = propertyTransaction.Id,
                    StartDate__c           = propertyTransaction.TransactionDate__c
                    
                ));
            }
            upsert buyerOwnerships CompositeKey__c;
            for(Ownership__c buyerOwnership : buyerOwnerships) {
                ownershipTransactions.add(new OwnershipTransaction__c(
                    // TODO - temp way to make it a negative, but UI needs to specify the % being sold
                    //ChangeInOwnershipPercent__c = buyerOwnership.OwnershipPercent__c,
                    Ownership__c                = buyerOwnership.Id,
                    PropertyTransaction__c      = propertyTransaction.Id,
                    Role__c                   = 'Buyer'
                ));
            }
            insert ownershipTransactions;

            return propertyTransaction;
        } catch(Exception ex) {
            Database.rollback(savePoint);
            throw new AuraHandledException('Error Type: ' + ex.getTypeName() + '\nError Message: ' + ex.getMessage());
        }
    }

}