/*************************************************************************************************
* This file is part of the Nebula Logger project, released under the MIT License.                *
* See LICENSE file or go to https://github.com/jongpie/NebulaLogger for full license details.    *
*************************************************************************************************/
public without sharing class Logger {

    private static final String TRANSACTION_ID              = new Uuid().getValue();
    private static final List<LogEntryEvent__e> LOG_ENTRIES = new List<LogEntryEvent__e>();

    private static Boolean suspendSaving = false;

    public static String getTransactionId() {
        return TRANSACTION_ID;
    }

    public static void suspendSaving() {
        suspendSaving = true;
    }

    public static void resumeSaving() {
        suspendSaving = false;
    }

    public static void flushBuffer() {
        LOG_ENTRIES.clear();
    }

    public static String addDebugEntry(String message) {
        return addDebugEntry(message, null);
    }

    public static String addDebugEntry(String message, String originLocation) {
        return addDebugEntry(null, message, originLocation);
    }

    public static String addDebugEntry(LoggingLevel loggingLevel, String message, String originLocation) {
        return addEntry(loggingLevel, null, message, null, 'Apex', originLocation, null, null);
    }

    public static String addDebugEntry(LoggingLevel loggingLevel, String message, String originLocation, List<String> topics) {
        return addEntry(loggingLevel, null, message, null, 'Apex', originLocation, topics, null);
    }

    public static String addExceptionEntry(Exception ex) {
        return addExceptionEntry(ex, null);
    }

    public static String addExceptionEntry(Exception ex, String originLocation) {
        return addExceptionEntry(ex, originLocation, null);
    }

    public static String addExceptionEntry(Exception ex, String originLocation, List<String> topics) {
        return addEntry(LoggingLevel.ERROR, null, ex.getMessage(), ex, 'Apex', originLocation, topics, null);
    }

    public static String addRecordDebugEntry(SObject record, String message) {
        return addRecordDebugEntry(record.Id, message);
    }
    
    public static String addRecordDebugEntry(Id recordId, String message) {
        return addRecordDebugEntry(recordId, message, null);
    }

    public static String addRecordDebugEntry(SObject record, String message, String originLocation) {
        return addRecordDebugEntry(record.Id, message, originLocation);
    }
    
    public static String addRecordDebugEntry(Id recordId, String message, String originLocation) {
        return addRecordDebugEntry(null, recordId, message, originLocation);
    }

    public static String addRecordDebugEntry(LoggingLevel loggingLevel, SObject record, String message, String originLocation) {
        return addRecordDebugEntry(loggingLevel, record.Id, message, originLocation);
    }
    
    public static String addRecordDebugEntry(LoggingLevel loggingLevel, Id recordId, String message, String originLocation) {
        return addRecordDebugEntry(loggingLevel, recordId, message, originLocation, null);
    }

    public static String addRecordDebugEntry(LoggingLevel loggingLevel, SObject record, String message, String originLocation, List<String> topics) {
        return addRecordDebugEntry(loggingLevel, record.Id, message, originLocation, topics);
    }
    
    public static String addRecordDebugEntry(LoggingLevel loggingLevel, Id recordId, String message, String originLocation, List<String> topics) {
        return addEntry(loggingLevel, recordId, message, null, 'Apex', originLocation, topics, null);
    }

    public static String addRecordExceptionEntry(SObject record, Exception ex) {
        return addRecordExceptionEntry(record.Id, ex, null);
    }
    
    public static String addRecordExceptionEntry(Id recordId, Exception ex) {
        return addRecordExceptionEntry(recordId, ex, null);
    }

    public static String addRecordExceptionEntry(SObject record, Exception ex, String originLocation) {
        return addRecordExceptionEntry(record.Id, ex, originLocation, null);
    }
    
    public static String addRecordExceptionEntry(Id recordId, Exception ex, String originLocation) {
        return addRecordExceptionEntry(recordId, ex, originLocation, null);
    }

    public static String addRecordExceptionEntry(SObject record, Exception ex, String originLocation, List<String> topics) {
        return addEntry(LoggingLevel.ERROR, record.Id, ex.getMessage(), ex, 'Apex', originLocation, topics, null);
    }
    
    public static String addRecordExceptionEntry(Id recordId, Exception ex, String originLocation, List<String> topics) {
        return addEntry(LoggingLevel.ERROR, recordId, ex.getMessage(), ex, 'Apex', originLocation, topics, null);
    }

    public static void addFlowEntries(List<FlowLogEntry> flowLogEntries) {
        Boolean saveLog = false;
        for(FlowLogEntry flowLogEntry : flowLogEntries) {
            LoggingLevel loggingLevel;
            if(!String.isBlank(flowLogEntry.LoggingLevelName)) loggingLevel = getLoggingLevel(flowLogEntry.LoggingLevelName);

            if(flowLogEntry.saveLog) saveLog = true;

            addEntry(loggingLevel, flowLogEntry.recordId, flowLogEntry.message, null, 'Process Builder/Flow', flowLogEntry.flowName, flowLogEntry.topics, null);
        }
        if(saveLog) saveLog();
    }

    @AuraEnabled
    public static void saveLightningEntries(String logEntriesJson) {
        List<LightningLogEntry> lightningLogEntries = (List<LightningLogEntry>)Json.deserialize(logEntriesJson, List<LightningLogEntry>.class);
        for(LightningLogEntry logEntry : lightningLogEntries) {
            LoggingLevel logLevel;
            if(!String.isBlank(logEntry.loggingLevelName)) logLevel = getLoggingLevel(logEntry.loggingLevelName);
            if(logEntry.error != null) logLevel = LoggingLevel.ERROR;

            addEntry(logLevel, null, logEntry.message, null, 'Lightning Component', logEntry.originLocation, logEntry.topics, logEntry);
        }
        saveLog();
    }

    public static LoggingLevel getLoggingLevel(String loggingLevelName) {
        if(loggingLevelName != null) loggingLevelName = loggingLevelName.toUpperCase();

        switch on loggingLevelName {
            when 'NONE'   { return LoggingLevel.NONE;   }
            when 'ERROR'  { return LoggingLevel.ERROR;  }
            when 'WARN'   { return LoggingLevel.WARN;   }
            when 'INFO'   { return LoggingLevel.INFO;   }
            when 'DEBUG'  { return LoggingLevel.DEBUG;  }
            when 'FINE'   { return LoggingLevel.FINE;   }
            when 'FINER'  { return LoggingLevel.FINER;  }
            when 'FINEST' { return LoggingLevel.FINEST; }
            when else     { return LoggingLevel.DEBUG;  }
        }
    }

    @InvocableMethod(label='Save Log' description='Saves any log entries that have been generated')
    public static void saveLog() {
        if(suspendSaving) return;
        if(LOG_ENTRIES.isEmpty()) return;

        List<Database.SaveResult> saveResults = EventBus.publish(LOG_ENTRIES);
        List<Database.Error> saveErrors = new List<Database.Error>();
        for(Database.SaveResult result : saveResults) {
            if(result.isSuccess()) continue;

            saveErrors.addAll(result.getErrors());
        }
        for(Database.Error error : saveErrors) {
            String errorMessage = 'Failed to publish platform event: ' + error.getMessage();
            System.debug(LoggingLevel.WARN, errorMessage);
        }

        LOG_ENTRIES.clear();
    }

    private static LoggerSettings__c getSettings() {
        return LoggerSettings__c.getInstance();
    }

    private static LoggingLevel getUserLoggingLevel() {
        return getLoggingLevel(getSettings().LoggingLevel__c);
    }

    private static LoggingLevel getOriginLocationLoggingLevel(String originType, String originLocation) {
        // For Apex, assume format is 'MyClass.myMethod'
        // Get just the class name to check the custom setting
        if(originType == 'Apex' && originLocation != null && originLocation.indexOf('.') > 0) {
            String apexClassName = originLocation.substring(0, originLocation.indexOf('.'));
            originLocation = apexClassName;
        }

        // If settings have been configured for this origin location, then use its logging level
        Integer originSettingsNameLength = Schema.LoggerOriginLocationSettings__c.Name.getDescribe().getLength();
        if(originLocation.length() > originSettingsNameLength) originLocation = originLocation.left(originSettingsNameLength);

        // Check if the originName has a logging level configured
        LoggerOriginLocationSettings__c originLocationSettings = LoggerOriginLocationSettings__c.getInstance(originLocation);
        LoggingLevel originLocationLoggingLevel;
        if(originLocationSettings != null) {
            originLocationLoggingLevel = getLoggingLevel(originLocationSettings.LoggingLevel__c);
        }

        return originLocationLoggingLevel;
    }

    private static Boolean meetsUserLoggingLevel(LoggingLevel userLoggingLevel, LoggingLevel logEntryLoggingLevel) {
        if(logEntryLoggingLevel == LoggingLevel.NONE) return false;

        return userLoggingLevel.ordinal() <= logEntryLoggingLevel.ordinal();
    }

    private static String truncateFieldValue(Schema.SObjectField field, String value) {
        Integer fieldMaxLength = field.getDescribe().getLength();
        if(String.isEmpty(value)) return value;
        else if(value.length() <= fieldMaxLength) return value;
        else return value.left(fieldMaxLength);
    }

    private static String getExceptionStackTrace(Exception ex, LightningLogEntry lightningLogEntry) {
        if(ex != null) return ex.getStackTraceString();
        else if(lightningLogEntry != null && lightningLogEntry.error != null) return lightningLogEntry.error.stack;
        else return null;
    }

    private static String getExceptionType(Exception ex, LightningLogEntry lightningLogEntry) {
        if(ex != null) return ex.getTypeName();
        else if(lightningLogEntry != null && lightningLogEntry.error != null) return 'LightningError';
        else return null;
    }

    private static String getApexOriginLocation() {
        // The only way to get a stack trace in Apex is to use a standard exception
        // But we don't have to throw the exception - we just need to initialize it to get the stack trace
        String originLocation;
        for(String currentStackTraceLine : new DmlException().getStackTraceString().split('\n')) {
            if(currentStackTraceLine.contains('.' + Logger.class.getName() + '.')) continue;

            originLocation = currentStackTraceLine.substringBefore(':');
            if(originLocation.startsWith('Class.')) {
                originLocation = originLocation.substringAfter('Class.');
            }
            break;
        }

        return originLocation;
    }

    private static String addEntry(LoggingLevel logEntryLoggingLevel, Id recordId, String message, Exception ex, String originType, String originLocation, List<String> topics, LightningLogEntry lightningLogEntry) {
        Datetime logEntryTimestamp = lightningLogEntry == null ? System.now() : lightningLogEntry.timestamp;

        if(originType == 'Apex') originLocation = getApexOriginLocation();
        if(logEntryLoggingLevel == null) logEntryLoggingLevel = LoggingLevel.DEBUG;
        if(ex != null) message = ex.getMessage();
        if(lightningLogEntry != null && lightningLogEntry.error != null) message = lightningLogEntry.error.message;

        LoggingLevel originLocationLoggingLevel = getOriginLocationLoggingLevel(originType, originLocation);
        if(originLocationLoggingLevel != null) logEntryLoggingLevel = originLocationLoggingLevel;

        if(getSettings().GenerateDebugStatements__c) System.debug(logEntryLoggingLevel, message);
        if(!meetsUserLoggingLevel(getUserLoggingLevel(), logEntryLoggingLevel)) return null;

        String type = 'Debug';
        if(ex != null) type = 'Exception';
        if(lightningLogEntry != null && lightningLogEntry.error != null) type = 'Exception';

        if(type == 'Debug' && !getSettings().StoreDebugLogEntries__c) return null;
        if(type == 'Exception' && !getSettings().StoreExceptionLogEntries__c) return null;

        String truncatedMessage = truncateFieldValue(Schema.LogEntryEvent__e.Message__c, message);
        Boolean messageTruncated = message != truncatedMessage;

        String visualforcePageName = ApexPages.currentPage() == null ? null : ApexPages.currentPage().getUrl();
        if(visualforcePageName != null && visualforcePageName.contains('?')) visualforcePageName = visualforcePageName.substringBetween('apex/', '?');
        else if(visualforcePageName != null) visualforcePageName = visualforcePageName.substringAfter('apex/');

        LogEntryEvent__e platformLogEntry = new LogEntryEvent__e(
            ContextIsApexRest__c              = RestContext.request != null,
            ContextIsBatch__c                 = System.isBatch(),
            ContextIsFuture__c                = System.isFuture(),
            ContextIsLightningComponent__c    = lightningLogEntry != null,
            ContextIsQueueable__c             = System.isQueueable(),
            ContextIsScheduledJob__c          = System.isScheduled(),
            ContextIsTriggerExecuting__c      = Trigger.isExecuting,
            ContextIsVisualforce__c           = ApexPages.currentPage() != null,
            ContextLightningComponentName__c  = lightningLogEntry == null ? null : lightningLogEntry.componentName,
            ContextThemeDisplayed__c          = UserInfo.getUiThemeDisplayed(),
            ContextTriggerOperationType__c    = Trigger.operationType == null ? null : Trigger.operationType.name(),
            ContextTriggerSObjectType__c      = Trigger.new == null ? null : String.valueOf(Trigger.new.getSObjectType()),
            ContextVisualforcePage__c         = visualforcePageName,
            ExceptionStackTrace__c            = truncateFieldValue(Schema.LogEntryEvent__e.ExceptionStackTrace__c, getExceptionStackTrace(ex, lightningLogEntry)),
            ExceptionType__c                  = truncateFieldValue(Schema.LogEntryEvent__e.ExceptionType__c, getExceptionType(ex, lightningLogEntry)),
            LimitsAggregateQueriesMax__c      = Limits.getLimitAggregateQueries(),
            LimitsAggregateQueriesUsed__c     = Limits.getAggregateQueries(),
            LimitsAsyncCallsMax__c            = Limits.getLimitAsyncCalls(),
            LimitsAsyncCallsUsed__c           = Limits.getAsyncCalls(),
            LimitsCalloutsMax__c              = Limits.getLimitCallouts(),
            LimitsCalloutsUsed__c             = Limits.getCallouts(),
            LimitsCpuTimeMax__c               = Limits.getLimitCpuTime(),
            LimitsCpuTimeUsed__c              = Limits.getCpuTime(),
            LimitsDmlRowsMax__c               = Limits.getLimitDmlRows(),
            LimitsDmlRowsUsed__c              = Limits.getDmlRows(),
            LimitsDmlStatementsMax__c         = Limits.getLimitDmlStatements(),
            LimitsDmlStatementsUsed__c        = Limits.getDmlStatements(),
            LimitsEmailInvocationsMax__c      = Limits.getLimitEmailInvocations(),
            LimitsEmailInvocationsUsed__c     = Limits.getEmailInvocations(),
            LimitsFutureCallsMax__c           = Limits.getLimitFutureCalls(),
            LimitsFutureCallsUsed__c          = Limits.getFutureCalls(),
            LimitsHeapSizeMax__c              = Limits.getLimitHeapSize(),
            LimitsHeapSizeUsed__c             = Limits.getHeapSize(),
            LimitsMobilePushApexCallsMax__c   = Limits.getLimitMobilePushApexCalls(),
            LimitsMobilePushApexCallsUsed__c  = Limits.getMobilePushApexCalls(),
            LimitsQueueableJobsMax__c         = Limits.getLimitQueueableJobs(),
            LimitsQueueableJobsUsed__c        = Limits.getQueueableJobs(),
            LimitsSoqlQueriesMax__c           = Limits.getLimitQueries(),
            LimitsSoqlQueriesUsed__c          = Limits.getQueries(),
            LimitsSoqlQueryLocatorRowsMax__c  = Limits.getLimitQueryLocatorRows(),
            LimitsSoqlQueryLocatorRowsUsed__c = Limits.getQueryLocatorRows(),
            LimitsSoqlQueryRowsMax__c         = Limits.getLimitQueryRows(),
            LimitsSoqlQueryRowsUsed__c        = Limits.getQueryRows(),
            LimitsSoslSearchesMax__c          = Limits.getLimitSoslQueries(),
            LimitsSoslSearchesUsed__c         = Limits.getSoslQueries(),
            LoggingLevel__c                   = logEntryLoggingLevel.name(),
            LoggingLevelOrdinal__c            = logEntryLoggingLevel.ordinal(),
            Message__c                        = truncatedMessage == null ? null : String.escapeSingleQuotes(truncatedMessage),
            MessageTruncated__c               = messageTruncated,
            OriginLocation__c                 = truncateFieldValue(Schema.LogEntryEvent__e.OriginLocation__c, originLocation),
            OriginType__c                     = originType,
            RelatedRecordId__c                = recordId,
            Timestamp__c                      = logEntryTimestamp,
            Topics__c                         = topics == null ? null : String.escapeSingleQuotes(String.join(topics, ',')),
            TransactionId__c                  = TRANSACTION_ID,
            Type__c                           = type,
            UserLoggingLevel__c               = getUserLoggingLevel().name(),
            UserLoggingLevelOrdinal__c        = getUserLoggingLevel().ordinal()
        );
        LOG_ENTRIES.add(platformLogEntry);

        if(ex != null && getSettings().AutoSaveExceptionEntries__c) saveLog();

        return platformLogEntry.TransactionEntryId__c;
    }

}