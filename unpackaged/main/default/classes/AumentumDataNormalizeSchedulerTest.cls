@isTest
public class AumentumDataNormalizeSchedulerTest {

    @isTest
    private static void AumentumSchedulerTest() {
    Test.startTest();
        AumentumDataNormalizeScheduler sched = new AumentumDataNormalizeScheduler();
        String jobId = System.schedule('AumentumDataNormalizeScheduler',
                                       '0 0 0 15 3 ? 2032', 
                                       sched); 
        sched.execute(null);
        System.debug('jobId'+jobId);
     	String s =sched.getJobNamePrefix();
        Test.stopTest();
              
        system.assertEquals(1, [SELECT count() FROM CronTrigger where id = :jobId],
                            'A job should be scheduled');
    }
}