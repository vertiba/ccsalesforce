/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR- 2366, task -8502
// @description : This controller class is used to cleanup Address data coming from Integration layer in StagingAumentumBusiness object
//-----------------------------

public class AumentumAutoCleanupController {
    
    public static final Set<String> STREET_TYPES = DescribeUtility.getPicklistAndValues('Account', 'MailingStreetType__c');
    public static final Set<String> UNIT_TYPES = DescribeUtility.getPicklistAndValues('Account', 'MailingUnitType__c');
    public static final Set<String> DIRECTIONS = DescribeUtility.getPicklistAndValues('Account', 'MailingStreetPreDirection__c'); 
    public static final String ORIGIN_LOCATION = CCSFConstants.AUMENTUM_PROCESS.AUMENTUM_AUTO_CLEANUP_CONTROLLER;


    //-----------------------------
    // @author : Publicis Sapient 
    // @param : 
    // @description : This method takes 
    // @return : String
    //-----------------------------

    public static List<StagingAumentumBusiness__c> cleanUpRecords(List<StagingAumentumBusiness__c> businessRecords){

        List<StagingAumentumBusiness__c> recordsToUpdate = new List<StagingAumentumBusiness__c>();
        List<String> contactStreetNames = new List<String>();
        Set<String> removeElementsFromLocationStreetName= new Set<String>();
        Set<String> removeElementsFromContactStreetName = new Set<String>();
        try{
        for(StagingAumentumBusiness__c record: businessRecords){
            StagingAumentumBusiness__c recordNewValue = new StagingAumentumBusiness__c();
            recordNewValue.Id = record.Id;
            // PO Box 
            if(String.isNotBlank(record.ContactStreetNumber__c) && checkIsPoBox(record.ContactStreetNumber__c) && String.isNotBlank(record.ContactStreetName__c)){                
               // if Contact Street number contains PO BOX and Contact Street is not Empty  - BUG 9074              
                recordNewValue.ContactStreetType__c= String.isBlank(record.ContactStreetType__c) ? populateStreetTypeAndUnitType(record.ContactStreetName__c,true): record.ContactStreetType__c;
                recordNewValue.ContactUnitType__c= String.isBlank(record.ContactUnitType__c)? populateStreetTypeAndUnitType(record.ContactStreetName__c,false):record.ContactUnitType__c;
                recordNewValue.ContactPreDirection__c=String.isBlank(record.ContactPreDirection__c)?populatePreDirection(record.ContactStreetName__c):record.ContactPreDirection__c;
                removeElementsFromContactStreetName.add(recordNewValue.ContactStreetType__c);
                removeElementsFromContactStreetName.add(recordNewValue.ContactUnitType__c);
                removeElementsFromContactStreetName.add(recordNewValue.ContactPreDirection__c);
                recordNewValue.ContactStreetName__c = record.ContactStreetNumber__c +' '+populateStreetName(record.ContactStreetName__c, removeElementsFromContactStreetName);
                recordNewValue.ContactStreetNumber__c='';              

            }else if(String.isNotBlank(record.ContactStreetNumber__c) && checkIsPoBox(record.ContactStreetNumber__c) && String.isBlank(record.ContactStreetName__c)){
                // if Contact Street Number contains PO Box and Contact Street is Empty
                recordNewValue.ContactStreetName__c = record.ContactStreetNumber__c;
            }
            else if(String.isNotBlank(record.ContactStreetName__c) && checkIsPoBox(record.ContactStreetName__c)){
                // if Contact Street Name contains PO BOX BUG 9074              
                recordNewValue.ContactStreetType__c= String.isBlank(record.ContactStreetType__c) ? populateStreetTypeAndUnitType(record.ContactStreetName__c,true): record.ContactStreetType__c;
                recordNewValue.ContactUnitType__c= String.isBlank(record.ContactUnitType__c)? populateStreetTypeAndUnitType(record.ContactStreetName__c,false):record.ContactUnitType__c;
                recordNewValue.ContactPreDirection__c=String.isBlank(record.ContactPreDirection__c)?populatePreDirection(record.ContactStreetName__c):record.ContactPreDirection__c;
                removeElementsFromContactStreetName.add(recordNewValue.ContactStreetType__c);
                removeElementsFromContactStreetName.add(recordNewValue.ContactUnitType__c);
                removeElementsFromContactStreetName.add(recordNewValue.ContactPreDirection__c);
                recordNewValue.ContactStreetName__c = populateStreetName(record.ContactStreetName__c, removeElementsFromContactStreetName);
            }
            else if(String.isNotBlank(record.ContactStreetName__c) && !checkIsPoBox(record.ContactStreetName__c)){
            recordNewValue.ContactStreetType__c= String.isBlank(record.ContactStreetType__c) ? populateStreetTypeAndUnitType(record.ContactStreetName__c,true): record.ContactStreetType__c;
            recordNewValue.ContactStreetNumber__c = String.isBlank(record.ContactStreetNumber__c)? populateStreetNumberAndUnitNumber(record.ContactStreetName__c,true):record.ContactStreetNumber__c;
            recordNewValue.ContactUnitNumber__c = String.isBlank(record.ContactUnitNumber__c)? populateStreetNumberAndUnitNumber(record.ContactStreetName__c,false): record.ContactUnitNumber__c;
            recordNewValue.ContactUnitType__c= String.isBlank(record.ContactUnitType__c)? populateStreetTypeAndUnitType(record.ContactStreetName__c,false):record.ContactUnitType__c;
            recordNewValue.ContactPreDirection__c=String.isBlank(record.ContactPreDirection__c)?populatePreDirection(record.ContactStreetName__c):record.ContactPreDirection__c;
            // Add elements in List so it can be removed from Street Type
            removeElementsFromContactStreetName.add(recordNewValue.ContactStreetType__c);
            removeElementsFromContactStreetName.add(recordNewValue.ContactStreetNumber__c);
            removeElementsFromContactStreetName.add(recordNewValue.ContactUnitType__c);
            removeElementsFromContactStreetName.add(recordNewValue.ContactUnitNumber__c);
            removeElementsFromContactStreetName.add(recordNewValue.ContactPreDirection__c);
            recordNewValue.ContactStreetName__c = populateStreetName(record.ContactStreetName__c, removeElementsFromContactStreetName);
            }
        
            // Location Address contains no PObox
            if(String.isNotBlank(record.LocationStreetName__c) && !checkIsPoBox(record.LocationStreetName__c)){
                recordNewValue.LocationStreetType__c= String.isBlank(record.LocationStreetType__c) ? populateStreetTypeAndUnitType(record.LocationStreetName__c,true): record.LocationStreetType__c;
                if(String.isBlank(recordNewValue.LocationStreetType__c) && String.isNotBlank(record.LocationPostalCode__c) ){
                    // If Location Street is not available in Street name, using Post Code
                    recordNewValue.LocationStreetType__c = populateStreetTypeByPostCode(record.LocationPostalCode__c);
                }
                recordNewValue.LocationStreetNumber__c = String.isBlank(record.LocationStreetNumber__c)? populateStreetNumberAndUnitNumber(record.LocationStreetName__c,true):record.LocationStreetNumber__c;
                recordNewValue.LocationUnitNumber__c = String.isBlank(record.LocationUnitNumber__c)? populateStreetNumberAndUnitNumber(record.LocationStreetName__c,false): record.LocationUnitNumber__c;
                recordNewValue.LocationUnitType__c= String.isBlank(record.LocationUnitType__c)? populateStreetTypeAndUnitType(record.LocationStreetName__c,false):record.LocationUnitType__c;
                recordNewValue.LocationPreDirection__c=String.isBlank(record.LocationPreDirection__c)?populatePreDirection(record.LocationStreetName__c):record.LocationPreDirection__c;
                    // Add elements in List so it can be removed from Street Type
                removeElementsFromLocationStreetName.add(recordNewValue.LocationStreetType__c);
                removeElementsFromLocationStreetName.add(recordNewValue.LocationStreetNumber__c);
                removeElementsFromLocationStreetName.add(recordNewValue.LocationUnitType__c);
                removeElementsFromLocationStreetName.add(recordNewValue.LocationUnitNumber__c);
                removeElementsFromLocationStreetName.add(recordNewValue.LocationPreDirection__c);
                recordNewValue.LocationStreetName__c = populateStreetName(record.LocationStreetName__c, removeElementsFromLocationStreetName);
            }     

            // In case Contact Address is Empty and Location Address is Not
            if(String.isBlank(recordNewValue.ContactStreetName__c) && String.isNotBlank(recordNewValue.LocationStreetName__c)){
                recordNewValue.ContactStreetName__c = recordNewValue.LocationStreetName__c;
                recordNewValue.ContactStreetType__c = recordNewValue.LocationStreetType__c;
                recordNewValue.ContactStreetNumber__c = recordNewValue.LocationStreetNumber__c;
                recordNewValue.ContactUnitNumber__c = recordNewValue.LocationUnitNumber__c;
                recordNewValue.ContactUnitType__c = recordNewValue.LocationUnitType__c;
                recordNewValue.ContactPreDirection__c = recordNewValue.LocationPreDirection__c;
                recordNewValue.ContactPostalCode__c   = record.LocationPostalCode__c;
                recordNewValue.ContactCity__c = record.LocationCity__c; // City also need to be replaced
                recordNewValue.ContactState__c = record.LocationState__c; // State also need to be replaced 
            }else if(String.isBlank(recordNewValue.LocationStreetName__c) && recordNewValue.ContactCity__c == CCSFConstants.AUMENTUM_PROCESS.CITY_SF){
                recordNewValue.LocationStreetName__c = recordNewValue.ContactStreetName__c;
                recordNewValue.LocationStreetType__c = recordNewValue.ContactStreetType__c;
                recordNewValue.LocationStreetNumber__c = recordNewValue.ContactStreetNumber__c;
                recordNewValue.LocationUnitNumber__c = recordNewValue.ContactUnitNumber__c;
                recordNewValue.LocationUnitType__c = recordNewValue.ContactUnitType__c;
                recordNewValue.LocationPreDirection__c = recordNewValue.ContactPreDirection__c;
                recordNewValue.LocationPostalCode__c =  record.ContactPostalCode__c ;
                recordNewValue.LocationCity__c = record.ContactCity__c;
                recordNewValue.LocationState__c = record.ContactState__c;
            }
            // ASR-8390 TaxIdentificationNumber__c format XX-XXXXXXXX
            if(String.isNotBlank(record.TaxIdentificationNumber__c)){
                recordNewValue.TaxIdentificationNumber__c = getNewFormatTIN(record.TaxIdentificationNumber__c);
            }
            
            recordNewValue.IsDataClean__c = true;           
            recordsToUpdate.add(recordNewValue);
        }
    } catch(Exception e){
         Logger.addExceptionEntry(e, ORIGIN_LOCATION);        
    }
    Logger.saveLog();
    return recordsToUpdate;
        
    }
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String, Boolean
    // @description : This method takes String as Street name and Boolean and return Street Type or Unit Type based on Boolean value
    // @return : String
    //-----------------------------
    private static String populateStreetTypeAndUnitType(String recordStreetName,Boolean isStreet){
        String newValue='';      
        if(String.isBlank(recordStreetName)){ return newValue;}
        List<String> streetNames =recordStreetName.split(' ') ;       
        for(String valueTocheck:streetNames){
            if(isStreet){
                // looking for Street type
                if(STREET_TYPES.contains(valueTocheck)){
                    newValue= valueTocheck;                   
                    break;
                }
            }else{
                // looking for Unit type
                if(UNIT_TYPES.contains(valueTocheck)){
                    newValue= valueTocheck;
                   break;
                }
            }           
        }
        return newValue;
    }

    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String 
    // @description : This method takes String as Post Code and return Street Type Ave or Street 
    // @return : String
    //-----------------------------
    private static String populateStreetTypeByPostCode(String recordPostCode){
        String newValue='';       
        recordPostCode= recordPostCode.deleteWhitespace();
        if(recordPostCode.length() == 9){
            recordPostCode = recordPostCode.substring(0,5);
        }        
        String postCodeValueForStreet=CCSFConstants.AUMENTUM_PROCESS.POSTCODE_STREET_TYPE_STREET;
        String postCodeValueForAvenue=CCSFConstants.AUMENTUM_PROCESS.POSTCODE_STREET_TYPE_AVENUE;
        postCodeValueForStreet=postCodeValueForStreet.deleteWhitespace();
        postCodeValueForAvenue=postCodeValueForAvenue.deleteWhitespace();
        List<String> postCodeValuesForStreetTypeStreet = postCodeValueForStreet.split(',');
        List<String> postCodeValuesForStreetTypeAvenue=postCodeValueForAvenue.split(',');  
       
        if(postCodeValuesForStreetTypeStreet.contains(recordPostCode)){
            newValue=CCSFConstants.AUMENTUM_PROCESS.STREET_TYPE_STREET;         
        }else if(postCodeValuesForStreetTypeAvenue.contains(recordPostCode)){
            newValue=CCSFConstants.AUMENTUM_PROCESS.STREET_TYPE_AVENUE;           
        }         
         return newValue;
    }
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String, Boolean
    // @description : This method takes StreetName and return street number and unit number based on index 
    // @return : String
    //-----------------------------
    private static String populateStreetNumberAndUnitNumber(String recordStreetName,boolean isStreetNumber){
        String newValue='';      
        if(String.isBlank(recordStreetName)){ return newValue;}
        recordStreetName=recordStreetName.remove('#');
        List<String> streetNames =recordStreetName.split(' ') ;  
        for(Integer i=0; i<streetNames.size();i++){
            if(isStreetNumber && i == 0 && (streetNames[0].isNumeric() || streetNames[0].isNumericSpace())){
                // If Number is Street Number
                newValue=streetNames[0]; continue;                
            }else if(!isStreetNumber && i>0 && (streetNames[i].isNumeric() || streetNames[i].isNumericSpace()) ){    
                // If Number is Unit Number          
                newValue=streetNames[i]; continue;             
            }                    
        }               
        return newValue;
    }

    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String, Set<String>
    // @description : This method takes String as Full StreetName and List<String> elements like Street Number,Unit Type etc and return only Street name 
    // @return : String
    //-----------------------------

    private static String populateStreetName(String fullStreetName,Set<String> elementsToRemove){
        String newStreetName = fullStreetName;      
        if(fullStreetName.contains('#')){
            fullStreetName=fullStreetName.remove('#');
        }
        List<String> fullStreetNames = fullStreetName.split(' ');
        String newValue='';
        for(String valueToCheck: fullStreetNames){
            if(!elementsToRemove.contains(valueToCheck) ){
                newValue += valueToCheck +CCSFConstants.WHITE_SPACE;
            }
        }      
        return newValue;        
    }

     //-----------------------------
    // @author : Publicis Sapient 
    // @param : 
    // @description : This method String as StreetName and Set of String as directions
    // @return : String
    //-----------------------------

    private static String populatePreDirection(String fullStreetName){
        String newValue='';      
        if(String.isBlank(fullStreetName)){ return newValue;}
        List<String> streetNames =fullStreetName.split(' ') ;       
        for(String valueTocheck:streetNames){         
                // looking for directions 
                if(DIRECTIONS.contains(valueTocheck)){
                    newValue= valueTocheck;                   
                    break;
                }
        }
        return newValue;
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param :  String
    // @description : This method takes String and return true or false if Address contains PO box in it
    // @return : Boolean
    //-----------------------------

    private static Boolean checkIsPoBox(String recordAddress){
        Boolean isPOBox = false;       
        List<String> variationsOfPOBOX= CCSFConstants.AUMENTUM_PROCESS.PO_BOX_Variation.split(',');              
        for(String valueToCheck: variationsOfPOBOX){
            if(recordAddress.contains(valueToCheck) ) { isPOBox= true; break;}
        }
        return isPOBox;
    }

    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String TaxIdentificationNumber
    // @description : This method takes TaxIdentificationNumber and return it with correct format XX-XXXXXXXXX
    // @return : String
    // @ASR-8390
    //-----------------------------

    public static String getNewFormatTIN(String TaxIdentificationNumber){
        String newTaxIdentificationNumber;  
        // Adding '-' after 2 digit 
        newTaxIdentificationNumber = TaxIdentificationNumber.substring(0,2)+'-'+ TaxIdentificationNumber.substring(2);
        return newTaxIdentificationNumber;
    }
}