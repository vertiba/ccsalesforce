/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Trigger handler class for the Content Version object//
public without sharing class ContentVersionHandler extends TriggerHandler {

  

    protected override void executeAfterInsert(List<SObject> newRecords, Map<Id, SObject> newRecordsById) {       

        Profile taxPayerProfile  = Profiles.TAXPAYER;       
        // If the file is Uploaded from the community user then only the notification shoud be generated.
        if(UserInfo.getProfileId() == taxPayerProfile.id) {
       		List<ContentVersion> contentVersionsCreated = (List<ContentVersion>) newRecords;
                this.createFeedPost(contentVersionsCreated);              
    	}
    }
   
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<ContentVersion> newRecords
    // @description : This method is used to create Feed Posts for the New files Uploaded
    // @return :void
    private void createFeedPost(List<ContentVersion> newRecords) {

        Map<String,String> parentIdByContentVersionId = getContentVersionIdsWithParentIds(newRecords);
        if(!parentIdByContentVersionId.isempty()) {
            feedPostCreationForAssessments(parentIdByContentVersionId);
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<ContentVersion> newContentVersions
    // @description : This method is used to get the ParentIds wrt to the files uploaded.
    // @return :map<String,String>
    public map<String,String> getContentVersionIdsWithParentIds(List<ContentVersion> newContentVersions) {

        Set<Id> contentDocumentIdSet = new Set<Id>();
        map<String,String> contentVersionIdByContentDocumentId = new map<String,String>();
        map<String,String> assessmentIdByContentDocumentId = new map<String,String>();
        map<String,String> assessmentIdByContentVersionId = new map<String,String>();

        for(ContentVersion contentVersion : newContentVersions) {
            if(contentVersion.ContentDocumentId != null) {
                contentVersionIdByContentDocumentId.put(contentversion.ContentDocumentId,contentversion.Id);
            }
        }
        List<ContentDocumentLink> contentDocumentLinks =  new List<ContentDocumentLink>();
        for(ContentDocumentLink contentDocumentlink : [SELECT ContentDocumentId, LinkedEntityId,Visibility FROM ContentDocumentLink WHERE ContentDocumentId IN:contentVersionIdByContentDocumentId.keyset()]) {
            if(contentDocumentlink.LinkedEntityId.getsobjecttype() != User.sObjectType) {
                assessmentIdByContentDocumentId.put(contentDocumentlink.ContentDocumentId,contentDocumentlink.LinkedEntityId);
            }
        }

        for(String contentDocumentId : contentVersionIdByContentDocumentId.Keyset()) {
            assessmentIdByContentVersionId.put(contentVersionIdByContentDocumentId.get(contentDocumentId),assessmentIdByContentDocumentId.get(contentDocumentId));
        }

        return assessmentIdByContentVersionId;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Map<String,String> parentIdByContentVersionId
    // @description : This method is used to create the Chatter Post and its notifications for the files Uploaded by Taxpayers.
    // @return : void
    public void feedPostCreationForAssessments(Map<String,String> parentIdByContentVersionId) {
        Set<String> parentIds  = new Set<String>(parentIdByContentVersionId.values());

        List<Case> assessments = new List<Case>();
        List<ChatterUtility.ChatterPost> chatterPosts = new List<ChatterUtility.ChatterPost>();

        for(Case assessment :  [SELECT id,OwnerId,CaseNumber,RequestforInfoReceived__c,Status,SubStatus__c FROM Case WHERE Id IN :parentIds]) {
            ChatterUtility.ChatterPost chatterPost = new ChatterUtility.ChatterPost();
            chatterPost.message = label.DocumentUploadedMentionLabel + CCSFConstants.WHITE_SPACE + assessment.CaseNumber;
            chatterPost.subject = assessment.Id;
            chatterPost.userId  = assessment.OwnerId;

            chatterPosts.add(chatterPost);

            assessment.RequestforInfoReceived__c = System.today();
            if(assessment.Status == CCSFConstants.ASSESSMENT.STATUS_ONHOLD && assessment.SubStatus__c == CCSFConstants.ASSESSMENT.SUB_STATUS_WAITINGFORINFO) {
                assessment.Status       = CCSFConstants.ASSESSMENT.STATUS_INPROGRESS;
                assessment.SubStatus__c = CCSFConstants.ASSESSMENT.SUB_STATUS_PENDINGREVIEW;
            }
            assessments.add(assessment);
        }
        ChatterUtility.post(chatterPosts);
        if(!assessments.isEmpty()) {
            update assessments;
        }
    }
   
}