/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name : RegenerateAccountAccessPIN
 * Description: This batch will regenerate all the Access PINs for all the Business Accounts.
 * Story: ASR-6231; ASR-10252 
 * Task: ASR-10268 Commented Queue assignment for Statements. Logic moved to Trigger on Statements (StatementHandler class).
 */ 
public class RegenerateAccountAccessPIN implements Database.Batchable<sObject>, Database.stateful, BatchableErrorHandler{

    private Integer initialNumber;
        
    public RegenerateAccountAccessPIN(Integer initialNumber){
        this.initialNumber = initialNumber;
    }
    
    /*
     * Method Name: start
     * Description: This method fetches all Business Accounts in the System
     * Story: ASR- 6231 
     */ 
    public Database.QueryLocator start(Database.BatchableContext context) {
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        String query = 'Select Id, recordTypeId, AccessPIN__c from Account where BusinessStatus__c=\'Active\' and recordTypeId =:businessAccountRecordTypeId';
        Logger.addDebugEntry(query,'RegenerateAccountAccessPIN - start method query');
        Logger.saveLog(); 
        return Database.getQueryLocator(query);
    }
    
    /*
     * Method Name: execute
     * Description: For Each Record, we shuffle the order, and assign Random Access Pins.
     * Story: ASR- 6231 
     */ 
    public void execute(Database.BatchableContext context, List<Account> businessAccounts) {

        Map<String,Account> shuffledAccounts = new Map<String,Account>();
        List<AccountContactRelation> accountContactRelations = new List<AccountContactRelation>();
        for(Account accountInstance : businessAccounts){
            String randomNumber = String.ValueOf(Crypto.getRandomInteger());
            shuffledAccounts.put(randomNumber,accountInstance);
        }
        
        // Fetching all the accountContactRelations for the associated Accounts
        if(!shuffledAccounts.values().isEmpty()){
			accountContactRelations = [Select Id from accountContactRelation where AccountId IN:shuffledAccounts.values() limit 50000];            
        }
        
        // Updating the Access PIN's with Unique Numbers 
        List<Account> updatedAccounts = new List<Account>();
        if(!shuffledAccounts.values().isEmpty()){
            for(Account accountInstance : shuffledAccounts.values()){
                if(initialNumber >= CCSFConstants.ACCESSPIN_MAXCOUNT){ 
                    initialNumber = CCSFConstants.ACCESSPIN_MINCOUNT;
                }
                
                initialNumber++;
                accountInstance.AccessPIN__c = String.valueOf(initialNumber);
                updatedAccounts.add(accountInstance);
            }
        }
        
        // Deleting the accountContactRelations for the accounts
        if(!accountContactRelations.isEmpty()){
            database.delete(accountContactRelations);
        }
        
        // Updating the accounts with new access pin
        if(!updatedAccounts.isEmpty()){
            database.update(updatedAccounts);
        }
    }
    
    /*
     * Method Name: finish
     * Description: We insert a Batch Log to keep a track for Retrying purposes
     * Story: ASR- 6231 
     */ 
    public void finish(Database.BatchableContext context) {
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncJobs = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'initialNumber' => this.initialNumber});                                
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, RegenerateAccountAccessPIN.class.getName(), asyncJobs[0].ParentJobId, 
                                           true, queryParams, asyncJobs[0].NumberOfErrors);     
    }
    
    // Batch Retry Framework handleErrors function. 
    public void handleErrors(BatchableError error) {}
}