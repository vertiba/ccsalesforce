/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class DocumentGenerationRequestHandler extends TriggerHandler {

    protected override void executeBeforeInsert(List<SObject> newRecords) {
        List<DocumentGenerationRequest__c> newDocumentGenerationRequests = (List<DocumentGenerationRequest__c>)newRecords;

        this.setRelatedRecordFields(newDocumentGenerationRequests);
    }

    protected override void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {
        List<DocumentGenerationRequest__c> updatedDocumentGenerationRequests = (List<DocumentGenerationRequest__c>)updatedRecords;

        this.setRelatedRecordFields(updatedDocumentGenerationRequests);
    }

    private void setRelatedRecordFields(List<DocumentGenerationRequest__c> documentGenerationRequests) {
        DescribeSObjectResult sobjectDescribe = Schema.DocumentGenerationRequest__c.SObjectType.getDescribe();
        for(DocumentGenerationRequest__c request : documentGenerationRequests) {
            if(request.RelatedRecordId__c == null) continue;

            Id relatedRecordId = (Id)request.RelatedRecordId__c;
            request.RelatedRecordType__c = String.valueOf(relatedRecordId.getSObjectType());

            String lookupFieldName = request.RelatedRecordType__c.endsWith('__c')
                ? request.RelatedRecordType__c
                : request.RelatedRecordType__c + '__c';
            Schema.SObjectField lookupField = sobjectDescribe.fields.getMap().get(lookupFieldName);
            if(lookupField != null) {
                request.put(lookupField, relatedRecordId);
            }
        }
    }

}