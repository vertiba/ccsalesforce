/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test RollYearHandler
//-----------------------------
@isTest
private class RollYearHandlerTest {

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    static void setupData() {
        Test.setMock(HttpCalloutMock.class, new AnnualRollBatchSyncMockCallout());
        Integer currentFiscalYear = FiscalYearUtility.getCurrentFiscalYear();
        Integer lastFiscalYear    = currentFiscalYear - 1;

        List<RollYear__c> rollYears = new List<RollYear__c>();
        RollYear__c currentRollYear = TestDataUtility.buildRollYear(String.valueof(currentFiscalYear),String.valueof(currentFiscalYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYears.add(currentRollYear);
        RollYear__c lastRollYear = TestDataUtility.buildRollYear(String.valueof(lastFiscalYear),String.valueof(lastFiscalYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_Close_STATUS);
        rollYears.add(lastRollYear);
        insert rollYears;
        
        //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        
        //insert account
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        List<Property__c> properties = new List<Property__c>();
        Property__c property = TestDataUtility.getBPPProperty();
        property.Account__c = businessAccount.Id;
        properties.add(property);
        insert properties;
        
        Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME) Limit 1];
        List<case> cases = new List<case>();   
        Case assessment2  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                            currentRollYear.Id,currentRollYear.Name,currentRollYear.Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(currentFiscalYear, 1, 1));
        assessment2.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment2.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        assessment2.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
        assessment2.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
        assessment2.AccountId = property.account__c;
        assessment2.Property__c = property.Id;
        cases.add(assessment2);
        
        insert cases;
        
        //Update the current roll year to Close status to trigger RollYearHandler class
        currentRollYear.Status__c = CCSFConstants.ROLLYEAR.ROLL_YEAR_Close_STATUS;
        update currentRollYear;
    }

    @isTest
    private static void testStartEscapeAssessmentGenerator() {
        //TODO the test method. 
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method to verify the integration status
    //-----------------------------
    @isTest
    private static void testSendToIntegrationLayer() {
         for(Case updatedCase : [select id,IntegrationStatus__c,RollCode__c from case])
        {
            System.assertEquals(CCSFConstants.ASSESSMENT.IN_TRANSIT_TO_TTX, updatedCase.IntegrationStatus__c);
        }
    }

    @isTest
    private static void testUpdatePropertyAssessedValue() {

        String currentFiscalYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        RollYear__c rollYear = [SELECT Id, Status__c FROM RollYear__c WHERE Year__c = :currentFiscalYear];

        Test.startTest();
        rollYear.Status__c = 'Roll Closed';
        rollYear.IntegrationStatus__c = CCSFConstants.ROLLYEAR.SENT_TO_TTX;
        update rollYear;
        Test.stopTest();

        System.assertNotEquals(null, rollYear.IntegrationStatus__c);
    }
}