/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR-8203. This class is used to test DocumentPostProcessor Class
//-----------------------------
@isTest
public class DocumentPostProcessorTest {
    
    @isTest
    private static void getfieldsMappingValuesTest() {
        
        //Create Account
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        //Create ContentVersion
        ContentVersion contentVersion=new Contentversion();
        contentVersion.title='Notice to File 571-L (Account - With Exemptions)';
        contentVersion.DocumentType__c = 'BPP-Outgoing-Annual-Filing Notice';
        contentVersion.PathOnClient ='test.pdf';
        contentVersion.VersionData = Blob.valueOf('Test Content');
        contentVersion.IsMajorVersion = true;
        contentVersion.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert contentVersion;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink contentDocumentLink = New ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = businessAccount.id;
        contentDocumentLink.ContentDocumentId = documents[0].Id;
        contentDocumentLink.shareType = 'V';
        insert contentDocumentLink;
        
        Test.startTest();
        //Call updateVersion method
        new DocumentPostProcessor().updateVersion(businessAccount.Id);
        Test.stopTest();
        ContentVersion versions = [select Id, DocumentSetting__C, DocumentType__c, AIMSUploadStatus__c, Title from ContentVersion Limit 1];
        System.assertEquals('Notice to File 571-L (Account - With Exemptions)', versions.Title);
        System.assertEquals('BPP-Outgoing-Annual-Filing Notice', versions.DocumentType__c);
    }

}