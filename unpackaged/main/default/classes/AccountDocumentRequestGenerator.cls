/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class AccountDocumentRequestGenerator implements Database.Batchable<SObject>, BatchableErrorHandler {

    private static final String LOG_LOCATION = 'AccountDocumentRequestGenerator';
    private String query;
 
    /**
     * @author : Publicis.Sapient
     * @description : Constructor that takes query and executes in start method
     * @param : String Query to execute in start method
     */
    public AccountDocumentRequestGenerator(String query) {
        this.query = query;
    }

    /**
     * @author : Publicis.Sapient
     * @description : Standard method from batchable interface, that returns querylocator
     * @param : Database.BatchableContext
     * @return : Database.QueryLocator
     */
    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        Logger.addDebugEntry('starting to process query: ' + this.query, LOG_LOCATION);
        Logger.saveLog();
        return Database.getQueryLocator(this.query);
    }

    /**
     * @author : Publicis.Sapient
     * @description : In this method document requests are generated based on the notice type. 
     * @param :Database.BatchableContext  batchableContext
     * @param : List<Account> set of records that will be executed in this chunk
     * @return : void
     */
    public void execute(Database.BatchableContext batchableContext, List<Account> scope) {
        try {
            // Requery the records to ensure we have the necessary fields
            scope = [SELECT Id, Name, BusinessStatus__c, HasMailingAddress__c, RecordType.DeveloperName, RequiredToFile__c, NoticeType__c FROM Account WHERE Id IN :scope];
            //Fetch all the document settings related to account as a Map. With template name as key.
            Map<String, TH1__Document_Setting__c> documentSettingsByFormName = SmartCOMMUtility.getDocumentSettingsBySObjectName('Account');
            Logger.addDebugEntry(String.valueOf(documentSettingsByFormName), LOG_LOCATION);
            
            List<DocumentGenerationRequest__c> requestsToCreate = new List<DocumentGenerationRequest__c>();
            TH1__Document_Setting__c documentSetting;
            for(Account account : scope) {
                if(account.RecordType.DeveloperName != 'BusinessAccount' ||
                   account.HasMailingAddress__c == false || 
                   account.RequiredToFile__c == null || account.RequiredToFile__c == 'No' ||
                   account.BusinessStatus__c == 'Inactive') {
                       //for non business accounts, or inactive accounts, or accounts that dont have
                       // mailing address or not required to file, then dont generate notices. 
                       continue;
                   } else {
                       //resetting documentSetting field to null
                       documentSetting = null;
                       if(Account.NoticeType__c == 'Direct Bill') {
                           documentSetting = documentSettingsByFormName.get(Label.AccountDirectBillingNotice);
                       } else if(Account.NoticeType__c == 'Low Value') {
                           documentSetting = documentSettingsByFormName.get(Label.AccountLowValueExemptionNotice);
                       } else if(Account.NoticeType__c == 'Notice to File') {
                           documentSetting = documentSettingsByFormName.get(Label.AccountNoticeOfRequirementToFile571L);
                       } else if(Account.NoticeType__c == 'SDR') {
                           documentSetting = documentSettingsByFormName.get(Label.AccountSDRNotice);
                       }
                       if(documentSetting != null ){
                           //If documentSetting is not null, then create a document generation request record.
                           DocumentGenerationRequest__c request = SmartCOMMUtility.createDocumentGenerationRequest(account.Id, documentSetting.Id);
                           requestsToCreate.add(request);
                       }
                   }
            }
            //Insert all the lists for accounts for which documents will be generated.
            insert requestsToCreate;
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, LOG_LOCATION);
            throw ex;
        }
    }

    public void finish(Database.BatchableContext batchableContext) { 
        string JobId = batchableContext.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, AccountDocumentRequestGenerator.class.getName(), asyncList[0].ParentJobId, 
                                           false,'', asyncList[0].NumberOfErrors);

    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}

}