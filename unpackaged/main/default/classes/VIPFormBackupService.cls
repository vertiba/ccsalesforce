public without sharing class VIPFormBackupService implements Database.Batchable<SObject>, Database.AllowsCallouts {

    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return Database.getQueryLocator([
            SELECT Uuid__c, Name, RecordType.DeveloperName, VIPForm__Allow_Edit_After_Submit_Full_Form__c,
                VIPForm__Button_Delete_Exit_Exclude_by_User_Type__c, VIPForm__Button_PDF_Button_Exclude_User_Type__c,
                VIPForm__Button_Save_Exclude_by_User_Type__c, VIPForm__Button_Save_Exit_Exclude_by_User_Type__c,
                VIPForm__Card_Active__c, VIPForm__Conditional_Header_User__c, VIPForm__Create_New_Collection__c,
                VIPForm__Display_in_Lightning_Community__c, VIPForm__Form_Style__c, VIPForm__Form_Title__c,
                VIPForm__Hide_All_Top_Buttons__c, VIPForm__Hide_Logout_Icon__c, VIPForm__Hide_Section_Headers__c,
                VIPForm__Multilingual_Support__c, VIPForm__Object_API_Name__c, VIPForm__Object_Record_Type__c,
                VIPForm__Print_Option__c, VIPForm__Remove_Header__c, VIPForm__Return_URL_Community__c,
                VIPForm__Section_Header_Exclude_User_Type__c, VIPForm__Return_URL_Guest__c, VIPForm__Return_URL__c,
                VIPForm__Status__c, VIPForm__Template_Name_Variable__c, VIPForm__Template_Stage__c, VIPForm__Test_Record_ID__c,
                VIPForm__Top_Buttons_Exclude_User_Type__c, VIPForm__VIP_Form_Branding__c
            FROM VIPForm__VIP_Template__c
            ORDER BY Name
        ]);
    }

    public void execute(Database.BatchableContext batchableContext, List<VIPForm__VIP_Template__c> scope) {
        for(VIPForm__VIP_Template__c template : scope) {
            String contentType        = 'application/json';
            String staticResourceName = VIPFormTemplateDTO.STATIC_RESOURCE_PREFIX + template.Name.replaceAll('[^a-zA-Z0-9]', '');

            VIPFormTemplateDTO formTemplateDTO = new VIPFormTemplateDTO();
            formTemplateDTO.branding   = getBranding(template.Id);
            formTemplateDTO.categories = getCategories(template.Id);
            formTemplateDTO.template   = template;

            StaticResourceCreator.saveStaticResource(staticResourceName, Json.serializePretty(formTemplateDTO), contentType);
        }
    }

    public void finish(Database.BatchableContext batchableContext) {}

    private VIPForm__VIP_Form_Branding__c getBranding(Id templateId) {
        return [
            SELECT Uuid__c, Name, VIPForm__Accent_Hex_Color__c, VIPForm__Alternate_Stylesheet__c, VIPForm__External_ID__c,
            VIPForm__Footer__c, VIPForm__Header_Hex_Color__c, /* VIPForm__Javascript__c, */ VIPForm__Logo_Image__c, VIPForm__Logo_Title__c,
            VIPForm__Primary_Hex_Color__c, VIPForm__Print_CSS__c, VIPForm__Site_Template__c, VIPForm__Summary_PDF_Footer__c,
            VIPForm__Summary_PDF_Header__c
            FROM VIPForm__VIP_Form_Branding__c
            WHERE Id IN (SELECT VIPForm__VIP_Form_Branding__c FROM VIPForm__VIP_Template__c WHERE Id = :templateId)
            ORDER BY Name
        ];
    }

    private List<VIPForm__VIP_Category__c> getCategories(Id templateId) {
        return [
            SELECT Uuid__c, Name, RecordType.DeveloperName,
                VIPForm__Parent_VIP_Form_Section__r.Uuid__c, VIPForm__Template__r.Uuid__c, VIPForm__VIP_Template__r.Uuid__c,
                VIPForm__Active__c, VIPForm__Conditional_Field__c, VIPForm__Conditional_Operator__c,
                VIPForm__Conditional_Users_Exclude__c, VIPForm__Conditional_Value__c, VIPForm__External_Id__c,
                VIPForm__Icon_class__c, VIPForm__Inactive__c, VIPForm__Name_Displayed__c, VIPForm__Number_Conditional_Value__c,
                VIPForm__Object_API__c, VIPForm__Order__c, VIPForm__VIP_Date_Time_Submitted__c, VIPForm__VIP_Template_Status__c,
                (
                    SELECT Uuid__c, Name, RecordType.DeveloperName, VIPForm__Category__r.Uuid__c, VIPForm__Parent_Component__r.Uuid__c,
                        VIPForm__Add_attachments__c, VIPForm__Add_to_Summary_PDF__c, VIPForm__Allow_Edit_after_Submit__c,
                        VIPForm__Child_Object_API_Name__c, VIPForm__Conditional_Field__c, VIPForm__Conditional_Operator__c,
                        VIPForm__Conditional_Value__c, VIPForm__Count__c, VIPForm__Display_Help_Text__c,
                        VIPForm__Do_Not_Allow_Delete_after_Submit__c, VIPForm__External_Id__c, VIPForm__Fields_per_Row__c,
                        VIPForm__Field_Set_Labels__c, VIPForm__Field_Set_Name__c, VIPForm__Field_Set_Overrides__c,
                        VIPForm__Information_Box__c, VIPForm__Lookup_Field_API_on_Child__c,
                        VIPForm__Multi_Record_Type_Field_Set_Override__c, VIPForm__Narration_Text__c, VIPForm__Number_Conditional_Value__c,
                        VIPForm__Object_API_Name__c, VIPForm__Object_Name_for_Users__c, VIPForm__Order_By_Clause__c, VIPForm__Order__c,
                        VIPForm__Parent_Obj_API_Name__c, VIPForm__Read_Conditional_Field__c, VIPForm__Read_Conditional_Operator__c,
                        VIPForm__Read_Conditional_Value__c, VIPForm__Read_Only__c, VIPForm__Record_Detail_Fields_per_Row__c, VIPForm__Record_Detail_Field_Set__c,
                        VIPForm__Record_Type_API_Name__c, VIPForm__Record_Type_Name__c, VIPForm__Related_List_Actions__c, VIPForm__Related_List_Edit_Mode__c,
                        VIPForm__Related_List_Limit__c, VIPForm__Related_List_Mode__c, VIPForm__Related_List_Record_Title_Field__c, VIPForm__Related_List_Style__c,
                        VIPForm__Related_List_Window_Columns__c, VIPForm__Related_List_Window_Fieldset_Override__c, VIPForm__Related_List_Window_Fieldset__c,
                        VIPForm__Related_Object_Label__c, VIPForm__Relationship_Field__c, VIPForm__Required_Fieldset__c, VIPForm__Required__c,
                        VIPForm__Require_Attachment_on_related_list__c, VIPForm__Return_URL_Variable__c, VIPForm__Return_URL__c, VIPForm__Show_Print_Form_Button__c,
                        VIPForm__Show_Rich_Text_Area_and_Long_Text__c, VIPForm__Show_Submit_Button__c, VIPForm__Signature_Label__c,
                        VIPForm__Text_Box__c, VIPForm__Title_Displayed__c, VIPForm__Upload_Type__c, VIPForm__Visualforce_Page_Reference__c,
                        VIPForm__Where_Clause_Field__c, VIPForm__Where_Clause__c
                    FROM VIPForm__Components__r
                    ORDER BY Name
                )
            FROM VIPForm__VIP_Category__c
            WHERE VIPForm__Template__c = :templateId
            ORDER BY Name
        ];
    }

}