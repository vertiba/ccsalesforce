/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for ReproMailJsonParserController which is a Controller class for ReproMailJsonParser VF component
 * @Date: 01/30/2020
 * @Author: Srini Aluri(A1)
 
 * ModifiedBy         ModifiedDate   Description
 * Srini Aluri(A1)    01/31/2020     Initial development
*/

@isTest
public class ReproMailJsonParserControllerTest {
    
    // BUILDER METHODS
    static EmailLogDetail__c buildEmailLogDetail() {
        return new EmailLogDetail__c(            
            RecipientEmail__c = 'test_user@examplepublisissapient.com',
            Type__c = 'ReproMail Upload Notification', 
            JsonContent__c = '{"AccountNumber":"00154","EmailAddress":"a025C000002NDKn","PhoneNo":"LIST_VIEW","PrintInstructions":[{"Description":"Lorem","Envelope":"Epsum","Fold":"Nortex"},{"Description":"Lorem epsum","Envelope":"Epsum nortex","Fold":"Korono"}]}'
        );
    }
    
    static ReproMailJsonParserController.ReproMailNotificationInfo buildNotifInfoObj() {
        ReproMailJsonParserController.PrintInstruction printInfo = new ReproMailJsonParserController.PrintInstruction();
        printInfo.Description = 'test';        
        printInfo.Envelope = 'test';        
        printInfo.Fold = 'test';        
        printInfo.Ink = 'test';        
        printInfo.InternationalMail = 'test';        
        printInfo.MailingIncludeReturnEnvelope = 'test';        
        printInfo.NoOfPagesPerMail = 'test';        
        printInfo.NumberOfMailItems = 'test';        
        printInfo.Paper = 'test';        
        printInfo.PaperColor = 'test';        
        printInfo.PrintFileName = 'test';        
        printInfo.ProofRequired = 'test';        
        printInfo.ReturnEnvelopeType = 'test';        
        printInfo.Sides = 'test';        
        printInfo.SingleMail = 'test';        
        printInfo.Size = 'test';        
        printInfo.SpecialPrintInstruction = 'test';        
        printInfo.TotalNoOfPage = 'test';
            
        ReproMailJsonParserController.ReproMailNotificationInfo info = new ReproMailJsonParserController.ReproMailNotificationInfo();
        
        info.AccountNumber = 'AC100';
        info.Address = 'test';
        info.DateIn = 'test';
        info.Department = 'test';
        info.EmailAddress = 'test';
        info.PeopleSoftChartfields = 'test';
        info.PhoneNo = 'test';
        info.PrintInstructions = new List<ReproMailJsonParserController.PrintInstruction>{ printInfo };
            
        return info;
    }
    
    
    
    // TEST METHODS
    static testMethod void getReproMailNotificationTest() {
		Test.startTest();
        
        EmailLogDetail__c logDtObj = buildEmailLogDetail();
        INSERT logDtObj;
        
		ReproMailJsonParserController.ReproMailNotificationInfo infoObj = buildNotifInfoObj();
        ReproMailJsonParserController controller = new ReproMailJsonParserController();
        controller.emailLogDetailId = logDtObj.Id;        
        infoObj = controller.getReproMailNotification();
        
        Test.stopTest();
        
        // Assert the functionality - Make sure we got right object with desired values on AccountNumber
        // We should expect AccountNumber to be '00154' & not the 'AC100'
        System.assertEquals('00154', infoObj.AccountNumber);
    }
}