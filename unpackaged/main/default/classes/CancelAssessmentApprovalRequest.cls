/*
* Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
* Class Name:CancelAssessmentApprovalRequest
* Description: This class is called by the process builder 'Case Processor', when the case is closed and sub-status is 'Minimal/No Value Change'
* 				If there are approval processes, we remove them, by calling a Custom Email Service.
*/ 
public class CancelAssessmentApprovalRequest {
    
    /*
	* Method Name:RecallAssessmentApproval
	* Description: We are calling an email service in this method to remove the approval process in system context.
	*/ 
    @InvocableMethod(label='RemoveApproval')
    public static void RemoveAssessmentApproval(List<Id> assessmentIds){
        List<ProcessInstanceWorkitem> workItems = [Select Id From ProcessInstanceWorkitem
                                                   Where ProcessInstance.TargetObjectId = :assessmentIds ];
        
        if(!workItems.isEmpty()){
            Map<String, Object> emailParams = new Map<String, Object>();
            Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
            emailParams.put('workItemId', workItems[0].id);
            emailParams.put('caseId', assessmentIds[0]);
            List<EmailServicesAddress> emailServices = [SELECT Id, LocalPart, EmailDomainName FROM EmailServicesAddress
                                                        WHERE IsActive = true AND LocalPart ='approval_process_recall_service']; 
            
            if(!emailServices.isEmpty()){
                emailMessage.toAddresses = new List<String>{emailServices[0].LocalPart + '@' + emailServices[0].EmailDomainName};
                emailParams.put('errorMessage','No Error');
            }else{
                emailMessage.toAddresses = null;
				emailParams.put('errorMessage','No To address');                
            }
            
            emailMessage.subject = 'Approval Rejection';
            emailMessage.plaintextbody = JSON.serialize(emailParams);
            
            if(emailMessage.toAddresses != null){
            	Messaging.SendEmailResult[] emailResults = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailMessage});    
            }
            
        }
        
    }
    
}