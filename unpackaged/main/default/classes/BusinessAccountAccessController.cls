/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: BusinessAccountAccessController
 * Description: This class validates the business account and the access PIN. 
 * Task: ASR-10268 Statements are assigned to a Queue on creation (logic in StatementHandler class). Taxpayers get access
 * to their statements based on Sharing sets.
*/
public without sharing class BusinessAccountAccessController {

    @AuraEnabled
    public static String validateEntityIdAndPin(String entityId, String pin) {
        try {
            Account businessAccount = getBusinessAccount(entityId);
            String matches = validatePIN(businessAccount, pin);
            Logger.saveLog();
			return matches;
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, 'BusinessAccountAccessController.validateEntityIdAndPin');
            Logger.saveLog();
			throw ex;
        }
    }

    @AuraEnabled
    public static Account submitWithAccessPIN(String entityId, String accessPIN) {
        Logger.addDebugEntry('entityId=' + entityId, 'BusinessAccountAccessController.submitRequest');
        Logger.addDebugEntry('accessPIN=' + accessPIN, 'BusinessAccountAccessController.submitRequest');
		try {
            Account businessAccount = getBusinessAccount(entityId);
            Logger.addDebugEntry('businessAccount=' + businessAccount, 'BusinessAccountAccessController.submitRequest');
			createAccountContactRelation(businessAccount.Id);
			Logger.saveLog();
			return businessAccount;
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, 'BusinessAccountAccessController.submitRequest');
            Logger.saveLog();
			throw ex;
        }
    }

    private static String validatePIN(Account businessAccount, String pin) {
        if(businessAccount.AccessPIN__c == pin) {
            return 'Access Pin Matched';
        }
        /*else if(businessAccount.AgentPIN__c == pin) {
            return 'Agent Pin Matched';
        }*/
        else {
            throw new AuraHandledException('PIN Not Matched');
        }
    }

    private static Account getBusinessAccount(String entityId) {
        List<Account> results = [
            SELECT Id, Name, AccessPIN__c, AgentPIN__c
            FROM Account
            WHERE EntityId__c = :String.valueOf(entityId)
        ];
		if(results.size() != 1) {
            throw new AuraHandledException('No Bussiness Account found');
        }
		return results[0];
    }

    @TestVisible
    private static void createAccountContactRelation(Id businessAccountId) {
        Id userId = UserInfo.getUserId();
		User currentUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        
		if(currentUser.ContactId == null) return;

        List<AccountContactRelation> existingRecords = [SELECT Id FROM AccountContactRelation WHERE AccountId = :businessAccountId
                                                        AND ContactId = :currentUser.ContactId];
        
        Date accessEndDate = Date.newInstance(System.today().year() + 1, 1, 1);
        if(!existingRecords.isEmpty()) {
            existingRecords[0].EndDate = accessEndDate;
            update existingRecords;
        } else {
            AccountContactRelation acr = new AccountContactRelation(
                AccountId = businessAccountId,
                ContactId = currentUser.ContactId,
                EndDate   = accessEndDate,
                IsActive  = true,
                StartDate = System.today()
            );
            insert acr;
        }
    }

    /*@AuraEnabled
    public static Account saveTheFile(String entityId, String agentPIN, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Account businessAccount = getBusinessAccount(entityId);
        //Insert ContentVersion
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        cVersion.PathOnClient = fileName;//File name with extention
        cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
        cVersion.Title = fileName;//Name of the file
        cVersion.VersionData = EncodingUtil.base64Decode(base64Data);//File content
        Insert cVersion;
        //After saved the Content Verison, get the ContentDocumentId
        Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        //Insert ContentDocumentLink
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
        cDocLink.LinkedEntityId = businessAccount.Id;//Add attachment parentId
        cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cDocLink.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers
        Insert cDocLink;
        createAccountContactRelation(businessAccount.Id);
        return businessAccount;
  }*/
}