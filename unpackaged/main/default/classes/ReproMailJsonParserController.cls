/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class ReproMailJsonParserController {

    public Id emailLogDetailId {get; set;}

    public ReproMailNotificationInfo getReproMailNotification() {
        if(this.emailLogDetailId == null) return null;

        EmailLogDetail__c emailLogDetail = [SELECT Id, Type__c, JsonContent__c FROM EmailLogDetail__c WHERE Id = :emailLogDetailId];
	    return parse(emailLogDetail.JsonContent__c);
	}

    private ReproMailNotificationInfo parse(String jsonContent) {
        return (ReproMailNotificationInfo)Json.deserialize(jsonContent, ReproMailNotificationInfo.class);
    }

    public class ReproMailNotificationInfo {
        public String AccountNumber                     {get; set;}
        public String Address                           {get; set;}
        public String DateIn                            {get; set;}
        public String Department                        {get; set;}
        public String EmailAddress                      {get; set;}
        public String PeopleSoftChartfields             {get; set;}
        public String PhoneNo                           {get; set;}
        public List<PrintInstruction> PrintInstructions {get; set;}

        public ReproMailNotificationInfo() {
            this.PrintInstructions = new List<PrintInstruction>();
        }

    }

    public class PrintInstruction {
        public String Description                  {get; set;}
        public String Envelope                     {get; set;}
        public String Fold                         {get; set;}
        public String Ink                          {get; set;}
        public String InternationalMail            {get; set;}
        public String MailingIncludeReturnEnvelope {get; set;}
        public String NoOfPagesPerMail             {get; set;}
        public String NumberOfMailItems            {get; set;}
        public String Paper                        {get; set;}
        public String PaperColor                   {get; set;}
        public String PrintFileName                {get; set;}
        public String ProofRequired                {get; set;}
        public String ReturnEnvelopeType           {get; set;}
        public String Sides                        {get; set;}
        public String SingleMail                   {get; set;}
        public String Size                         {get; set;}
        public String SpecialPrintInstruction      {get; set;}
        public String TotalNoOfPage                {get; set;}
    }

}