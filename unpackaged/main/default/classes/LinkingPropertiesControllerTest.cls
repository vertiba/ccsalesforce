/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test LinkingPropertiesController
//-----------------------------
@isTest
private class LinkingPropertiesControllerTest {

    @testSetup
    private static void commonDataSetupForTesting(){
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        List<Account> accounts = new List<Account>();
        Account childAccount = new Account();
        childAccount.Name='Child Account';
        childAccount.BusinessStatus__c='active';
        childAccount.MailingCountry__C='US';
        childAccount.RecordTypeId = businessAccountRecordTypeId;
        accounts.add(childAccount);
        
        Account parentAccount = new Account();
        parentAccount.Name='Parent Account';
        parentAccount.BusinessStatus__c='Active';
        parentAccount.MailingCountry__C='US';
        parentAccount.RecordTypeId = businessAccountRecordTypeId;
        accounts.add(parentAccount);
        insert accounts;
        
        Id bppRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Business Personal Property').getRecordTypeId();
        List<Property__c> propertyList = new List<Property__c>();
        Property__c childProperty = new Property__c();
        childProperty.Name = 'child property'; 
        childProperty.MailingCountry__c = 'US';
        childProperty.Status__c = 'Active';
        childProperty.Account__c = accounts.get(0).Id;
        childProperty.LocationIdentificationNumber__c = 'LIN123567';
        childProperty.LegacyCompanyId__c = 'LC1234567';
        propertyList.add(childProperty);
        
        Property__c parentProperty = new Property__c();
        parentProperty.Name = 'test property'; 
        parentProperty.MailingCountry__c = 'US';
        parentProperty.Status__c = 'Active';
        parentProperty.Account__c = accounts.get(1).Id;
        propertyList.add(parentProperty);
        insert propertyList;
    }
    @isTest
    private static void doNotCloneChildAccountProperties(){
        Id parentAccountId = [select id from Account where Name = 'Parent Account' limit 1].id;
        Id childAccountId = [select id from Account where Name = 'Child Account' limit 1].id;
        Test.startTest();
        LinkingPropertiesController.updateParentIdWithFeinStatus(childAccountId, parentAccountId, 'Keep the current FEIN');
        List<Property__c> propertyList = [select id, Status__c from Property__c];
        System.assertEquals(2, propertyList.size());
        System.assertEquals('Active', propertyList.get(0).Status__c);
        System.assertEquals('Active', propertyList.get(1).Status__c);
        System.assertEquals('Active', [select BusinessStatus__c from Account where Name = 'Child Account' limit 1].BusinessStatus__c);
        Test.stopTest();
    }
    
    @isTest
    private static void cloneChildAccountProperties() {
        Id parentAccountId = [select id from Account where Name = 'Parent Account' limit 1].id;
        Id childAccountId = [select id from Account where Name = 'Child Account' limit 1].id;
        Test.startTest();
        LinkingPropertiesController.updateParentIdWithFeinStatus(childAccountId, parentAccountId, 'Use the parent account\'s FEIN');
        List<Property__c> propertyList = [select id, Status__c from Property__c order by Status__c];
        System.assertEquals(3, propertyList.size()); //One property cloned
        System.assertEquals('Active', propertyList.get(0).Status__c);
        System.assertEquals('Active', propertyList.get(1).Status__c);
        System.assertEquals('Inactive', propertyList.get(2).Status__c);
        System.assertEquals('Inactive', [select BusinessStatus__c from Account where Name = 'Child Account' limit 1].BusinessStatus__c);
        List<Property__c> clonedProperties = [select LegacyCompanyId__c, LocationIdentificationNumber__c, name from Property__c where Account__c=:parentAccountId];
        //Now the child/Cloned account refrences are removed
        for(Property__c clonedProperty:clonedProperties)
        {
            System.assertEquals(null, clonedProperty.LegacyCompanyId__c, 'Legacy Company Id Should be set to null');
        	System.assertEquals(null, clonedProperty.LocationIdentificationNumber__c, 'Legacy identification Number Should be set to null');
        }
        Test.stopTest();
    }
}