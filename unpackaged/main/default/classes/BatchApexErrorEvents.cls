/*
* Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
*  Class Name : BatchApexErrorEvents
* Description : Handles BatchApexErrorEvent events
*/
public class BatchApexErrorEvents {
    
    private List<BatchApexErrorEvent> events;
    
    public BatchApexErrorEvents(List<BatchApexErrorEvent> events) { 
        this.events = events;
    }
    
    public void handle() {
        
        // Query associated AsyncApexJobs
        Set<Id> asyncApexJobIds = new Set<Id>();
        for(BatchApexErrorEvent eventInstance : events){
            asyncApexJobIds.add(eventInstance.AsyncApexJobId);
        }
        system.debug('BatchApexErrorEvents asyncApexJobIds-->'+asyncApexJobIds);
        
        // Creating a Map of Async Ids, and BatchApexLog__c records
        Map<Id,BatchApexLog__c> batchApexMap = checkBatchApexContext(asyncApexJobIds);
        system.debug('batchApexMap-->'+batchApexMap);
        
        Map<Id, JobInfo> jobs = new RuntimeJobInfoResolver().resolveById(asyncApexJobIds);
        // Construct log records
        Map<Id, BatchApexErrorLog__c> errorLogsById = new Map<Id, BatchApexErrorLog__c>();
        Map<Id, List<BatchApexError__c>> errorsById = new Map<Id, List<BatchApexError__c>>();
        
        for(BatchApexErrorEvent eventInstance : events){
            Id jobId = eventInstance.AsyncApexJobId;
            JobInfo asyncApexJob = jobs.get(jobId);
            if(asyncApexJob!=null) {
                String apexClassName = asyncApexJob.ApexClassName;
                String jobScope = eventInstance.JobScope;
                // From retry job?
                if(apexClassName == BatchableRetryJob.class.getName()) {
                    // Adjust apex class and scope to reflect the orginal job not the retry job
                    BatchableError retryBatchableError = (BatchableError) JSON.deserialize(jobScope, BatchableError.class);
                    apexClassName = retryBatchableError.ApexClassName;
                    jobScope = retryBatchableError.JobScope;
                }
                
                // Create Logger Record
                Logger.addDebugEntry(eventInstance.Message, asyncApexJob.ApexClassName);
                Logger.saveLog();
                
                // Create Log?
                if(!errorLogsById.containsKey(jobId)) {
                    BatchApexErrorLog__c errorLog = new BatchApexErrorLog__c(JobId__c = jobId,JobApexClass__c = apexClassName,
                                                                             JobCreatedDate__c = asyncApexJob.CreatedDate);
                    errorLogsById.put(jobId, errorLog);
                    errorsById.put(jobId, new List<BatchApexError__c>());
                }
                
                // Add Error to Log
                errorsById.get(jobId).add(new BatchApexError__c(AsyncApexJobId__c = jobId,
                                                                DoesExceedJobScopeMaxLength__c = eventInstance.DoesExceedJobScopeMaxLength,
                                                                ExceptionType__c = eventInstance.ExceptionType,
                                                                JobApexClass__c = apexClassName,
                                                                JobScope__c = jobScope,
                                                                Message__c = eventInstance.Message,
                                                                RequestId__c = eventInstance.RequestId,
                                                                StackTrace__c = eventInstance.StackTrace));
                
            }
        }
        
        // Insert log records; We are upserting the external Key[JobId]
        upsert errorLogsById.values() JobId__c;
        
        List<BatchApexError__c> errors = new List<BatchApexError__c>();
        for(Id jobId : errorLogsById.keySet()) {
            BatchApexErrorLog__c log = errorLogsById.get(jobId);
            for(BatchApexError__c error : errorsById.get(jobId)) {
                error.BatchApexErrorLog__c = log.Id;
                errors.add(error);
            }        
        }
        database.insert(errors);
        
        // Allow the Batch Apex class a chance to handle the error
        for(BatchApexError__c error : errors) {    
            Type apexJobClassType = Type.forName(error.JobApexClass__c);
            system.debug('apexJobClassType-->'+apexJobClassType);
            Database.batchable<sObject> jobInstance;
            system.debug('BatcApexErrorEvents error.AsyncApexJobId-->'+error.AsyncApexJobId__c);
            system.debug('BatcApexErrorEvents batchApexMap.containsKey(error.AsyncApexJobId__c)-->'+
                         batchApexMap.containsKey(error.AsyncApexJobId__c));
            if(batchApexMap.containsKey(error.AsyncApexJobId__c)){
                // We are checking if the batch has params to be passed in their constructor. If yes, we pass it along to the batch
                if(batchApexMap.get(error.AsyncApexJobId__c).AreBatchParamsPresent__c == true){
                    Map<String, Object> variableMap = (Map<String, Object>)JSON.deserializeUntyped(
                        batchApexMap.get(error.AsyncApexJobId__c).QueryParams__c);
                    system.debug('variableMap-->'+variableMap);
                    jobInstance = (Database.Batchable<sObject>) JSON.deserialize(JSON.serialize(variableMap), apexJobClassType);
                    if(jobInstance instanceof BatchableErrorHandler) {
                        BatchableErrorHandler handler = (BatchableErrorHandler) jobInstance;
                        handler.handleErrors(BatchableError.newInstance(error));
                    }
                }else{
                    jobInstance = (Database.batchable<sObject>)apexJobClassType.newInstance();
                    if(jobInstance instanceof BatchableErrorHandler) {
                        BatchableErrorHandler handler = (BatchableErrorHandler) jobInstance;
                        handler.handleErrors(BatchableError.newInstance(error));
                    }
                }
            }else{
                system.debug('Check the context of the AsyncApexJobID. Is it the parentJobId or Record ID ');
            }
        }      
    }
    
    // Abstract queries made against AsyncApexJob to aid testability
    public abstract class JobInfoResolver {
        public abstract Map<Id, JobInfo> resolveById(Set<Id> jobIds);
    }
    
    public class RuntimeJobInfoResolver extends JobInfoResolver {
        public override Map<Id, JobInfo> resolveById(Set<Id> jobIds) {            
            Map<Id, JobInfo> jobsById = new Map<Id, JobInfo>();
            for(AsyncApexJob job : [select id, CreatedDate, ApexClass.Name from AsyncApexJob where Id IN :jobIds]) {
                JobInfo jobInfo = new JobInfo();
                jobInfo.Id = job.Id;
                jobInfo.ApexClassName = job.ApexClass.Name;
                jobInfo.CreatedDate = job.CreatedDate;
                jobsById.put(jobInfo.Id, jobInfo);
            }
            
            if(Test.isRunningTest()) {                
                JobInfo jobInfo = new JobInfo();
                jobInfo.Id = '707S000000nKE4fIAG';
                jobInfo.ApexClassName = BatchableErrorTestJob.class.getName();
                jobInfo.CreatedDate = System.today();
                jobsById.put(jobInfo.Id, jobInfo);
            }
            return jobsById;
        }
    }
    
    // Wrapper class
    public class JobInfo {
        public Id Id;
        public String ApexClassName;
        public DateTime CreatedDate;
    } 
    
    //
    // Method Name: checkBatchApexContext
    // Description: This method will create a Map of Async Ids, and BatchApexLog__c records.
    public static Map<Id,BatchApexLog__c> checkBatchApexContext(Set<Id> asyncApexJobIds){
        Map<Id,BatchApexLog__c> batchApexMap = new Map<Id,BatchApexLog__c>();
        Set<Id> batchIDs = new Set<Id>();
        Map<Id,AsyncApexJob> asyncParentIdMap = new Map<Id,AsyncApexJob>();
        Map<Id,AsyncApexJob> asyncJobIdMap = new Map<Id,AsyncApexJob>();
        List<BatchApexLog__c> batchApexList = new List<BatchApexLog__c>();
        
        // Create a Maps of AsyncjobParentId and asyncJobId so that, in the below function we can iterate the batchApexList.
        if(!asyncApexJobIds.isEmpty()){
            for(AsyncApexJob asyncInstance : [Select Id,ParentJobId from AsyncApexJob where
                                              (ParentJobId IN:asyncApexJobIds or Id IN:asyncApexJobIds) limit 50000]){
                                                  asyncParentIdMap.put(asyncInstance.ParentJobId, asyncInstance);
                                                  asyncJobIdMap.put(asyncInstance.Id, asyncInstance);
                                              } 
            
            system.debug('asyncParentIdMap-->'+asyncParentIdMap);
            system.debug('asyncJobIdMap-->'+asyncJobIdMap);
            
            //Check If record is found with AsyncId
            if(!asyncJobIdMap.isEmpty()){
                batchApexList = [SELECT Id, Name, JobChildId__c, JobId__c, AreBatchParamsPresent__c, 
                                 QueryParams__c, BatchClassName__c,AsyncParentJobId__c FROM BatchApexLog__c 
                                 WHERE (JobId__c IN:asyncJobIdMap.keySet() or AsyncParentJobId__c IN:asyncJobIdMap.keySet()) 
                                 LIMIT 50000];
            }
            
            system.debug('batchApexList with AsyncId-->'+batchApexList);
            //If no record is found with AsyncId, then requery with AsyncParentId
            if(!asyncParentIdMap.isEmpty() && batchApexList.isEmpty()){
                batchApexList = [SELECT Id, Name, JobChildId__c, JobId__c, AreBatchParamsPresent__c, 
                                 QueryParams__c, BatchClassName__c,AsyncParentJobId__c FROM BatchApexLog__c 
                                 WHERE (JobId__c IN:asyncParentIdMap.keySet() or AsyncParentJobId__c IN:asyncParentIdMap.keySet()) 
                                 LIMIT 50000];
            }
            
            system.debug('batchApexList with AsyncParentId-->'+batchApexList);
            // Create a Map of AsyncJobId, BatchApexLog Instance, so that, at the time of initialising the failed batch, we can pass params.
            if(!batchApexList.isEmpty()){
                for(BatchApexLog__c batchInstance : batchApexList){
                    if( !asyncParentIdMap.isEmpty() && asyncParentIdMap.containsKey(batchInstance.JobId__c) ){
                        batchApexMap.put(asyncParentIdMap.get(batchInstance.JobId__c).Id,batchInstance);
                    }else if( !asyncJobIdMap.isEmpty() && asyncJobIdMap.containsKey(batchInstance.JobId__c)){
                        batchApexMap.put(asyncJobIdMap.get(batchInstance.JobId__c).Id,batchInstance);
                    }
                }
            }
        }
        
        return batchApexMap;
    }
}