public with sharing class StaticResourceCreator {

    private static final String STATIC_RESOURCE_ENDPOINT = System.Url.getSalesforceBaseUrl().toExternalForm() + '/services/data/v47.0/sobjects/StaticResource';

    private class StaticResourceTemplate {
        private String Body         { get; set; }
        private String CacheControl { get; set; }
        private String ContentType  { get; set; }
        private String Description  { get; set; }
        private String Name         { get; set; }
    }

    public static void saveStaticResource(String staticResourceName, String content, String contentType) {
        // Get any existing static resources so we can update them
        Map<String, Id> existingStaticResourceIdsByName = new Map<String, Id>();
        for(StaticResource staticResource : [SELECT Id, Name FROM StaticResource WHERE Name = :staticResourceName]) {
            existingStaticResourceIdsByName.put(staticResource.Name, StaticResource.Id);
        }

        Blob requestBlob = Blob.valueOf(content);
        StaticResourceTemplate staticResourceTemplate = new StaticResourceTemplate();
        staticResourceTemplate.Body         = EncodingUtil.base64Encode(requestBlob);//finReq;//Json.serialize(scope);
        staticResourceTemplate.CacheControl = 'Private';
        staticResourceTemplate.ContentType  = contentType;
        staticResourceTemplate.Name         = staticResourceName;

        Id staticResourceId = existingStaticResourceIdsByName.get(staticResourceTemplate.Name);
        callStaticResourceEndpoint(staticResourceTemplate, staticResourceId);
    }

    private static HttpResponse callStaticResourceEndpoint(StaticResourceTemplate staticResourceTemplate, Id staticResourceId) {
        String calculatedEndpoint = STATIC_RESOURCE_ENDPOINT;
        if(staticResourceId != null)  calculatedEndpoint += '/' + staticResourceId + '?_HttpMethod=PATCH';
        System.debug('calculatedEndpoint=' + calculatedEndpoint);
        System.debug('staticResourceTemplate=' + staticResourceTemplate);
        System.debug('staticResourceTemplate.Body=' + staticResourceTemplate.Body);

        HttpRequest request = new HttpRequest();
        request.setEndpoint(calculatedEndpoint);
        request.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        request.setHeader('Content-Type', 'application/json');
        request.setBody(Json.serialize(staticResourceTemplate));
        request.setMethod('POST');

System.debug('request=' + request);

        // Execute the callout & verify the response
        HttpResponse response = new Http().send(request);
System.debug('response=' + response);
        System.assert(response.getStatusCode() < 300, 'Status code ' + response.getStatusCode() + ', Error: ' + response.getStatus());

        return response;
    }

}