//Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class AssessmentDataWrapper {
    @AuraEnabled public String Id;
    @AuraEnabled public Case caseRecord;
    @AuraEnabled public Case lastYearAssessmentRecord;
    @AuraEnabled public List<Case> escapeCloseouts;
    @AuraEnabled public List<Case> escapeRollCorrection;
    @AuraEnabled public List<Case> reissuedRecordRelatedParentAssessments; //ASR-9548
    @AuraEnabled public boolean isRollCorrectionToBeCreated; //ASR-8549
    @AuraEnabled public boolean isEscapeRollCorrectionToBeCreated; //ASR-8550
    @AuraEnabled public boolean isReissuedCase; //ASR-8550
    @AuraEnabled public string errorMsg; //ASR-8565, will use it to show specifc error to user
}