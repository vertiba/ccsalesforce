/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test LowValueAssessorScheduler
//-----------------------------
@isTest
public class LowValueAssessorSchedulerTest {
    @testSetup
    private static void dataSetup(){
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueOf(system.today().Year());
        rollYear.Year__c = String.valueOf(system.today().Year());
        rollYear.Status__c='Roll Open';
        rollYear.IsNonFilerBulkGeneratorProcessed__c = true;
        rollYear.IsVesselNonFilerGeneratorProcessed__c = true;
        insert rollYear;
        
        Account account = new Account();
        account.Name='test account';
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert account;
        
        Penalty__c penalty = new Penalty__c();
        penalty.PenaltyMaxAmount__c = 500;
        penalty.Percent__c = 10;
        penalty.RTCode__c = '463';
        insert penalty;
        
        Property__c property = new Property__C();
        property.Name = 'test property'; 
        property.Account__c = account.Id;
        insert property;
        
        FactorSource__c factorSource = new FactorSource__c();
        factorSource.Name = 'Commercial & Industrial (Untrended)';        
        insert factorSource;
        
        BusinessAssetMapping__c businessAssetMapping = new BusinessAssetMapping__c();
        businessAssetMapping.AssetClassification__c='Machinery & Equipment';
        businessAssetMapping.AssetSubclassification__c='Bakery (large)';
        businessAssetMapping.FactorSource__c = factorSource.id;
        businessAssetMapping.Subcomponent__c = '';
        businessAssetMapping.FixturesAllocationPercent__c = 75;
        businessAssetMapping.YearsOfUsefulLife__c = 15;
        businessAssetMapping.CompositeKey__c='Machinery & Equipment-Bakery (large)';
        businessAssetMapping.FactorType__c='Depreciation Factor';
        insert businessAssetMapping;
        
        Factor__c factor = new Factor__c();
        factor.FactorPercent__c=67;
        factor.AcquisitionYear__c='2018';
        factor.AssessmentYear__c='2020';
        factor.FactorSource__c=factorSource.Id;
        factor.RecordTypeId=Schema.SObjectType.Factor__c.getRecordTypeInfosByDeveloperName().get('DepreciationFactor').getRecordTypeId();
        insert factor;
    }
    //verify that a job is scheduled successfully in a future date. 
    @isTest
    private static void jobShouldBeScheduledToRunInFuture() {
        Test.startTest();
        String jobId = System.schedule('LowValueAssessorSchedulerTest',
                                       '0 0 0 15 3 ? 2032', 
                                       new LowValueAssessorScheduler()); 
        System.debug('jobId'+jobId);
        LowValueAssessorScheduler testMethod1 = new LowValueAssessorScheduler();
        testMethod1.getJobNamePrefix();
        Test.stopTest();
        
        system.assertEquals(1, [SELECT count() FROM CronTrigger where id = :jobId],
                            'A job should be scheduled');
    }
}