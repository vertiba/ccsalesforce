/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name : BatchableErrorsComponentController
 * Description: Controller queries for failed jobs and allows component to start retry jobs
 */
public class BatchableErrorsComponentController {

    // Fetch all failed jobs
    @AuraEnabled
    public static List<BatchApexErrorLog__c> failedJobs(){
        return [SELECT Id,JobApexClass__c, JobCreatedDate__c, JobErrors__c, JobId__c 
                FROM BatchApexErrorLog__c ORDER BY JobCreatedDate__c DESC LIMIT 5000];
    }

    // Execute Retry
    @AuraEnabled
    public static List<BatchApexErrorLog__c> retryJob(String retryJobId){
        BatchableRetryJob.run(retryJobId);      
        return failedJobs();
    }

    // Fetch Current Apex Jobs
    @AuraEnabled
    public static List<AsyncApexJob> fetchCurrentApexJobs(){
        return [SELECT Id, JobType, ApexClass.Name, Status, JobItemsProcessed, TotalJobItems,
                NumberOfErrors, CompletedDate, MethodName, ExtendedStatus, ParentJobId, 
                CreatedDate,CreatedBy.Name 
                FROM AsyncApexJob WHERE JobType !='ApexToken' and JobType !='BatchApexWorker'
                ORDER BY CreatedDate DESC LIMIT 500];
    }
    
}