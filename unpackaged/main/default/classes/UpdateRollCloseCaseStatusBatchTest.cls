/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test UpdateRollCloseCaseStatusBatch
//-----------------------------
@isTest
private class UpdateRollCloseCaseStatusBatchTest {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    private static void dataSetup(){
        TestDataUtility.disableAutomationCustomSettings(true);
        //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        
        //insert account
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        List<Property__c> properties = new List<Property__c>();
        Property__c property = TestDataUtility.getBPPProperty();
        property.Account__c = businessAccount.Id;
        properties.add(property);
        
        Property__c property2 = TestDataUtility.getBPPProperty();
        property2.Account__c = businessAccount.Id;
        property2.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
        properties.add(property2);
        insert properties;
        
        String currentYear 		= String.valueof(system.today().year());
        String nextYear 		= String.valueof(system.today().year()+1);
        Date filingDate = Date.newInstance(Integer.valueOf(currentYear), 1, 1);
        Integer currentFiscalYear 	= FiscalYearUtility.getCurrentFiscalYear(); 
        
        List<RollYear__c> rollYears = new List<RollYear__c>();
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = currentYear;
        rollYear.Year__c = currentYear;
        rollYear.Status__c='Roll Open';
        rollYears.add(rollYear);
        RollYear__c rollYear1 = new RollYear__c();
        rollYear1.Name = nextYear;
        rollYear1.Year__c = nextYear;
        rollYear1.Status__c='Roll Closed';
        rollYears.add(rollYear1);
        insert rollYears;
        
        Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME) Limit 1];
        List<case> cases = new List<case>();   
        Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                           rollYear.Id,rollYear.Name,rollYear.Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(currentFiscalYear, 1, 1));
        assessment.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        assessment.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_SECURED;
        assessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
        assessment.AccountId = property2.account__c;
        assessment.Property__c = property2.Id;
        assessment.Billable__c = CCSFConstants.ASSESSMENT.BILLABLE_NO;
        cases.add(assessment);
        
        Case assessment2  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                            rollYear.Id,rollYear.Name,rollYear.Name,
                                                            CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(currentFiscalYear, 1, 1));
        assessment2.Status = CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment2.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        assessment2.RollCode__c = CCSFConstants.ASSESSMENT.ROLLCODE_UNSECURED;
        assessment2.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
        assessment2.AccountId = property.account__c;
        assessment2.Property__c = property.Id;
        cases.add(assessment2);
        
        insert cases;
        TestDataUtility.disableAutomationCustomSettings(false);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to verify that the integration status is updating
    //----------------------------- 
    @isTest
    static void testIntegrationStatus() {
        String currentYear 		= String.valueof(system.today().year());
        Test.setMock(HttpCalloutMock.class, new AnnualRollBatchSyncMockCallout());
        Test.startTest();         
        RollYear__c rollYear = [SELECT  id,Year__c FROM RollYear__c where Year__c=:currentYear];
        UpdateRollCloseCaseStatusBatch  updateRollCloseCaseStatusBatch = new UpdateRollCloseCaseStatusBatch(rollYear);
        Database.executeBatch(updateRollCloseCaseStatusBatch);
        Test.stopTest();
        
        // Assert the functionality
        for(Case updatedCase : [select id,IntegrationStatus__c,RollCode__c,Billable__c from case])
        {
            if(updatedCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO){
                System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND, updatedCase.IntegrationStatus__c);
            }else if(updatedCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED){
                System.assertEquals(CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_RP, updatedCase.IntegrationStatus__c); 
            }
        }
    }
    
    @isTest
    static void errorFrameworkTest(){
        Case assessment = [Select id from Case limit 1];
        
        Id testJobId = '707S000000nKE4fIAG';
        BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
        testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testJob.JobCreatedDate__c = System.today();
        testJob.JobId__c = testJobId;
        database.insert(testJob);        
        
        BatchApexError__c testError = new BatchApexError__c();
        testError.AsyncApexJobId__c = testJobId;
        testError.BatchApexErrorLog__c = testJob.Id;
        testError.DoesExceedJobScopeMaxLength__c = false;
        testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
        testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testError.JobScope__c = assessment.Id;
        testError.Message__c = 'Test exception';
        testError.RequestId__c = null;
        testError.StackTrace__c = '';
        database.insert(testError);
        
        
        BatchableError batchError = BatchableError.newInstance(testError);
        ReOpenAnnualRollBatch batchRetry= new ReOpenAnnualRollBatch();
        batchRetry.handleErrors(batchError);
        system.assertEquals('707S000000nKE4fIAG', testError.AsyncApexJobId__c);
    }
}