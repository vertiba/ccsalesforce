/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class FiscalYearUtilsTest {

    @isTest
    static void it_should_return_the_fiscal_year_for_a_datetime() {
        String stage = [
            SELECT Id, MasterLabel
            FROM OpportunityStage
            WHERE IsActive = true
            LIMIT 1
        ].MasterLabel;

        Opportunity opportunity = new Opportunity(
            CloseDate = System.today(),
            Name      = 'Test opportunity',
            StageName = stage
        );
        insert opportunity;
        opportunity = [SELECT Id, CreatedDate FROM Opportunity];

        Test.startTest();

        AggregateResult soqlFY = [
            SELECT COUNT(Id) cnt, FISCAL_YEAR(CreatedDate) fy
            FROM Opportunity
            WHERE Id = :opportunity.Id
            GROUP BY FISCAL_YEAR(CreatedDate)
        ][0];
        
        FiscalYearUtility.getFiscalYear(opportunity.CreatedDate);
        System.assertEquals(soqlFY.get('fy'), FiscalYearUtility.getCurrentFiscalYear());
        //System.assertEquals(soqlFY.get('fy'), FiscalYearUtility.getFiscalYear(opportunity.CreatedDate));
        Test.stopTest();
    }

     @isTest
    static void getLastWorkingDayToFileStatementWithoutPenaltyTest() {
        String stage = [
            SELECT Id, MasterLabel
            FROM OpportunityStage
            WHERE IsActive = true
            LIMIT 1
        ].MasterLabel;

        Opportunity opportunity = new Opportunity(
            CloseDate = System.today(),
            Name      = 'Test opportunity',
            StageName = stage
        );
        insert opportunity;
        opportunity = [SELECT Id, CreatedDate FROM Opportunity];

        Test.startTest();

        AggregateResult soqlFY = [
            SELECT COUNT(Id) cnt, FISCAL_YEAR(CreatedDate) fy
            FROM Opportunity
            WHERE Id = :opportunity.Id
            GROUP BY FISCAL_YEAR(CreatedDate)
        ][0];
        Integer currentFiscalYear = FiscalYearUtility.getCurrentFiscalYear();
        FiscalYearUtility.getLastWorkingDayToFileStatementWithoutPenalty(String.ValueOf(currentFiscalYear));
        System.assertEquals(soqlFY.get('fy'), FiscalYearUtility.getCurrentFiscalYear());
        //System.assertEquals(soqlFY.get('fy'), FiscalYearUtility.getFiscalYear(opportunity.CreatedDate));
        Test.stopTest();
    }
     @isTest
    static void it_should_return_the_fiscal_year() {
        String stage = [
            SELECT Id, MasterLabel
            FROM OpportunityStage
            WHERE IsActive = true
            LIMIT 1
        ].MasterLabel;

        Opportunity opportunity = new Opportunity(
            CloseDate = System.today(),
            Name      = 'Test opportunity',
            StageName = stage
        );
        insert opportunity;
        opportunity = [SELECT Id, CreatedDate FROM Opportunity];

        Test.startTest();

        AggregateResult soqlFY = [
            SELECT COUNT(Id) cnt, FISCAL_YEAR(CreatedDate) fy
            FROM Opportunity
            WHERE Id = :opportunity.Id
            GROUP BY FISCAL_YEAR(CreatedDate)
        ][0];
		Integer currentFiscalYear = FiscalYearUtility.getCurrentFiscalYear();
		FiscalYearUtility.getFiscalYearSettings(String.ValueOf(currentFiscalYear));
         System.assertEquals(soqlFY.get('fy'), FiscalYearUtility.getCurrentFiscalYear());
        Test.stopTest();
    }
    
    @isTest
    static void it_should_return_the_fiscal_year_for_a_date() {
        String stage = [
            SELECT Id, MasterLabel
            FROM OpportunityStage
            WHERE IsActive = true
            LIMIT 1
        ].MasterLabel;

        Opportunity opportunity = new Opportunity(
            CloseDate = System.today(),
            Name      = 'Test opportunity',
            StageName = stage
        );
        insert opportunity;

        Test.startTest();

        AggregateResult soqlFY = [
            SELECT COUNT(Id) cnt, FISCAL_YEAR(CloseDate) fy
            FROM Opportunity
            WHERE Id = :opportunity.Id
            GROUP BY FISCAL_YEAR(CloseDate)
        ][0];

        System.assertEquals(soqlFY.get('fy'), FiscalYearUtility.getCurrentFiscalYear());
        System.assertEquals(soqlFY.get('fy'), FiscalYearUtility.getFiscalYear(opportunity.CloseDate));

        Test.stopTest();
    }

    @isTest
    static void it_should_throw_error_for_null_datetime() {
        try {
            Datetime dt = null;
            FiscalYearUtility.getFiscalYear(dt);
            throw new ExpectedException('Exception expected');
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('Datetime is required'), ex.getMessage());
        }
    }

    @isTest
    static void it_should_throw_error_for_null_date() {
        try {
            Date d = null;
            FiscalYearUtility.getFiscalYear(d);
            throw new ExpectedException('Exception expected');
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('Date is required'), ex.getMessage());
        }
    }
    /* ---------------------------------------------
    // @author : Publicis.Sapient
    // @description : for Escape CLoseout Assessment year from Case Handler
    ------------------------------------------------*/
    @isTest
    static void getCloseoutRollYearTest() {
        Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
        Integer lastYear 			= currentYear-1;
        //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
       //insert property
        Property__c property1 = TestDataUtility.getBPPProperty();
        property1.Account__c = businessAccount.Id;
        property1.Status__c= CCSFConstants.STATUS_ACTIVE;
        insert property1;
        
        //Roll Year for Current year
        String fiscalYear = String.valueof(currentYear);
        Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear), 01, 01);
        //Roll Year for Last year
        String prevfiscalYear = String.valueof(lastYear);
        
        RollYear__c prevRollYear = new RollYear__c();
        prevRollYear.Name = prevfiscalYear;
        prevRollYear.Year__c = prevfiscalYear;
        prevRollYear.Status__c='Roll Closed';
        insert prevRollYear;
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = fiscalYear;
        rollYear.Year__c = fiscalYear;
        rollYear.Status__c='Roll Open';
        insert rollYear;
        Date eventDate1 = Date.newInstance(integer.valueof(prevRollYear.Year__c), 01, 01);
        
        Case priorYearAssessment = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property1.Id,null,null,
                                                                   null,'Escape',48, 'Notice to File',null,eventDate1);
        priorYearAssessment.SubType__c = 'Closeout';
        priorYearAssessment.AdjustmentType__c = 'New';
        insert priorYearAssessment;
        Case validRollYear = [Select Id,Roll__c FROM Case Where id =: priorYearAssessment.id LIMIT 1];
        System.assertEquals(prevRollYear.id, validRollYear.Roll__c);
    }

    private class ExpectedException extends Exception {}

}