/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      When a record has to be updated for certain criteria and that criteria can be 
//      identified only in after trigger context. In future this can be used for any async 
//      operation. 
//-----------------------------
public class QueuableDMLOperations implements Queueable, Callable {
    private List<sObject> queuedChildRecords;
    private String operationType;
    
    /**
     * Constructor taking in records on which action needs to be performed 
     * and the action to which action needs to be done.
     */
    public QueuableDMLOperations(List<sObject> recordsTobeUpdated, String dmlOperation){
        queuedChildRecords = recordsTobeUpdated;
        operationType = dmlOperation;
    }	
    /**
     * @param : queuablecontext
     * @description : method will be called when the job is enqueued. 
     * @return : void
     */
    public void execute(QueueableContext queueableContext) {
        if(queuedChildRecords!=null && !queuedChildRecords.isEmpty()){
            List<Database.SaveResult> dmlResults = (List<Database.SaveResult>)call(operationType,new Map<String,Object>());
            ErrorChatterPostNotifications.createFailedDMLChatterNotification(dmlResults,queuedChildRecords);
        }
    }
    /**
     * @param : string - action to perform, so that can be used for multiple places.
     * @param : Map<String, Object> - Dummy, as this is required by the interface.
     * @description: method from callable interface. does the specific database actions as required
     * @return : returns the database result. 
     */
    public Object call(String action, Map<String, Object> args) {
        switch on action {
            when 'Insert' {
                return Database.insert(queuedChildRecords,false);
            } when 'Update' {
                return Database.update(queuedChildRecords,false);
            } when 'Delete' {
                return Database.delete(queuedChildRecords,false);
            } when else {
                return null;
            } 
        }
    }
}