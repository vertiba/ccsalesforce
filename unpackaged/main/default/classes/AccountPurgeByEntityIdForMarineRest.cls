/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * Class Name: AccountPurgeByEntityIdForMarineRest
 * Description: This class purges Accounts by Entity Id on Marine Properties 
 * Author : Publicis Sapient 
 */
@RestResource(urlMapping='/AccountsPurgeForMarineByEntity/*')
global class AccountPurgeByEntityIdForMarineRest {

    @HttpPost
    global Static Map<String, String> postMethod(){
        
        String requestBody = RestContext.request.requestBody.toString();   
        Map<String, String> deletedObjectNameByCount = new Map<String, String>();
        LegacyCompanyWrapper legacyCompanyIds = (LegacyCompanyWrapper)System.JSON.deserialize(requestBody, LegacyCompanyWrapper.class);
        system.debug('legacyCompany Id'+ legacyCompanyIds);
        if(System.Label.AllowDataPurging == 'True'){
            try{
                DataPurgeForMarine accountPurge = new DataPurgeForMarine();
                deletedObjectNameByCount = accountPurge.deleteAccountsByCompanyId(legacyCompanyIds.entityIds);
                if(Test.isRunningTest()){
                    // Manually invoking an error for covering the exception test scenario 
                    // if the above callout does not fail by itself.
                    Integer count = 10/0;
                }
            }catch(exception e){
                deletedObjectNameByCount.put('Error', e.getMessage());
                system.debug('error message'+ deletedObjectNameByCount);
            }
        }else{
            deletedObjectNameByCount.put('Error', system.label.DataPurgingError);
        }
        
        return deletedObjectNameByCount;
    }
    
    public class LegacyCompanyWrapper {
        List<String> entityIds;
        public LegacyCompanyWrapper(){
            
        }
    }
}