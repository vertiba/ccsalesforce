/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Class is to do any custom processing once smartcomm
// 			generates the document.
//-----------------------------
global class DocumentPostProcessor implements TH1.GLOBAL_IF_PostGenerationProcessor {
	//-----------------------------
    // @author : Publicis.Sapient
    // @param : Id Related Record id, to which the document is attached.
    // @param : TH1.GLOBAL_VO_DocumentPackage, managed object that contains information 
    // 			about the document that is generated. 
    // @description : interface method, to do any callouts or queueables.
    // @return : void
    //-----------------------------
    public void beforeDMLProcess(Id relatedRecordId, TH1.GLOBAL_VO_DocumentPackage documentPackage) {
    }
	//-----------------------------
    // @author : Publicis.Sapient
    // @param : Id Related Record id, to which the document is attached.
    // @param : TH1.GLOBAL_VO_DocumentPackage, managed object that contains information 
    // 			about the document that is generated. 
    // @description : interface method, to do any database operations, after the documents are created.
    // @return : void
    //-----------------------------
    public void afterDMLProcess(Id relatedRecordId, TH1.GLOBAL_VO_DocumentPackage documentPackage) {
        //Calling seperate method for unit test coverage as in same method there are some limits as Managed package
        //Object is not available. 
        updateVersion(relatedRecordId);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Id Related Record id, to which the document is attached.
    // @description : This method is created as a seperate method for unit test coverage as in same method there are some
    // limits as Managed package Object is not available.
    // @return : void
    //-----------------------------
    @TestVisible
    private void updateVersion(Id relatedRecordId){
        Set<Id> documentIds = new Set<Id>();
        Set<ContentVersion> versionsToUpdate = new Set<ContentVersion>();
        List<ContentVersion> uniqueVersionsToUpdate = new List<ContentVersion>();
        Set<String> documentSettingNames = new Set<String>();
        Map<String, Id> documentSettingIdsByFilename = new Map<String, Id>();
        DateTime currentTime = System.NOW().addMinutes(-2);
        //There is a scenario where an envelope can be created with 3 different documents. Not sure for each document 
        //this mehtod will be called once or will be called once for all the 3 generated documents. Also, we want only
        //current documents so, added time condition as well. 
        for(ContentDocumentLink documentLink : [SELECT Id, ContentDocumentId, ContentDocument.Title FROM ContentDocumentLink where LinkedEntityId = :relatedRecordId and ContentDocument.createdDate >:currentTime]){
            documentIds.add(documentLink.ContentDocumentId);
            documentSettingNames.add(documentLink.ContentDocument.Title);
        }
        for(TH1__Document_Setting__c documentSetting : [select Id, TH1__Storage_file_name__c from TH1__Document_Setting__c where TH1__Storage_file_name__c in :documentSettingNames and TH1__Thunderhead_channel_name__c = 'Print']){
            documentSettingIdsByFilename.put(documentSetting.TH1__Storage_file_name__c, documentSetting.Id);
        }
        
        List<ContentVersion> versions = [select Id, DocumentSetting__C, DocumentType__c, AIMSUploadStatus__c, Title from ContentVersion where contentDocumentId in :documentIds and FileExtension = 'pdf'];
        if(versions.isEmpty()) return;
        Logger.addDebugEntry('List of ContentVersion retrived', 'DocumentPostProcessor afterDMLProcess');
        //fetch all the values of MappingOfNoticeToDocumentType custom metadata type and store it in a map
        Map<String,String> templateNamesByDocumentType = getfieldsMappingValues();
        for(ContentVersion version:versions){
            if(version.DocumentSetting__C==null){
                version.DocumentSetting__C=documentSettingIdsByFilename.get(version.Title);
                versionsToUpdate.add(version);
            }
            //ASR-8203 - Check if the title is inside the map
            if(templateNamesByDocumentType.containsKey(version.Title)){
                //Check remove the version if it already added to avoid duplicates
                if(versionsToUpdate.contains(version)){
                	versionsToUpdate.remove(version);
                    //Adding Document Setting again as the version is deleted
                    version.DocumentSetting__C=documentSettingIdsByFilename.get(version.Title);
                }
                //Set document type and AIMSUploadStatus__c
            	version.DocumentType__c = templateNamesByDocumentType.get(version.Title);
                version.AIMSUploadStatus__c = CCSFConstants.ONBASE_CONTENTVERSION.AIMS_UPLOAD_STATUS_READY_FOR_UPLOAD;
                versionsToUpdate.add(version);
        	}
        }
        //Check if there are some records to update in versionsToUpdate Set
        if(versionsToUpdate.isEmpty()) return;
        //Add all the Set values to List as DML operations can't be done on Set
        uniqueVersionsToUpdate.addAll(versionsToUpdate);
        update uniqueVersionsToUpdate;
        Logger.saveLog();
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : No Parameter
    // @description : ASR-8203 - Fetch all the values of MappingOfNoticeToDocumentType custom metadata type and store in a map to return
    // @return : Map<String,String>
    //-----------------------------
    private Map<String,String> getfieldsMappingValues(){
        List<MappingOfNoticeToDocumentType__mdt> fieldsMapping = [SELECT MasterLabel, QualifiedApiName, DeveloperName,DocumentType__c,TemplateName__c FROM MappingOfNoticeToDocumentType__mdt];
        Map<String,String> templateNamesByDocumentType = new Map<String,String>();
        for(MappingOfNoticeToDocumentType__mdt record: fieldsMapping){
            String templateNames = record.TemplateName__c;
            if(templateNames != null && templateNames !=''){
                //split the value of template names with comma delimiter
                List<String> names = templateNames.split(',');
                for(String name : names){
                    templateNamesByDocumentType.put(name,record.DocumentType__c);
                }
            }
        }
        return templateNamesByDocumentType;
    }
}