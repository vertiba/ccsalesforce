@isTest
private class VIPTemplateTest {

    @isTest
    static void it_should_set_uuid() {
        VIPForm__VIP_Template__c template = new VIPForm__VIP_Template__c(
            Name                           = 'test',
            VIPForm__Object_Record_Type__c = 'Account'
        );
        insert template;

        template = [SELECT Id, Name, Uuid__c FROM VIPForm__VIP_Template__c WHERE Id = :template.Id];
        System.assert(Uuid.isValid(template.Uuid__c));
    }

}