/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
    // @author : Publicis.Sapient
    // @description : Test class for waiveNoticePeriodController class
//-----------------------------

@isTest
private class waiveNoticePeriodControllerTest {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to create test data
    //-----------------------------
    @testSetup
    private static void commanData(){
        
        //account creation
        Account account = new Account();
        account.Name='test account';
        account.BusinessStatus__c='active';
        account.MailingCountry__C='US';
        insert account;
        
        //penalty creation
        List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty1 = new Penalty__c();
        penalty1.PenaltyMaxAmount__c = 500;
        penalty1.Percent__c = 10;
        penalty1.RTCode__c = '463';
        penalties.add(penalty1);
        
        Penalty__c penalty2 = new Penalty__c();
        penalty2.PenaltyMaxAmount__c = 500;
        penalty2.Percent__c = 25;
        penalty2.RTCode__c = '214.13';
        penalties.add(penalty2);
        insert penalties;
        
        Date filingDate = System.today();
        
        //property creation
        Property__C property = new Property__C();
        property.Name = 'test property'; 
        property.MailingCountry__c = 'US';
        property.Account__c = account.Id;
        insert property;
        
        //rollYear creation
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Status__c = 'Roll Open';
        insert rollYear;
        
        RollYear__c closedRollYear = new RollYear__c();
        closedRollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        closedRollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        closedRollYear.Status__c = 'Roll Closed';
        insert closedRollYear;
        
        //Case creation
        Case cas = new Case();
        cas.AccountId = property.account__c;
        cas.Property__C = property.id;
        cas.MailingCountry__c = 'US';
        cas.EventDate__c = Date.newInstance(2020, 1, 1);
        cas.Type='Escape';
        cas.Roll__c = closedRollYear.Id;
        insert cas;
        
        Case cs = new Case();
        cs.AccountId = property.account__c;
        cs.Property__C = property.id;
        cs.MailingCountry__c = 'US';
        cs.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        cs.Type='Regular';
        cs.AdjustmentType__c = 'New';
        cs.Roll__c = rollYear.Id;
        insert cs; 
    }
    
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to test getWaiverValue
    //-----------------------------
    @isTest
    private static void getWaiverValueTest(){
        Case cas = [Select Id,IntegrationStatus__c,Type,DeclarationName__c from Case where Type = 'Escape'];       
        Test.startTest();
        Case waiverValue = waiveNoticePeriodController.getWaiverValue(cas.Id);
        System.assertEquals(waiverValue.Id, cas.Id);
        Test.stopTest();    
    }    
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to test updateWaiverNoticeDetails
    //-----------------------------
    @isTest
    private static void setValueTest(){
        Case cas = [Select Id,IntegrationStatus__c,Type,DeclarationName__c,WaiveNoticePeriod__c,isLocked__c from Case where Type = 'Escape']; 
   
        Test.startTest();
        Case getValue = waiveNoticePeriodController.updateWaiverNoticeDetails(cas.DeclarationName__c , cas.Id);
        System.assertEquals(true,getValue.WaiveNoticePeriod__c);
        Test.stopTest();    
    }
	
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to test exception in updateWaiverNoticeDetails
    //-----------------------------
	@isTest
    private static void testException(){
        Case cas = [Select Id,IntegrationStatus__c,Type,DeclarationName__c,WaiveNoticePeriod__c,isLocked__c from Case where Type='Regular'];
        
        RollYear__c rollYear = [Select Id,Status__c from RollYear__c where Status__c = 'Roll Open'];
		rollYear.Status__c = 'Roll Closed';
		update rollYear;
       
        Test.startTest();
        //
        try{
            Case getValue = waiveNoticePeriodController.updateWaiverNoticeDetails(cas.DeclarationName__c , cas.Id);
            System.assert(false,'Exception should be thrown');
        }catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Script-thrown exception') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        Test.stopTest();
    }    
}