/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for FieldSetUtility
 * @Date: 01/28/2020
 * @Author: Srini Aluri(A1)
 
 * ModifiedBy         ModifiedDate   Description
 * Srini Aluri(A1)    01/31/2020     Initial development
*/

/*
 Pre-Deployment Instructions:
 1 - Please make sure the fieldset Track_Repro_Delivery of ContentVersion object
     is deployed before deploying this class to target org.
*/


@isTest
public class FieldSetUtilTest {

    static testMethod void getFieldSetFieldsTest() {
        List<FieldSetUtility.FieldSetMember> fieldSetMembers;
            
        Test.startTest();
        
        fieldSetMembers = FieldSetUtility.getFieldSetFields('ContentVersion', 'Track_Repro_Delivery');
        
        Test.stopTest();
        
        // Assert the functionality - We should get fields(more than 1) from above Track_Repro_Delivery FieldSet
        System.assertEquals(true, fieldSetMembers.size() > 0);
    }
}