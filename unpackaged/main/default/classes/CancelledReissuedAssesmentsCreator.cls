/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : 

public class CancelledReissuedAssesmentsCreator {
    
    public final String CASE_QUERY;    
    public Boolean calledFromOnlyCancel{get;set;}
    public string recordStatus {get;set;} 
    public string baseURL {get;set;}
    Map<Id,Id> recordTypeId = new Map<Id,Id>();
    List<Case> cas = new List<Case>();
    Case currentCase = new Case();
    
    private String originLocation;
    
    public CancelledReissuedAssesmentsCreator(ApexPages.StandardController standardController){
        CASE_QUERY = DescribeUtility.buildQueryToFetchAllDetailsOfParentandChild(Case.sObjectType, AssessmentLineItem__c.sObjectType);       
        currentCase = (Case) standardController.getRecord();
    }
    //-----------------------------.
    // @author : Publicis.Sapient
    // @description : This method is redirecting to VF page CancelledAssessments and CancelledReissuedAssessments
    // @return : void
    //-----------------------------
    
    public void redirect(){
        String MESSAGE;
        Boolean callCancel;
        if(calledFromOnlyCancel == true){
            // Getting passed from VF page CancelAssessment as true
            // It means called by clicking Button Cancel Assessment
            // Custom persmission
            originLocation = CCSFConstants.ASSESSMENT.CUSTOM_PERMISSION_CANCEL;        
            MESSAGE = System.Label.CancelAssessmentNotAllowed;
            callCancel = true;
        }else{
            // Getting passed from VF page CancelReissuedAssessment as true
            // It means called by clicking Button Cancel and Reissued Assessment
            // Custom persmission
            originLocation = CCSFConstants.ASSESSMENT.CUSTOM_PERMISSION_CANCEL_REISSUE;
            MESSAGE = System.Label.CancelReisueNotAllowed;
            callCancel = false;
        }
        
        //Check whether user has access to Custom permission to perform the action or not
        Boolean hasPermission = FeatureManagement.checkPermission(originLocation);       
        if(!hasPermission) { 
            // Custom permission     
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + currentCase.Id;
            recordStatus = System.Label.NoPermission;       
            return;
        }
        
       
        String recordId = currentCase.id;
        //Added as part of ASR-9360 start
        List<String> splitedQueryStings = CASE_QUERY.split('FROM Case');
        String query = splitedQueryStings[0] + ',Parent.CancelAttempted__c';        
        query += ' from Case WHERE Id =: recordId';
        //Added as part of ASR-9360 End
        cas = new List<Case>((List<Case>)Database.query(query));
               
        // ASR -8548 - mentioned in Solution to update ASR-3482   
        // Bug- 9751 Check if Assessment is been already cancelled 
        AccessWithMessageWrapper accessAndMessageWrap=   hasAccessToPerformAction(cas[0],MESSAGE) ;
        if(!accessAndMessageWrap.access ){           
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + cas[0].Id;
            recordStatus = accessAndMessageWrap.message ;
            return;
        }
    
        if(cas[0].RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_LABEL_NAME).getRecordTypeId()){
            recordTypeId.put(cas[0].RecordTypeId,Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.BPP_RECORD_LABEL_NAME).getRecordTypeId());
            
        }else if(cas[0].RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_LABEL_NAME).getRecordTypeId()){
            recordTypeId.put(cas[0].RecordTypeId, Schema.SObjectType.Case.getRecordTypeInfosByName().get(CCSFConstants.ASSESSMENT.VESSEL_RECORD_LABEL_NAME).getRecordTypeId());
        }
        
        if(callCancel){          
            // Called from Cancel Button           
            Database.DMLOptions dmlOptions = new Database.DMLOptions();
            dmlOptions.OptAllOrNone = false;
            createCancelledCase(cas,cas[0].AssessmentLineItems__r,true, dmlOptions);
            
            
        }else{
            // Called from Cancel & Reissue Button 
            cloningOfAssessmentAndLineItems(); 
            
        }
        
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method is called from redirect method for cloning the Assessments
    // @return : void
    //-----------------------------
    
    public void cloningOfAssessmentAndLineItems() {
        Map<String,List<AssessmentLineItem__c>> assessmentLineItems = new Map<String,List<AssessmentLineItem__c>>();
        List<Case> assessment =  new List<Case>(); 
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        
        List<AssessmentLineItem__c> lineItem  = new List<AssessmentLineItem__c>();
        //ASR-8548 - to Clone Case Ownership
        List<CaseOwnershipRelation__c> caseOwnershipRelations = new List<CaseOwnershipRelation__c>();
        Id assessmentVessselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME);
        Id assessmentLegacyVessselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME);
         Map<Id,List<CaseOwnershipRelation__c>> caseIdByCaseOwnership = new Map<Id,List<CaseOwnershipRelation__c>>();
        //Calling the Assessment Utility method for cloning 
        Map<String,Object> clonedDataMapByListNames =  AssessmentsUtility.getCloneAssessmentsForAdjustments(cas,recordTypeId);
        Id cancelledCaseId;
        try{
            if(cas[0].Id != null){
                // Cannot insert the multiple records instances in case of Deep Clonning in same DML as Salesforce gives DML error 
                // and hence called a different method to create Cancelled Case.
                // ASR-8548 : Send false if reissue and cancel option need
                //ASR-9548 get cancelled case id to update in Parent Cancel Case of reissued case
                cancelledCaseId = createCancelledCase(cas,cas[0].AssessmentLineItems__r,false, dmlOptions); 
            }
            if(!clonedDataMapByListNames.isEmpty()) {
                
                assessment =  (List<Case>)clonedDataMapByListNames.get('assessments');
                assessmentLineItems = (Map<String,List<AssessmentLineItem__c>>)clonedDataMapByListNames.get('assessmentLIBySourceId');
            }
            
            for(Case cs : assessment){
              	// Moved Few fields in AssessmentUtility genric Method (getCloneAssessmentsForAdjustments)
                
                cs.ownerId = userInfo.getUserId();
                cs.type = CCSFConstants.ASSESSMENT.TYPE_ESCAPE; 
                cs.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
                
                // Checking if parent CancelAssessment__c
                if(cas[0].ParentId != null && !cas[0].Parent.CancelAttempted__c) {
                    cs.ParentId =cas[0].ParentId;
                }else {
                    //Changes a per ASR-9360,parent Case be null if No assessment exist which is not canceled
                    cs.ParentId = null;
                }
                //Bug fix for resissued case, created from cascaded case
                if(cs.ParentRegularAssessment__c !=null){
                    cs.ParentRegularAssessment__c = null;
                }
                if(cancelledCaseId !=null){
                    cs.ParentCancelCase__c = cancelledCaseId;
                }
                cs.AdjustmentReason__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_REASON_TRANSFER;
            }
            AsyncDlrsCalculator.getInstance().setQueueToRun(false);
            // Inserting Assessments
            insert assessment;
            List<AssessmentLineItem__c> assessmentLineItemsToInsert = new List<AssessmentLineItem__c>();
            if(!assessmentLineItems.isEmpty()){
                // Assessment line Items exists 
                lineItem = (List<AssessmentLineItem__c>)assessmentLineItems.get(cas[0].id);
                for(AssessmentLineItem__c assessmentLineItem : lineItem){
                    AssessmentLineItem__c clonedLineItem = assessmentLineItem;
                    assessmentLineItem.Case__c = assessment[0].id;
                }
                insert lineItem;
            }
            // Update the CancelAttempted__c on assessment when button is clicked.This will prevent user to generate multiple Cancel Assessments
            updateCancelledAssessment(cas[0].Id);
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + assessment[0].id;
            AsyncDlrsCalculator.getInstance().setQueueToRun(true);
            recordStatus = System.Label.CancelAndReissuedAssessmentsCreated;
        } catch(Exception ex) {
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + cas[0].Id;
            String error = ex.getmessage();
            if(error.contains(CCSFConstants.ASSESSMENT.VALIDATION_EXCEPTION_TEXT)){
                error = error.substringAfter(CCSFConstants.ASSESSMENT.VALIDATION_EXCEPTION_TEXT + ',');
                error = error.removeEndIgnoreCase(': []');
                recordStatus = error;
            }else {
                recordStatus = Label.ErrorMessageForUser;
            }
            Logger.addExceptionEntry(ex);
        } finally{
            Logger.saveLog();
        }
       
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method is used to create Cancelled Case after Canceling and reissued button is pressed.
    // @return : void
    //----------------------------
    
    public Id createCancelledCase(List<Case> assessmentToCancel, List<AssessmentLineItem__c> lineItems, Boolean onlyCancel, Database.DMLOptions dmlOptions ) {
        try{
            Id assessmentBPPRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
            Id assessmentVessselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME);
            Id assessmentLegacyVessselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME);
            Id assessmentLegacyBPPRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME); 
            //ASR-8548 - to Clone Case Ownership
            List<CaseOwnershipRelation__c> caseOwnershipRelations = new List<CaseOwnershipRelation__c>();
            Map<Id,List<CaseOwnershipRelation__c>> caseIdByCaseOwnership = new Map<Id,List<CaseOwnershipRelation__c>>();
        
            Map<String,Object> clonedDataMapByListNames =  AssessmentsUtility.getCloneAssessmentsForAdjustments(assessmentToCancel,recordTypeId); 
            List<Case> cancelledCaseList =  (List<Case>)clonedDataMapByListNames.get('assessments'); 

            if(assessmentToCancel[0].Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR  && (assessmentToCancel[0].recordtypeId == assessmentBPPRecordTypeId || assessmentToCancel[0].recordtypeId == assessmentLegacyBPPRecordTypeId))  {
                // ASR-8548: Type = Regular and (Record Type BPP or Legacy BPP)
                cancelledCaseList[0].TotalFixturesValueOverride__c = 0;
                cancelledCaseList[0].TotalPersonalPropertyValueOverride__c = 0;
                // ASR -10600 - ExemptAmountManual = 0
                cancelledCaseList[0].ExemptAmountManual__c = 0;                
                cancelledCaseList[0].type = assessmentToCancel[0].Type;
                if(assessmentToCancel[0].recordtypeId == assessmentLegacyBPPRecordTypeId)  {
                    // ASR-8548: If Record Type is Legacy BPP
                    cancelledCaseList[0].IsLegacy__c = false;
                    // ASR-8548: Setting Legacy BPP record Type as BPP Record Type
                    cancelledCaseList[0].RecordTypeId = assessmentBPPRecordTypeId;
                }
                
            }else if(assessmentToCancel[0].Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && (assessmentToCancel[0].recordtypeId == assessmentBPPRecordTypeId || assessmentToCancel[0].recordtypeId == assessmentLegacyBPPRecordTypeId) ) {
                // ASR-8548: Type = Escape and ( Record type BPP and Legacy Record Type)
                //ASR -10600 - adding null check
                cancelledCaseList[0].TotalFixturesValueOverride__c = assessmentToCancel[0].BasisFixtureValue__c != null ? assessmentToCancel[0].BasisFixtureValue__c:0;
                cancelledCaseList[0].TotalPersonalPropertyValueOverride__c = assessmentToCancel[0].BasisPersonalPropertyValue__c != null ? assessmentToCancel[0].BasisPersonalPropertyValue__c:0;
                // ASR-10600 ExemptAmountManual__c Parent BasisExemptAmount__c
                cancelledCaseList[0].ExemptAmountManual__c = assessmentToCancel[0].BasisExemptAmount__c !=null ? assessmentToCancel[0].BasisExemptAmount__c :0;
                if(assessmentToCancel[0].recordtypeId == assessmentLegacyBPPRecordTypeId)  {
                    // ASR-8548: If Record Type is Legacy BPP
                    cancelledCaseList[0].IsLegacy__c = false;
                    // ASR-8548: Setting Legacy BPP record Type as BPP Record Type
                    cancelledCaseList[0].RecordTypeId = assessmentBPPRecordTypeId;
                }
                
            }else if((assessmentToCancel[0].recordtypeId == assessmentVessselRecordTypeId || assessmentToCancel[0].recordtypeId == assessmentLegacyVessselRecordTypeId) && assessmentToCancel[0].Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR) {
                //ASR-8548: Type = Regular and (Record Type Vessel and Legacy Vessel)
                cancelledCaseList[0].VesselTotalValueOverride__c = 0;
                // ASR -10600 - ExemptAmountManual = 0
                cancelledCaseList[0].ExemptAmountManual__c = 0;
                cancelledCaseList[0].type = assessmentToCancel[0].Type;
                if(assessmentToCancel[0].recordtypeId == assessmentLegacyVessselRecordTypeId)  {
                    // ASR-8548: If Record Type is Legacy Vessel
                    cancelledCaseList[0].IsLegacy__c = false;
                    // ASR-8548: Setting Legacy BPP record Type as BPP Record Type
                    cancelledCaseList[0].RecordTypeId = assessmentVessselRecordTypeId;
                }
                
            }else if((assessmentToCancel[0].recordtypeId == assessmentVessselRecordTypeId || assessmentToCancel[0].recordtypeId == assessmentLegacyVessselRecordTypeId) && assessmentToCancel[0].Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE) {
                //ASR-8548: Type = Escape and (Record Type Vessel and Legacy Vessel)
                //ASR -10600 - adding null check as per this bug
                cancelledCaseList[0].VesselTotalValueOverride__c = assessmentToCancel[0].BasisPersonalPropertyValue__c != null? assessmentToCancel[0].BasisPersonalPropertyValue__c : 0;
                // ASR-10600 ExemptAmountManual__c Parent BasisExemptAmount__c
                cancelledCaseList[0].ExemptAmountManual__c = assessmentToCancel[0].BasisExemptAmount__c !=null ? assessmentToCancel[0].BasisExemptAmount__c :0;
               if(assessmentToCancel[0].recordtypeId == assessmentLegacyVessselRecordTypeId)  {
                    // ASR-8548: If Record Type is Legacy Vessel
                    cancelledCaseList[0].IsLegacy__c = false;
                    // ASR-8548: Setting Legacy BPP record Type as BPP Record Type
                    cancelledCaseList[0].RecordTypeId = assessmentVessselRecordTypeId;
                }
            }
            
            cancelledCaseList[0].parentId  = assessmentToCancel[0].id;
            cancelledCaseList[0].type  = assessmentToCancel[0].type;
            cancelledCaseList[0].Roll__c = null;
            cancelledCaseList[0].AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_CANCEL;
            cancelledCaseList[0].ownerId = userInfo.getUserId();
            if(!onlyCancel){
                cancelledCaseList[0].AdjustmentReason__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_REASON_TRANSFER;
                //Bug fix for new cancelled case, created from cascaded case
                if(cancelledCaseList[0].ParentRegularAssessment__c !=null){
                    cancelledCaseList[0].ParentRegularAssessment__c = null;
                }
            }
            
            AsyncDlrsCalculator.getInstance().setQueueToRun(false);
            //inserting new cancelled assessment
            insert cancelledCaseList;
            List<AssessmentLineItem__c> assessmentLineItems  = new List<AssessmentLineItem__c>();
            if(!lineItems.isEmpty()) {
                for(AssessmentLineItem__c assessmentLineItem : lineItems){
                    AssessmentLineItem__c lineItem = assessmentLineItem.clone(false, true, false, false);
                    lineItem.Case__c = cancelledCaseList[0].id;
                    assessmentLineItems.add(lineItem);
                }
                //inserting new cancelled assessment line items
                insert assessmentLineItems;
            }
            
            if(onlyCancel){
                // Update the CancelAttempted__c on assessment when button is clicked.This will prevent user to generate multiple Cancel Assessments
                updateCancelledAssessment(assessmentToCancel[0].Id);
                recordStatus = System.Label.CancelAssessmentsCreated;
                if(cancelledCaseList[0].id != null ) {
                    baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + cancelledCaseList[0].id;
                }
            }
            AsyncDlrsCalculator.getInstance().setQueueToRun(true);
            return cancelledCaseList[0].Id;
        }
        catch(Exception ex) {
            baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + assessmentToCancel[0].Id;
            //recordStatus = System.Label.NoPermission;
            String error = ex.getmessage();
            if(error.contains(CCSFConstants.ASSESSMENT.VALIDATION_EXCEPTION_TEXT)){
                error = error.substringAfter(CCSFConstants.ASSESSMENT.VALIDATION_EXCEPTION_TEXT + ',');
                error = error.removeEndIgnoreCase(': []');
                recordStatus = error;
            }else {
                recordStatus = Label.ErrorMessageForUser;
            }
            Logger.addExceptionEntry(ex);
        } finally{
            Logger.saveLog();
        }
        return null;
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String CaseId
    // @description : This method takes Case id and return if access to perform action
    // @return : Boolean
    // @ASR- 8548 
    //-----------------------------
    
    public Static AccessWithMessageWrapper hasAccessToPerformAction(Case currentCase,String genericMessage){
       String filter = 'AND Status =\''+ CCSFConstants.ASSESSMENT.STATUS_CLOSED +'\'';
       filter += ' AND SubStatus__c =\''+ CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED+'\'';
       filter += ' AND AssessmentYear__c =\''+ currentCase.AssessmentYear__c+'\'';

       // Bug- ASR 9751
       // Wrapper class with Boolean and String 
       AccessWithMessageWrapper accessAndMessageWrap= new AccessWithMessageWrapper();
       // by default setting Wrapper Access as False with Generic Message
       accessAndMessageWrap.access = false;
       accessAndMessageWrap.message = genericMessage;
      
            if(currentCase.CancelAttempted__c){
            // If Cancel Attempted is checked then user should not allowed to Cancel   
                accessAndMessageWrap.access = false;
                accessAndMessageWrap.message = System.Label.AlreadyCancelMessage;                
                return accessAndMessageWrap;               
            }
            if( currentCase.AdjustmentType__c != CCSFConstants.ASSESSMENT.ADJUSTMENT_CANCEL && (currentCase.IntegrationStatus__c == CCSFConstants.ASSESSMENT.SENT_TO_TTX || currentCase.IntegrationStatus__c == CCSFConstants.ASSESSMENT.SENT_TO_RP) ){
                // AdjustType should not be cancelled and Integration Status can be Sent to TTX or Sent to RP
                if(AssessmentsUtility.retrieveCaseToPerformAction(currentCase.Property__c, currentCase.Id,filter)){
                    // if method return true then set allowed Action as true
                    accessAndMessageWrap.access = true;
                    accessAndMessageWrap.message = System.Label.CancelAllowed;
                }
                return accessAndMessageWrap;
            }
        
        return accessAndMessageWrap;
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : 
    // @description :Wrapper Class with Access and Message to display
    // @return : Boolean
    // @ASR- 9751
    //-----------------------------

    public class AccessWithMessageWrapper{
        Boolean access{get;set;}
        String message{get;set;}
    }

    //-----------------------------
    // @author : Publicis Sapient 
    // @param : 
    // @description : ASR-9548 - Created this future method to put cancelled case update in async, to prevent CPU time limit error
    // @return : Boolean
    // @ASR- 9751
    //-----------------------------
    @future
    public static void updateCancelledAssessment(Id assessmentId){ 
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        //Case cancelledAssessment = [select id, CancelAttempted__c from Case where Id =: assessmentId];
        Case cancelledAssessment = new Case();
        cancelledAssessment.Id = assessmentId;
        cancelledAssessment.CancelAttempted__c = true;   
        Map<String,Object> updateRecord = DescribeUtility.updateRecords(new  List<Case>{cancelledAssessment},dmlOptions);
    }
  
}