/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class ConversationController{

    @AuraEnabled
    
    public static String conversationCallString(String extName, Map<String, Object> filterCondition){
        String result;
        try{
        String jwtToken = CreateJWTToken.generateJWTToken();
        Map<String,String> headers = new Map<String,String>();
        headers.put('Authorization', 'Bearer '+jwtToken);
        headers.put('Content-Type', 'application/json');
        String urlPath = Test.isRunningTest() ? 'http://dummyClassName.com' :[Select Endpoint from NamedCredential where developerName ='SMART_Job_Monitoring_Endpoint' limit 1].Endpoint;
        urlPath +=extName;
      
        Callout callout= new Callout(urlPath).setHeaders(headers);
        HttpResponse response = callout.post(filterCondition);
     	 result = response.getBody();
     
        return response.getBody();
        }catch (Exception e){
            Logger.addExceptionEntry(e, 'Exception at conversationCallString');
        }
        Logger.saveLog();
        return result;
    }
}