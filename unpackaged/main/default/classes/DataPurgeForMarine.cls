/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * Class Name: DataPurgeForMarine
 * Description: Generic Class where all purge functions are held.
 * Author : Publicis Sapient 
 */
public without sharing class DataPurgeForMarine {
    Map<String, String> deletedObjectNameByCount = new Map<String, String>();
    
    private void deleteCaseOwnerRelationShip(Set<Id> caseIds){
        Integer countCaseOwnership =0;
        List<CaseOwnershipRelation__c> caseOwnershipRelations = new List<CaseOwnershipRelation__c>();
        if(caseIds!=null && !caseIds.isEmpty()){
            caseOwnershipRelations = [select id from CaseOwnershipRelation__c where Case__c in :caseIds];
        }
        if(caseOwnershipRelations.isEmpty()){
            return;
        } else {
            Database.DeleteResult[] drList = Database.delete(caseOwnershipRelations, false);

            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countCaseOwnership++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('CaseOwnershipRelation__c')), string.valueOf(countCaseOwnership));
        }
    }
    
    private void deleteCases(Set<Id> propertyIds, Set<Id> accountIds){
        Integer countByCases = 0;
        Map<Id,Case> casesFromProperty = new Map<Id,Case>();
        Map<Id,Case> casesFromAccount = new Map<Id,Case>();
        if(propertyIds!=null && !propertyIds.isEmpty()){
            casesFromProperty = new Map<Id,Case> ([select id from Case where property__c in :propertyIds]);
        }
        if(accountIds!=null && !accountIds.isEmpty()){
            casesFromAccount = new Map<Id,Case>([select id from Case where AccountId in :accountIds]);
        }
        if(casesFromProperty.isEmpty() && casesFromAccount.isEmpty()){
            return;
        } else {
            casesFromAccount.putAll(casesFromProperty);
            deleteValuations(casesFromAccount.KeySet());
            deleteAudits(casesFromAccount.KeySet(),'Case');
            deleteDocumentGenerationRequests(casesFromAccount.KeySet());
            deleteCaseOwnerRelationShip(casesFromAccount.KeySet());
            Database.DeleteResult[] drList = Database.delete(casesFromAccount.values(), false);

            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countByCases++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('Case')), string.valueOf(countByCases));
        }
    }
    
    // Delete all Document Generation Requests associated with a Case
    private void deleteDocumentGenerationRequests(Set<Id> caseIds){
        Integer countByDocumentGenerationRequests = 0;
        List<DocumentGenerationRequest__c> documentGenerationRequests = new List<DocumentGenerationRequest__c>();
        
        if(caseIds!=null && !caseIds.isEmpty()){
            documentGenerationRequests = [SELECT Id FROM DocumentGenerationRequest__c where Case__c in :caseIds];
        }
        
        if(documentGenerationRequests.isEmpty()){
            return;
        } else {
            Database.DeleteResult[] deleteResults = Database.delete(documentGenerationRequests, false);
            for(Database.DeleteResult result : deleteResults) {
                if (result.isSuccess()) {
                    countByDocumentGenerationRequests++;
                }
            }
            
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('DocumentGenerationRequest__c')), 
                                         string.valueOf(countByDocumentGenerationRequests));
        }        
    }
    
    // Delete all Valuations for a case
    private void deleteValuations(Set<Id> caseIds){
        Integer countByValuation = 0;
        List<Valuation__c> caseValuations = new List<Valuation__c>();
        
        if(caseIds!=null && !caseIds.isEmpty()){
            caseValuations = [select id from Valuation__c where Assessment__c in :caseIds];
        }
        
        if(caseValuations.isEmpty()){
            return;
        } else {
            Database.DeleteResult[] deleteResults = Database.delete(caseValuations, false);
            for(Database.DeleteResult result : deleteResults) {
                if (result.isSuccess()) {
                    countByValuation++;
                }
            }
            
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('Valuation__c')), string.valueOf(countByValuation));
        }
        
    } 
    
    // Delete All Audits associated to Case
    private void deleteAudits(Set<Id> recordIds, String caseOrAccount){
        Integer countByAudits = 0;
        List<Audit__c> audits = new List<Audit__c>();
        
        
        if(recordIds!=null && !recordIds.isEmpty()){
            if(caseOrAccount == 'Case'){
                audits = [select id from Audit__c where Assessment__c in :recordIds];
            }else if(caseOrAccount == 'Account'){
                audits = [select id from Audit__c where Account__c in :recordIds];
            }
        }
        
        if(audits.isEmpty() ){
            return;
        } else {
            Database.DeleteResult[] deleteResults = Database.delete(audits, false);
            for(Database.DeleteResult result : deleteResults) {
                if (result.isSuccess()) {
                    countByAudits++;
                }
            }
            
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('Audit__c')), string.valueOf(countByAudits));
        }
    }
    
    private void deleteStatements(Set<Id> propertyIds, Set<Id> accountIds){
        Integer countStatements = 0;
        List<Statement__c> statementsFromProperty = new List<Statement__c>();
        List<Statement__c> statementsFromAccount = new List<Statement__c>();
        if(propertyIds!=null && !propertyIds.isEmpty()){
            statementsFromProperty = [select id from Statement__c where property__c in :propertyIds];
        }
        if(accountIds!=null && !accountIds.isEmpty()){
            statementsFromAccount = [select id from Statement__c where BusinessAccount__c in :accountIds or PrimaryAccount__c in :accountIds];
        }
        if(statementsFromProperty.isEmpty() && statementsFromAccount.isEmpty()){
            return;
        } else {
            statementsFromProperty.addAll(statementsFromAccount);
            Database.DeleteResult[] drList = Database.delete(statementsFromProperty, false);

            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countStatements++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('Statement__c')), string.valueOf(countStatements));
        }
    }
    
    private void deleteDocumentGenerationRequests(Set<Id> propertyIds, Set<Id> accountIds){
        Integer countDocumentGenerationRequests = 0;
        List<DocumentGenerationRequest__c> dgrFromProperty = new List<DocumentGenerationRequest__c>();
        List<DocumentGenerationRequest__c> dgrFromAccount = new List<DocumentGenerationRequest__c>();
        if(propertyIds!=null && !propertyIds.isEmpty()){
            dgrFromProperty = [select id from DocumentGenerationRequest__c where property__c in :propertyIds];
        }
        if(accountIds!=null && !accountIds.isEmpty()){
            dgrFromAccount = [select id from DocumentGenerationRequest__c where account__c in :accountIds];
        }
        if(dgrFromProperty.isEmpty() && dgrFromAccount.isEmpty()){
            return;
        } else {
            dgrFromProperty.addAll(dgrFromAccount);
            Database.DeleteResult[] drList = Database.delete(dgrFromProperty, false);

            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countDocumentGenerationRequests++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('DocumentGenerationRequest__c')), string.valueOf(countDocumentGenerationRequests));
        }
    }
    
    private void deleteOwnershipTransactions(Set<Id> ownerShipIds){
        Integer countOwnerShips = 0;
        List<OwnershipTransaction__c> ownerShips = new List<OwnershipTransaction__c>();
        if(ownerShipIds!=null && !ownerShipIds.isEmpty()){
            ownerShips = [select id from OwnershipTransaction__c where Ownership__c in :ownerShipIds];
        }
        if(ownerShips.isEmpty()){
            return;
        } else {
            Database.DeleteResult[] drList = Database.delete(ownerShips, false);

            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countOwnerShips++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('OwnershipTransaction__c')), string.valueOf(countOwnerShips));
        }
    }
    
    private void deleteOwnerships(Set<Id> accountIds){
        Integer countOwnerShips =0;
        Map<Id,Ownership__c> ownerShips;
        if(accountIds!=null && !accountIds.isEmpty()){
            ownerShips = new Map<Id,Ownership__c>([select id from Ownership__c where Account__c in :accountIds]);
        }
        if(ownerShips == null || ownerShips.isEmpty()){
            return;
        } else {
            deleteOwnershipTransactions(ownerShips.KeySet());
            Database.DeleteResult[] drList = Database.delete(ownerShips.values(), false);

            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countOwnerShips++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('Ownership__c')), string.valueOf(countOwnerShips));
        }
    }
    
    private void deleteOwnershipsByProperty(Set<Id> propertyIds){
        Integer countOwnershipsByProperty = 0;
        Map<Id,Ownership__c> ownerShips;
        if(propertyIds!=null && !propertyIds.isEmpty()){
            ownerShips = new Map<Id,Ownership__c>([select id from Ownership__c where Property__c in :propertyIds]);
        }
        if(ownerShips == null || ownerShips.isEmpty()){
            return;
        } else {
            deleteOwnershipTransactions(ownerShips.KeySet());
            Database.DeleteResult[] drList = Database.delete(ownerShips.values(), false);

            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countOwnershipsByProperty++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('Ownership__c')), string.valueOf(countOwnershipsByProperty));
        }
    }
    
    private void deleteNotes(Set<Id> propertyIds){
        Integer countByNotes = 0;
        Set<ContentDocumentLink> contentDocumentLinks;
        Set<Id> contentDocumentId = new Set<Id>();
        Map<Id,ContentNote > notes;
        if(propertyIds!=null && !propertyIds.isEmpty()){
            contentDocumentLinks = new Set<ContentDocumentLink>([SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId in : propertyIds]);
			system.debug('propertyIds'+propertyIds);
            system.debug('contentDocumentLinks'+contentDocumentLinks);
            for(ContentDocumentLink link : contentDocumentLinks)
            {
                contentDocumentId.add(link.ContentDocumentId);
            }
            system.debug('contentDocumentLinks'+contentDocumentId);
            notes = new Map<Id,ContentNote>([SELECT Id from ContentNote where Id In : contentDocumentId]);
            system.debug('notes'+notes);
        }
        if(notes == null || notes.isEmpty()){
            return;
        } else {
            Database.DeleteResult[] drList = Database.delete(notes.values(), false);
            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countByNotes++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('ContentNote')), string.valueOf(countByNotes));
        }
    }
    
    private void deletePropertyEvents(Set<Id> propertyIds, Set<Id> accountIds){
        Integer countByPropertyEvent = 0;
        Map<Id,PropertyEvent__c> propertyEventsFromProperty = new Map<Id,PropertyEvent__c>();
        Map<Id,PropertyEvent__c> propertyEventsFromAccount = new Map<Id,PropertyEvent__c>();
        if(propertyIds!=null && !propertyIds.isEmpty()){
            propertyEventsFromProperty = new Map<Id,PropertyEvent__c> ([select id from PropertyEvent__c where property__c in :propertyIds]);
        }
        if(accountIds!=null && !accountIds.isEmpty()){
            propertyEventsFromAccount = new Map<Id,PropertyEvent__c>([select id from PropertyEvent__c where PropertyAccount__c in :accountIds]);
        }
        if(propertyEventsFromProperty.isEmpty() && propertyEventsFromAccount.isEmpty()){
            return;
        } else {
            propertyEventsFromAccount.putAll(propertyEventsFromProperty);
            //deleteCaseOwnerRelationShip(casesFromAccount.KeySet());
            Database.DeleteResult[] drList = Database.delete(propertyEventsFromAccount.values(), false);
            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countByPropertyEvent++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('PropertyEvent__c')), string.valueOf(countByPropertyEvent));
        }
    }
    
    private void deletePropertiesById(Set<Id> propertyIds){
        Integer countByProperty = 0;
        Map<Id, Property__c> propertiesById;
        if(propertyIds!=null && !propertyIds.isEmpty()){
            propertiesById = new Map<Id,Property__c>([select id from Property__c where id in :propertyIds]);
        }
        if(propertiesById == null || propertiesById.isEmpty()){
            return;
        } else {
            deleteOwnershipsByProperty(propertiesById.keySet());
            deleteDocumentGenerationRequests(propertiesById.keySet(),new Set<Id>());
            deleteStagingAumentumBusiness(propertiesById.keySet(),'Property');
            deleteStagingBusinessCompareFields(propertiesById.keySet(),'Property');
            deleteNotes(propertiesById.keySet());
            deleteCases(propertiesById.keySet(),new Set<Id>());
            deleteStatements(propertiesById.keySet(),new Set<Id>());
            deletePropertyEvents(propertiesById.keySet(),new Set<Id>());
            Database.DeleteResult[] drList = Database.delete(propertiesById.values(), false);

            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countByProperty++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('Property__c')), string.valueOf(countByProperty));
        }
    }
    
    // Delete all StagingAumentumBusiness for an Account/Property
    private void deleteStagingAumentumBusiness(Set<Id> recordIds, String accountOrProperty){
        Integer countStagingAumentumBusiness = 0;
        List<StagingAumentumBusiness__c> stagingAumentumBusinesses = new List<StagingAumentumBusiness__c>();
        
        if(recordIds!=null && !recordIds.isEmpty()){
            if(accountOrProperty == 'Account'){
            	stagingAumentumBusinesses = [select id from StagingAumentumBusiness__c where MappedAccount__c in :recordIds];    
            }else if(accountOrProperty == 'Property'){
                stagingAumentumBusinesses = [select id from StagingAumentumBusiness__c where MappedProperty__c in :recordIds];
            }
        }
        
        if(stagingAumentumBusinesses.isEmpty()){
            return;
        } else {
            Database.DeleteResult[] deleteResults = Database.delete(stagingAumentumBusinesses, false);
            for(Database.DeleteResult result : deleteResults) {
                if (result.isSuccess()) {
                    countStagingAumentumBusiness++;
                }
            }
            
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('StagingAumentumBusiness__c')), 
                                         string.valueOf(countStagingAumentumBusiness));
        }
        
    }
    
    // Delete all StagingBusinessCompareFields for an Account/Property
    private void deleteStagingBusinessCompareFields(Set<Id> recordIds, String accountOrProperty){
        Integer countStagingBusinessesCompare = 0;
        List<StagingBusinessCompareField__c> stagingBusinessesCompare = new List<StagingBusinessCompareField__c>();
        
        if(recordIds!=null && !recordIds.isEmpty()){
            if(accountOrProperty == 'Account'){
            	stagingBusinessesCompare = [select id from StagingBusinessCompareField__c where MappedBusiness__c in :recordIds];    
            }else if(accountOrProperty == 'Property'){
                stagingBusinessesCompare = [select id from StagingBusinessCompareField__c where MappedProperty__c in :recordIds];
            }
        }
        
        if(stagingBusinessesCompare.isEmpty()){
            return;
        } else {
            Database.DeleteResult[] deleteResults = Database.delete(stagingBusinessesCompare, false);
            for(Database.DeleteResult result : deleteResults) {
                if (result.isSuccess()) {
                    countStagingBusinessesCompare++;
                }
            }
            
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('StagingBusinessCompareField__c')), 
                                         string.valueOf(countStagingBusinessesCompare));
        }
        
    }
    
    public Map<String, String> deletePropertiesByLegacyProperty(List<String> legacyPropertyIds){
        Map<id, Property__c> propertiesById = new Map<id, Property__c>();
        Set<Id> propertiesToProcess = new Set<Id>();
        String nonVesselIds = '';
        
        if(legacyPropertyIds!=null && !legacyPropertyIds.isEmpty()){
            propertiesById = new Map<id, Property__c>([select id,RecordTypeId from Property__c where PropertyId__c in :legacyPropertyIds]);
        }
        
        if(propertiesById == null || propertiesById.isEmpty()){
            return deletedObjectNameByCount;
        } else {
            Id recordTypeById= Schema.SObjectType.property__c.getRecordTypeInfosByDeveloperName().get('Vessel').getRecordTypeId();
            for(String propertyId : propertiesById.keySet())
            {
                Property__c property = propertiesById.get(propertyId);
                if(property.RecordTypeId == recordTypeById){
                    propertiesToProcess.add(propertyId);
                }else{
                    nonVesselIds = nonVesselIds + propertyId + ' ,';
                }
            }
            
            if(nonVesselIds.length()>0){
                deletedObjectNameByCount.put('Non Vessel Ids', nonVesselIds.substring(0, nonVesselIds.length()-2));
            }
            deletePropertiesById(propertiesToProcess);
        }
        system.debug('deletedObjectNameByCount: '+deletedObjectNameByCount);
        return deletedObjectNameByCount;
    }
    
    public Map<String, String> deleteAccountsByCompanyId(List<String> legacyCompanyIds){
        Integer countByAccount = 0;
        Map<Id,Account> accountsById;
        accountsById = new Map<Id,Account>([select id from Account where EntityId__c  in :legacyCompanyIds]);
        List<Ownership__c> Ownerships;
        
        Id recordTypeById= Schema.SObjectType.property__c.getRecordTypeInfosByDeveloperName().get('Vessel').getRecordTypeId(); 
        Ownerships = new List<Ownership__c>([select id,property__c,Account__c from Ownership__c 
                                             where Account__c in :accountsById.keySet() and property__r.RecordType.Id= :recordTypeById]);
        
        if(Ownerships == null || Ownerships.isEmpty()){
            return deletedObjectNameByCount;
        } else {
            Set<Id> propertyIds = new Set<Id>();
            Set<Id> accounts = new Set<Id>();
            for(Ownership__c o:Ownerships){
                propertyIds.add(o.property__c);
                accounts.add(o.Account__c);
            }
            
            List<Account> processedAccounts;
            processedAccounts = new List<Account>([select id from Account where id in :accounts]);
            deleteStagingAumentumBusiness(accounts,'Account');
            deleteStagingBusinessCompareFields(accounts,'Account');
            deleteAudits(accounts,'Account');
            deleteCases(propertyIds, new Set<Id>());
            deleteDocumentGenerationRequests(new Set<Id>(),accounts);
            deleteOwnerships(accounts);
            deleteStatements(new Set<Id>(),accounts);
            deletePropertiesById(propertyIds);
            Database.DeleteResult[] drList = Database.delete(processedAccounts, false);
            // Iterate through each returned result
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, count the record.
                    countByAccount++;
                }
            }
            deletedObjectNameByCount.put(String.valueOf(Schema.getGlobalDescribe().get('Account')), string.valueOf(countByAccount));
            system.debug(System.LoggingLevel.ERROR, 'deletedObjectNameByCount : '+deletedObjectNameByCount);
        }
        return deletedObjectNameByCount;
    }
}