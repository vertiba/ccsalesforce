public without sharing class VIPFormRestoreService implements Database.Batchable<SObject>, Database.Stateful {

    private static final String LOG_LOCATION = 'VIPFormRestoreService';

    private static Boolean executingImport = false;

    private Set<Id> formBrandingIds = new Set<Id>();
    private Set<Id> templateIds     = new Set<Id>();
    private Set<Id> categoryIds     = new Set<Id>();
    private Set<Id> componentIds    = new Set<Id>();

    public static Boolean isImporting() {
        return executingImport;
    }

    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return Database.getQueryLocator([
            SELECT Id, Body, Name
            FROM StaticResource
            WHERE Name LIKE :VIPFormTemplateDTO.STATIC_RESOURCE_PREFIX + '%'
        ]);
    }

    public void execute(Database.BatchableContext batchableContext, List<StaticResource> scope) {
        executingImport = true;

        Map<String, VIPForm__VIP_Form_Branding__c> formBrandingsToSaveByUuid = new Map<String, VIPForm__VIP_Form_Branding__c>();
        Map<String, VIPForm__VIP_Template__c> templatesToSaveByUuid          = new Map<String, VIPForm__VIP_Template__c>();
        Map<String, VIPForm__VIP_Category__c> categoriesToSaveByUuid         = new Map<String, VIPForm__VIP_Category__c>();
        Map<String, String> categoriesToRelateByUuid                         = new Map<String, String>();
        Map<String, VIPForm__VIP_Component__c> componentsToSaveByUuid        = new Map<String, VIPForm__VIP_Component__c>();

        for(StaticResource templateStaticResource : scope) {
            String templateStaticResourceBody  = this.getTemplateStaticResourceBody(templateStaticResource);
            VIPFormTemplateDTO formTemplateDTO = (VIPFormTemplateDTO)Json.deserialize(templateStaticResourceBody, VIPFormTemplateDTO.class);

            Logger.addDebugEntry('Processing form, name: ' + formTemplateDTO.template.Name, LOG_LOCATION);

            /* Process form branding */
            VIPForm__VIP_Form_Branding__c formBranding = formTemplateDTO.branding;
            Logger.addDebugEntry('Processing template, UUID: ' + formBranding.Uuid__c, LOG_LOCATION);
            formBranding.Id = null;

            formBrandingsToSaveByUuid.put(formbranding.Uuid__c, formBranding);

            /* Process template */
            VIPForm__VIP_Template__c template = (VIPForm__VIP_Template__c)this.removeField(formTemplateDTO.template, 'VIPForm__VIP_Form_Branding__c');
            Logger.addDebugEntry('Processing template, UUID: ' + template.Uuid__c, LOG_LOCATION);

            template.RecordTypeId                  = this.getRecordTypeId(template.getSObjectType(), template.RecordType.DeveloperName);
            template.VIPForm__VIP_Form_Branding__r = new VIPForm__VIP_Form_Branding__c(Uuid__c = formBranding.Uuid__c);

            templatesToSaveByUuid.put(template.Uuid__c, template);

            /* Process categories */
            Logger.addDebugEntry('Processing categories', LOG_LOCATION);
            List<VIPForm__VIP_Category__c> categories = formTemplateDTO.categories;
            for(VIPForm__VIP_Category__c category : categories) {
                List<String> fieldsToRemove = new List<String>{'VIPForm__Parent_VIP_Form_Section__c', 'VIPForm__Template__c'};
                category = (VIPForm__VIP_Category__c)this.removeFields(category, fieldsToRemove);

                Logger.addDebugEntry('Processing category, UUID: ' + category.Uuid__c, LOG_LOCATION);

                category.RecordTypeId         = this.getRecordTypeId(category.getSObjectType(), category.RecordType.DeveloperName);
                category.VIPForm__Template__r = new VIPForm__VIP_Template__c(Uuid__c = template.Uuid__c);

                if(category.VIPForm__Parent_VIP_Form_Section__r != null) {
                    categoriesToRelateByUuid.put(category.Uuid__c, category.VIPForm__Parent_VIP_Form_Section__r.Uuid__c);
                    category.VIPForm__Parent_VIP_Form_Section__r = null;
                }

                categoriesToSaveByUuid.put(category.Uuid__c, category);
            }

            /* Process components */
            Logger.addDebugEntry('Processing components', LOG_LOCATION);
            List<VIPForm__VIP_Component__c> components = new List<VIPForm__VIP_Component__c>();
            for(VIPForm__VIP_Category__c category : categories) {
                for(VIPForm__VIP_Component__c component : category.VIPForm__Components__r) {
                    component = (VIPForm__VIP_Component__c)this.removeField(component, 'VIPForm__Category__c');

                    component.RecordTypeId         = this.getRecordTypeId(component.getSObjectType(), component.RecordType.DeveloperName);
                    component.VIPForm__Category__r = new VIPForm__VIP_Category__c(Uuid__c = category.Uuid__c);

                    componentsToSaveByUuid.put(component.Uuid__c, component);
                }
            }
        }

        try {
            /* Save form branding */
            Logger.addDebugEntry('formBrandingsToSaveByUuid.values()=' + formBrandingsToSaveByUuid.values(), LOG_LOCATION);
            upsert formBrandingsToSaveByUuid.values() Uuid__c;
            formBrandingIds.addAll(new Map<Id, SObject>(formBrandingsToSaveByUuid.values()).keySet());

            /* Save template */
            Logger.addDebugEntry('templatesToSaveByUuid.values()=' + templatesToSaveByUuid.values(), LOG_LOCATION);
            upsert templatesToSaveByUuid.values() Uuid__c;
            templateIds.addAll(new Map<Id, SObject>(templatesToSaveByUuid.values()).keySet());

            /* Save categories & related parent-child categories */
            Logger.addDebugEntry('categoriesToSaveByUuid.values()=' + Json.serialize(categoriesToSaveByUuid.values()), LOG_LOCATION);
            upsert categoriesToSaveByUuid.values() Uuid__c;
            categoryIds.addAll(new Map<Id, SObject>(categoriesToSaveByUuid.values()).keySet());
            List<VIPForm__VIP_Category__c> childCategories = this.relateCategories(categoriesToRelateByUuid);
            Logger.addDebugEntry('childCategories=' + Json.serialize(childCategories), LOG_LOCATION);
            upsert childCategories Uuid__c;
            categoryIds.addAll(new Map<Id, SObject>(childCategories).keySet());

            /* Save components */
            Logger.addDebugEntry('componentsToSaveByUuid.values()=' + componentsToSaveByUuid.values(), LOG_LOCATION);
            upsert componentsToSaveByUuid.values() Uuid__c;
            componentIds.addAll(new Map<Id, SObject>(componentsToSaveByUuid.values()).keySet());

            Logger.saveLog();
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, LOG_LOCATION);
            Logger.saveLog();
            throw ex;
        }
        executingImport = false;
    }

    public void finish(Database.BatchableContext batchableContext) {
        /* Delete any old records that were not part of the restore process */
        List<VIPForm__VIP_Form_Branding__c> oldFormBrandings = [SELECT Id FROM VIPForm__VIP_Form_Branding__c WHERE Id NOT IN :this.formBrandingIds];
        delete oldFormBrandings;
        List<VIPForm__VIP_Template__c> oldTemplates          = [SELECT Id FROM VIPForm__VIP_Template__c WHERE Id NOT IN :this.templateIds];
        delete oldTemplates;
        List<VIPForm__VIP_Category__c> oldCategorys          = [SELECT Id FROM VIPForm__VIP_Category__c WHERE Id NOT IN :this.categoryIds];
        delete oldCategorys;
        List<VIPForm__VIP_Component__c> oldComponents        = [SELECT Id FROM VIPForm__VIP_Component__c WHERE Id NOT IN :this.componentIds];
        delete oldComponents;
    }

    private String getTemplateStaticResourceBody(StaticResource templateStaticResource) {
        String templateStaticResourceBody = templateStaticResource.Body.toString();
        for(VIPFormTextReplacement__c textReplacement : [SELECT OriginalText__c, ReplacementText__c FROM VIPFormTextReplacement__c]) {
            templateStaticResourceBody = templateStaticResourceBody.replace(textReplacement.OriginalText__c, textReplacement.ReplacementText__c);
        }
        return templateStaticResourceBody;
    }

    private Id getRecordTypeId(Schema.SObjectType sobjectType, String recordTypeDeveloperName) {
        return sobjectType.getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName).getRecordTypeId();
    }

    private SObject removeField(SObject record, String fieldName) {
        return this.removeFields(record, new List<String>{fieldName});
    }

    private SObject removeFields(SObject record, List<String> fieldNames) {
        Map<String, Object> recordMap = (Map<String, Object>)Json.deserializeUntyped(Json.serialize(record));

        fieldNames.add('Id');
        for(String fieldName : fieldNames) {
            recordMap.remove(fieldName);
        }

        return (SObject)Json.deserialize(Json.serialize(recordMap), SObject.class);
    }

    private List<VIPForm__VIP_Category__c> relateCategories(Map<String, String> categoriesToRelateByUuid) {
        List<VIPForm__VIP_Category__c> categoriesToRelate = new List<VIPForm__VIP_Category__c>();
        for(String categoryUuid : categoriesToRelateByUuid.keySet()) {
            String parentCategoryUuid = categoriesToRelateByUuid.get(categoryUuid);
            categoriesToRelate.add(new VIPForm__VIP_Category__c(
                Uuid__c                             = categoryUuid,
                VIPForm__Parent_VIP_Form_Section__r = new VIPForm__VIP_Category__c(Uuid__c = parentCategoryUuid)
            ));
        }

        return categoriesToRelate;
    }

}