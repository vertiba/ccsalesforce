/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
* @Business: Test class for WaiverExpirationDateUpdated
* 
* @Author: Publicis.Sapient
*
*/
@isTest
private class WaiverExpirationDateUpdatedTest { 
    
    @testSetup
    private static void createTestData(){
        
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        
        Property__c bppProperty = TestDataUtility.getBPPProperty();
        bppProperty.Account__c = businessAccount.Id;
        insert bppProperty;
        
        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        rollYear.Status__c = 'Roll Open';
        insert rollYear;
        
        RollYear__c closedRollYear = new RollYear__c();
        closedRollYear.Name = String.valueof([Select Name from RollYear__c where Status__c ='Roll Closed' ORDER BY Year__c DESC]);
        closedRollYear.Year__c = String.valueof([Select Name from RollYear__c where Status__c ='Roll Closed' ORDER BY Year__c DESC]);
        closedRollYear.Status__c = 'Roll Closed';
        insert closedRollYear;
        
        List<Case> cases = new List<Case>();
        Case cas = new Case();
        cas.AccountId = bppProperty.account__c;
        cas.Property__C = bppProperty.id;
        cas.MailingCountry__c = 'US';
        cas.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        cas.StatuteOfLimitations__c = 48;
        cas.Billable__c = 'No';
        cas.Type='Escape';
        cas.AdjustmentType__c = 'New';
        cas.Status = 'In Progress';
        cas.Roll__c = closedRollYear.Id;
        cas.WaiverExpirationDate__c = System.today()+2;
        cas.IntegrationStatus__c = Null;
        cas.CreatedDate = System.today();
        cases.add(cas);
        
        Case cs = new Case();
        cs.AccountId = bppProperty.account__c;
        cs.Property__C = bppProperty.id;
        cs.MailingCountry__c = 'US';
        cs.EventDate__c = Date.newInstance(2016, 01, 01);
        cs.StatuteOfLimitations__c = 48;
        cs.Billable__c = 'Yes';
        cs.Type='Escape';
        cs.AdjustmentType__c = 'New';
        cs.Status = 'New';
        cs.Roll__c = closedRollYear.Id;
        cs.IntegrationStatus__c = Null;
        cs.CreatedDate = System.today();
        cases.add(cs);
        
        insert cases;
    } 
    @isTest
    private static void testForWaiverExpirationDateGreaterThanCurrentDate(){
        List<Case> cases =  [Select Id, Status, Type, WaiverExpirationDate__c, Billable__c, StatuteOfLimitations__c, IntegrationStatus__c,CreatedDate 
                             FROM Case 
                             WHERE Type ='Escape'
                             AND Status = 'In Progress'
                             AND Billable__c != Null];
        
        Test.startTest();
        WaiverExpirationDateUpdated waiverExpirationDateUpdated = new WaiverExpirationDateUpdated();
        Id batchId = Database.executeBatch(waiverExpirationDateUpdated);
        Test.stopTest();
        
        List<Case> actualCases =  [Select Id, Status, Type, WaiverExpirationDate__c, Billable__c, StatuteOfLimitations__c, IntegrationStatus__c,CreatedDate 
                                   FROM Case 
                                   WHERE Type ='Escape'
                                   AND Status = 'In Progress'
                                   AND Billable__c != Null]; 
        System.assertEquals('Yes',actualCases[0].Billable__c);   
    } 
    
    @isTest
    private static void testForWaiverExpirationDateLessThanCurrentDate(){
        List<Case> cs = new List<Case>();
        List<Case> cases =  [Select Id, Status, Type, WaiverExpirationDate__c, Billable__c, StatuteOfLimitations__c, IntegrationStatus__c,CreatedDate 
                             FROM Case 
                             WHERE Type ='Escape'
                             AND Status = 'In Progress'
                             AND Billable__c != Null];
        
        for(Case cas : cases ){
            cas.WaiverExpirationDate__c = System.today()-2;
            cas.Billable__c = 'Yes';
            
            cs.add(cas);
        } 
        TestDataUtility.disableAutomationCustomSettings(true);
        update cases;
        TestDataUtility.disableAutomationCustomSettings(true);
        
        Test.startTest();
        WaiverExpirationDateUpdated waiverExpirationDateUpdated = new WaiverExpirationDateUpdated();
        Id batchId = Database.executeBatch(waiverExpirationDateUpdated);
        Test.stopTest();
        
        List<Case> actualCases =  [Select Id, Status, Type, WaiverExpirationDate__c, Billable__c, StatuteOfLimitations__c, IntegrationStatus__c,CreatedDate 
                                   FROM Case 
                                   WHERE Type ='Escape'
                                   AND Status = 'In Progress'
                                   AND Billable__c != Null]; 
        System.assertEquals('No',actualCases[0].Billable__c);   
    } 
    @isTest
    private static void testForWaiverExpirationDateNull(){
        List<Case> cs = new List<Case>();
        List<Case> cases =  [Select Id, Status, Type, WaiverExpirationDate__c, Billable__c, StatuteOfLimitations__c, IntegrationStatus__c,CreatedDate 
                             FROM Case 
                             WHERE Type ='Escape'
                             AND Status = 'New'
                             AND Billable__c != Null];
        
        Test.startTest();
        WaiverExpirationDateUpdated waiverExpirationDateUpdated = new WaiverExpirationDateUpdated();
        Id batchId = Database.executeBatch(waiverExpirationDateUpdated);
        Test.stopTest();
        
        List<Case> actualCases =  [Select Id, Status, Type, WaiverExpirationDate__c, Billable__c, StatuteOfLimitations__c, IntegrationStatus__c,CreatedDate 
                                   FROM Case 
                                   WHERE Type ='Escape'
                                   AND Status = 'New'
                                   AND Billable__c != Null]; 
        System.assertEquals('No',actualCases[0].Billable__c);   
    }
}