/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name : BatchableRetryJob
 * Generic Apex Job for retrying failed scopes/chunks from other Batch Apex jobs
 */
public class BatchableRetryJob implements Database.Batchable<BatchableError>, BatchableErrorHandler{
    
    private List<BatchableError> jobScopes;
	
    public BatchableRetryJob(List<BatchableError> jobScopes){
        this.jobScopes = jobScopes;
    }

    public BatchableRetryJob(){
        // Retry framework requires default constructor
    }

    public List<BatchableError> start(Database.BatchableContext context){
        return jobScopes;
    }
    
    public void execute(Database.BatchableContext context, List<BatchableError> scope){
        List<Id> scopeErrorIds = new List<Id>();
        SObjectType sObjectType;
        List<SObject> retryScope = new List<SObject>();
        Set<Id> AsyncIds = new Set<Id>();
        // One batch scope here represents an entire previously failed batch scope
        BatchableError error = scope[0];
        AsyncIds.add(error.AsyncApexJobId);
        
        // Creating a Map of Async Ids, and BatchApexLog__c records
        Map<Id,BatchApexLog__c> batchApexMap = BatchApexErrorEvents.checkBatchApexContext(AsyncIds);
        system.debug('BatchableRetryJob batchApexMap-->'+batchApexMap);
        
        if(error.JobScope != null){
            scopeErrorIds = error.JobScope.split(',');
        }
        
        system.debug('BatchableRetryJob scopeErrorIds-->'+scopeErrorIds);
        if(!scopeErrorIds.isEmpty()){
            sObjectType = scopeErrorIds[0].getSobjectType();    
            for(Id errorId : scopeErrorIds) {
                retryScope.add(sObjectType.newSObject(errorId));
            }
        }
        
        // Invoke the execute method on the original batch apex job class
        Type apexJobClassType = Type.forName(error.ApexClassName);
        
        //Creating a New BatchableContext Instance
        String asyncContext = JSON.serializePretty(new Map<String,Object>{'jobId' => error.AsyncApexJobId});
        Database.Batchable<SObject> batchJob;
    	Database.BatchableContext contextInstance = (Database.BatchableContext)
            										JSON.deserialize(asyncContext, Database.BatchableContextImpl.class);
        
        
        if(batchApexMap.containsKey(error.AsyncApexJobId)){
            if(batchApexMap.get(error.AsyncApexJobId).AreBatchParamsPresent__c == true){
                Map<String, Object> variableMap = (Map<String, Object>)JSON.deserializeUntyped(
                    batchApexMap.get(error.AsyncApexJobId).QueryParams__c);
                batchJob = (Database.Batchable<sObject>) JSON.deserialize(JSON.serialize(variableMap), apexJobClassType);
                batchJob.execute(context,retryScope);
            }else{
                try{
                    batchJob = (Database.Batchable<SObject>) apexJobClassType.newInstance();
                    batchJob.execute(context, retryScope);
                }catch(Exception exceptionInstance){
                    throw exceptionInstance;
                }
            }
        }else{
            try{
                batchJob = (Database.Batchable<SObject>) apexJobClassType.newInstance();
                batchJob.execute(context, retryScope);   
            }catch(Exception exceptionInstance){
                throw exceptionInstance;
            }
            system.debug('BatchApexLog__c record was not created in the Batch. Retry Not possible without that.');
        }
        
        if(Test.isRunningTest()){
            batchJob = (Database.Batchable<SObject>) apexJobClassType.newInstance();
            batchJob.execute(context, retryScope);
        }
    }
    
    public void finish(Database.BatchableContext context){
       //this.batchJob.finish(this.contextInstance);
    }
    
    public void handleErrors(BatchableError error){}
    
    public static Id run(Id retryJobId) {
        // Query log and errors for the job to retry
        List<BatchApexErrorLog__c> errorLogs = [select (select AsyncApexJobId__c, BatchApexErrorLog__c, DoesExceedJobScopeMaxLength__c,ExceptionType__c, 
                                                                 JobApexClass__c, JobScope__c, Message__c, RequestId__c, StackTrace__c 
                                                                 from BatchApexErrors__r) 
                                                         from BatchApexErrorLog__c
                                                         where JobId__c = : retryJobId ];
        
        // Each scope represents a failed scope to retry
        List<BatchableError> batchableErrors = new List<BatchableError>();
        for(BatchApexError__c error : errorLogs[0].BatchApexErrors__r) {
            batchableErrors.add(BatchableError.newInstance(error));
        }
        
        // Delete the logs (new ones will be produced if this retry fails again)
        database.delete(errorLogs);
        return Database.executeBatch(new BatchableRetryJob(batchableErrors), 1);
    }

    public class BatchableRetryJobException extends Exception {}
    
}