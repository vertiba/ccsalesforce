/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for GenerateBillVesselAssesment APEX Controller which has been used on GenerateDirectBillVesselAssesment Lightning Component
 *
 * @Author: Publicis.Sapient
 *
 * ModifiedBy           ModifiedDate   Description
 * Publicis.Sapient     2020-05-20     Initial development
*/


@isTest
public class GenerateBillVesselAssesmentTest {

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    static void createTestData() {
       String currentFiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        String lastFiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
		
        List<RollYear__c> rollYears = new List<RollYear__c>();
        
        RollYear__c rollYear1 = TestDataUtility.buildRollYear(currentFiscalYear,currentFiscalYear,CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYear1.Threshold__c            = 10;
        rollYear1.IsLowValueVesselAssessmentgenerator__c = true;
        rollYears.add(rollYear1);
        
        RollYear__c rollYear2 = TestDataUtility.buildRollYear(lastFiscalYear,lastFiscalYear,CCSFConstants.ROLLYEAR.ROLL_YEAR_Close_STATUS);
        rollYear2.Threshold__c            = 10;
        rollYear2.IsLowValueVesselAssessmentgenerator__c = true;
        rollYears.add(rollYear2);
        insert rollYears;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to Varify if batch is running with admin
    //----------------------------- 
    @isTest
    static void testGenerateBillVesselForSysAdmin() {
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c = :CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS and Year__c = :String.valueof(FiscalYearUtility.getCurrentFiscalYear())];
		rollYear.IsLowValueVesselAssessmentgenerator__c = true;
        update rollYear;
        
        Test.startTest();
        GenerateBillVesselAssesment.Response response;
        GenerateBillVesselAssesment.Response response1;

        System.runAs(systemAdmin) {
            response = GenerateBillVesselAssesment.checkPermission(rollYear.Id);
            response1= GenerateBillVesselAssesment.runDirectBillVesselAssessmentBulkGenerator();
        }
        Test.stopTest();

        // Assert the functionality users with System Administrator profile
        System.assertEquals(true, response.isSuccess);
        system.assertEquals(true, response1.isSuccess);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to Varify if batch is running with nonadmin
    //----------------------------- 
    @isTest
    static void testGenerateBillForNonAdmin() {
        User nonAdmin = TestDataUtility.getBPPAuditorUser();
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c = 'Roll Open'];
        Test.startTest();
        GenerateBillVesselAssesment.Response response;
        System.runAs(nonAdmin) {
            response = GenerateBillVesselAssesment.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality users without System Administrator profile
        System.assertEquals(false, response.isSuccess);
        System.assertEquals(CCSFConstants.BUTTONMSG.NO_PERMISSION, response.message);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to Varify if batch is running for Close roll year
    //----------------------------- 
    @isTest
    static void testGenerateBillForCloseRollYear() {
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c = :CCSFConstants.ROLLYEAR.ROLL_YEAR_Close_STATUS];

        Test.startTest();
        GenerateBillVesselAssesment.Response response;
        GenerateBillVesselAssesment.Response response1;

        System.runAs(systemAdmin) {
            response = GenerateBillVesselAssesment.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e for roll closed
        System.assertEquals(false, response.isSuccess);
        System.assertEquals(CCSFConstants.BUTTONMSG.CLOSED_ROLL_YEAR_BUTTON_WARNING, response.message);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to Varify if batch is running again after the batch is already ran for the same roll year
    //----------------------------- 
    @isTest
    static void testGenerateBillVesselIFBatchAlreadyRan() {
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c = :CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS];
        rollYear.IsDirectBillVesselAssessmentProcess__c = true;
        rollYear.IsLowValueVesselAssessmentgenerator__c = true;
        update rollYear;

        Test.startTest();
        
        GenerateBillVesselAssesment.Response response;
        
        System.runAs(systemAdmin) {
            response = GenerateBillVesselAssesment.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e users with System Administrator profile with IsDirectBillVesselAssessmentProcess true
        System.assertEquals(false, response.isSuccess);
        System.assertEquals(CCSFConstants.BUTTONMSG.DIRECTBILLVESSEL_ASSESMENT_ALREADY_GENERATED_MSG, response.message);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to Varify if batch is running before the low value batch completion
    //----------------------------- 
    @isTest
    static void testGenerateBillVesselBeforeCompletingLowValueBatchRun() {
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        RollYear__c rollYear = [SELECT Id FROM RollYear__c where Status__c = :CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS];
        rollYear.IsLowValueVesselAssessmentgenerator__c = false;
		update rollYear;
        Test.startTest();
        
        GenerateBillVesselAssesment.Response response;
        
        System.runAs(systemAdmin) {
            response = GenerateBillVesselAssesment.checkPermission(rollYear.Id);
        }
        Test.stopTest();

        // Assert the functionality i.e users with System Administrator profile with IsLowValueVesselAssessmentgenerator false
        System.assertEquals(false, response.isSuccess);
        System.assertEquals(CCSFConstants.BUTTONMSG.DIRECTBILLVESSEL_ASSESMENT_GENERATOR_WARNING, response.message);
    }
}