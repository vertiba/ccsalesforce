/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// ASR-2366 
// @description : This class is used to .
//-----------------------------
public with sharing class PotentialMatchController {
    public static final String  ORIGINLOCATION= CCSFConstants.AUMENTUM_PROCESS.AUMENTUM_POTENTIAL_MATCH_LOCATION ;
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String
    // @description : This method takes String as ParentId and Returns its Childs details in Wrapper
    // @return : List<ResultWrapper>
    //-----------------------------
    @AuraEnabled(cacheable=true)
    public static List<ResultWrapper> getDataForSelection(String parentRecordId){
        List<ResultWrapper> recordsForDisplay = new List<ResultWrapper>();
        List<PotentialMatchForBusinessAndLocation__c> potentialMatchRecords=[Select Id,ParentAumentumBusiness__c,PotentialMatchBusiness__c, PotentialMatchLocation__c, PotentialMatchBusiness__r.BusinessAccountNumber__c,PotentialMatchBusiness__r.Name,PotentialMatchBusiness__r.FederalId__c,PotentialMatchLocation__r.LocationIdentificationNumber__c ,PotentialMatchLocation__r.DoingBusinessAs__c ,PotentialMatchLocation__r.LocationAddress__c,PotentialMatchLocation__r.Account__r.Name
                                                                             FROM PotentialMatchForBusinessAndLocation__c
                                                                             WHERE ParentAumentumBusiness__c =: parentRecordId];
        If(potentialMatchRecords.isEmpty()){return null;}        
        for(PotentialMatchForBusinessAndLocation__c match:potentialMatchRecords){
            ResultWrapper result= new ResultWrapper();
            result.matchId=match.Id;  
            result.parentId= match.ParentAumentumBusiness__c;      
            if(match.PotentialMatchLocation__c != null){
                result.objectName='Property';
                result.fieldBANOrLIN=match.PotentialMatchLocation__r.LocationIdentificationNumber__c ;
                result.fieldNAMEOrDBA=match.PotentialMatchLocation__r.DoingBusinessAs__c ;
                result.fieldFEINOrAddress=match.PotentialMatchLocation__r.LocationAddress__c;
                result.objectId=match.PotentialMatchLocation__c;  
                result.propertyAccountName= match.PotentialMatchLocation__r.Account__r.Name;             
            }else if(match.PotentialMatchBusiness__c != null){
                result.objectName='Account';
                result.fieldBANOrLIN=match.PotentialMatchBusiness__r.BusinessAccountNumber__c ;
                result.fieldNAMEOrDBA=match.PotentialMatchBusiness__r.Name ;
                result.fieldFEINOrAddress=match.PotentialMatchBusiness__r.FederalId__c;
                result.objectId=match.PotentialMatchBusiness__c;    
                result.propertyAccountName='';           
            }
            recordsForDisplay.add(result);
        }   
        return recordsForDisplay;
    }
    
    // Wrapper for datatable to display in UI
    public class ResultWrapper{
        @AuraEnabled
        public String matchId{get;set;}
        @AuraEnabled
        public String objectName{get;set;}
        @AuraEnabled
        public String fieldFEINOrAddress{get;set;}
        @AuraEnabled
        public String fieldBANOrLIN{get;set;}
        @AuraEnabled
        public String fieldNAMEOrDBA{get;set;}
        @AuraEnabled
        public Id objectId{get;set;}  
        @AuraEnabled
        public String propertyAccountName{get;set;} 
        @AuraEnabled
        public Id parentId{get;set;}   
    }
    
    //-----------------------------
    // @author : Publicis Sapient 
    // @param : String
    // @description : This method takes JSON string which contains new changes and update Account,Property and Potential 
    // @return : none
    //-----------------------------
    @AuraEnabled
    public static void updateNewChanges(String newChanges,String parentId){
        
        List<object> recordsForChanges= (List<object>)JSON.deserializeUntyped(newChanges); 
        Map<String,Object> fieldInformation = new Map<String,Object>();
        Property__c property= new Property__c();
        Map<Id,Id> stageAndAccountId = new Map<Id,Id>();
        Map<Id,Id> stageAndPropertyId = new Map<Id,Id>();
        Map<Id,StagingAumentumBusiness__c> stageToUpdate = new Map<Id,StagingAumentumBusiness__c>();
        try{
            for(Object record: recordsForChanges){
                fieldInformation = (Map<string,Object>)record;    
                if(fieldInformation.get('ObjectName') =='Property'){                    
                    // Selected ObjectId is of Property                  
                    stageAndPropertyId.put(parentId, String.valueOf(fieldInformation.get('ObjectId')));                   
                }else{
                    // Selected ObjectId is of Account Type                   
                    stageAndAccountId.put(parentId, String.valueOf(fieldInformation.get('ObjectId')));                   
                }
            }
            
            if(!stageAndPropertyId.isEmpty() && stageAndPropertyId.containsKey(parentId)){
                // Getting Property details from It's Id
                property =[Select Id, Account__c from Property__c where Id =: stageAndPropertyId.get(parentId)];
            }
            StagingAumentumBusiness__c aumentumRecord = [Select Id,Status__c,isLocked__c,MappedAccount__c,MappedProperty__c, BusinessEndDate__c, LocationEndDate__c, TransientOccupancyTax__c, TransientOccupancyTaxSmallOperator__c, Lessor__c
                                                        from StagingAumentumBusiness__c 
                                                        where Id =: parentId];
            if(aumentumRecord.Status__c != CCSFConstants.AUMENTUM_STATUS_PROCESSED){
                // If Status is not Processed then only allow Update
                if(stageAndAccountId.containsKey(parentId) )   {
                    aumentumRecord.MappedAccount__c=stageAndAccountId.get(parentId);
                    if(aumentumRecord.MappedProperty__c == null){
                        aumentumRecord.Status__c= CCSFConstants.AUMENTUM_PROCESS.STATUS_WARNING;
                        // Bug - ASR-10441
                        aumentumRecord.Messages__c = CCSFConstants.AUMENTUM_PROCESS.MESSAGE_CREATE_PROPERTY;
                        aumentumRecord.isLocked__c = false;
                    }                    
                } 
                if(stageAndPropertyId.containsKey(parentId) )   {
                    // if property is selected, its Account will be populated
                    // Bug- ASR:10441 - If Property selected then Review Child record should 
                    // be created for User to Update Smart

                    aumentumRecord.MappedProperty__c = stageAndPropertyId.get(parentId);
                    aumentumRecord.MappedAccount__c = property.Account__c;
                    AumentumWrapper aumentumWrap = createReviewChild(aumentumRecord, stageAndPropertyId.get(parentId));
                    if(aumentumWrap != null){
                        aumentumRecord.Status__c = aumentumWrap.status;
                        aumentumRecord.Messages__c = aumentumWrap.message;
                        if(aumentumWrap.message == CCSFConstants.AUMENTUM_STATUS_PROCESSED ){
                            // When Status is Processed record should be locked.
                            aumentumRecord.isLocked__c = true;
                        }
                    }
                }  
                update aumentumRecord;
            }
        } catch(Exception ex){            
            Logger.addExceptionEntry(ex, ORIGINLOCATION);
            Logger.saveLog();
            throw new AuraException(' Contact ASR- '+ex.getMessage());
        }
    } 

    //-----------------------------
    // @author : Publicis Sapient 
    // @param : StagingAumentumBusiness__c aumentumRecord
    // @param : String PropertyId
    // @description : This method takes StagingAumentumBusiness__c record and PropertyId and return 
    // Wrapper - with Status and Message (Status = Processed, Ignore, In Review or Failed) depends 
    // on various conditions
    // @return : AumentumWrapper
    // ASR - 10441
    //-----------------------------
    
    public static AumentumWrapper createReviewChild(StagingAumentumBusiness__c aumentumRecord, String mapPropertyId){
      
        AumentumWrapper aumentumWrap = new AumentumWrapper();
        List<StagingBusinessCompareField__c> reviewRecords = new List<StagingBusinessCompareField__c>();        
        Map<String,List<String>> insertResults = new Map<String,List<String>>();

        Property__c mapProperty = [Select Id,Account__c,Account__r.BusinessCloseDate__c,BusinessLocationCloseDate__c,PrimaryFormType__c, 
                                    Account__r.BusinessStatus__c, Status__c
                                    FROM Property__c
                                    WHERE Id =: mapPropertyId 
                                    LIMIT 1];
        String primaryFormType = AumentumDataProccess.populatePropertyPrimaryFormType(aumentumRecord);        
        if(mapProperty.Account__r.BusinessStatus__c == CCSFConstants.PROPERTY.STATUS_INACTIVE || mapProperty.Status__c == CCSFConstants.PROPERTY.STATUS_INACTIVE){
            // When Property or Account in SMART is INACTIVE
            // Status should be Ignored
            aumentumWrap.status = CCSFConstants.AUMENTUM_PROCESS.STATUS_IGNORE;
            aumentumWrap.message = CCSFConstants.AUMENTUM_PROCESS.POTENTIAL_MESSAGE_FOR_IGNORE;
        }else if(!AumentumDataProccess.isDifferenceInRecords(mapProperty,aumentumRecord,primaryFormType)){
            // Property and Account both are Active
            // The Aumentum Records and SMART details has no Difference
            // Status should be Processed
            aumentumWrap.status = CCSFConstants.AUMENTUM_STATUS_PROCESSED;
            aumentumWrap.message = CCSFConstants.AUMENTUM_PROCESS.POTENTIAL_MESSAGE_FOR_PROCESSED;            
        }else{
            // Property and Account both are Active
            // The Aumentum Records and SMART details has Difference
            // Status should be In Review
            // Review Child should be get created
            reviewRecords.add(AumentumDataProccess.populateCompareReviewRecord(aumentumRecord.Id,mapProperty.Account__c,mapProperty.Id,primaryFormType));
            if(!reviewRecords.isEmpty()){
                // StagingBusinessCompareField__c is added in List and Its not Empty
                // Inserting StagingBusinessCompareField__c child record
                insertResults = AumentumDataProccess.insertRecords(reviewRecords, CCSFConstants.AUMENTUM_PROCESS.REVIEW_OBJECT_API_NAME);
                if(!insertResults.isEmpty()){
                    // Error occured During Insert of StagingBusinessCompareField__c record
                    if(insertResults.containsKey(aumentumRecord.Id)){
                        // DML Failure happened throwing Exception                    
                     throw new AuraException( CCSFConstants.AUMENTUM_PROCESS.MESSAGE_DML_ERROR + ' of Review Record '+ insertResults.get(aumentumRecord.Id));             
                    }
                }else{
                     // No Error Occured while inserting record 
                     aumentumWrap.status = CCSFConstants.AUMENTUM_PROCESS.STATUS_REVIEW;
                     aumentumWrap.message = CCSFConstants.AUMENTUM_PROCESS.POTENTIAL_MESSAGE_FOR_IN_REVIEW;                    
                }
               
            }          
        }
        return aumentumWrap;       
    }

     //-----------------------------
    // @author : Publicis Sapient 
    // @param : StagingAumentumBusiness__c aumentumRecord
    // @param : String PropertyId
    // @description : Wrapper class used to retun Message or Status from createReviewChild
    // @return : AumentumWrapper
    // ASR - 10441
    //-----------------------------

    public class AumentumWrapper{

        public String message{get;set;}
        public String status{get;set;}     

    }

}