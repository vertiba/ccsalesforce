/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class RecordChangeValidator {

    private SObject updatedRecord, originalRecord;

    public RecordChangeValidator(SObject updatedRecord, SObject originalRecord) {
        this.updatedRecord  = updatedRecord;
        this.originalRecord = originalRecord;
    }

    public Boolean hasFieldChanged(Schema.SObjectField field) {
        return this.hasFieldChanged(field.getDescribe().getName());
    }

    public Boolean hasFieldChanged(String fieldName) {
        return updatedRecord.get(fieldName) != originalRecord.get(fieldName);
    }

    public Boolean hasAnyFieldChanged(Schema.FieldSet fieldSet) {
        List<String> fieldNames = new List<String>();
        for(Schema.FieldSetMember fieldSetMember : fieldSet.getFields()) {
            fieldNames.add(fieldSetMember.getFieldPath());
        }

        return this.hasAnyFieldChanged(fieldNames);
    }

    public Boolean hasAnyFieldChanged(List<Schema.SObjectField> fields) {
        List<String> fieldNames = new List<String>();
        for(Schema.SObjectField field : fields) {
            fieldNames.add(field.getDescribe().getName());
        }

        return this.hasAnyFieldChanged(fieldNames);
    }

    public Boolean hasAnyFieldChanged(List<String> fieldNames) {
        if(fieldNames.isEmpty()) return false;

        Boolean anyFieldChanged;

        for(String fieldName : fieldNames) {
            anyFieldChanged = this.hasFieldChanged(fieldName);
            if(anyFieldChanged) break;
        }

        return anyFieldChanged;
    }

}