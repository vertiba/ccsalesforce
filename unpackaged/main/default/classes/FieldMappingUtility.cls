/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Utility class to apply data transformation logic to field values when mapping data from a staging object
//      into a domain object (e.g., StagingAumentumBusiness__c object is used to update Account & Property__c records).
//      This utility class is used to read the settings configured in custom metadata StagingFieldMappingValue__mdt and apply the mapped values
//      Known limitations:
//          1. This currently only supports static values (defined in custom metadata). It does not currently support dynamic logic, field references, etc.
//          2. This currently is only being used for string replacements. Other data types (dates, datetimes, etc.) may need additional logic added to be properly supported.
//-----------------------------
public without sharing class FieldMappingUtility {

    private static List<StagingFieldMappingValue__mdt> stagingFieldMappingValues;

    static {
        // Cache the list of custom metadata to reduce CPU usage
        stagingFieldMappingValues = [
            SELECT Id, StagingSObject__r.DeveloperName, StagingField__r.DeveloperName, StagingFieldValue__c, MappedRecordLookupField__r.DeveloperName, MappedField__r.DeveloperName, MappedFieldValue__c
            FROM StagingFieldMappingValue__mdt
        ];
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : stagingRecord The record stored in the staging object, populated with data from an external sytem that is used to update data in a domain object.
    // @param : mappedRecordLookupField The lookup field on the staging object that references the mapped record being updated
    // @param : mappedRecord The related record in the domain object that is being updated, based on the data from an external system
    // @description : After any mapping & transformation logic has already been supplied (in separated Apex classes), this method can then be called to apply
    //      value mappings that automatically update the field values on the mappedRecord supplied, based on any matching rules defined in StagingFieldMappingValue__mdt.
    //      For example, Aumentum supplies 'C-CORP' for business structure, so the value is updated to 'Corporation' on mappedRecord's field, based on a configured rule.
    // @return : void
    //-----------------------------
    public static void mapFieldValues(SObject stagingRecord, Schema.SObjectField mappedRecordLookupField, SObject mappedRecord) {      

        // Loop through all of the fields on the staging record to see what incoming data is being processed
        // The loop then compares the data on the mapped record + reads any matching rules defined in StagingFieldMappingValue__mdt
        for(String stagingRecordFieldName : stagingRecord.getPopulatedFieldsAsMap().keySet()) {
           
            // Parent object relationships (represented as another object) are included in getPopulatedFieldsAsMap()
            // Filter them out since we are only looking at fields directly on the staging object (not related objects)
            if(stagingRecordFieldName.endsWith('__r')) continue;

            Object stagingRecordFieldValue = stagingRecord.get(stagingRecordFieldName);
           
            // Skip processing if there's no data in the staging record's field
            if(stagingRecordFieldValue == null) continue;

            // See if any rules have been configured for the staging object + lookup field + staging value
            StagingFieldMappingValue__mdt fieldMappingSetting = getFieldMapping(stagingRecord, mappedRecordLookupField, stagingRecordFieldName, String.valueOf(stagingRecordFieldValue));
           
            // Skip processing this field if we can't find any rules
            if(fieldMappingSetting == null) continue;

            // When using metadata relationship fields, Salesforce removes the '__c' for custom objects & fields, so add them back
            String mappedRecordFieldName = fieldMappingSetting.MappedField__r.DeveloperName + '__c';
           
            // If the mapped record doesn't have the mapped field, then we didn't reference that field, so skip the rule
            if(mappedRecord.getPopulatedFieldsAsMap().containsKey(mappedRecordFieldName) == false) continue;

            // If the staging field & mapped field aren't the same, then we don't need to run the mapping rule
            if(stagingRecord.get(stagingRecordFieldName) != mappedRecord.get(mappedRecordFieldName)) continue;

            // Finally, we can update the value on the mapped record
            mappedRecord.put(mappedRecordFieldName, fieldMappingSetting.MappedFieldValue__c);
        }
    }

    private static StagingFieldMappingValue__mdt getFieldMapping(SObject stagingRecord, Schema.SObjectField mappedRecordLookupField, String stagingRecordFieldName, String stagingFieldValue) {
       

        // When using metadata relationship fields, Salesforce removes the '__c' for custom objects & fields
        String stagingSObjectName          = String.valueOf(stagingRecord.getSObjectType()).replace('__c', '');
        String mappedRecordLookupFieldName = String.valueOf(mappedRecordLookupField).replace('__c', '');
       

        // Loop through the custom metadata rules to see if there is a matching rules
        for(StagingFieldMappingValue__mdt stagingFieldMappingValue : stagingFieldMappingValues) {
            if(stagingFieldMappingValue.StagingSObject__r.DeveloperName != stagingSObjectName) continue;
            if(stagingFieldMappingValue.StagingField__r.DeveloperName != stagingRecordFieldName.replace('__c', '')) continue;
            if(stagingFieldMappingValue.StagingFieldValue__c != stagingFieldValue) continue;
            if(stagingFieldMappingValue.MappedRecordLookupField__r.DeveloperName != mappedRecordLookupFieldName) continue;

            return stagingFieldMappingValue;
        }

        // No matching rules were found, so return null
        return null;
    }

}