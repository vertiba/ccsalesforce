/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class StatementNewFormController {
    
    public static string STATEMENTAPI = 'Statement__c';
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns the list view of Statement Object
    // @return : List<ListView>
    //-----------------------------
    
    @AuraEnabled
    public static List<ListView> getListViews() {
        List<ListView> listviews = [SELECT Id, Name FROM ListView WHERE SobjectType =: STATEMENTAPI];
        return listviews;
    }

    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns the default recordtype for the logged in user of Statement Object. Will be used for logged In user
    // @return : Id -  recordtype Id
    //-----------------------------
    
    @AuraEnabled
    public static Id getDefaultRecordType() {
      
      List<Schema.RecordTypeInfo> statementRecordTypes = Schema.SObjectType.Statement__c.RecordTypeInfos;
      Id defaultRecordTypeId;

     
      for (Schema.RecordTypeInfo recordType : statementRecordTypes) {
          if (recordType.DefaultRecordTypeMapping) {
            defaultRecordTypeId = recordType.RecordTypeId;
          }
      }
      return defaultRecordTypeId;
    }
  
     //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns the statement record along with RecordType.Developername of statement record. 
    // @return : sObject
    //-----------------------------
    
    @AuraEnabled
    public static Statement__c getStatementRecordType(Id statementId) {
        return [SELECT Id, Name, RecordType.Developername FROM Statement__c WHERE Id = :statementId];
    }
}