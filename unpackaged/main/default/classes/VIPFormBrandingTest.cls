@isTest
private class VIPFormBrandingTest {

    @isTest
    static void it_should_set_uuid() {
        VIPForm__VIP_Form_Branding__c formBranding = new VIPForm__VIP_Form_Branding__c(
            Name = 'test'
        );
        insert formBranding;

        formBranding = [SELECT Id, Name, Uuid__c FROM VIPForm__VIP_Form_Branding__c WHERE Id = :formBranding.Id];
        System.assert(Uuid.isValid(formBranding.Uuid__c));
    }

}