/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//                                                                                                                       
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######  
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##    
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##    
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##    
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##    
//                                                                                                                       
//=======================================================================================================================



/*************************************************
 *      CONTENTVERSIONREPORTCONTROLLERTEST       *
 * TEST CLASS FOR CONTENTVERSIONREPORTCONTROLLER *
 *************************************************/


@isTest
private class ContentVersionReportControllerTest {
    
    @isTest
    private static void should_GetEmptyListOfContentVersion_When_NoRelevantDataExists(){

        List<ContentVersion> result;
        Test.startTest();
            result = ContentVersionReportController.getAllContentVersionRecords('','');
        Test.stopTest();
        System.assert(result.isEmpty());
    }

    @isTest
    private static void should_GetEmptyListOfContentVersion_When_NoRelevantDataExists1(){

        List<ContentVersion> result;
        Test.startTest();
            result = ContentVersionReportController.getAllContentVersionRecords('ContentVersion','');
        Test.stopTest();
        System.assert(result.isEmpty());
    }

    @isTest
    private static void should_GetEmptyListOfContentVersion_When_NoRelevantDataExists2(){

        List<ContentVersion> result;
        Test.startTest();
            result = ContentVersionReportController.getAllContentVersionRecords('ContentVersion','Track_Repro_Delivery');
        Test.stopTest();
        System.assert(result.isEmpty());
    }

    @isTest
    private static void should_GetListOfContentVersion_When_RelevantDataExists(){
        Contentversion contentVersion   = new Contentversion();
        contentVersion.Title            = 'ABC';
        contentVersion.PathOnClient     = 'test';
        contentVersion.VersionData      = EncodingUtil.base64Decode(EncodingUtil.urlEncode('Unit Test Attachment Body','UTF-8'));
        insert contentVersion;

        List<ContentVersion> result;
        Test.startTest();
            result = ContentVersionReportController.getAllContentVersionRecords('ContentVersion','Track_Repro_Delivery');
        Test.stopTest();
        System.assert(!result.isEmpty());
        System.assertEquals('ABC', result[0].Title);
    }

    @isTest
    private static void should_GetListOfFieldSetMembers_When_RelevantDataExists(){

        List<FieldSetUtility.FieldSetMember> result;
        Test.startTest();
            result = ContentVersionReportController.getFieldSetFields('ContentVersion','Track_Repro_Delivery');
        Test.stopTest();
        System.assert(!result.isEmpty());
    }

}