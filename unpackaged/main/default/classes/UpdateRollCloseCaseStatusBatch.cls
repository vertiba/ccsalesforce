/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR-5669. This Batch class is used to Update assessment Integration staus based on various criteria
// This class will run only when roll year close.
//-----------------------------
public class UpdateRollCloseCaseStatusBatch implements Database.Batchable<sObject>,Database.Stateful, BatchableErrorHandler {
    
    private String originLocation;
    private String currentClosingRollYear;
    private boolean processSuccess;
    private RollYear__c rollYear;
    
    public UpdateRollCloseCaseStatusBatch(RollYear__c roll){
        this.rollYear = roll;
        this.currentClosingRollYear = roll.Year__c;
        originLocation = CCSFConstants.UPDATE_ROLLCLOSE_CASE_STATUS_BATCH_NAME;
        processSuccess = true;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method gets all Assessment (Case) records which follow the below criteria.
    // @return : Database.QueryLocator (List of Cases)
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        //Build dynamic query for batch
        String query='SELECT id, RollCode__c, IntegrationStatus__c,Billable__c ';
        query += ' from Case where  isAssessment__c = true';//IsClosed = true and
        query += ' AND Status = \''+CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED+'\'';
        query += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';
        query += ' AND SubStatus__c = \''+CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED+'\'';
        query += ' and AdjustmentType__c = \''+CCSFConstants.ASSESSMENT.STATUS_NEW+'\'';
        query += ' and IntegrationStatus__c != \''+CCSFConstants.ASSESSMENT.SENT_TO_TTX+'\'';
        query += ' and IntegrationStatus__c != \''+CCSFConstants.ASSESSMENT.IN_TRANSIT_TO_TTX+'\'';
        query += ' and IntegrationStatus__c != \''+CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_REJECTED_BY_TTX+'\'';
        query += ' and IntegrationStatus__c != \''+CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_SENT_TO_RP+'\'';
        query += ' and IntegrationStatus__c != \''+CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_IN_TRANSIT_TO_RP+'\'';
        query += ' and IntegrationStatus__c != \''+CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_REJECTED_BY_RP+'\'';
        query += ' AND (SubType__c = \'\'';
        query += ' OR SubType__c = null)';
        query += ' and RollYear__c = :currentClosingRollYear';
        Logger.addDebugEntry(query+'- start method query',originLocation);
        Logger.saveLog();
        return database.getQueryLocator(query);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : CasesList (List of Cases)
    // @description : This method update fields for all the Assessment (Case) records which
    //          are returned by start() method of the Batch class 
    // @return : void
    public void execute(Database.BatchableContext bc, List<Case> cases) {
        
        try {
            
            for(Case inputCase : cases){
                // ASR-10634 - If Billable is marked as No, status is Not Eligible to Send
                if(inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO){
                    inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                }else{
                    if(inputCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED){
                        inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_RP;
                    }else{
                        inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX;
                    } 
                }
                
            }
            
            boolean isSuccess = updateAssessments(cases);
            if(isSuccess==false)
            {
                processSuccess=false;
            }
            Logger.addDebugEntry('isSuccess'+String.valueof(isSuccess),originLocation);
        }
        catch(Exception ex) {
            processSuccess = false;
            Logger.addExceptionEntry(ex, originLocation);
            throw ex;
        }
        Logger.saveLog();
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method updates the roll year fields record that the Reopen Annual batch has been completed.
    // @return : void
    public void finish(Database.BatchableContext bc) {
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'rollYear' => this.rollYear});                                
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, UpdateRollCloseCaseStatusBatch.class.getName(), asyncList[0].ParentJobId, true, queryParams, asyncList[0].NumberOfErrors);
        
        if(processSuccess){
            Database.executeBatch(new AnnualRollBatchSync(rollYear)); 
        }
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method update assessments
    // @return : return boolean value based on the success/fail result
    //-----------------------------
    private Boolean updateAssessments(List<Case> cases) {
        
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        //update assessments
        Map<String,Object> updateResultMap = DescribeUtility.updateRecords(cases,dmlOptions);
        return (Boolean)updateResultMap.get('updateSuccess');
    }
}