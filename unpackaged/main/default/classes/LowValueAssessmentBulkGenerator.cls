/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This Batch class is used to get all the Property related Assessments (Cases)
//                with Assesment Line Items with Notice Type as Low Value from the previous years and clone the Assesment with its Line Items.
//-----------------------------
public class LowValueAssessmentBulkGenerator implements Database.Batchable<sObject>, Database.Stateful, BatchableErrorHandler {
    
    public String originLocation ='LowValueAssessmentBulkGenerator';
    private Integer lastRollYear = FiscalYearUtility.getCurrentFiscalYear() - 1;
    private Integer currentYear = lastRollYear + 1;
    private final String CASE_QUERY;
    private boolean cloneSuccess = true;
    private Id assessmentLegacyRecordTypeId;
	private Id assessmentBPPRecordTypeId;
    private Id propertyBPPRecordTypeId;
    private List<Property__c> propertiesToInclude = new List<Property__c>();
    
    public LowValueAssessmentBulkGenerator(){
        assessmentLegacyRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME);
		assessmentBPPRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
        propertyBPPRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);
        CASE_QUERY = buildQueryToFetchAllDetailsOfCases();
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        String  batchQuery;
        batchQuery = 'SELECT Id, Name,(SELECT Id, RecordType.DeveloperName, Type FROM Cases__r WHERE IsAssessment__c = true and AssessmentYear__c = \''+ String.valueof(lastRollYear)+'\') FROM Property__c';
        batchQuery += 'WHERE Account__r.BusinessStatus__c = \'Active\' ';
        batchQuery += 'AND Account__r.NoticeType__c = \'Low Value\' ';
        batchQuery += 'AND Status__c = \'Active\' ';
        batchQuery += 'AND Id NOT IN (SELECT Property__c FROM Case WHERE IsAssessment__c = true AND AssessmentYear__c =\''+ String.valueof(currentYear)+'\') ';
        
        Logger.addDebugEntry(batchQuery,originLocation+'- start method query');
        Logger.saveLog();
        
        return database.getQueryLocator([SELECT Id, Name,(SELECT Id, RecordType.DeveloperName, Type FROM Cases__r WHERE IsAssessment__c = true and AssessmentYear__c =: String.valueof(lastRollYear))FROM Property__c
                                         WHERE Account__r.BusinessStatus__c = 'Active'
                                         AND Account__r.NoticeType__c = 'Low Value'
                                         AND Status__c = 'Active'
                                         AND RecordtypeId =: propertyBPPRecordTypeId 
                                         AND Id NOT IN (SELECT Property__c FROM Case WHERE IsAssessment__c = true AND AssessmentYear__c =: String.valueof(currentYear))
                                        ]);
    }
    
    public void execute(Database.BatchableContext bc, List<Property__c> matchingProperties){
        List<Property__c> propertiesToProcess = new List<Property__c>();
		RollYear__c currentRollYear = getRollYearDetails(String.valueOf(currentYear));
        String rollYear = currentRollYear.Id;
        if(!matchingProperties.isEmpty()){
            for(Property__c property : matchingProperties) {
                List<Case> assessments = property.Cases__r;
                if(assessments.size() != 1) continue;
                if(assessments[0].Type != 'Regular') continue;
                propertiesToInclude.add(property);
                propertiesToProcess.add(property);
            }  
        }
        Set<Id> caseIds = new Set<Id>();
        if(!propertiesToProcess.isEmpty()){
            for(Case caseRecord : [SELECT Id, Property__c 
                                   FROM Case 
                                   WHERE Property__c In :propertiesToProcess 
                                   AND AssessmentYear__c =: String.valueof(lastRollYear)]){
                                       
                                       caseIds.add(caseRecord.Id);
                                   }  
        }
        
        // Build query to get all fields of Case and Assessment Line Items
        String query = CASE_QUERY + ' WHERE Id IN: caseIds';
        
        // Execute query and fetch all cases to be cloned
        Map<Id,Case> casesToBeClonedById= new Map<Id,Case>((List<Case>)Database.query(query));
        // Create new list of cloned cases
        List<Case> clonedCases = new List<Case>();
        
        // Create a map of original Case Id(Source Id) to cloned Assessment Line Items
        Map<String, List<AssessmentLineItem__c>> clonedCaseToAssessmentLIBySourceId = new Map<String, List<AssessmentLineItem__c>>();
    	Date eventDate = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
        if(!casesToBeClonedById.isEmpty()){                
            for(Case caseToClone: casesToBeClonedById.values() ){
                //  Clone the case, perform the deep clone here
                Case newCase = caseToClone.clone(false, true, false, false);
                newCase.Status = 'In Progress';
                newCase.SubStatus__c= null;

                    //ASR-8544 Changes starts
                    newCase.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;                    
                    newCase.SubType__c = CCSFConstants.ASSESSMENT.SUB_TYPE_LOWVALUE;
                    //ASR-8544 Changes ends
                    //Addedd AutoProcessed__c per bug ASR-7853 Clone assessment from Auto Proccessed should not be auto Processed 
                    newCase.AutoProcessed__c = false;
                	//ASR-9701. Enrolled Date should be blank after cloning an assessment
            		newCase.EnrolledDate__c = null;
                    // ASR-8548
                    newCase.CancelAttempted__c = false;
                	// ASR-10029 -Updating Event Date as 1/1/Calendar Current Year i.e.,Current Fiscal Year
                    //newCase.EventDate__c = system.today();
                    newCase.EventDate__c = eventDate;
					newCase.AssessmentYear__c = String.valueOf(FiscalYearUtility.getFiscalYear(newCase.EventDate__c));
                    newCase.AccountNumber__c = null;
                    newCase.AssessmentNumberSequence__c = null;
                    newCase.AssessmentNumber__c = null;
					newCase.RollYear__c = String.valueOf(currentYear);
                    newCase.Roll__c = rollYear;
                    
					newCase.IntegrationStatus__c = null;
                    newCase.IsLocked__c = false;
                    //Make the Penalty fields to null
                    newCase.Penalty__c = null;
                    newCase.WaivePenaltyReason__c =null;
                    newCase.ApplyFraudPenalty__c=false;
                    newCase.PenaltyPercent__c =0;
                    
                    //Make all the Exemption Fields to null
                    newCase.ExemptionPenalty__c = null;
                    newCase.ExemptAmountManual__c= null;
                    newCase.PercentExempt__c= null;
                    newCase.ExemptionCode__c= null;
                    newCase.ExemptionDate__c= null;
                    newCase.ExemptionType__c= null;
                    newCase.ExemptionSubtype__c= null;
                    newCase.ExemptionReviewed__c = false;
                    newCase.ExemptAmountManual__c = null;
                    newCase.PercentExempt__c = null;
                    newCase.Exemption__c = null;

                // As part of 9070 Fix
                newCase.ClosedDate = null;
                newCase.Status = CCSFConstants.ASSESSMENT.STATUS_CLOSED;
                newCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                newCase.IsLocked__c = true;
                
                // as part of ASR 10048	
                newCase.IsCorrectedOrCancelled__c = false;

                //ASR-10768 Changes
                newCase.AdjustmentReason__c = null;
                
                //call utility method to clone legacy assessment
                if(newCase.RecordTypeId == assessmentLegacyRecordTypeId) {
                    newCase = LegacyDataUtility.convertAssessment(newCase,assessmentBPPRecordTypeId);
                }
                
                clonedCases.add(newCase);
                if(!caseToClone.AssessmentLineItems__r.isEmpty()) {
                    List<AssessmentLineItem__c> clonedLineItems = new List<AssessmentLineItem__c>();
                    for(AssessmentLineItem__c lineItemToClone : caseToClone.AssessmentLineItems__r) {
                        AssessmentLineItem__c newAssessmentLineItem = lineItemToClone.clone(false, true, false, false);
                        newAssessmentLineItem.DepreciationFactor__c = null;
                        newAssessmentLineItem.isLegacy__c= false;
                        clonedLineItems.add(newAssessmentLineItem) ; 
                    }
                    clonedCaseToAssessmentLIBySourceId.put(newCase.getCloneSourceId(), clonedLineItems);
                }
            }   
        }
        
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        AsyncDlrsCalculator.getInstance().setQueueToRun(false);
        List<Database.SaveResult> insertResults, insertChildResults;
        // insert cloned Cases
        if(!clonedCases.isEmpty()){
            insertResults = Database.insert(clonedCases, dmlOptions); 
        }
        
        // insert cloned AssessmentListItems
        // but before update Case__c(lookup) with newly cloned Case Id's on Assessment Line Item
        List<AssessmentLineItem__c> clonedAssessmentLItems = new List<AssessmentLineItem__c>();
        if(!clonedCases.isEmpty()){
            for(Case clonedCase : clonedCases) {
                String sourceCaseId = clonedCase.getCloneSourceId();
                if(clonedCaseToAssessmentLIBySourceId.containsKey(sourceCaseId)) {
                    for(AssessmentLineItem__c lineItem : clonedCaseToAssessmentLIBySourceId.get(sourceCaseId)) {
                        lineItem.Case__c = clonedCase.Id;
                        clonedAssessmentLItems.add(lineItem);
                    }
                }
            }  
        }

        if(!clonedAssessmentLItems.isEmpty()){
            insertChildResults = Database.insert(clonedAssessmentLItems, dmlOptions);
        }
        AsyncDlrsCalculator.getInstance().setQueueToRun(true);
    }

    public void finish(Database.BatchableContext bc){
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncApexJobs = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                            TotalJobItems, CreatedBy.Email,ParentJobId
                                            FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, LowValueAssessmentBulkGenerator.class.getName(), asyncApexJobs[0].ParentJobId, 
                                           false,'', asyncApexJobs[0].NumberOfErrors);

        RollYear__c rollYear = [select id,IsLowValueBulkGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1 ];         
        
        /*Update the roll year IsLowValueBulkGeneratorProcessed checkbox to true,
        if the batch is successful and there are no failed records.*/
        if(cloneSuccess && rollYear !=null) {
            rollYear.IsLowValueBulkGeneratorProcessed__c = true;
            update rollYear;
        }

        String strBody = '';
        User u = [select email from user where Id=:userinfo.getuserid()];
        String[] toAddresses = new String[]{u.Email};
            List<AsyncApexJob> apexJobs=[SELECT ApexClassId, ApexClass.Name,Id,JobItemsProcessed,JobType,NumberOfErrors,ExtendedStatus,TotalJobItems,Status FROM AsyncApexJob WHERE ApexClass.Name= 'LowValueAssessmentBulkGenerator' and CompletedDate =Today ];
        Set<Id> batchJobIds = new Set<Id>();
        if(!apexJobs.isEmpty()){
            strBody = '<html><body><table>';
            for(AsyncApexJob objAsyncJob:apexJobs ){
                batchJobIds.add(objAsyncJob.Id);
                strBody += '<tr><td>ID</td><td>' + objAsyncJob.Id + '</td></tr><tr><td>ApexClassId</td><td>' + objAsyncJob.ApexClassId + '</td></tr><tr><td>JobsProcessed</td><td>' + objAsyncJob.JobItemsProcessed + '</td></tr><tr><td>JobType</td><td>' + objAsyncJob.JobType + '</td></tr><tr><td>NumberOfErrors</td><td>' + objAsyncJob.NumberOfErrors + '</td></tr><tr><td>Error</td><td>' +objAsyncJob.ExtendedStatus + '</td></tr><tr><td>TotalJobItems</td><td>' + objAsyncJob.TotalJobItems + '</td></tr><tr><td>Status</td><td>' + objAsyncJob.Status + '</td></tr>';
            }
            strBody += '</table></body></html>';
        }

        if(!batchJobIds.isEmpty()){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setSubject('LowValueAssessmentBulkGenerator Status');
            mail.setHtmlBody(strBody);
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {mail};
                if(!Test.isRunningTest()){
                  Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    if (results[0].success) {
                        System.debug('The email was sent successfully.');
                    } 
                    else {
                        System.debug('The email failed to send: ' + results[0].errors[0].message);
                    }   
                }      
        }
              
        String recordProcessed;
        if(!propertiesToInclude.isEmpty()){
            recordProcessed = String.valueOf(propertiesToInclude);
        }else{
            recordProcessed ='None';
        }      
        Logger.addDebugEntry('Property processed-'+recordProcessed,originLocation);
        Logger.saveLog();
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
    
    // Building query to include all fields of Case and AssessmentLineItem__c Dynamically 
    public static String buildQueryToFetchAllDetailsOfCases(){
        String queryTemplate ='Select {0},(Select {1} from AssessmentLineItems__r) FROM Case';
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        
        // 1 - Get all Case object fields
        Set<String> caseFieldsSet = globalDescribe.get('Case').getDescribe().fields.getMap().keyset();
        String caseFields = String.join(new List<String>(caseFieldsSet), ',');
        
        // 2 - Get all AssessmentLineItem fields
        Set<String> assessLineItemFieldsSet = globalDescribe.get('AssessmentLineItem__c').getDescribe().fields.getMap().keyset();
        String lineItemFields = String.join(new List<String>(assessLineItemFieldsSet), ',');
        
        return String.format(queryTemplate, new List<String>{caseFields,lineItemFields} );
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns Roll_Year__c object record for requested year
    // @return : roll year record
    //-----------------------------
    private static RollYear__c getRollYearDetails(String year) {
        
        RollYear__c[] rollYear = [SELECT Id,Name,Status__c,IsLowValueBulkGeneratorProcessed__c
                                  FROM RollYear__c WHERE Year__c =: year  limit 1];
        //check query result and return first returned result's id
        if(rollYear.size()>0)
        {
            return rollYear[0];
        }
        return null;
    }
}