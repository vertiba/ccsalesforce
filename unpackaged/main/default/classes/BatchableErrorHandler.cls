/**
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Interface Name : BatchableErrorHandler
 * Description: Implement this interface to allow the framework to pass 
 *   			back errors to your Batch Apex class for handling and retry. This interface
 *   			also extends the RaisesPlatformEvents interface ensuring that error events are sent
 **/
public interface BatchableErrorHandler extends Database.RaisesPlatformEvents {
	/*  
	 * Update related records that errors have occured
	 */ 
    void handleErrors(BatchableError error);
}