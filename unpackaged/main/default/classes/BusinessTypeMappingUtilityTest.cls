/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: BusinessTypeMappingUtilityTest 
 * Description : Test Class for BusinessTypeMappingUtility
*/
@isTest
public class BusinessTypeMappingUtilityTest {

    public static TestMethod void businessTypeTest(){
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='AdminUser1',alias='tuse3r4',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testuser14@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            BusinessTypeMapping__c businessMapping = new BusinessTypeMapping__c();
            businessMapping.BusinessType__c = 'Biotech';
            businessMapping.BusinessSubtype__c = 'Bakery';
            businessMapping.AssetClassification__c = 'ATMs';
            businessMapping.DefaultAssetSubclassification__c = 'Artwork';
            database.insert(businessMapping);
            
            Test.startTest();
            BusinessTypeMappingUtility.getBusinessTypeAssetMappingsByCompositeKey('Biotech-Bakery-ATMs');
            Test.stopTest();
        }
    }
}