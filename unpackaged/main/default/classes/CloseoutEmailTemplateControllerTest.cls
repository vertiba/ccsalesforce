/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// Batch ->Test class for  CloseoutEmailTemplateController Apex class

@isTest
public class CloseoutEmailTemplateControllerTest {
    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method to create Testdata.
    // @return : void
    //-----------------------------

    @testSetup
    private static void dataSetup(){

        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin); 
        User officeAssistant = TestDataUtility.getOfficeAssistantUser();
        users.add(officeAssistant);
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
          
            insert penalty1;
            
            Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            Id businessPPPropertyRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Business Personal Property').getRecordTypeId();
            Id bPPAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('BPP Assessment').getRecordTypeId();
            Account account = TestDataUtility.buildAccount('Escape Closeout Test Account','Notice to File',businessAccountRecordTypeId);
            insert account;
            
            List<Property__c> properties = new List<Property__c>();
            Property__c propertyWithNoBusinessOpendate = TestDataUtility.buildProperty('TestProperty1',businessPPPropertyRecordTypeId);
            Property__c propertyWithBusinessOpendate = TestDataUtility.buildProperty('TestProperty2',businessPPPropertyRecordTypeId);
            propertyWithBusinessOPendate.BusinessLocationOpenDate__c = Date.newInstance(system.today().year() -2, 01,01);
			
            propertyWithNoBusinessOPendate.Account__c = account.id;
            propertyWithBusinessOPendate.Account__c = account.id;
            properties.add(propertyWithNoBusinessOPendate);
            properties.add(propertyWithBusinessOPendate);

            insert properties;
            
            String currentFiscalyear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            String previousFiscalyear = String.valueOf(Integer.valueOf(currentFiscalyear)-1);
            
            Test.startTest();
                RollYear__c closedRollyear = TestDataUtility.buildRollYear(previousFiscalyear,previousFiscalyear,'Roll Closed');
                insert closedRollyear;
           
                Date eventDate1 = Date.newInstance(integer.valueof(closedRollyear.Year__c), 01, 01);
                RollYear__c rollYear2 = TestDataUtility.buildRollYear(currentFiscalyear,currentFiscalyear,'Roll Open');
                insert rollYear2;
                List<Case> caseList = new List<Case>(); 
                Case priorYearAssessment = TestDataUtility.buildAssessment(bPPAssessmentRecordTypeId,propertyWithNoBusinessOpendate.Id,closedRollyear.id,closedRollyear.Year__c,
                                                                           closedRollyear.Year__c,'Escape',48, 'Notice to File',null,eventDate1);
                priorYearAssessment.AdjustmentType__c = 'New';
                caseList.add(priorYearAssessment);
                Date eventDate2 = Date.newInstance(integer.valueof(rollYear2.Year__c), 01, 01);
                Case currentYearAssessment = TestDataUtility.buildAssessment(bPPAssessmentRecordTypeId,propertyWithNoBusinessOpendate.Id,rollYear2.id,rollYear2.Year__c,
                                                                            rollYear2.Year__c,'Regular',48, 'Notice to File',null,eventDate2);
                                                                        
                currentYearAssessment.subtype__c = 'Closeout';
                currentYearAssessment.AdjustmentType__c = 'New';
                caseList.add(currentYearAssessment);
                insert caseList;
    
                FactorSource__c factorSource = new FactorSource__c(name = 'Commercial CAA Factor Source');
                insert factorSource;
                BusinessAssetMapping__c businessAssetMapping = TestDataUtility.getBusinessAssetMapping('Machinery & Equipment','General',String.valueOf(factorSource.id),20.00,'Depreciation Factor');
                insert businessAssetMapping;
                Factor__c factor = TestDataUtility.getfactor(previousFiscalyear,rollYear2.Year__c,factorSource.Id,95.00,4);
                insert factor;
                
                List<AssessmentLIneItem__c> lineItems = new List<AssessmentLIneItem__c>();
                AssessmentLIneItem__c lineItem = TestDataUtility.buildAssessmentLineItem('Equipment','Machinery & Equipment',previousFiscalyear,1000.00,currentYearAssessment.id,'Description');
                
                lineItems.add(lineItem);
                
                String previousToPreviousYear = String.valueOf(Integer.valueOf(currentFiscalyear)-2);
                AssessmentLIneItem__c lineItem2 = TestDataUtility.buildAssessmentLineItem('Equipment','Machinery & Equipment',previousToPreviousYear,1000.00,currentYearAssessment.id,'Description 1');
                
                lineItems.add(lineItem2);
                insert  lineItems;
                
                TriggerHandlerSettings__c settings = TriggerHandlerSettings__c.getOrgDefaults();
                settings.DisableDLRSCalculationTriggers__c = true;
                upsert settings TriggerHandlerSettings__c.Id;
            Test.stopTest();
        }
    }


    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is used to email data for Tax liability for the escapes closeout.
    // @return : void
    //-----------------------------

    @isTest
    private static void getEmailDataTest() { 

        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,FinalCloseOutValue__c,integrationstatus__c ,RecordTypeId from Case Where subtype__c =: 'Closeout'];
        cs.FinalCloseOutValue__c =5000;
       
        

       CloseoutEmailTemplateController emailController = new CloseoutEmailTemplateController();
       emailController.assessmentId = cs.Id;
        System.runAs(sysAdminUser){
            Test.startTest();
            	update cs;
                CreateCloseOutEscapesController extension = new CreateCloseOutEscapesController(new ApexPages.standardController(cs));
                extension.redirect();
     
                CloseoutEmailTemplateController.DataWrapper wrapperData =  emailController.getdata(); 
            Test.stopTest();
           
            System.assert(wrapperData.emailTaxLiableWrapperList.size() == 1 , 'Escape Closeouts Tax Liable Data is retrived successfully');   
            List<Case> escapeCloseouts = [SELECT id,AssessmentYear__c,TotalAssessedValue__c,TotalPersonalPropertyValueOverride__c,(Select id,Acquisitionyear__c from AssessmentLineItems__r) FROM Case WHERE type =: 'Escape' AND subtype__c =: 'Closeout' ];
            System.assertEquals(escapeCloseouts[0].AssessmentYear__c, wrapperData.emailTaxLiableWrapperList[0].assessmentYear);
            System.assertEquals(escapeCloseouts[0].TotalAssessedValue__c, wrapperData.emailTaxLiableWrapperList[0].escapeAssessedValue);
            System.assert(wrapperData.emailTaxLiableWrapperList[0].escapeAssessedValue > Decimal.valueOf(Label.LowValueAmount),'Total Assessed Value is greater than Default Low Value Amount hence Tax Liable Assessment');
    	}
    }

}