/** Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d **/
/**
* @author Publicis.Sapient
*
* @description Class to test DocumentGeneratorServiceScheduler
**/
@isTest
private class DocumentGeneratorServiceSchedulerTest {

    @TestSetup
    static void setupData() {
        insert DocumentGeneratorServiceSettings__c.getInstance();
    }

    @isTest
    private static void should_returnJobNamePrefix() {
        String expectedJobNamePrefix = 'Document Generator Service';
        String actualJobNamePrefix   = new DocumentGeneratorServiceScheduler().getJobNamePrefix();

        System.assertEquals(expectedJobNamePrefix, actualJobNamePrefix);
    }

    @isTest
    private static void should_scheduleJob() {
        EnhancedSchedulable schedulableClass = new DocumentGeneratorServiceScheduler();

        Test.startTest();

        String jobId = System.schedule(schedulableClass.getJobNamePrefix(), TestDataFactory.getSampleCronExpression(), schedulableClass);
        System.debug('jobId' + jobId);

        Test.stopTest();

        Integer numberOfScheduledJobs = [SELECT COUNT() FROM CronTrigger WHERE Id = :jobId];
        System.assertEquals(1, numberOfScheduledJobs, 'A job should be scheduled');
    }

}