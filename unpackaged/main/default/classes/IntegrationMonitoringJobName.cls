/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class IntegrationMonitoringJobName extends VisualEditor.DynamicPickList{

    public override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow value1 = new VisualEditor.DataRow('None', '');
        return value1;
    }
    public override VisualEditor.DynamicPickListRows getValues() {
        return DescribeUtility.getDynPickListRows('IntegrationMonitoring__mdt','JobName__c');
    }
}