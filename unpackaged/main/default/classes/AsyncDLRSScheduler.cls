/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : schedulable class used to execute dlrs rules. 
// 		Created to avoid queuable limitations
//-----------------------------
public class AsyncDLRSScheduler implements Schedulable {
    private AsyncDlrsCalculator chainedInstance;
    
    /**
     * @description : overwritten method for schedulable interface, that triggers
     * 		the queueable job that triggers DLRS rule execution.
     * @param : SchedulableContext
     * @return : void
     */
    public void execute(SchedulableContext sc) {
        System.enqueueJob(chainedInstance);
        // Abort the job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
    /**
     * Public constructor
     */
    public AsyncDLRSScheduler(AsyncDlrsCalculator instance){
        this.chainedInstance = instance;
    }
}