/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
global class GenerateBillAssesment {

    @AuraEnabled
    public static Response generateBill(Id recordId) {
        Response response = new Response();
        Boolean hasPermission = FeatureManagement.checkPermission(System.Label.MassGenerateDirectBillAssessments);
        RollYear__c rollYear = [Select Status__c,IsLowValueBulkGeneratorProcessed__c,IsDirectBillBulkGeneratorProcessed__c from RollYear__c where id =: recordId];

        //Check if roll year is not closed, if closed return error message
        if (rollYear.Status__c == 'Roll Closed'){
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.CLOSED_ROLL_YEAR_BUTTON_WARNING;
            return response;
        }else if(!rollYear.IsLowValueBulkGeneratorProcessed__c) {
		//check if low value batch already ran, if not ran return error message
			response.isSuccess = false;
			response.message = CCSFConstants.BUTTONMSG.DIRECT_BILL_ASSESMENT_GENERATOR_WARNING;
		}else if(rollYear.IsDirectBillBulkGeneratorProcessed__c) {
		//check if direct bill batch has not already ran, if ran return error message
			response.isSuccess = false;
			response.message = CCSFConstants.BUTTONMSG.DIRECT_BILL_ASSESMENT_ALREADY_GENERATED_MSG;
		}else if(!hasPermission) {
        //check if user has Custom Permission to run this batch, if not return error message
            response.isSuccess = false;
            response.message = System.Label.NoPermission;
            return response;
        }else {
		//if all the above condtions are false, means user can run this batch
            response.isSuccess = true;
            response.message = System.Label.HasPermissionDirectBill;
        }

        return response;
    }

    @AuraEnabled
    public static Response runDirectBillAssessmentGenerator(){
         Response response = new Response();
        DirectBillAssessmentGenerator obj = new DirectBillAssessmentGenerator();
        Id jobId = Database.executeBatch(obj,Integer.valueOf(Label.DirectBillAssesmentGeneratorBatchSize));
        AsyncApexJob apexJobs = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email, ExtendedStatus
                          from AsyncApexJob where Id = :jobId];
        if(apexJobs.Status != 'Failed'){
            response.isSuccess = true;
            response.message= System.Label.BatchQueued + apexJobs.Status;
        }else{
            response.isSuccess = false;
            response.message = System.Label.BatchErrors+apexJobs.NumberOfErrors;
        }

        return response;
    }
    public class Response {
        @AuraEnabled
        public Boolean isSuccess;

        @AuraEnabled
        public String message;

        public Response() {
            isSuccess = false;
            message = '';
        }
    }
}