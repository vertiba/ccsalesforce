/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class MassApprovalController {

    private List<sObject> selectedListViewRecords;
    private List<sObject> allRecordsAsPerFilter;
    private List<sObject> recordsForProcessing = new List<sObject>();
    private String listviewFilterId;
    private String fixedQuery;
    private Integer limitForRecords;
    private String sobjectName;
    private RestApi.ListViewResult listView;
    private Boolean hasPermission;
    public String recordStatus {get;set;}

    public MassApprovalController(ApexPages.StandardSetController controller) {

        this.selectedListViewRecords = (List<sObject>)controller.getSelected();
        this.listviewFilterId		 = controller.getFilterId();
        // Getting SObject Name
        this.sobjectName 			= DescribeUtility.getObjectName(getObjectNameFromListView(listviewFilterId));
        this.limitForRecords 		= !String.isBlank(CCSFConstants.MASSAPPROVAL.MASS_APPROVAL_LIMITS_CONFIGED)?Integer.valueOf(CCSFConstants.MASSAPPROVAL.MASS_APPROVAL_LIMITS_CONFIGED):Integer.valueOf(CCSFConstants.MASSAPPROVAL.MASS_APPROVAL_LIMITS_HARDCODED);
        this.fixedQuery 			= this.getQuery(sobjectName,this.getListView().query);
        // Fetching all the records in selected List view
        if(this.selectedListViewRecords.isEmpty()){
            this.allRecordsAsPerFilter = database.query( fixedQuery +' LIMIT '+ limitForRecords+1);
        }
        this.hasPermission = FeatureManagement.checkPermission(CCSFConstants.MASSAPPROVAL.MASS_APPROVAL_PERMISSION);
    }

    public void approveActionFunction(){
        if(!hasPermission){
            recordStatus = CCSFConstants.BUTTONMSG.NO_PERMISSION;
            return;
        }

        if(!selectedListViewRecords.isEmpty()){recordsForProcessing.addAll(selectedListViewRecords);} // If records are selected
        else if(selectedListViewRecords.isEmpty() && !allRecordsAsPerFilter.isEmpty()){recordsForProcessing.addAll(allRecordsAsPerFilter);} //If none is selected in ListView

        if(recordsForProcessing.isEmpty()){
            recordStatus = CCSFConstants.MASSAPPROVAL.NO_RECORDS;
            return;
        }
        if(Test.isRunningTest()){
            if(selectedListViewRecords.isEmpty()){limitForRecords = 1;}
        }
        if(recordsForProcessing.size() < limitForRecords){
            // Based on Limit calling Batch
            //Record updates depends on the sObject Type

            if(sobjectName == 'DocumentGenerationRequest__c'){
                recordStatus =  MassApprovalUtility.updateGenerateNotices(recordsForProcessing,false);

            }else{
                recordStatus =  callController(recordsForProcessing,false);

            }
        }else{
            recordStatus = callBatch(fixedQuery);
        }
    }

    // to getListView filters
    public RestApi.ListViewResult getListView() {
        if(this.listView != null && this.listView.id == listviewFilterId) return listView;
        RestApi.ListViewResult listView = RestApi.getListView(sobjectName, listviewFilterId);
        this.listView = listView;
        return listView;
    }

    // to get sObject Name from Selected List View
    public static String getObjectNameFromListView(String listViewFilterId){
        return [SELECT SobjectType FROM ListView WHERE Id =: listViewFilterId ].sObjectType;
    }

    // to get Query String
    public String getQuery(String sobjectName, string listViewQuery){
        String queryPrefix ='SELECT Id FROM '+ sobjectName;
        listViewQuery = queryPrefix + listViewQuery.substringAfter('FROM '+ sobjectName);
        return listViewQuery;
    }

    // Calling Mass Approval Utility for approving records real-time
    public static String callController(List<sObject> recordsForProcessing, Boolean callingFromBatch){
        return MassApprovalUtility.approvalProcess(recordsForProcessing, callingFromBatch);
    }
    // Calling Batch if records are more than Limit
    public static String callBatch(String queryfix){
        String response = CCSFConstants.MASSAPPROVAL.BATCH_RUNNING_MSG;
        Database.executeBatch(new MassApprovalBatch(queryfix));
        return response;
    }

}