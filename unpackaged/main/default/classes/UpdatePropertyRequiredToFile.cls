/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Batch class to set Required To File field on All the Properties of an Account
//-----------------------------
public class UpdatePropertyRequiredToFile implements Database.Batchable<sObject>,Database.Stateful, BatchableErrorHandler {
    
    private String originLocation;
    private String currentRollYear;
    private String lastRollYear;    
    private Date lienDate;
    private Id propertyBPPRecordTypeId;
    private Id propertyVesselRecordTypeId;
    private Id propertyLeasedEquipmentRecordTypeId;
    private List<String> errors = new List<String>();
    private boolean processSuccess;
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Constructor method.
    //-----------------------------
    public UpdatePropertyRequiredToFile() {
        originLocation                      = 'UpdatePropertyRequiredToFile';
        currentRollYear 	                = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        lastRollYear 	                    = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);        
        lienDate                            = Date.parse('01/01/'+currentRollYear);
        propertyBPPRecordTypeId             = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);
        propertyVesselRecordTypeId          = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME);
        propertyLeasedEquipmentRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.LEASED_EQUIPMENT_RECORD_TYPE_API_NAME);
        processSuccess = true;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : Database.QueryLocator
    //-----------------------------
    public Database.QueryLocator start(Database.BatchableContext context) {
        String query = 'SELECT Id, TotalValueofProperties__c, TotalDirectBillCost__c,TotalCostofProperties__c, RequiredToFileYear__c ';
        query += ' FROM Account WHERE (BusinessStatus__c = :CCSFConstants.STATUS_ACTIVE AND RecordTypeId =: DescribeUtility.getRecordTypeId(Account.SobjectType,CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME) )';
        query += ' OR RecordTypeId =: DescribeUtility.getRecordTypeId(Account.sObjectType,CCSFConstants.ACCOUNT.PERSON_ACCOUNT_RECORD_TYPE_API_NAME)';
        query += ' ORDER BY LastModifiedDate DESC';

        Logger.addDebugEntry(query, 'UpdatePropertyRequiredToFile-start method' );
        Logger.saveLog();

        return Database.getQueryLocator([SELECT Id, 
                                                TotalValueofProperties__c, 
                                                TotalCostofProperties__c,
                                         		TotalDirectBillCost__c,
                                                RequiredToFileYear__c 
                                        FROM Account
                                        WHERE (BusinessStatus__c = :CCSFConstants.STATUS_ACTIVE 
                                                AND RecordTypeId =: DescribeUtility.getRecordTypeId(Account.SobjectType,CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME)
                                             )
                                            OR RecordTypeId =: DescribeUtility.getRecordTypeId(Account.sObjectType,CCSFConstants.ACCOUNT.PERSON_ACCOUNT_RECORD_TYPE_API_NAME)
                                            ORDER BY LastModifiedDate DESC
                                        ]);   
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : List<sObject> 
    // @description : Validating all the criterias and assigning notice type to account
    // @return : void
    //-----------------------------
    public void execute(Database.BatchableContext context, List<Account> accounts) {

        Set<Id> bppandvesselAccounts = new Set<Id>();
        Set<Id> filteredProperties = new Set<Id>();
        Map<Id,String> accountsByNoticeType = AccountNoticeTypeUtility.getNoticeTypesForAccounts(accounts, false);
        Map<Id,Property__c> propertiesByRequiredToFile = new Map<Id,Property__c>();
        Map<Id,Property__c> activeVesselPropertiesByOwnershipStartDate = new Map<Id,Property__c>();
        
        List<Property__c> properties =  [SELECT Id, 
                                                LastFilingMethod__c, 
                                                RecordTypeId,Account__c, 
                                                Type__c, 
                                                Status__c, 
                                                BusinessLocationOpenDate__c, 
                                                BusinessLocationCloseDate__c, 
                                                InactivationDate__c, 
                                                RequiredToFile__c, 
                                                NotRequiredToFileReason__c, 
                                                PrimaryOwnership__c, 
                                                PrimaryOwnership__r.StartDate__c
                                         FROM Property__c 
                                         WHERE Account__c in :accounts
                                         ORDER BY RecordTypeId];//Ordering to help with branch prediction

        for(Property__c property : properties) {
            //Update all Properties Required to File with some default values based on Last Filing Method 
            propertiesByRequiredToFile.put(property.Id, updatePropertiesDefaultRequiredToFile(property));

            //Update BPP or Leased Equipment Properties Required to File      
            if(property.RecordTypeId == propertyBPPRecordTypeId || property.RecordTypeId == propertyLeasedEquipmentRecordTypeId ) {
                propertiesByRequiredToFile.put(property.Id, identifyBPPRequiredToFileByOpenCloseDate(property, accountsByNoticeType));
                if(property.RequiredToFile__c != CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO) {
                    filteredProperties.add(property.Id);
                }
            }
            
            //Update Vessel Properties Required to File
            if(property.RecordTypeId == propertyVesselRecordTypeId) {
                propertiesByRequiredToFile.put(property.Id, updateVesselPropertiesRequiredToFile(property, accountsByNoticeType));
                if(property.RequiredToFile__c != CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO) {
                    filteredProperties.add(property.Id);
                }
            }
        }  
       
        //Verify if latest annual Assessment for the property is determined as "Not Assessable"
        propertiesByRequiredToFile = overrideRequiredToFileBasedOnPreviousAssessment(filteredProperties, propertiesByRequiredToFile);
        Map<String,Object> updateResultMap = DescribeUtility.getUpdateRecordsResults(propertiesByRequiredToFile.values(), null, originLocation);
        processSuccess = (Boolean) updateResultMap.get('updateSuccess');
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : Batchable interface method.
    // @return : void
    //-----------------------------
    public void finish(Database.BatchableContext context) {
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, UpdatePropertyRequiredToFile.class.getName(), asyncList[0].ParentJobId, false, '', asyncList[0].NumberOfErrors);
        Database.executeBatch(new UpdateAccountNoticeType(),Integer.valueOf(System.Label.UpdatePropertyRequiredToFileAndUpdateAccountNotiiceTypeBatchSize));
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Property record
    // @description : This method updates the required to file of all properties
    // @return : Property record
    //-----------------------------
    public Property__c updatePropertiesDefaultRequiredToFile(Property__c property) {
        //Check Account notice type for BPP or Vessel property and update required to file
        if((property.RecordTypeId == propertyBPPRecordTypeId || property.RecordTypeId == propertyVesselRecordTypeId) && property.LastFilingMethod__c != null ) {            
            /*If property.Last Filing Method not "None"
            AND Property.Record Type = "Business Personal Property", "Vessel"
            THEN Property.Required to File = "Yes"*/
            property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES;
            //Make the not required to reason blank if its already poplulated
            property.NotRequiredToFileReason__c = null;      
        } else if(property.RecordTypeId == propertyLeasedEquipmentRecordTypeId) {
            //Check for Leased property and update required to file
            if (property.Type__c == CCSFConstants.PROPERTY.PROPERTY_TYPE_LESSOR  && property.LastFilingMethod__c != null) {
                /*Else if property.Last Filing Method not "None" AND Property.Type = "Lessor"
                THEN Property.Required to File = "Yes"*/
                property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES;
                //Make the not required to reason blank if its already poplulated and current RequiredToFile__c = Yes
                property.NotRequiredToFileReason__c = null;
            } else if (property.Type__c == CCSFConstants.PROPERTY.PROPERTY_TYPE_LESSEE) {
                /*Else if Property.Type = "Lessee" THEN Property.Required to File = "No"*/
                property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO;
                property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_LEASED_EQUIPMENT;
            }
        }
        return property;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Property record , Map<Id,String> accountsByNoticeType
    // @description : This method is for all Business Personal Property & Leased Equipment properties, to determine business closures
    // @return : bpp or leased property record
    //-----------------------------    
    //
    public Property__c identifyBPPRequiredToFileByOpenCloseDate(Property__c property,Map<Id,String> accountsByNoticeType) {        
        
        if(property.RecordTypeId == propertyLeasedEquipmentRecordTypeId && property.Type__c == CCSFConstants.PROPERTY.PROPERTY_TYPE_LESSEE) {
            property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO;
            property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_LEASED_EQUIPMENT;
            return property;
        }
        
        //Check for currently Inactive Properties
        if(property.Status__c == CCSFConstants.PROPERTY.STATUS_INACTIVE) {
            if(property.BusinessLocationCloseDate__c < lienDate) {
                /*If(property was Active as of Lien-Date, but closed down before Lien date) THEN Property.Required to File = "No"*/
                property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO;
                property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_INACTIVE_ON_LIEN_DATE;
            }         
            else if(property.BusinessLocationCloseDate__c >= lienDate) {
                /*If (property was Active as of Lien-Date, but later closed down) THEN Property.Required to File = "Optional" or "Yes"*/
                if(accountsByNoticeType.containsKey(property.Account__c) && 
                    (accountsByNoticeType.get(property.Account__c) == CCSFConstants.NOTICE_TYPE_LOW_VALUE || 
                    accountsByNoticeType.get(property.Account__c) == CCSFConstants.NOTICE_TYPE_DIRECT_BILL)) {
                    /*Check if the Account of the property is determined as "Direct Bill" or "Low Value"
                    THEN update Property.Required to File = "Optional"*/
                    property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_OPTIONAL;
                } else {                    
                    property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES; 
                }
                //Make the not required to reason blank if its already poplulated and current RequiredToFile__c = Yes
                property.NotRequiredToFileReason__c = null;               
            }
        }
        //Check for currently Active Properties
        if(property.Status__c == CCSFConstants.PROPERTY.STATUS_ACTIVE) {
            if(property.BusinessLocationOpenDate__c > lienDate) {
                /*If (active properties that were opened after current Lien date) THEN Property.Required to File = "No*/
                 property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO;
                 property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_INACTIVE_ON_LIEN_DATE;
            }
            else if(property.BusinessLocationOpenDate__c <= lienDate) {
                /*Else If (active properties that were opened on or before current Lien date) THEN Property.Required to File = "Optional" or "Yes"*/
                if(accountsByNoticeType.containsKey(property.Account__c) && 
                    (accountsByNoticeType.get(property.Account__c) == CCSFConstants.NOTICE_TYPE_LOW_VALUE || 
                     accountsByNoticeType.get(property.Account__c) == CCSFConstants.NOTICE_TYPE_DIRECT_BILL)) {
                    /*Check if the Account of the property is determined as "Direct Bill" or "Low Value"
                    THEN update Property.Required to File = "Optional"*/
                    property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_OPTIONAL;
                } else {                   
                    property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES;
                }
                //Make the not required to reason blank if its already poplulated and current RequiredToFile__c = Yes
                property.NotRequiredToFileReason__c = null;
            } 
        }
        return property;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Property record , Map<Id,String> accountsByNoticeType
    // @description : This method is for Vessels properties to determine, moving out of SF or Sold property.
    // @return : vessel property record
    //-----------------------------    
    public Property__c updateVesselPropertiesRequiredToFile(Property__c property,Map<Id,String> accountsByNoticeType) {
        //Check for currently Inactive Properties
        if(property.Status__c == CCSFConstants.PROPERTY.STATUS_INACTIVE) {
            if(property.InactivationDate__c < lienDate) {
                /*If (property was Active as of Lien-Date, but moved out of SF or Sold before Line Date) THEN Property.Required to File = "No"*/
                property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO;
                property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_INACTIVE_ON_LIEN_DATE;
            } else if(property.InactivationDate__c >= lienDate) {
                /*Else If (property was Active as of Lien-Date, but later moved out of SF or Sold) THEN Property.Required to File = "Yes"*/
                if(accountsByNoticeType.containsKey(property.Account__c) && 
                    (accountsByNoticeType.get(property.Account__c) == CCSFConstants.NOTICE_TYPE_LOW_VALUE || 
                     accountsByNoticeType.get(property.Account__c) == CCSFConstants.NOTICE_TYPE_DIRECT_BILL)) {
                    /*Check if the Account of the property is determined as "Direct Bill" or "Low Value"
                    THEN update Property.Required to File = "Optional"*/
                    property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_OPTIONAL;
                } else { 
                    property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES;
                }
                //Make the not required to reason blank if its already poplulated and current RequiredToFile__c = Yes
                property.NotRequiredToFileReason__c = null;
            }            
        }
        //Check for currently Active Properties
        if(property.Status__c == CCSFConstants.PROPERTY.STATUS_ACTIVE) {
            if(property.PrimaryOwnership__c != null && property.PrimaryOwnership__r.StartDate__c > lienDate) {
                /*If (active vessels that were owned after current Lien date) THEN Property.Required to File = "No"*/
                property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO;
                property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_INACTIVE_ON_LIEN_DATE;
            } else if(property.PrimaryOwnership__c != null && property.PrimaryOwnership__r.StartDate__c <= lienDate) {
                /*Else If (active vessels that were owned on or before current Lien date) THEN Property.Required to File = "Yes"*/
                if(accountsByNoticeType.containsKey(property.Account__c) && 
                    (accountsByNoticeType.get(property.Account__c) == CCSFConstants.NOTICE_TYPE_LOW_VALUE || 
                     accountsByNoticeType.get(property.Account__c) == CCSFConstants.NOTICE_TYPE_DIRECT_BILL)) {
                    /*Check if the Account of the property is determined as "Direct Bill" or "Low Value"
                    THEN update Property.Required to File = "Optional"*/
                    property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_OPTIONAL;
                } else { 
                    property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES;
                }
                //Make the not required to reason blank if its already poplulated and current RequiredToFile__c = Yes
                property.NotRequiredToFileReason__c = null;
            }            
        }
        return property;
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Set<Id> ids of all the required to file - yes properties , Map<Id,Property__c>
    // @description : This method update the property required to file as no , if the there is any latest non-assesable assement exists of that property.
    // @return : Map<Id,Property__c>
    //-----------------------------
    public Map<Id,Property__c> overrideRequiredToFileBasedOnPreviousAssessment(Set<Id> filteredProperties, Map<Id,Property__c> propertiesByRequiredToFile) {
        List<Property__c> properties = [SELECT Id,
                                            RequiredToFile__c,
                                            NotRequiredToFileReason__c,
                                            (SELECT Id,
                                                    SubType__c 
                                            FROM Cases__r 
                                            WHERE Type = :CCSFConstants.ASSESSMENT.TYPE_REGULAR 
                                            AND AdjustmentType__c =: CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW
                                            AND RollYear__c =: lastRollYear and SubType__c in: CCSFConstants.ASSESSMENT.SUB_TYPES_FOR_NOT_REQUIRED_TO_FILE
                                            ORDER BY AssessmentNumber__c DESC, SequenceNumber__c DESC 
                                            LIMIT 1)
                                        FROM Property__c WHERE Id IN :filteredProperties];
        
        for(Property__c property : properties) {
            if(property.Cases__r.Size() == 1) {
                if(property.Cases__r[0].SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_REFERENCE_PROPERTY) {
                    property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_REFERENCE_PROPERTY;
                } else if(property.Cases__r[0].SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_GOVERNMENT_ENTITY) {
                    property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_GOVERNMENT_ENTITY;
                } else if(property.Cases__r[0].SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_FEDERAL_ENCLAVE_PROPERTY) {
                    property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_FEDERAL_ENCLAVE_PROPERTY;
                } else if(property.Cases__r[0].SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_STATE_ASSESSED_PROPERTY) {
                    property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_STATE_ASSESSED_PROPERTY;
                } else if (CCSFConstants.ASSESSMENT.SUB_TYPES_FOR_PROPERTY_REQUIRED_TO_FILE_NON_ASSESSABLE.contains(property.Cases__r[0].SubType__c)) {
                    property.NotRequiredToFileReason__c = CCSFConstants.PROPERTY.NOT_REQUIRED_TO_FILE_REASON_NON_ASSESSABLE;
                }
                property.RequiredToFile__c = CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO;                
                propertiesByRequiredToFile.put(property.Id,property);
            }
        }
        return propertiesByRequiredToFile;
    }

}