/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// 
//-----------------------------
@isTest(seealldata = false)
public with sharing class AmendStatementControllerTest {
    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
    private static Date filingDate          = System.today();
    
    
    @TestSetup
    static void dataSetup(){
        
        TestDataUtility.disableAutomationCustomSettings(true);
        User sysAdmin= TestDataUtility.getSystemAdminUser();
        insert sysAdmin;        
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        System.runAs(systemAdminUser) {   
            User taxpayer = TestDataUtility.getPortalUser();
            
            RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            insert rollYearCurrentFiscalYear;
            
            //Insert Penalty
            Penalty__c penalty463 = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty463;
            
            //insert account
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            //insert property
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            bppProperty.BusinessLocationOpenDate__c= Date.newInstance(2016, 3,4);
            insert bppProperty;            
                   
            //Create Statement         
            Map<String,String> inputParamsStatement = new Map<String,String>();            
            inputParamsStatement.put('accountId',businessAccount.Id);
            inputParamsStatement.put('propertyId',bppProperty.Id);
            inputParamsStatement.put('formType','571-L');
            inputParamsStatement.put('businessScenario','Existing');
            inputParamsStatement.put('propertyScenario','Existing');
            inputParamsStatement.put('assessmentYear',String.valueOf(lastYear)); 
            inputParamsStatement.put('recordTypeId',DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME));
            inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/'+lastToLastYear));
            inputParamsStatement.put('filingDate',String.valueOf('01/03/'+lastYear));            
            Statement__c bppStatement = TestDataUtility.buildStatement(inputParamsStatement);
            bppStatement.FilingMethod__c = 'eFile';
            bppStatement.Status__c = CCSFConstants.STATEMENT.STATUS_SUBMITTED;
            bppStatement.VIP_Date_Time_Submitted__c = System.now();     
            insert bppStatement;
            
            Id statementReportedOwnedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.OWNED_ASSETS_RECORD_TYPE_API_NAME);   
            StatementReportedAsset__c bppStatementReportedAsset = new StatementReportedAsset__c();
            bppStatementReportedAsset = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Alternate Schedule A', 'ATMs', '2017', 100, null, bppStatement.id, 'Statement Reported Asset for last year');
            insert bppStatementReportedAsset;  
            
            //Insert ReportedAssetSchedule
            Map<String,String> inputParamsReportedAssetSch= new Map<String,String>();
            inputParamsReportedAssetSch.put('month','March');
            inputParamsReportedAssetSch.put('year',string.valueOf(lastToLastYear));             
            inputParamsReportedAssetSch.put('cost','30');
            inputParamsReportedAssetSch.put('type','Disposal');
            inputParamsReportedAssetSch.put('statementReportedAssetId',bppStatementReportedAsset.Id);
            inputParamsReportedAssetSch.put('description','Art and craft');
            
            ReportedAssetSchedule__c bppReportAssetSch = TestDataUtility.buildReportedAssetSchedule(inputParamsReportedAssetSch);
            insert bppReportAssetSch;
            test.startTest();
            Statement__c statementToBeCloned =[Select Id,Status__c,AlreadyAmended__c from Statement__c Limit 1];
            statementToBeCloned.Status__c = CCSFConstants.STATEMENT.STATUS_SUBMITTED; 
            statementToBeCloned.VIP_Date_Time_Submitted__c = System.now();     
            update statementToBeCloned;
            test.stopTest();
            TestDataUtility.disableAutomationCustomSettings(false);
            
        }
        
    } 
    /*
* Method Name: amendStatementTest
* Description: Statement is submitted and get amended
*/ 
    @isTest
    static void amendStatementTest(){
        Statement__c statementToBeCloned = [Select Id,Status__c,AlreadyAmended__c,Property__c from Statement__c Limit 1];
                
        Test.startTest();        
        String response = AmendStatementController.validateInProgressStatements(statementToBeCloned.Id);           
        RollYear__c currentRollYear = AmendStatementController.rollYearStatus();        
        Statement__c clonedRecord= AmendStatementController.cloneStatement(String.valueof(statementToBeCloned.Id)); 
        List<Case> relatedCaseforthisStatement = [Select Id, Property__c from Case where Property__c =: statementToBeCloned.Property__c];
        Test.stopTest();
        
        system.assert(currentRollYear != null, 'Returns Current Roll Year');
        system.assert(clonedRecord !=null, 'New record is created');              
    
        List<Statement__c> statements =[Select Id,AlreadyAmended__c,AmendedFrom__c from Statement__c];
        
        system.assert(statements.size() == 2, 'One Clone generated for Amendement');
        system.assertEquals(true, statements[0].AlreadyAmended__c, 'Already Amended true for Orginial Statement');
        system.assertEquals(statementToBeCloned.Id, statements[1].AmendedFrom__c, 'Parent Id in AmendedFrom for Clone');
        system.assertEquals( CCSFConstants.STATEMENT.NO_INPROGRESS_STATEMENTS,response, 'No In progress Property had before');
       // Process Builder Create a Case
        system.assertEquals(1,relatedCaseforthisStatement.size(),'Case is created from BPP statement');
       
    } 
    
    /* 	Method Name : alreadyAmendTest
		Description:  Method to cover if alreadyAmended is True then Clone is not allowed 
	*/
    
    @isTest
    static void alreadyAmendTest(){
         Statement__c statementToBeCloned = [Select Id,Status__c,AlreadyAmended__c from Statement__c Limit 1];
         statementToBeCloned.AlreadyAmended__c = true;
         Test.startTest();  
        	update statementToBeCloned; 
        	Statement__c clonedRecord= AmendStatementController.cloneStatement(String.valueof(statementToBeCloned.Id));
         Test.stopTest();
        
        system.assert(clonedRecord ==null, 'No New record is created'); 
        
    } 
    
   
}