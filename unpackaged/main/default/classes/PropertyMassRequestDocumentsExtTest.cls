/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for PropertyMassRequestDocumentsExtension
 * @Date: 01/30/2020
 * @Author: Srini Aluri(A1)
 
 * ModifiedBy         ModifiedDate   Description
 * Srini Aluri(A1)    01/31/2020     Initial development
*/

@isTest
public class PropertyMassRequestDocumentsExtTest {
    
    public class MockHttpResponseGenerator implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String endPoint = req.getEndpoint();
            
            // Create a fake response
            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-Type', 'application/json');
            resp.setBody('{"label":"LIST_VIEW_LABEL","id":"a025C000002NDKn","developerName":"LIST_VIEW","sobjectType":"Property__c","soqlCompatible":true,"query":"SELECT Id FROM Property__c"}');
            resp.setStatusCode(200);
            
            return resp;
        }
    }
    
	@testSetup
    static void createTestData() {
        
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        insert systemAdmin;
        
        //Create PermissionSet with Custom Permission and assign to test user
            PermissionSet ps = new PermissionSet();
            ps.Name = 'CustomPermission';
            ps.Label = 'CustomPermission';
            insert ps;
            SetupEntityAccess sea = new SetupEntityAccess();
            sea.ParentId = ps.Id;
            sea.SetupEntityId = [select Id from CustomPermission WHERE MasterLabel = 'Mass Generate Document Request'][0].Id;
            insert sea;
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.AssigneeId = systemAdmin.Id;
            psa.PermissionSetId = ps.Id;
            insert psa;
        
        System.runAs(systemAdmin){
        //Insert Account
        Id businessAccountRecordTypeId = DescribeUtility.getRecordTypeId(Account.sObjectType,CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME);
        Account account = TestDataUtility.buildAccount('Sample Property Account #101', null, businessAccountRecordTypeId);
        account.MailingStreetName__c = 'Test Street';
        account.MailingCity__c = 'Test City';
        INSERT account;

        //Insert Ownership
        Ownership__c ownership = TestDataUtility.getOwnerShip(account.Id,null);    
        INSERT ownership;
        
        // Create Property__c records of "Vessel" & "Business Personal Property" Record Type        
        Id vesselRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType,CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME);
        Id bppRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType,CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);
        
        // Create Properties
        List<Property__c> properties = new List<Property__c>();
        
        Property__c propertyNoticeToFile = new Property__c();
        propertyNoticeToFile = buildProperty(vesselRecordTypeId,account.Id, 'Test Property - #1','576-D','Notice to File','Yes');
        propertyNoticeToFile.PrimaryOwnership__c = ownership.Id;     
        properties.add(propertyNoticeToFile);
        
        Property__c propertyLowValue = new Property__c();
        propertyLowValue = buildProperty(vesselRecordTypeId,account.Id, 'Test Property - #2','576-D','Low Value','Optional');
        propertyLowValue.PrimaryOwnership__c = ownership.Id;
        properties.add(propertyLowValue);
        
        Property__c propertyDirectBill = new Property__c();
        propertyDirectBill = buildProperty(vesselRecordTypeId,account.Id, 'Test Property - #3','576-D','Direct Bill','Optional');
        propertyDirectBill.PrimaryOwnership__c = ownership.Id;
        properties.add(propertyDirectBill);  
        
        Property__c property1 = buildProperty(vesselRecordTypeId, account.Id, 'Test Property - #201','576-D','Direct Bill','Optional');
        property1.PrimaryOwnership__c = ownership.Id;
        property1.Status__c = 'Inactive';
        properties.add(property1);
        
        Property__c property2 = buildProperty(bppRecordTypeId, account.Id, 'Test Property - #202','571-L','Low Value','Optional');
        property2.PrimaryFormType__c = null;
        properties.add(property2);
        
        Property__c property3 = buildProperty(vesselRecordTypeId, account.Id, 'Test Property - #203','571-L','Low Value','Optional');
        property3.MailingStreetName__c = null;
        property3.MailingCity__c = null;
        property3.MailingCountry__c = null;
        property3.PrimaryOwnership__c = ownership.Id;
        properties.add(property3);
        
        Property__c property4 = buildProperty(vesselRecordTypeId, account.Id, 'Test Property - #204','571-L','Low Value','Optional');
        property4.RequiredToFile__c = null;
        property4.PrimaryOwnership__c = ownership.Id;
        properties.add(property4);
        
        Property__c property5 = buildProperty(bppRecordTypeId, account.Id, 'Test Property - #205','571-L','Low Value','Optional');
        properties.add(property5);

        Property__c property6 = buildProperty(vesselRecordTypeId, account.Id, 'Test Property - #204','571-L',null,'Optional');
        property6.RequiredToFile__c = 'Yes';
        property6.PrimaryOwnership__c = ownership.Id;
        properties.add(property6);
        
        INSERT properties;
        
    }
    }
    
    static Property__c buildProperty(Id recTypId,Id accountId,String name,String primaryFormType,String noticeType,String requiredTtoFile) {
        return new Property__c(
            Account__c = accountId,
            RecordTypeId = recTypId,
            Name = name,
            MailingStreetName__c = '7800 Blackwood',
            MailingCity__c = 'Portland',
            MailingCountry__c = 'US',
            Status__c = 'Active',
            PrimaryFormType__c =  primaryFormType, 
            ContactEmail__c = 'test@test.com',
            NoticeType__c = noticeType,
            RequiredToFile__c = requiredTtoFile
        );
    }

    // TEST METHODS
    @isTest
    static void extensionTest_PostiveScenario() {
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        
        System.runAs(systemAdminUser){
        //Insert Schema Set
        TH1__Schema_Set__c schemaSet = TestDataUtility.buildSchemaSets(1,true)[0];    
        //Insert Schema Object for property
        TH1__Schema_Object__c schemaObject = TestDataUtility.buildSchemaObject(1,true,'Property__c',schemaSet.Id)[0];
        //Update Schema Set
        schemaSet.TH1__Primary_Object__c = schemaObject.Id;
        UPDATE schemaSet;
        // Create Document Settings for Properties
        List<TH1__Document_Setting__c> documentSettings = TestDataUtility.buildDocumentSettingsForProperties(schemaSet.Id);
            
        String  batchQuery = 'SELECT Id, HasMailingAddress__c, MailingAddress__c, PrimaryFormType__c, Status__c,';
        batchQuery+= 'ContactEmail__c,NoticeType__c FROM Property__c WHERE RecordType.DeveloperName = \'Vessel\'';
        batchQuery+= 'And HasMailingAddress__c = true And RequiredToFile__c != null And RequiredToFile__c != \'No\'';
        batchQuery+= 'And Status__c != \'Inactive\'';
        
        // Set VF for test execution
        PageReference pageRef = Page.PropertyMassRequestDocuments;
        Test.setCurrentPage(pageRef);
        
        // Set mock 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        // Construct query to fetch Property__c records
        Database.QueryLocator queryLoc = Database.getQueryLocator(batchQuery);
        
        // Initialising StandardSetController
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(queryLoc);
        
        // Begin test
        Test.startTest();
        
        // Initialise Extension Controller
        PropertyMassRequestDocumentsExtension extCon = new PropertyMassRequestDocumentsExtension(setCon);
        
        // Call various extensions methods
        extCon.processSampleProperties();
        List<Property__c> processedProperties   =  extCon.getProcessedProperties();
        List<Property__c> problematicProperties =  extCon.getProblematicProperties();
        
        // Get Listview
        RestApi.ListViewResult listView = extCon.getListView();
        
        // Call generateNotices() function, so listview is generated and associated batch job execution is initiated
        PageReference retUrlPageRef = extCon.generateNotices();
        
        Test.stopTest();
        
        // Assert the functionality - We should get the ListView URL which is also the input(argument) to batch job
        System.assertEquals(true, String.isNotBlank(retUrlPageRef.getUrl()));
        System.assertEquals(4, processedProperties.size());
        System.assertEquals(5, problematicProperties.size());
        Set<String> processedPropertiesIds = new Set<String>();
        for(Property__c property : processedProperties)
        {
            processedPropertiesIds.add(property.id);
        }

        List<DocumentGenerationRequest__c> documentGenerationRequests = [SELECT Id, Status__c, RelatedRecordType__c, RelatedRecordName__c,DocumentSetting__r.Name, 
                                                                         Name FROM DocumentGenerationRequest__c WHERE RelatedRecordId__c in : processedPropertiesIds];        
        
        // Assert the functionality - We should expect DocumentGenerationRequest__c size same as processed Property size
        System.assertEquals(processedProperties.size()*2, documentGenerationRequests.size());
    }
    }
    
    @isTest
    static void extensionTest_NeagtiveScenario() {
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        
        System.runAs(systemAdminUser){
        //Update account mailing address for negative test case to make has mailing address as false
        Account acc = [select id, MailingStreetName__c from Account limit 1];
        acc.MailingStreetName__c = null;
        update acc;
        //Insert Schema Set
        TH1__Schema_Set__c schemaSet = TestDataUtility.buildSchemaSets(1,true)[0];    
        //Insert Schema Object for property
        TH1__Schema_Object__c schemaObject = TestDataUtility.buildSchemaObject(1,true,'Property__c',schemaSet.Id)[0];
        //Update Schema Set
        schemaSet.TH1__Primary_Object__c = schemaObject.Id;
        UPDATE schemaSet;
        // Create Document Setting for Property
		TH1__Document_Setting__c documentSetting = TestDataUtility.buildDocumentSetting(schemaSet.Id,'BPP 571-LA Statement (Property)');
        //documentSetting.TH1__Filter_field_value__c = '576-D';
        INSERT documentSetting;
        
        
        // Set VF for test execution
        PageReference pageRef = Page.PropertyMassRequestDocuments;
        Test.setCurrentPage(pageRef);
        
        // Set mock 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        // Construct query to fetch Property__c records
        Database.QueryLocator queryLoc = Database.getQueryLocator([SELECT Id 
                                                                   FROM Property__c 
                                                                   WHERE RecordType.Name = 'Vessel']);
        
        // Initialising StandardSetController
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(queryLoc);
        
        // Begin test
        Test.startTest();
        
        // Initialise Extension Controller
        PropertyMassRequestDocumentsExtension extCon = new PropertyMassRequestDocumentsExtension(setCon);
        
        // Call various extensions methods
        extCon.processSampleProperties();
        List<Property__c> processedProperties   =  extCon.getProcessedProperties();
        List<Property__c> problematicProperties =  extCon.getProblematicProperties();
        
        // Get Listview
        RestApi.ListViewResult listView = extCon.getListView();
        
        // Call generateNotices() function, so listview is generated and associated batch job execution is initiated
        PageReference retUrlPageRef = extCon.generateNotices();
        
        Test.stopTest();
        
        // Assert the functionality - We should get the ListView URL which is also the input(argument) to batch job
        System.assertEquals(true, String.isNotBlank(retUrlPageRef.getUrl()));
        System.assertEquals(0, processedProperties.size());
        System.assertEquals(9, problematicProperties.size());
    }
}
}