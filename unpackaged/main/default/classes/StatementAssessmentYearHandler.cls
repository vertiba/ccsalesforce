/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/

//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to display the updated value of Assessment year on Statement record.
//---
public class StatementAssessmentYearHandler {
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : recordId
    // @description : This method, will call the statement processor process builder and pass the Date and String values.
    // @return : Void
    //-----------------------------
	@InvocableMethod
    public static void getStatementAssessmentYear(List<wrapperClass> wrapperList){
        wrapperClass wrapper = wrapperList[0];
        Statement__c updateStatement = new Statement__c();
        updateStatement.id = wrapper.statementId;
        String fiscalYear = String.valueof(FiscalYearUtility.getFiscalYear(wrapper.vipDate));
        updateStatement.AssessmentYear__c = fiscalYear;
        if(updateStatement != null){
            Database.update(updateStatement,false);
        }
        setAmendedForm(wrapper.statementId);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : recordId
    // @description : Updating AmendedFrom Based on the latest file date entry in the Statement record..
    // @return : Void
    //-----------------------------

    public static void setAmendedForm(Id statementIdfromFlow) {
        Statement__c currentStatement = [Select Id,FileDate__c,Form__c,Property__c,AssessmentYear__c,AmendedFrom__c
                                         from statement__c where Id =:statementIdfromFlow LIMIT 1];
        if(currentStatement == null) {
            return;
        }
        List<Statement__c> amendedStatments = new List<Statement__c>([Select id,AssessmentYear__c,Form__c,FileDate__c,Property__c from Statement__c
                                                                          where Id != :currentStatement.Id and Property__c=:currentStatement.Property__c and Form__c=:currentStatement.Form__c and
                                                                          AssessmentYear__c=:currentStatement.AssessmentYear__c and (Status__c =: CCSFConstants.STATEMENT.STATUS_SUBMITTED OR Status__c =: CCSFConstants.STATEMENT.STATUS_PROCESSED) ORDER BY FileDate__c DESC LIMIT 1]);
		
        if(!amendedStatments.isEmpty()){
            currentStatement.AmendedFrom__c = amendedStatments[0].Id;
            Database.update(currentStatement,false); 
        }
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This Wrapper class is used as a invocable variable to populate Assessment Year value.
    //---
    public class wrapperClass{
        @InvocableVariable
        public DateTime vipDate;
        
        @InvocableVariable
        public String statementId;
    }
}