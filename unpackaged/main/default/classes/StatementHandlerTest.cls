/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//                            Class to test StatementHandler
//-----------------------------
@isTest
private class StatementHandlerTest {
    
    @TestSetup
    static void dataForTesting(){
       		List<Account> accounts = new List<Account>(); 
            Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
            Account testAccount = new Account();
            testAccount.Name = 'Test Account';
            testAccount.BusinessStatus__c = 'Active';
            testAccount.NoticeType__c ='Notice to File';
            testAccount.RequiredToFile__c ='Yes';
            testAccount.RecordTypeId = accRecordTypeId;
            accounts.add(testAccount);            
             
            Account testAccount1 = new Account();
            testAccount1.Name = 'Test Account1';
            testAccount1.RecordTypeId = accRecordTypeId;
            testAccount1.BusinessStatus__c = 'Active';
            testAccount1.NoticeType__c ='Notice to File';
            testAccount1.RequiredToFile__c ='Yes';
            accounts.add(testAccount1);           
            insert accounts;
        
            List<Property__c> properties = new List<Property__c>();            
            Id proRecordTypeId  = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
            Property__c testProperty =  new Property__c();
            testProperty.RecordTypeId = proRecordTypeId;
            testProperty.Account__c = testAccount.Id;
            testProperty.DoingBusinessAs__c = 'Chemicals';
            testProperty.Status__c ='Active';
            testProperty.RollCode__c ='Unsecured';
            testProperty.AssessorsParcelNumber__c ='1234A567';
            testProperty.DiscoverySource__c ='Audit';
            properties.add(testProperty);
            
            Property__c testProperty1 =  new Property__c();
            testProperty1.RecordTypeId = proRecordTypeId;
            testProperty1.Account__c = testAccount1.Id;
            testProperty1.DoingBusinessAs__c = 'IT';
            testProperty1.Status__c ='Active';
            testProperty1.RollCode__c ='Secured';
            testProperty1.AssessorsParcelNumber__c ='1234B567';
            testProperty1.DiscoverySource__c ='Audit';
            properties.add(testProperty1);
            insert properties; 
           
            Date filingDate = System.today();            
            Id statementRecordTypeId  = Schema.SObjectType.Statement__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalPropertyStatement').getRecordTypeId();
            
            Statement__c testStatement = new Statement__c();
            testStatement.RecordTypeId = statementRecordTypeId;
            testStatement.BusinessAccount__c = testProperty.Account__c;
            testStatement.Property__c = testProperty.Id;
            testStatement.FileDate__c = filingDate; 
            insert testStatement;           
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Tests that statementhandler.SetAssesmentYear updates the Assessment Year
    //                                        Based on the FileDate. Testing for both insert and update
    // @return : void
    //-----------------------------
   
    @isTest
    private static void assesmentYearToBeSet() {
        Date filingDate = System.today();
        Statement__c statement = [Select id,FileDate__c, AssessmentYear__c from Statement__c];
        System.assertEquals( statement.AssessmentYear__c, String.valueOf(FiscalYearUtility.getFiscalYear(statement.FileDate__c)));
        statement.FileDate__c = filingDate.addYears(-1);        
        update statement;
        Statement__c testStatement = [select AssessmentYear__c, FileDate__c from statement__c where id =:statement.id];    
        System.assertEquals( testStatement.AssessmentYear__c, String.valueOf(FiscalYearUtility.getFiscalYear(statement.FileDate__c)));
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Tests that statementhandler.SetAssesmentYear does not update assesment year
    //                                        when FileDate is empty. Testing for both insert and update
    // @return : void
    //-----------------------------
   
    @isTest
    private static void assesmentYearNotToBeSet() {
        Statement__c statement = new Statement__c();
        statement.FileDate__c = Null;
        insert statement;
        statement = [select AssessmentYear__c from statement__c where id =:statement.id];
        System.assertEquals(statement.AssessmentYear__c,null );
        //Updating some field not related to Filing Date.
        statement.AccountingContactName__c = 'Test Contact Name';
        update statement;
        statement = [select AssessmentYear__c from statement__c where id =:statement.id];
        System.assertEquals(statement.AssessmentYear__c,null);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Tests statementhandler.setStatementOwner to make sure Statement Owner is set to a Queue.
    //                Testing for insert only.
    // @return : void
    //-----------------------------
    @isTest
    private static void statementOwnerSetToQueue() {
        Statement__c statement = new Statement__c();
        statement.FileDate__c = Null;
        statement.FilingMethod__c = CCSFConstants.eFile;
        insert statement;

        statement = [select Id, OwnerId from statement__c where id = :statement.id];
        List<Group> stmntQueueList = [Select Id from Group 
                                Where type = :CCSFConstants.typeQueue and 
                                DeveloperName = :CCSFConstants.statementQueueDevName];

        if (stmntQueueList.size() > 0){
            System.assertEquals(statement.OwnerId, stmntQueueList[0].Id );
        }
    }
}