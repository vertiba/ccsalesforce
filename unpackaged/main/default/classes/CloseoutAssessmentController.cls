//Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used for Aura Componenet for Approval, as part of ASR-3504- 
//                 Submiiting the Regular Closeoout for Approval with its related escapeCloseouts
//                 EscapeCloseout 
//-----------------------------
public class CloseoutAssessmentController {
     public static final String  ORIGINLOCATION= 'CloseoutAssessmentController';
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method is used for Aura Componenet for Approval
    // @params : Record Id
    //-----------------------------
    @AuraEnabled
    public static AssessmentDataWrapper getCloseoutAssessment(String recordId) {
        AssessmentDataWrapper assesmentData = new AssessmentDataWrapper();
        List<Case> closeoutAssessments = new List<Case>();
        Case closeoutCase = [Select Id,Type,Subtype__c,ParentRegularAssessment__c FROM CASE WHERE ID =:recordId];
        //Dymaic Query -- Depending on If Record is Submitted for Approval id REGULAR Closeout or ESCAPE Closeout
        String query = 'SELECT Id,Property__c,Property__r.id,IsClosed__c,IsLocked__c,RecordType.Name,TotalPersonalPropertyValue__c,RecordTypeId,TotalFixturesValue__c,AssessmentYear__c,';
        query += 'CaseNumber,Status,SubStatus__c,Penalty__r.Name,InterestCode__c,NumberOfMonthsFor506Interest__c,ParentRegularAssessment__c,AssessmentNumber__c,SubType__c,AdjustmentType__c,';
        query += 'RollCode__c,Type,ExemptAmountManual__c,CalculatedAmountExempt__c,TotalATMsCost__c,TotalATMsValue__c,TotalConstructioninProgressCost__c,AdjustmentReason__c,TotalAssessedValue__c,TotalAssessedCost__c,';
        query += 'TotalConstructioninProgressValue__c,TotalCounterlinesPartitionsetcCost__c,TotalCarpetsDrapesCost__c,TotalCarpetsDrapesValue__c,TotalDriveupWalkupKioskValue__c,';
        query += 'TotalDriveupWalkupWindowKioskCost__c,TotalFurnitureAppliancesValue__c,TotalFurnitureandAppliancesCost__c,TotalLandDevelopmentValue__c,';
        query +=  'TotalLandImprovementsCost__c,TotalLandImprovementsValue__c,TotalLeasedEquipmentCost__c,TotalLeasedEquipmentValue__c,TotalCounterlinesPartitionsectValue__c,';
        query +=  'TotalLeaseholdImprovementsFixCost__c,TotalLeaseholdImprovementsFixValue__c,TotalLeaseholdImprovementsStrCost__c,TotalLeaseholdImprovementsStrValue__c,';
        query +=  'TotalLANMainframesCost__c,TotalLANMainframesValue__c,TotalMachineryEquipmentCost__c,TotalMachineryEquipmentValue__c,Property__r.BusinessLocationOpenDate__c,Account.Name,';
        query +=  'TotalOfficeFurnitureEquipmentCost__c,TotalOfficeFurnitureEquipmentValue__c,TotalOtherEquipmentCost__c,TotalOtherEquipmentValue__c,Property__r.LocationAddress__c,';
        query +=  'TotalOtherFurnitureEquipmentCost__c,TotalOtherFurnitureEquipmentValue__c,TotalPersonalComputersCost__c,TotalPersonalComputersValue__c,Property__r.PropertyId__c,Property__r.DoingBusinessAs__c,';
        query +=  'TotalSignsCameraTVEquipmentetcCost__c,TotalSignsCameraTVetcValue__c,TotalFixturesValueOverride__c,TotalLandandLandDevelopmentCost__c,BasisPersonalPropertyValue__c,';
        query +=  'TotalSuppliesValue__c,TotalSuppliesReportedCost__c,TotalToolsMoldsDiesJigsCost__c,TotalToolsMoldsDiesJigsValue__c,BasisFixtureValue__c,Roll__r.Status__c,WaiveInterest__c,';
        query +=  'TotalVaultDoorNightDepositoriesCost__c,VesselTotalValueOverride__c,TotalVaultDoorNightDepositoryValue__c,TotalPersonalPropertyValueOverride__c FROM Case';
        
        // if trying submitted Regular Closeout
        if(closeoutCase.Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR && closeoutCase.SubType__c ==CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT ){
            query += ' WHERE ID= \''+recordId+'\'';
            query += ' OR ParentRegularAssessment__c= \''+recordId+'\'';
        }else{  
            // if trying submitted Escape Closeout
            query += ' WHERE ID= \''+closeoutCase.ParentRegularAssessment__c+'\'';
            query += ' OR ParentRegularAssessment__c= \''+closeoutCase.ParentRegularAssessment__c+'\'';
        }
        query += ' ORDER BY AssessmentYear__c DESC';
        closeoutAssessments= Database.query(query);

        assesmentData.escapeCloseouts = new List<Case>();
        // Add data to AssessmentDataWrapper
        for(Case assessment: closeoutAssessments){
            if(assessment.Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR && assessment.AdjustmentType__c ==CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW){
                assesmentData.caseRecord = assessment;
            }else if(assessment.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE && assessment.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT){
               assesmentData.escapeCloseouts.add(assessment);
            }
        }        
        return assesmentData;
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns the AdjustmentReason__c
    // 		 picklist values as a map
    // @return : Map<String,String>
    //-----------------------------
    @AuraEnabled
    public static Map<String, String> fetchAdjustmentReason() {
        Map<String, String> options = new Map<String, String>();
        for (Schema.PicklistEntry pickList: DescribeUtility.getPicklistValues(CCSFConstants.ASSESSMENT.CASE_API_NAME,'AdjustmentReason__c')) {
            options.put(pickList.getValue(), pickList.getLabel());
        }
        return options;
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Type 
    // @description : This method returns the dependent picklists for Assessment SubType
    // 		 picklist values as a map, so that they are displayed in UI.
    // @return : Map<String,String>
    //-----------------------------
    @AuraEnabled
    public static Map<String, String> fetchSubType(String typeValue,String recordTypeName) {
        String recordTypeValue;
        List<String> subTypeValues = new List<String>();
        if(recordTypeName.contains('Vessel')){
            recordTypeValue = Label.VesselAssessmentSubType;
            subTypeValues = recordTypeValue.split(',');
        }else{
            recordTypeValue = Label.BPPAssessmentSubType;
            subTypeValues = recordTypeValue.split(',');
        }
        Schema.SObjectField typeFieldValue = Case.Type.getDescribe().getSObjectField();
        Schema.SObjectField subTypeField = Case.SubType__c.getDescribe().getSObjectField();
        Map<String, String> options = new Map<String, String>();
        List<DependentPicklistUtil.PicklistEntryWrapper> dependentList = DependentPicklistUtil.getDependentOptionsImpl(subTypeField, typeFieldValue).get(typeValue);
        if(dependentList != null){
            for (DependentPicklistUtil.PicklistEntryWrapper pickList: dependentList) {
                if(recordTypeValue.contains(pickList.label)){
                    options.put(pickList.value, pickList.label);
                }
            }
        }
        return options;
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : property Id of record, Assessment Year, record Id, and Wrapper with updated values on UI 
    // @description : This Method validates all the scenrio updates the assessments and Submit for Approval. 
    // @return : String
    //-----------------------------
    @AuraEnabled
    public static String updateAndApprove(String assessmentDataWrapper) {
        //serialize dataWrapper from Aura Compoenent
        AssessmentDataWrapper dataWrapper = (AssessmentDataWrapper)SYSTEM.JSON.deserialize(assessmentDataWrapper, AssessmentDataWrapper.class);
        Case assessmentToSubmit = new Case();
        List<Case> assessmentToUpdate  = new List<Case>();
        assessmentToSubmit= dataWrapper.caseRecord; 
        //Escape Closeout Update the Status once the regular Closeout is Submitted for Approval.
        //All escape Closeout willnot go for the Actual Approval Process
        if(!dataWrapper.escapeCloseouts.isEmpty()){			        
                for(Case cas: dataWrapper.escapeCloseouts){
                    cas.Status = CCSFConstants.STATUS_INPROGRESS;
                    cas.SubStatus__c = CCSFConstants.ASSESSMENT.SUB_STATUS_PENDINGREVIEW;
                    // Added this logic as part of 8367 to update the interest code.
                    //  1st September of the assessmentyear of the Submit for approval Assessment as cutoff Date for Interest to be applied
                    Date cutOffDate = Date.newInstance(Integer.valueOf(cas.AssessmentYear__c), 09, 01); 
                    // Logic to check Submit for approval date is after the cutoff date.
                    if(cutOffDate <= system.today() && cas.Roll__r.Status__c == CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS && cas.WaiveInterest__c == null)  {
                        cas.InterestCode__c = CCSFConstants.INTEREST_CODE;
                    }
                    //ASR-9259. if interest code is populated and NumberOfMonthsFor506Interest__c is not show an error
                    if(cas.InterestCode__c !=null && cas.NumberOfMonthsFor506Interest__c == null){
                        throw new AuraHandledException(Label.NumberMonthsfor506InterestError);
                    }
                    //AdjustmentReason__c field mandatory-- for ASR-10823
                    if(cas.AdjustmentReason__c == null || String.isBlank(cas.AdjustmentReason__c)){
                        throw new AuraHandledException(System.Label.RequiredAdjustmentReasonMsg);
                    }
                    assessmentToUpdate.add(cas);
                }
        }       
        try{ // Update the fields Selected from UI -- AdjustmentReason And SubType On Regular Closeout
            if(assessmentToSubmit != null){
                update assessmentToSubmit;   
            }
        }Catch(Exception e){
            String eMessage;
            eMessage = e.getMessage();
            if(eMessage.contains(CCSFConstants.ASSESSMENT.VALIDATION_EXCEPTION_TEXT)){
                eMessage = eMessage.substringAfter(CCSFConstants.ASSESSMENT.VALIDATION_EXCEPTION_TEXT + ',');
                eMessage = eMessage.removeEndIgnoreCase(': []');
            }
            else {
                eMessage = Label.ErrorMessageForUser;
            }      
            Logger.addExceptionEntry(e);
            Logger.saveLog();
            throw new AuraHandledException(eMessage);
        }
        String response;
         // Regular Closeout is Submitted for Approval
        List<Case> assessmentSentForApproval = new List<Case>(); 
        assessmentSentForApproval.add(assessmentToSubmit);         
       Savepoint sp = Database.setSavepoint();
       response = updateEscapeCloseouts(assessmentToUpdate);
       if(!response.contains(CCSFConstants.ESCAPECLOSEOUTS.SYSTEM_ERROR)){
        response = AssessmentsUtility.submitRecordApproval(assessmentSentForApproval);
       }else{
              Database.rollback(sp);
       }
       // Once Regular Closeout is Submitted for Approval Update related Escape Closeout
       if(response.contains(CCSFConstants.ESCAPECLOSEOUTS.SYSTEM_FAILED)){Database.rollback(sp);}
        return response;
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : property Id of record, Assessment Year, record Id, and Wrapper with updated values on UI 
    // @description : This Method updates all Escape Closeout related to the regular closeout submitted for approval
    // @return : String
    //-----------------------------
    @AuraEnabled
    public static String updateEscapeCloseouts(List<Case> assessmentToUpdate) {  
        List<String> databaseErrors = new List<String>();
        if(!assessmentToUpdate.isEmpty()){
            try{
                Map<String,Object> updateResultMap =  DescribeUtility.updateRecords(assessmentToUpdate,null);
                List<Database.SaveResult> saveUpdateResults= (List<Database.SaveResult>)updateResultMap.get('updateResults');        
                for(Integer i=0;i< saveUpdateResults.size();i++){
                    if (!saveUpdateResults.get(i).isSuccess()) {
                        Database.Error error = saveUpdateResults.get(i).getErrors().get(0);
                        ID errorAssessmentId = saveUpdateResults.get(i).getId();
                        databaseErrors.add(String.valueof(error)+errorAssessmentId);
                        Logger.addDebugEntry(LoggingLevel.ERROR,'ID:'+ errorAssessmentId+String.valueof(error),ORIGINLOCATION+' -updateEscapeCloseouts',null);
                    }
                }
            }catch(Exception e){
                String eMessage;
                eMessage = e.getMessage();
                if(eMessage.contains(CCSFConstants.ASSESSMENT.VALIDATION_EXCEPTION_TEXT)){
                    eMessage = eMessage.substringAfter(CCSFConstants.ASSESSMENT.VALIDATION_EXCEPTION_TEXT + ',');
                    eMessage = eMessage.removeEndIgnoreCase(': []');
                }else {
                    eMessage = Label.ErrorMessageForUser;
                }
                Logger.addExceptionEntry(e);
                Logger.saveLog();
                throw new AuraHandledException(eMessage);
            }
        }
        if(!databaseErrors.isEmpty()){return  CCSFConstants.ESCAPECLOSEOUTS.SYSTEM_ERROR;}
        return CCSFConstants.ESCAPECLOSEOUTS.SYSTEM_SUCCESS;
    }

}