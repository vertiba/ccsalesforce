/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test RollUpAssessedValue Test Class
//-----------------------------
@isTest
public class RollUpAssessedValueTest {
    
    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
    private static Integer nextYear 		= currentYear+1;
    
    @testSetup
    private static void dataSetup(){
        //insert penalty
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
        insert penalty;
        Account businessAccount = TestDataUtility.getBusinessAccount();
        insert businessAccount;
        
        Property__c property = TestDataUtility.getVesselProperty();
        property.Account__c = businessAccount.Id;
        property.NoticeType__c = CCSFConstants.NOTICE_TYPE_DIRECT_BILL;
        property.Status__c= CCSFConstants.STATUS_ACTIVE;
        property.AssessedCost__c = 100;
        property.MostRecentAssessedValue__c = 200;
        insert property;
        
        String fiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        Date filingDate = Date.newInstance(Integer.valueOf(fiscalYear), 5, 30);
        String prevfiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);

        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = fiscalYear;
        rollYear.Year__c = fiscalYear;
        rollYear.Status__c='Roll Open';
        insert rollYear;
        
        
        Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),property.Id,
                                                          null,null,null,
                                                          CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(Integer.valueOf(prevfiscalYear), 3, 11));
        assessment.Status =  CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED;
        assessment.IntegrationStatus__c = CCSFConstants.SENT_TO_TTX;
        assessment.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
        assessment.AccountId = property.account__c;
        assessment.Property__c = property.Id;
        assessment.SequenceNumber__c = 1;
        assessment.TotalPersonalPropertyValueOverride__c = 1000;        
        insert assessment;
    }    

    @isTest static void testUpdateClosedRollYearPropertyAssessedCost(){
        String nextfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
        RollYear__c rollYears = [Select id,Name,Status__c,Year__c FROM RollYear__c order by Name desc LIMIT 1];

        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = nextfiscalYear;
        rollYear.Year__c = nextfiscalYear;
        rollYear.Status__c='Roll Closed';
        insert rollYear; 
        Test.startTest();
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,
                                  		 TotalAssessedCost__c,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE AssessmentYear__c   =: rollYear.Name 
                                  AND Status               	=: CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED                                 
                                  ORDER BY SequenceNumber__c DESC]; 
        Property__c property = [Select Id,AssessedCost__c,MostRecentAssessedValue__c FROM Property__c where id = :assessments[0].Property__r.id];
        Set<Property__c> propertyTest= new Set<Property__c>();
        propertyTest.add(property);
        UpdatePropertyAssessedCost uca = new UpdatePropertyAssessedCost();
        Id batchId = Database.executeBatch(uca);
        Test.stopTest();
        Property__c updProperty = [Select Id,AssessedCost__c,MostRecentAssessedValue__c FROM Property__c where id = :assessments[0].Property__r.id];
        system.assertNotEquals(null, updProperty.AssessedCost__c);

    }
     @isTest static void testUpdatePropertyAssessedCost(){
        String nextfiscalYear  =  String.valueof(FiscalYearUtility.getCurrentFiscalYear()-2);
        RollYear__c rollYears = [Select id,Name,Status__c,Year__c FROM RollYear__c order by Name desc LIMIT 1];

        RollYear__c rollYear = new RollYear__c();
        rollYear.Name = nextfiscalYear;
        rollYear.Year__c = nextfiscalYear;
        rollYear.Status__c='Roll Closed';
        insert rollYear; 
        Test.startTest();
        List<Case> assessments 	=[SELECT Property__c,TotalAssessedValue__c,RollCode__c,AssessmentNumber__c,MostRecentAssessedValue__c,
                                  		 TotalAssessedCost__c,Type,AdjustmentType__c,AssessmentYear__c,SubType__c,IntegrationStatus__c,Property__r.id
                                  FROM Case 
                                  WHERE AssessmentYear__c   =: rollYear.Name 
                                  AND Status               	=: CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED                                 
                                  ORDER BY SequenceNumber__c DESC]; 
       	
        Property__c property = [Select Id,AssessedCost__c,MostRecentAssessedValue__c FROM Property__c];
        Set<Property__c> propertyTest= new Set<Property__c>();         
        propertyTest.add(property);
        UpdatePropertyAssessedCost uca = new UpdatePropertyAssessedCost();
        Id batchId = Database.executeBatch(uca);
        Test.stopTest();
        Property__c updProperty = [Select Id,AssessedCost__c,MostRecentAssessedValue__c FROM Property__c];
        system.assertEquals(null, updProperty.AssessedCost__c);

    }
}