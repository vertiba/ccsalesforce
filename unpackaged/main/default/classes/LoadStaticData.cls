/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/ 
//-----------------------------
// @author : Publicis.Sapient
//-----------------------------
public class LoadStaticData {

    private string objectName;
    private string uniqueField;
    private List<string> headerFields;
    private Map <String, Schema.SObjectField> fieldMap;
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : FileName -- Static resource file name as input
    // @description: Static resource file is expected to be in this format, with minimum 3 lines
    // 		ObjectName, UniqueId --Line 1 uniqueId is optional. If there is any duplicate check
    //		Headers --Line 2
    //		data --From Line 3 
    //		Then data is parsed, inserted or updated into records. 
    // @return : void
    //-----------------------------
    public void loadDataFromStaticResource(String fileName){
        List<String> csvFileLines = fetchDataFromFile(fileName);
        if(csvFileLines.isEmpty() || csvFileLines.size()<3){
            System.assert(false, 'Either empty file or only header line or object Name is mising. Records are not inserted');
        }
        List<String> firstLine = csvFileLines.remove(0).split(',');
        objectName = firstLine.get(0);
        uniqueField = firstLine.size()>1?firstLine.get(1):null;//optional value
        headerFields = csvFileLines.remove(0).split(',');
        String exceptionString = isValidObjectAndFields();
        System.assertEquals('', exceptionString, exceptionString);
        createAndPopulateRecords(csvFileLines);
    }

    //-----------------------------
    // @author: Publicis.Sapient
    // @param: FileName - Static resource file name. 
    // @description: Reads the file and then splits into strings, based on the new line character. 
    // @return : List<String>
    //-----------------------------
    private List<String> fetchDataFromFile(String fileName){
        StaticResource sr= [select id, body from StaticResource Where Name =:String.escapeSingleQuotes(fileName)];
        String allcontents = sr.body.toString();
        //Sometimes I get \r\n and sometimes as \n for new line.
        allcontents=allcontents.replaceAll('"', '').replaceAll('\r', '');
        return allcontents.split('\n'); //Splitting at new line.
    }

    //-----------------------------
    // @author: Publicis.Sapient
    // @description: Verifies, if the org has the object, headerfields are valid, 
    // 		If unique field is set, then it has to be in the object and headerfields.
    // 		Various strings are returned based on the validation, so that proper message can be 
    // 		shown to user. If data looks good then, empty string is returned. 
    // @return : String
    //-----------------------------
    private String isValidObjectAndFields(){
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        if(objectType == null){
            return objectName+' is not there in the System';
        }
        fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        for(String headerField:headerFields){
            if(!fieldMap.containsKey(headerField.toLowerCase())){
                return headerField+' is not there in the object';
            }
        }
        //Unique field is not passed. Rest of the things match. Return true.
        if (uniqueField == null || uniqueField.trim().equals('')) return '';
        if(!fieldMap.containsKey(uniqueField.toLowerCase())){
            return uniqueField+' is not there in the object';
        }
        if(!(new Set<String>(headerFields)).contains(uniqueField)){
            return uniqueField+' is not available in Headers';
        }
        return '';
    }

    //-----------------------------
    // @author: Publicis.Sapient
    // @param: List<String> - List of records to insert or update
    // @description: From the list of strings, Split each one of them based on comma 
    // 		and create an object based on the object name, PUT values into it and proceed 
    // 		for insertion.
    // @return : void
    //-----------------------------
    private void createAndPopulateRecords(List<String> csvFileLines){
        sObject sObjectInstance;
        List<String> fieldValues;
        List<sObject> objectsToUpsert = new List<sObject>();
        Schema.SObjectType objectType= Schema.getGlobalDescribe().get(ObjectName);
        Integer fieldSize = headerFields.size();
        Map<String,object> fieldObject = new Map<String,Object>();
        FieldConversion fieldConversion = new FieldConversion();
        for(String csvFileLine:csvFileLines){
            sObjectInstance = objectType.newSObject();
            fieldValues = csvFileLine.split('\\\\,');
            System.debug('fieldValues'+fieldValues);
             //Might be bad data, dont insert if values and header size dont match
            if(fieldValues.size()!=fieldSize) continue;
            for(Integer index=0;index<fieldSize;index++){
                String fieldType= string.valueOf(fieldMap.get(((String)headerFields.get(index)).toLowerCase()).getDescribe().getType());
                fieldObject.put('value',fieldValues.get(index));
                sObjectInstance.put(headerFields.get(index), fieldConversion.call(fieldType,fieldObject));
            }
            objectsToUpsert.add(sObjectInstance);
        }
        if(objectsToUpsert.size()>0)
            doUpsert(objectsToUpsert, objectType);
    }	

    //-----------------------------
    // @author: Publicis.Sapient
    // @param: List<objectsToInsert> - List of records to insert or update
    // @param: Schema.SObjectType - object type for insertion. 
    // @description: Based on the sobject type, create a list and add all the records for insertion. 
    // 		insert records, and check for any failures. For each failed record, check if it is 
    // 		failed for duplicate data. Get the Id from duplicate record, add it to the corresponding
    // 		failed record from insertion list. Once all the records are collected, update them
    // @return : void
    //-----------------------------
    @testVisible
    private void doUpsert(List<sObject> objectsToUpsert, Schema.SObjectType objectType) {
        String listType = 'List<' + objectType + '>';
        List<sObject> upsertObjects = (List<SObject>)Type.forName(listType).newInstance();
        upsertObjects.addAll(objectsToUpsert);
        try{
            Database.UpsertResult[] upsertResults =  Database.Upsert(upsertObjects, false);
            List<sObject> updateObjects = (List<SObject>)Type.forName(listType).newInstance();
            Database.UpsertResult upsertResult;
            sObject failedRecord;
            for (Integer index=0; index<upsertResults.size(); index++) {
                upsertResult = upsertResults.get(index);
                failedRecord = upsertObjects.get(index);
                if (!upsertResult.isSuccess()) {
                    System.debug('upsertResult'+upsertResult);
                    for(Database.Error error : upsertResult.getErrors()) {
                        if (error instanceof Database.DuplicateError) {//Caught using Duplicate rules
                            Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                            Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                            Datacloud.MatchResult matchResult = duplicateResult.getMatchResults()[0];
                            Datacloud.MatchRecord matchRecord = matchResult.getMatchRecords()[0];
                            System.debug('matchRecord'+matchRecord);
                            failedRecord.id = matchRecord.getRecord().id;
                            updateObjects.add(failedRecord);
                        } else if (error.getStatusCode() == System.StatusCode.DUPLICATE_VALUE){
                            String errorMessage = error.getMessage();
                            //Substring will be 15 digit id, so assign to Id.
                            Id duplicateId = errorMessage.subString(errorMessage.indexOf('id:')+3).trim();
                            failedRecord.id = duplicateId;
                            updateObjects.add(failedRecord);
                        }
                    }
                }
            }
            if(!updateObjects.isEmpty()){
                Database.update(updateObjects, false);
            }
        } catch (Exception e){
            System.assert(false, e.getMessage());
        }
    }
    
    class FieldConversion implements Callable {
        public Object call(String action, Map<String, Object> args) {
            if(args.get('value') == null || (string)args.get('value') == '') return null;
            switch on action.toLowerCase() {
				when 'double', 'decimal', 'currency', 'percent', 'integer' {
                    return Double.valueOf(args.get('value'));
                } when 'boolean' {
                    return Boolean.valueOf(args.get('value'));
                } when 'date' {
                    if((string)args.get('value') == '') return null;
                    return Date.parse(Date.parse((string)args.get('value')).format());
                } when else {//'String', 'TextArea'
                    return String.valueOf(args.get('value'));
                } 
            }
        }
    }
}