/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class AumentumDataNormalizeScheduler extends EnhancedSchedulable {
     public override String getJobNamePrefix() {
        return 'AumentumDataNormalizeSchedule';
    }  
    // For Scheduling Batch class 
    public void execute(SchedulableContext sc){
        try{            
            Logger.addDebugEntry('AumentumDataNormalizeBatch Batch is scheduled','AumentumDataNormalizeBatch.Schedulable Execute');
            Id batchProcessId = Database.executeBatch(new AumentumDataNormalizeBatch());
            Logger.addDebugEntry('After Scheduling batch, jobid:'+batchProcessId, 'AumentumDataNormalizeBatch.execute');
            
        }catch (Exception ex){
           Logger.addExceptionEntry(ex, 'AumentumDataNormalizeBatch.execute');
        }
        Logger.saveLog();
    }  

}