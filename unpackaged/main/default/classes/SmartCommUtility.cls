/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class SmartCommUtility {

    private static final String NOTICE_TEMPLATE_PREFIX = 'Notice of Requirement to File';

    public static DocumentGenerationRequest__c createDocumentGenerationRequest(Id recordId, Id documentTemplateId) {
        DocumentGenerationRequest__c request = new DocumentGenerationRequest__c(
            DocumentSetting__c = documentTemplateId,
            RelatedRecordId__c = recordId
        );
        return request;
    }

    public static List<SObject> getAvailableDocumentSettings(Id recordId) {
        // Get all of the document settings
        List<TH1__Document_Setting__c> documentSettings = [
            SELECT Id, Name, TH1__Filter_field_name__c, TH1__Filter_field_value__c
            FROM TH1__Document_Setting__c
            WHERE TH1__Primary_Object__c = :String.valueOf(recordId.getSObjectType())
            AND TH1__Is_disabled__c = false
            AND TH1__Generate_document__c = true
            ORDER BY Name
        ];

        // Get the record
        SObject record = queryRecord(documentSettings, recordId);

        // Get the matching document settings
        return getMatchingDocumentSettings(documentSettings, record);
    }

    public static Map<String, TH1__Document_Setting__c> getDocumentSettingsByPrimaryFormType(List<Property__c> properties) {
        List<String> documentSettingNames = new List<String>();
        for(Property__c property : properties) {
            if(property.PrimaryFormType__c == null) continue;

            String documentSettingName = NOTICE_TEMPLATE_PREFIX + ' ' + property.PrimaryFormType__c;
            documentSettingNames.add(documentSettingName);
        }

        List<TH1__Document_Setting__c> documentSettings = [SELECT Id, Name FROM TH1__Document_Setting__c WHERE Name IN :documentSettingNames];

        Map<String, TH1__Document_Setting__c> documentSettingsByFormName = new Map<String, TH1__Document_Setting__c>();
        for(TH1__Document_Setting__c documentSetting : documentSettings) {
            String formType = documentSetting.Name.replace(NOTICE_TEMPLATE_PREFIX, '').trim();
            documentSettingsByFormName.put(formType, documentSetting);
        }

        return documentSettingsByFormName;
    }

    public static Map<String, TH1__Document_Setting__c> getDocumentSettingsBySObjectName(string sObjectName) {
        Map<String, TH1__Document_Setting__c> documentSettingsByFormName = new Map<String, TH1__Document_Setting__c>();
        for(TH1__Document_Setting__c documentSetting : [
            SELECT Id, Name FROM TH1__Document_Setting__c WHERE TH1__primary_object__c =: sObjectName and TH1__is_disabled__c = false]) {
            documentSettingsByFormName.put(documentSetting.Name.trim(), documentSetting);
        }
        return documentSettingsByFormName;
    }

    private static SObject queryRecord(List<TH1__Document_Setting__c> documentSettings, Id recordId) {
        Set<String> queryFields = new Set<String>();
        for(TH1__Document_Setting__c documentSetting : documentSettings) {
            if(documentSetting.TH1__Filter_field_name__c == null) continue;
            else queryFields.add(documentSetting.TH1__Filter_field_name__c);
        }

        String additionalQueryFields = queryFields.isEmpty() ? '' : ', ' + String.join(new List<String>(queryFields), ', ');
        String query = 'SELECT Id' + additionalQueryFields
            + ' FROM ' + recordId.getSObjectType()
            + ' WHERE Id = \'' + recordId + '\'';

        return Database.query(query);
    }

    private static List<TH1__Document_Setting__c> getMatchingDocumentSettings(List<TH1__Document_Setting__c> documentSettings, SObject record) {
        List<TH1__Document_Setting__c> matchingDocumentSettings = new List<TH1__Document_Setting__c>();
        for(TH1__Document_Setting__c setting : documentSettings) {
            Boolean matches = false;
            if(setting.TH1__Filter_field_name__c == null) matches = true;
            else if((String)record.get(setting.TH1__Filter_field_name__c) == (String)setting.TH1__Filter_field_value__c) matches = true;

            if(matches) matchingDocumentSettings.add(setting);
        }

        return matchingDocumentSettings;
    }

}