/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test PrintScheduler
//-----------------------------
@isTest
private class PrintSchedulerTest {
    //verify that a job is scheduled successfully in a future date. 
    @isTest
    private static void jobShouldBeScheduledToRunInFuture() {
        Test.startTest();
        String jobId = System.schedule('PrintSchedulerTest',
                                       '0 0 0 15 3 ? 2032', 
                                       new PrintScheduler()); 
        System.debug('jobId'+jobId);
        PrintScheduler printsch = new PrintScheduler();
        printsch.getJobNamePrefix();
        Test.stopTest();
        
        system.assertEquals(1, [SELECT count() FROM CronTrigger where id = :jobId],
                            'A job should be scheduled');
    }
}