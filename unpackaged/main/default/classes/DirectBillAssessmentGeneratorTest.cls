/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test DirectBillAssessmentGenerator
//-----------------------------
@isTest
private class DirectBillAssessmentGeneratorTest {
    
    private static Integer currentYear 		            = FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastRollYear 	            = currentYear-1;
    private static Integer lastToLastYear 	            = lastRollYear-1;
    private static Integer lastFromLastToLast			= lastRollYear-2;
    private static Integer nextYear 		            = currentYear+1;
    private static Date eventDate                       = Date.newInstance(lastRollYear, 1, 1);
    private static Date eventDateLastToLastYear         = Date.newInstance(lastToLastYear, 1, 1);
    private static Date eventDateLastFromLastToLastYear = Date.newInstance(lastFromLastToLast, 1, 1);
    
    @testSetup
    private static void dataSetup(){
        
        TestDataUtility.disableAutomationCustomSettings(true);
        //insert roll years
        List<RollYear__c> rollYears = new List<RollYear__c>();
        RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYears.add(rollYearCurrentFiscalYear);
        
        RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastRollYear),String.valueof(lastRollYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYears.add(rollYearLastFiscalYear);
        
        RollYear__c rollYearLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastToLastYear),String.valueof(lastToLastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYears.add(rollYearLastToLastFiscalYear);
        
        RollYear__c rollYearFromLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastFromLastToLast),String.valueof(lastFromLastToLast),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
        rollYears.add(rollYearFromLastToLastFiscalYear);
        
        RollYear__c rollYearNextFiscalYear = TestDataUtility.buildRollYear(String.valueof(nextYear),String.valueof(nextYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_PENDING_STATUS);
        rollYears.add(rollYearNextFiscalYear);        
        insert rollYears;
        
        List<Penalty__c> penalties = new List<Penalty__c>();
        Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463); 
        penalties.add(penalty);
        
        Penalty__c penalty1 = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_506);
        penalties.add(penalty1);
        insert penalties;        
        
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAccount = TestDataUtility.buildAccount('Johansson&Jon', CCSFConstants.NOTICE_TYPE_DIRECT_BILL, businessAccountRecordTypeId);
        Account objAccount1 = TestDataUtility.buildAccount('Johansson', CCSFConstants.NOTICE_TYPE_DIRECT_BILL, businessAccountRecordTypeId);
        Account objAccount2 = TestDataUtility.buildAccount('Milky', CCSFConstants.NOTICE_TYPE_DIRECT_BILL, businessAccountRecordTypeId);
        Account objAccount3 = TestDataUtility.buildAccount('Glaxy', CCSFConstants.NOTICE_TYPE_DIRECT_BILL, businessAccountRecordTypeId);
        Account objAccount4 = TestDataUtility.buildAccount('US Marshal', CCSFConstants.NOTICE_TYPE_DIRECT_BILL, businessAccountRecordTypeId);
        
        insert new List<Account>{ objAccount, objAccount1,objAccount2,objAccount3,objAccount4 };
            
         
        Id PersonalPropRecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId();
        Property__c objProperty  = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount.Id, '11-123456', 'Johansson&Jon', null);
        Property__c objProperty1  = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount1.Id, '11-123455', 'Johansson', null);
        Property__c objProperty2  = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount2.Id, '11-123465', 'Milky', null);
        Property__c objProperty3  = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount3.Id, '12-123465', 'Glaxy', null);
        Property__c objProperty4  = TestDataUtility.buildProperty(PersonalPropRecordTypeId, objAccount4.Id, '22-123465', 'Marshal', null);
        
        insert new List<Property__c>{ objProperty,objProperty1,objProperty2,objProperty3, objProperty4 }; 
            
        Case assessment = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),objProperty.Id,
                                                              rollYearLastFiscalYear.Id,rollYearLastFiscalYear.Name,rollYearLastFiscalYear.Name,
                                                              CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        assessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        
        Case assessment1 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME),objProperty1.Id,
                                                           rollYearLastFiscalYear.Id,rollYearLastFiscalYear.Name,rollYearLastFiscalYear.Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        
        assessment1.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        Case assessment2 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME),objProperty2.Id,
                                                           rollYearFromLastToLastFiscalYear.Id,rollYearFromLastToLastFiscalYear.Name,rollYearFromLastToLastFiscalYear.Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDateLastFromLastToLastYear);
        
        assessment2.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        Case assessment3 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME),objProperty2.Id,
                                                           rollYearLastToLastFiscalYear.Id,rollYearLastToLastFiscalYear.Name,rollYearLastToLastFiscalYear.Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDateLastToLastYear);
        
        assessment3.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        Case assessment4 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),objProperty2.Id,
                                                              rollYearLastFiscalYear.Id,rollYearLastFiscalYear.Name,rollYearLastFiscalYear.Name,
                                                              CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        assessment4.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        
        Case assessment5 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),objProperty3.Id,
                                                              rollYearLastFiscalYear.Id,rollYearLastFiscalYear.Name,rollYearLastFiscalYear.Name,
                                                              CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,penalty.Id,eventDate);
        assessment5.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        assessment5.ApplyPenaltyReason__c = 'Test Class';
        assessment5.ExemptionPenalty__c = penalty.Id;
        
        Case assessment6 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME),objProperty4.Id,
                                                           rollYearLastToLastFiscalYear.Id,rollYearLastToLastFiscalYear.Name,rollYearLastToLastFiscalYear.Name,
                                                           CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDateLastToLastYear);
        
        assessment6.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
      	Test.startTest();
        insert new List<Case>{ assessment,assessment1,assessment2,assessment3, assessment4,assessment5, assessment6 }; 
        AssessmentLineItem__c objAssessmentLineItem = TestDataUtility.buildAssessmentLineItem('2017', 2000,assessment.id);
        AssessmentLineItem__c objAssessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('2017', 2000,assessment1.id);
        AssessmentLineItem__c objAssessmentLineItem2 = TestDataUtility.buildAssessmentLineItem('2017', 2000,assessment4.id);
        AssessmentLineItem__c objAssessmentLineItem3 = TestDataUtility.buildAssessmentLineItem('2017', 2000,assessment5.id);
        insert new List<AssessmentLineItem__c>{ objAssessmentLineItem,objAssessmentLineItem1,objAssessmentLineItem2,objAssessmentLineItem3 }; 
      	
        Case assessment7 = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),objProperty4.Id,
                                                               rollYearLastFiscalYear.Id,rollYearLastFiscalYear.Name,rollYearLastFiscalYear.Name,
                                                               CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,eventDate);
        assessment7.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
        assessment7.ParentId = assessment6.Id;
        insert assessment7;
        Test.stopTest();
        TestDataUtility.disableAutomationCustomSettings(false);
        
    }
    
    @isTest
    static void cloneCasesWithLineItemsTest1() {
        // Lets Assume
        // FiscalYearUtility.getCurrentFiscalYear() = 2020
        // lastRollYear = FiscalYearUtility.getCurrentFiscalYear() - 1 = 2019
        // currentYear = lastRollYear + 1 = 2020
        
        Test.startTest();
        Database.executeBatch(new DirectBillAssessmentGenerator());
        Test.stopTest();		
        
        // Assert the functionality
        // Assessment Line Item's count should be 2 i.e. 1 original + 1 clone line item = 2        
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Johansson&Jon' LIMIT 1].Id;
        System.assertEquals(2, [SELECT count() FROM AssessmentLineItem__c where Case__r.Property__c =: propId]);
    } 
  
  @isTest
    static void directBillLegacyAssessment() {
        // Lets Assume
        // FiscalYearUtility.getCurrentFiscalYear() = 2020
        // lastRollYear = FiscalYearUtility.getCurrentFiscalYear() - 1 = 2019
        // currentYear = lastRollYear + 1 = 2020
        
        
      Test.startTest(); 
      Database.executeBatch(new DirectBillAssessmentGenerator());
      Test.stopTest();
        
        // Assert the functionality
        // Assessment Line Item's count should be 2 i.e. 1 original + 1 clone line item = 2
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Johansson' LIMIT 1].Id;
        System.assertEquals(2, [SELECT count() FROM AssessmentLineItem__c where Case__r.Property__c =: propId]);
        //  new direct bill Assessment will have RecordType BPP
        System.assertEquals(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME), [Select RecordTypeId from Case where Property__c =: propId and AssessmentYear__c =: String.valueof(currentYear)].RecordTypeId);
    } 
   
    @isTest
    static void cloneCasesWithLineItemsTest3() {
        // Lets Assume
        // FiscalYearUtility.getCurrentFiscalYear() = 2020
        // lastRollYear = FiscalYearUtility.getCurrentFiscalYear() - 1 = 2019
        // currentYear = lastRollYear + 1 = 2020
        
    
        Test.startTest();
        Database.executeBatch(new DirectBillAssessmentGenerator());        
        Test.stopTest();
        
        // Assert the functionality
        // Assessment Line Item's count should be 2 i.e. 1 original line item of objCase + 1 clone line item = 2
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Milky' LIMIT 1].Id;
        System.assertEquals(2, [SELECT count() FROM AssessmentLineItem__c where Case__r.Property__c =: propId]);
     
    } 
   
    @isTest
    static void cloneCasesWithLineItemsTest4() {
        // Lets Assume
        // FiscalYearUtility.getCurrentFiscalYear() = 2020
        // lastRollYear = FiscalYearUtility.getCurrentFiscalYear() - 1 = 2019
        // currentYear = lastRollYear + 1 = 2020
        
     	Test.startTest();
        Database.executeBatch(new DirectBillAssessmentGenerator());
        Test.stopTest();
        
        // Assert the functionality
        // Assessment Line Item's count should be 2 i.e. 1 original line item of objCase + 1 clone line item = 3
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'US Marshal' LIMIT 1].Id;
        System.assertEquals(3, [SELECT count() FROM case where Property__c =: propId ]);
    } 
    
    @isTest
    static void cloneCasesWithLineItemsTest5() {
        // Lets Assume
        // FiscalYearUtility.getCurrentFiscalYear() = 2020
        // lastRollYear = FiscalYearUtility.getCurrentFiscalYear() - 1 = 2019
        // currentYear = lastRollYear + 1 = 2020
        
        Test.startTest();     
        Database.executeBatch(new DirectBillAssessmentGenerator());
        Test.stopTest();
        
        // Assert the functionality
        // On Cloned Assessment record a.k.a Case record Penalty__c & ExemptionPenalty__c should set to null
        
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Glaxy' LIMIT 1].Id;
       
        Case clonedCase = [SELECT Id, Penalty__c,Property__c, ExemptionPenalty__c, AdjustmentType__c,ExemptAmountManual__c,ExemptionCode__c,ExemptionDate__c,ExemptionType__c,ExemptionSubtype__c,Roll__r.Year__c
                           FROM Case 
                           WHERE RollYear__c =: String.valueOf(currentYear)
                           AND Property__c =: propId];
        System.assertEquals(null, clonedCase.Penalty__c);
        System.assertEquals(null, clonedCase.ExemptionPenalty__c);
        System.assertEquals(null, clonedCase.ExemptAmountManual__c);
        System.assertEquals(null, clonedCase.ExemptionCode__c);
        System.assertEquals(null, clonedCase.ExemptionDate__c);
        System.assertEquals(null, clonedCase.ExemptionType__c);
        System.assertEquals(null, clonedCase.ExemptionSubtype__c);
        System.assertEquals(CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW, clonedCase.AdjustmentType__c);
        System.assertEquals(String.valueof(currentYear), clonedCase.Roll__r.Year__c);
    } 

  
    @isTest
    static void errorFrameworkTest(){
        Id propId = [SELECT Id,Account__r.Name FROM Property__c where Account__r.Name = 'Johansson&Jon' LIMIT 1].Id;
        
        Case assessment = [Select Id from Case where property__c =:propId limit 1];
        
        
        Id testJobId = '707S000000nKE4fIAG';
        BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
        testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testJob.JobCreatedDate__c = System.today();
        testJob.JobId__c = testJobId;
        database.insert(testJob);        
        
        BatchApexError__c testError = new BatchApexError__c();
        testError.AsyncApexJobId__c = testJobId;
        testError.BatchApexErrorLog__c = testJob.Id;
        testError.DoesExceedJobScopeMaxLength__c = false;
        testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
        testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testError.JobScope__c = assessment.Id;
        testError.Message__c = 'Test exception';
        testError.RequestId__c = null;
        testError.StackTrace__c = '';
        database.insert(testError);
        
        
        BatchableError batchError = BatchableError.newInstance(testError);
        DirectBillAssessmentGenerator batchRetry= new DirectBillAssessmentGenerator();
        batchRetry.handleErrors(batchError);
        system.assertEquals('707S000000nKE4fIAG', testError.AsyncApexJobId__c);
    } 
  
}