/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class StatementComparatorControllerTest {

    private static Integer currentYear     = FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear        = currentYear-1;
    private static Integer lastToLastYear  = lastYear-1;
    private static Integer nextYear        = currentYear+1;
    private static Date filingDate         = Date.parse('01/01/'+currentYear);
    
    @testSetup
    private static void commonDataSetupForTesting() {
       
        //Create Users
        List<User> users = new List<User>();
        User principal = TestDataUtility.getPrincipalUser();
        users.add(principal);
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin);  
        User auditor = TestDataUtility.getBPPAuditorUser();
        users.add(auditor); 
        //Insert Users
        insert users;
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        System.runAs(systemAdminUser) {

            //Insert Account
            Account account = TestDataUtility.getBusinessAccount();
            insert account;
                        
            //Insert Property
            Property__c property = TestDataUtility.getBPPProperty();
            property.Account__c = account.Id;
            insert property;           
            
            //Insert Roll Years
            List<RollYear__c> rollyears = new List<RollYear__c>();
            RollYear__c currentRollYear = new RollYear__c();
            currentRollYear.Name = String.valueOf(filingDate.Year());
            currentRollYear.Year__c = String.valueOf(filingDate.Year());
            currentRollYear.Status__c = CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS ;
            rollyears.add(currentRollYear);
            RollYear__c lastRollYear = new RollYear__c();
            lastRollYear.Name = String.valueOf(filingDate.Year()-1);
            lastRollYear.Year__c = String.valueOf(filingDate.Year()-1);
            lastRollYear.Status__c = CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS ;
            rollyears.add(lastRollYear);
            insert rollyears;

            //Insert Penalty
            Penalty__c penalty463 = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty463;

            Test.startTest();

            //Insert Assessment
            Case cas  = TestDataUtility.buildAssessment (DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property.Id,
                                                        rollyears.get(1).Id,rollyears.get(1).Name,rollyears.get(1).Name,CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,
                                                        CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,filingDate.addYears(-1));
            cas.islocked__c = true;
            insert cas;
                        
             //Create Assessment Line Items
            List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
            AssessmentLineItem__c assessmentLineItem1 = new AssessmentLineItem__c();
            assessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('Alternate Schedule A', 'ATMs', '2017', 100.00, cas.id, 'Assessment Line Item for Current year');
            assessmentLineItems.add(assessmentLineItem1);
            AssessmentLineItem__c assessmentLineItem2 = new AssessmentLineItem__c();
            assessmentLineItem2 = TestDataUtility.buildAssessmentLineItem('Construction in Progress', 'Construction in Progress', '2016', 100.00, cas.id, 'Assessment Line Item for Current year');
            assessmentLineItems.add(assessmentLineItem2);
            //Insert Assessment Line Items        
            insert assessmentLineItems;
            
            //Create Statement         
            Map<String,String> inputParamsStatement = new Map<String,String>();            
            inputParamsStatement.put('accountId',account.Id);
            inputParamsStatement.put('propertyId',property.Id);
            inputParamsStatement.put('formType','571-L');
            inputParamsStatement.put('businessScenario','Existing');
            inputParamsStatement.put('propertyScenario','Existing');
            inputParamsStatement.put('assessmentYear',String.valueOf(lastYear)); 
            inputParamsStatement.put('recordTypeId',DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME));
            inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/'+lastToLastYear));
            inputParamsStatement.put('filingDate',String.valueOf('01/03/'+lastYear));            
            Statement__c statement = TestDataUtility.buildStatement(inputParamsStatement);
            //Insert Statement  
            insert statement;
            
            //Create Statement Reported Asset
            Id statementReportedOwnedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.OWNED_ASSETS_RECORD_TYPE_API_NAME);   
            List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
            StatementReportedAsset__c statementReportedAsset1 = new StatementReportedAsset__c();
            statementReportedAsset1 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Alternate Schedule A', 'ATMs', '2017', 100, null, statement.id, 'Statement Reported Asset for last year');
            statementReportedAssets.add(statementReportedAsset1);        
            StatementReportedAsset__c statementReportedAsset2 = new StatementReportedAsset__c();
            statementReportedAsset2 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Alternate Schedule A', 'ATMs', '2016', 100, null, statement.id, 'Statement Reported Asset for last year');        
            statementReportedAssets.add(statementReportedAsset2);
            //Insert Statement Reported Asset
            insert statementReportedAssets;

            Test.stopTest();
        }    
    }
    
    @isTest
    private static void fetchAndSaveRecordsForLineItemsToLineItems() {
        Case lastYearFiledCase = [select id, property__c, AccountId from case];
        //Insert Assessment
        Case currentYearFiledCase  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),lastYearFiledCase.Property__c,
                                                                    null,'','',CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,System.today());
            
        insert currentYearFiledCase;
       
        List<Map<String,object>> displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledCase.id, 'LineItemToLineItemComparator');
        System.assertEquals(2, displayRecords.size());

        //Create Assessment Line Items
        List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
        AssessmentLineItem__c assessmentLineItem1 = new AssessmentLineItem__c();
        assessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('Alternate Schedule A', 'ATMs', '2017', 100.00, currentYearFiledCase.id, 'Assessment Line Item for Current year');
        assessmentLineItems.add(assessmentLineItem1);
        AssessmentLineItem__c assessmentLineItem3 = new AssessmentLineItem__c();
        assessmentLineItem3 = TestDataUtility.buildAssessmentLineItem('Construction in Progress', CCSFConstants.Leasehold_Improvements_Structure, '2020', 100.00, currentYearFiledCase.id, 'Assessment Line Item for Current year');
        assessmentLineItems.add(assessmentLineItem3);
        AssessmentLineItem__c assessmentLineItem4 = new AssessmentLineItem__c();
        assessmentLineItem4 = TestDataUtility.buildAssessmentLineItem('Construction in Progress', CCSFConstants.Leasehold_Improvements_Structure, '2012', 100.00, currentYearFiledCase.id, 'Assessment Line Item for Current year');
        assessmentLineItem4.AssetSubclassification__c = 'General';
	assessmentLineItems.add(assessmentLineItem4);
        AssessmentLineItem__c assessmentLineItem5 = new AssessmentLineItem__c();
        assessmentLineItem5 = TestDataUtility.buildAssessmentLineItem('Construction in Progress', CCSFConstants.Leasehold_Improvements_Structure, '2016', 100.00, currentYearFiledCase.id, 'Assessment Line Item for Current year');
        assessmentLineItems.add(assessmentLineItem5);
        //Insert Assessment Line Items        
        insert assessmentLineItems;

        displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledCase.id, 'LineItemToLineItemComparator');
        System.assertEquals(5, displayRecords.size());
        for(Map<String,object> valuesByLabels:displayRecords){
            valuesByLabels.put('This Year Description','Changed Description');
            valuesByLabels.put('This Year\'s Assessed Cost','100');
        }
        StatementComparatorController.saveUpdatedRecords(currentYearFiledCase.id, displayRecords, 'LineItemToLineItemComparator');
        assessmentLineItems = [select id, Cost__c, AssetSubclassification__c,Description__c from AssessmentLineItem__c where Case__c =:currentYearFiledCase.id];        
        System.assertEquals(5,assessmentLineItems.size());
        for(AssessmentLineItem__c assessmentLineItem:assessmentLineItems) {            
	    System.assertEquals('General', assessmentLineItem.AssetSubclassification__c);	
            System.assertEquals(100, assessmentLineItem.Cost__c);
            System.assertEquals('Changed Description', assessmentLineItem.Description__c);
        }
    }
    
    @isTest
    private static void fetchAndSaveLineItemsToStatementReports() {
        Case lastYearFiledCase = [select id, property__c, AccountId from case];
        //Insert Statement
        Statement__c currentYearFiledStatement = new Statement__c();
        currentYearFiledStatement.Property__c = lastYearFiledCase.Property__c;
        currentYearFiledStatement.FileDate__c = System.today();
        currentYearFiledStatement.RecordTypeId = DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);
        currentYearFiledStatement.Status__c = CCSFConstants.STATEMENT.STATUS_PROCESSED;
        currentYearFiledStatement.Form__c = '571-L';
        currentYearFiledStatement.FilingMethod__c = 'eFile';
        insert currentYearFiledStatement;
        
        Test.startTest();
        List<Map<String,object>> displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledStatement.id, 'LineItemToStatementComparator');
        System.assertEquals(2, displayRecords.size());
        
        //Create Statement Reported Asset
        Id statementReportedOwnedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.OWNED_ASSETS_RECORD_TYPE_API_NAME);   
        List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
        StatementReportedAsset__c statementReportedAsset1 = new StatementReportedAsset__c();
        statementReportedAsset1 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Alternate Schedule A', 'ATMs', '2017', 100, null, currentYearFiledStatement.id, 'Statement Reported Asset for last year');
        statementReportedAssets.add(statementReportedAsset1);        
        StatementReportedAsset__c statementReportedAsset2 = new StatementReportedAsset__c();
        statementReportedAsset2 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Equipment', 'Personal Computers', '2016', 100, null, currentYearFiledStatement.id, 'Statement Reported Asset for last year');        
        statementReportedAssets.add(statementReportedAsset2);
        //Insert Statement Reported Asset
        insert statementReportedAssets;
        
        displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledStatement.id, 'LineItemToStatementComparator');
        System.assertEquals(3, displayRecords.size());
        for(Map<String,object> valuesByLabels:displayRecords){
            valuesByLabels.put('Description of This Year\'s Reported Cost','Changed Description');
            valuesByLabels.put('This Year\'s Reported Cost','35');
        }
        
        StatementComparatorController.saveUpdatedRecords(currentYearFiledStatement.id, displayRecords, 'LineItemToStatementComparator');
        Test.stopTest();
        statementReportedAssets = [select id, Cost__c, Description__c from StatementReportedAsset__c where statement__c =:currentYearFiledStatement.id];
        System.assertEquals(3,statementReportedAssets.size());
        for(StatementReportedAsset__c statementReportedAsset:statementReportedAssets) {           
            System.assertEquals(35, statementReportedAsset.Cost__c);
            System.assertEquals('Changed Description', statementReportedAsset.Description__c);
        }    
    }
    
    @isTest
    private static void fetchAndSaveStatementReportsToStatementReports() {
        Statement__c lastYearFiledStatement = [select id, property__c from Statement__c]; 
        //Update Statement       
        lastYearFiledStatement.RecordTypeId = DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);
        lastYearFiledStatement.Status__c = CCSFConstants.STATEMENT.STATUS_PROCESSED;
        lastYearFiledStatement.VIP_Date_Time_Submitted__c = System.today().addYears(-1);       
        update lastYearFiledStatement;
        //Insert Statement
        Statement__c currentYearFiledStatement = new Statement__c();
        currentYearFiledStatement.Property__c = lastYearFiledStatement.Property__c;
        currentYearFiledStatement.FileDate__c = System.today();
        currentYearFiledStatement.Form__c = '571-L';
        currentYearFiledStatement.RecordTypeId = DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);
        insert currentYearFiledStatement;
        
        List<Map<String,object>> displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledStatement.id, 'StatementToStatementComparator');        
        System.assertEquals(2, displayRecords.size());        

        //Create Statement Reported Asset
        Id statementReportedOwnedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.OWNED_ASSETS_RECORD_TYPE_API_NAME);   
        List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
        StatementReportedAsset__c statementReportedAsset1 = new StatementReportedAsset__c();
        statementReportedAsset1 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Alternate Schedule A', 'ATMs', '2017', 100, null, currentYearFiledStatement.id, 'Statement Reported Asset for last year');
        statementReportedAssets.add(statementReportedAsset1);        
        StatementReportedAsset__c statementReportedAsset2 = new StatementReportedAsset__c();
        statementReportedAsset2 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Equipment', 'Personal Computers', '2016', 100, null, currentYearFiledStatement.id, 'Statement Reported Asset for last year');        
        statementReportedAssets.add(statementReportedAsset2);
        //Insert Statement Reported Asset
        insert statementReportedAssets;

        displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledStatement.id, 'StatementToStatementComparator');        
        System.assertEquals(3, displayRecords.size());
        
        for(Map<String,object> valuesByLabels:displayRecords){
            valuesByLabels.put('Description of This Year\'s Reported Cost','Changed Description');
            valuesByLabels.put('This Year\'s Reported Cost','25');
        }
         StatementComparatorController.saveUpdatedRecords(currentYearFiledStatement.id, displayRecords, 'StatementToStatementComparator');
        Test.startTest();
           
            //Update Statement
            currentYearFiledStatement.Status__c = CCSFConstants.STATEMENT.STATUS_PROCESSED;
            currentYearFiledStatement.VIP_Date_Time_Submitted__c = System.today();
            update currentYearFiledStatement;
        Test.stopTest(); 
        statementReportedAssets = [select id, Cost__c, Description__c from StatementReportedAsset__c where statement__c =:currentYearFiledStatement.id];
        System.assertEquals(3,statementReportedAssets.size());        
        
        for(StatementReportedAsset__c statementReportedAsset:statementReportedAssets) {            
            System.assertEquals(25, statementReportedAsset.Cost__c);
            System.assertEquals('Changed Description', statementReportedAsset.Description__c);
        }
    }
    
    @isTest
    private static void fetchAndSaveStatementReportedAssetsToLineItems() {
        TestDataUtility.disableAutomationCustomSettings(true);
        Statement__c currentYearFiledStatement = [select id, property__c,FileDate__c, property__r.Account__c from Statement__c];
        currentYearFiledStatement.FileDate__c = System.today();
        currentYearFiledStatement.RecordTypeId = DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);
        currentYearFiledStatement.Status__c = CCSFConstants.STATEMENT.STATUS_PROCESSED;
        currentYearFiledStatement.VIP_Date_Time_Submitted__c = System.today();      

        update currentYearFiledStatement;
        TestDataUtility.disableAutomationCustomSettings(false);
        Test.startTest();
        //Insert Assessment
        Case currentYearFiledCase  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),currentYearFiledStatement.Property__c,
                                                                    null,'','',CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,System.today());
            
        insert currentYearFiledCase;

        List<Map<String,object>> displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledCase.id, 'StatementToLineItemComparator');
        
        System.assertEquals(2, displayRecords.size());
        //Create Assessment Line Items
        List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
        AssessmentLineItem__c assessmentLineItem1 = new AssessmentLineItem__c();
        assessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('Supplies', 'Supplies', '2017', 100.00, currentYearFiledCase.id, 'Assessment Line Item for Current year');
        assessmentLineItems.add(assessmentLineItem1);
        AssessmentLineItem__c assessmentLineItem2 = new AssessmentLineItem__c();
        assessmentLineItem2 = TestDataUtility.buildAssessmentLineItem('Construction in Progress', 'Construction in Progress', '2016', 100.00, currentYearFiledCase.id, 'Assessment Line Item for Current year');
        assessmentLineItem2.AssetSubclassification__c = 'General';
        assessmentLineItems.add(assessmentLineItem2);
        //Insert Assessment Line Items
        insert assessmentLineItems;
        displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledCase.id, 'StatementToLineItemComparator');
        
        System.assertEquals(4, displayRecords.size());
        boolean deleteAddedOnce = false;
        for(Map<String,object> valuesByLabels:displayRecords){
            valuesByLabels.put('This Year\'s Assessed Cost','100');
            if(valuesByLabels.get('readRecord') == null && !deleteAddedOnce){
                deleteAddedOnce = true;
                valuesByLabels.put('toBeDeleted',true);
            }
        }
        Test.stopTest(); 
        StatementComparatorController.saveUpdatedRecords(currentYearFiledCase.id, displayRecords, 'StatementToLineItemComparator');
        assessmentLineItems = [select id, Cost__c,AssetSubclassification__c, Description__c from AssessmentLineItem__c where Case__c =:currentYearFiledCase.id];
               
        System.assertEquals(3,assessmentLineItems.size());
        for(AssessmentLineItem__c assessmentLineItem:assessmentLineItems){
            System.assertEquals('General', assessmentLineItem.AssetSubclassification__c);
            System.assertEquals(100, assessmentLineItem.Cost__c);            
        }       
    }    
    
    @isTest
    private static void fetchFormTypeFromStatement() {
        Statement__c currentYearFiledStatement = [select id, Form__c from Statement__c];
        Test.startTest();
        String formType = StatementComparatorController.getFormTypeOfStatement(currentYearFiledStatement.id);
        System.assertEquals(currentYearFiledStatement.Form__c, formType);
        Test.stopTest();
    }

    @isTest
    private static void fetchAndSaveLeasedLineItemsToStatementReports() {
        Case lastYearFiledCase = [select id, property__c, AccountId from case];
        TestDataUtility.disableAutomationCustomSettings(true);
        //Insert Statement
        Statement__c currentYearFiledStatement = new Statement__c();
        currentYearFiledStatement.Property__c = lastYearFiledCase.Property__c;
        currentYearFiledStatement.FileDate__c = System.today();
        currentYearFiledStatement.RecordTypeId = DescribeUtility.getRecordTypeId(Statement__c.sObjectType, CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME);
        currentYearFiledStatement.Status__c = CCSFConstants.STATEMENT.STATUS_PROCESSED;
        currentYearFiledStatement.Form__c = CCSFConstants.PROPERTY.PRIMARY_FORM_TYPE_571_L;
        insert currentYearFiledStatement;
        
        StatementComparatorController.getStatementRecordType(currentYearFiledStatement.id);
     
        List<Map<String,object>> displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledStatement.id, 'LeasedLineItemToStatementComparator');
        System.assertEquals(0, displayRecords.size());
        
        //Create Leased Statement Reported Asset
        Id statementReportedLeasedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.LEASED_ASSETS_RECORD_TYPE_API_NAME);   
        List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
        StatementReportedAsset__c statementReportedAsset1 = new StatementReportedAsset__c();
        statementReportedAsset1 = TestDataUtility.buildStatementReportedAsset(statementReportedLeasedAssetsRecordTypeId, '571-L', 'Alternate Schedule A', 'ATMs', '2017', 100, null, currentYearFiledStatement.id, 'Statement Reported Asset for last year');
        statementReportedAssets.add(statementReportedAsset1);        
        StatementReportedAsset__c statementReportedAsset2 = new StatementReportedAsset__c();
        statementReportedAsset2 = TestDataUtility.buildStatementReportedAsset(statementReportedLeasedAssetsRecordTypeId, '571-L', 'Equipment', 'Personal Computers', '2016', 100, null, currentYearFiledStatement.id, 'Statement Reported Asset for last year');        
        statementReportedAssets.add(statementReportedAsset2);
        //Insert Statement Reported Asset
        insert statementReportedAssets;
        TestDataUtility.disableAutomationCustomSettings(false);
        displayRecords = StatementComparatorController.getRecordsForDisplay(currentYearFiledStatement.id, 'LeasedLineItemToStatementComparator');
        System.assertEquals(2, displayRecords.size());
        for(Map<String,object> valuesByLabels:displayRecords){
            valuesByLabels.put('Description of This Year\'s Reported Cost','Changed Description');
            valuesByLabels.put('This Year\'s Reported Cost','35');
        }
        Test.startTest();
        StatementComparatorController.saveUpdatedRecords(currentYearFiledStatement.id, displayRecords, 'LeasedLineItemToStatementComparator');
        Test.stopTest();
        
        statementReportedAssets = [select id, Cost__c, Description__c from StatementReportedAsset__c where statement__c =:currentYearFiledStatement.id];
        System.assertEquals(2,statementReportedAssets.size());
        for(StatementReportedAsset__c statementReportedAsset:statementReportedAssets) {           
            System.assertEquals(100, statementReportedAsset.Cost__c);
            System.assertEquals('Statement Reported Asset for last year', statementReportedAsset.Description__c);
        }
        
    }
    
    @isTest
    private static void fetchStatementReportedAssetsRecordTypes() {        
        Test.startTest();
        List<String> statementReportedAssetsRecordTypes = StatementComparatorController.getstatementReportedAssetsRecordTypes();
        System.assertEquals(2, statementReportedAssetsRecordTypes.size());
        Test.stopTest();
    }    
    @isTest
    private static void updateStatementCertifiedNoAssetLineItemsCBTest() {        
        Test.startTest();
        Statement__c statement = [select id, property__c from Statement__c limit 1]; 
        StatementComparatorController.updateStatementCertifiedNoAssetLineItemsCB(statement.id,true);
        Statement__c updatedStatement = [select id, CertifiedNoAssetLineItems__c from Statement__c where id=: statement.id limit 1]; 
        System.assertEquals(true, updatedStatement.CertifiedNoAssetLineItems__c);
        Test.stopTest();
    }
    @isTest
    private static void updateStatementNoPropertyBelongingtoOthersCBTest() {        
        Test.startTest();
        Statement__c statement = [select id, property__c from Statement__c limit 1]; 
        StatementComparatorController.updateStatementNoPropertyBelongingtoOthersCB(statement.id,true);
        Statement__c updatedStatement = [select id, NoPropertyBelongingtoOthers__c from Statement__c where id=: statement.id limit 1]; 
        System.assertEquals(true, updatedStatement.NoPropertyBelongingtoOthers__c);
        Test.stopTest();
    } 
}