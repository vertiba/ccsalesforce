/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class PropertyDocumentRequestGenerator implements Database.Batchable<SObject>, BatchableErrorHandler {

    private static final String LOG_LOCATION = 'PropertyDocumentRequestGenerator';
    private static final String SOBJECT_NAME = Schema.Property__c.SObjectType.getDescribe().getName();

    private String query;

    public PropertyDocumentRequestGenerator(String query) {
        this.query = query;
        System.debug('query+++ '+query);
    }

    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        Logger.addDebugEntry('starting to process query: ' + this.query, LOG_LOCATION);
        Logger.saveLog();
        return Database.getQueryLocator(this.query);
    }

    public void execute(Database.BatchableContext batchableContext, List<Property__c> scope) {
        try {
            // Requery the records to ensure we have the necessary fields
            scope = [SELECT Id, HasMailingAddress__c, MailingAddress__c, PrimaryFormType__c, Status__c,
                     ContactEmail__c,NoticeType__c FROM Property__c 
                     WHERE Id IN :scope And RecordType.DeveloperName = 'Vessel' 
                     And HasMailingAddress__c = true 
                     And RequiredToFile__c != null And RequiredToFile__c != 'No'
                     And Status__c != 'Inactive']; 

            Map<String, TH1__Document_Setting__c> documentSettingsBySObjectName = SmartCOMMUtility.getDocumentSettingsBySObjectName(SOBJECT_NAME);		
            Logger.addDebugEntry(String.valueOf(documentSettingsBySObjectName), LOG_LOCATION);
            List<DocumentGenerationRequest__c> documentGenerationRequests = new List<DocumentGenerationRequest__c>();
            TH1__Document_Setting__c documentSetting, emailDocumentSetting;
            
            if(!documentSettingsBySObjectName.isEmpty())
            {                 
            for(Property__c property : scope) {
                    documentSetting 	 = null;
                    emailDocumentSetting = null;
                    if(property.NoticeType__c == 'Direct Bill') {
                        if(documentSettingsBySObjectName.containsKey(Label.PropertyDirectBillNotice576D)){
                            documentSetting = documentSettingsBySObjectName.get(Label.PropertyDirectBillNotice576D);
                        }                       
                        if(property.ContactEmail__c !=null && documentSettingsBySObjectName.containsKey(Label.PropertyDirectBillNotice576DEmail)){
                            emailDocumentSetting = documentSettingsBySObjectName.get(Label.PropertyDirectBillNotice576DEmail);
                        }                                             
                    } else if(property.NoticeType__c == 'Low Value') {
                        if(documentSettingsBySObjectName.containsKey(Label.PropertyLowValueNotice576D)){
                            documentSetting = documentSettingsBySObjectName.get(Label.PropertyLowValueNotice576D);
                        }                       
                        if(property.ContactEmail__c !=null && documentSettingsBySObjectName.containsKey(Label.PropertyDirectBillNotice576DEmail)){
                            emailDocumentSetting = documentSettingsBySObjectName.get(Label.PropertyDirectBillNotice576DEmail);
                        }   
                        
                    } else if(property.NoticeType__c == 'Notice to File') {
                        if(documentSettingsBySObjectName.containsKey(Label.PropertyNoticeOfRequirementToFile576D)){
                            documentSetting = documentSettingsBySObjectName.get(Label.PropertyNoticeOfRequirementToFile576D);
                        }                         
                        if(property.ContactEmail__c !=null && documentSettingsBySObjectName.containsKey(Label.PropertyNoticeOfRequirementToFile576DEmail)){
                            emailDocumentSetting = documentSettingsBySObjectName.get(Label.PropertyNoticeOfRequirementToFile576DEmail);
                        }                        
                    } 
                    if(documentSetting != null ){
                        DocumentGenerationRequest__c documentGenerationRequest = SmartCOMMUtility.createDocumentGenerationRequest(property.Id, documentSetting.id);
                    documentGenerationRequests.add(documentGenerationRequest);
                }
                    if(emailDocumentSetting != null ){
                        DocumentGenerationRequest__c documentGenerationRequestEmail = SmartCOMMUtility.createDocumentGenerationRequest(property.Id, emailDocumentSetting.id);
                        documentGenerationRequests.add(documentGenerationRequestEmail);
                    }                    
                }
            }
            if(!documentGenerationRequests.isEmpty())
            {
                insert documentGenerationRequests;
            }
            
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, LOG_LOCATION);
            throw ex;
        }
    }

    public void finish(Database.BatchableContext batchableContext) {
        string JobId = batchableContext.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        string queryParams = JSON.serializePretty(new Map<String,Object>{'query' => this.query});

        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, PropertyDocumentRequestGenerator.class.getName(), asyncList[0].ParentJobId, 
                                           true,queryParams, asyncList[0].NumberOfErrors);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}

}