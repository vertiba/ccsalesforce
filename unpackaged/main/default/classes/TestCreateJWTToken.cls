/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@istest
public class TestCreateJWTToken {

    @istest
    static void testCreateJWTTokenWithDefaultValues(){
        String token = CreateJWTToken.generateJWTToken();
        System.assertEquals('abcdefgh.ijklemnop.qrstuvwxyz1234567890', token);
    }
    
    @istest
    static void testCreateJWTTokenForException(){
        try{
            CreateJWTToken.generateJWTToken(Label.JWT_Token_Subject,Label.JWT_Token_Audience,Label.JWT_Token_Issuer,
                                            0,'', new map<string,object>());
            System.assert(false);//Exception should have been thrown. 
        } catch (Exception e) {
            
        }
    }
}