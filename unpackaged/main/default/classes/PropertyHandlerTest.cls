/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test PropertyHandler
//-----------------------------
@isTest
private class PropertyHandlerTest {
    
    @TestSetup
    static void DataForTest(){
        //Insert Account
        Account account = TestDataUtility.getBusinessAccount();
        insert account;
        
        //Insert Property
        Property__c property = TestDataUtility.getBPPProperty();
        property.Account__c = account.Id;
        property =TestDataUtility.getLocationAddress(property);   
        insert property;       
        
    }
    
    @isTest
    private static void setBasicTest(){
        // Test - setName, setsetAddressFields,
        Property__c resultProperty=[Select Id from Property__c limit 1];
        resultProperty.LocationStreetNumber__c = '1150';
        resultProperty.DoingBusinessAs__c ='Trade Limit';   
        Test.startTest();
        update resultProperty;
        Test.stopTest();
        
        Property__c resultProperty1=[Select Name,LocationAddress__c,Id,PropertyId__c,CalculatedName__c from Property__c where Id=: resultProperty.Id limit 1] ;
        system.assert(resultProperty1.PropertyId__c.startsWith('A5'),'Property Sequence should start with A5');
        system.assert(resultProperty1.Name.contains('Trade Limit'),'Name updated');
        system.assert(resultProperty1.LocationAddress__c.contains('1150'),'Address updated');       
        
        
    }
    @isTest
    private static void bulkPropertyIdCheck(){
        Account account=[Select Id from Account limit 1];
        //Insert Properties
        List<Property__c> properties= new List<Property__c>();
        Property__c property = TestDataUtility.getBPPProperty();
        property.Account__c = account.Id;
        properties.add(property);
        Property__c vesselProperty = TestDataUtility.getVesselProperty();
        vesselProperty.Account__c = account.Id;
        properties.add(vesselProperty);
        insert properties; 
        
        List<Property__c> resultProperty=[Select PropertyId__c,Id from Property__c] ;        
        List<String> propertyIds = new List<String>() ;
        if(!resultProperty.isEmpty()){
            // getting all property Ids
            for(Property__c prop: resultProperty){
                if(prop.PropertyId__c.startsWith('A5')){
                    propertyIds.add(prop.PropertyId__c);
                }            
            }
            system.assertEquals(resultProperty.size(), propertyIds.size(), 'Both Property should be start with A5 series');
        }  
    }
    @isTest
    private static void setPropertyIdAlreadyExistWithStartSequence(){
        // Test - if one property exist already
        Account account=[Select Id from Account limit 1];
        //Insert Property
        Property__c property = TestDataUtility.getBPPProperty();
        property.Account__c = account.Id;
        property =TestDataUtility.getLocationAddress(property);
        insert property; 
        Property__c resultProperty=[Select PropertyId__c,Id from Property__c where Id=:property.Id limit 1] ;
        system.assert(resultProperty.PropertyId__c.startsWith('A5'),'Property Sequence should start with A5');
        
        Property__c property1 = TestDataUtility.getBPPProperty();
        property1.Account__c = account.Id;
        property1 =TestDataUtility.getLocationAddress(property1);
        insert property1; 
        
        Property__c resultProperty1=[Select PropertyId__c,Id from Property__c where Id=:property1.Id limit 1] ;
        system.assert(resultProperty1.PropertyId__c.startsWith('A5'),'Property Sequence should start with A5');
    }
    
    
}