/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//      Class to test AccountHandler
//-----------------------------
@isTest
private class AccountHandlerTest {

    /**
     * @author : Publicis.sapient
     * @description : Setting up an account for testing.
     * @return : void
     */
    @testSetup
    private static void commonDataSetupForTesting(){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account newaccount = TestDataUtility.buildAccount('new test account', 'Notice to File', recordTypeId);
        newaccount.EntityId__c = '55555';
        insert newaccount;
    }

    /**
     * @author : publicis.sapient
     * @description : This method is used to verify that new account has company Id 
     *      automatically populated in account creation.
     * @return : void
     */
    @isTest
    private static void companyIdToBeUpdated(){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account account = TestDataUtility.buildAccount('test account', 'Notice to File', recordTypeId);
        Test.startTest();
        insert account;
        account = [select id, EntityId__c, CompanyIdUnformattedPrefix__c from account where id=: account.Id];
        System.assertEquals('5000000', account.EntityId__c);
        System.assertEquals(5000000, account.CompanyIdUnformattedPrefix__c);
        Test.stopTest();
    }
    
    /**
     * @author : publicis.sapient
     * @description : Testing for negative scenarios where Company Id not to be prefixed,
     *      when company is inactive or for other record types. 
     * @return : void
     */
    @isTest
    private static void companyIdNotToBeUpdated(){
        List<Account> accountList = new List<Account>();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account account1 = TestDataUtility.buildAccount('test account', 'Notice to File', recordTypeId);
        account1.BusinessStatus__c='Inactive';
        account1.BusinessCloseDate__c = System.today().addDays(-30);
        accountList.add(account1);
        
        Account account2 = TestDataUtility.buildAccount('account prefix exists', 'Notice to File', recordTypeId);
        account2.EntityId__c='1234324';
        accountList.add(account2);

        Test.startTest();
        insert accountList;
        accountList = [select id, EntityId__c, CompanyIdUnformattedPrefix__c from account];
        for(Account account : accountList){
            System.assertEquals(null, account.CompanyIdUnformattedPrefix__c);
        }
        Test.stopTest();
    }

    /**
     * @author : publicis.sapient
     * @description : Once entity id is set, then should not be updated. If updated
     *      throw exception and also verify that correct exception is thrown. 
     * @return : void
     */
    @isTest
    private static void throwExceptionIfCompanyIdIsUpdated(){
        Account account1 = [select id, EntityId__c, CompanyIdUnformattedPrefix__c from account where Name='new test account'];
        account1.EntityId__c = '12346';
        Test.startTest();
        try{
            update account1;
            account1 = [select id, EntityId__c, CompanyIdUnformattedPrefix__c from account where Name='new test account'];
        	System.assert(false, 'When Company Id is updated, Exception should have been thrown');
        } catch (Exception e) {
            //should not have failed for another exception.
            System.assert(e.getMessage().contains(Label.CompanyPrefixChangedError));
        }
        //reconfirm, account is not updated.
        Account account2 = [select id, EntityId__c, CompanyIdUnformattedPrefix__c from account where Name='new test account'];
        System.assertEquals('55555', account2.EntityId__c);
        Test.stopTest();
    }
}