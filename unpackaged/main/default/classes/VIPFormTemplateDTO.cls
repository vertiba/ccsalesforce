public without sharing class VIPFormTemplateDTO {

    public static final String STATIC_RESOURCE_PREFIX = 'VIPForm_';

    public VIPForm__VIP_Form_Branding__c branding;
    public VIPForm__VIP_Template__c template;
    public List<VIPForm__VIP_Category__c> categories;

}