@isTest
private class VIPCategoryTest {

    @isTest
    static void it_should_set_uuid() {
        VIPForm__VIP_Category__c category = new VIPForm__VIP_Category__c(
            Name = 'test'
        );
        insert category;

        category = [SELECT Id, Name, Uuid__c FROM VIPForm__VIP_Category__c WHERE Id = :category.Id];
        System.assert(Uuid.isValid(category.Uuid__c));
    }

}