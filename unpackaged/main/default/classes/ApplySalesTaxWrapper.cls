/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//                                                                                                                       
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######  
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##    
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##    
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##    
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##    
//                                                                                                                       
//=======================================================================================================================

/*********************************************
 *           APPLYSALESTAXWRAPPER            *
 * WRAPPER CLASS USE TO SHOW / SAVE / UPDATE *
 *   DATA ON LIGHTNING CMP APPLY SALESTAX    *
 *********************************************/

public with sharing class ApplySalesTaxWrapper {
    
    @AuraEnabled public Boolean isSelected ;
    @AuraEnabled public AssessmentLineItem__c assessmentLineItem ;
    
    public ApplySalesTaxWrapper() {
        this.isSelected         = false;
        this.assessmentLineItem = new AssessmentLineItem__c();
    }

    public ApplySalesTaxWrapper(AssessmentLineItem__c assessmentLineItem) {
        this.isSelected         = false;
        this.assessmentLineItem = assessmentLineItem;
    }

    public ApplySalesTaxWrapper(AssessmentLineItem__c assessmentLineItem, Boolean isSelected) {
        this.isSelected         = isSelected;
        this.assessmentLineItem = assessmentLineItem;
    } 



}