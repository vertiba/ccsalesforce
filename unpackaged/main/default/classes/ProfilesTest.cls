/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class ProfilesTest {

    static List<Profile> getProfiles() {
        return [
            SELECT Id, Name, UserLicenseId, UserLicense.Name, UserType
            FROM Profile
            ORDER BY Name
        ];
    }

    @isTest
    static void it_should_return_all_profiles() {
        List<Profile> expectedProfiles = getProfiles();
        System.assertEquals(expectedProfiles.size(), Profiles.ALL_PROFILES.size());
    }

    @isTest
    static void it_should_return_all_profiles_by_id() {
        Map<Id, Profile> expectedProfilesById = new Map<Id, Profile>();
        for(Profile profile : getProfiles()) {
            expectedProfilesById.put(profile.Id, profile);
        }
        System.assertEquals(expectedProfilesById, Profiles.ALL_PROFILES_BY_ID);
    }

    @isTest
    static void it_should_return_all_profiles_by_name() {
        Map<String, Profile> expectedProfilesByName = new Map<String, Profile>();
        for(Profile profile : getProfiles()) {
            expectedProfilesByName.put(profile.Name, profile);
        }
        System.assertEquals(expectedProfilesByName, Profiles.ALL_PROFILES_BY_NAME);
    }

    @isTest
    static void it_should_return_admin_profile() {
        Profile expectedProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        System.assertEquals(expectedProfile.Id, Profiles.ADMIN.Id);
    }

    @isTest
    static void it_should_return_auditor_profile() {
        Profile expectedProfile = [SELECT Id FROM Profile WHERE Name = :CCSFConstants.OMNICHANNEL.AUDITOR_PROFILE_NAME];
        System.assertEquals(expectedProfile.Id, Profiles.AUDITOR.Id);
    }

    @isTest
    static void it_should_return_exemption_staff_profile() {
        Profile expectedProfile = [SELECT Id FROM Profile WHERE Name = 'Exemptions Staff'];
        System.assertEquals(expectedProfile.Id, Profiles.EXEMPTIONS_STAFF.Id);
    }

    @isTest
    static void it_should_return_taxpayer_profile() {
        Profile expectedProfile = [SELECT Id FROM Profile WHERE Name = 'Taxpayer'];
        System.assertEquals(expectedProfile.Id, Profiles.TAXPAYER.Id);
    }
}