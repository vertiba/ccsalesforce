@isTest
private class VIPComponentTest {

    @isTest
    static void it_should_set_uuid() {
        VIPForm__VIP_Component__c component = new VIPForm__VIP_Component__c(
            Name = 'test'
        );
        insert component;

        component = [SELECT Id, Name, Uuid__c FROM VIPForm__VIP_Component__c WHERE Id = :component.Id];
        System.assert(Uuid.isValid(component.Uuid__c));
    }

}