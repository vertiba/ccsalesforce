/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: BatchApexErrorEventsTest 
 * Description : Test Class to validate the Batch Retry Framework
*/
@isTest
public class BatchApexErrorEventsTest {
    
    @isTest
    public static void batchableComponentsTest(){
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User 4',alias='tuser4',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testuser4@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            List<AsyncApexJob> asyncApexList = new List<AsyncApexJob>();
            Id testJobId = '707S000000nKE4fIAG';
            Test.startTest();
            try{
                BatchableErrorsComponentController.failedJobs();
                asyncApexList = BatchableErrorsComponentController.fetchCurrentApexJobs();
                BatchableErrorsComponentController.retryJob(testJobId);
                Test.stopTest();
            }catch(Exception ex){}
            Test.getEventBus().deliver();
            
            System.assertEquals(true, asyncApexList!=null);
        }
        
    }
    
    @IsTest
    public static void whenJobExceptionThenLogGeneratedAndHandlerCalled() {
        
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User',alias='tuser',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testuser1@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            Account testAccount = new Account(LastName = 'Bad');
            database.insert(testAccount);
            List<Account> testRecords = [Select id,Name,LastName from Account limit 1];
            BatchableErrorTestJob job = new BatchableErrorTestJob(testRecords);
            
            Test.startTest();
            Id jobId = Database.executeBatch(job);
            try { Test.stopTest(); }
            catch(Exception ex) { 
                // don't fail this test if there were errors in the batch job - we want that  
            }
            Test.getEventBus().deliver();
            
            // Then (failures captured)
            System.assertEquals(1, [select id from BatchApexErrorLog__c].size());
            System.assertEquals('Test exception', [select Message__c from BatchApexError__c][0].Message__c);
            System.assertEquals('Handled', [select Id, LastName from Account where Id = :testAccount.Id].LastName);
        }
        
    }
    
    @IsTest
    public static void whenFailedJobLogThenRetrySuccessful() {
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User 2',alias='tuser2',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser2@test.com',UserName='testuser2@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            Account testAccount = new Account(LastName = 'Good');
            database.insert(testAccount);
            Id testJobId = '707S000000nKE4fIAG';
            
            BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
            testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testJob.JobCreatedDate__c = System.today();
            testJob.JobId__c = testJobId;
            database.insert(testJob);
            
            BatchApexError__c testError = new BatchApexError__c();
            testError.AsyncApexJobId__c = testJobId;
            testError.BatchApexErrorLog__c = testJob.Id;
            testError.DoesExceedJobScopeMaxLength__c = false;
            testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
            testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testError.JobScope__c = testAccount.Id;
            testError.Message__c = 'Test exception';
            testError.RequestId__c = null;
            testError.StackTrace__c = '';
            database.insert(testError);
            
            Test.startTest();
            DescribeUtility.createApexBatchLog(testJobId, BatchableErrorTestJob.TestJobException.class.getName(), testJobId, 
                                               false,'', 0);
            BatchableRetryJob batchRetry = new BatchableRetryJob();
            BatchableRetryJob.run(testJobId);
            try { Test.stopTest(); }
            catch(Exception ex) { 
                // don't fail this test if there were errors in the batch job - we want that
            }
            Test.getEventBus().deliver();
            
            // Then (retry successful)
            System.assertEquals(0, [select id from BatchApexErrorLog__c].size());
        }
    }
    
    @isTest
    public static void whenFailedJobLogThenRetryFailed() {
        
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User 3',alias='tuser3',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser2@test.com',UserName='testuser3@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            Id testJobId = '707S000000nKE4fIAG';
            Account testAccount = new Account(LastName = 'Bad');
            database.insert(testAccount);
            
            BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
            testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testJob.JobCreatedDate__c = System.today();
            testJob.JobId__c = testJobId;
            database.insert(testJob);
            
            BatchApexError__c testError = new BatchApexError__c();
            testError.AsyncApexJobId__c = testJobId;
            testError.BatchApexErrorLog__c = testJob.Id;
            testError.DoesExceedJobScopeMaxLength__c = false;
            testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
            testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
            testError.JobScope__c = testAccount.Id;
            testError.Message__c = 'Test exception';
            testError.RequestId__c = null;
            testError.StackTrace__c = '';
            database.insert(testError);
            
            BatchableError batchError = BatchableError.newInstance(testError);
            
            // When
            Test.startTest();
            BatchableRetryJob batchRetry = new BatchableRetryJob();
            BatchableRetryJob.run(testJobId);
            batchRetry.handleErrors(batchError);
            try { Test.stopTest(); }
            catch(Exception ex) { 
                // don't fail this test if there were errors in the batch job - we want that
            }
            Test.getEventBus().deliver();
            
            // Then (retry failed)
            System.assertEquals(1, [select id from BatchApexErrorLog__c].size());
            System.assertEquals('Test exception', [select Message__c from BatchApexError__c][0].Message__c);
            System.assertEquals('Handled', [select Id, Name, LastName from Account where Id = :testAccount.Id].LastName);
        }
    }
    
}