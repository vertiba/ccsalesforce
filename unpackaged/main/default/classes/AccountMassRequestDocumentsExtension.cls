/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class AccountMassRequestDocumentsExtension {

    private ApexPages.StandardSetController setController;
    private RestApi.ListViewResult listView;
    private List<Account> sampleRecords;
    private List<Account> processedAccounts;
    private List<Account> problematicAccounts;
    public Boolean hasPermission; // Added for validating custom persmission for User
    public String recordStatus {get;set;}

    public AccountMassRequestDocumentsExtension(ApexPages.StandardSetController setController) {
        this.setController = setController;
        //In VF page limit to 50 for display.
        this.sampleRecords = Database.query(this.getQuery()+' LIMIT 50');
        this.processedAccounts = new List<Account>();
        this.problematicAccounts = new List<Account>();
        this.hasPermission 	     = FeatureManagement.checkPermission(Label.MassGenerateDocumentRequest); 
    }

    public void processSampleAccounts() {
         if(!hasPermission){ recordStatus = CCSFConstants.BUTTONMSG.NO_PERMISSION; return;}     
        Map<String, TH1__Document_Setting__c> documentSettingsByFormName = SmartCOMMUtility.getDocumentSettingsBySObjectName('Account');
        for(Account account : this.sampleRecords) {
            if(account.RecordType.DeveloperName != 'BusinessAccount') {
                problematicAccounts.add(account);
            } else if(account.HasMailingAddress__c == false) {
                problematicAccounts.add(account);
            } else if(account.RequiredToFile__c == null || account.RequiredToFile__c == 'No') {
                problematicAccounts.add(account);
            } else if(account.BusinessStatus__c == 'Inactive') {
                problematicAccounts.add(account);
            } else {
                if(Account.NoticeType__c == 'Direct Bill' && documentSettingsByFormName.containsKey(Label.AccountDirectBillingNotice)) {
                    processedAccounts.add(account);
                } else if(Account.NoticeType__c == 'Low Value' && documentSettingsByFormName.containsKey(Label.AccountLowValueExemptionNotice)) {
                    processedAccounts.add(account);
                } else if(Account.NoticeType__c == 'Notice to File' && documentSettingsByFormName.containsKey(Label.AccountNoticeOfRequirementToFile571L)) {
                    processedAccounts.add(account);
                } else if(Account.NoticeType__c == 'SDR' && documentSettingsByFormName.containsKey(Label.AccountSDRNotice)) {
                    processedAccounts.add(account);
                } else {
                    problematicAccounts.add(account);
                }
            }
        }
          
    }

    public RestApi.ListViewResult getListView() {
        if(this.listView != null && this.listView.id == this.setController.getFilterId()) return listView;

        String sobjectName = Schema.Account.SObjectType.getDescribe().getName();
        RestApi.ListViewResult listView = RestApi.getListView(sobjectName, this.setController.getFilterId());
        this.listView = listView;
        return listView;
    }

    public List<Account> getProcessedAccounts() {
        return this.processedAccounts;
    }

    public List<Account> getProblematicAccounts() {
        return this.problematicAccounts;
    }

    public String getQuery(){
        String queryPrefix = 'SELECT Id, Name, BusinessStatus__c, HasMailingAddress__c, MailingAddress__c, RecordType.DeveloperName, RequiredToFile__c, NoticeType__c FROM Account ';
        String listViewQuery;
        if(!Test.isRunningTest()){
            listViewQuery = this.getListView().query;
        }
        if(listViewQuery == null){
            listViewQuery = queryPrefix;
        } else {
            //getting where clause from list view otherwise all records are displayed and it is wrong.
            listViewQuery = queryPrefix+listViewQuery.substringAfter('FROM Account');
        }
        return listViewQuery;
    }
    
    public PageReference generateNotices() {
        Database.executeBatch(new AccountDocumentRequestGenerator(this.getQuery()));
        String returnUrl = ApexPages.currentPage().getParameters().get('retURL');
        if(String.isEmpty(returnUrl)) {
            returnUrl = '/' + Schema.Account.SObjectType.getDescribe().getKeyPrefix();
        }
        return new PageReference(returnUrl);
    }
}