/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: DepreciationFactorSelectorControllerTest 
 * Description : Test Class for DepreciationFactorSelectorController, IntegrationMonitoringBatchStatus, IntegrationMonitoringJobName
 * 				 and LightningLogEntry
*/
@isTest
public class DepreciationFactorSelectorControllerTest {

    /*
     * Method Name:depreciationFactorSelectorTest
     * Class: DepreciationFactorSelectorController
     * Description: This testmethod will cover the test scenarios for the DepreciationFactorSelectorController class
     */
    public static testmethod void depreciationFactorSelectorTest(){
        Id profileId = [Select Id from Profile where Name='System Administrator'].Id;
        User userInstance = new User(FirstName = 'Test',LastName='Admin User56',alias='tuse5r6',ProfileId = profileId,
                                     TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey='UTF-8',email='testuser@test.com',UserName='testus56r@test.com.ccsf');
        
        database.insert(userInstance);
        System.runAs(userInstance){
            List<Penalty__c> penalties = new List<Penalty__c>();
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            penalties.add(penalty1);
            
            Penalty__c penalty2 = new Penalty__c();
            penalty2.PenaltyMaxAmount__c = 500;
            penalty2.Percent__c = 25;
            penalty2.RTCode__c = '214.13';
            penalties.add(penalty2);
            
            Penalty__c penalty3 = new Penalty__c();
            penalty3.PenaltyMaxAmount__c = 500;
            penalty3.Percent__c = 25;
            penalty3.RTCode__c = '504';
            penalties.add(penalty3);
            insert penalties;
            
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c='Roll Open';
            insert rollYear;
            
            RollYear__c prevrollYear = new RollYear__c();
            prevrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            prevrollYear.Status__c='Roll Closed';
            insert prevrollYear;
            
            RollYear__c nextrollYear = new RollYear__c();
            nextrollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()+1);
            nextrollYear.Status__c='Pending';
            insert nextrollYear;
            
            Account accountRecord = new Account();
            accountRecord.Name='New Test Account';
            accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            accountRecord.EntityId__c = '654321';
            accountRecord.BusinessStatus__c='active';
            insert accountRecord;
            
            Property__c propertyRecord = new Property__c();
            propertyRecord.Name = 'new test property';
            propertyRecord.PropertyId__c = '153536';
            propertyRecord.MailingCountry__c = 'US';
            propertyRecord.Account__c = accountRecord.Id;
            insert propertyRecord;
            
            Case caseRecord = new Case();
            caseRecord.AccountName__c='new test account';
            caseRecord.type = 'Regular';
            caseRecord.AccountId = propertyRecord.account__c;
            caseRecord.ApplyFraudPenalty__c = true;
            caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            caseRecord.Property__c = propertyRecord.Id;
            caseRecord.Status = 'In Progress';
            caseRecord.AssessmentYear__c ='2017';
            caseRecord.EventDate__c = Date.valueOf('2019-11-26');
            caseRecord.MailingCountry__c = 'US';
            database.insert(caseRecord);
            
            AssessmentLineItem__c assessmentLineItem = new AssessmentLineItem__c();
            assessmentLineItem.Case__c = caseRecord.Id;
            assessmentLineItem.AcquisitionYear__c = '2016';
            assessmentLineItem.Subcategory__c = 'ATMs';
            assessmentLineItem.AssetSubclassification__c = 'General';
            assessmentLineItem.Cost__c = 12000;
            database.insert(assessmentLineItem);
            
            FactorSource__c factorSource = new FactorSource__c();
            factorSource.Name = 'Sample';
            database.insert(factorSource);
            
            Factor__c factor = new Factor__c();
            factor.Name = 'F1';
            factor.FactorSource__c = factorSource.Id;
            factor.AssessmentYear__c = '2017';
            factor.AcquisitionYear__c = '2016';
            factor.FactorPercent__c = 2;
            factor.YearsOfUsefulLife__c = 3;
            database.insert(factor);
            
            BusinessAssetMapping__c businessMapping = new BusinessAssetMapping__c();
            businessMapping.AssetClassification__c = 'ATMs';
            businessMapping.AssetSubclassification__c = 'General';
            businessMapping.FactorSource__c = factorSource.Id;
            businessMapping.FactorType__c = 'Depreciation Factor';
            businessMapping.YearsOfUsefulLife__c = 3;
            businessMapping.FixturesAllocationPercent__c = 10;
            database.insert(businessMapping);
            
            Test.startTest();
            DepreciationFactorSelectorController.getMatchingFactors(assessmentLineItem.Id);
            DepreciationFactorSelectorController.setFactor(assessmentLineItem.Id, factor.Id);
            // Test Coverage for Factor Utility as well
            FactorUtility.getBusinessAssetMappingsByCompositeKeys(assessmentLineItem.FactorCompositeKey__c, assessmentLineItem.FallbackFactorCompositeKey__c);
            FactorUtility.getMatchingVesselFactorsByCompositeKeys(new Set<String>{'2017-2016-Sample'});
            FactorUtility.findDefaultFactor(assessmentLineItem, businessMapping, new List<Factor__c>{factor}, new List<Factor__c>{factor});
            FactorUtility.getMatchingFactorsByCompositeKeys('2017-2016-Sample','2017-2016-Sample');
            system.assertEquals(true, businessMapping.Id!=null);
            Test.stopTest();
            
        }
    }
    
    /*
     * Method Name:integrationMonitoringBatchStatusTest
     * Class: IntegrationMonitoringBatchStatus
     * Description: This testmethod will cover the test scenarios for the integration monitoring batch class
     */ 
    public static testmethod void integrationMonitoringBatchStatusTest(){
        Test.startTest();
        IntegrationMonitoringBatchStatus batch = new IntegrationMonitoringBatchStatus();
        batch.getDefaultValue();
        batch.getValues();
        Test.stopTest();
    }
    
    /*
     * Method Name:integrationMonitoringJobNameTest
     * Class: IntegrationMonitoringJobName
     * Description: This testmethod will cover the test scenarios for the IntegrationMonitoringJobName class
     */ 
    public static testmethod void integrationMonitoringJobNameTest(){
        Test.startTest();
        IntegrationMonitoringJobName batch = new IntegrationMonitoringJobName();
        batch.getDefaultValue();
        batch.getValues();
        Test.stopTest();
    }
    
    /*
     * Method Name:lightningLoggerTest
     * Class: LightningLogEnrty
     * Description: This testmethod will cover the test scenarios for the LightningLogEnrty class
     */ 
    public static testmethod void lightningLoggerTest(){
        Test.startTest();
        LightningLogEntry lightningLogEntry = new LightningLogEntry();
        lightningLogEntry.componentName    = 'myComponent';
        lightningLogEntry.loggingLevelName = 'DEBUG';
        lightningLogEntry.message          = 'my test message';
        lightningLogEntry.originLocation   = 'test';
        lightningLogEntry.timestamp = system.now();
        
        LightningLogEntry.Error errorWrapper = new LightningLogEntry.Error();
        errorWrapper.columnNumber = 1;
        errorWrapper.lineNumber = 1;
        errorWrapper.message = 'Error Message';
        errorWrapper.stack = 'Stack Message';
        
        lightningLogEntry.error = errorWrapper;
        lightningLogEntry.topics = new List<String>{'Sample'};
        Test.stopTest();
    }
}