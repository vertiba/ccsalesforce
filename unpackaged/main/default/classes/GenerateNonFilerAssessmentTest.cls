/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to test GenerateNonFilerAssessment & NonFilerAssessmentBulkGenerator class.
//-----------------------------
@isTest
public class GenerateNonFilerAssessmentTest {
    
    private static Integer currentYear      = FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear         = currentYear-1;
    private static Integer lastToLastYear   = lastYear-1;
    private static Integer nextYear         = currentYear+1;
    
    @testSetup
    static void createTestData() {
        //Create Users
        List<User> users = new List<User>();
        User principal = TestDataUtility.getPrincipalUser();
        users.add(principal);
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin);  
        User auditor = TestDataUtility.getBPPAuditorUser();
        users.add(auditor); 
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            
            //insert roll years
            List<RollYear__c> rollYears = new List<RollYear__c>();
            
            RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYearCurrentFiscalYear.IsLowValueBulkGeneratorProcessed__c = true;
            rollYearCurrentFiscalYear.IsDirectBillBulkGeneratorProcessed__c = true;            
            rollYears.add(rollYearCurrentFiscalYear);
            
            RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastYear),String.valueof(lastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastFiscalYear);
            
            RollYear__c rollYearLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastToLastYear),String.valueof(lastToLastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastToLastFiscalYear);
            
            RollYear__c rollYearNextFiscalYear = TestDataUtility.buildRollYear(String.valueof(nextYear),String.valueof(nextYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearNextFiscalYear);
            
            insert rollYears;
            
            //insert penalty
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            //insert account
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            //insert property
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;
           
            //insert statement            
            Map<String,String> inputParamsStatement = new Map<String,String>();
            Date filingDate = Date.newInstance(2019, 2, 25);  
            Date startofBussinessDate = Date.newInstance(2016, 3,4);
            inputParamsStatement.put('accountId',businessAccount.Id);
            inputParamsStatement.put('propertyId',bppProperty.Id);
            inputParamsStatement.put('formType','571-L');
            inputParamsStatement.put('businessScenario','Existing');
            inputParamsStatement.put('propertyScenario','Existing');
            inputParamsStatement.put('assessmentYear','2020'); 
            inputParamsStatement.put('recordTypeId',DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME));
            inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/2016'));
            
            Statement__c bppStatement = TestDataUtility.buildStatement(inputParamsStatement);
            insert bppStatement;
        }
    }
    
    @isTest
    static void testGenerateNonFilerAssessmentForBPPPrinicipal() {
        
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
        
        Test.startTest();        
                                  
            Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME)];
            
            //insert assessment
            Date lastYearEventDate=  Date.newInstance(lastYear, 3, 11);           
            Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),bppProperty.Id,
                                                                rollYears[2].Id,rollYears[2].Name,rollYears[2].Name,
                                                                CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,lastYearEventDate);
            
            insert assessment;
            
            //insert assessment line item
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,assessment.id);
            insert assessmentLineItem;                
            
        System.runAs(principalUser) 
        {
            GenerateNonFilerAssessment.Response response;
            response = GenerateNonFilerAssessment.checkPermission(rollYears[1].Id);
            //assert response from class is success false
            System.assertEquals(false, response.isSuccess);
            //asert valid permissions are there to run batch or not
            System.assertEquals(CCSFConstants.BUTTONMSG.NO_PERMISSION, response.message);
        }
        Test.stopTest();       
    }
    
    @isTest
    static void testForNoPreviousYearAssesmentExists() {
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];         
        
        Test.startTest();        
                                  
            Property__c  bppProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME)];
            
            System.runAs(principalUser) 
            {
                GenerateNonFilerAssessment.Response response;
                response = GenerateNonFilerAssessment.checkPermission(rollYears[1].Id);
                //assert response from class is success
                System.assertEquals(false, response.isSuccess);
                //asert valid permissions are there to run batch
                System.assertEquals(CCSFConstants.BUTTONMSG.NO_PERMISSION, response.message);
            }
        Test.stopTest();       
    }
    
    @isTest
    static void testForCloseRollYearValidations() {
        
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c rollYear = [select id,Name, Status__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1 ];         
        rollYear.Status__c = CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS;
        update rollYear;
        
        Test.startTest();
        GenerateNonFilerAssessment.Response response;
        System.runAs(principalUser) 
        {    
            response = GenerateNonFilerAssessment.checkPermission(rollYear.Id);
            //assert response from class is not success
            System.assertEquals(false, response.isSuccess);
            //assert response from class about roll year is closed
            System.assertEquals(CCSFConstants.BUTTONMSG.CLOSED_ROLL_YEAR_BUTTON_WARNING, response.message);
        }
        Test.stopTest();    
    }
    
    @isTest
    static void testForPreviousBatchesRanValidations() {
        
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c rollYear = [SELECT Id,IsDirectBillBulkGeneratorProcessed__c FROM RollYear__c WHERE Year__c =: String.valueOf(currentYear)  limit 1];           
        rollYear.IsDirectBillBulkGeneratorProcessed__c = false;
        update rollYear;
        
        Test.startTest();
        GenerateNonFilerAssessment.Response response;
        System.runAs(principalUser) 
        {    
            response = GenerateNonFilerAssessment.checkPermission(rollYear.Id);
            //assert response from class is not success
            System.assertEquals(false, response.isSuccess);
            //assert response from class about previous batched are not ran
            System.assertEquals(CCSFConstants.BUTTONMSG.NONFILER_ASSESMENT_GENERATOR_WARNING, response.message);
        }
        Test.stopTest();    
    }
    
    @isTest
    static void testForBatchHasAlreadyRanValidations() {
        
        User principalUser = [SELECT Id FROM User WHERE LastName = 'principalUser'];
        RollYear__c rollYear = [SELECT Id,IsNonFilerBulkGeneratorProcessed__c FROM RollYear__c WHERE Year__c =: String.valueOf(currentYear)  limit 1];                         
        rollYear.IsNonFilerBulkGeneratorProcessed__c = true;
        update rollYear;
        
        Test.startTest();
        GenerateNonFilerAssessment.Response response;
        System.runAs(principalUser) 
        {    
            response = GenerateNonFilerAssessment.checkPermission(rollYear.Id);
            //assert response from class is not success
            System.assertEquals(false, response.isSuccess);
            //assert response from class about batch already ran
            System.assertEquals(CCSFConstants.BUTTONMSG.NONFILER_ASSESMENT_ALREADY_GENERATED_MSG, response.message);
        }
        Test.stopTest();    
    }
    
    @isTest
    static void testForUserHasPermissionValidations() {
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        //RollYear__c rollYear = [SELECT Id,IsNonFilerBulkGeneratorProcessed__c FROM RollYear__c WHERE Year__c =: String.valueOf(currentYear)  limit 1];        
        RollYear__c[] rollYears = [select id,Name, BPPLateFilingDate__c FROM RollYear__c order by Name desc];        
        Test.startTest();
        GenerateNonFilerAssessment.Response response;
        System.runAs(systemAdminUser) 
        {    
            response = GenerateNonFilerAssessment.checkPermission(rollYears[1].Id);
            //assert response from class is success
            System.assertEquals(true, response.isSuccess);
            //assert response from class about user having permission
            System.assertEquals(System.Label.HasPermissionNonFiler, response.message);
            
            //run batch class
            GenerateNonFilerAssessment.runNonFilerAssessmentBulkGenerator();
            rollYears = [select id,Name, BPPLateFilingDate__c,IsNonFilerBulkGeneratorProcessed__c FROM RollYear__c where name=:String.valueOf(currentYear) limit 1] ;      
        }
        Test.stopTest();
        
    }
}