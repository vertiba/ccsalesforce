/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description :est class for ApplyExemptionsPropertyToAssesments
//-----------------------------
@isTest(SeeAllData=false)
public class ApplyExemptionsPropertyToAssesmentsTest {
    
    @testSetUp
    static void createTestData(){
        
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin); 
        User bppStaff = TestDataUtility.getBPPAuditorUser();
        users.add(bppStaff);
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            
            String currentFiscalyear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());            
            String previousFiscalyear = String.valueOf(Integer.valueOf(currentFiscalyear)-1);
            List<RollYear__c> rollyears = new List<RollYear__c>();
            RollYear__c currentRollyear = TestDataUtility.buildRollYear(currentFiscalyear,currentFiscalyear,'Roll Open');
            RollYear__c closedRollyear = TestDataUtility.buildRollYear(previousFiscalyear,previousFiscalyear,'Roll Closed');
            rollyears.add(currentRollyear);
            rollyears.add(closedRollyear);
            insert rollyears;
            
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;
            
            Case cas = new Case();
            cas.AccountId = bppProperty.account__c;
            cas.Property__C = bppProperty.id;
            cas.MailingCountry__c = 'US';
            cas.EventDate__c = System.today();
            cas.Type='Regular';
            cas.RecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
            
            insert cas; 
            
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,cas.id);
            insert assessmentLineItem; 
            
            //Exemption Property Event 
            PropertyEvent__c propertyEvent = new PropertyEvent__c();
            propertyEvent.property__c = bppProperty.id;
            propertyEvent.Type__c = 'Institutional';
            propertyEvent.SubType__c ='Welfare';
            propertyEvent.Status__c = 'New';
            insert propertyEvent;

            //insert Vessel property
            Property__c vesselProperty = TestDataUtility.getVesselProperty();
            vesselProperty.Account__c = businessAccount.Id;
            insert vesselProperty;
        }
    }


    @isTest
    public static void testPermissionCheck(){

        User auditiorUser = [SELECT Id FROM User WHERE LastName = 'auditiorUser'];
        PropertyEvent__c propertyEvent = [Select Id,property__c ,Type__c,SubType__c,Status__c from PropertyEvent__c];
        Case cs = [Select Id,integrationstatus__c ,Status,SubStatus__c,SequenceNumber__c from Case];
        cs.integrationstatus__c = 'Sent To TTX';
        update cs;

        System.runAs(auditiorUser){
            Test.StartTest();
                Boolean hasPermission = ApplyExemptionsPropertyToAssesments.checkPermission();
                system.assertEquals(false , hasPermission);
            Test.StopTest();
        }

    }

    @isTest
    public static void testFetchAssessments(){

        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PropertyEvent__c propertyEvent = [Select Id,property__c ,Type__c,SubType__c,Status__c from PropertyEvent__c];
        Case cs = [Select Id,integrationstatus__c ,Status,SubStatus__c,SequenceNumber__c from Case];
        cs.integrationstatus__c = 'Sent To TTX';
        update cs;

        System.runAs(sysAdminUser){
            Test.startTest();
                List<Case> assessments = ApplyExemptionsPropertyToAssesments.fetchAssessments(propertyEvent.Id);
                integer listSize = assessments.size();
                system.assertEquals(1, listSize);
            Test.stopTest();
        }

    }

    @isTest
    public static void testSaveExemptionData(){

        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PropertyEvent__c propertyEvent = [Select Id,property__c ,Type__c,SubType__c,Status__c from PropertyEvent__c];
        Case cs = [SELECT id,Roll__c,AssessmentYear__c,TotalAssessedValue__c,isLocked__c,
                    IntegrationStatus__c,PercentExempt__c,NetAssessedValue__c,CalculatedAmountExempt__c,
                    ExemptAmountManual__c,type,status,Sequencenumber__c,ExemptionPenalty__c,ExemptionPenalty__r.Name 
                    FROM Case];
        cs.integrationstatus__c = 'Sent To TTX';
        update cs;

        System.runAs(sysAdminUser){
        
            Test.startTest(); 
                List<Case> assessments = ApplyExemptionsPropertyToAssesments.fetchAssessments(propertyEvent.Id);
                assessments[0].PercentExempt__c = 20.00;
                assessments[0].ExemptAmountManual__c = null;
                String jsonData = JSON.serialize(assessments);
                ApplyExemptionsPropertyToAssesments.saveExemptionData(jsonData,propertyEvent.Id);
                List<Case> caseOnBPPProperty = [SELECT id FROM Case Where property__c =: propertyEvent.property__c]; 
                integer listSize = caseOnBPPProperty.size();
                system.assertEquals(2, listSize);
            Test.stopTest();
        }

    }
}