/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/*********************** 
 * @Author: Publicis Sapient
 * @Apex TestClassName: RollCorrectionBatchSyncMockCallout
 * 
 **********************/
@isTest
global class RollCorrectionBatchSyncMockCallout implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest request) {
        
        //String passUrl = [Select Endpoint from NamedCredential where developerName ='SMART_Job_Taxsys_Pass_Assessment_Endpoint' limit 1].Endpoint;
        
        HttpResponse response = new HttpResponse();
        // Send a mock response for the endpoint
        System.assertEquals('POST', request.getMethod());
        response.setHeader('Content-Type', 'application/json; charset=utf-8');
        response.setStatusCode(202);
        return response;
    } 

}