/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This is a test class for the COntentVersionHandler Class.
@isTest
public with sharing class ContentVersionHandlerTest {

    @isTest(SeeAllData=true)
    private static void addFilesNotifications() {
		Account account = TestDataUtility.getBusinessAccount();
        insert account;
		
        Id propertyRecordTypeId = Schema.SObjectType.Property__C.getRecordTypeInfosByDeveloperName().get('Vessel').getRecordTypeId();
		Property__c property = TestDataUtility.buildProperty('new test property', propertyRecordTypeId);
		property.Account__c = account.Id;
        insert property;		
        
        Id vesselAssessmentRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VesselAssessment').getRecordTypeId();
        
        Case cas = TestDataUtility.buildAssessment(vesselAssessmentRecordTypeId,property.id,null,null,null,'Regular',null,null,null,system.today());
        
        insert cas;
        
        Test.startTest();
        ContentVersion cv = new Contentversion(); 
        cv.Title = 'CZDSTOU'; 
        cv.PathOnClient = 'test'; 
        cv.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
        cv.FirstPublishLocationId  =  cas.Id;  
        List<ContentVersion> cvl = new List<ContentVersion>(); 
        cvl.add(cv); 
        insert cvl;
        
        ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
        List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
        testItemList.add(new ConnectApi.FeedItem());
        testPage.elements = testItemList;

        // Set the test data
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null,ConnectApi.FeedType.Record, cas.id, testPage);
        ConnectApi.FeedElementPage elements = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(null,ConnectApi.FeedType.Record, cas.Id);
       
        System.assertEquals(1,elements.elements.size());
        Test.stopTest();
		
    }
        

}