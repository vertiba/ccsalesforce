/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @description: Test class for RecallApprovalProcess APEX Controller which has been used on Flow Create Assessment from Statement and Create Vessel Assessment From Statements
 *
 * @author: Publicis.Sapient
*/

@isTest()
public class RecallApprovalProcessTest {

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to create test data
    //-----------------------------
    @testSetup
    static void testDataSetup(){
        
        //user creation
        Id systemAdminProfileId = [Select Id from Profile where Name='System Administrator'].Id;
        User managerUser = TestDataUtility.buildUser('Manager','manager@test.com','manag',systemAdminProfileId,null);
        insert managerUser;
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        systemAdmin.ManagerId = managerUser.Id;
        users.add(systemAdmin); 
        User exemptionStaff = TestDataUtility.getExemptionStaffUser();
        exemptionStaff.ManagerId = managerUser.Id;
        users.add(exemptionStaff);
        insert users;
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        
        System.runAs(systemAdminUser){
            //account creation
            Account account = new Account();
            account.Name='test account';
            account.BusinessStatus__c='active';
            account.MailingCountry__C='US';
            insert account;
            
            //penalty creation
            Penalty__c penalty1 = new Penalty__c();
            penalty1.PenaltyMaxAmount__c = 500;
            penalty1.Percent__c = 10;
            penalty1.RTCode__c = '463';
            insert penalty1;
            
            Property__C property = new Property__C();
            property.Name = 'test property'; 
            property.MailingCountry__c = 'US';
            property.Account__c = account.Id;
            insert property;
            
            Date filingDate = System.today();
            
            //rollyear creation
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear.Status__c = 'Roll Open';
            insert rollYear;
            
            RollYear__c closedRollYear = new RollYear__c();
            closedRollYear.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            closedRollYear.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            closedRollYear.Status__c = 'Roll Closed';
            insert closedRollYear;
            
            //Case creation
            Case cas = new Case();
            cas.AccountId = property.account__c;
            cas.Property__C = property.id;
            cas.MailingCountry__c = 'US';
            cas.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            cas.Type='Regular';
            cas.AdjustmentType__c = 'New';
            cas.Roll__c = rollYear.Id;
            insert cas;
        }
        
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to test RecallApproval Method
    //-----------------------------
    @isTest()
    private static void testRejectApprovalProcess(){
        List<Id> recordIds = new List<Id>();
        List<Case> caseId = new List<Case>([Select Id from Case]);
        recordIds.add(caseId[0].id);
        
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setComments( 'Submitting request for approval' );
        request.setObjectId(caseId[0].Id);
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        
        
        System.runAs(systemAdminUser){
        Approval.ProcessResult result = Approval.process(request);
        }
        
        Test.startTest();
        RecallApprovalProcess.RecallApproval(recordIds);
        Test.stopTest();   
        
        Case casSubStatus = [Select SubStatus__c from Case where id IN:recordIds];
        System.assertEquals(null,casSubStatus.SubStatus__c);
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to test Exception in RecallApproval Method
    //-----------------------------
    @isTest()
    private static void testExceptionInRecallApproval(){
        List<Id> recordIds = new List<Id>();
        List<Case> caseId = new List<Case>([Select Id from Case]);
        recordIds.add(caseId[0].id);
        
        User exemptionStaffUser = [SELECT Id FROM User WHERE LastName = 'exemptionStaffUser']; 
        
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setComments( 'Submitting request for approval' );
        request.setObjectId(caseId[0].Id);
        
        System.runAs(exemptionStaffUser){
            Approval.ProcessResult result = Approval.process(request);
            Test.startTest();
            try{
                RecallApprovalProcess.RecallApproval(recordIds);
                System.assert(false,'Exception should be thrown');
            }catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            Test.stopTest(); 
        }
    }
    
    
}