/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//                                                                                                                       
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######  
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##    
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##    
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##    
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##    
//                                                                                                                       
//=======================================================================================================================


/*************************************************************
 *              CONTENTVERSIONREPORTCONTROLLER               *
 * CONTROLLER CLASS FOR LIGHTNING CMP CONTENT VERSION REPORT *
 *************************************************************/




public with sharing class ContentVersionReportController {
    
    @AuraEnabled
    public static List<FieldSetUtility.FieldSetMember> getFieldSetFields(String objectName, String fieldSetName){        
        return FieldSetUtility.getFieldSetFields(objectName,fieldSetName);
    }

    @AuraEnabled
    public static List<ContentVersion> getAllContentVersionRecords(String objectName, String fieldSetName){

        List<ContentVersion> contentVersions = new List<ContentVersion>();
        if(String.isBlank(fieldSetName)) return contentVersions;

        String query = 'SELECT Id' ;
        String queryFields = '';

        for(FieldSetUtility.FieldSetMember fieldSetMember : FieldSetUtility.getFieldSetFields(objectName,fieldSetName)){
            queryFields = queryFields +', '+ fieldSetMember.fieldPath ;            
        }
        query = query + queryFields + ' FROM ContentVersion';
        system.debug(query);
        contentVersions = (List<ContentVersion>) Database.query(query);
        return contentVersions; 
    }

}