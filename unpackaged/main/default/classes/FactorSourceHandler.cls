/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class FactorSourceHandler extends TriggerHandler {

    protected override void executeBeforeInsert(List<SObject> newRecords) {
        List<FactorSource__c> newFactorSources = (List<FactorSource__c>)newRecords;

        this.setUuid(newFactorSources);
    }

    protected override void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {
        List<FactorSource__c> updatedFactorSources = (List<FactorSource__c>)updatedRecords;

        this.setUuid(updatedFactorSources);
    }

    private void setUuid(List<FactorSource__c> factorSources) {
        for(FactorSource__c factorSource : factorSources) {
            if(Uuid.isValid(factorSource.Uuid__c)) continue;

            factorSource.Uuid__c = new Uuid().getValue();
        }
    }

}