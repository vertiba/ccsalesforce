/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
* @Business: Test class for PropertyDocumentRequestGenerator
* @Date: 04/01/2020
*/

@isTest
public class PropertyDocumentRequestGeneratorTest {
    @testSetup
    static void createTestData() {

        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account account = TestDataUtility.buildAccount('Sample Property Account #101', null, businessAccountRecordTypeId);
        account.MailingStreetName__c = 'Test Street';
        account.MailingCity__c = 'Test City';
        INSERT account;
        

        Ownership__c ownership = new Ownership__c();
        ownership.StartDate__c = system.today();
        ownership.Account__c = account.Id;
        INSERT ownership;
        
        // Create Properties
        List<Property__c> properties = new List<Property__c>();
        
        Property__c propertyNoticeToFile = new Property__c();
        propertyNoticeToFile = buildProperty(account.Id, 'Test Property - #1','Notice to File','Yes');
        propertyNoticeToFile.PrimaryOwnership__c = ownership.Id;
        properties.add(propertyNoticeToFile);
        
        Property__c propertyLowValue = new Property__c();
        propertyLowValue = buildProperty(account.Id, 'Test Property - #2','Low Value','Optional');
        propertyLowValue.PrimaryOwnership__c = ownership.Id;     
        properties.add(propertyLowValue);
        
        Property__c propertyDirectBill = new Property__c();
        propertyDirectBill = buildProperty(account.Id, 'Test Property - #3','Direct Bill','Optional');
        propertyDirectBill.PrimaryOwnership__c = ownership.Id;     
        properties.add(propertyDirectBill);        
        
        INSERT properties;
        
        List<TH1__Schema_Set__c> schemaSets = new List<TH1__Schema_Set__c>();
        TH1__Schema_Set__c schemaSet1 = new TH1__Schema_Set__c();
        schemaSets.add(schemaSet1);
        INSERT schemaSets;
        
        List<TH1__Schema_Object__c> schemaObjects = new List<TH1__Schema_Object__c>();
        TH1__Schema_Object__c schemaObject1 = new TH1__Schema_Object__c();
        schemaObject1.TH1__Is_Primary_Object__c = true;
        schemaObject1.TH1__Object_Label__c = 'Property__c';
        schemaObject1.TH1__Schema_Set__c = schemaSets.get(0).Id;
        schemaObject1.Name = 'Property__c';
        schemaObjects.add(schemaObject1);
        INSERT schemaObjects;
        
        schemaSets.get(0).TH1__Primary_Object__c = schemaObjects.get(0).Id;
        UPDATE schemaSets;

        // Create Document Settings for Properties
        List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
    
        TH1__Document_Setting__c documentSettingNoticeToFile = new TH1__Document_Setting__c();
        documentSettingNoticeToFile = buildDocumentSetting(schemaSets.get(0).Id,Label.PropertyNoticeOfRequirementToFile576D);        
        documentSettings.add(documentSettingNoticeToFile);  
    
        TH1__Document_Setting__c documentSettingNoticeToFileEmail = new TH1__Document_Setting__c();
        documentSettingNoticeToFileEmail = buildDocumentSetting(schemaSets.get(0).Id,Label.PropertyNoticeOfRequirementToFile576DEmail);        
        documentSettings.add(documentSettingNoticeToFileEmail); 
        
        TH1__Document_Setting__c documentSettingLowValue = new TH1__Document_Setting__c();
        documentSettingLowValue = buildDocumentSetting(schemaSets.get(0).Id,Label.PropertyLowValueNotice576D);        
        documentSettings.add(documentSettingLowValue);  
        
        TH1__Document_Setting__c documentSettingLowValueEmail = new TH1__Document_Setting__c();
        documentSettingLowValueEmail = buildDocumentSetting(schemaSets.get(0).Id,Label.PropertyLowValueNotice576DEmail);        
        documentSettings.add(documentSettingLowValueEmail);
		
        TH1__Document_Setting__c documentSettingDirectBill = new TH1__Document_Setting__c();
        documentSettingDirectBill = buildDocumentSetting(schemaSets.get(0).Id,Label.PropertyDirectBillNotice576D);        
        documentSettings.add(documentSettingDirectBill);  
        
        TH1__Document_Setting__c documentSettingDirectBillEmail = new TH1__Document_Setting__c();
        documentSettingDirectBillEmail = buildDocumentSetting(schemaSets.get(0).Id,Label.PropertyDirectBillNotice576DEmail);        
        documentSettings.add(documentSettingDirectBillEmail); 
        
        
        INSERT documentSettings;
        
    }    
        
    //Build Property Object Method
    static Property__c buildProperty(Id accountId, String name, string noticeType, string requiredTtoFile) {
        return new Property__c(
            Account__c = accountId,
            Name = name,
            MailingStreetName__c = '7800 Blackwood',
            MailingCity__c = 'Portland',
            MailingCountry__c = 'US',
            Status__c = 'Active',
            PrimaryFormType__c = '576-D', 
            ContactEmail__c = 'test@test.com',
            NoticeType__c = noticeType,
            RequiredToFile__c = requiredTtoFile,        
            RecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Vessel').getRecordTypeId()
        );
    }
    
    //Build Document Settings Object Method
    static TH1__Document_Setting__c buildDocumentSetting(Id schemaSetId, String name) {
        return new TH1__Document_Setting__c(
            TH1__Document_Data_Model__c = schemaSetId,
            Name = name,     
            TH1__Storage_File_Name__c = name,
            TH1__Is_disabled__c = false,
            TH1__Generate_document__c = true,
            TH1__Filter_field_name__c = 'PrimaryFormTypeText__c',
            TH1__Filter_field_value__c = '576-D'
		
        );
    }
        
    @isTest
    static void testBatchJob() {
        String  batchQuery = 'SELECT Id, HasMailingAddress__c, MailingAddress__c, PrimaryFormType__c, Status__c,';
                batchQuery+= 'ContactEmail__c,NoticeType__c FROM Property__c WHERE RecordType.DeveloperName = \'Vessel\'';
                batchQuery+= 'And HasMailingAddress__c = true And RequiredToFile__c != null And RequiredToFile__c != \'No\'';
                batchQuery+= 'And Status__c != \'Inactive\'';
    
        Test.startTest();
        
        PropertyDocumentRequestGenerator batch = new PropertyDocumentRequestGenerator(batchQuery);                                              
        Database.executeBatch(batch);
        
        Test.stopTest();
        
        Set<String> properties = new Set<String>();       
        for(Property__c property : Database.query(batchQuery))
        {
            properties.add(property.id);
        }
        
        List<DocumentGenerationRequest__c> documentGenerationRequests = [SELECT Id, Status__c, RelatedRecordType__c, RelatedRecordName__c,DocumentSetting__r.Name, 
                                                                         Name FROM DocumentGenerationRequest__c WHERE RelatedRecordId__c in : properties];        
        
        // Assert the functionality - We should expect DocumentGenerationRequest__c size same as created Property size
        System.assertEquals(properties.size()*2, documentGenerationRequests.size());
    }
}