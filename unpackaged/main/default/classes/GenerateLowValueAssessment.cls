/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class GenerateLowValueAssessment {

    @AuraEnabled
    public static Response checkPermission(Id recordId) {
        Response response = new Response();
        Boolean hasPermission = FeatureManagement.checkPermission(System.Label.MassGenerateDirectBillAssessments);        
        RollYear__c rollYear = [Select Status__c,IsLowValueBulkGeneratorProcessed__c from RollYear__c where id =: recordId];

        //Check if roll year is not closed, if closed return error message
        if (rollYear.Status__c == 'Roll Closed') {
            response.isSuccess = false;
            response.message = System.Label.RollYearClosed;
            return response;
        }else if(rollYear.IsLowValueBulkGeneratorProcessed__c) {
        //check if low value batch has not already ran, if ran return error message
			response.isSuccess = false;
			response.message = CCSFConstants.BUTTONMSG.LOW_VALUE_ASSESMENT_ALREADY_GENERATED_RMSG;
        }else if(!hasPermission) {
        //check if user has Custom Permission to run this batch, if not return error message
            response.isSuccess = false;
            response.message = System.Label.NoPermission;
            return response;
        }else {
		//if all the above condtions are false, means user can run this batch
            response.isSuccess = true;
            response.message = System.Label.HasPermission;
        }
           
        return Response;
    }

    @AuraEnabled
    public static Response runLowValueAssessmentBulkGenerator() {

        Response response = new Response();
        Integer currentYear = FiscalYearUtility.getCurrentFiscalYear();
        RollYear__c roll=[Select Id,IsLowValueBulkGeneratorProcessed__c from RollYear__c where year__c =:String.valueof(currentYear) and Status__c ='Roll Open'];
        //Checking if Batch ran already
        if(roll!= null && !roll.IsLowValueBulkGeneratorProcessed__c) {
            //calling Batch
            LowValueAssessmentBulkGenerator obj = new LowValueAssessmentBulkGenerator();
            Id jobId = Database.executeBatch(obj,Integer.valueOf(Label.LowValueAssessmentGeneratorBatchSize));
            AsyncApexJob apexJobs = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                     TotalJobItems, CreatedBy.Email, ExtendedStatus
                                     from AsyncApexJob where Id = :jobId]; 
            if(apexJobs.Status != 'Failed'){
                response.isSuccess = true;
                response.message= System.Label.BatchQueued + apexJobs.Status; 
            }else{
                response.isSuccess = false;
                response.message = System.Label.BatchErrors + apexJobs.NumberOfErrors;
            }
        }else{
            response.isSuccess = false;
            response.message = System.Label.AlreadyRanBatchMessage;
        }
        return response;
    }
    public class Response {
        @AuraEnabled
        public Boolean isSuccess;
        @AuraEnabled
        public String message;

        public Response(){
            isSuccess =false;
            message='';
        }
    }
}