/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test AccountMassRequestDocumentsExtension
//-----------------------------
@isTest
public class AccountMassRequestDocumentsExtensionTest {
    public class MockHttpResponseGenerator implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String endPoint = req.getEndpoint();
            
            // Create a fake response
            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-Type', 'application/json');
            resp.setBody('{"label":"LIST_VIEW_LABEL","id":"a025C000002NDKn","developerName":"LIST_VIEW","sobjectType":"Account","soqlCompatible":true,"query":"SELECT Id FROM Account"}');
            resp.setStatusCode(200);
            
            return resp;
        }
    }

    @testSetup
    private static void dataSetup(){
        
        Id profileId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
        Id auditorRoleId= [select Id,Name from UserRole where Name='BPP Auditor'].Id;        
        User user1 = TestDataUtility.buildUser('Testing', 'newuser@testccs.com', 'newUser', profileId, auditorRoleId);
        insert user1;
        
        //Create PermissionSet with Custom Permission and assign to test user
        PermissionSet ps = new PermissionSet();
        ps.Name = 'CustomPermission';
        ps.Label = 'CustomPermission';
        insert ps;
        SetupEntityAccess sea = new SetupEntityAccess();
        sea.ParentId = ps.Id;
        sea.SetupEntityId = [select Id from CustomPermission WHERE MasterLabel = 'Mass Generate Document Request'][0].Id;
        insert sea;
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = user1.Id;
        psa.PermissionSetId = ps.Id;
        insert psa;
        
        System.runAs(user1){
    
        TH1__Schema_Set__c schemaSet = new TH1__Schema_Set__c();
        insert schemaSet;

        TH1__Schema_Object__c schemaObject = new TH1__Schema_Object__c();
        schemaObject.TH1__Is_Primary_Object__c = true;
        schemaObject.TH1__Object_Label__c = 'Account';
        schemaObject.TH1__Schema_Set__c = schemaSet.Id;
        schemaObject.Name = 'Account';
        insert schemaObject;

        schemaSet.TH1__Primary_Object__c = schemaObject.Id;
        update schemaSet;

        List<String> documentSettingNames = new List<String>{Label.AccountDirectBillingNotice,
            Label.AccountLowValueExemptionNotice, Label.AccountNoticeOfRequirementToFile571L,
            Label.AccountSDRNotice};
        List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        TH1__Document_Setting__c documentSettting;
        for(integer i=0;i<4;i++) {
            documentSettting = new TH1__Document_Setting__c();
            documentSettting.TH1__Document_Data_Model__c = schemaSet.Id;
            documentSettting.Name = documentSettingNames.get(i);
            documentSettting.TH1__Storage_File_Name__c = documentSettingNames.get(i);
            documentSettting.TH1__Is_disabled__c=false;
            documentSettings.add(documentSettting);
        }
        insert documentSettings;
         // insert validation rule custom setting
            ValidationRuleSettings__c setting = new ValidationRuleSettings__c();            
            setting.DisableValidationRules__c = true;
            insert setting;
    }
    }

    @isTest
    private static void processSampleAccountsTest(){
        List<Account> accounts = new List<Account>();
        Account account;
        User user1 = [SELECT Id FROM User WHERE LastName = 'Testing'];
        
        system.runAs(user1){
            List<String> picklistValues = new List<String>{'Direct Bill','Notice to File','Low Value','SDR'};
                Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
            for(integer i=0;i<4;i++){
                account = new Account();
                account.Name='test account'+i;
                account.RecordTypeId=businessAccountRecordTypeId;
                account.BusinessStatus__c='active';
                account.MailingCity__c='San Franciso';
                account.MailingCountry__C='US';
                account.MailingStreetName__c = 'Bush Street';
                account.RequiredToFile__c = 'Yes';
                account.NoticeType__c = picklistValues.get(i);
                accounts.add(account);
            }
            insert accounts;
            // Set mock 
        	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(accounts);
            Test.startTest();
            AccountMassRequestDocumentsExtension accountMassRequestDocumentsExtension = new AccountMassRequestDocumentsExtension(controller);
            accountMassRequestDocumentsExtension.processSampleAccounts();
            RestApi.ListViewResult listView = accountMassRequestDocumentsExtension.getListView();
            System.assertEquals(4, accountMassRequestDocumentsExtension.getProcessedAccounts().size());
            System.assertEquals(0, accountMassRequestDocumentsExtension.getProblematicAccounts().size());
            Test.stopTest();
        }
    }

    @isTest
    private static void donotProcessSampleAccountsTest(){
        User user1 = [SELECT Id FROM User WHERE LastName = 'Testing'];
        system.runAs(user1){
            List<Account> accounts = new List<Account>();
            Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
            Account account1 = new Account();
            account1.Name='Account with wrong Notice Type';
            account1.RecordTypeId=businessAccountRecordTypeId;
            account1.BusinessStatus__c='active';
            account1.MailingCity__c='San Franciso';
            account1.MailingCountry__C='US';
            account1.MailingStreetName__c = 'Bush Street';
            account1.RequiredToFile__c = 'Yes';
            account1.NoticeType__c = '';
            accounts.add(account1);
            
            Account account2 = new Account();
            account2.Name='Account with inactive status';
            account2.RecordTypeId=businessAccountRecordTypeId;
            account2.BusinessStatus__c='Inactive';
            account2.MailingCity__c='San Franciso';
            account2.MailingCountry__C='US';
            account2.MailingStreetName__c = 'Bush Street';
            account2.RequiredToFile__c = 'Yes';
            account2.NoticeType__c = 'Low Value';
            accounts.add(account2);
            
            Account account3 = new Account();
            account3.Name='Account with Required to File';
            account3.RecordTypeId=businessAccountRecordTypeId;
            account3.BusinessStatus__c='Active';
            account3.MailingCity__c='San Franciso';
            account3.MailingCountry__C='US';
            account3.MailingStreetName__c = 'Bush Street';
            account3.RequiredToFile__c = 'No';
            account3.NotRequiredToFileReason__c = 'Government Entity';
            account3.NoticeType__c = 'Low Value';
            accounts.add(account3);
            
            Account account4 = new Account();
            account4.Name='Account with No Mailing Address';
            account4.RecordTypeId=businessAccountRecordTypeId;
            account4.BusinessStatus__c='Active';
            account4.MailingCity__c='San Franciso';
            account4.MailingCountry__C='US';
            account4.RequiredToFile__c = 'Yes';
            account4.NoticeType__c = 'Low Value';
            accounts.add(account4);   
            insert accounts;
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(accounts);
            Test.startTest();
            AccountMassRequestDocumentsExtension accountMassRequestDocumentsExtension = new AccountMassRequestDocumentsExtension(controller);
            accountMassRequestDocumentsExtension.processSampleAccounts();            
            System.assertEquals(0, accountMassRequestDocumentsExtension.getProcessedAccounts().size());
             System.assertEquals(4, accountMassRequestDocumentsExtension.getProblematicAccounts().size());
            Test.stopTest();
        }
    }

    @isTest
    private static void generateNoticesForAccounts(){
        List<Account> accounts = new List<Account>();
        Account account;
        List<String> picklistValues = new List<String>{'Direct Bill','Notice to File','Low Value','SDR'};
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        for(integer i=0;i<4;i++){
            account = new Account();
            account.Name='test account'+i;
            account.RecordTypeId=businessAccountRecordTypeId;
            account.BusinessStatus__c='active';
            account.MailingCity__c='San Franciso';
            account.MailingCountry__C='US';
            account.MailingStreetName__c = 'Bush Street';
            account.RequiredToFile__c = 'Yes';
            account.NoticeType__c = picklistValues.get(i);
            accounts.add(account);
        }
        insert accounts;
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(accounts);
        Test.startTest();        
        AccountMassRequestDocumentsExtension accountMassRequestDocumentsExtension = new AccountMassRequestDocumentsExtension(controller);
        accountMassRequestDocumentsExtension.generateNotices();        
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];        
        System.assertEquals(0, documentGenerationRequests.size());
        Test.stopTest();
        documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(4, documentGenerationRequests.size());
    }
}