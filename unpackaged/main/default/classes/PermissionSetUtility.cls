/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description :  This class is used for inserting the permission set assignments.
// This class will be used in process builder and flows.
//-----------------------------
public class PermissionSetUtility {

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<PermissionSetInput>
    // @description : InvocableMethod.
    // @return : void
    //-----------------------------
    @InvocableMethod(label = 'Assign Permission Set')
    public static void createPermissionSet(List<PermissionSetInput> permissionSetInputs) {
        List<Id> userIds = new List<Id>();
        List<String> permissionSetNames = new List<String>();
        for(PermissionSetInput input : permissionSetInputs) {
            userIds.add(input.userId);
            permissionSetNames.add(input.permissionSetName);
        }
        Map<Id,PermissionSet> permissionSetById = new Map<Id,PermissionSet>([select id from PermissionSet where name in :permissionSetNames]);
        for(Id id : permissionSetById.keySet()) {
            addPermissionSetAssignments(id, userIds);
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Id permissionSetId, List<Id> userIds
    // @description : future Method.
    // @return : void
    //-----------------------------
    @future
    private static void addPermissionSetAssignments(Id permissionSetId, List<Id> userIds) {
        List<PermissionSetAssignment> assignmentsToCreate = new List<PermissionSetAssignment>();
        for(Id userId : userIds) {
            PermissionSetAssignment permissionSetAssignment =new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = permissionSetId);
            assignmentsToCreate.add(permissionSetAssignment);
        }
        Database.insert(assignmentsToCreate, false);
    }

    //-----------------------------
	// @author : Publicis.Sapient
	// @description :  This class will be used to set PermissionSet input variables.
	//-----------------------------
    public class PermissionSetInput {
        @InvocableVariable(label = 'User ID' required = true)
        public Id userId;

        @InvocableVariable(label = 'Permission Set Name' required = true)
        public String permissionSetName;
    }

}