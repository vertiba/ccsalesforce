/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR 8978 : Controller Class Apply Exemption Property To Manage Exemption Lightning Web Component
//-----------------------------
public class ApplyExemptionsPropertyToAssesments {

   
    //----------
    //@author : Publicis Sapient 
    //@param : None
    //@description : This method is used to check Permission if the logged in user is allowed to Manage Exemptions.
    //@return : Boolean for Permission Check
    //@ASR-8978 
    //-------------
   
    @AuraEnabled
    public static Boolean checkPermission(){
        String customPermisionName ='ApplyExemptionsPropertyToAssesments';

        // Checking if the logged in user has the permission to Manage Exemptions
        Boolean hasPermission = FeatureManagement.checkPermission(customPermisionName); 
        return hasPermission;
    }


    //----------
    //@author : Publicis Sapient 
    //@param : RecordID 
    //@description : This method is used fetch assessemnts on the Manage Exemption Quick Action.
    //@return : List<Case>:  FethedAssessments to show on the Screen
    //@ASR-8978 
    //-------------

    @AuraEnabled 
    public static List<Case> fetchAssessments(String recordId){ 
       
        
        PropertyEvent__c  propertyEvent = [SELECT id, Property__c 
                                            FROM PropertyEvent__c 
                                            WHERE Id=: recordId];

        Id PropertyId = propertyEvent.Property__c;

        // Fetching eligible assessments.
        if(PropertyId != null) {
            Set<Id> assessmentsRecordtypeIds = new Set<Id>();
            assessmentsRecordtypeIds.add(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME));
            assessmentsRecordtypeIds.add(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME));
            assessmentsRecordtypeIds.add( DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME));
            assessmentsRecordtypeIds.add(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME));
            String filteredSubStatus = 'Cancelled';
            String filteredAdjustmentType = 'Cancel';
           
            String query = 'Select id,Roll__c,AssessmentYear__c,recordtypeId,TotalAssessedValue__c,isLocked__c,ExemptionReviewed__c,IntegrationStatus__c,PercentExempt__c,NetAssessedValue__c,CalculatedAmountExempt__c,ExemptAmountManual__c,type,status,Sequencenumber__c,ExemptionPenalty__c,ExemptionPenalty__r.Name,CaseNumber FROM Case';
            query += ' WHERE property__c =:PropertyId AND recordtypeId IN : assessmentsRecordtypeIds AND AdjustmentType__c !=: filteredAdjustmentType AND SubStatus__c !=:filteredSubStatus AND IsCorrectedOrCancelled__c = false'; 
            query += ' ORDER BY assessmentYear__c DESC, sequencenumber__c DESC NULLS FIRST';
            List<Case> cases = Database.query(query);
        
            return cases;
        }
        return null;
    } 
    
    //----------
    //@author : Publicis Sapient 
    //@param : String jsonData =  Data to be saved received in JSON form, String propertyExemptionId = Exemption Property Event Id
    //@description : This method is used to save the Exemption Data which came from Exemption Staff.
    //@return : void
    //@ASR-8978 
    //-------------

    @AuraEnabled
    public static String saveExemptionData(String jsonData, String propertyExemptionId){ 

        if(String.isBLank(jsonData) || String.isBLank(propertyExemptionId)) {
            return null;
        }
           
        List<Case> assessments = (List<Case>)System.JSON.deserialize(jsonData, List<Case>.class);

        if(assessments.isEmpty()) {
            return null;
        }

        List<Case> assessmentsTobeUpserted = new List<Case>();
        map<Id,Case> assessmentsTobeClonedWithIds = new map<Id,Case>();
        set<Id> propertyIdsForClonnedAssessments = new Set<Id>();

        // Collecting all the valid recordtype Ids for the ASR 8978
        Id bppRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
        Id vesselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME);
        Id legacyVesselrecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME);
        Id legacyBppRecordtypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_BPP_RECORD_TYPE_API_NAME);

        Map<Id,Id> recordTypeId = new Map<Id,Id>();
        recordTypeId.put(legacyBppRecordtypeId,bppRecordTypeId);
        recordTypeId.put(legacyVesselrecordTypeId,vesselRecordTypeId);

        Set<Id> assessmentsRecordtypeIds = new Set<Id>(); 
        assessmentsRecordtypeIds.add(legacyVesselrecordTypeId);
        assessmentsRecordtypeIds.add(legacyBppRecordtypeId);

        String currentFiscalYear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
        RollYear__c currentrollYear = [Select Id,Status__c,Year__c from RollYear__c where Year__c =: currentFiscalYear];
        Boolean hasPermissionToUpdateReviewedAssessments = FeatureManagement.checkPermission('ReviewAndApproveExemptions');
        // Logic to find assessments which has to be cloned and which can be updated. After that the following logic of cloning the assessments.
        for(Case assessment : assessments) {
            if(assessment.Roll__c == currentrollYear.id && assessment.isLocked__c) {
                assessment.Exemption__c = propertyExemptionId;
                assessmentsTobeUpserted.add(assessment);
            }else {
                if(assessment.IntegrationStatus__c == CCSFConstants.ASSESSMENT.SENT_TO_TTX || assessment.IntegrationStatus__c == CCSFConstants.ASSESSMENT.IN_TRANSIT_TO_TTX || assessment.IntegrationStatus__c == CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND
                || assessmentsRecordtypeIds.contains(assessment.RecordTypeId) ){

                    assessment.Exemption__c = propertyExemptionId;
                    assessmentsTobeClonedWithIds.put(assessment.Id,assessment);
                    propertyIdsForClonnedAssessments.add(assessment.property__c);
                }else {
                   // If Exmeption is already approved then only Mangers can update the exemption details. Else the record will by bypassed.
                    if( hasPermissionToUpdateReviewedAssessments || !assessment.ExemptionReviewed__c ) {
                        assessment.Exemption__c = propertyExemptionId;
                        assessmentsTobeUpserted.add(assessment);
                    } 
                }
            }
        }

       
        // Updating the existing assesments with exemption details.
        if(!assessmentsTobeUpserted.isEmpty()) {
            update assessmentsTobeUpserted;
        }

        // method to clone the assessments and create the new assessments with the exemption details.
        if(!assessmentsTobeClonedWithIds.isEmpty()) {
            processClonedData(assessmentsTobeClonedWithIds,recordTypeId);
        }

    return null;

    }

    //-----------
    //@author : Publicis Sapient 
    //@param : assessmentsTobeClonedWithIds,recordTypeId 
    //@description : This method is used to process the Data which needs to be cloned for the Exemption details updated by Exemption Staff/Manager.
    //@return : void
    //@ASR-8978 
    //-----------

    public static void processClonedData(Map<Id,Case> assessmentsTobeClonedWithIds, Map<Id,Id> recordTypeId){

        Set<Id> assessmentIds = assessmentsTobeClonedWithIds.keyset();
        //ASR-10611, Query cases
       String query = DescribeUtility.getDynamicQueryString('Case',null);

        List<String> splitedQueryStings = query.split('FROM Case');
        query = splitedQueryStings[0] + ',parent.NetAssessedValue__c';
       
        query += ' FROM Case';
        query += ' WHERE Id IN: assessmentIds'; 
        List<Case> tobeClonedCases = (List<Case>)Database.query(query);
        Map<Id,List<AssessmentLineItem__c>> caseByLineItems = new Map<Id,List<AssessmentLineItem__c>>(); 
        caseByLineItems = AssessmentsUtility.retrieveAssessmentAndLineItems(tobeClonedCases);
        Map<String,Object> clonedDataMapByListNames =  AssessmentsUtility.retrieveCloneAssessmentsForAdjustments(tobeClonedCases,caseByLineItems,recordTypeId);
        List<Case> clonedAssessments =  (List<Case>)clonedDataMapByListNames.get('assessments');
        Map<String,List<AssessmentLineItem__c>> clonedAssessmentLineItems = (Map<String,List<AssessmentLineItem__c>>)clonedDataMapByListNames.get('assessmentLIBySourceId');
        // Cloning the assessments and updating the exemption details.
        for(Case clonedAssessment : clonedAssessments) {
            clonedAssessment.PercentExempt__c = assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).PercentExempt__c;
            clonedAssessment.ExemptAmountManual__c = assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).ExemptAmountManual__c;
            clonedAssessment.ExemptionPenalty__c = assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).ExemptionPenalty__c;
            clonedAssessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_ROLLCORRECTION;
            clonedAssessment.AdjustmentReason__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_REASON_EXEMPTION;
            clonedAssessment.SubType__c = null;
            clonedAssessment.status = CCSFConstants.ASSESSMENT.STATUS_INPROGRESS;
            clonedAssessment.SubStatus__c = null;
            clonedAssessment.ExemptionReviewed__c = false;
            clonedAssessment.OwnerId = UserInfo.getUserId();
            clonedAssessment.ParentId = assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).Id;
            clonedAssessment.Exemption__c = assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).Exemption__c;

            // Adding Type logic  before the cloned assessment is created
            if(assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).Type !=  CCSFConstants.ASSESSMENT.TYPE_ESCAPE ) {
                List<Case> cases = new List<Case>{clonedAssessment};
                List<FormulaRecalcResult> results = Formula.recalculateFormulas(cases);
                if(cases[0].NetAssessedValue__c > 0 && cases[0].NetAssessedValue__c <= assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).NetAssessedValue__c){
                    clonedAssessment.type = CCSFConstants.ASSESSMENT.TYPE_REGULAR;
                }else if(cases[0].NetAssessedValue__c > assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).NetAssessedValue__c 
                        && !(assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).SubType__c == 'Low Value'
                            && assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).IntegrationStatus__c == CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND)){
                    clonedAssessment.type = CCSFConstants.ASSESSMENT.TYPE_ESCAPE;
                }

            }else {
                clonedAssessment.type = assessmentsTobeClonedWithIds.get(clonedAssessment.getCloneSourceId()).Type;
            }
        }

        // Inserting Line items on the cloned assessments.
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        Map<String,Object> insertedAssessmentsResult = DescribeUtility.getInsertRecordsResults(clonedAssessments,dmlOptions,'');
        if((Boolean) insertedAssessmentsResult.get('insertSuccess')) {
           
            Map<Id,Case> sourceIdWithClonedAssessment = new Map<Id,Case>(); 
            List<Case> insertedAssessments = (List<Case>) insertedAssessmentsResult.get('successRecords');
            for(Case insertedAssessment : insertedAssessments) {
                sourceIdWithClonedAssessment.put(insertedAssessment.getCloneSourceId(),insertedAssessment);
                
            }            
            List<AssessmentLineItem__c> lineItem = new List<AssessmentLineItem__c>();
            if(!clonedAssessmentLineItems.isEmpty()){
                for(String sourceAssessentId : clonedAssessmentLineItems.keyset()) {
                    for(AssessmentLineItem__c assessmentLineItem :clonedAssessmentLineItems.get(sourceAssessentId)){
                        assessmentLineItem.Case__c =sourceIdWithClonedAssessment.get(sourceAssessentId).Id;
                        lineItem.add(assessmentLineItem);
                    }
                }                
                Map<String,Object> insertedAssessmentLineItemsResult = DescribeUtility.getInsertRecordsResults(lineItem,dmlOptions,'');
            }
        } 
    }
    
}