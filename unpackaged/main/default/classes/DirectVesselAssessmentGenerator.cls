/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This Batch class is used to get all the Direct Bill Vessel Property related Assessments (Cases)
//          from the previous years and clone the Assesment.
//-----------------------------
public class DirectVesselAssessmentGenerator implements Database.Batchable<sObject>,Database.Stateful, BatchableErrorHandler {

    private String originLocation;
    private Integer currentYear;
    private final String CASE_QUERY;
    private boolean processSuccess;
    private Id assessmentLegacyVesselRecordTypeId;
    private Id assessmentVesselRecordTypeId;
    private Id propertyVesselRecordTypeId;
    
    public DirectVesselAssessmentGenerator() {
        originLocation = CCSFConstants.DIRECTBILL_VESSEL_GENERATOR_BATCH_NAME;
        //get all objects records type ids
        assessmentLegacyVesselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.LEGACY_VESSEL_RECORD_TYPE_API_NAME);
        assessmentVesselRecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME);
        propertyVesselRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME);
        
        //Get current year and last rollyear
        currentYear =   FiscalYearUtility.getCurrentFiscalYear();
        processSuccess = true;
        CASE_QUERY = DescribeUtility.buildQueryToFetchAllDetailsOfParentandChild(Case.sObjectType, AssessmentLineItem__c.sObjectType);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method gets all Property records and its child Assessment (Case) records.
    // @return : Database.QueryLocator (List of Properties)
    public Database.QueryLocator start(Database.BatchableContext bc) {
		
        String  batchQuery;
        //Buld dynamic query for batch
        batchQuery = 'SELECT Id, Name, ( SELECT Id,AdjustmentType__c, RecordType.DeveloperName, Type FROM Cases__r WHERE IsAssessment__c = true ';
		batchQuery += ' AND RollYear__c != \''+String.valueof(currentYear)+'\'';
        batchQuery += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';
        batchQuery += ' AND (RecordTypeId = \''+assessmentLegacyVesselRecordTypeId+'\'';
        batchQuery += ' OR RecordTypeId = \''+assessmentVesselRecordTypeId+'\')';
        batchQuery += ' AND AdjustmentType__c != \''+String.escapeSingleQuotes('Cancel')+'\' ';
        batchQuery += ' AND SubStatus__c  != \''+String.escapeSingleQuotes('Cancelled')+'\' ';
        batchQuery += ' AND IsCorrectedOrCancelled__c = false ';
        batchQuery += ' Order By EventDate__c desc, sequencenumber__c desc nulls last limit 1) ';
        batchQuery += ' FROM Property__c WHERE Status__c = \''+CCSFConstants.STATUS_ACTIVE+'\'';
        batchQuery += ' AND NoticeType__c = \''+CCSFConstants.NOTICE_TYPE_DIRECT_BILL+'\'';
        batchQuery += ' AND RecordTypeId = \''+propertyVesselRecordTypeId+'\'';
		batchQuery += ' AND Id NOT IN ( SELECT Property__c FROM Case WHERE IsAssessment__c = true ';
		batchQuery += ' AND RollYear__c = \''+String.valueof(currentYear)+'\'';
		batchQuery += ' AND Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';		
		batchQuery += ' AND (RecordTypeId = \''+assessmentLegacyVesselRecordTypeId+'\'';
		batchQuery += ' OR RecordTypeId = \''+assessmentVesselRecordTypeId+'\'))';
        Logger.addDebugEntry(batchQuery,originLocation+'- start method query');
        Logger.saveLog();
		return database.getQueryLocator(batchQuery);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : propertyWithCasesList (List of Properties)
    // @description : This method clones all the Assessment (Case) records of Property(s) which
    //          are returned by start() method of the Batch class 
    // @return : void
    public void execute(Database.BatchableContext bc, List<Property__c> matchingProperties) {
        
        try {
                List<Property__c> propertiesToInclude = new List<Property__c>();
                Set<Id> cases = new Set<Id>();
                List<Case> assessments = new List<Case>();
                if(matchingProperties.isEmpty()) {return;}
                
                for(Property__c property : matchingProperties) {
                    List<Case> propertyAssessments = property.Cases__r;
                    //Check if previous year regular case exists and current year regular case doesn't exists.
                    if(propertyAssessments.size() > 0) {
                        if(propertyAssessments[0].AdjustmentType__c == CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW){
                            cases.add(propertyAssessments[0].Id);
                        }
                    }
                }
            	
    			if(cases.isEmpty()) {return;}
                // Build query to get all fields of Case
                String query = CASE_QUERY + ' WHERE Id IN: cases';
                
                // Execute query and fetch all cases to be cloned
                Map<Id,Case> assessmentsToBeClonedById= new Map<Id,Case>((List<Case>)Database.query(query));              
    
                //insert assessments
                processSuccess = insertAssessments(assessmentsToBeClonedById,assessments);               
    
                Logger.addDebugEntry(String.valueof(processSuccess),'processSuccess'+originLocation);
           }
            catch(Exception ex) {
                processSuccess = false;
                Logger.addExceptionEntry(ex, originLocation);
                throw ex;
            }
        Logger.saveLog();
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method uddates the roll year record that the Direct Bill Vessel batch has been completed.
    // @return : void
    public void finish(Database.BatchableContext bc) {
        RollYear__c rollYear = getRollYearDetails(String.valueOf(currentYear));        
        //Update the roll year IsDirectBillVesselAssessmentProcess__c checkbox to true,
        //if the batch is successful and there are no failed records.
        
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, DirectVesselAssessmentGenerator.class.getName(), asyncList[0].ParentJobId, 
                                           false,'', asyncList[0].NumberOfErrors);

        
        if(processSuccess && rollYear !=null) {
            rollYear.IsDirectBillVesselAssessmentProcess__c = true;
            update rollYear;
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method inserts assessments and assessments line items
    // @return : return map of records of old assessments form which new assessments are cloned
    //-----------------------------
    private Boolean insertAssessments(Map<Id,Case> assessmentsToBeClonedById,List<Case> assessments) {
        RollYear__c rollYear = getRollYearDetails(String.valueOf(currentYear));
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        Map<Id,Id> recordtypeIdForLegacyRecord = new Map<Id,Id>{assessmentLegacyVesselRecordTypeId => assessmentVesselRecordTypeId};
        Map<String,Object> listOfRecordsByListName = AssessmentsUtility.getCloneAssessments(assessmentsToBeClonedById.values(),true,recordtypeIdForLegacyRecord);
        Boolean insertAssessmentsSuccess = true;
        if(!listOfRecordsByListName.isEmpty()) {
            List<Case> casesToProcess = new List<Case>();
            Map<ID,Case> caseById= (Map<ID,Case>)listOfRecordsByListName.get('caseBySourceId');
            if(!caseById.isEmpty()) {
                for(Case originalCase : assessmentsToBeClonedById.values()){
                    if(caseById.containsKey(originalCase.id)){
                        Case clonedCase = caseById.get(originalCase.id);
                        clonedCase.ExemptionPenalty__c = originalCase.ExemptionPenalty__c;
                        clonedCase.ExemptAmountManual__c= originalCase.ExemptAmountManual__c;
                        clonedCase.PercentExempt__c= originalCase.PercentExempt__c;
                        clonedCase.ExemptionCode__c= originalCase.ExemptionCode__c;
                        clonedCase.ExemptionDate__c= originalCase.ExemptionDate__c;
                        clonedCase.ExemptionType__c= originalCase.ExemptionType__c;
                        clonedCase.ExemptionSubtype__c= originalCase.ExemptionSubtype__c;
                        //Addedd as per bug ASR-7853 Clone assessment from Auto Proccessed should not be auto Processed 
                        clonedCase.AutoProcessed__c = false;
                        //ASR-9701. Enrolled Date should be blank after cloning an assessment
            			clonedCase.EnrolledDate__c = null;
                        // ASR-8548
                        clonedCase.CancelAttempted__c = false;
                        //ASR-8544 Changes Starts
                        clonedCase.AdjustmentType__c = CCSFConstants.ASSESSMENT.ADJUSTMENT_TYPE_NEW;
                        clonedCase.Status = CCSFConstants.ASSESSMENT.STATUS_NEW;                                        
                        clonedCase.SubStatus__c= null;
                        clonedCase.Roll__c = rollYear.Id;
                        clonedCase.IntegrationStatus__c = null;
                        //ASR-8544 Changes Ends

                        // As part of 9070 Fix
                        clonedCase.ClosedDate = null;
                        clonedCase.Status = CCSFConstants.ASSESSMENT.STATUS_CLOSED;
                        clonedCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                        clonedCase.IsLocked__c = true;
                        //ASR-10768 Changes
                        clonedCase.AdjustmentReason__c = null;
                        casesToProcess.add(clonedCase);
                    }
                }
            }
            assessments.addAll(casesToProcess);
        }

        //insert cloned assessments
        Map<String,Object> insertResultMap = DescribeUtility.getInsertRecordsResults(assessments,dmlOptions,originLocation);      
        
        //ASR-8544 Changes starts
        insertAssessmentsSuccess = (Boolean)insertResultMap.get('insertSuccess');
        List<SObject> clonedAssessments = (List<Sobject>) insertResultMap.get('successRecords');
        //Create a set of newly created assessments
        
        
        return insertAssessmentsSuccess;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns Roll_Year__c object record for requested year
    // @return : roll year record
    //-----------------------------
    private static RollYear__c getRollYearDetails(String year) {
        
        RollYear__c[] rollYear = [SELECT Id,Name,Status__c,IsDirectBillVesselAssessmentProcess__c
                                  FROM RollYear__c WHERE Year__c =: year  limit 1];
        //check query result and return first returned result's id
        if(rollYear.size()>0)
        {
            return rollYear[0];
        }
		return null;
    }

}