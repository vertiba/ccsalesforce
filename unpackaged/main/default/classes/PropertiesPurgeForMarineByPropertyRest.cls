/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * Class Name: PropertiesPurgeForMarineByPropertyRest
 * Description: This class purges Marine Properties
 * Author : Publicis Sapient 
 */
@RestResource(urlMapping='/PropertiesPurgeForMarineByLegacyProperty/*')
global with sharing class PropertiesPurgeForMarineByPropertyRest {
    
    @HttpPost
    global Static Map<String, String> postMethod(){
        
        String requestBody = RestContext.request.requestBody.toString();  
        Map<String, String> deletedObjectNameByCount = new Map<String, String>();
        LegacyPropertyWrapper legacyPropertyIds = (LegacyPropertyWrapper)System.JSON.deserialize(requestBody, LegacyPropertyWrapper.class);
        if(System.Label.AllowDataPurging == 'True'){
            try{
                DataPurgeForMarine propertiesPurge = new DataPurgeForMarine();
                deletedObjectNameByCount = propertiesPurge.deletePropertiesByLegacyProperty(legacyPropertyIds.entityIds); 
            }catch(Exception ex){
                deletedObjectNameByCount.put('Error', ex.getMessage());
                system.debug('error message'+ deletedObjectNameByCount);
            }
        }else{
            deletedObjectNameByCount.put('Error', system.label.DataPurgingError);
        }
        
        return deletedObjectNameByCount;
    }
    
    public class LegacyPropertyWrapper {
        List<String> entityIds;
        public LegacyPropertyWrapper(){
            
        }
    }
    
}