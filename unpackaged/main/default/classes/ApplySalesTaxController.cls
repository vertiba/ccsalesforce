/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//=======================================================================================================================
//
//  #####   ##   ##  #####   ##      ##   ####  ##   ####         ####    ###    #####   ##  #####  ##     ##  ######
//  ##  ##  ##   ##  ##  ##  ##      ##  ##     ##  ##           ##      ## ##   ##  ##  ##  ##     ####   ##    ##
//  #####   ##   ##  #####   ##      ##  ##     ##   ###          ###   ##   ##  #####   ##  #####  ##  ## ##    ##
//  ##      ##   ##  ##  ##  ##      ##  ##     ##     ##           ##  #######  ##      ##  ##     ##    ###    ##
//  ##       #####   #####   ######  ##   ####  ##  ####         ####   ##   ##  ##      ##  #####  ##     ##    ##
//
//=======================================================================================================================


/**********************************************************
 *                APPLYSALESTAXCONTROLLER                 *
 * CONTROLLER CLASS FOR LIGHTNING COMPONENT APPLYSALESTAX *
 **********************************************************/



public with sharing class ApplySalesTaxController {

    @AuraEnabled
    public static List<ApplySalesTaxWrapper> getAllAssessmentLineItems(String caseId) {

        List<ApplySalesTaxWrapper> applySalesTaxWrappers = new List<ApplySalesTaxWrapper>();
        if(String.isBlank(caseId)) return applySalesTaxWrappers;

        for(AssessmentLineItem__c assessmentLineItem : [SELECT Id, Name, SalesTax__c, AcquisitionYear__c, Subcategory__c, Cost__c, ManualAdjustedCost__c,  
                                                        Value__c, SalesTaxPercentage__c
                                                        FROM AssessmentLineItem__c
                                                        WHERE Case__c =:caseId
                                                        ORDER BY Subcategory__c ASC, AcquisitionYear__c DESC]) {                                                         
            if(String.isBlank(assessmentLineItem.SalesTax__c)){
                applySalesTaxWrappers.add(new ApplySalesTaxWrapper(assessmentLineItem));
            } else{
                applySalesTaxWrappers.add(new ApplySalesTaxWrapper(assessmentLineItem,true));
            }

        }
        return applySalesTaxWrappers;
    }

    @AuraEnabled
    public static void applySalesTaxAction(String assessmentLineItemRecords) {
        if(String.isBlank(assessmentLineItemRecords)) return;
        String defaultAcquisitionYear = System.Label.DefaultAcquisitionYear;
        List<ApplySalesTaxWrapper> applySalesTaxWrappers = (List<ApplySalesTaxWrapper>)System.JSON.deserialize(assessmentLineItemRecords,List<ApplySalesTaxWrapper>.class);

        List<AssessmentLineItem__c> assessmentLineItemsToUpdate = new List<AssessmentLineItem__c>();
        Set<String> acquisitionYears = new Set<String>();
        acquisitionYears.add(defaultAcquisitionYear);
        Map<String,SalesTax__c> salesTaxByYear = new Map<String,SalesTax__c>();
        for(ApplySalesTaxWrapper applySalesTaxWrapperRecord : applySalesTaxWrappers){
            acquisitionYears.add(applySalesTaxWrapperRecord.assessmentLineItem.AcquisitionYear__c);
        }
        for(SalesTax__c salesTax : [SELECT Id, Sales_Tax_Year__c,SalesTaxPercentage__c FROM SalesTax__c Where Sales_Tax_Year__c IN :acquisitionYears ]){
            salesTaxByYear.put(salesTax.Sales_Tax_Year__c,salesTax);
        }

        for(ApplySalesTaxWrapper applySalesTaxWrapperRecord : applySalesTaxWrappers) {
            if(applySalesTaxWrapperRecord.isSelected) {
                if(Integer.valueof(applySalesTaxWrapperRecord.assessmentLineItem.AcquisitionYear__c) <= Integer.valueOf(defaultAcquisitionYear)) {
                    applySalesTaxWrapperRecord.assessmentLineItem.SalesTax__c           = salesTaxByYear.get(defaultAcquisitionYear).Id;
                    applySalesTaxWrapperRecord.assessmentLineItem.SalesTaxPercentage__c = salesTaxByYear.get(defaultAcquisitionYear).SalesTaxPercentage__c;
                } else {
                    applySalesTaxWrapperRecord.assessmentLineItem.SalesTax__c           = salesTaxByYear.get(applySalesTaxWrapperRecord.assessmentLineItem.AcquisitionYear__c).Id;
                    applySalesTaxWrapperRecord.assessmentLineItem.SalesTaxPercentage__c = salesTaxByYear.get(applySalesTaxWrapperRecord.assessmentLineItem.AcquisitionYear__c).SalesTaxPercentage__c;
                }
            } else {
                applySalesTaxWrapperRecord.assessmentLineItem.SalesTax__c           = null;
                applySalesTaxWrapperRecord.assessmentLineItem.SalesTaxPercentage__c = null;
            }
            assessmentLineItemsToUpdate.add(applySalesTaxWrapperRecord.assessmentLineItem);
        }

        if(!assessmentLineItemsToUpdate.isEmpty()) {
            update assessmentLineItemsToUpdate;
        }
    }
}