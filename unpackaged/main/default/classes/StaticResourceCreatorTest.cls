/*
 * Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d
 * Class Name: StaticResourceCreatorTest 
 * Description : Test Class for DataPurge and StaticResourceCreator class
*/
@isTest
public class StaticResourceCreatorTest {

    /*
     * Method Name: staticResourceTest
     * Description: This method tests all the functions in the StaticResourceCreator class 
     */ 
    public static TestMethod void staticResourceTest(){
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('VIPForm_571LA');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        StaticResourceCreator.saveStaticResource('VIPForm_571LA', '{"Name":"571-LA"}', 'application/json');
        Test.stopTest();
    }
}