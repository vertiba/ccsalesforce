/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/ 
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test LoadStaticData
//-----------------------------
@isTest
public class LoadStaticDataTest {

    @isTest
    private static void processSampleAccountsTest(){
        Test.startTest();
        List<RollYear__c> rollyears = [select id, year__c, Status__c from RollYear__c];
        System.assertEquals(0, rollyears.size());
        new LoadStaticData().loadDataFromStaticResource('RollYearData');
        rollyears = [select id, year__c, Status__c from RollYear__c];
        //Cant test for update as duplicate id will be empty, when tested from test class.
        System.assertNotEquals(0, rollyears.size());
        Test.stopTest();
    }
    
    /*
     * Method Name: negativeScenario
     * Description: This method tests the negative scenario for LoadStaticData class 
     */ 
    public static TestMethod void negativeScenario(){
        Test.startTest();
        Schema.SObjectType objectType= Schema.getGlobalDescribe().get('RollYear__c');
        new LoadStaticData().doUpsert(new List<sObject>{new RollYear__c()}, objectType);
        Test.stopTest();
    }
}