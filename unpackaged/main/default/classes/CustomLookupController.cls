/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Apex Controller for CustomLookup LWC. 
//-----------------------------

public with sharing class CustomLookupController {

    //-----------------------------
    //@author : Publicis Sapient 
    //@param : searchKey : Search String, objectName = object api name on which the search should be done
    //@description : This method is send the lookedup records on the basis of search key into the component.
    //@return : void
    //-----------------------------
    @AuraEnabled(cacheable=true)  
    public static List<sobject> findRecords(String searchKey, String objectName) { 
         
        string searchText = '\'' + String.escapeSingleQuotes(searchKey) + '%\''; 
        string query;
        if(objectName != 'case') { 
             query = 'SELECT Id, Name FROM ' +objectName+ ' WHERE Name LIKE '+searchText+' LIMIT 5'; 
        } else {
            query = 'SELECT Id, CaseNumber FROM ' +objectName+ ' WHERE CaseNumber LIKE '+searchText+' LIMIT 5'; 
        }
        
        return Database.query(query);  
   }
}