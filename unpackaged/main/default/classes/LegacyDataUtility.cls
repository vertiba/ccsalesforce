/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class LegacyDataUtility {

	public static Case convertAssessment(Case assessment,Id recordTypeId) {

		assessment.RecordTypeId = recordTypeId;
		assessment.IsLegacy__c = false;
		assessment.AssessmentCompositeKey__c = null;
		assessment.SequenceNumber__c = null;
		return assessment;
	}

	public static AssessmentLineItem__c  convertAssessmentLineItem() {
		return new AssessmentLineItem__c  ();
	}
}