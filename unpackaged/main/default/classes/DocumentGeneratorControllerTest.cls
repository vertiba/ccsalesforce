/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for DocumentGeneratorController
 * @Date: 01/28/2020
 * @Author: Srini Aluri(A1)
 
 * ModifiedBy         ModifiedDate   Description
 * Srini Aluri(A1)    01/31/2020     Initial development
*/

@isTest
public class DocumentGeneratorControllerTest {

    @testSetup
    static void createTestData(){
        TestDataUtility.disableAutomationCustomSettings(true);
        List<TH1__Schema_Set__c> schemaSets = new List<TH1__Schema_Set__c>();
        TH1__Schema_Set__c schemaSet1 = new TH1__Schema_Set__c();
        schemaSets.add(schemaSet1);
        TH1__Schema_Set__c schemaSet2 = new TH1__Schema_Set__c();
        schemaSets.add(schemaSet2);
        INSERT schemaSets;

        List<TH1__Schema_Object__c> schemaObjects = new List<TH1__Schema_Object__c>();
        TH1__Schema_Object__c schemaObject1 = new TH1__Schema_Object__c();
        schemaObject1.TH1__Is_Primary_Object__c = true;
        schemaObject1.TH1__Object_Label__c = 'Account';
        schemaObject1.TH1__Schema_Set__c = schemaSets.get(0).Id;
        schemaObject1.Name = 'Account';
        schemaObjects.add(schemaObject1);

        TH1__Schema_Object__c schemaObject2 = new TH1__Schema_Object__c();
        schemaObject2.TH1__Is_Primary_Object__c = true;
        schemaObject2.TH1__Object_Label__c = 'Property__c';
        schemaObject2.TH1__Schema_Set__c = schemaSets.get(1).Id;
        schemaObject2.Name = 'Property__c';
        schemaObjects.add(schemaObject2);
        INSERT schemaObjects;

        schemaSets.get(0).TH1__Primary_Object__c = schemaObjects.get(0).Id;
        schemaSets.get(1).TH1__Primary_Object__c = schemaObjects.get(1).Id;
        UPDATE schemaSets;

        List<String> documentSettingNames = new List<String>{'Account Document Setting', 'Notice of Requirement to File 571-L'};
        List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        
        // Create Document Setting for Account
        TH1__Document_Setting__c docSetting1 = new TH1__Document_Setting__c();
        docSetting1.TH1__Document_Data_Model__c = schemaSets.get(0).Id;
        docSetting1.Name = documentSettingNames.get(0);
        docSetting1.TH1__Storage_File_Name__c = documentSettingNames.get(0);
        docSetting1.TH1__Is_disabled__c = false;
        docSetting1.TH1__Generate_document__c = true;
        docSetting1.TH1__Filter_field_name__c = 'NoticeTemplate__c';
        docSetting1.TH1__Filter_field_value__c = 'Notice to File';
        documentSettings.add(docSetting1);        
    
		// Create Document Setting for Account
		TH1__Document_Setting__c docSetting2 = new TH1__Document_Setting__c();
        docSetting2.TH1__Document_Data_Model__c = schemaSets.get(1).Id;
        docSetting2.Name = documentSettingNames.get(1);
        docSetting2.TH1__Storage_File_Name__c = documentSettingNames.get(1);
        docSetting2.TH1__Is_disabled__c = false;
        docSetting2.TH1__Generate_document__c = true;
        docSetting2.TH1__Filter_field_name__c = 'RecordTypeName__c';
        docSetting2.TH1__Filter_field_value__c = 'Business Personal Property';
        documentSettings.add(docSetting2);
        
    	INSERT documentSettings;
        
        //insert account
        List<Account> accounts = new List<Account>();
        Account businessAccount1 = TestDataUtility.getBusinessAccount();
        accounts.add(businessAccount1);
        
        Account businessAccount2 = TestDataUtility.getBusinessAccount();
        businessAccount2.MailingAddress__c = '1233 STREET BRKSSAN FRANCISCO, CA 94104-9410';
        accounts.add(businessAccount2);
        INSERT accounts;
        
        List<Property__c> properties = new List<Property__c>();
        Property__c property1 = TestDataUtility.getBPPProperty();
        property1.Account__c = businessAccount1.Id;
        properties.add(property1);
        
        Property__c property2 = TestDataUtility.getBPPProperty();
        property2.Account__c = businessAccount2.Id;
        properties.add(property2);
        
        Property__c property3 = TestDataUtility.getVesselProperty();
        property3.Account__c = businessAccount2.Id;
        properties.add(property3);
        INSERT properties;
        TestDataUtility.disableAutomationCustomSettings(false);
    }
    	
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method to check getMailingAddress() method return value
    //-----------------------------
    @isTest
    static void testGetMailingAddressIfAccountMailingAdressIsNull() {
        Property__c  bppProperty = [select id, Account__r.MailingAddress__c from Property__c where Account__r.MailingAddress__c = null];
		Boolean isMailingAddressAvailable = DocumentGeneratorController.getMailingAddress(bppProperty.Id);
        System.assertEquals(false, isMailingAddressAvailable);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method to check getMailingAddress() method return value
    //-----------------------------
    @isTest
    static void testGetMailingAddressIfAccountMailingAdressIsNotNull() {
        Property__c  bppProperty = [select id, Account__r.MailingAddress__c,recordtypeId,recordtype.Name  from Property__c where Account__r.MailingAddress__c != null AND recordType.Name = 'Business Personal Property'];
		Boolean isMailingAddressAvailable = DocumentGeneratorController.getMailingAddress(bppProperty.Id);
        System.assertEquals(true, isMailingAddressAvailable);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Test Method to check testCheckMailingAddressOfVesselProperty() method return value
    //-----------------------------
    @isTest
    static void testCheckMailingAddressOfVesselProperty() {
        Property__c  vesselProperty = [select id, Account__r.MailingAddress__c,recordtypeId,recordtype.Name from Property__c where Account__r.MailingAddress__c != null  AND recordtype.Name = 'Vessel'];
		Boolean isMailingAddressAvailableOnVessel = DocumentGeneratorController.getMailingAddress(vesselProperty.Id);
        System.assertEquals(false, isMailingAddressAvailableOnVessel);
    }
    
    static testMethod void getAvailableTemplatesTest() {
        Account act = new Account(Name = 'Sample Test Account');
        INSERT act;
        
        Test.startTest();
        
        List<SObject> tmplList = DocumentGeneratorController.getAvailableTemplates(act.Id);
            
        Test.stopTest();
        
        // Assert functionality - We should expect Account based DocumentSetting object, as we have given accountId above
        TH1__Document_Setting__c docSetting = (TH1__Document_Setting__c)tmplList[0];
        System.assertEquals('Account Document Setting', docSetting.Name);
    }
    
    static testMethod void generateDocumentTest1() {
        TH1__Document_Setting__c docSetting = [SELECT Id 
                                               FROM TH1__Document_Setting__c
                                               WHERE TH1__Storage_File_Name__c = 'Account Document Setting'];
        
        Account act = new Account(Name = 'Sample Test Account');
        INSERT act;
        
        Test.startTest();
        
        DocumentGenerationRequest__c request = DocumentGeneratorController.generateDocument(act.Id, docSetting.Id);
            
        Test.stopTest();
        
        // Assert functionality - Make sure DocumentGenerationRequest__c object is Approved (by default)
        System.assertEquals('Approved', request.Status__c);
    }
    
    static testMethod void generateDocumentTest2() {
        Account act = new Account(Name = 'Sample Test Account');
        INSERT act;
        
        Test.startTest();
        DocumentGeneratorController.DocumentTemplateRequest tmplReq = new DocumentGeneratorController.DocumentTemplateRequest();
        tmplReq.recordId = act.Id;
        tmplReq.documentTemplateName = 'Account Document Setting';
        
        DocumentGeneratorController.generateDocument(new List<DocumentGeneratorController.DocumentTemplateRequest>{tmplReq});
            
        Test.stopTest();
        
        // Assert functionality - Make sure respected DocumentGenerationRequest__c records are generated successfully
        TH1__Document_Setting__c docSetting = [SELECT Id 
                                               FROM TH1__Document_Setting__c
                                               WHERE TH1__Storage_File_Name__c = 'Account Document Setting'];
        
        List<DocumentGenerationRequest__c> docGenReqList = [SELECT Id, Name, Status__c
                                                            FROM DocumentGenerationRequest__c
                                                            WHERE DocumentSetting__c = :docSetting.Id];
        System.assertEquals(false, docGenReqList.isEmpty());
    }
}