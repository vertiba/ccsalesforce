/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This is a test class to ReOpenAnnualRollBatch

    @isTest
    public class ReOpenAnnualRollBatchTest {
    
        //-----------------------------
        // @author : Publicis.Sapient
        // @description : Method For creating data 
        //-----------------------------
        @testSetup
        private static void dataSetup(){
            TestDataUtility.disableAutomationCustomSettings(true);
            //insert penalty
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
                    
            //insert account
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            Property__c property = TestDataUtility.getVesselProperty();
            property.Account__c = businessAccount.Id;
            insert property;
            
            String currentYear 		    = String.valueof(system.today().year());
            String nextYear 		    = String.valueof(system.today().year()+1);
            Integer currentFiscalYear 	= FiscalYearUtility.getCurrentFiscalYear();         
            Date filingDate = Date.newInstance(Integer.valueOf(currentYear), 1, 1);
            
            List<RollYear__c> rollYears = new List<RollYear__c>();
            RollYear__c rollYear = new RollYear__c();
            rollYear.Name = currentYear;
            rollYear.Year__c = currentYear;
            rollYear.Status__c='Roll Open';
            rollYear.IntegrationStatus__c=CCSFConstants.IN_TRANSIT_TO_TTX;
            rollYear.FailureReason__c='some failed reason';
            rollYear.TriggerBatchId__c='111111fe-11b1-1c11-11b1-ed1111111a11';
            rollYears.add(rollYear);
            
            RollYear__c rollYear2 = new RollYear__c();
            rollYear2.Name = nextYear;
            rollYear2.Year__c = nextYear;
            rollYear2.Status__c=CCSFConstants.ROLLYEAR.ROLL_YEAR_Close_STATUS;
            rollYears.add(rollYear2);
            
            insert rollYears;
            
            Property__c  vesselProperty = [select id from Property__c where RecordTypeId =:DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME)];
                
            Case assessment  = TestDataUtility.buildAssessment(DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.VESSEL_RECORD_TYPE_API_NAME),vesselProperty.Id,
                                                                        rollYear.Id,rollYear.Name,rollYear.Name,
                                                                        CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,CCSFConstants.NOTICE_TYPE_DIRECT_BILL,null,Date.newInstance(currentFiscalYear, 1, 1));
            assessment.Status = 'Closed';
            assessment.IntegrationStatus__c = CCSFConstants.READY_TO_SENT_TO_TTX;
            assessment.AdjustmentType__c = CCSFConstants.ASSESSMENT.STATUS_NEW;
            assessment.TriggerBatchId__c='111111fe-11b1-1c11-11b1-ed1111111a11';
            assessment.AccountId = property.account__c;
            assessment.Property__c = property.Id;
            insert assessment;
            TestDataUtility.disableAutomationCustomSettings(false);
        }
   
        //-----------------------------
        // @author : Publicis.Sapient
        // @description : Method to check if batch class is triggering for current roll
        //----------------------------- 
        @isTest
        static void testReOpenAnnualRollForCurrentYear() {
            String currentYear 		= String.valueof(system.today().year());
            Test.startTest();         
            
             ReOpenAnnualRollBatch  reOpenAnnualRollBatch = new ReOpenAnnualRollBatch();
             Database.executeBatch(reOpenAnnualRollBatch);
        	 Test.stopTest();

        	 // Assert the functionality
        	 for(RollYear__c rollYear : [select id,IntegrationStatus__c, FailureReason__c, TriggerBatchId__c, Status__c from RollYear__c where Year__c = :currentYear])
             {
                 System.assertEquals(CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS, rollYear.Status__c);
                 System.assertEquals(null, rollYear.IntegrationStatus__c);
                 System.assertEquals(null, rollYear.FailureReason__c);
                 System.assertEquals(null, rollYear.TriggerBatchId__c);
             }
        	 for(Case updatedCase : [select id,IntegrationStatus__c, TriggerBatchId__c from case])
             {
                 System.assertEquals(null, updatedCase.IntegrationStatus__c);
                 System.assertEquals(null, updatedCase.TriggerBatchId__c);
             }
        }
        
         @isTest
    static void errorFrameworkTest(){
       Case assessment = [Select id from Case limit 1];
        
        Id testJobId = '707S000000nKE4fIAG';
        BatchApexErrorLog__c testJob = new BatchApexErrorLog__c();
        testJob.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testJob.JobCreatedDate__c = System.today();
        testJob.JobId__c = testJobId;
        database.insert(testJob);        
        
        BatchApexError__c testError = new BatchApexError__c();
        testError.AsyncApexJobId__c = testJobId;
        testError.BatchApexErrorLog__c = testJob.Id;
        testError.DoesExceedJobScopeMaxLength__c = false;
        testError.ExceptionType__c = BatchableErrorTestJob.TestJobException.class.getName();
        testError.JobApexClass__c = BatchableErrorTestJob.class.getName();
        testError.JobScope__c = assessment.Id;
        testError.Message__c = 'Test exception';
        testError.RequestId__c = null;
        testError.StackTrace__c = '';
        database.insert(testError);
        
        
        BatchableError batchError = BatchableError.newInstance(testError);
        ReOpenAnnualRollBatch batchRetry= new ReOpenAnnualRollBatch();
        batchRetry.handleErrors(batchError);
        system.assertEquals('707S000000nKE4fIAG', testError.AsyncApexJobId__c);
    }
}