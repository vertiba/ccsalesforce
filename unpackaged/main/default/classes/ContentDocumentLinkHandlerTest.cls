/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @story number : ASR-8201
// @description : This class is used to test class for ContentDocumentLinkHandler.
//-----------------------------
@istest
public with sharing class ContentDocumentLinkHandlerTest {
    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
    private static Integer nextYear 		= currentYear+1;
    private static Date filingDate          = System.today();
    
    @TestSetup
    static void setupData(){

        User sysAdmin= TestDataUtility.getSystemAdminUser();
        insert sysAdmin;        
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        System.runAs(systemAdminUser) {   
            User taxpayer = TestDataUtility.getPortalUser();

            //insert roll years
            List<RollYear__c> rollYears = new List<RollYear__c>();
            
            RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYearCurrentFiscalYear.IsLowValueBulkGeneratorProcessed__c = true;
            rollYearCurrentFiscalYear.IsDirectBillBulkGeneratorProcessed__c = true;            
            rollYears.add(rollYearCurrentFiscalYear);
            
            RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastYear),String.valueof(lastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastFiscalYear);
            
            RollYear__c rollYearLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastToLastYear),String.valueof(lastToLastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastToLastFiscalYear);
            
            RollYear__c rollYearNextFiscalYear = TestDataUtility.buildRollYear(String.valueof(nextYear),String.valueof(nextYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearNextFiscalYear);
             
            insert rollYears;
             
            //insert penalty
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;        
        
            //Insert Account
            Account account = TestDataUtility.getBusinessAccount();
            account.AgentPIN__c='1234';
            insert account;
            System.debug(' account @@@ '+[Select EntityId__c from Account where id =:account.id]);        
            //Insert Property
            Property__c property = TestDataUtility.getBPPProperty();
            property.Account__c = account.Id;
            insert property;   
            
            
            //Create Statement         
            Map<String,String> inputParamsStatement = new Map<String,String>();            
            inputParamsStatement.put('accountId',account.Id);
            inputParamsStatement.put('propertyId',property.Id);
            inputParamsStatement.put('formType','571-L');
            inputParamsStatement.put('businessScenario','Existing');
            inputParamsStatement.put('propertyScenario','Existing');
            inputParamsStatement.put('assessmentYear',String.valueOf(lastYear)); 
            inputParamsStatement.put('recordTypeId',DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME));
            inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/'+lastToLastYear));
            inputParamsStatement.put('filingDate',String.valueOf('01/03/'+lastYear));            
            Statement__c statement = TestDataUtility.buildStatement(inputParamsStatement);
            //Insert Statement  
            insert statement;
         
            //Create Statement Reported Asset
            Id statementReportedOwnedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.OWNED_ASSETS_RECORD_TYPE_API_NAME);   
            List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
            StatementReportedAsset__c statementReportedAsset1 = new StatementReportedAsset__c();
            statementReportedAsset1 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Bldg/Bldg Impr/Leasehold Impr/Land/Land Impr', 'Land and Land Development', '2017', 100, null, statement.id, 'Statement Reported Asset for last year');
            statementReportedAssets.add(statementReportedAsset1);        
            StatementReportedAsset__c statementReportedAsset2 = new StatementReportedAsset__c();
            statementReportedAsset2 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Bldg/Bldg Impr/Leasehold Impr/Land/Land Impr', 'Land Improvements', '2016', 100, null, statement.id, 'Statement Reported Asset for last year');        
            statementReportedAssets.add(statementReportedAsset2);
            //Insert Statement Reported Asset
            insert statementReportedAssets;

            ReportedAssetSchedule__c reportedAssetSchedule1 = 
                TestDataUtility.buildReportedAssetSchedule(statementReportedAsset1.id,'January','2017',10000,
                                                          'New Report Asset Schedule', 'Addition');
            insert reportedAssetSchedule1;
        
            Id customerCaseRecordId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.CUSTOMER_CARE_RECORD_TYPE_API_NAME);
            Case newcase =TestDataUtility.buildCustomerCase(account.Id, property.Id, customerCaseRecordId);         
            insert newCase;
        }
    }

    @isTest
    static void setDocumentTypeBPPStatementsSupportTest() {       
        
         Statement__c statement = [select id, property__c from Statement__c limit 1]; 
          
        //Create Document
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.Title = 'Test Document';
        contentVersion.PathOnClient = 'TestDocument.pdf';
        contentVersion.VersionData = Blob.valueOf('Test Content');
        contentVersion.IsMajorVersion = true;
        insert contentVersion;        
        
        //Get Content Documents
        List<ContentDocument> contentDocuments = [Select id, Title from ContentDocument];
      
        //Create ContentDocumentLink 
       
        ContentDocumentLink contentDocumentLink = New ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = statement.Id;
        contentDocumentLink.ContentDocumentId = contentDocuments[0].id;
        contentDocumentLink.shareType = 'V';
        insert contentDocumentLink;
      
        ContentVersion contentVersionResult=[SELECT Id,AIMSUploadStatus__c,ContentDocumentId,DocumentType__c FROM ContentVersion 
                                                     LIMIT 1];
       
         system.assertEquals('Ready to upload', contentVersionResult.AIMSUploadStatus__c, 'Ready to upload document AIMSUploadStatus');
         system.assertEquals('BPP-Statements-Support Document', contentVersionResult.DocumentType__c, 'BPP-Statements-Support Document');
    }


    @istest
    static void customerCaseTest(){
    
        Case newCase=[Select Id from Case limit 1];
        //Create Document
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.Title = 'Test Document';
        contentVersion.PathOnClient = 'TestDocument.pdf';
        contentVersion.VersionData = Blob.valueOf('Test Content');
        contentVersion.IsMajorVersion = true;
        insert contentVersion;        
        
        //Get Content Documents
        List<ContentDocument> contentDocuments = [Select id, Title from ContentDocument];
        
        //Create ContentDocumentLink 
       
        ContentDocumentLink contentDocumentLink = New ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = newcase.Id;
        contentDocumentLink.ContentDocumentId = contentDocuments[0].id;
        contentDocumentLink.shareType = 'V';
        insert contentDocumentLink;

        ContentVersion contentVersionResult=[SELECT Id,AIMSUploadStatus__c,ContentDocumentId,DocumentType__c FROM ContentVersion 
                                                     LIMIT 1];
        system.assertEquals('Ready to upload', contentVersionResult.AIMSUploadStatus__c, 'Ready to upload document AIMSUploadStatus');
        system.assertEquals('BPP-Incoming-Taxpayer-Request', contentVersionResult.DocumentType__c, 'BPP-Incoming-Taxpayer-Request Document');

    }

    @isTest
    static void exSupportDocumentTest(){
        Property__c property=[Select Id from Property__c limit 1];

        Statement__c exemptionStatement=TestDataUtility.buildExemptionStatement(property.Id,DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.PROPERTY_EVENT_RECORD_TYPE_API_NAME),'BOE-260');
        insert exemptionStatement;
        
         //Create Document
         ContentVersion contentVersion = new ContentVersion();
         contentVersion.Title = 'Test Document';
         contentVersion.PathOnClient = 'TestDocument.pdf';
         contentVersion.VersionData = Blob.valueOf('Test Content');
         contentVersion.IsMajorVersion = true;
         insert contentVersion;        
         
         //Get Content Documents
         List<ContentDocument> contentDocuments = [Select id, Title from ContentDocument];
       
         //Create ContentDocumentLink 
        
         ContentDocumentLink contentDocumentLink = New ContentDocumentLink();
         contentDocumentLink.LinkedEntityId = exemptionStatement.Id;
         contentDocumentLink.ContentDocumentId = contentDocuments[0].id;
         contentDocumentLink.shareType = 'V';
         insert contentDocumentLink;
       
         ContentVersion contentVersionResult=[SELECT Id,AIMSUploadStatus__c,ContentDocumentId,DocumentType__c FROM ContentVersion 
                                                      LIMIT 1];
        
          system.assertEquals('Ready to upload', contentVersionResult.AIMSUploadStatus__c, 'Ready to upload document AIMSUploadStatus');
          system.assertEquals('EX-Support Documents', contentVersionResult.DocumentType__c, 'EX-Support Documents');
    }
    
}