/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
public class StatementCommunityCardControllerTest {

    @isTest
    private static void getRecordTypeIdTest() {
        Id statementRecordTypeId = Schema.SObjectType.Statement__c.getRecordTypeInfosByName().get('Business Personal Property Statement').getRecordTypeId();

        Test.startTest();
        String statementRecordTypeName = Schema.SObjectType.Statement__c.getRecordTypeInfosByName().get('Business Personal Property Statement').getName();
        Id statementRecordIdFromController = statementCommunityCardController.getRecordTypeId('Statement__c', statementRecordTypeName);
        Test.stopTest();

        System.assertEquals(statementRecordTypeId, statementRecordIdFromController);
    }

}