//-----------------------------
// @author : Publicis.Sapient
// 		Scheduler class to run VIP Form backup service.
//-----------------------------
public without sharing class VIPFormBackupServiceScheduler extends EnhancedSchedulable {

    public override String getJobNamePrefix() {
        return 'VIP Form Backup Service';
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : SchedulableContext
    // @description : Schedulable interface method. Schedules the VIPFormBackupService job.
    // @return : void
    //-----------------------------
    public void execute(SchedulableContext schedulableContext) {
        Database.executebatch(new VIPFormBackupService(), 10);
    }

}