/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
private class VIPFormsListTest {

    @testSetup
    private static void setupData() {
        VIPForm__VIP_Template__c template = new VIPForm__VIP_Template__c(
            Name                           = 'Test Template',
            VIPForm__Object_Record_Type__c = 'Primary',
            VIPForm__Status__c             = 'Active'
        );
        insert template;
        System.assertNotEquals(null, template.Id);
    }

    @isTest
    private static void getDefaultValue() {
        VisualEditor.DataRow defaultValue = new VIPFormsList().getDefaultValue();
        System.assertEquals('None', defaultValue.getLabel());
        System.assertEquals('', defaultValue.getValue());
    }

    @isTest
    private static void getValues() {
        VisualEditor.DynamicPickListRows expectedPicklistValues = new VisualEditor.DynamicPickListRows();
		expectedPicklistValues.addRow(new VisualEditor.DataRow('None', ''));

        for(VIPForm__VIP_Template__c template : [SELECT Id, Name FROM VIPForm__VIP_Template__c]) {
            String idString = (String)template.Id;
            expectedPicklistValues.addRow(new VisualEditor.DataRow(template.Name, idString));
        }

        Test.startTest();

        VisualEditor.DynamicPickListRows returnedPicklistValues = new VIPFormsList().getValues();
        System.assertEquals(expectedPicklistValues.size(), returnedPicklistValues.size());

        Test.stopTest();
    }

}