/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @Business: Test class for UpdateVesselNoticeTypeController
 * 
 * @Author: Publicis.Sapient
 *
*/
@isTest
public class UpdateVesselNoticeTypeControllerTest {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method For creating data 
    //-----------------------------
    @testSetup
    static void createTestData() {
        
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User nonAdmin = TestDataUtility.getBPPAuditorUser();    
        User sysAdmin = TestDataUtility.getSystemAdminUser();
        INSERT new List<User>{ nonAdmin, sysAdmin };
        PermissionSetAssignment assignment = new PermissionSetAssignment(
        	AssigneeId = sysAdmin.Id,
        	PermissionSetId = permissionSet.Id
    	);
    	insert assignment;    
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to verify permission for RollYearUpdate PermissionSet
    //-----------------------------  
    static testMethod void  checkPermissionForRollYearUpdatePermissionSet() {
        User systemAdmin = [SELECT Id FROM User WHERE Email = 'testsysadmin@asrtest.com'];
        Test.startTest();
        System.runAs(systemAdmin) {
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(new List<Property__c>());
          	UpdateVesselNoticeTypeController extension = new UpdateVesselNoticeTypeController(controller);
            extension.redirect();
          	system.assertEquals(System.Label.UpdateVesselNoticeType,extension.recordStatus);
        }
        Test.stopTest();
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method to verify permission for other user
    //-----------------------------  
    static testMethod void  checkPermissionForOthers() {
        User systemNonAdmin = [SELECT Id FROM User WHERE Email = 'testauditor@asrtest.com'];
        Test.startTest();
        System.runAs(systemNonAdmin) {
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(new List<Property__c>());
          	UpdateVesselNoticeTypeController extension = new UpdateVesselNoticeTypeController(controller);
            extension.redirect();
          	system.assertEquals(System.Label.NoPermission,extension.recordStatus);
        }
        Test.stopTest();
    }

}