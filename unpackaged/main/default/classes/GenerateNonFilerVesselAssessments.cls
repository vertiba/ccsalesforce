/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used as controller for GenerateNonFilerVesselAssessment aura component.

public class GenerateNonFilerVesselAssessments {
        //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method checks permissions before allowing user to generate non filer assessments.          
    // @return : Response wrapper object
	@AuraEnabled
    public static Response checkPermission(Id recordId) {
		
        Response response = new Response();    
        Boolean hasPermission = FeatureManagement.checkPermission(System.Label.MassGenerateNonFilerAssessments); 
        RollYear__c rollYear = [SELECT Status__c,IsLowValueVesselAssessmentgenerator__c,IsDirectBillVesselAssessmentProcess__c,IsVesselNonFilerGeneratorProcessed__c 
                                FROM RollYear__c 
                                WHERE id =: recordId];    
        
        //Check if roll year is not closed, if closed return error message
        if(rollYear.Status__c == CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS)
        {
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.CLOSED_ROLL_YEAR_BUTTON_WARNING;        
            return Response;    
        }else if(!rollYear.IsLowValueVesselAssessmentgenerator__c || !rollYear.IsDirectBillVesselAssessmentProcess__c) {
        //check if low value and direct bill batch already ran, if not ran return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.NONFILER_ASSESMENT_GENERATOR_WARNING ;
            return Response; 
        }else if (rollYear.IsVesselNonFilerGeneratorProcessed__c) {
        //check if non-filer batch has not already ran, if ran return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.NONFILER_ASSESMENT_ALREADY_GENERATED_MSG ;
            return Response; 
        }else if(!hasPermission) {
        //check if user has Custom Permission to run this batch, if not return error message
            response.isSuccess = false;
            response.message = CCSFConstants.BUTTONMSG.NO_PERMISSION;      
            return Response;       
        }else {
        //if all the above condtions are false, means user can run this batch
            response.isSuccess = true;
            response.message = System.Label.HasPermissionNonFiler;     
            return Response;  
        }             
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method runs the NonFilerAssessmentBulkGenerator batch.          
    // @return : Response wrapper object
    @AuraEnabled 
    public static Response runNonFilerAssessmentBulkGenerator() {
        
        Response response = new Response();        
        
        List<AsyncApexJob> apexJob = [Select Id, Status,ApexClass.name from AsyncApexJob 
											 where ApexClass.name =: CCSFConstants.VESSEL_NON_FILER_GENERATOR_BATCH_NAME
											 and Status IN : CCSFConstants.BATCHMSG.BATCH_STATUSES order by CreatedDate desc limit 1];
		
		//Check if the same batch is not already running.
        if(!apexJob.isEmpty())
        {
            response.isSuccess = false;
            response.message = CCSFConstants.BATCHMSG.BATCH_ALREADY_RUNNING_MSG;
        }else {            
            //calling Batch
            NonFilerVesselAssessmentBulkGenerator nonfilerbatch = new NonFilerVesselAssessmentBulkGenerator();
            Id jobId = Database.executeBatch(nonfilerbatch,Integer.valueOf(Label.NonFilerAssessmentGeneratorBatchSize));

            AsyncApexJob apexJobs = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                     TotalJobItems, CreatedBy.Email, ExtendedStatus
                                     from AsyncApexJob where Id = :jobId];
            
            if(apexJobs.Status != CCSFConstants.BATCHMSG.BATCH_STATUS_FAILED) {
                response.isSuccess = true;
                response.message= System.Label.BatchQueued + apexJobs.Status; 
            } else { 
                response.isSuccess = false;
                response.message = System.Label.BatchErrors + apexJobs.NumberOfErrors;
            }  
        }        
        return response;
    }
	
	//wrapper class for response
    public class Response {
        @AuraEnabled
        public Boolean isSuccess;
        @AuraEnabled
        public String message;
        
        public Response() {
            isSuccess =false;
            message='';
        }  
    }
}