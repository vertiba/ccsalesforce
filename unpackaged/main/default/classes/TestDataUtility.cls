/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to create test date for test class.
@isTest
public class TestDataUtility {
    // build User Object
    public static User buildUser(String lastName, String email, String alias, Id profileId, Id roleId) {
        String username = new Uuid().getValue() + '@test.com';
        return new User(
            Alias             = alias,
            Email             = email,
            EmailEncodingKey  = 'UTF-8',
            IsActive          = true,
            LanguageLocaleKey = 'en_US',
            LastName          = lastName,
            LocaleSidKey      = 'en_US',
            ProfileId         = profileId,
            TimeZoneSidKey    = 'America/Los_Angeles',
            Username          = username,
            UserRoleId        = roleId
        );
    }

    public static RollYear__c buildOpenRollYear(String rollYearName, String year){
        return buildRollYear(rollyearName, year, 'Roll Open');
    }

    // build Roll Year Object
    public static RollYear__c buildRollYear(string rollYearName, String year, String status) {
        return new RollYear__c(
            Name        = rollYearName,
            Status__c   = status,
            Year__c     = year
        );
    }

    public static Account buildBusinessAccount(String name){
        return buildAccount(Name, 'Notice to File', Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId());
    }

    //build Account Object
    public static Account buildAccount(string name,string noticeType,string recordTypeId) {
        return new Account(
            BusinessStatus__c       = 'Active',
            MailingCountry__C       = 'US',
            Name                    = name,
            NoticeType__c           = noticeType,
            RecordTypeId            = recordTypeId
        );
    }

    // Creating a new user record
    public static User createUserRecord(String FirstName,String LastName,String alias,Id profileId,
                                       String TimeZoneSidKey,String LocaleSidKey,String LanguageLocaleKey,
                                       String EmailEncodingKey,String email,String UserName)
    {
        return new User(FirstName = FirstName,LastName=LastName,alias=alias,ProfileId = profileId,
                        TimeZoneSidKey = TimeZoneSidKey, LocaleSidKey = LocaleSidKey, LanguageLocaleKey = LanguageLocaleKey,
                        EmailEncodingKey= EmailEncodingKey, email=email, UserName=UserName);

    }

    // build Person Account
    public static Account buildPersonAccount(String firstName,String lastName,String email,String postCode){
        User owner = buildPortalOwner();
        insert owner;
        String recordTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account') and IsPersonType=True].Id;
        return new Account(
            RecordTypeID=recordTypeId,
            FirstName=firstName,
            LastName=lastName,
            PersonEmail=email,
            PersonMailingPostalCode=postCode ,
            OwnerId =owner.Id
        );
    }
    //build Property Object
    public static Property__c buildProperty(string name,string recordTypeId) {
        return new Property__c(
            MailingCountry__c   = 'US',
            Name                = name,
            RecordTypeId        = recordTypeId,
            Status__c           = 'Active'
        );
    }

    public static Property__c buildBusinessPersonalProperty(String name, Id accountId){
        return buildProperty(Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Business Personal Property').getRecordTypeId(), accountId, '0000', 'DBA', 'Test Address');
    }

    //Build Property Object Method
    public static Property__c buildProperty(Id propertyRecordTypeId, Id accountId, string locationIdentificationNumber, string doingBusinessAs,string locationAddress) {
        Property__c property = TestDataUtility.buildProperty('Test',propertyRecordTypeId);
        property.Account__c = accountId;
        property.LocationIdentificationNumber__c = locationIdentificationNumber;
        property.DoingBusinessAs__c = doingBusinessAs;
        property.LocationAddress__c = locationAddress;
        property.LocationStreetName__c = 'test street';
        property.LocationState__c = 'US-CA';
        return property;
    }

    //build Statement Object
    public static Statement__c buildStatement(Map<String,String> inputParamsStatement) {
        return new Statement__c(
            BusinessAccount__c = inputParamsStatement.get('accountId'),
            Property__c = inputParamsStatement.get('propertyId'),
            Form__c = inputParamsStatement.get('formType'),
            BusinessAccountScenario__c = inputParamsStatement.get('businessScenario'),
            PropertyScenario__c= inputParamsStatement.get('propertyScenario'),
            AssessmentYear__c = inputParamsStatement.get('assessmentYear'),
            RecordTypeId = inputParamsStatement.get('recordTypeId'),
            StartofBusinessatthisLocation__c = inputParamsStatement.get('startofBussinessDate') !=null ? Date.parse(inputParamsStatement.get('startofBussinessDate')) : null,
            FileDate__c = inputParamsStatement.get('filingDate') !=null ? Date.parse(inputParamsStatement.get('filingDate')) : null
        );
    }

    //build Statement Object
    public static Statement__c buildStatement(String accountId,String propertyId,String formType,String businessScenario,String propertyScenario,String assessmentYear,String recordTypeId,Date startofBussinessDate,Date filingDate) {
        return new Statement__c(
            AssessmentYear__c                   = assessmentYear,
            BusinessAccount__c                  = accountId,
            BusinessAccountScenario__c          = businessScenario,
            FileDate__c                         = filingDate !=null ? filingDate : null,
            Form__c                             = formType,
            Property__c                         = propertyId,
            PropertyScenario__c                 = propertyScenario,
            RecordTypeId                        = recordTypeId,
            StartofBusinessatthisLocation__c    = startofBussinessDate !=null ? startofBussinessDate : null
        );
    }

    //build Statement Reported Asset Object
    public static StatementReportedAsset__c buildStatementReportedAsset(String recordTypeId, String form, String category, String subCategory,String acquiredYear, Decimal cost, Decimal manualAdjustedCost, Id statementId, String description) {
        return new StatementReportedAsset__c(
            AcquisitionYear__c      = acquiredYear,
            Category__c             = category,
            Cost__c                 = cost,
            Description__c          = description,
            Form__c                 = form,
            ManualAdjustedCost__c   = manualAdjustedCost,
            RecordTypeId            = recordTypeId,
            Statement__c            = statementId,
            Subcategory__c          = subCategory
        );
    }

    //build Statement Reported Asset Object
    public static StatementReportedAsset__c buildStatementReportedAsset(Map<String,String> inputParamsStatement) {
        return new StatementReportedAsset__c(
            AcquisitionYear__c = inputParamsStatement.get('acquiredYear'),
            Category__c = inputParamsStatement.get('category'),
            Cost__c = Decimal.valueof(inputParamsStatement.get('cost')),
            Description__c = inputParamsStatement.get('description'),
            Form__c= inputParamsStatement.get('form'),
            ManualAdjustedCost__c = Decimal.valueof(inputParamsStatement.get('manualAdjustedCost')),
            RecordTypeId = inputParamsStatement.get('recordTypeId'),
            Statement__c = inputParamsStatement.get('statementId'),
            Subcategory__c = inputParamsStatement.get('subCategory')
        );
    }

    // build Reported Asset Schedule Object
    public static ReportedAssetSchedule__c buildReportedAssetSchedule(Id statementReportedAssetId,String month,String year, Decimal cost,String description, String type){
        return new ReportedAssetSchedule__c(
            StatementReportedAsset__c = statementReportedAssetId,
            Addition_Disposal_Month__c= month,
            Addition_Disposal_Year__c = year,
            Cost__c = cost,
            Description__c= description,
            Type__c= type
        );
    }

    // build Reported Asset Schedule Object
    public static ReportedAssetSchedule__c buildReportedAssetSchedule(Map<String,String> inputParamsStatement) {
        return new ReportedAssetSchedule__c(
            StatementReportedAsset__c = inputParamsStatement.get('statementReportedAssetId'),
            Addition_Disposal_Month__c = inputParamsStatement.get('month'),
            Addition_Disposal_Year__c = inputParamsStatement.get('year'),
            Cost__c =  Decimal.valueof(inputParamsStatement.get('cost')),
            Description__c= inputParamsStatement.get('description'),
            Type__c = inputParamsStatement.get('type')
        );
    }
    //build Penalty Object
    public static Penalty__c buildPenalty(decimal maxAmount,integer percent,string rtcode) {
        return new Penalty__c(
            PenaltyMaxAmount__c = maxAmount,
            Percent__c          = percent,
            RTCode__c           = rtcode
        );
    }

    //build Assessments Object
    public static Case buildAssessment(Id recordTypeId,Id propertyId,Id rollId,String rollYear,String assessmentYear, String type,Integer statute,String noticeType,Id penalty,Date eventDate) {
        return new Case(
            AssessmentYear__c       = assessmentYear,
            EventDate__c            = eventDate,
            NoticeType__c           = noticeType,
            Penalty__c              = penalty,
            Property__c             = propertyId,
            RecordTypeId            = recordTypeId,
            Roll__c                 = rollId,
            RollYear__c             = rollYear,
            Status                  = 'In Progress',
            StatuteOfLimitations__c = statute,
            Type                    = type
        );
    }

    public static Case buildRegularBPPAssessment(Id propertyId, RollYear__c rollYear, String assessmentYear, String noticeType, Id penaltyId, Date eventDate){
        return new Case(
            AssessmentYear__c = assessmentYear,
            EventDate__c = eventDate,
            NoticeType__c = noticeType,
            Penalty__c = penaltyId,
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME).getRecordTypeId(),
            Roll__c = rollYear.Id,
            RollYear__c = rollYear.Year__c,
            Status = 'In Progress'
        );
    }

    // build Assessments LineItems Object
    public static AssessmentLineItem__c buildAssessmentLineItem(String acquiredYear, Decimal cost,Id caseId) {
        return new AssessmentLineItem__c(
            AcquisitionYear__c = acquiredYear,
            Cost__c             = cost,
            Case__c             = caseId,
            Subcategory__c      = 'Construction in Progress'
        );
    }

    // build Assessments LineItems Object
    public static AssessmentLineItem__c buildAssessmentLineItem(String category, String subCategory, String acquiredYear, Decimal cost,Id caseId, String description) {

        AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem(acquiredYear, cost, caseId);
        assessmentLineItem.Category__c           = category;
        assessmentLineItem.Subcategory__c        = subCategory;
        assessmentLineItem.Description__c        = description;
        return assessmentLineItem;
    }

    //Build Staging: Aumentum Business Object Method
    public static StagingAumentumBusiness__c buildStagingAumentumBusiness(string businessAccountNumber, string locationIdentificationNumber,string taxIdentificationNumber,string tradeName, string businessName,string locationState,string locationCity,string transientOccupancyTax,string transientOccupancyTaxSmallOperator,string lessor,string locationStreetName, string locStartDate, string locEndDate, string busStartDate, string busEndDate) {
        return new StagingAumentumBusiness__c(
            Status__c = 'New',
            BusinessAccountNumber__c = businessAccountNumber,
            LocationIdentificationNumber__c = locationIdentificationNumber,
            TaxIdentificationNumber__c = taxIdentificationNumber,
            TradeName__c = tradeName,
            BusinessName__c = businessName,
            SFEmployees__c = '123',
            LocationStartDate__c = locStartDate,
            BusinessStartDate__c = busStartDate,
            BusinessEndDate__c = BusEndDate,
            LocationEndDate__c =  locEndDate,
            ContactName__c = 'Test Contact',
            ContactEmail__c = 'test@fakemail.com',
            LocationState__c = locationState,
            LocationStreetName__c = locationStreetName,
            TransientOccupancyTax__c = transientOccupancyTax,
            TransientOccupancyTaxSmallOperator__c = transientOccupancyTaxSmallOperator,
            Lessor__c = lessor,
            ContactPostalCode__c = '94103',
            LocationPostalCode__c = '94103'
        );
    }


    // Build Staging Compare Review Object Method
    public static StagingBusinessCompareField__c buildStagingCompareField(Id parentId, Id accountId, Id propertyId, String primaryFormType){
        return new StagingBusinessCompareField__c(
            ParentStagingAumentumBusiness__c=parentId,
            MappedBusiness__c=accountId,
            MappedProperty__c=propertyId,
            StagingPrimaryFormType__c = primaryFormType
        );
    }

      // build AccountObject -for Aumentum
      public static Account buildAccountAumentum(String name, String ban,String fein, Date startDate, Date closeDate){
        Id businessAccountRecordTypeId = DescribeUtility.getRecordTypeId(Account.sObjectType, CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME);
        return new Account(
            BusinessStatus__c             = 'Active',
            MailingCountry__c             = 'US',
            MailingState__c               ='US-CA',
            MailingPostalCode__c          ='94104',
            MailingPostalCodeExtension__c = '303',
            Name                          = name,
            BusinessAccountNumber__c      = ban,
            FederalId__c                  = fein,
	        BusinessCloseDate__c          = closeDate !=null ? closeDate : null ,
            BusinessOpenDate__c           = startDate !=null ? startDate : null,
            RecordTypeId                  = businessAccountRecordTypeId


        );
    }

    // build Aumentum property
    public static Property__c buildAumentumProperty(String lin,String dba,String formType, Date locOpenDate,Date locCloseDate,Id accountId, String streetName){
        Id businessPropertyRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);
        return new Property__c(
            Status__c                          = 'Active',
            LocationIdentificationNumber__c    = lin,
	        DoingBusinessAs__c	               = dba,
            BusinessLocationOpenDate__c        = locOpenDate  != null ? locOpenDate: null,
            BusinessLocationCloseDate__c       = locCloseDate != null ? locCloseDate: null,
	        PrimaryFormType__c                 = formType,
            Account__c                         = accountId,
            LocationStreetName__c              = streetName,
            LocationState__c                   = 'US-CA',
            recordTypeId                       =  businessPropertyRecordTypeId
        );
    }

     // build Aumentum property
     public static Property__c buildAumentumProcessProperty(String lin,String dba,String formType, Date locOpenDate,Date locCloseDate,Id accountId, String streetName,String streetNumber,String streetType,String unitNumber,String unitType,String preDirection, String city, String state, String postalCode,String exten){
        Id businessPropertyRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);
        return new Property__c(
            Status__c                          = 'Active',
            LocationIdentificationNumber__c    = lin,
	        DoingBusinessAs__c	               = dba,
            BusinessLocationOpenDate__c        = locOpenDate  != null ? locOpenDate: null,
            BusinessLocationCloseDate__c       = locCloseDate != null ? locCloseDate: null,
	        PrimaryFormType__c                 = formType,
            Account__c                         = accountId,
            LocationStreetName__c              = streetName,
            LocationStreetNumber__c            = streetNumber,
            LocationStreetType__c              = streetType,
            LocationUnitNumber__c              = unitNumber,
            LocationUnitType__c                = unitType,
            LocationStreetPreDirection__c      = preDirection,
            LocationCity__c                    = city,
            LocationState__c                   = state,
            LocationPostalCode__c              = postalCode,
            LocationPostalCodeExtension__c     = exten,
            recordTypeId                       =  businessPropertyRecordTypeId
        );
    }

    //  build PotentialMatchForBusinessAndLocation__c
    public static PotentialMatchForBusinessAndLocation__c buildPotentialMatchAumentum(Id parentId,Id accountId, Id propertyId){
        return new PotentialMatchForBusinessAndLocation__c(
            ParentAumentumBusiness__c=  parentId,
            PotentialMatchBusiness__c = accountId !=null ? accountId : null,
            PotentialMatchLocation__c = propertyId != null? propertyId : null
        );
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Principal User object.
    // @return : return user record
    public static User getPrincipalUser() {
        Id principalProfileId = [Select Id from Profile where Name =: CCSFConstants.OMNICHANNEL.AUDITOR_PROFILE_NAME].Id;
        Id principalRoleId = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName =:CCSFConstants.OMNICHANNEL.BPP_PRINCIPAL_ROLE_NAME ].Id;
        User principalUser = buildUser('principalUser', 'testprinicipal@asrtest.com', 'pruser', principalProfileId,principalRoleId);
        return principalUser;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Chief User object.
    // @return : return user record
    public static User getChiefUser() {
        Id chiefProfileId = [Select Id from Profile where Name =: CCSFConstants.OMNICHANNEL.AUDITOR_PROFILE_NAME].Id;
        Id chiefRoleId = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName =:CCSFConstants.ROLE_BPPCHIEF].Id;
        User chieflUser = buildUser('cheifUser', 'testchiefl@asrtest.com', 'cfuser', chiefProfileId,chiefRoleId);
        return chieflUser;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create System Admin User object.
    // @return : return user record
    public static User getSystemAdminUser() {
        Id systemAdminProfileId = [Select Id from Profile where Name='System Administrator'].Id;
        User systemAdminUser = buildUser('sysAdminUser', 'testsysadmin@asrtest.com', 'syadm', systemAdminProfileId,null);
        return systemAdminUser;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Auditor User object.
    // @return : return user record
    public static User getBPPAuditorUser() {
        Id auditorProfileId = [Select Id from Profile where Name =:CCSFConstants.OMNICHANNEL.AUDITOR_PROFILE_NAME].Id;
        Id auditorRoleId = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName = :CCSFConstants.OMNICHANNEL.BPP_AUDITOR].Id;
        User auditorUser = buildUser('auditiorUser', 'testauditor@asrtest.com', 'auduser', auditorProfileId,auditorRoleId);
        return auditorUser;
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Office Assistant User object.
    // @return : return user record
    public static User getOfficeAssistantUser() {
        Id officeAssistantProfileId = [Select Id from Profile where Name =:CCSFConstants.OMNICHANNEL.AUDITOR_PROFILE_NAME].Id;
        Id officeAssistantRoleId = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName =:CCSFConstants.OMNICHANNEL.BPP_OFFICE_ASSISTANT_ROLE_NAME].Id;
        User officeAssistantUser = buildUser('officeAssistantUser', 'testOfficeAssistantUser@asrtest.com', 'ofuser', officeAssistantProfileId,officeAssistantRoleId);
        return officeAssistantUser;
    }
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Exemption Staff.
    // @return : return user record
    public static User getExemptionStaffUser() {
        Id ExemptionStaffProfileId = [Select Id from Profile where Name =:CCSFConstants.EXEMPTIONSTAFF_PROFILE].Id;
        Id ExemptionStaffRoleId = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName ='ExemptionsStaff'].Id;
        User exemptionStaffUser = buildUser('exemptionStaffUser', 'testExemptionStaffUser@asrtest.com', 'esuser', ExemptionStaffProfileId,ExemptionStaffRoleId);
        return exemptionStaffUser;
    }

   // build PortalOwner
    public static User buildPortalOwner(){
        Id portalRoleId = [Select Id From UserRole Where PortalType = 'None' Limit 1].Id;
        Id systemAdminProfileId = [Select Id from Profile where Name='System Administrator'].Id;
        User portalOwner = buildUser('portalOwner', 'portalOwner@asrtest.com', 'pown', systemAdminProfileId,portalRoleId);
        return portalOwner;

    }
    // build PortalUser - Taxpayer
    public static User getPortalUser(){

        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User taxpayer;
        Profile adminProfile = Profiles.ADMIN;
        User portalAccountOwner = new User(
            UserRoleId = portalRole.Id,
            ProfileId = adminProfile.Id,
            Username = System.now().millisecond() + 'asr@test.com',
            Alias = 'asrB',
            Email='bruce.wayne@ASRwayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='ASRBruce',
            Lastname='ASRWayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner);

        System.runAs ( portalAccountOwner ) {
            //Create account
            Account personalAccount = buildPersonAccount('ASRTom','Cartoon','carton@fake.com','94103');
            insert personalAccount;
            Account portalAccount=[Select PersonContactId from Account where Id=:personalAccount.Id];

            //Create user
            Profile portalProfile = Profiles.TAXPAYER;
            taxpayer = new User(
                Username = System.now().millisecond() + 'taxpayerASR@test.com',
                ContactId =  portalAccount.PersonContactId,
                ProfileId = portalProfile.Id,
                Alias = 'taxP',
                Email = 'taxpayerASR@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTestyPayer',
                CommunityNickname = 'McTestyPayerNine',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(taxpayer);
        }
        return taxpayer;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Buisness Account object.
    // @return : return account record
    public static Account getBusinessAccount() {
        Id businessAccountRecordTypeId = DescribeUtility.getRecordTypeId(Account.sObjectType, CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME);
        Account businessAccount = buildAccount('Test Business Acc', CCSFConstants.NOTICE_TYPE_NOTICE_TO_FILE, businessAccountRecordTypeId);
        return businessAccount;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Buisness Account object.
    // @return : return account record
    public static Account getBusinessAccount(String name,String noticeType) {
        Id businessAccountRecordTypeId = DescribeUtility.getRecordTypeId(Account.sObjectType, CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME);
        Account businessAccount = buildAccount(name,noticeType,businessAccountRecordTypeId);
        return businessAccount;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Buisness Property object.
    // @return : return property record
    public static Property__c getBPPProperty() {
        Id businessPropertyRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.BPP_RECORD_TYPE_API_NAME);
        Property__c businessProperty = buildProperty('Test Business Property',businessPropertyRecordTypeId);
        return businessProperty;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Vessel Property object.
    // @return : return property record
    public static Property__c getVesselProperty() {
        Id vesselPropertyRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, CCSFConstants.PROPERTY.VESSEL_RECORD_TYPE_API_NAME);
        Property__c vesselProperty = buildProperty('Test Vessel Property',vesselPropertyRecordTypeId);
        return vesselProperty;
    }

    // build schemaSets (TH1__Schema_Set__c) for Document Generation requests/
    public static List<TH1__Schema_Set__c> buildSchemaSets(integer numberOfRecords,Boolean doInsert) {

        List<TH1__Schema_Set__c> schemaSets = new List<TH1__Schema_Set__c>();
        TH1__Schema_Set__c schemaSet1 = new TH1__Schema_Set__c();
        schemaSets.add(schemaSet1);
        if(doInsert){
        	INSERT schemaSets;
        }
        return schemaSets;
    }

    // build schemaobject (TH1__Schema_Object__c) for Document Generation requests/
    public static List<TH1__Schema_Object__c> buildSchemaObject(integer numberOfRecords,Boolean doInsert, String objectLabel,String schemaSetId) {

       	List<TH1__Schema_Object__c> schemaObjects = new List<TH1__Schema_Object__c>();
        if(String.isNotBlank(objectLabel) && String.isNotBlank(schemaSetId)){
            for(integer i=0;i< numberOfRecords ; i++) {
                TH1__Schema_Object__c schemaObject1 = new TH1__Schema_Object__c();
                schemaObject1.TH1__Is_Primary_Object__c = true;
                schemaObject1.TH1__Object_Label__c = objectLabel;
                schemaObject1.TH1__Schema_Set__c = schemaSetId;
                schemaObject1.Name = objectLabel;
                schemaObjects.add(schemaObject1);
            }
        }

        if(doInsert){
        	INSERT schemaObjects;
        }
        return schemaObjects;
    }

    // Create Document Settings for Properties
    public static List<TH1__Document_Setting__c> buildDocumentSettingsForProperties(String schemaSetId) {

        List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        TH1__Document_Setting__c documentSettingNoticeToFile = new TH1__Document_Setting__c();
        documentSettingNoticeToFile = buildDocumentSetting(schemaSetId,Label.PropertyNoticeOfRequirementToFile576D);
        documentSettings.add(documentSettingNoticeToFile);

        TH1__Document_Setting__c documentSettingNoticeToFileEmail = new TH1__Document_Setting__c();
        documentSettingNoticeToFileEmail = buildDocumentSetting(schemaSetId,Label.PropertyNoticeOfRequirementToFile576DEmail);
        documentSettings.add(documentSettingNoticeToFileEmail);

        TH1__Document_Setting__c documentSettingLowValue = new TH1__Document_Setting__c();
        documentSettingLowValue = buildDocumentSetting(schemaSetId,Label.PropertyLowValueNotice576D);
        documentSettings.add(documentSettingLowValue);

        TH1__Document_Setting__c documentSettingLowValueEmail = new TH1__Document_Setting__c();
        documentSettingLowValueEmail = buildDocumentSetting(schemaSetId,Label.PropertyLowValueNotice576DEmail);
        documentSettings.add(documentSettingLowValueEmail);

        TH1__Document_Setting__c documentSettingDirectBill = new TH1__Document_Setting__c();
        documentSettingDirectBill = buildDocumentSetting(schemaSetId,Label.PropertyDirectBillNotice576D);
        documentSettings.add(documentSettingDirectBill);

        TH1__Document_Setting__c documentSettingDirectBillEmail = new TH1__Document_Setting__c();
        documentSettingDirectBillEmail = buildDocumentSetting(schemaSetId,Label.PropertyDirectBillNotice576DEmail);
        documentSettings.add(documentSettingDirectBillEmail);

        INSERT documentSettings;
        return documentSettings;
    }

    //Build Document Settings Object Method
    public static TH1__Document_Setting__c buildDocumentSetting(String schemaSetId, String name) {
        return new TH1__Document_Setting__c(
            TH1__Document_Data_Model__c = schemaSetId,
            Name = name,
            TH1__Storage_File_Name__c = name,
            TH1__Is_disabled__c = false,
            TH1__Generate_document__c = true,
            TH1__Filter_field_name__c = 'PrimaryFormTypeText__c',
            TH1__Filter_field_value__c = '576-D'
        );
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Buisness Asset Mapping test Data.
    // @return : return account record
    public static BusinessAssetMapping__c getBusinessAssetMapping(String classification,String subClassification, String factorSource,Decimal fixturesPercentage, String factorType) {

        return new  BusinessAssetMapping__c(
            AssetClassification__c = classification,
            AssetSubclassification__c = subClassification,
            FactorSource__c = factorSource,
            FixturesAllocationPercent__c = fixturesPercentage,
            FactorType__c = factorType
        );

    }
     //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Buisness Asset Mapping test Data.
    // @return : return account record
    public static factor__c getfactor(String acquisitionyear,String assessmentyear, Id factorSource,Decimal factorPercentage, Integer usefulLife) {

        return new  factor__c(
            name = 'Commercial',
            FactorSource__c = factorSource,
            AssessmentYear__c = assessmentyear,
            AcquisitionYear__c = acquisitionyear,
            FactorPercent__c = factorPercentage,
            YearsOfUsefulLife__c = usefulLife
        );

    }

    // Build Customer Care Case Object Method
    public static Case buildCustomerCase(Id accountId, Id propertyId, Id recordTypeId){
        return new Case(
            AccountId=accountId,
            Property__c=propertyId,
            RecordTypeId= recordTypeId,
            Origin ='Web'
            );
    }

    // Build PropertyEvent Statement Methos
    public static Statement__c buildExemptionStatement(Id propertyId,Id recordTypeId,String formType){
        return new Statement__C(
            Property__c = propertyId,
            RecordTypeId= recordTypeId,
            FileDate__c= System.today(),
            Form__c= formType
        );
    }

    // get Location Address for Property
    public Static Property__c getLocationAddress(Property__c property){
        // Location Address
        property.LocationStreetName__c = 'Market';
        property.LocationStreetNumber__c ='1155';
        property.LocationCareOf__c ='Mr Jack';
        property.LocationCity__c='San Francisco';
        property.LocationStreetType__c='ST';
        property.LocationCountry__c ='US';
        property.LocationState__c = 'US-CA';
        property.LocationPostalCode__c='94104';



        return property;
    }

    //This method is created to disable trigger,dlrs and validations rule to create data in tets setup in test class
    public static void disableAutomationCustomSettings(Boolean disable) {
        TriggerHandlerSettings__c triggerHandlerSettings = TriggerHandlerSettings__c.getInstance(UserInfo.getUserId());
        triggerHandlerSettings.DisableAllTriggers__c             = disable;
        triggerHandlerSettings.DisableDLRSCalculationTriggers__c = disable;
        upsert triggerHandlerSettings;

        ProcessBuilderSettings__c processBuilderSettings = ProcessBuilderSettings__c.getInstance(UserInfo.getUserId());
        processBuilderSettings.DisableAllProcesses__c = disable;
        upsert processBuilderSettings;

        ValidationRuleSettings__c validationRuleSettings = ValidationRuleSettings__c.getInstance(UserInfo.getUserId());
        validationRuleSettings.DisableValidationRules__c = disable;
        upsert validationRuleSettings;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : This method is used to create Ownership record.          
    // @return : return account record
    public static Ownership__c getOwnerShip(String accId,Date startDate) {
        return new Ownership__c(
            StartDate__c = startDate != null ? startDate : system.today(),
            Account__c = accId
        );
    }
}