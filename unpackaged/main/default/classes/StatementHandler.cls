/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public without sharing class StatementHandler extends TriggerHandler {

    protected override void executeBeforeInsert(List<SObject> newRecords) {
        List<Statement__c> newStatements = (List<Statement__c>)newRecords;

        this.setAddressFields(newStatements);
        this.setAssessmentYear(newStatements);
        this.setStatementOwner(newStatements); // ASR-10268
    }

    protected override void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {
        List<Statement__c> updatedStatements = (List<Statement__c>)updatedRecords;
        Map<Id,Statement__c> oldStatementsByIds = (Map<Id,Statement__c> )oldRecordsById;
        List<Statement__c> statementsTobeProcessed = new List<Statement__c>(); 
        for(Statement__c statement : updatedStatements) {
            // Bug- 10428
            // Addedd Check, it should allow Statement to Update if its only updating Assessment Field on it
            if((statement.Status__c == CCSFConstants.STATEMENT.STATUS_SUBMITTED || statement.Status__c == CCSFConstants.STATEMENT.STATUS_PROCESSED ) && statement.Status__c == oldStatementsByIds.get(statement.Id).Status__c && statement.FilingMethod__c == CCSFConstants.STATEMENT.FILING_METHOD && statement.Assessment__c == oldStatementsByIds.get(statement.Id).Assessment__c)  { 
                Logger.addDebugEntry(LoggingLevel.ERROR, CCSFConstants.STATEMENT.SUBMITTED_STATEMENT_ERROR, 'StatementHandler', null);
                statement.addError(CCSFConstants.STATEMENT.SUBMITTED_STATEMENT_ERROR);  
            }else {
                statementsTobeProcessed.add(statement);
            }
        }

        this.setAddressFields(statementsTobeProcessed);
        this.setAssessmentYear(statementsTobeProcessed);
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Statement__c>
    // @description : This method sets address fields based on formula fields. This allows field history tracking on the address fields.
    // @return : void
    //-----------------------------
    private void setAddressFields(List<Statement__c> statements) {
        for(Statement__c statement : statements) {
            statement.AccountingAddress__c = String.isBlank(statement.CalculatedAccountingAddress__c) ? null : statement.CalculatedAccountingAddress__c.replace('_BR_ENCODED_', '\n');
            statement.LocationAddress__c   = String.isBlank(statement.CalculatedLocationAddress__c) ? null : statement.CalculatedLocationAddress__c.replace('_BR_ENCODED_', '\n');
            statement.MailingAddress__c    = String.isBlank(statement.CalculatedMailingAddress__c) ? null : statement.CalculatedMailingAddress__c.replace('_BR_ENCODED_', '\n');
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Statement__c>
    // @description : This method, sets the Assesment year from FileDate__c on statement
    // @return : void
    //-----------------------------
    private void setAssessmentYear(List<Statement__c> statements) {
        for(Statement__c statement : statements) {
            if(statement.FileDate__c == null) {continue;}
            statement.AssessmentYear__c = String.valueOf(FiscalYearUtility.getFiscalYear(statement.FileDate__c));                  
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Statement__c>
    // @description : ASR-10268 This method assigns all statements to a Queue - 'Assignment_Queue'. Statements will 
    //                rerouted via Omnichannel later on.
    // @return : void
    //-----------------------------
    private void setStatementOwner(List<Statement__c> statements) {
        // Fetch the Queue, statement(s) will be assigned to.
        List<Group> stmntQueueList = [Select Id from Group 
                                Where type = :CCSFConstants.typeQueue and 
                                DeveloperName = :CCSFConstants.statementQueueDevName];
        if (stmntQueueList.size() > 0){
            for(Statement__c statement : statements) {
                statement.OwnerId = stmntQueueList[0].Id;          
            }
        }
    }
}