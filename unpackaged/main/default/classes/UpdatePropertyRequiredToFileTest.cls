/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This is a test class for UpdatePropertyRequiredToFile Class
@isTest
public class UpdatePropertyRequiredToFileTest {
    private static Integer currentYear      = FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear         = currentYear-1;
    private static Integer lastToLastYear   = lastYear-1;
    private static Integer nextYear         = currentYear+1;
    private Id assessmentLegacyRecordTypeId;
    private Id assessmentBPPRecordTypeId;
    private static Id propertyBPPRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, 'BusinessPersonalProperty');
    private static Id propertyLeasedRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, 'LeasedEquipment');   
    private static Id propertyVesselRecordTypeId = DescribeUtility.getRecordTypeId(Property__c.sObjectType, 'Vessel');
    
    
    @testSetup
    static void dataSetup() {
        
        //Create Users
        List<User> users = new List<User>();
        User principal = TestDataUtility.getPrincipalUser();
        users.add(principal);
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin);  
        User auditor = TestDataUtility.getBPPAuditorUser();
        users.add(auditor); 
        insert users;
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        System.runAs(systemAdminUser) {
            
            //insert roll years
            List<RollYear__c> rollYears = new List<RollYear__c>();
            
            RollYear__c rollYearCurrentFiscalYear = TestDataUtility.buildRollYear(String.valueof(currentYear),String.valueof(currentYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYearCurrentFiscalYear.IsLowValueBulkGeneratorProcessed__c = true;
            rollYearCurrentFiscalYear.IsDirectBillBulkGeneratorProcessed__c = true;            
            rollYears.add(rollYearCurrentFiscalYear);
            
            RollYear__c rollYearLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastYear),String.valueof(lastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_CLOSE_STATUS);
            rollYears.add(rollYearLastFiscalYear);
            
            RollYear__c rollYearLastToLastFiscalYear = TestDataUtility.buildRollYear(String.valueof(lastToLastYear),String.valueof(lastToLastYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearLastToLastFiscalYear);
            
            RollYear__c rollYearNextFiscalYear = TestDataUtility.buildRollYear(String.valueof(nextYear),String.valueof(nextYear),CCSFConstants.ROLLYEAR.ROLL_YEAR_OPEN_STATUS);
            rollYears.add(rollYearNextFiscalYear);
            
            insert rollYears;
            
            //insert penalty
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            //insert accounts
            List<Account> businessAccounts = new List<Account>();
            Account businessAccount1 = TestDataUtility.getBusinessAccount('Test Business Account 1','Low Value');
            businessAccount1.TotalValueofProperties__c = 2000;
            businessAccounts.add(businessAccount1);
            Account businessAccount2 = TestDataUtility.getBusinessAccount('Test Business Account 2','Direct Bill');            
            businessAccounts.add(businessAccount2);
            businessAccount2.TotalCostofProperties__c = 50000;
            insert businessAccounts;
            
            //insert property
            List<Property__c> properties = new List<Property__c>();        
            Property__c property1 = new Property__C();
            property1.Name = 'test property1';
            property1.RollCode__c = 'unsecured';
            property1.MostRecentAssessedValue__c = 2000;
            property1.PrimaryFormType__c = '571-L';
            property1.AssessedCost__c = 20000;
            property1.Account__c = businessAccount1.Id;
            //property1.LastFilingMethod__c = 'eFile';
            property1.RecordTypeId = propertyBPPRecordTypeId;
            properties.add(property1);
            
            Property__c property2 = new Property__C();
            property2.Name = 'test property2';
            property2.MostRecentAssessedValue__c = 50000;
            property2.AssessedCost__c = 20000;
            property2.PrimaryFormType__c = '571-L';
            property2.Account__c = businessAccount2.Id;
            //property2.LastFilingMethod__c = 'eFile';
            property2.RecordTypeId = propertyBPPRecordTypeId;
            properties.add(property2);
            
            Property__c property3 = new Property__C();
            property3.Name = 'test property3';
            property3.MostRecentAssessedValue__c = 50000;
            property3.PrimaryFormType__c = '571-L';
            property3.AssessedCost__c = 20000;
            property3.Account__c = businessAccount2.Id;
            //property2.LastFilingMethod__c = 'eFile';
            property3.RecordTypeId = propertyLeasedRecordTypeId;
            properties.add(property3);
            
            insert properties;
            
            Property__c vesselProperty = TestDataUtility.getVesselProperty();
            vesselProperty.Account__c = businessAccount1.Id;
            //vesselProperty.LastFilingMethod__c = 'eFile';
            insert vesselProperty;
            
            List<Case> casestests = new List<Case>();
            Case cas = new Case();
            cas.Property__C = property1.id;
            cas.MailingCountry__c = 'US';
            cas.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            cas.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            cas.RollYear__c=String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
            cas.Type='Regular';
            cas.IntegrationStatus__c = 'Ready to send to TTX';
            cas.AdjustmentType__c = 'New';
            casestests.add(cas);
            
            Case cas1 = new Case();
            cas1.Property__C = property2.id;
            cas1.MailingCountry__c = 'US';
            cas1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
            cas1.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            cas1.RollYear__c= String.valueOf(lastYear);
            cas1.Type='Regular';
            cas1.AdjustmentType__c = 'New';
            casestests.add(cas1);
            
            Case cas2 = new Case();
            cas2.Property__C = vesselProperty.id;
            cas2.MailingCountry__c = 'US';
            cas2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('VesselAssessment').getRecordTypeId();
            cas2.EventDate__c = Date.newInstance(FiscalYearUtility.getFiscalYear(System.today()), 1, 1);
            cas2.RollYear__c=String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
            cas2.Type='Regular';
            cas2.SubType__c= 'Non-Assessable (Moved Within SF)';
            cas2.IntegrationStatus__c = 'Ready to send to TTX';
            cas2.AdjustmentType__c = 'New';
            casestests.add(cas2);
            
            insert casestests;        
        }
        
    }
    
    @isTest
    static void updateBPPAndVesselPropetiesDefaultRequiredToFile() {
        Test.startTest();
        Property__c property = [select Id,LastFilingMethod__c from Property__c 
                                where RecordTypeId =:propertyBPPRecordTypeId limit 1] ;
        property.LastFilingMethod__c = 'eFile';
        update property;
        
        Database.executeBatch(new UpdatePropertyRequiredToFile());
        Test.stopTest();
        Property__c updatedProperty = [select Id,RequiredToFile__c,Account__r.id from Property__c 
                                       where RecordTypeId =:Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId() limit 1] ;
        System.assertEquals(CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES, updatedProperty.RequiredToFile__c); 
         Account accRecord = [Select id,NoticeType__c From Account Where id =: updatedProperty.Account__r.id];
        System.assertEquals('Notice to File', accRecord.NoticeType__c); 

    }
    
    @isTest
    static void updateLeasedPropetiesLessorDefaultRequiredToFile() {
        Test.startTest();
        Property__c property = [select Id,LastFilingMethod__c from Property__c 
                                where RecordTypeId =:propertyLeasedRecordTypeId limit 1] ;
        property.LastFilingMethod__c = 'eFile';
        property.Type__c = CCSFConstants.PROPERTY.PROPERTY_TYPE_LESSOR;
        update property;
        
        Database.executeBatch(new UpdatePropertyRequiredToFile());
        Test.stopTest();       
        Property__c updatedProperty = [select Id,RequiredToFile__c,Account__r.id from Property__c 
                                       where RecordTypeId =:propertyLeasedRecordTypeId limit 1] ;
        System.assertEquals(CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES, updatedProperty.RequiredToFile__c); 
        Account accRecord = [Select id,NoticeType__c From Account Where id =: updatedProperty.Account__r.id];
        System.assertEquals('Direct Bill', accRecord.NoticeType__c); 

    }
    
    @isTest
    static void updateLeasedPropetiesLesseeDefaultRequiredToFile() {
        Test.startTest();
        Property__c property = [select Id,LastFilingMethod__c from Property__c 
                                where RecordTypeId =:Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('LeasedEquipment').getRecordTypeId() limit 1] ;
        property.LastFilingMethod__c = null;
        property.Type__c = CCSFConstants.PROPERTY.PROPERTY_TYPE_LESSEE;
        update property;
        Database.executeBatch(new UpdatePropertyRequiredToFile());
        
        Test.stopTest();
        Property__c updatedProperty = [select Id,RequiredToFile__c,Account__r.id from Property__c 
                                       where RecordTypeId =:Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('LeasedEquipment').getRecordTypeId() limit 1] ;
        System.assertEquals(CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO, updatedProperty.RequiredToFile__c); 
        Account accRecord = [Select id,NoticeType__c From Account Where id =: updatedProperty.Account__r.id];
        System.assertEquals('Direct Bill', accRecord.NoticeType__c); 

    }

    @isTest
    static void updateInactiveBeforeLienDateBPPAndLeasedPropertiesRequiredToFile() {
        Test.startTest();
        Property__c property = [select Id,Status__c,BusinessLocationCloseDate__c from Property__c 
                                where RecordTypeId =:propertyBPPRecordTypeId limit 1] ;
        property.Status__c = 'Inactive';
        property.BusinessLocationCloseDate__c = Date.parse('12/31/'+lastYear);
        update property;
        
        Database.executeBatch(new UpdatePropertyRequiredToFile());
        Test.stopTest();
        Property__c updatedProperty = [select Id,RequiredToFile__c,Account__r.id from Property__c 
                                       where RecordTypeId =:Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId() limit 1] ;
        System.assertEquals(CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO, updatedProperty.RequiredToFile__c); 
        Account accRecord = [Select id,NoticeType__c From Account Where id =: updatedProperty.Account__r.id];
        System.assertEquals('Notice to File', accRecord.NoticeType__c); 

    }

    @isTest
    static void updateInactiveAfterLienDateBPPAndLeasedPropertiesRequiredToFile() {
        Test.startTest();
        Property__c property = [select Id,Status__c,BusinessLocationCloseDate__c from Property__c 
                                where RecordTypeId =:propertyBPPRecordTypeId limit 1] ;
        property.Status__c = 'Inactive';
        property.BusinessLocationCloseDate__c = Date.parse('01/02/'+currentYear);
        update property;
        
        Database.executeBatch(new UpdatePropertyRequiredToFile());
        Test.stopTest();
        Property__c updatedProperty = [select Id,RequiredToFile__c,Account__r.id from Property__c 
                                       where RecordTypeId =:Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId() limit 1] ;
        System.assertEquals(CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES, updatedProperty.RequiredToFile__c); 
        Account accRecord = [Select id,NoticeType__c From Account Where id =: updatedProperty.Account__r.id];
        System.assertEquals('Notice to File', accRecord.NoticeType__c); 

    }

    @isTest
    static void updateActiveAfterLienDateBPPAndLeasedPropertiesRequiredToFile() {
        Test.startTest();
        Property__c property = [select Id,Status__c,BusinessLocationOpenDate__c from Property__c 
                                where RecordTypeId =:propertyBPPRecordTypeId limit 1] ;
        property.Status__c = 'Active';
        property.BusinessLocationOpenDate__c = Date.parse('01/02/'+currentYear);
        update property;
        
        Database.executeBatch(new UpdatePropertyRequiredToFile());
        Test.stopTest();
        Property__c updatedProperty = [select Id,RequiredToFile__c,Account__r.id from Property__c 
                                       where RecordTypeId =:Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId() limit 1] ;
        System.assertEquals(CCSFConstants.PROPERTY.REQUIRED_TO_FILE_NO, updatedProperty.RequiredToFile__c); 
        Account accRecord = [Select id,NoticeType__c From Account Where id =: updatedProperty.Account__r.id];
        System.assertEquals('Notice to File', accRecord.NoticeType__c); 

    }

    @isTest
    static void updateActiveBeforeLienDateBPPAndLeasedPropertiesRequiredToFile() {
        Test.startTest();
        Property__c property = [select Id,Status__c,BusinessLocationOpenDate__c from Property__c 
                                where RecordTypeId =:propertyBPPRecordTypeId limit 1] ;
        property.Status__c = 'Active';
        property.BusinessLocationOpenDate__c = Date.parse('12/31/'+lastYear);
        update property;
        
        Database.executeBatch(new UpdatePropertyRequiredToFile());
        Test.stopTest();
        Property__c updatedProperty = [select Id,RequiredToFile__c,Account__r.id from Property__c 
                                       where RecordTypeId =:Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('BusinessPersonalProperty').getRecordTypeId() limit 1] ;
        Account accRecord = [Select id,NoticeType__c From Account Where id =: updatedProperty.Account__r.id];
        System.assertEquals(CCSFConstants.PROPERTY.REQUIRED_TO_FILE_YES, updatedProperty.RequiredToFile__c);
        System.assertEquals('Notice to File', accRecord.NoticeType__c); 
    }
    
    
    //@isTest
    static void SetNoticeTypeForAllKindsOfAccount() {
        Test.startTest();
        List<Account> lst = [SELECT Id, TotalValueofProperties__c, TotalCostofProperties__c 
                             FROM Account
                             WHERE BusinessStatus__c = 'Active' and RecordTypeId =: DescribeUtility.getRecordTypeId(Account.SobjectType,CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME)
                             order by lastmodifieddate desc];
       
        
        Database.executeBatch(new UpdatePropertyRequiredToFile());
        Test.stopTest();
    }
}