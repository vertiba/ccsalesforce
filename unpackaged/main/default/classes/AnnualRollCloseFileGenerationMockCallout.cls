/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/*********************** 
 * @Author: Publicis Sapient
 * @Apex TestClassName: AnnualRollCloseFileGenerationMockCallout 
 * @Date : 15th June 2020 
 * 
 **********************/
@isTest
global with sharing class AnnualRollCloseFileGenerationMockCallout implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest request) {
        String endUrl = [Select Endpoint from NamedCredential where developerName ='SMART_Annual_Roll_Close_File_Generation_Service' limit 1].Endpoint;

        HttpResponse response = new HttpResponse();
        String currentRollYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());

        if(request.getEndpoint().contains(endUrl))
        {
            System.assertEquals('POST', request.getMethod());
            response.setHeader('Content-Type', 'application/json; charset=utf-8');
            response.setStatusCode(202);
        }
        return response;
    }
}