/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//            Created to post Chatter Notification to the failed record.
// 

public with sharing class ErrorChatterPostNotifications {
    
    // @author : Publicis.Sapient
    // @param  : dmlResults :  Datebase.saveresults of the DML ,
    // @param  : dmlRecords :  Records on Which DML has been done
    // @description :This method is used to Post the Chatter Notification on the record when DML is failed on it. 
    // @return : void

    public static void createFailedDMLChatterNotification(List<Database.SaveResult> dmlResults, List<Sobject> dmlRecords){ 

	
        if(dmlRecords == null || dmlRecords.isEmpty() || dmlResults == null || dmlResults.isEmpty() ||  !ChatterNotifications__c.getOrgDefaults().EnableChatterNotitfication__c) {return;}
        
        List<ChatterUtility.ChatterPost> chatterPosts = new List<ChatterUtility.ChatterPost>();
        for(Integer i=0;i<dmlResults.size();i++){

            if (!dmlResults.get(i).isSuccess()){
                // DML operation failed
                Database.Error error = dmlResults.get(i).getErrors().get(0);
                String failedDML = error.getMessage();
              
                Logger.addRecordDebugEntry(LoggingLevel.Error, dmlRecords.get(i),failedDML,'ErrorChatterPostNotifications - execute Method');
        
                // Createing A chatter post each failure.
                ChatterUtility.ChatterPost chatterPost = new ChatterUtility.ChatterPost();
                chatterPost.message = 'Error while updating the record. Please contact your System Admin to check the Exception Logs';
                chatterPost.subject = dmlRecords.get(i).Id;
                chatterPost.userId  = Userinfo.getUserId();
                chatterPosts.add(chatterPost);
               
             }
        }
        Logger.saveLog();
        ChatterUtility.post(chatterPosts);
       
    }
}