/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : ASR-5669. This Batch class is used to Update assessment Integration staus based on various criteria
//-----------------------------
public class UpdateCaseIntegrationStatusBatch implements Database.Batchable<sObject>,Database.Stateful, BatchableErrorHandler {
    
    private String originLocation;
    private String currentYear;
    private String previousYear;
    private Date eligibleNoticeSentDate;
    private List<Case> casesToSendIntegrationLayer;
    
    //Constructor to set values
    public UpdateCaseIntegrationStatusBatch() {
        originLocation = CCSFConstants.UPDATE_CASE_INTEGRATIONSTATUS_BATCH_NAME;
        currentYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        previousYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        eligibleNoticeSentDate = system.today()-Integer.valueOf(Label.RequiredPostNoticeDays);
        casesToSendIntegrationLayer = new List<Case>();
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : This method gets all Assessment (Case) records which follow the below criteria.
    // @return : Database.QueryLocator (List of Cases)
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        //Build dynamic query for batch
        String query='SELECT id, Type, SubType__c, VaultDoorNightDepositoryValueDiff__c, TotalToolsMoldsDiesJigsValueDiff__c, TotalMachineryEquipmentValueDiff__c, ';
        query += ' TotalSuppliesValueDifference__c, TotalSignsCameraTVetcValueDiff__c, TotalPersonalComputerValueDifference__c, TotalLANMainframesValueDifference__c, '; 
        query += ' OtherFurnitureEquipmentValueDiff__c, TotalOtherEquipmentValueDifference__c, OfficeFurnitureEquipmentValueDiff__c, LeaseholdImprovementsStrValueDiff__c,'; 
        query += ' LeaseholdImprovementsFixValueDiff__c, TotalLandImprovementsValueDifference__c, TotalLandDevelopmentValueDiff__c, TotalFurnitureAppliancesValueDiff__c,';
        query += ' TotalDriveupWalkupKioskValueDiff__c, TotalCarpetsDrapesValueDifference__c, TotalCounterlinesPartitionsValueDiff__c, TotalATMsValueDifference__c,TotalConstructioninProgressDiff__c,';
        query += ' AdjustmentType__c, Status, SubStatus__c, WaiveNoticePeriod__c, Billable__c, RollCode__c, IntegrationStatus__c, NoticeSentDate__c, TotalAssessedValue__c ';
        query += ' from Case where  isAssessment__c = true';
        query += ' AND Status = \''+CCSFConstants.ASSESSMENT.SUBSTATUS_CLOSED+'\'';
        query += ' AND (Type = \''+CCSFConstants.ASSESSMENT.TYPE_REGULAR+'\'';
        query += ' OR Type = \''+CCSFConstants.ASSESSMENT.TYPE_ESCAPE+'\')';
        query += ' AND (SubStatus__c = \''+CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED+'\'';
        query += ' OR SubStatus__c = \''+CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING+'\')';
        query += ' AND (IntegrationStatus__c = \'\'';
        query += ' OR IntegrationStatus__c = null)';
        query += ' AND (RollYear__c = :currentYear or RollYear__c = :previousYear)';
        Logger.addDebugEntry(query,originLocation+'- start method query');
        Logger.saveLog();
        return database.getQueryLocator(query);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @param : CasesList (List of Cases)
    // @description : This method update fields for all the Assessment (Case) records which
    //          are returned by start() method of the Batch class 
    // @return : void
    public void execute(Database.BatchableContext bc, List<Case> cases) {
        
        try {
            Map<Id,Case> casesToUpdateById = new Map<Id,Case>();
            for(Case inputCase : cases){
                // If roll code is secured; IntegrationStatus is READY_TO_SEND_TO_RP; else READY_TO_SEND_TO_TTX
                // If Billable is No, IntegrationStatus is INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND
                String IntegrationStatus;
                // ASR-10634 - If Billable is marked as No, status is Not Eligible to Send
                if(inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO){
                    IntegrationStatus = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                }else{
                    IntegrationStatus = inputCase.RollCode__c == CCSFConstants.ASSESSMENT.ROLLCODE_SECURED?
                        										 CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_RP : 
                    											 CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_TTX;
                }
                
                // If subtype of Case is anything below, IntegrationStatus is not elible to send
                if(inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_MOVED_WITHIN_SF || 
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_CLOSED ||
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_SOLD || 
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_MOVED_OUTSIDE_SF ||
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_REFERENCE_PROPERTY || 
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_GOVERNMENT_ENTITY ||
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_FEDERAL_ENCLAVE_PROPERTY || 
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NON_ASSESSABLE_STATE_ASSESSED_PROPERTY ||
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NOTHING_TO_ASSESS || 
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_NOTHING_TO_ASSESS_VESSEL_INOPERABLE ||
                   inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_LOWVALUE) {
                       inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                       inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                       casesToUpdateById.put(inputCase.id,inputCase);
                       continue;
                   }
                
                // If Canceled, update integration status based on roll code and mark case as complete 
                if(inputCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.ADJUSTMENT_CANCEL){
                    inputCase.IntegrationStatus__c = IntegrationStatus;
                    inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                    casesToUpdateById.put(inputCase.id,inputCase);
                    continue;
                }
                
                // Handling Regular Cases
                if(inputCase.Type == CCSFConstants.ASSESSMENT.TYPE_REGULAR){
                    if(inputCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW && 
                       inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED)
                    {
						if(inputCase.SubType__c == '' || inputCase.SubType__c == null){
                            inputCase.IntegrationStatus__c = IntegrationStatus;
                        }else if(inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT) {
                            // ASR-10145/ASR-3505 for the closeout case we have to check for the Total Assessed Value
                            if(inputCase.TotalAssessedValue__c <= Decimal.valueOf(Label.LowValueAmount)){
                                inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                            } else {
                                inputCase.IntegrationStatus__c = IntegrationStatus;
                            }
                        }
                        
                        casesToUpdateById.put(inputCase.id,inputCase);
                    }
                    else if((inputCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_ROLLCORRECTION) && 
                            (inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING || 
                             inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED))
                    {
                        //ASR-10629: If any embedded Escapes present on Case, keep sub-status as Noticing
                        boolean embeddedEscape = checkEmbeddedEscape(inputCase);
                        
                        if(embeddedEscape == false){
                            inputCase.IntegrationStatus__c = IntegrationStatus;                            
                        }else{
                            if(inputCase.WaiveNoticePeriod__c == true && (inputCase.SubType__c == '' || inputCase.SubType__c == null)){
                                inputCase.IntegrationStatus__c = IntegrationStatus;
                                inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                            }
                            else if(inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING &&
                                    inputCase.WaiveNoticePeriod__c == false && inputCase.NoticeSentDate__c !=null &&
                                    (inputCase.SubType__c == '' || inputCase.SubType__c == null))
                            {
                                if(inputCase.NoticeSentDate__c < eligibleNoticeSentDate){
                                    inputCase.IntegrationStatus__c = IntegrationStatus;
                                    inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                                }
                            }else{
                                inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
                            } 
                        }
                        
                        casesToUpdateById.put(inputCase.id,inputCase);
                    }
                    
                    // End of Regular Cases
                }
                // Handling Escape Cases
                else if(inputCase.Type == CCSFConstants.ASSESSMENT.TYPE_ESCAPE){
                    
                    // New Cases with SubStatus as Noticing/Completed and SubType as Closeout
                    if(inputCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.STATUS_NEW &&
                       (inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED || 
                        inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING) && 
                       inputCase.SubType__c == CCSFConstants.ASSESSMENT.SUB_TYPE_CLOSEOUT)
                    {
                        // If Waive Notice Period false, billable yes, check if 15 days already passed after notice is sent to taxpayer, 
                        // change status based on Roll Code
                        if(inputCase.WaiveNoticePeriod__c == false && inputCase.NoticeSentDate__c !=null &&
                           inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_YES)
                        {
                            if(inputCase.NoticeSentDate__c < eligibleNoticeSentDate){
                                inputCase.IntegrationStatus__c = IntegrationStatus;
                                inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                                casesToUpdateById.put(inputCase.id,inputCase);
                            }
                            
                        } 
                        // If Waive Notice Period false, billable No, check if 15 days already passed after notice is sent to taxpayer, 
                        // change status to Not Eligible to Send
                        else if(inputCase.WaiveNoticePeriod__c == false && inputCase.NoticeSentDate__c !=null &&
                                  inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO)
                        {
                            if(inputCase.NoticeSentDate__c < eligibleNoticeSentDate){
                                inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                                inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                                casesToUpdateById.put(inputCase.id,inputCase);
                            }       
                        }
                        // If Waive Notice Period true, billable Yes, Update Integration Status based on Roll Code
                        else if(inputCase.WaiveNoticePeriod__c == true && inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_YES){
                            inputCase.IntegrationStatus__c = IntegrationStatus;
                            inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                            casesToUpdateById.put(inputCase.id,inputCase);
                        
                        }
                        // If Waive Notice Period true, billable No, Update Integration Status as NOT_ELIGIBLE_TO_SEND
                        else if(inputCase.WaiveNoticePeriod__c == true && inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO){
                            inputCase.IntegrationStatus__c=CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                            inputCase.SubStatus__c=CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                            casesToUpdateById.put(inputCase.id,inputCase);
                        }
                        
                        // ASR-10145/ASR-3505 if one of the above condition is executed and change integration status 
                        // as ready to send to RP then check for the Total Assessed Value
                        if(inputCase.IntegrationStatus__c == CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_READY_TO_SEND_TO_RP &&
                           inputCase.TotalAssessedValue__c <= Decimal.valueOf(Label.LowValueAmount)){
                               inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                               casesToUpdateById.put(inputCase.id,inputCase);
                           }
                        
                    }
                    else if(inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED || 
                            inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING)
                    {
                        boolean embeddedEscape = checkEmbeddedEscape(inputCase);
                        // ASR-10629: If any embedded Escapes present on Case, keep sub-status as Noticing
                        if(inputCase.AdjustmentType__c == CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_ROLLCORRECTION){
                            if(embeddedEscape == false){
                                inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                                //ASR-10236 Escape/Roll Correction or Escape/Cancel should immediately be sent to TaxSys
                                inputCase.IntegrationStatus__c = inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_YES ? 
                                    IntegrationStatus: CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                                casesToUpdateById.put(inputCase.id,inputCase);
                                continue;
                            }else if(embeddedEscape == true && inputCase.SubStatus__c != CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING){
                                inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING;
                                casesToUpdateById.put(inputCase.id,inputCase);
                            }
                        } 
                        
                        // If Waive Notice Period true, billable Yes, SubType null, Update Integration Status based on Roll Code
                        if(inputCase.WaiveNoticePeriod__c == true && inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_YES &&
                                  (inputCase.SubType__c == '' || inputCase.SubType__c == null))
                        {
                            inputCase.IntegrationStatus__c = IntegrationStatus;
                            inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                            casesToUpdateById.put(inputCase.id,inputCase);
                        } 
                        // If Waive Notice Period true, billable No, SubType null, Update Integration Status Not eligible to Send
                        else if(inputCase.WaiveNoticePeriod__c == true && inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO &&
                                  (inputCase.SubType__c == '' || inputCase.SubType__c == null))
                        {
                            inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                            inputCase.SubStatus__c=CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                            casesToUpdateById.put(inputCase.id,inputCase);
                        } 
                        // If Waive Notice Period false, SubStatus noticing, SubType null, Billable Yes,
                        // Check if 15 days already passed after notice is sent to taxpayer, Update Integration Status based on roll code
                        else if(inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING &&
                                  inputCase.WaiveNoticePeriod__c == false && inputCase.NoticeSentDate__c !=null &&
                                  inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_YES && 
                                  (inputCase.SubType__c == '' || inputCase.SubType__c == null))
                        {
                            if(inputCase.NoticeSentDate__c < eligibleNoticeSentDate){
                                inputCase.IntegrationStatus__c = IntegrationStatus;
                                inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                                casesToUpdateById.put(inputCase.id,inputCase);
                            }
                        }
                        // If Waive Notice Period false, SubStatus noticing, SubType null, Billable No
                        // Check if 15 days already passed after notice is sent to taxpayer, Update Integration Status Not eligible to send
                        else if(inputCase.SubStatus__c == CCSFConstants.ASSESSMENT.SUBSTATUS_NOTICING &&
                                  inputCase.WaiveNoticePeriod__c == false && inputCase.NoticeSentDate__c !=null &&
                                  inputCase.Billable__c == CCSFConstants.ASSESSMENT.BILLABLE_NO && 
                                  (inputCase.SubType__c == '' || inputCase.SubType__c == null))
                        {
                            if(inputCase.NoticeSentDate__c < eligibleNoticeSentDate){
                                inputCase.IntegrationStatus__c = CCSFConstants.ASSESSMENT.INTEGRATIONSTATUS_NOT_ELIGIBLE_TO_SEND;
                                inputCase.SubStatus__c = CCSFConstants.ASSESSMENT.SUBSTATUS_COMPLETED;
                                casesToUpdateById.put(inputCase.id,inputCase);
                            }
                        }
                        
                    }
                    // End of Escape Case
                }
			}
            
            if(casesToUpdateById == null || casesToUpdateById.isEmpty()) {
                Logger.addDebugEntry('No eligible cases found for updating integration status.',originLocation);
                Logger.saveLog();
                return;
            }
            
            updateAssessments(casesToUpdateById.values());
            Logger.addDebugEntry('Updated eligible cases. Count:'+casesToUpdateById.values().size(),originLocation);
        }catch(Exception ex) {
            Logger.addExceptionEntry(ex, originLocation);
            throw ex;
        }
        Logger.saveLog();
    }
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : Database.BatchableContext
    // @description : ASR-5801. This method sends eligible Roll Correction Cases to Integration Layer
    // @return : void
    public void finish(Database.BatchableContext bc) {
        
        string JobId = bc.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, UpdateCaseIntegrationStatusBatch.class.getName(), 
                                           asyncList[0].ParentJobId, false, '', asyncList[0].NumberOfErrors);
        
        Logger.addDebugEntry('Number of Eligible cases found to be updated for In Transit To TTX: '+casesToSendIntegrationLayer.size(),originLocation);
        if(casesToSendIntegrationLayer == null || casesToSendIntegrationLayer.isEmpty()) return;
        
        ID batchprocessid = Database.executeBatch(new RollCorrectionBatchSync(),Integer.valueOf(Label.RollCorrectionBatchSyncBatchSize));
        Logger.addDebugEntry(batchprocessid, 'UpdateCaseIntegrationStatusBatch sendToIntegrationLayerRollCorrectionCases');
        Logger.saveLog();
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<Case>
    // @description : This method updates assessments and store the successful updated cases for sending to integration layer
    // @return : void
    //-----------------------------
    private void updateAssessments(List<Case> cases) {
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.allowSave = true;
        //update assessments
        Map<String,Object> updateResultMap = DescribeUtility.getUpdateRecordsResults(cases,dmlOptions,originLocation);
        //Storing all the successful updated cases for sending to integration layer
        casesToSendIntegrationLayer.addAll((List<Case>)updateResultMap.get('successRecords'));
    }
    
    /*
* Method Name: checkEmbeddedEscape
* Description: This method checks if the Case has any embedded Escapes on 1 or more of the Asset Classifications.
*/
    private Boolean checkEmbeddedEscape(Case assessment) {
        boolean embeddedEscape = false;
        if(assessment.TotalATMsValueDifference__c < 0 || assessment.TotalConstructioninProgressDiff__c < 0 || 
           assessment.TotalCounterlinesPartitionsValueDiff__c < 0 || assessment.TotalCarpetsDrapesValueDifference__c < 0 ||
           assessment.TotalDriveupWalkupKioskValueDiff__c < 0 || assessment.TotalFurnitureAppliancesValueDiff__c < 0 ||
           assessment.TotalLandDevelopmentValueDiff__c < 0 || assessment.TotalLandImprovementsValueDifference__c < 0 ||
           assessment.LeaseholdImprovementsFixValueDiff__c < 0 || assessment.LeaseholdImprovementsStrValueDiff__c < 0 ||
           assessment.TotalLANMainframesValueDifference__c < 0 || assessment.TotalMachineryEquipmentValueDiff__c < 0 || 
           assessment.OfficeFurnitureEquipmentValueDiff__c < 0 || assessment.TotalOtherEquipmentValueDifference__c < 0 ||
           assessment.OtherFurnitureEquipmentValueDiff__c < 0 ||assessment.TotalPersonalComputerValueDifference__c < 0 || 
           assessment.TotalSignsCameraTVetcValueDiff__c < 0 || assessment.TotalSuppliesValueDifference__c < 0 || 
           assessment.TotalToolsMoldsDiesJigsValueDiff__c < 0 || assessment.VaultDoorNightDepositoryValueDiff__c < 0){
               // There are Embedded Escapes
               embeddedEscape = true;
           }else{
               // No Embedded Escape
               embeddedEscape = false;
           }
        
        return embeddedEscape;
    }
}