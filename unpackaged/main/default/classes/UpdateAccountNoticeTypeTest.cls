/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
//                            Class to test UpdateAccountNoticeType
//-----------------------------
@isTest
public class UpdateAccountNoticeTypeTest {
    
    @testSetup
    private static void dataSetup(){
        insert new LoggerSettings__c();
        String rollYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear());
        String lastYear = String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        Integer prevoiusYear = FiscalYearUtility.getCurrentFiscalYear()-1;
        
        RollYear__c rollYear1 = new RollYear__c();
        rollYear1.Name = rollYear;
        rollYear1.Year__c = rollYear;
        rollYear1.Status__c='Roll Open';
        insert rollYear1;

        RollYear__c rollYear2 = new RollYear__c();
        rollYear2.Status__c = 'Roll Closed';
        rollYear2.Year__c = lastYear;
        rollYear2.Name = lastYear;
        insert rollYear2;
        
        List<Account> accounts = new List<Account>();
        Account account1 = new Account();
        account1.Name='test account1';
        account1.BusinessStatus__c='Active';
        account1.MailingCountry__C='US';
        account1.TotalValueofProperties__c = 2000;
        account1.RequiredToFileYear__c =  String.valueOf(FiscalYearUtility.getCurrentFiscalYear()+1);
        account1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accounts.add(account1);
        
        Account account2 = new Account();
        account2.Name='test account2';
        account2.BusinessStatus__c='Active';
        account2.MailingCountry__C='US';
        account2.TotalCostofProperties__c = 50000;
        account2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accounts.add(account2);
        
        Account account3 = new Account();
        account3.Name='test account3';
        account3.BusinessStatus__c='Active';
        account3.MailingCountry__C='US';
        account3.TotalValueofProperties__c = 5000;
        account3.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accounts.add(account3);
        system.debug('account3.NoticeType__c--'+account3.NoticeType__c);
        
        Account account4 = new Account();
        account4.Name='test account4';
        account4.BusinessStatus__c='Active';
        account4.MailingCountry__C='US';
        account4.TotalValueofProperties__c = 2000;
        account4.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accounts.add(account4);
        
        Account account5 = new Account();
        account5.Name='test account5';
        account5.BusinessStatus__c='Active';
        account5.MailingCountry__C='US';
        account5.TotalValueofProperties__c = 2000;
        account5.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accounts.add(account5);

        Account account6 = new Account();
        account6.Name='test account6';
        account6.BusinessStatus__c='Active';
        account6.MailingCountry__C='US';
        account6.TotalValueofProperties__c = 450;
        account6.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        accounts.add(account6);
        insert accounts;
        
        Penalty__c penalty = new Penalty__c();
        penalty.PenaltyMaxAmount__c = 500;
        penalty.Percent__c = 10;
        penalty.RTCode__c = '463';
        insert penalty;
        
        List<Property__c> properties = new List<Property__c>();        
        Property__c property1 = new Property__C();
        property1.Name = 'test property1';
        property1.RollCode__c = 'Unsecured';
        property1.MostRecentAssessedValue__c = 2000;
        property1.PrimaryFormType__c = '571-L';
        property1.Account__c = account1.Id;
        property1.RequiredToFile__c = 'Yes';
        properties.add(property1);
        
        Property__c property2 = new Property__C();
        property2.Name = 'test property2';
        property2.MostRecentAssessedValue__c = 50000;
        property2.PrimaryFormType__c = '571-L';
        property2.Account__c = account2.Id;
        property2.AssessedCost__c = 1000;
        properties.add(property2);
        
        Property__c property3 = new Property__C();
        property3.Name = 'test property3';
        property3.RollCode__c = 'Unsecured';
        property3.PrimaryFormType__c = '571-L';
        property3.Account__c = account3.Id;
        property3.LastFilingMethod__c = 'SDR';
       properties.add(property3);
        
        Property__c property4 = new Property__C();
        property4.Name = 'test property4';
        property4.RollCode__c = 'Unsecured';
        property4.Account__c = account4.Id;
        property4.PrimaryFormType__c = '571-L';
        properties.add(property4);
        
        Property__c property5 = new Property__C();
        property5.Name = 'test property5';
        property5.RollCode__c = 'Unsecured';
        property5.Account__c = account5.Id;
        property5.RequiredToFile__c='No';
        property5.NotRequiredToFileReason__c = 'Reference';
        property5.PrimaryFormType__c = '571-L';
        properties.add(property5);

        Property__c property6 = new Property__C();
        property6.Name = 'test property6';
        property6.RollCode__c = 'Unsecured';
        property6.Account__c = account6.Id;
        property6.PrimaryFormType__c = '576-D';
        property6.RecordTypeId = Schema.SObjectType.Property__c.getRecordTypeInfosByDeveloperName().get('Vessel').getRecordTypeId();
        properties.add(property6);
        insert properties;
        
        List<Case> casestests = new List<Case>();
        Case cas = new Case();
        cas.Property__C = property1.id;
        cas.MailingCountry__c = 'US';
        cas.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        cas.EventDate__c = Date.newInstance(prevoiusYear, 1, 1);
        cas.RollYear__c=String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        cas.Type='Regular';
        cas.IntegrationStatus__c = 'Ready to send to TTX';
        casestests.add(cas);
        
        Case cas1 = new Case();
        cas1.Property__C = property2.id;
        cas1.MailingCountry__c = 'US';
        cas1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        cas1.EventDate__c = Date.newInstance(prevoiusYear, 1, 1);
        cas1.RollYear__c=lastYear;
        cas1.Type='Regular';
        casestests.add(cas1);
        
        Case cas2 = new Case();
        cas2.Property__C = property3.id;
        cas2.MailingCountry__c = 'US';
        cas2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        cas2.EventDate__c = Date.newInstance(prevoiusYear, 1, 1);
        cas2.RollYear__c=lastYear;
        cas2.Type='Regular';
        casestests.add(cas2);
        
        Case cas3 = new Case();
        cas3.Property__C = property4.id;
        cas3.MailingCountry__c = 'US';
        cas3.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        cas3.EventDate__c = Date.newInstance(prevoiusYear, 1, 1);
        cas3.RollYear__c=lastYear;
        cas3.Type='Escape';
        casestests.add(cas3);
        
        Case cas4 = new Case();
        cas4.Property__C = property5.id;
        cas4.MailingCountry__c = 'US';
        cas4.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('BPPAssessment').getRecordTypeId();
        cas4.EventDate__c = Date.newInstance(prevoiusYear, 1, 1);
        cas4.RollYear__c=lastYear;
        cas4.Type='Regular';
        casestests.add(cas4);
        insert casestests;        
    }

    @isTest
    private static void SetNoticeTypeForAllKindsOfAccount(){
        List<Case> cases = [select id, Penalty__c,RollYear__c,Property__r.name from Case];
        for(Case case1 : cases)
        {
            case1.Penalty__c = null;
            case1.RollYear__c =String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
            case1.WaivePenaltyReason__c = 'System Removed';
        }
        Test.startTest();
        update cases;
        
        List<Account> accountsk = [select name, TotalValueofProperties__c,TotalCostofProperties__c from Account];
        for(Account account : accountsk)
        {
            if(account.Name=='test account1')
            {
                account.TotalValueofProperties__c = 2000;
            }
            else if(account.Name=='test account2')
            {
                account.TotalCostofProperties__c = 50000;
            }                      
        }
        update accountsk;
        Database.executeBatch(new UpdateAccountNoticeType());
        Test.stopTest();
        
        List<Account> accounts = [select name, NoticeType__c from Account];
        for(Account account : accounts)
        {
            if(account.name == 'test account1'){
                System.assertEquals('Low Value', account.NoticeType__c);
            } else if(account.name == 'test account2'){
                System.assertEquals('Direct Bill', account.NoticeType__c);
            } else if(account.name == 'test account3')
            {
                System.assertEquals('SDR', account.NoticeType__c);
            } else if(account.name == 'test account4'){
                System.assertEquals('Notice to File', account.NoticeType__c);
            } else if(account.name == 'test account5'){
                System.assertEquals('Not Applicable', account.NoticeType__c);
            } else if(account.name == 'test account6'){
                System.assertEquals('Not Applicable', account.NoticeType__c);
            }
        }       
    }
    
    @isTest
    private static void checkNoticeTypeForRegularWithPenalityCase(){
        
        Penalty__c penalty = [select id from Penalty__c];
        Case caseTest = [select id, Penalty__c,RollYear__c,Property__r.name from Case where Property__r.name Like 'test account1%'];
        caseTest.Penalty__c=penalty.id;
       caseTest.ApplyPenaltyReason__c='Test Penalty applied';
        caseTest.RollYear__c =String.valueOf(FiscalYearUtility.getCurrentFiscalYear()-1);
        Test.startTest();
        update caseTest;
        
        
        Database.executeBatch(new UpdateAccountNoticeType());
        Test.stopTest();
        
        List<Account> accounts = [select name, NoticeType__c from Account];
        for(Account account : accounts)
        {
            if(account.name == 'test account1')
            {
                System.assertEquals('Notice to File', account.NoticeType__c);
            }
        }
    }
}