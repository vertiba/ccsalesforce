/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// 		Class to test AccountDocumentRequestGenerator
//-----------------------------
@isTest
public class AccountDocumentRequestGeneratorTest {

    /**
     * @author : Publicis.Sapient
     * @description : creating data setup, so that main functionality can be tested. 
     * @return  : void
     */
    @testSetup
    private static void dataSetup(){
        TH1__Schema_Set__c schemaSet = new TH1__Schema_Set__c();
        insert schemaSet;
        
        TH1__Schema_Object__c schemaObject = new TH1__Schema_Object__c();
        schemaObject.TH1__Is_Primary_Object__c = true;
        schemaObject.TH1__Object_Label__c = 'Account';
        schemaObject.TH1__Schema_Set__c = schemaSet.Id;
        schemaObject.Name = 'Account';
        insert schemaObject;
        
        schemaSet.TH1__Primary_Object__c = schemaObject.Id;
        update schemaSet;
        
        List<String> documentSettingNames = new List<String>{Label.AccountDirectBillingNotice,
            Label.AccountLowValueExemptionNotice, Label.AccountNoticeOfRequirementToFile571L,
            Label.AccountSDRNotice};
                List<TH1__Document_Setting__c> documentSettings = new List<TH1__Document_Setting__c>();
        TH1__Document_Setting__c documentSettting;
        for(integer i=0;i<4;i++) {
            documentSettting = new TH1__Document_Setting__c();
            documentSettting.TH1__Document_Data_Model__c = schemaSet.Id;
            documentSettting.Name = documentSettingNames.get(i);
            documentSettting.TH1__Storage_File_Name__c = documentSettingNames.get(i);
            documentSettting.TH1__Is_disabled__c=false;
            documentSettings.add(documentSettting);
        }
        insert documentSettings;
    }

    /**
     * @author : Publicis.Sapient
     * @description : Testing positive scenarions, so that one document generation 
     *      request is generated for each notice type. 
     * @return  : void
     */
    @isTest
    private static void generateDocumentsForAccount(){
        List<Account> accounts = new List<Account>();
        Account account;
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        List<String> picklistValues = new List<String>{'Direct Bill','Notice to File','Low Value','SDR'};
        for(integer i=0;i<4;i++){
            account = new Account();
            account.Name='test account'+i;
            account.RecordTypeId=businessAccountRecordTypeId;
            account.BusinessStatus__c='active';
            account.MailingCity__c='San Franciso';
            account.MailingCountry__C='US';
            account.MailingStreetName__c = 'Bush Street';
            account.RequiredToFile__c = 'Yes';
            account.NoticeType__c = picklistValues.get(i);
            accounts.add(account);
        }
        insert accounts;
        
        Test.startTest();
        Database.executeBatch(new AccountDocumentRequestGenerator('Select Id from Account'));
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
        Test.stopTest();
        documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(4, documentGenerationRequests.size());
    }

    /**
     * @author : Publicis.Sapient
     * @description : Testing negative scenarions, so that for any negative
     *      conditions, document generation request is not created. 
     * @return  : void
     */
    @isTest
    private static void donotGenerateDocumentsForAccount(){
        List<Account> accounts = new List<Account>();
        Account account;
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();
        for(integer i=0;i<4;i++){
            account = new Account();
            account.Name='test account'+i;
            account.RecordTypeId=businessAccountRecordTypeId;
            account.BusinessStatus__c='active';
            account.MailingCity__c='San Franciso';
            account.MailingCountry__C='US';
            account.RequiredToFile__c = 'No';
            account.NotRequiredToFileReason__c = 'Government Entity';
            account.NoticeType__c = '';
            accounts.add(account);
        }
        insert accounts;
        
        Test.startTest();
        Database.executeBatch(new AccountDocumentRequestGenerator('Select Id from Account'));
        List<DocumentGenerationRequest__c> documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
        Test.stopTest();
        documentGenerationRequests = [select id from DocumentGenerationRequest__c];
        System.assertEquals(0, documentGenerationRequests.size());
    }
}