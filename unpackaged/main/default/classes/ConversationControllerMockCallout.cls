@isTest
global class ConversationControllerMockCallout implements HttpCalloutMock {
     Boolean  isMockResponseSuccessful; 
    
    public ConversationControllerMockCallout(Boolean isMockResponseSuccessful) {
        this.isMockResponseSuccessful  = isMockResponseSuccessful;
    }
	
    global HTTPResponse respond(HTTPRequest req) {
        String passUrl = Test.isRunningTest() ? 'http://dummyClassName.com' :[Select Endpoint from NamedCredential where developerName ='SMART_Job_Monitoring_Endpoint' limit 1].Endpoint;
        System.assertEquals(true, req.getEndpoint().contains(passUrl));
        System.assertEquals('POST', req.getMethod());
        string sessionBody='Fake Response';
        
        if (req.getEndpoint().contains('summary/2020-1-21')){
           sessionBody ='Conversion';
        }
        // Create a fake response
        HttpResponse res = new HttpResponse();
        if (this.isMockResponseSuccessful) {
            res.setHeader('Content-Type', 'application/json');
            res.setBody(sessionBody);
            res.setStatusCode(200);
        }else{
            res.setHeader('Content-Type', 'application/json');         
            res.setStatusCode(400);
            res.setStatus('Bad request');
        }
        return res;
    }
}