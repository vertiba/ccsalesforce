//Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to collect the data for Email that will be sent to escro after the Approval
// 					of Closeouts.
//-----------------------------
public class CloseoutEmailTemplateController {
    
    public Id assessmentId {get;set;}
    public Case regularCloseout{get;set;}
   
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param : None
    // @description : This method collect the data as per required for email to escro.
    // @return : DataWrapper
    //-----------------------------
    public DataWrapper getdata(){
        
        List<EmailWrapper> emailTaxLiableWrapperList = new List<EmailWrapper>();
        List<EmailWrapper> emailNonTaxLiableWrapperList = new List<EmailWrapper>();
        DataWrapper objBigWrapper = new DataWrapper();
        
        AssessmentDataWrapper assessmentData = CloseoutAssessmentController.getCloseoutAssessment(assessmentId);
        
        Date businessOpenDate = assessmentData.caseRecord.Property__r.BusinessLocationOpenDate__c;
        Integer currentCaseAssessmentYear = Integer.valueOf(assessmentData.caseRecord.AssessmentYear__c);

        Set<String> missingEscapeYears = new Set<String>();
        // Logic to get valid escape years on the basis of Statue of Limitation
        Integer statuteOfLimitation = String.isNotBlank(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION_CONFIG) ? Integer.valueOf(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION_CONFIG) : Integer.valueOf(CCSFConstants.ASSESSMENT.STATUTE_OF_LIMITATION);
        Integer numberOfLimitationYears = Integer.ValueOf(statuteOfLimitation/12);
      	
        // Logic to calculate escape years based on the Statue Of Limitation.
        if(businessOpenDate == null || (currentCaseAssessmentYear -  Integer.valueOf(businessOpenDate.year()) >= numberOfLimitationYears)) {
            missingEscapeYears = calculateEscapeYears(Integer.valueOf(assessmentData.caseRecord.AssessmentYear__c), numberOfLimitationYears);
        }else {
            missingEscapeYears = calculateEscapeYears(currentCaseAssessmentYear, currentCaseAssessmentYear -  Integer.valueOf(businessOpenDate.year()));
        }
        regularCloseout = assessmentData.caseRecord;
        if(assessmentData !=null && !assessmentData.escapeCloseouts.isEmpty()) {
            for(Case escapeCloseout : assessmentData.escapeCloseouts) {
                EmailWrapper emailWrapper = new EmailWrapper();
                emailWrapper.assessmentYear = escapeCloseout.AssessmentYear__c;
                emailWrapper.escapeAssessedValue = escapeCloseout.TotalAssessedValue__c;
                if(escapeCloseout.TotalAssessedValue__c > Decimal.valueOf(Label.LowValueAmount)) {
                	emailTaxLiableWrapperList.add(emailWrapper);
                }else {
                    emailNonTaxLiableWrapperList.add(emailWrapper);
                }
                missingEscapeYears.remove(escapeCloseout.AssessmentYear__c); 
            }
        }
        if(!missingEscapeYears.isEmpty()) {
            for(String missingEscapeYear : missingEscapeYears){
                 EmailWrapper emailWrapper = new EmailWrapper();
                 emailWrapper.assessmentYear = missingEscapeYear;
                 emailNonTaxLiableWrapperList.add(emailWrapper);
            }
        }
        objBigWrapper.emailTaxLiableWrapperList = emailTaxLiableWrapperList;
        objBigWrapper.emailNonTaxLiableWrapperList = emailNonTaxLiableWrapperList;
        return objBigWrapper;
    }
     //-----------------------------.
    // @author : Publicis.Sapient
    // @param : currentAssessmentYear -  Asessment Year in which the closeout happens, 
    //          subtractor - number of iterations to get the escape year by subtracting unity in each iteration.
    // @description : This method does the calculations for the years based on Statue of Limitation.
    // @return : void
    //-----------------------------

    public set<String> calculateEscapeYears(integer currentAssessmentYear, Integer subtractor) {

        // returning if any of the paramter is null.
        if(currentAssessmentYear == null || subtractor == null) { 
            return null;
        }
        // Getting Escape years by subtracting unity form the current assessment year as per the Statue of limitation 
        Set<String> desiredYears = new Set<String>();
        for(Integer i = 1; i<= subtractor; i++){
            Integer desiredYear = currentAssessmentYear -i;
            desiredYears.add(String.valueOf(desiredYear));
        }
        return desiredYears;
    }
    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @description : Wrapper class to hold the table columns data foremail
    //-----------------------------
    public class EmailWrapper{
        
        public String assessmentYear{get;set;}
        public Decimal escapeAssessedValue{get;set;}
    }

    //-----------------------------.
    // @author : Publicis.Sapient
    // @description : Wrapper class to hold the table data based on Tax Liability
    //-----------------------------
    public class DataWrapper{
        
        public List<EmailWrapper> emailTaxLiableWrapperList{get;set;}
        public List<EmailWrapper> emailNonTaxLiableWrapperList{get;set;}
    }
}