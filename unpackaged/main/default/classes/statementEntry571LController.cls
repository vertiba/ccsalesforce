/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/

//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to display the records in fileUpload component.
//---
public class statementEntry571LController {
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : recordId
    // @description : This method, returns whether document has uploaded or not.
    // @return : Boolean
    //-----------------------------
    @AuraEnabled
    public static Boolean getcontentDocumentLinks(String recordId){
        List <ContentDocumentLink> contentDocumentLinks = 
            [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =:recordId];
        //getting the document related to statement
        return !contentDocumentLinks.isEmpty();
        
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @param : recordId
    // @description : This method, returns record type of the property on statement.
    // @return : String
    //-----------------------------
    @AuraEnabled
    public static String getStatementRecordType(String recordId)
    {
        Statement__c statement = [SELECT Id, Property__r.RecordType.DeveloperName FROM Statement__c WHERE Id = :recordId];
        //fetching the statement record
        return statement.Property__r.RecordType.DeveloperName;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : recordId   
    // @description : This method, returns all the statement reported assest and their child reported asset schedules record agains an statement.
    // @return : String
    //-----------------------------
    @AuraEnabled
    public static List<StatementReportedAsset__c> getStatementReportedAssets(String recordId)
    {      
        List<StatementReportedAsset__c> statementReportedAssets = [SELECT Id, Form__c, Subcategory__c, Category__c, AcquisitionYear__c, Cost__c,
                                                                   NetValueChangedfromLastYear__c, TotalAdditionValue__c, TotalDisposalValue__c, IsMatchedAmount__c,
                                                                   (SELECT Id, Name, Addition_Disposal_Month__c, Addition_Disposal_Year__c, Cost__c, Type__c, Description__c FROM ReportedAssetSchedules__r order by Type__c)
                                                                   FROM StatementReportedAsset__c WHERE Statement__c =: recordId 
                                                                   AND Subcategory__c IN : CCSFConstants.STATEMENT_REPORTED_ASSETS.SCHEDULE_D_ASSET_CLASSIFICATIONS AND NetValueChangedfromLastYear__c != 0];
        return statementReportedAssets;
    }
    //-----------------------------
    // @author : Publicis.Sapient  
    // @description : This method, returns the boolean value on the basis of profile checks.
    // @return : Boolean
    //-----------------------------
    @AuraEnabled
    public static Boolean getTaxPayerInfo(){
         Boolean isTaxPayer = UserUtility.isTaxPayer(); 
        return isTaxPayer;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : ASR- 10039 - This method returns the tax payer info
    // @return : User details
    //-----------------------------
    @AuraEnabled
    public static User retrieveTaxPayerInfo(){
        User taxpayerinfo = [Select Id,Profile.Name,Email from User where Id =: userinfo.getuserid()];
        return taxpayerinfo;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : none
    // @description : ASR- 10039 - This method returns the default acq year to be shown on creating 
    // new statement reported asset on part II&III of statement comparator.
    // @return : String
    //-----------------------------
    @AuraEnabled
    public static String getDefaultAcquistionYear(){
        String defaultAcquistionYear;
        Integer assessmentYear = FiscalYearUtility.getFiscalYear(System.Today());
        if(assessmentYear != null){
            defaultAcquistionYear = String.valueof(assessmentYear-1);
        }
        return defaultAcquistionYear;
    }
}