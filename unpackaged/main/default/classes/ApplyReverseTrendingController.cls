/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class ApplyReverseTrendingController {

    //Stores default factor for multiple years.
    @TestVisible
    private static Map<String, Factor__c> defaultFactor;
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : This method returns the assessmentlineitem.subcategory
    // 		 picklist values as a map, so that they are displayed in UI.
    // @return : Map<String,String>
    //-----------------------------
    @AuraEnabled
    public static Map<String, String> fetchAssetClassificationPicklists() {
        Map<String, String> options = new Map<String, String>();
        for (Schema.PicklistEntry pickList: DescribeUtility.getPicklistValues('AssessmentLineItem__c','Subcategory__c')) {
            options.put(pickList.getValue(), pickList.getLabel());
        }
        return options;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : assetClassification
    // @description : This method returns the dependent picklists for assessmentlineitem.subcategory
    // 		 picklist values as a map, so that they are displayed in UI.
    // @return : Map<String,String>
    //-----------------------------
    @AuraEnabled
    public static Map<String, String> fetchSubAssetClassificationPicklists(String assetClassification) {
        Schema.SObjectField assetClassificationField = AssessmentLineItem__c.Subcategory__c.getDescribe().getSObjectField();
        Schema.SObjectField subAssetClassificationField = AssessmentLineItem__c.AssetSubclassification__c.getDescribe().getSObjectField();
        Map<String, String> options = new Map<String, String>();
        List<DependentPicklistUtil.PicklistEntryWrapper> dependentList = DependentPicklistUtil.getDependentOptionsImpl(subAssetClassificationField, assetClassificationField).get(assetClassification);
        if(dependentList != null){
            for (DependentPicklistUtil.PicklistEntryWrapper pickList: dependentList) {
                options.put(pickList.value, pickList.label);
            }
        }
        return options;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : caseId
    // @param : assetClassification
    // @param : assetSubClassification
    // @description : This method returns the list of assessmentlineitems mathcing
    // 			with asset classification and subclassification in the case.
    // @return : List<AssessmentLineItem__c>
    //-----------------------------
    @AuraEnabled
    public static List<AssessmentLineItem__c> fetchRelatedAssessmentLineItems(String caseId, String assetClassification,
                                                                             String assetSubClassification){
        return [select id, Name, AcquisitionYear__c, AdjustedCost__c, ReverseTrendingAffectedYear__c,
                ReverseTrendingCostRemoved__c, ReverseTrendingAdjustedCost__c
                from AssessmentLineItem__c where case__c =:caseId and subcategory__c =:assetClassification 
                and AssetSubclassification__c =:assetSubClassification 
                and AcquisitionYear__c != null order by AcquisitionYear__c desc];
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : caseId
    // @param : assetClassification
    // @param : assetSubClassification
    // @param : fromYear
    // @param : toYear
    // @description : This method calculates the reversetrending values for assessment line items
    // 		on a case based on classification, subclassification, fromyear and toyears.
    // @return : List<AssessmentLineItem__c>
    //-----------------------------
    @AuraEnabled
    public static List<AssessmentLineItem__c> fetchReverseTrendingDetails(String caseId, String assetClassification, String assetSubClassification, String fromYear, String toYear){
        //Lineitems for display
        List<AssessmentLineItem__c> uiDisplayedAssessmentLineItems = [
            select id, name, AcquisitionYear__c, AdjustedCost__c, ReverseTrendingAffectedYear__c, ReverseTrendingCostRemoved__c,
            ReverseTrendingAdjustedCost__c from Assessmentlineitem__c where case__c =:caseId and subcategory__c =:assetClassification 
            and AssetSubclassification__c =: assetSubClassification
            and AcquisitionYear__c != null order by AcquisitionYear__c asc];
        if(uiDisplayedAssessmentLineItems == null || uiDisplayedAssessmentLineItems.isEmpty()) return uiDisplayedAssessmentLineItems;
        //Assessment Year, to get Depreciation factor. Property to get previous year line items. 
        Case currentAssessment = [select id, AssessmentYear__c, Property__c from case where id=:caseId];
        Assessmentlineitem__c lastYearReverseTrendingAppliedItem = getLastYearsReverseTrendedLineItem(currentAssessment,assetClassification, assetSubClassification);
        Integer frmYear = Integer.valueOf(fromYear);
        if(lastYearReverseTrendingAppliedItem != null && String.isNotBlank(lastYearReverseTrendingAppliedItem.ReverseTrendingAffectedYear__c)){
            //Now we are concatinating year like 2014, 2015, 2016
            String lastReverseTrendedYear = lastYearReverseTrendingAppliedItem.ReverseTrendingAffectedYear__c;
            if(Integer.valueOf(lastReverseTrendedYear) >= frmYear) {
                // If last year case is reverse trended with 2015 year, and this year also auditor applies
                // 2015 as the year in the range, then system has to ignore it and go with next year.
                frmYear = Integer.valueOf(lastYearReverseTrendingAppliedItem.ReverseTrendingAffectedYear__c) +1;
            }
        }

        Integer tYear = Integer.valueOf(toYear);
        List<AssessmentLineItem__c> assessmentLineItemsInRange = getAssessmentLineItemsInRange (uiDisplayedAssessmentLineItems, frmYear, tYear); 
        Set<String> acquistionYearsForReverseTrending = new Set<String>();
        
        while(frmYear<=tYear){
            acquistionYearsForReverseTrending.add(String.valueOf(frmYear));
            frmYear++;
        }
        Map<String, Map<String, Factor__c>> factorsByAcquistionYear = getFactorsForReverseTrending(assetClassification, assetSubClassification, acquistionYearsForReverseTrending);
        Double amountToBeReverseTrended = 0;
        AssessmentLineItem__c wrapper;
        //reversetrending not applied last year, start from first line item, else find the matching one.
        for(AssessmentLineItem__c assessmentLineItem : uiDisplayedAssessmentLineItems){
            //Anything above from year, should be ignored.
            if(assessmentLineItem.AcquisitionYear__c >= fromYear) {
                assessmentLineItem.ReverseTrendingAffectedYear__c = '';
                assessmentLineItem.ReverseTrendingAdjustedCost__c = assessmentLineItem.AdjustedCost__c;
                assessmentLineItem.ReverseTrendingCostRemoved__c = 0;
                continue;
            }
            if (lastYearReverseTrendingAppliedItem != null && Integer.valueOf(assessmentLineItem.AcquisitionYear__c) < 
                Integer.valueOf(lastYearReverseTrendingAppliedItem.AcquisitionYear__c)) {
                    //last year reverse trending applied year is 2004, but we are still 
                    //looping at 2002 and 2001. Reset to 0.
                    assessmentLineItem.ReverseTrendingAdjustedCost__c = 0;
                    continue;
                }
            if (lastYearReverseTrendingAppliedItem != null && Integer.valueOf(assessmentLineItem.AcquisitionYear__c) == 
                Integer.valueOf(lastYearReverseTrendingAppliedItem.AcquisitionYear__c)) {
                    //In Scenarion, last year reverse trending applied to acquistion year is 2004, 
                    //but in this year auditor tries to reverse trend only for 2004, then it is ignored.
                    //In that case, calculation will not happen and empty columns in shown, fixing it.
                    assessmentLineItem.ReverseTrendingAdjustedCost__c = lastYearReverseTrendingAppliedItem.ReverseTrendingAdjustedCost__c;
                } else if(assessmentLineItemsInRange.size() == 0 && amountToBeReverseTrended == 0){
                    //Some line items are between the last calculated line item and from year. This 
                    //will avoid the empty column in UI for ReverseTrendingAdjustedCost__c. 
                    assessmentLineItem.ReverseTrendingAdjustedCost__c = assessmentLineItem.AdjustedCost__c;
                    assessmentLineItem.ReverseTrendingCostRemoved__c = 0;
                }
            
            //Calculation starts from this line item, till amountToBeReverseTrended is 0 or 
            //all the line items in range or completed.
            while(assessmentLineItemsInRange.size() > 0 || amountToBeReverseTrended>0){
                double factorAmount = 0;
                if(amountToBeReverseTrended == 0) {
                    wrapper = assessmentLineItemsInRange.remove(0);
                    amountToBeReverseTrended = wrapper.AdjustedCost__c;
                }
                Decimal factorPercent = getFactorForAcquistionYear(wrapper.AcquisitionYear__c, assessmentLineItem.AcquisitionYear__c, factorsByAcquistionYear);
                factorAmount = (amountToBeReverseTrended/factorPercent);//.round(System.RoundingMode.HALF_UP);
                double amountToConsider = assessmentLineItem.ReverseTrendingAdjustedCost__c >0?assessmentLineItem.ReverseTrendingAdjustedCost__c:assessmentLineItem.AdjustedCost__c;
                if(lastYearReverseTrendingAppliedItem != null && 
                   assessmentLineItem.AcquisitionYear__c == lastYearReverseTrendingAppliedItem.AcquisitionYear__c){
                       if(lastYearReverseTrendingAppliedItem.ReverseTrendingAdjustedCost__c != null){
                           amountToConsider = lastYearReverseTrendingAppliedItem.ReverseTrendingAdjustedCost__c;
                       }
                       //has to be applied to one item only and not required after that, every item so, making it null\
                       lastYearReverseTrendingAppliedItem = null;
                   }
                if(String.isNotBlank(assessmentLineItem.ReverseTrendingAffectedYear__c)) {
                    //Now we are concatinating year like 2014, 2015, 2016
            		assessmentLineItem.ReverseTrendingAffectedYear__c += ', '+ wrapper.AcquisitionYear__c;
                } else {
                    assessmentLineItem.ReverseTrendingAffectedYear__c = wrapper.AcquisitionYear__c;
                }
                assessmentLineItem.ReverseTrendingCostRemoved__c = assessmentLineItem.ReverseTrendingCostRemoved__c == null?
                    0 : assessmentLineItem.ReverseTrendingCostRemoved__c;
                if(amountToConsider>=factorAmount) {
                    assessmentLineItem.ReverseTrendingCostRemoved__c += factorAmount;
                    assessmentLineItem.ReverseTrendingAdjustedCost__c = amountToConsider - factorAmount;
                    //if assessmentLineItemsInRange has more rows, continue else this will be last calculation.
                    amountToBeReverseTrended = 0;
                } else {
                    factorAmount -= amountToConsider;
                    amountToBeReverseTrended = (factorAmount * factorPercent);//.round(System.RoundingMode.HALF_UP)
                    assessmentLineItem.ReverseTrendingCostRemoved__c += amountToConsider;
                    assessmentLineItem.ReverseTrendingAdjustedCost__c = 0;
                    //if uiDisplayedAssessmentLineItems has more rows, for reversetrending, 
                    //calculation will continue, to next line item.
                    break;
                }
            } 
        }
        List<AssessmentLineItem__c> returnedAssessmentLineItems = new List<AssessmentLineItem__c>();
        for(integer index=uiDisplayedAssessmentLineItems.size()-1;index>=0;index--){
            wrapper = uiDisplayedAssessmentLineItems.get(index);
            if(wrapper.ReverseTrendingCostRemoved__c>0){
                wrapper.ReverseTrendingCostRemoved__c = wrapper.ReverseTrendingCostRemoved__c.round(System.RoundingMode.HALF_UP);
            }
            if(wrapper.ReverseTrendingAdjustedCost__c>0){
                wrapper.ReverseTrendingAdjustedCost__c = wrapper.ReverseTrendingAdjustedCost__c.round(System.RoundingMode.HALF_UP);
            }
            returnedAssessmentLineItems.add(wrapper);
        }
        return returnedAssessmentLineItems;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : List<AssessmentLineItem__c>
    // @description : This method Saves the earlier calculated reversetrending values into system.
    // 			 While Saving updates the assessed cost as well.
    // @return : void
    //-----------------------------
    @AuraEnabled
    public static void saveLineItemsWithReverseTrendingValues(List<AssessmentLineItem__c> assessmentLineItems) {
        if(!assessmentLineItems.isEmpty()){
            update assessmentLineItems;
        }
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : currentAssessment
    // @param : assetClassification
    // @param : assetSubClassification
    // @description : Based on the case, assetclassification, assetsubclassification get last years 
    // 		assessment line item for which reverse trended calulcation happened. So only adjusted cost
    // 		is calculated in this years calculation. Also, last year applied reverse trending line item
    // 		is not considered for this years calculation.
    // @return : AssessmentLineItem__c
    //-----------------------------
    @TestVisible
    private static AssessmentLineItem__c getLastYearsReverseTrendedLineItem(
        Case currentAssessment, String assetClassification, String assetSubClassification){
        Assessmentlineitem__c lastYearReverseTrendingAppliedItem;
        String lastFinancialYear = String.valueOf(Double.valueOf(currentAssessment.AssessmentYear__c).intValue()-1);
        List<Case> previousCases = [select id, ( select AcquisitionYear__c, Cost__c, ReverseTrendingCostRemoved__c, 
            ReverseTrendingAffectedYear__c, ReverseTrendingAdjustedCost__c from AssessmentLineItems__r where AcquisitionYear__c != null
            and subcategory__c =:assetClassification and AssetSubclassification__c =: assetSubClassification 
            order by AcquisitionYear__c desc, ReverseTrendingAffectedYear__c asc)
            from Case where property__c = :currentAssessment.property__c and AssessmentYear__c = :lastFinancialYear 
            // Filter modified as part of 10050.
            and AdjustmentType__c !=: CCSFConstants.ASSESSMENT.ADJUSTMENTTYPE_CANCEL AND SubStatus__c !=: CCSFConstants.ASSESSMENT.SUBSTATUS_CANCELLED AND IsCorrectedOrCancelled__c = false  order by EventDate__c,sequencenumber__c desc nulls last limit 1];
        if(previousCases != null && !previousCases.isEmpty()){
            List<Assessmentlineitem__c> lastYearAssessmentLineItems = new List<Assessmentlineitem__c>();
            lastYearAssessmentLineItems = previousCases.get(0).AssessmentLineItems__r;
            for(Assessmentlineitem__c lastYearItem : lastYearAssessmentLineItems){
                //Pick the item such that last year, reverse trending is applied, 
                if(String.isNotBlank(lastYearItem.ReverseTrendingAffectedYear__c)){ 
                    if (lastYearItem.ReverseTrendingCostRemoved__c != 0.0) {
                        //Last year this line item got reverse trended and has some value, consider this and break.
                        lastYearReverseTrendingAppliedItem = lastYearItem;
                    }
                    //If value is 0, then consider previous line item, so breaking here. 
                    break;
                }
                lastYearReverseTrendingAppliedItem = lastYearItem;
            }
        }
        return lastYearReverseTrendingAppliedItem;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : assessmentLineItems
    // @param : fromYear
    // @param : toYear
    // @description : Based on the from year and to year provided filter out the assessment line items
	//			Both from year and to year are inclusive in the returned list.
    // @return : List<AssessmentLineItem__c>
    //-----------------------------
    @TestVisible 
    private static List<AssessmentLineItem__c> getAssessmentLineItemsInRange(List<AssessmentLineItem__c> assessmentLineItems, Integer fromYear, Integer toYear){
        List<AssessmentLineItem__c> assessmentLineItemsInRange = new List<AssessmentLineItem__c>();
        Integer acquisionYear;
        for(AssessmentLineItem__c assessmentLineItem : assessmentLineItems){
            acquisionYear = Integer.valueOf(assessmentLineItem.AcquisitionYear__c);
            if(acquisionYear <= toYear && acquisionYear >= fromYear){
                assessmentLineItemsInRange.add(assessmentLineItem);
            }
        }
        return assessmentLineItemsInRange;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : assetClassification
    // @param : subclassification
    // @param : Set<assessmentYears>
    // @description : Based on the asset classification, sub classification and assessment years get 
	//			business asset mapping, and using that factors from the system. Once factors are returned 
    // 			then prepare a map of map based on Map<assessmentYear,Map<acquisitionyear, Factor__c>>. 
    // 			Also, if there is any factor that has fallback set to true, update class variable defaultFactor.
    // 			If there are No matching records in the system then return empty map.
    // @return : Map<String, Map<String, Factor__c>>
    //-----------------------------
    @TestVisible
    private static Map<String, Map<String, Factor__c>> getFactorsForReverseTrending(String assetClassification, String subclassification, Set<String> acquisitionYears) {
        defaultFactor = new Map<String,Factor__c>();
        Map<String, Map<String, Factor__c>> factorsByAcquistionYear = new Map<String, Map<String, Factor__c>>();
        for(String acquistionYear : acquisitionYears){
            factorsByAcquistionYear.put(acquistionYear, new Map<String,Factor__c>());
        }
        String businessAssetCompositeKey = Label.ReverseTrendingFactor+'-'+assetClassification + '-' + (subclassification == null ? '' : subclassification);
        BusinessAssetMapping__c businessAssetMapping = FactorUtility.getBusinessAssetMappingsByCompositeKeys(
            businessAssetCompositeKey, Label.ReverseTrendingFactor).get(businessAssetCompositeKey);
        if(businessAssetMapping == null ) return factorsByAcquistionYear;
		List<Factor__c> factors = [
            select AcquisitionYear__c, FactorPercent__c, IsFallback__c, AssessmentYear__c from Factor__c 
            where RecordType.Name =: Label.ReverseTrendingFactor and factorSource__c =:businessAssetMapping.FactorSource__c 
            and Subcomponent__c=:businessAssetMapping.Subcomponent__c and AssessmentYear__c in :acquisitionYears
            order by AssessmentYear__c desc, AcquisitionYear__c asc];
        if(factors == null || factors.isEmpty()) return factorsByAcquistionYear;
        for(Factor__c factor : factors) {
            factorsByAcquistionYear.get(factor.AssessmentYear__c).put(factor.AcquisitionYear__c, factor);
            if(factor.IsFallback__c) {
                defaultFactor.put(factor.AssessmentYear__c, factor);
            }
        }
        return factorsByAcquistionYear;
    }

    //-----------------------------
    // @author : Publicis.Sapient
    // @param : acquistionYear
    // @param : factorsByAcquistionYear
    // @description : Based on the acquisition year and factor map, this method returns the factor percent 
    // 			that needs to be used in caluclation. If there is no factor for corresponding year then, 
    // 			return fallback year. If not then return 1. 
    // @return : double
    //-----------------------------
    @TestVisible
    private static double getFactorForAcquistionYear(String assessmentYear, String acquisitionYear, Map<String, Map<String, Factor__c>> factorsByAcquistionYear){
        if(factorsByAcquistionYear.containsKey(assessmentYear) && 
          factorsByAcquistionYear.get(assessmentYear).containsKey(acquisitionYear)){
            return factorsByAcquistionYear.get(assessmentYear).get(acquisitionYear).FactorPercent__c/100;
        }
        if(defaultFactor.containsKey(assessmentYear) && String.isNotEmpty(defaultFactor.get(assessmentYear).AcquisitionYear__c)){
            return defaultFactor.get(assessmentYear).FactorPercent__c/100;
        }
        //avoid divide by zero if there is no data or no matching factors in the system.
        return 1;
    }
}