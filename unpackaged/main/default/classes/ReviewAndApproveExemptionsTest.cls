/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// Batch ->Test class for  ReviewAndApproveExemptions Apex class

@isTest
public class ReviewAndApproveExemptionsTest {
   

    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method to create Testdata.
    // @return : void
    //-----------------------------

    @testSetup
    private static void dataSetup(){
            
        List<User> users = new List<User>();
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin); 
        User bppStaff = TestDataUtility.getBPPAuditorUser();
        users.add(bppStaff);
        insert users;
        PermissionSet permissionSet = [select Id from PermissionSet where Name = :Label.EditRollYear];
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(
            AssigneeId = systemAdminUser.Id,
            PermissionSetId = permissionSet.Id
        );
        insert assignment;
        System.runAs(systemAdminUser) {
            
            String currentFiscalyear = String.valueof(FiscalYearUtility.getCurrentFiscalYear());            
            String previousFiscalyear = String.valueOf(Integer.valueOf(currentFiscalyear)-1);
            List<RollYear__c> rollyears = new List<RollYear__c>();
            RollYear__c currentRollyear = TestDataUtility.buildRollYear(currentFiscalyear,currentFiscalyear,'Roll Open');
            RollYear__c closedRollyear = TestDataUtility.buildRollYear(previousFiscalyear,previousFiscalyear,'Roll Closed');
            rollyears.add(currentRollyear);
            rollyears.add(closedRollyear);
            insert rollyears;
            
            Account businessAccount = TestDataUtility.getBusinessAccount();
            insert businessAccount;
            
            Penalty__c penalty = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty;
            
            Property__c bppProperty = TestDataUtility.getBPPProperty();
            bppProperty.Account__c = businessAccount.Id;
            insert bppProperty;
            
            Case cas = new Case();
            cas.AccountId = bppProperty.account__c;
            cas.Property__C = bppProperty.id;
            cas.MailingCountry__c = 'US';
            cas.EventDate__c = System.today();
            cas.Type='Regular';
            cas.RecordTypeId = DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME);
            
            insert cas; 
            
            AssessmentLineItem__c assessmentLineItem = TestDataUtility.buildAssessmentLineItem('2018', 12000.00,cas.id);
            insert assessmentLineItem; 
            
            //Exemption Property Event 
            PropertyEvent__c propertyEvent = new PropertyEvent__c();
            propertyEvent.property__c = bppProperty.id;
            propertyEvent.Type__c = 'Institutional';
            propertyEvent.SubType__c ='Welfare';
            propertyEvent.Status__c = 'New';
            insert propertyEvent;

            cas.Exemption__c = propertyEvent.id;
            update cas; 

            //insert Vessel property
            Property__c vesselProperty = TestDataUtility.getVesselProperty();
            vesselProperty.Account__c = businessAccount.Id;
            insert vesselProperty;
        }
    }

    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is used check the permission is there for the logged in User to Manage Exemptions
    // @return : void
    //-----------------------------

    @isTest
    public static void testPermissionCheck(){

        User auditiorUser = [SELECT Id FROM User WHERE LastName = 'auditiorUser'];
        PropertyEvent__c propertyEvent = [Select Id,property__c ,Type__c,SubType__c,Status__c from PropertyEvent__c];
        Case cs = [Select Id,integrationstatus__c ,Status,SubStatus__c,SequenceNumber__c from Case];
        List<Case> selectedCases = new List<Case>{cs};

        System.runAs(auditiorUser){
            Test.StartTest();
                Test.setCurrentPage(Page.ReviewAndApproveExemptions);
                ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedCases);
                stdSetController.setSelected(selectedCases);
                ReviewAndApproveExemptions ext = new ReviewAndApproveExemptions(stdSetController);
                ext.redirect();
                system.assertEquals(ext.recordStatus , System.Label.NoPermission);
            Test.StopTest();
        }

    }

    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is used to test the approve exemption Process.
    // @return : void
    //-----------------------------

    @isTest
    private static void approveExemtionTest() { 

        User sysAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        Case cs = [Select Id,Type,FinalCloseOutValue__c,integrationstatus__c ,RecordTypeId from Case WHERE Type =:'Regular'];
        List<Case> selectedCases = new List<Case>{cs};
        Test.startTest();
            Test.setCurrentPage(Page.ReviewAndApproveExemptions);
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(selectedCases);
            stdSetController.setSelected(selectedCases);
            ReviewAndApproveExemptions ext = new ReviewAndApproveExemptions(stdSetController);
            ext.redirect();
        Test.stopTest();

    }

}