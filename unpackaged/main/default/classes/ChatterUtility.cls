/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public with sharing class ChatterUtility {

    public class ChatterPost {
        @InvocableVariable(required=true label='Chatter Group Name or Record ID' description='Chatter group name or record ID used when posting the Chatter post')
        public String subject;

        @InvocableVariable(required=true label='Message to Post' description='The message to include in the Chatter post')
        public String message;

        @InvocableVariable(required=false label='(Optional) User ID to Mention' description='The ID of the user to mention in the Chatter post')
        public Id userId;
    }

    @InvocableMethod(label='Post to Chatter')
    public static void post(List<ChatterPost> chatterPosts) {
       
        Map<String, CollaborationGroup> chatterGroupByName = getChatterGroups(chatterPosts);

        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();

        for(ChatterPost post : chatterPosts) {
            ConnectApi.FeedItemInput feedItemInput             = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput       = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput       = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            if(post.userId != null) {
                mentionSegmentInput.id = post.userId;
                messageBodyInput.messageSegments.add(mentionSegmentInput);

                // Add padding so there's spacing between the mentioned user & the message
                post.message = ' ' + post.message;
            }
            textSegmentInput.text = post.message;
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body            = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId       = getSubjectId(post.subject, chatterGroupByName);
            feedItemInput.visibility      = ConnectApi.FeedItemVisibilityType.AllUsers;

            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);
        }

         // Adding Amend Test Class
         if(!Test.isRunningTest()) ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
    }

    private static Map<String, CollaborationGroup> getChatterGroups(List<ChatterPost> chatterPosts) {
        Set<String> postSubjects = new Set<String>();
        for(ChatterPost chatterPost : chatterPosts) {
            postSubjects.add(chatterPost.subject);
        }

        Map<String, CollaborationGroup> chatterGroupByName = new Map<String, CollaborationGroup>();
        for(CollaborationGroup chatterGroup : [SELECT Id, Name FROM CollaborationGroup WHERE Name IN :postSubjects]) {
            chatterGroupByName.put(chatterGroup.Name, chatterGroup);
        }
        return chatterGroupByName;
    }

    private static Id getSubjectId(String subject, Map<String, CollaborationGroup> chatterGroupByName) {
        if(chatterGroupByName.containsKey(subject)) {
            // If the subject matches a group name, return the group ID
            return chatterGroupByName.get(subject).Id;
        } else {
            // Otherwise, assume the subject is an ID and return it
            return Id.valueOf(subject);
        }
    }

}