/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
@isTest
public class statementEntry571LControllerTest {


    private static Integer currentYear 		= FiscalYearUtility.getCurrentFiscalYear();
    private static Integer lastYear 		= currentYear-1;
    private static Integer lastToLastYear 	= lastYear-1;
    private static Integer nextYear 		= currentYear+1;
    private static Date filingDate          = System.today();
	
    @testSetup
    private static void commonDataSetupForTesting() {
       
        //Create Users
        List<User> users = new List<User>();
        User principal = TestDataUtility.getPrincipalUser();
        users.add(principal);
        User systemAdmin = TestDataUtility.getSystemAdminUser();
        users.add(systemAdmin);  
        User auditor = TestDataUtility.getBPPAuditorUser();
        users.add(auditor);
        //Insert Users
        insert users; 
        
        User systemAdminUser = [SELECT Id FROM User WHERE LastName = 'sysAdminUser'];
        System.runAs(systemAdminUser) {
			
            User taxpayer = TestDataUtility.getPortalUser();
            //Insert Account
            Account account = TestDataUtility.getBusinessAccount();
            insert account;
                        
            //Insert Property
            Property__c property = TestDataUtility.getBPPProperty();
            property.Account__c = account.Id;
            insert property;           
            
            //Insert Roll Years
            List<RollYear__c> rollyears = new List<RollYear__c>();
            RollYear__c rollYear1 = new RollYear__c();
            rollYear1.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear1.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear());
            rollYear1.Status__c='Roll Open';
            rollyears.add(rollYear1);
            RollYear__c rollYear2 = new RollYear__c();
            rollYear2.Name = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            rollYear2.Year__c = String.valueof(FiscalYearUtility.getCurrentFiscalYear()-1);
            rollYear2.Status__c='Roll Closed';
            rollyears.add(rollYear2);
            insert rollyears;

            //Insert Penalty
            Penalty__c penalty463 = TestDataUtility.buildPenalty(500,10,CCSFConstants.PENALTY_RT_CODE_463);            
            insert penalty463;

            Test.startTest();

            //Insert Assessment
            Case cas  = TestDataUtility.buildAssessment (DescribeUtility.getRecordTypeId(Case.sObjectType, CCSFConstants.ASSESSMENT.BPP_RECORD_TYPE_API_NAME),property.Id,
                                                        rollyears.get(1).Id,rollyears.get(1).Name,rollyears.get(1).Name,CCSFConstants.ASSESSMENT.TYPE_REGULAR,4,
                                                        CCSFConstants.NOTICE_TYPE_LOW_VALUE,null,filingDate.addYears(-1));
            
            insert cas;
                        
             //Create Assessment Line Items
            List<AssessmentLineItem__c> assessmentLineItems = new List<AssessmentLineItem__c>();
            AssessmentLineItem__c assessmentLineItem1 = new AssessmentLineItem__c();
            assessmentLineItem1 = TestDataUtility.buildAssessmentLineItem('Alternate Schedule A', 'ATMs', '2017', 100.00, cas.id, 'Assessment Line Item for Current year');
            assessmentLineItems.add(assessmentLineItem1);
            AssessmentLineItem__c assessmentLineItem2 = new AssessmentLineItem__c();
            assessmentLineItem2 = TestDataUtility.buildAssessmentLineItem('Construction in Progress', 'Construction in Progress', '2016', 100.00, cas.id, 'Assessment Line Item for Current year');
            assessmentLineItems.add(assessmentLineItem2);
            //Insert Assessment Line Items        
            insert assessmentLineItems;
            
            //Create Statement         
            Map<String,String> inputParamsStatement = new Map<String,String>();            
            inputParamsStatement.put('accountId',account.Id);
            inputParamsStatement.put('propertyId',property.Id);
            inputParamsStatement.put('formType','571-L');
            inputParamsStatement.put('businessScenario','Existing');
            inputParamsStatement.put('propertyScenario','Existing');
            inputParamsStatement.put('assessmentYear',String.valueOf(lastYear)); 
            inputParamsStatement.put('recordTypeId',DescribeUtility.getRecordTypeId(Statement__c.sObjectType,CCSFConstants.STATEMENT.BPP_RECORD_TYPE_API_NAME));
            inputParamsStatement.put('startofBussinessDate',String.valueOf('01/03/'+lastToLastYear));
            inputParamsStatement.put('filingDate',String.valueOf('01/03/'+lastYear));            
            Statement__c statement = TestDataUtility.buildStatement(inputParamsStatement);
            //Insert Statement  
            insert statement;
            
            //Create Statement Reported Asset
            Id statementReportedOwnedAssetsRecordTypeId = DescribeUtility.getRecordTypeId(StatementReportedAsset__c.sObjectType, CCSFConstants.STATEMENT_REPORTED_ASSETS.OWNED_ASSETS_RECORD_TYPE_API_NAME);   
            List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
            StatementReportedAsset__c statementReportedAsset1 = new StatementReportedAsset__c();
            statementReportedAsset1 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Bldg/Bldg Impr/Leasehold Impr/Land/Land Impr', 'Land and Land Development', '2017', 100, null, statement.id, 'Statement Reported Asset for last year');
            statementReportedAssets.add(statementReportedAsset1);        
            StatementReportedAsset__c statementReportedAsset2 = new StatementReportedAsset__c();
            statementReportedAsset2 = TestDataUtility.buildStatementReportedAsset(statementReportedOwnedAssetsRecordTypeId, '571-L', 'Bldg/Bldg Impr/Leasehold Impr/Land/Land Impr', 'Land Improvements', '2016', 100, null, statement.id, 'Statement Reported Asset for last year');        
            statementReportedAssets.add(statementReportedAsset2);
            //Insert Statement Reported Asset
            insert statementReportedAssets;

            ReportedAssetSchedule__c reportedAssetSchedule1 = TestDataUtility.buildReportedAssetSchedule(statementReportedAsset1.id, 'January', '2017', 10000, 'Test Description', 'Addition');
            insert reportedAssetSchedule1;

            Test.stopTest();
        }    
    }
    
    @isTest
    static void getcontentDocumentLinksTest() {       
        Statement__c statement = [select id, property__c from Statement__c limit 1]; 
        System.assertEquals(false,statementEntry571LController.getcontentDocumentLinks(statement.Id));        
        
        //Create Document
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.Title = 'Test Document';
        contentVersion.PathOnClient = 'TestDocument.pdf';
        contentVersion.VersionData = Blob.valueOf('Test Content');
        contentVersion.IsMajorVersion = true;
        Insert contentVersion;        
        
        //Get Content Documents
        List<ContentDocument> contentDocuments = [Select id, Title from ContentDocument];
        
        //Create ContentDocumentLink 
        
        ContentDocumentLink contentDocumentLink = New ContentDocumentLink();
        contentDocumentLink.LinkedEntityId = statement.Id;
        contentDocumentLink.ContentDocumentId = contentDocuments[0].id;
        contentDocumentLink.shareType = 'V';
        Insert contentDocumentLink;
        
        System.assertEquals(true,statementEntry571LController.getcontentDocumentLinks(statement.Id));        
    }
    
    @isTest
    static void statementrecordTypeTest() {      
        Statement__c insertedStatements = [SELECT Id, Property__r.RecordType.DeveloperName FROM Statement__c limit 1];        
        String propertyRecordType = insertedStatements.Property__r.RecordType.DeveloperName;
        System.assertEquals(propertyRecordType,statementEntry571LController.getStatementRecordType(insertedStatements.Id));
    }
    
    @isTest
    static void getStatementReportedAssetsTest() {        
        Statement__c statement = [select id, Status__c from Statement__c limit 1]; 
        statement.Status__c = CCSFConstants.STATEMENT.STATUS_SUBMITTED;
        update statement;        
        List<StatementReportedAsset__c> statementReportedAssets = new List<StatementReportedAsset__c>();
        statementReportedAssets = statementEntry571LController.getStatementReportedAssets(statement.Id);        
        System.assertEquals(2,statementReportedAssets.size());
    }
    
    @isTest
    static void getTaxPayerInfotest(){
        Id taxpayerProfileId =[Select Id from Profile where name='Taxpayer'].Id;
        User taxpayer= [Select Id,ProfileId from User where ProfileId =: taxpayerProfileId Limit 1];        
        System.runAs(taxpayer){
          Test.startTest();
          Boolean isTaxPayer = statementEntry571LController.getTaxPayerInfo();
          Test.stopTest();
          System.assertEquals(true,isTaxpayer);      
        }  
    }
    
    @isTest
    static void retrieveTaxPayerInfotest(){
        Id taxpayerProfileId =[Select Id from Profile where name='Taxpayer'].Id;
        User taxpayer= [Select Id,ProfileId from User where ProfileId =: taxpayerProfileId limit 1];        
        System.runAs(taxpayer){
          Test.startTest();
          User taxpayerinfo = statementEntry571LController.retrieveTaxPayerInfo();
          Test.stopTest();
          System.assertEquals(taxpayerProfileId,taxpayerinfo.ProfileId);
        }  
    } 
    
    @isTest
    static void getDefaultAcquistionYeartest(){
        Id taxpayerProfileId =[Select Id from Profile where name='Taxpayer'].Id;
        User taxpayer= [Select Id,ProfileId from User where ProfileId =: taxpayerProfileId limit 1];
        Integer assessmentYear = FiscalYearUtility.getFiscalYear(System.Today());
        System.runAs(taxpayer){
          Test.startTest();
          String acqYear = statementEntry571LController.getDefaultAcquistionYear();
          Test.stopTest();
          System.assertEquals(String.valueof(assessmentYear-1),acqYear);
        }  
    } 
}