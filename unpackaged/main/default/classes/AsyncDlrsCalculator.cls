/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Queueable class that is used to asynchronously calculate DLRS-based roll-ups
//-----------------------------
public without sharing class AsyncDlrsCalculator implements Queueable {

    // Inner class for exceptions
    private class AsyncDlrsCalculatorException extends Exception {}

    // Constants
    private static final AsyncDlrsCalculator INSTANCE = new AsyncDlrsCalculator();
    private static final String LOG_LOCATION          = 'AsyncDlrsCalculator';
    private static final List<String> LOG_TOPICS      = new List<String>{'DLRS'};

    // Static variables
    private static Id jobId;

    // Member variables
    private List<DLRSWrapperClass> queuedChildRecords;
    private boolean startJob = true;

    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Method used to get the singleton instance of the queueable class
    // @return : AsyncDlrsCalculator - The current transaction's instance of AsyncDlrsCalculator
    //-----------------------------
    public static AsyncDlrsCalculator getInstance() {
        return INSTANCE;
    }

    /**
     * private constructor
     */
    private AsyncDlrsCalculator() {
        queuedChildRecords = new List<DLRSWrapperClass>();
    }

    /**
     * @author : Publicis.Sapient
     * @description : add the child record to the existing list.
     * @param : DLRSWrapperClass child records for which DLRS is yet to be processed
     * @return : void
     */
    public void add(DLRSWrapperClass childRecords) {
        queuedChildRecords.add(childRecords);
    }

    /**
     * @author : Publicis.Sapient
     * @description : add all the child records to the existing list.
     * @param : List<DLRSWrapperClass> child records for which DLRS is yet to be processed
     * @return : void
     */
    public void addAll(List<DLRSWrapperClass> childRecords) {
        queuedChildRecords.addAll(childRecords);
    }

    /**
     * @author : Publicis.Sapient
     * @description : Method used to add multiple sets of records to a single instance of the job per transaction (singleton pattern)
     * @param : childRecords - The list of child SObject records that have been updated - any matching DLRS rules are used to update the parent records
     * @return : Id - The ID of the queueable job
     */
    public Id calculate(Map<Id,sObject> newRecords, Map<Id,sObject> oldRecords) {
        final String METHOD_LOG_LOCATION = LOG_LOCATION + '.calculate(List<SObject> childRecords)';
        DLRSWrapperClass childRecords = new DLRSWrapperClass(newRecords, oldRecords);

        Logger.addDebugEntry(LoggingLevel.FINE, 'Starting calculate method, jobId=' + jobId, METHOD_LOG_LOCATION, LOG_TOPICS);
        Logger.addDebugEntry(LoggingLevel.FINE, 'childRecords=' + childRecords, METHOD_LOG_LOCATION, LOG_TOPICS);

        // Add the records to the list of records - they will be processed in the order that they are added
        this.add(childRecords);

        if(jobId != null) {
            Logger.addDebugEntry(LoggingLevel.FINE, 'Queueable job already queued', METHOD_LOG_LOCATION, LOG_TOPICS);
        } else if(startJob){
            jobId = System.enqueueJob(instance);
            Logger.addDebugEntry(LoggingLevel.FINE, 'Queueable job queued, jobId=' + jobId, METHOD_LOG_LOCATION, LOG_TOPICS);
        }
        Logger.saveLog();

        return jobId;
    }

    /**
     * @author : Publicis.Sapient
     * @description : Method called by the platform when the queueable job is executed, used to update parent records asynchronously
     * @param : queueableContext - The instance of the QueueableContext class, provided by the platform
     * @return : void
     */
    public void execute(QueueableContext queueableContext) {
        final String METHOD_LOG_LOCATION = LOG_LOCATION + '.execute(QueueableContext context)';

        Logger.addDebugEntry(LoggingLevel.FINE, 'Starting execute method', METHOD_LOG_LOCATION, LOG_TOPICS);
        if(this.queuedChildRecords == null) return;
        if(this.queuedChildRecords.isEmpty()) return;

        try {
            // Get the next list of child records/remove it from the list of queued child records
            DLRSWrapperClass currentChildRecords = this.queuedChildRecords.remove(0);
            Logger.addDebugEntry(LoggingLevel.FINE, 'IDs of currentChildRecords=' + Json.serialize(currentChildRecords.recordsById.keySet()), METHOD_LOG_LOCATION, LOG_TOPICS);

            if(currentChildRecords.oldRecordsById == null || currentChildRecords.oldRecordsById.isEmpty()){
                // Call DLRS to calculate the parent records & save them - any errors are logged
                List<SObject> parentRecords = dlrs.RollupService.rollup(currentChildRecords.recordsById.values());
                if(parentRecords != null && !parentRecords.isEmpty()) {
                    List<Database.SaveResult> saveResults = Database.update(parentRecords, false);
                    // This could also include functionality for tying the errors directly to the records via a lookup field
                    this.processSaveResults(saveResults);
                }
            } else {
                //When child record is updated this is executed.
                dlrs.RollupService.rollUp(currentChildRecords.recordsById, currentChildRecords.oldRecordsById, currentChildRecords.objectType);
            }
            // If there additional child records to calculate, start a new instance of the queueable job
            if(!this.queuedChildRecords.isEmpty()) {
                AsyncDlrsCalculator chainedInstance = new AsyncDlrsCalculator();
                chainedInstance.addAll(this.queuedChildRecords);
                if(Limits.getQueueableJobs() == 1){
                    //This check is required in following scenarions to avoid limits on queueable.
                    //case and assessmentlineitems are inserted at same time, For updates processSaveResults
                    //will not be called so no issues. So first dlrs on case is executed, updating property, 
                    //on property also dlrs are calculated, so one new queuable is generated from trigger.
                    //after that cursor goes on to create another queueable for assessment line items and fails.
                    DateTime dt = Datetime.now().addSeconds(3);
                    //String hour = String.valueOf(dt.hour());
                    //String min = String.valueOf(dt.minute()); 
                    //String ss = String.valueOf(dt.second() + 5);
                    //String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                    AsyncDLRSScheduler scheduler = new AsyncDLRSScheduler(chainedInstance);
                    System.schedule('ScheduledJob ' + String.valueOf(Math.random()), dt.format('ss mm hh dd MM ? YYYY'), scheduler);                
                } else {
                    //When a parent record is updated and didnt trigger a queueable job then this 
                    //else is executed. Like account where no dlrs are there or property changes for 
                    //which DLRS processing is not required. 
                    System.enqueueJob(chainedInstance);
                }
            }
        } catch(Exception ex) {
            Logger.addExceptionEntry(ex, METHOD_LOG_LOCATION, LOG_TOPICS);
        }
        Logger.saveLog();
    }

    /**
     * @author : Publicis.Sapient
     * @description : For insert, delete and undelete records once the dlrs are
     * 		calculated for child records, a set of updated parent results are returned. 
     * 		Those results are saved the saved results are logged for success and error
     * @param : List<Database.SaveResult> saved parent records results
     * @return : void
     */
    private void processSaveResults(List<Database.SaveResult> saveResults) {
        final String METHOD_LOG_LOCATION = LOG_LOCATION + '.processSaveResults(List<Database.SaveResult> saveResults)';
        for(Database.SaveResult saveResult : saveResults) {
            if(saveResult.isSuccess()) {
                Logger.addDebugEntry(LoggingLevel.FINE, 'Record ID ' + saveResult.getId() + ' successfully processed', METHOD_LOG_LOCATION, LOG_TOPICS);
            } else {
                AsyncDlrsCalculatorException recordSaveException = new AsyncDlrsCalculatorException(Json.serialize(saveResult.getErrors()));
                Logger.addExceptionEntry(recordSaveException, METHOD_LOG_LOCATION, LOG_TOPICS);
            }
        }
    }

    /**
     * @author : Publicis.Sapient
     * @description : For insert, delete and undelete records once the dlrs are
     * 		calculated for child records, a set of updated parent results are returned. 
     * 		Those results are saved the saved results are logged for success and error
     * @param : List<Database.SaveResult> saved parent records results
     * @return : void
     */
    public void setQueueToRun(boolean runQueue){
       
        this.startJob = runQueue;
        if(!runQueue) return;
        if(Limits.getQueueableJobs() == 1){
             AsyncDlrsCalculator chainedInstance = new AsyncDlrsCalculator();
             chainedInstance.addAll(this.queuedChildRecords);
            //This check is required in following scenarions to avoid limits on queueable.
            //in certain scenarior we are creating a new future method to avoid Apex CPU time outs.
            //so both the threads are calling this method and causing queueable limits because of this
            //we are adding this logic. 
            DateTime dt = Datetime.now().addSeconds(4);
            if(Test.isRunningTest()){
               //sometimes Salesforce sets it to the past day, so I am adding a day to bypass that exception
               //Reference Link - https://salesforce.stackexchange.com/questions/145545/scheduled-job-hanging-in-the-apex-jobs-queue 
               dt = dt.addDays(1);
            }
            //String hour = String.valueOf(dt.hour());
            //String min = String.valueOf(dt.minute()); 
            //String ss = String.valueOf(dt.second() + 5);
            //String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
            AsyncDLRSScheduler scheduler = new AsyncDLRSScheduler(chainedInstance);
            System.schedule('ScheduledJob ' + String.valueOf(Math.random()), dt.format('ss mm hh dd MM ? YYYY'), scheduler);                
        } else { 
            //This is a first queueable instance so we are running it.
            jobId = System.enqueueJob(instance);
        }
    }

    /**
     * @author : Publicis.Sapient
     * @description : Wrapper class to hold new records, old records on which rules are calculated.
     */
    public class DLRSWrapperClass {
        //Holds records on which rules have to be calculated. Can be 
        //old or new records depending on the operation.
        Map<Id, sObject> recordsById;
        //always holds old records only.
        Map<Id, sObject> oldRecordsById;
        Schema.SObjectType objectType;
        DLRSWrapperClass(Map<Id, sObject> records, Map<Id, sObject> oldRecords){
            recordsById = records;
            oldRecordsById = oldRecords;
            objectType = recordsById.values().get(0).getSObjectType();
        }
    }
}