/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
public class ReviewAndApproveExemptions {

    
    List<SObject> selectedCases = new List<SObject>();
    String customPermisionName ='ReviewAndApproveExemptions';
    public string recordStatus {get;set;}
    public  ApexPages.StandardSetController controller;

    public ReviewAndApproveExemptions(ApexPages.StandardSetController standardSetController){
        selectedCases = standardSetController.getSelected();
     }
     

    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is used to check the permission for the review exemption Action.
    // @return : void
    //-----------------------------
    
    public void redirect(){

        // Checking if the logged in user has the permission to create Escape Closeouts.
        Boolean hasPermission = FeatureManagement.checkPermission(customPermisionName); 
        if(!hasPermission) { 
            // If permission is not granted then we will return and show the error message as required.
            recordStatus = System.Label.NoPermission;
            return;
            
        }
        approveExemptions();
    }

    
    //-----------------------------.
    // @author : Publicis.Sapient
    // @param  : none
    // @description : This method is used to update the Exemption Review check on Assessments after Exemption Manager Approve the exemptions
    // @return : void
    //-----------------------------
    public void approveExemptions(){
        
       // selectedCases = controller.getSelected();
        if(!selectedCases.isEmpty()) {

            List<Case> assessmentList = [SELECT createdBy.Profile.Name, ExemptionReviewed__c,AdjustmentReason__c 
                                            FROM Case WHERE ID IN : selectedCases];

            List<Case> assessmentsByExemtionsStaff = new List<Case>();
            List<Case> assessmentsByAuditor = new List<Case>();
            List<Case> finalListToUpdate = new List<Case>();
            
            for(Case assessment : assessmentList) {

                if(!assessment.ExemptionReviewed__c) {
                    assessment.ExemptionReviewed__c = true;
                    finalListToUpdate.add(assessment);
                }
            }

            try{
                if(!finalListToUpdate.isEmpty()) {
                    update finalListToUpdate;
                    recordStatus = 'Exemptions Successfully Approved';
                }else {
                    recordStatus = 'Exemptions Details already Approved on the selected records';
                }
            }
            catch (Exception ex){
                recordStatus = 'Error Occured : '+ex.getMessage();
            }
            
        }else {
            recordStatus = 'Please select atleast one Asssessment for Approval';
        }
        return;
    }
}