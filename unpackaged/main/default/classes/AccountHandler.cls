/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
/**
 * @author : Publicis.Sapient
 * @description : trigger handler class for account object.
 */
public without sharing class AccountHandler extends TriggerHandler {

    /**
     * @author : Publicis.Sapient
     * @description : Method that will be called before insert
     * @param : List<SObject> newRecords Records that are inserted. 
     * @return : void
     */
    protected override void executeBeforeInsert(List<SObject> newRecords) {
        List<Account> newAccounts = (List<Account>)newRecords;

        this.setAccessPin(newAccounts);
        this.setAddressFields(newAccounts);
        this.populateCompanyIdPrefix(newAccounts);
    }

    /**
     * @author : Publicis.Sapient
     * @description : Method that will be called before update.
     * @param : List<SObject> updatedRecords Records with the updated values.
     * @param : Map<Id, SObject> updatedRecordsById Map of updated records with id as key.
     * @param : List<SObject> oldRecords records with old values for comparision.
     * @param : Map<Id, SObject> oldRecordsById Map of old records with id as key.
     * @return : void
     */
    protected override void executeBeforeUpdate(List<SObject> updatedRecords, Map<Id, SObject> updatedRecordsById, List<SObject> oldRecords, Map<Id, SObject> oldRecordsById) {
        List<Account> updatedAccounts = (List<Account>)updatedRecords;
        Map<Id, Account> oldAccountsById = (Map<Id, Account>)oldRecordsById;
        
        this.setAddressFields(updatedAccounts);
        this.checkForCompanyPrefixChange(updatedAccounts, oldAccountsById);
    }

	/*
     * Method Name: setAssessmentPin
     * Description: This method sets the AccessPins for the newly created Business Accounts. A random number will be generated.
     * Story: ASR- 6231 
     */ 
    private void setAccessPin(List<Account> newAccounts){
        //Though we require the trigger to be executed when migration happens, 
        //This method need not be executed. So adding this if check. As we are running the 
        //data load parallely this method is taking lots of time. So disabling it this way.
        if(TriggerHandlerSettings__c.getInstance().IsDataMigrationRun__c) return;
        
        List<Account> accountsToBeProcessed = new List<Account>();
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME).getRecordTypeId();
        for(Account accountInstance: newAccounts){
            if(accountInstance.RecordTypeId == businessAccountRecordTypeId){
                accountsToBeProcessed.add(accountInstance);
            }
        }
        
        if(accountsToBeProcessed.isEmpty()){
            return;
        }
        
        Integer accountSize = accountsToBeProcessed.size();
        String randomNumber;
        List<String> randomNumbers = new List<String>(); 
        Integer count = 1;
        while(count<20){
            randomNumbers.addAll(generateRandomNumbers(accountSize - randomNumbers.size()));
            List<Account> accounts = [Select Id,AccessPIN__c from Account where AccessPIN__c IN: randomNumbers];
            
            if(accounts.size() == 0){
                break;
            }else{
                count++;  
                integer index = -1;
                for(Account eachAccount : accounts){
                    index = randomNumbers.indexof(eachAccount.AccessPIN__c);
                    if(index >=0){   
                        randomNumbers.remove(index);    
                    }
                }
                if(randomNumbers.size() >= accountSize){
                    break;
                }
            }
        }
        
        for(Integer index = 0; index<accountsToBeProcessed.size();index++){
            accountsToBeProcessed[index].AccessPIN__c = randomNumbers[index];
        }
        
    }
    
    /*
     * Method Name:generateRandomNumbers
     * Description: This method is used to generate Random Numbers for the new Accounts
     * Params: Integer
     * Return Type: List<String>
     */ 
    private List<String> generateRandomNumbers(Integer accountSize){
        List<String> randomNumbers = new List<String>();
        for(Integer index = 0; index<accountSize+10;index++){
            Integer random = Crypto.getRandomInteger();
            // Adding a two level check to see if the Random No. generated is more than 6 digits
            if(random.format().length() < 6 ){
                random = Crypto.getRandomInteger();
                if(random.format().length() < 6 ){
                    random = Crypto.getRandomInteger();
                }
            }
            // Also, we require only +ve values. If negative, the random no is made +ve  
            if(random < 0){
                random = (random * -1);
            }
            String randomNumber = String.ValueOf(random);
            if(randomNumber.length()>=6){
                //though we are generating thrice, we are still getting strings with 
                //size less than 6. So this additional check.
                randomNumber = randomNumber.substring(0,6);
                randomNumbers.add(randomNumber);
            }
        }
        
        return randomNumbers;
    }
    
    /**
     * @author : Publicis.Sapient
     * @param : List<Account> newly created accounts
     * @description : This method sets address fields based on formula fields. 
     *      This allows field history tracking on the address fields.
     * @return : void
     */
    private void setAddressFields(List<Account> accounts) {
        for(Account account : accounts) {
            account.MailingAddress__c = String.isBlank(account.CalculatedMailingAddress__c) ? null : account.CalculatedMailingAddress__c.replace('_BR_ENCODED_', '\n');
        }
    }

    /**
     * @author : Publicis.Sapient
     * @param : List<Account> newly created accounts
     * @description : When Business account is created and company id prefix is empty, then
     *      get the highest company id, increment it by one and assign to the company.
     * @return : void
     */
    private void populateCompanyIdPrefix(List<Account> Accounts) {
        List<Account> accountsToBeUpdated = new List<Account>();
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        for(Account account : accounts) {
            //Business Account should be active, and EntityId__c is null then collect the account 
            if(account.BusinessStatus__c != 'Inactive' &&
               account.RecordTypeId == businessAccountRecordTypeId &&
               account.EntityId__c == null) {
                   accountsToBeUpdated.add(account);
               }
        }
        if(!accountsToBeUpdated.isEmpty()) {
            //When using in new sandbox, CompanyIdUnformattedPrefix will not be there in the system, then use label.
            Integer startingCompanyPrefix=Integer.valueOf(Label.EntityIdStartingNumber);
            //Get the hightest CompanyIdUnformattedPrefix from the system. Since ordering on text fields
            //will give alphabatically. EX: 2 as highest number over any thing that start with 1. So new field is used.
            List<Account> existingAccounts = [select CompanyIdUnformattedPrefix__c from Account where recordTypeId =:businessAccountRecordTypeId AND CompanyIdUnformattedPrefix__c != null order by CompanyIdUnformattedPrefix__c desc nulls last limit 1];
            if(existingAccounts != null && !existingAccounts.isEmpty()){
                startingCompanyPrefix = existingAccounts.get(0).CompanyIdUnformattedPrefix__c.intValue()+1;
            }
            for(Account accountToUpdate:accountsToBeUpdated) {
                //Populating each new account with an entity id and updated CompanyIdUnformattedPrefix
                accountToUpdate.CompanyIdUnformattedPrefix__c = startingCompanyPrefix;
                accountToUpdate.EntityId__c = String.valueof(startingCompanyPrefix);
                startingCompanyPrefix++;
            }
        }
    }

    /**
     * @author : Publicis.Sapient
     * @description : When a business account is updated verifies that, Once set Company prefix
     *     should not be changed ever again. If it is changed then throw error to that account.
     * @param : List<Account> newAccounts records that are updated
     * @param : Map<Id, Account__c> oldAccountsById old records for comparision
     * @return : void
     */
    private void checkForCompanyPrefixChange(List<Account> newAccounts, Map<Id, Account> oldAccountsById){
        Account existingAccount;
        Id businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        for(Account updatedAccount:newAccounts){
            existingAccount = oldAccountsById.get(updatedAccount.Id);
            if(updatedAccount.BusinessStatus__c != 'Inactive' &&
               updatedAccount.RecordTypeId == businessAccountRecordTypeId &&
               existingAccount.EntityId__c != null &&
               updatedAccount.EntityId__c != existingAccount.EntityId__c) {
                   //For an active business account, if entity id is modified, then throw error. 
                   updatedAccount.addError(Label.CompanyPrefixChangedError);
                   Logger.addDebugEntry(LoggingLevel.Error,'ID: '+ updatedAccount.Id+' '+Label.CompanyPrefixChangedError,'AccountHandler - checkForCompanyPrefixChange');
               }
        }
    }
}