/*************************************************************************************************
* This file is part of the Nebula Logger project, released under the MIT License.                *
* See LICENSE file or go to https://github.com/jongpie/NebulaLogger for full license details.    *
*************************************************************************************************/
@isTest
private class FlowLogEntry_Tests {

    @testSetup
    static void setupData(){
        LoggerSettings__c loggerSettings = LoggerSettings__c.getInstance();
        loggerSettings.LoggingLevel__c = 'FINEST';
        insert loggerSettings;
    }

    @isTest
    static void it_should_log_a_flow_log_entry() {
        FlowLogEntry logEntry = new FlowLogEntry();
        logEntry.flowName         = 'Fake flow for unit test';
        logEntry.loggingLevelName = 'DEBUG';
        logEntry.message          = 'Fake message for unit test';

        Test.startTest();
        FlowLogEntry.addFlowEntries(new List<FlowLogEntry>{logEntry});
        Logger.saveLog();
        Test.stopTest();

        List<LogEntry__c> logEntries = [SELECT Id, LoggingLevel__c, OriginLocation__c, OriginType__c FROM LogEntry__c];
        System.assertEquals(1, logEntries.size());

        LogEntry__c savedLogEntry = logEntries[0];
        System.assertEquals('Process Builder/Flow', savedLogEntry.OriginType__c);
        System.assertEquals(logEntry.flowName, savedLogEntry.OriginLocation__c);
        System.assertEquals(logEntry.loggingLevelName, savedLogEntry.LoggingLevel__c);
    }

}