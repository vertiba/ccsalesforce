/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : Batch class used to process data from Aumentum (stored in StagingAumentumBusiness__c) for businesses.
//      The Aumentum data is then compared against Account & Property records to find matches & map field values
//      Accounts & properties are then upserted automatically when possible - any failures will then be flagged as 'Failed' and
//      a user will have to manually resolve any issues/re-process the data
// 2020-07-04 :ASR 2366- updated the logic and moved to AumentumDataProcess class
//-----------------------------
public class AumentumDataNormalizeBatch implements Database.Batchable<sObject>, BatchableErrorHandler{ 
    
    // Constants
    private static final String LOG_LOCATION        = CCSFConstants.AUMENTUM_BATCH_NAME;
    private static final String FILTERS = 'IsDataClean__c = true and (Status__c = \'New\' OR Status__c =\'Reprocessed\')';
    private static final String STAGEQUERY = DescribeUtility.getDynamicQueryString('StagingAumentumBusiness__c', FILTERS);
    
       
    public Database.QueryLocator start(Database.BatchableContext context) {
        // collect the batches of records or objects to be passed to execute 
        Logger.addDebugEntry(STAGEQUERY,LOG_LOCATION+'- start method query');
        Logger.saveLog();      
        return Database.getQueryLocator(STAGEQUERY);
    }
    
    public void execute(Database.BatchableContext context, List<StagingAumentumBusiness__c> businessRecords){
        try{
            AumentumDataProccess.processData(businessRecords);
        }catch (Exception ex){
            Logger.addExceptionEntry(ex, 'StagingDataNormalizeBatch.execute(Database.BatchableContext context, List<StagingAumentumBusiness__c businessRecords)');
            throw ex;
        }
        Logger.addDebugEntry('Leaving the method', 'StagingDataNormalizeBatch.execute');
        Logger.saveLog();
    }
    
    public void finish(Database.BatchableContext context){
        string JobId = context.getJobId();
        List<AsyncApexJob> asyncList = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                                        TotalJobItems, CreatedBy.Email,ParentJobId
                                        FROM AsyncApexJob WHERE Id =:JobId];
        
        // Inserting a log for retrying purposes
        DescribeUtility.createApexBatchLog(JobId, AumentumDataNormalizeBatch.class.getName(), asyncList[0].ParentJobId, 
                                           false,'', asyncList[0].NumberOfErrors);
    }
    
    //-----------------------------
    // @author : Publicis.Sapient
    // @description : Batch Retry Handler Function, which will hold the error record(if any), for further processing.
    // @return :void
    //-----------------------------
    public void handleErrors(BatchableError error) {}
    
}