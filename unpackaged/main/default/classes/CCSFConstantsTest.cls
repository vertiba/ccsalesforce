/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis.Sapient
// @description : This class is used to test CCSFConstants class.
//-----------------------------
@isTest
public class CCSFConstantsTest {
    
    @isTest
    static void testConstants() {
        System.assertEquals('BusinessAccount', CCSFConstants.ACCOUNT.BUSINESS_RECORD_TYPE_API_NAME);
        System.assertEquals(Label.BatchAlreadyRunningMessage, CCSFConstants.BATCHMSG.BATCH_ALREADY_RUNNING_MSG);
        System.assertEquals(Label.LowValueAssesmentGeneratorMsg, CCSFConstants.BUTTONMSG.LOW_VALUE_ASSESMENT_ALREADY_GENERATED_RMSG);
        System.assertEquals('Low Value', CCSFConstants.NOTICE_TYPE_LOW_VALUE);
    }
}