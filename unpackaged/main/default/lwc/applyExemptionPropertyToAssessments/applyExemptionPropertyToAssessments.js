import { LightningElement,api } from 'lwc';
import fetchAssessments from '@salesforce/apex/ApplyExemptionsPropertyToAssesments.fetchAssessments';  
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveAssessments from '@salesforce/apex/ApplyExemptionsPropertyToAssesments.saveExemptionData';  

export default class ApplyExemptionPropertyToAssessments extends LightningElement {

    // Api Properties
    @api recordIdPropertyEvent;

    // Local properties
    selectedRecords;
    unchangedSelectedrecords;
    fetchedAssessments;
    currentStep = 'step-1';
    showTableOne = true;
    updatedRecord = {
        Id : null,
        PercentExempt__c : 0.0,
        ExemptAmountManual__c : 0.00,
        ExemptionPenalty__c : null
    }
    updatedRecords =[];

    steps = [
        { label: 'Select Assessments', value: 'step-1' },
        { label: 'Apply Exemption', value: 'step-2' }
    ];

    COLUMNS=[  
        {label:'Assessment Year',fieldName:'AssessmentYear__c', type:'text'},  
        {label:'Sequence Number',fieldName:'SequenceNumber__c', type:'number'},  
        {label:'Total Assessed Value',fieldName:'TotalAssessedValue__c', type:'currency'},
        {label:'Type',fieldName:'Type', type:'text'},
        {label:'Calculated Amount Exempt',fieldName:'CalculatedAmountExempt__c', type:'currency'},
        {label:'Status',fieldName:'Status', type:'text'},
        {label:'Case Number',fieldName:'CaseNumber', type:'text'} 
      ];

        connectedCallback(){
            const recordId = this.recordIdPropertyEvent;
            const recordInput = { recordId: recordId };
            fetchAssessments(recordInput)
            .then(result => {
                this.fetchedAssessments = result;
                this.error = undefined;
            })
            .catch(error => {
                this.error = error;
                this.fetchedAssessments = undefined;
            });
       } 

    handleOnClick(event){
        
        this.currentStep = event.target.dataset.id;
        this.showTableOne =  this.currentStep == "step-1" ? true : false;
        this.selectedRecords = undefined;
        //this.unchangedSelectedrecords = this.selectedRecords;
        this.updatedRecords = [];

    }
    
    handleNextClick() {

        this.currentStep = "step-2";
        this.showTableOne =  this.currentStep == "step-1" ? true : false;
        this.selectedRecords =  this.template.querySelector("lightning-datatable").getSelectedRows();
        //this.unchangedSelectedrecords = this.selectedRecords;
    }

    handleDataChange(event){
        
        var errorBoolean = false;
        var percentageError = false;
        var amountError = false;
        var updatedRecordConst = {};
        var updatedRecords = this.updatedRecords;
        updatedRecordConst.Id = event.target.dataset.id;

        this.selectedRecords.forEach(function(selectedrecord){
            if(selectedrecord.Id === updatedRecordConst.Id) {
                updatedRecordConst.PercentExempt__c = selectedrecord.PercentExempt__c !== undefined || selectedrecord.PercentExempt__c !== null ?  selectedrecord.PercentExempt__c : null;

                updatedRecordConst.ExemptAmountManual__c = selectedrecord.ExemptAmountManual__c !== undefined || selectedrecord.ExemptAmountManual__c !== null ?  selectedrecord.ExemptAmountManual__c : null;

                updatedRecordConst.ExemptionPenalty__c = selectedrecord.ExemptionPenalty__c !== undefined || selectedrecord.ExemptionPenalty__c !== null ?  selectedrecord.ExemptionPenalty__c : null;

                updatedRecordConst.TotalAssessedValue__c = selectedrecord.TotalAssessedValue__c !== undefined || selectedrecord.TotalAssessedValue__c !== null ?  selectedrecord.TotalAssessedValue__c : null;

                updatedRecordConst.NetAssessedValue__c = selectedrecord.NetAssessedValue__c !== undefined || selectedrecord.NetAssessedValue__c !== null ?  selectedrecord.NetAssessedValue__c : null;

                updatedRecordConst.Type = selectedrecord.Type !== undefined || selectedrecord.Type !== null ?  selectedrecord.Type : null;
                updatedRecordConst.IntegrationStatus__c = selectedrecord.IntegrationStatus__c !== undefined || selectedrecord.IntegrationStatus__c !== null ?  selectedrecord.IntegrationStatus__c : null;

                updatedRecordConst.Roll__c  = selectedrecord.Roll__c !== undefined || selectedrecord.Roll__c !== null ?  selectedrecord.Roll__c : null;

                updatedRecordConst.IsLocked__c = selectedrecord.isLocked__c !== undefined || selectedrecord.IsLocked__c !== null ?  selectedrecord.IsLocked__c : null;

                updatedRecordConst.CaseNumber = selectedrecord.CaseNumber !== undefined || selectedrecord.CaseNumber !== null ?  selectedrecord.CaseNumber : null;

                updatedRecordConst.RecordTypeId = selectedrecord.RecordTypeId !== undefined || selectedrecord.RecordTypeId !== null ?  selectedrecord.RecordTypeId : null;

                if(event.target.name === 'exemptPercent') {
                    errorBoolean = false;
                    updatedRecordConst.PercentExempt__c =event.target.value;
                    if( !isNaN(event.target.value) && parseInt(event.target.value) < 100 ) {
                        percentageError =false;
                    }else {
						if(event.target.value !=="") {
                            percentageError = true;
                        }
                    }
                    if(updatedRecords.length != 0 && updatedRecordConst.PercentExempt__c != '' && updatedRecordConst.PercentExempt__c !== undefined ) {
                        updatedRecords.forEach(function(record){
                            if(record.Id === updatedRecordConst.Id && record.ExemptAmountManual__c !== '' && record.ExemptAmountManual__c !== undefined 
                                && record.ExemptAmountManual__c != null ) {
                                errorBoolean = true;
                            }

                        });
                    }
                    else {
                        if(updatedRecordConst.PercentExempt__c != '' && updatedRecordConst.PercentExempt__c !==undefined && selectedrecord.ExemptAmountManual__c !== undefined && selectedrecord.ExemptAmountManual__c !== null) {
                        
                            updatedRecordConst.ExemptAmountManual__c = selectedrecord.ExemptAmountManual__c;
                            errorBoolean = true;
                        }
                    }
                }else if(event.target.name === 'exemptAmount') {
                    errorBoolean = false;
                    updatedRecordConst.ExemptAmountManual__c = event.target.value;
                    if(!isNaN(event.target.value)) {
                        amountError =false;
                    }else {
                        amountError = true;
                    }

                    if(updatedRecords.length != 0 && updatedRecordConst.ExemptAmountManual__c != '' && updatedRecordConst.ExemptAmountManual__c !== undefined ) {
                        updatedRecords.forEach(function(record){
                            if(record.Id === updatedRecordConst.Id && record.PercentExempt__c !== '' && record.PercentExempt__c !== undefined && record.PercentExempt__c != null ) {
                                errorBoolean = true;
                            }
                        });
                    }
                    else {
                        if(updatedRecordConst.ExemptAmountManual__c != '' && updatedRecordConst.ExemptAmountManual__c !==undefined && selectedrecord.PercentExempt__c !== undefined && selectedrecord.PercentExempt__c !== null) {
                        
                            updatedRecordConst.PercentExempt__c = selectedrecord.PercentExempt__c;
                            errorBoolean = true;
                        }
                    }
                }
            }
        });
        
        if(errorBoolean){
            this.showToast("Invalid Data ",'Either of Exempt percent and Exempt Amount can be filled',"error");
        }else if(percentageError) {
            this.showToast("Invalid Data ",'Exempt Percentage cannot be greater than 100% or it contains invalid data' ,"error");

        }else if(amountError) {
            this.showToast("Invalid Data ",'Amount can only be numeric' ,"error");
        }

        this.buildUpatedArray(updatedRecordConst,event.target.name);
    }

    onPenaltySelection(event) {

        const updatedRecord = {};
        updatedRecord.Id = event.detail.record.Id;    
        this.selectedRecords.forEach(function(selectedrecord){
            if(selectedrecord.Id === updatedRecord.Id) {
                updatedRecord.PercentExempt__c = selectedrecord.PercentExempt__c !== undefined || selectedrecord.PercentExempt__c !== null ?  selectedrecord.PercentExempt__c : null;
                updatedRecord.ExemptAmountManual__c = selectedrecord.ExemptAmountManual__c !== undefined || selectedrecord.ExemptAmountManual__c !== null ?  selectedrecord.ExemptAmountManual__c : null;
                updatedRecord.TotalAssessedValue__c = selectedrecord.TotalAssessedValue__c !== undefined || selectedrecord.TotalAssessedValue__c !== null ?  selectedrecord.TotalAssessedValue__c : null;
                updatedRecord.NetAssessedValue__c = selectedrecord.NetAssessedValue__c !== undefined || selectedrecord.NetAssessedValue__c !== null ?  selectedrecord.NetAssessedValue__c : null;
                updatedRecord.type = selectedrecord.Type !== undefined || selectedrecord.Type !== null ?  selectedrecord.Type : null;
                updatedRecord.IntegrationStatus__c = selectedrecord.IntegrationStatus__c !== undefined || selectedrecord.IntegrationStatus__c !== null ?  selectedrecord.IntegrationStatus__c : null;
                updatedRecord.CaseNumber = selectedrecord.CaseNumber !== undefined || selectedrecord.CaseNumber !== null ?  selectedrecord.CaseNumber : null;
                updatedRecord.RecordTypeId = selectedrecord.RecordTypeId !== undefined || selectedrecord.RecordTypeId !== null ?  selectedrecord.RecordTypeId : null;
                
                updatedRecord.roll__c  = selectedrecord.Roll__c !== undefined || selectedrecord.TyRoll__cpe !== null ?  selectedrecord.Roll__c : null;

                updatedRecord.isLocked__c = selectedrecord.isLocked__c !== undefined || selectedrecord.isLocked__c !== null ?  selectedrecord.isLocked__c : null;

                updatedRecord.ExemptionPenalty__c = event.detail.selectedRecordId; 
            }

        });

        this.buildUpatedArray(updatedRecord,'exemptPenalty');
        
    }
   
    buildUpatedArray(updateData,datasetName) {

        
        var toBeAdded = true; 
        // If else is used for the existing record check in updatedrecords array.
        if(this.updatedRecords.length == 0) {
            this.updatedRecords.push(updateData);
            toBeAdded = false;
        }else {
         
            this.updatedRecords.forEach(function(selectedrecord){
                if(selectedrecord.Id === updateData.Id) {
                    
                    if(updateData.PercentExempt__c == undefined && updateData.PercentExempt__c !='' && updateData.PercentExempt__c != null){
                        updateData.PercentExempt__c = selectedrecord.PercentExempt__c !== undefined || selectedrecord.PercentExempt__c !== null ?  selectedrecord.PercentExempt__c : null;
                    }
                    if(updateData.ExemptAmountManual__c == undefined && updateData.ExemptAmountManual__c !='' && updateData.ExemptAmountManual__c != null) {

                        updateData.ExemptAmountManual__c = selectedrecord.ExemptAmountManual__c !== undefined || selectedrecord.ExemptAmountManual__c !== null ?  selectedrecord.ExemptAmountManual__c : null;
                    }
                   
                    if(updateData.ExemptionPenalty__c == undefined && updateData.ExemptionPenalty__c !='' && updateData.ExemptionPenalty__c != null) {
                        updateData.ExemptionPenalty__c = selectedrecord.ExemptionPenalty__c !== undefined || selectedrecord.ExemptionPenalty__c !== null ?  selectedrecord.ExemptionPenalty__c : null;
                    }
                    if(datasetName === 'exemptPenalty') {
                        selectedrecord.ExemptionPenalty__c = updateData.ExemptionPenalty__c;
                    } else if(datasetName === 'exemptPercent') {
                        selectedrecord.PercentExempt__c = updateData.PercentExempt__c != 0 ? updateData.PercentExempt__c : null ;    
                    } else if(datasetName === 'exemptAmount') {
                        
                        selectedrecord.ExemptAmountManual__c = updateData.ExemptAmountManual__c != 0 ? updateData.ExemptAmountManual__c : null; 
                    }
                    toBeAdded = false;
                    return;
                }
            });
        }
       
        if(toBeAdded) {    
            this.updatedRecords.push(updateData);
        }
      
    }   

    showToast(title,message,variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    handleSaveData(event){

        
        var errorCaseNumbers = '';
         
        this.updatedRecords.forEach(function(selectedrecord){
            if(selectedrecord.PercentExempt__c !== undefined 
                && selectedrecord.PercentExempt__c !== null
                && selectedrecord.ExemptAmountManual__c !== undefined 
                && selectedrecord.ExemptAmountManual__c !== null ) {

                    errorCaseNumbers += selectedrecord.CaseNumber +',';
            }
           
            else if((selectedrecord.PercentExempt__c!= null && selectedrecord.PercentExempt__c != undefined && isNaN(selectedrecord.PercentExempt__c)) ||  parseInt(selectedrecord.PercentExempt__c) > 100) { 

                errorCaseNumbers += selectedrecord.CaseNumber +',';
            }
            else if(selectedrecord.ExemptAmountManual__c!= null && selectedrecord.ExemptAmountManual__c != undefined && isNaN(selectedrecord.ExemptAmountManual__c)) {
                errorCaseNumbers += selectedrecord.CaseNumber +',';
            }

        });
        errorCaseNumbers = errorCaseNumbers.slice(0, -1);
        if(errorCaseNumbers !== '') {
            this.showToast("Save Data",'Invalid Data, Error Case Numbers ::'+ errorCaseNumbers,"error");
            errorCaseNumbers = '';
        }else {
            saveAssessments({jsonData : JSON.stringify(this.updatedRecords), propertyExemptionId : this.recordIdPropertyEvent })
            .then(result => {
                
                this.showToast("Save Data","Records Saved Successfuly","success");
                this.updatedRecords = [];
                this.fetchedAssessments = undefined;
                errorCaseNumbers='';

                const closequickaction = new CustomEvent("closequickaction", {
                    
                  });
                  this.dispatchEvent(closequickaction);
                
            })
            .catch(error => {
                
                this.error = error;
                this.showToast("Save Data",this.error,"error");
            });
        }

    }
    
}