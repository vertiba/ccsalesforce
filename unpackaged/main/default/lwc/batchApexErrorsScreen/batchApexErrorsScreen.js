/* Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d */
import { LightningElement,track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import fetchFailures from '@salesforce/apex/BatchableErrorsComponentController.failedJobs';
import retryBatch from '@salesforce/apex/BatchableErrorsComponentController.retryJob';
import fetchAsyncApex from '@salesforce/apex/BatchableErrorsComponentController.fetchCurrentApexJobs';
import { NavigationMixin } from 'lightning/navigation';

export default class brf_batchErrors extends NavigationMixin(LightningElement) {

    // Column Characteristic for the Batch Job Failures Datatables
    columns= [
        { label: 'Job Apex Class', fieldName: 'JobApexClass__c', type: 'text'},
        { label: 'Job Creation Date', fieldName: 'JobCreatedDate__c', type: 'date'},
        { label: 'Job Errors', fieldName: 'JobErrors__c', type: 'number',
                cellAttributes: {
                     alignment: 'left' 
                    }
                },
        { label: 'View', type: "button", initialWidth: 135, 
                typeAttributes: 
                { label: 'View Details', name: 'view_details', title: 'Click to View Details'}
        },
        { type: 'action', 
            typeAttributes: {
                 rowActions: [
                     { label: 'Retry', name: 'retry' }
                    ] 
                } 
            }
    ];

    // Column Characteristic for the Async Apex Job Datatables
    apexColumns= [
        { label: 'Submitted Date', fieldName: 'CreatedDate', type: 'date'},
        { label: 'Submitted By', fieldName: 'CreatedBy', type: 'text',sortable: true},
        { label: 'Status', fieldName: 'Status', type: 'text',sortable: true},
        { label: 'Status Detail', fieldName: 'ExtendedStatus', type: 'text'},
        { label: 'Total Batches', fieldName: 'TotalJobItems', type: 'Number', 
          cellAttributes: {
            alignment: 'left' 
          }
        },
        { label: 'Batches Processed', fieldName: 'JobItemsProcessed', type: 'Number', 
          cellAttributes: {
            alignment: 'left' 
          }
        },
        { label: 'Failures', fieldName: 'NumberOfErrors', type: 'Number', 
          cellAttributes: {
            alignment: 'left' 
          }
        },
        { label: 'Apex Class', fieldName: 'ApexClassName', type: 'text',sortable: true}
    ];

    failedJobs = [];
    currentApexJobs = [];

    // This function fires on Page Load
    connectedCallback(){
        Promise.all([
            this.loadSection(),
            this.loadAsyncApex()
        ]);
    }

    // Fetch all the current Async Apex Jobs
    loadAsyncApex(){
        fetchAsyncApex().then(data => {
            console.log('data',data)
            let currentData = [];
            data.forEach(row => {
                let rowData = {};
                rowData.CreatedDate = row.CreatedDate;
                rowData.CreatedBy = row.CreatedBy.Name;
                rowData.Status = row.Status;
                rowData.ExtendedStatus = row.ExtendedStatus;
                rowData.TotalJobItems = row.TotalJobItems;
                rowData.JobItemsProcessed = row.JobItemsProcessed;
                rowData.NumberOfErrors = row.NumberOfErrors;
                rowData.ApexClassName = row.ApexClass.Name;
                currentData.push(rowData);
            });
            this.currentApexJobs = currentData;
        }).catch(error =>{
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error loading Async Apex Jobs",
                    message: error.message,
                    variant: "error"
                })
            );
        });
    }

    // Fetch all the failures
    loadSection(){
        fetchFailures().then(result => {
            this.failedJobs = result;
        }).catch(error =>{
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error loading Data",
                    message: error.message,
                    variant: "error"
                })
            );
        });
    }

    // This function handles both the View Details button and the retry feature
    handleRowAction(event){
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        
        if(actionName == 'view_details'){
            this[NavigationMixin.Navigate]({  
                type: 'standard__recordPage',  
                attributes: {  
                    recordId: row.Id,  
                    objectApiName: 'CCSF_BRF_BatchApexErrorLog__c',  
                    actionName: 'view'  
                }  
            });  
        }else if(actionName == 'retry'){
            retryBatch({retryJobId: row.JobId__c}).then(result =>{
                this.failedJobs = result;
                this.dispatchEvent(
                    new ShowToastEvent({
                        message: "Job retry for '"+row.JobApexClass__c+"' has been submitted.",
                        variant: "success"
                    })
                );
            }).then(() => {
                this.loadAsyncApex();
            }).catch(error =>{
                console.log('error-->',error)
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: "Error while retrying Job.",
                        message: error.body.message,
                        variant: "error"
                    })
                );
            })
        }
        
    }
}