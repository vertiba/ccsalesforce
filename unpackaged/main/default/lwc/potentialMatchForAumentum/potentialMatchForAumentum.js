/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
//-----------------------------
// @author : Publicis Sapient 
// @description : This class is used to show user correction records between StagingAumentum records, Account, Property .
//-----------------------------
import {
    LightningElement,
    api,
    wire,
    track
} from 'lwc';
// for record information
import {
    getRecord
} from 'lightning/uiRecordApi';
import {
    showToast
} from "c/showToastUtility";
//Apex Methods
import getDataForSelection from "@salesforce/apex/PotentialMatchController.getDataForSelection";
import updateNewChanges from "@salesforce/apex/PotentialMatchController.updateNewChanges";

//label
import successReviewProcessorMessage from '@salesforce/label/c.SuccessReviewProcessorMessage';
import noRecordReviewProcessorMessage from '@salesforce/label/c.NoRecordReviewProcessorMessage';
import noAccessMessage from '@salesforce/label/c.AumentumNoPermissionMessage';

// Custom permission ASR-8974
import hasPermission from '@salesforce/customPermission/AumentumProcessPermission'

// Error Handler Utility
import {
    reduceErrors
} from 'c/errorHandlerUtility';

// Bug 10441
const FIELDS = ['StagingAumentumBusiness__c.Status__c','StagingAumentumBusiness__c.MappedAccount__c'];
export default class PotentialMatchForAumentum extends LightningElement {
    @api recordId;
    @track potentialMatches;
    @track stagingAumentumBusiness;
    @track stagingAumentumStatus;
    @track stagingMappedAccount;
    pageErrors;
    @track changesForSaving;
    disableSave = false;
    // Custom label
    label = {
        successReviewProcessorMessage,
        noRecordReviewProcessorMessage,
        noAccessMessage
    }

    // ASR-8974 For Custom permission access 
    get accessPermission() {
        return hasPermission;
    }

    // Getting Staging Status Information Using LDS
    @wire(getRecord, {
        recordId: "$recordId",
        fields: FIELDS
    })
    wiredRecord({
        error,
        data
    }) {
        if (error) {
            let message = "Unknown Error";
            if (Array.isArray(error.body)) {
                message = error.body.map((e) => e.message).join(", ");
            } else if (typeof error.body.message === "string") {
                message = error.body.message;
            }
            showToast(this, 'Unknown Error', message, 'error', 'pester');
        } else if (data) {
            this.stagingAumentumBusiness = data;
            this.stagingAumentumStatus = this.stagingAumentumBusiness.fields.Status__c.value;
            this.stagingMappedAccount  = this.stagingAumentumBusiness.fields.MappedAccount__c.value;
           // Bug - 10441 Button disable if not warning
            if(this.stagingMappedAccount != null || this.stagingAumentumStatus != "Warning" ){               
                this.disableSave = true;
            }
        }
    }


    // On load get value of table for display
    connectedCallback() {

        getDataForSelection({
                parentRecordId: this.recordId
            })
            .then((data) => {
                this.potentialMatches = JSON.parse(JSON.stringify(data));
                this.pageErrors = undefined;
            })
            .catch((error) => {
                this.pageErrors = error;
                let errorMessage = reduceErrors(error);
                showToast(this, 'Error Occured while loading', errorMessage[0],'warning', 'dismissible');
                this.potentialMatches = undefined;
            });

    }

    // Make checkbox checked
    storeChange(event) {
        let selectedObjectName = event.target.getAttribute('data-objectname');
        let selectedMatchId = event.target.getAttribute('data-id');
        if (selectedObjectName === 'Property') {
            this.template.querySelectorAll('lightning-input').forEach(element => { // get all the checkbox from template                   
                if (selectedMatchId != element.getAttribute('data-id')) {
                    element.checked = false;
                }
            });
        }
    }

    // Saved checked option and send to Server
    saveChanges() {
        this.changesForSaving = [];
        let selectedRows = this.template.querySelectorAll('lightning-input');
        // based on selected row getting values of the changes
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                this.changesForSaving.push({
                    ObjectId: selectedRows[i].value,
                    MatchId: selectedRows[i].dataset.id,
                    ObjectName: selectedRows[i].dataset.objectname
                })
            }
        }
        // Calling Server- Apex
        updateNewChanges({
                newChanges: JSON.stringify(this.changesForSaving),
                parentId: this.recordId
            })
            .then((result) => {
                showToast(this, 'Success', 'Saved Changes successfully', 'success', 'pester');
                this.pageErrors = undefined;
                window.location.reload();
                // Bug 10441 - Disable Save button if status not warning
                if(this.stagingMappedAccount != null || this.stagingAumentumStatus != "Warning"){
                    this.disableSave = true;
                }
        
            })
            .catch((error) => {
                this.pageErrors = error;
                let errorMessage = reduceErrors(error);
                showToast(this, 'Error Occured', errorMessage[0],'error', 'sticky');               
            });
    }

}