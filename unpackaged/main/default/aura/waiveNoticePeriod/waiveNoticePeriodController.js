/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    confirmAction : function(component, event, helper){
        var name = component.find("nameInput").get("v.value");
        if(name === undefined){
            helper.showToast('Warning','Warning','Please provide correct input values.');   
        }
        else if(helper.validateFields(component)){
            helper.confirmAction(component);
        }
    }
})