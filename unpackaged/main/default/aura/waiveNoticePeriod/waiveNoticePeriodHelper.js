/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    validateFields : function(component){
        var valid = component.find("nameInput").checkValidity();
        if(!valid) this.showToast('Warning','Warning','Please provide correct input values.');
        return valid;
    },
    confirmAction : function(component, event, helper) {
        var name = component.find("nameInput").get("v.value");
        var action = component.get("c.updateWaiverNoticeDetails");
        action.setParams({
            'recordId' : component.get('v.recordId'),
            'DeclarationName' : name
        });
        action.setCallback(this,function(Response){
            var state = Response.getState();
            if(state ==='SUCCESS'){
                this.showToast('Success','Success','This has been submitted successfully');
                $A.get('e.force:refreshView').fire();
                component.set('v.isSaved', true);
            }else if(state ==='ERROR'){
                let errors = Response.getError();
                this.showToast('Error','Error',errors[0].message);
            }
        }); 
        $A.enqueueAction(action); 
    },
    showToast : function(title,type,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title, 
            "message": message,
            "type":type
        });
        toastEvent.fire();
    }
})