/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    onInit : function(component){
        var recordTypeLabel = component.get('v.formDescription');        
        var action= component.get('c.getRecordTypeId');
        var dowloadButtonLink = component.get('v.downloadLink');
        
        action.setParams({
            objectApiName: 'Statement__c',
            recordTypeLabel: recordTypeLabel
        });        
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state ==='SUCCESS'){
                var result = response.getReturnValue();
                component.set("v.recordTypeIdValue",result);
                this.disableDowloadButton(component,dowloadButtonLink);
            }else{
                helper.showTost('Error','error','Record Type Id is Null','sticky');
            }
            
        });
        $A.enqueueAction(action);        
        
        
    },
    
    /* To Launch a form*/
    openFormHelper : function(component, event){
        var recordTypeId = component.get('v.recordTypeIdValue');
        component.find("recordHandler").getNewRecord(
            "Statement__c",      // sObject type (entityAPIName)
            recordTypeId, // recordTypeId
            false,     // skip cache?
            $A.getCallback(function () {
                var formRecord = component.get('v.formRecord');
                
                var recordHandler = component.find("recordHandler");
                component.find("recordHandler").saveRecord($A.getCallback(function (saveResult) {
                    if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                      
                        var navEvent = $A.get('e.force:navigateToSObject');
                        navEvent.setParams({
                            recordId: component.get('v.formRecord').Id
                        });
                        navEvent.fire();
                    } else if (saveResult.state === "INCOMPLETE") {
                        console.log("User is offline, device doesn't support drafts.");
                    } else if (saveResult.state === "ERROR") {
                        console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                    } else {
                        console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                    }
                }));
            })
        ); 
        
    },
    
    /* to hide dowload button */
    disableDowloadButton : function(component, dowloadButtonLink){ 
        var isEnabled = true;
        if(dowloadButtonLink == '' || dowloadButtonLink == undefined){          
            isEnabled = false;
        }
        if(isEnabled == false){
            component.set('v.enableDowloadLink',false);
          }        
        
    },
        
    downloadFormHelper : function(component){
        var downloadLink = component.get('v.downloadLink');
          window.open(downloadLink, '_blank');
    },
    
    showToast : function(title,type,message, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,      
            "message": message,
            "mode": mode
        });
        toastEvent.fire();	
    },
})