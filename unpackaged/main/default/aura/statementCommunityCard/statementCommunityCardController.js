/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit:function(component,event,helper){       
        helper.onInit(component);     // Calling on Load  
    },
    
    openForm : function(component,event,helper){
     
        helper.openFormHelper(component,event); // Open Launch Button
             
    },
    
    downloadForm: function (component, event, helper) {
        
        helper.downloadFormHelper(component); // Download the Form       
       
    }
});