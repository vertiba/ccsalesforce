/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit: function (component, event, helper) {
        component.set("v.showSpinner", false);
    },
    reloadTotalRelatedData: function (component, event, helper) {
        debugger;
        component.set("v.statementReportedAssetsData", []);
        //component.set("v.showSpinner",true);
        var action = component.get("c.getStatementReportedAssets");
        var actParams = {
            'recordId': component.get('v.recordId')
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if (results != undefined) {
                    var result = response.getReturnValue();
                    component.set("v.statementReportedAssetsData", result);                    
                    var isUnMatched = false;
                    for (var i = 0; i < result.length; i++) {
                        console.log('doinit+++ ' + JSON.stringify(result[i].IsMatchedAmount__c));
                        if (!result[i].IsMatchedAmount__c) {
                            isUnMatched = true;
                            break;
                        }
                    }
                    if (isUnMatched) {
                        component.set("v.unSavedChangesExist", true);
                    } else {
                        component.set("v.unSavedChangesExist", false);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    }
})