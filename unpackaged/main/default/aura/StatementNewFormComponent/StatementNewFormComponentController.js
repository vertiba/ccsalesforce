({
    doInit : function(component, event, helper) {
        //This piece of code will get the current recordTypeId from the pageReference.
        var recordTypeId = component.get( "v.pageReference" ).state.recordTypeId;
        if(recordTypeId === undefined) {
            var action = component.get("c.getDefaultRecordType");
            action.setCallback(this, function (response) {
               
                if (response.getState() === "SUCCESS") {
                    var returnValue = response.getReturnValue(); 
                    recordTypeId = returnValue;
                    component.set("v.recTypeValue",recordTypeId);
                }
        });
        $A.enqueueAction(action);
           
        }
       
        component.set("v.recTypeValue",recordTypeId);
        component.find("propertyfield").set("v.value", "");
    },
    
    handlePageChange : function(component, event, helper) {
        var recordTypeId = component.get( "v.pageReference" ).state.recordTypeId;
        //Assigning the fields as null because on change of the page, it was holding the previous value.
        component.set("v.recTypeValue",recordTypeId);
        component.find("propertyfield").set("v.value", "");
        component.find("formfield").set("v.value", "");
        component.find("vipfield").set("v.value", "");
        
    },
    handleOnLoad : function(component, event, helper) {
        component.set("v.showSpinner", false); 
    },
    
    handleOnSubmit : function(component, event, helper) {
        
        /*Since after saving the File Date field, was still getting the 
        	previous value on the creation of new record.*/
        component.find("fileDate").set("v.value", "");
        component.set("v.showSpinner", true);
    },
    
    handleOnSuccess : function(component, event, helper) {
        var params = event.getParams();
        component.set('v.recordId', params.response.id);
        //Redirect to detail page on success
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The statement record has been created.",
            "type": "success"
        });
        toastEvent.fire();
        
       var workspaceAPI = component.find("workspace");
         workspaceAPI.getFocusedTabInfo().then(function(response) {
          var focusedTabId = response.tabId;
             debugger;
            workspaceAPI.closeTab({tabId: focusedTabId});
       })
        .catch(function(error) {
            console.log('error');
        });
        
        var payload = event.getParams().response;
        var recId = JSON.stringify(payload);
        var workspaceAPI2 = component.find("workspace");
        workspaceAPI2.openTab({
            recordId: payload.id,
            focus: true
        }).then(function(response) {
            workspaceAPI2.focusTab({tabId : response});
        })
        .catch(function(error) {
            console.log(error);
        });
        event.preventDefault();
    },
    
    handleOnError : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error',
            message:'There is an error.',
            type: 'error'
        });
        toastEvent.fire();
    },
    onClickCancel : function(component, event, helper) {
        component.find("fileDate").set("v.value", "");
        var action = component.get("c.getListViews");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var listviews = response.getReturnValue();
                var navEvent = $A.get("e.force:navigateToList");
                navEvent.setParams({
                    "listViewId": listviews.Id,
                    "scope": "Statement__c"
                });
                navEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
})