({
	generateLowValueAssessements : function(component,event) {
        
        var action = component.get("c.checkPermission");
        action.setParams({
            'recordId' : component.get('v.recordId')
        });
         action.setCallback(this,function(serverResponse){
            var state = serverResponse.getState();
            if(state ==='SUCCESS'){
                var response = serverResponse.getReturnValue();
                 component.set('v.serverResponse', response);  
            }else{
               var errorMsg = ''; 
               var errors = serverResponse.getError();
               if (errors && errors[0] && errors[0].message) {
                   errorMsg = errors[0].message;                    
               } else {
                   errorMsg = "Unknown error. Please try again after some time.";
               }
                component.find('notifLib').showNotice({
                    "variant": "error",
                    "header": "Error!",
                    "message": errorMsg                    
                });
                
                $A.get("e.force:closeQuickAction").fire();
            }
            
        });
		$A.enqueueAction(action);
	},
    
    confirm : function(component,event) {
       
        var action= component.get("c.runLowValueAssessmentBulkGenerator");
        action.setCallback(this,function(serverResponse){
            var state = serverResponse.getState();
            if(state === "SUCCESS"){
                var response = serverResponse.getReturnValue();
                if(response.isSuccess){
                    component.find('notifLib').showNotice({
                        "variant": 'info',
                        "header": 'Alert',
                        "message": response.message 
                    });  
                }else{
                     component.find('notifLib').showNotice({
                        "variant": 'warning',
                        "header": 'Warning',
                        "message": response.message 
                    }); 
                }
             $A.get("e.force:closeQuickAction").fire();
            }else{
                var errorMsg = ''; 
                var errors = serverResponse.getError();
                if (errors && errors[0] && errors[0].message) {
                    errorMsg = errors[0].message;                    
                } else {
                    errorMsg = "Unknown error. Please try again after some time.";
                }
                component.find('notifLib').showNotice({
                    "variant": "error",
                    "header": "Error!",
                    "message": errorMsg                    
                });
            }
            });
 
        	$A.enqueueAction(action);
    }
})