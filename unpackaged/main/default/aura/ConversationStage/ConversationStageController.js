/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        component.set('v.data',[]);
        // Setting up Page Reference from other tab
        var pageReference = component.get("v.pageReference");
        component.set("v.objectName", pageReference.state.objectName);
        component.set("v.objAttributeParam", pageReference.state.objAttributeParam);
        component.set("v.filter", pageReference.state.filter); 
        helper.doInitHelper(component); 
    },
    displayExceptions: function (component, event, helper) {
        helper.displayExceptionsHelper(component, event);
    },
    statusChange : function(component,event,helper){
        component.set("v.data",[]);
        helper.statusChangeHelper(component, event);
    },
    changeInDate :function (component,event,helper){
        component.set("v.data",[]);
        helper.changeInDateHelper(component, event);        
    },
    handleNext : function(component,event, helper){
        helper.handleNextHelper(component, event);
    },    
    handlePrevious : function(component,event, helper){
        helper.handlePreviousHelper(component, event);        
    },    
    // To display Failed Message
    openFailurePopup : function(component,event, helper){
        var failureMessage= event.currentTarget.getAttribute('data-failureMessage');
        helper.showToast(component, 'Failure Message', failureMessage, 'sticky', 'warning');        
    },
    // to replay the batch
    replay : function(component,event,helper){
        helper.replayAction(component,event);
    }
})