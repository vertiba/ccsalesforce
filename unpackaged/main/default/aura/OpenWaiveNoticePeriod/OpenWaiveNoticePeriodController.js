/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit: function(component, event, helper){
        var action = component.get("c.getWaiverValue");
        action.setParams({
            'recordId' : component.get('v.recordId'),
        });
        action.setCallback(this,function(Response){
            var state = Response.getState();
            if(state ==='SUCCESS'){
                component.set('v.case',Response.getReturnValue());
                if((Response.getReturnValue().Type == 'Escape' || Response.getReturnValue().Type == 'Supplemental') && Response.getReturnValue().DeclarationName__c == null && Response.getReturnValue().Status == 'Closed' && Response.getReturnValue().SubStatus__c == 'Noticing'){
                    component.set('v.isValid',true);
                }
                else{
                    component.set('v.isValid',false);
                    var staticLabel = $A.get("$Label.c.DeclarationNameAvailable"); 
                    component.set('v.warningMessage', staticLabel); 
                }
            }
        });
        $A.enqueueAction(action);  
    }
})