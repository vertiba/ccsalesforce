({
    handleOnLoad : function(component, event, helper) {
        component.set("v.showSpinner", false); 
    },
    doInit : function(component, event, helper) {
       /* Checking Access of Statement Record Type
          in the Apex and returning it.*/
          helper.checkRecordTypeAccess(component, event, helper);
    },
     handleOnSubmit : function(component, event, helper) {
        component.set("v.showSpinner", true); 
    },
    handleOnSuccess : function(component, event, helper) { 
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The statement record has been edited.",
            "type": "success"
        });
        toastEvent.fire();
        event.preventDefault();
        //Redirect to detail page on success
	 var recId = component.get("v.recordId");
        var navService = component.find("navService");
    
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": recId,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
        /*The workspaceApI is used for 
          controlling workspace tabs and subtabs in a Lightning console app.*/
          var workspaceAPI = component.find("workspace");
         workspaceAPI.getFocusedTabInfo().then(function(response) {
          var focusedTabId = response.tabId;
             debugger;
            workspaceAPI.closeTab({tabId: focusedTabId});
       })
        .catch(function(error) {
            alert('error');
        });
    },
    handleOnError : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error',
            message:'There is an error.',
            type: 'error'
        });
        toastEvent.fire();
    },
    onClickCancel : function(component, event, helper) {
		component.set("v.openModal", false);
        var recId = component.get("v.recordId");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId,
            "slideDevName": "detail"
        });
        navEvt.fire();
        //Invoking WorkspaceAPI on Cancel to close the subtabs in Console App.
         var workspaceAPI = component.find("workspace");
         workspaceAPI.getFocusedTabInfo().then(function(response) {
          var focusedTabId = response.tabId;
             debugger;
            workspaceAPI.closeTab({tabId: focusedTabId});
       })
        .catch(function(error) {
            console.log('error');
        });
    }
})