/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	updateParentIdWithFeinStatus : function(component, event) {
        var fields = event.getParam('fields');
        var action = component.get("c.updateParentIdWithFeinStatus");
        var actParams = {
            'accountId': component.get('v.recordId'),
            'parentAccountId': fields.ParentId,
            'feinStatus' : fields.FEINStatus__c
        }
        action.setParams(actParams);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                $A.get('e.force:closeQuickAction').fire();
                this.showToast(component, 'SUCCESS', $A.get("$Label.c.SuccessfullyLinkedAccounts"),'dismissible', 'success');
                $A.get('e.force:refreshView').fire();
            } else {
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                component.set('v.disabled', false);
                component.set('v.showSpinner', false);
                this.showToast(component, 'ERROR', message,'sticky', 'error');
            }
        });
        $A.enqueueAction(action);  
	},
    showToast : function(component, title, messageToShow, mode, typeofMsg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": mode,
            "type": typeofMsg,
            "title": title,
            "message": messageToShow
        });
        toastEvent.fire();
    }
})