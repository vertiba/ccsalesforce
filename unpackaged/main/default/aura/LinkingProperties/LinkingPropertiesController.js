/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	handleSuccess : function(component, event, helper) {
		$A.get('e.force:closeQuickAction').fire();
        $A.get('e.force:refreshView').fire();
	},
    closeModel : function(component, event, helper) {
		$A.get('e.force:closeQuickAction').fire();
	},
    handleSubmit : function(component, event, helper) {
        event.preventDefault(); 
        component.set('v.disabled', true);
        component.set('v.showSpinner', true);
        helper.updateParentIdWithFeinStatus(component, event);
	},
    handleLoad : function(component, event, helper){
        var recordUI = event.getParam("recordUi");
        var feinStatus = recordUI.record.fields["FEINStatus__c"];
        var parentRecord = recordUI.record.fields["ParentId"].value
        if(!(parentRecord === null || parentRecord === '') 
           && !(feinStatus === null || feinStatus === '')){
            //If both the values are not empty then disable save.
            component.set('v.disabled', true);
            helper.showToast(component, 'WARNING', $A.get("$Label.c.ParentAccountFeinAlreadySetWarningMessage"),'sticky', 'warning');
        }
        component.set('v.showSpinner', false);
    }
})