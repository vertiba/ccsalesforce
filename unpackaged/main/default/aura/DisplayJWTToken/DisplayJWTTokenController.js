/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	doInit : function(component, event, helper) {
		var action = component.get("c.generateJWTToken");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                var result =response.getReturnValue();
                component.set('v.jwtToDisplay',result);
            } else {
                let errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    component.set('v.jwtToDisplay',errors[0].message);
                } else {
                    component.set('v.jwtToDisplay',errors);
                }
            }
        });
        $A.enqueueAction(action);  
	}
})