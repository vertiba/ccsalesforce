/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        component.set('v.data',[]);
        // Setting up Page reference for tab
        var pageReference = component.get("v.pageReference");
        component.set("v.objectName", pageReference.state.objectName);
        component.set("v.objAttributeParam", pageReference.state.objAttributeParam);
        component.set("v.filter", pageReference.state.filter); 
        component.set('v.selectedDate',$A.localizationService.formatDate(new Date(), "YYYY-MM-DD"));
        helper.doInitHelper(component);        
    },
    changeInDate : function(component,event,helper){
        component.set('v.data',[]);
        helper.changeInDateHelper(component,event);
    },
    handleNext : function(component,event, helper){
        helper.handleNextHelper(component,event);
    },
    handlePrevious : function(component,event, helper){
        helper.handlePreviousHelper(component,event);
    },
    // To show exception message 
    showExceptionMessage : function(component,event, helper){
        var errorMessage = event.currentTarget.getAttribute('data-exceptionMessage');
        helper.showToast(component, 'Exception Message Details', errorMessage, 'sticky', 'warning');
    }
})