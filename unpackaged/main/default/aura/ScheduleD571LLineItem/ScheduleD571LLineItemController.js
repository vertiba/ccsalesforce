/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        var apiFields = [];
        component.set("v.apiFields",apiFields);
        var records = component.find('articleOne');
        for(var cmp in records) {
            $A.util.toggleClass(records[cmp], 'slds-show');
            $A.util.toggleClass(records[cmp], 'slds-hide');
        }
    },
    sectionOne : function(component, event, helper) {
        var records = component.find('articleOne');
        for(var cmp in records) {
            $A.util.toggleClass(records[cmp], 'slds-show');
            $A.util.toggleClass(records[cmp], 'slds-hide');
        } 
    },
    createNewRecord: function (component, event, helper) {
        //debugger;
        component.set('v.showModal', true);
        var objectApi = 'ReportedAssetSchedule__c';
        var comparatorType ;
        var acquistionYear = component.get("v.acquistionYear");
        if("LineItemToStatementComparator" === component.get("v.comparatorType")) {
            comparatorType = "CreateReportedAssetSchedule";
        }
        var parentId = component.get("v.parentId");
        var componentAttributes = {
            "comparatorType": comparatorType,
            "parentId": parentId,
            "isCommunityPage": true,
            "objectAPI": objectApi,
            "comparatorType":comparatorType,
            "acqYear": acquistionYear
        };
        $A.createComponent(
            "c:CreateNewLineItem",
            componentAttributes,
            function (createRecordModal, status, errorMessage) {
                if (status === "SUCCESS") {
                    var targetCmp = component.find('recordCreatorModal');
                    var body = targetCmp.get("v.body");
                    body.push(createRecordModal);
                    targetCmp.set("v.body", body);
                    component.set('v.showModal', true);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    editRecord: function (component, event, helper) {
        debugger;
        component.set('v.showModal', true);
        var objectApi = 'ReportedAssetSchedule__c';
        var editRecordId = event.getSource().get("v.name");        
        var comparatorType ;    
        if("LineItemToStatementComparator" === component.get("v.comparatorType")) {
            comparatorType = "CreateReportedAssetSchedule";
        }
        var parentId = component.get("v.parentId");               
        var componentAttributes = {
            "comparatorType": comparatorType,
            "parentId":parentId,
            "editRecordId": editRecordId,
            "isCommunityPage": true,
            "objectAPI": objectApi,           
            "comparatorType":comparatorType              
        };
        $A.createComponent(
            "c:CreateNewLineItem",
            componentAttributes,
            function (createRecordModal, status, errorMessage) {
                if (status === "SUCCESS") {
                    var targetCmp = component.find('recordCreatorModal');
                    var body = targetCmp.get("v.body");
                    body.push(createRecordModal);
                    targetCmp.set("v.body", body);
                    component.set('v.showModal', true);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    closeModal: function (component, event, helper) {
        //helper.getRecordsToDisplay(component);
        var divComponent = component.find('recordCreatorModal');
        divComponent.set("v.body", []);
        component.get('v.showModal', false);
    },
})