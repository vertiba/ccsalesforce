/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	doInit : function(component, event, helper) {
		component.set('v.showSpinner', true);
        component.set('v.reverseTrendingAlreadyApplied',false);
        helper.getAssetClassificationPicklists(component);
	},
    handleAssetClassificationChange : function(component, event, helper) {
		component.set('v.showSpinner', true);
        //Clearing values once asset classification is changed. 
        component.set('v.selectedSubClassification','');
        component.set("v.assetSubClassifications", []);
        component.set('v.assessmentLineItems',[]);
        component.find('toYear').set('v.value','');
        component.find('fromYear').set('v.value','');
        component.set('v.reverseTrendingAlreadyApplied',false);
        helper.getSubAssetClassificationPicklists(component);
        helper.getRelatedAssessmentLineItems(component);
	},
    handleAssetSubClassificationChange :  function(component, event, helper) {
		component.set('v.showSpinner', true);
        //Clearing values once asset classification is changed. 
        component.set('v.assessmentLineItems',[]);
        component.set('v.reverseTrendingAlreadyApplied',false);
        component.find('toYear').set('v.value','');
        component.find('fromYear').set('v.value','');
        helper.getRelatedAssessmentLineItems(component);
	},
    calculateReverseTrending : function(component, event, helper){
        if(helper.validateFields(component)){
            component.set('v.showSpinner', true);
            helper.calculateReverseTrending(component);
        }
    },
    saveReverseTrendingValues : function(component, event, helper){
        component.set('v.showSpinner', true);
        helper.saveReverseTrendingValues(component, true);
    }, 
    removeReverseTrendingValues : function(component, event, helper){
        component.set('v.showSpinner', true);
        var lineItems = component.get('v.assessmentLineItems');
        for(var i = 0; i < lineItems.length; i++){
            //remove already applied reverse trending details in the system.
            lineItems[i].ReverseTrendingAffectedYear__c='';
            lineItems[i].ReverseTrendingCostRemoved__c=null;
            lineItems[i].ReverseTrendingAdjustedCost__c=null;
        }
        component.set("v.assessmentLineItems", lineItems);
        helper.saveReverseTrendingValues(component, false);
        component.set('v.reverseTrendingAlreadyApplied',false);
    }
})