({
    getAssetClassificationPicklists: function(component, event) {
        var action = component.get("c.fetchAssetClassificationPicklists");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var assetClassifications = [];
                for(var key in result) {
                    assetClassifications.push({key: key, value: result[key]});
                }
                component.set("v.assetClassifications", assetClassifications);
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    getSubAssetClassificationPicklists: function(component, event) {
        var action = component.get("c.fetchSubAssetClassificationPicklists");
        action.setParams({ 
            assetClassification : component.get('v.selectedClassification')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                var assetSubClassifications = [];
                if(!$A.util.isEmpty(results)){
                    for(var key in results){
                        assetSubClassifications.push({key: key, value: results[key]});
                    }
                }
                component.set("v.assetSubClassifications", assetSubClassifications);
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    getRelatedAssessmentLineItems : function(component) {
        var action = component.get("c.fetchRelatedAssessmentLineItems");
        action.setParams({ 
            caseId : component.get("v.recordId"),
            assetClassification : component.get('v.selectedClassification'),
            assetSubClassification : component.get('v.selectedSubClassification')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var results = response.getReturnValue();
                if(!$A.util.isUndefined(results) && !$A.util.isEmpty(results)){
                    component.set("v.assessmentLineItems",results);
                    component.find('toYear').set('v.value',results[0].AcquisitionYear__c);
                    component.find('fromYear').set('v.value',results[results.length-1].AcquisitionYear__c);
                    for (var i = results.length - 1; i >= 0; i--) {
                        var result = results[i].ReverseTrendingAffectedYear__c;
                        if(!$A.util.isEmpty(result)){
                            component.set('v.reverseTrendingAlreadyApplied',true);
                            break;
                        }
                    }
                } else {
                    component.set("v.assessmentLineItems",[]);
                }
            } else if(state === "ERROR"){
                this.showToast('Error','error','There is some issue in fetching details, Please try after some time.'); 
            }
            component.set('v.showSaveButton',false);
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    saveReverseTrendingValues : function(component, closeComponent) {
        var action = component.get("c.saveLineItemsWithReverseTrendingValues");
        action.setParams({ 
            assessmentLineItems : component.get('v.assessmentLineItems')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                this.showToast('Success','Success','Records are updated successfully.'); 
                if(closeComponent === true){
                    component.getEvent("closeModal").fire();
                } else {
                    this.getRelatedAssessmentLineItems(component);
                }
            } else if(state === "ERROR"){
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                this.showToast('Error','error','There is some issue in Saving details, Please contact System admin.'); 
                this.showToast('Error','error', message); 
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    calculateReverseTrending : function(component) {
        var action = component.get("c.fetchReverseTrendingDetails");
        action.setParams({ 
            caseId : component.get("v.recordId"),
            assetClassification : component.get('v.selectedClassification'),
            assetSubClassification : component.get('v.selectedSubClassification'),
            fromYear : component.find('fromYear').get('v.value'),
            toYear : component.find('toYear').get('v.value')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if(!$A.util.isUndefined(result) && !$A.util.isEmpty(result)){
                    component.set("v.assessmentLineItems",result);
                } else {
                    component.set("v.assessmentLineItems",[]);
                }
                component.set('v.showSaveButton',true); //Calculated, now ready to save.
            } else if(state === "ERROR"){
                this.showToast('Error','error','There is some issue in fetching details, Please try after some time.'); 
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    validateFields : function(component){
        var valid = component.find("assetClassificationPicklist").checkValidity();
        valid = valid && component.find("fromYear").checkValidity();
        valid = valid && component.find("toYear").checkValidity();
        if(!valid) this.showToast('Warning','Warning','Please provide correct input values.');
        var fromYear = component.find('fromYear').get('v.value');
        var toYear = component.find('toYear').get('v.value');
        if(fromYear>toYear) {
            valid = false;
            this.showToast('Warning','Warning','From Year should be less than or equal to To Year');
        }
        if($A.util.isEmpty(component.get("v.assessmentLineItems"))){
            valid = false;
            this.showToast('Warning','Warning','No Assessment Line Items to apply reverse trending');
        }
        return valid;
    },
    showToast : function(title,type,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title, 
            "message": message,
            "type":type
        });
        toastEvent.fire();
    }
})