/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        var addressType = component.get('v.addressType');       
        helper.dataLoad(component); 
        helper.getAddressMetadata(component, addressType);    
        component.set('v.showSpinner', false);         
    },
    closeQuickAction : function(component, event, helper) {
        component.set("v.isSaved" , true);
        component.set("v.showMailingAddressEntry" , false);
        component.set("v.showAccountingAddressEntry", false);        
        $A.get('e.force:closeQuickAction').fire();
        var addresses= component.get("v.addressValues");
        if(addresses.StreetNumber !== undefined && addresses.StreetNumber !== '' 
           && addresses.City !== undefined && addresses.City !== ''
           && addresses.State !== undefined && addresses.State !== '') {
            var addressEvent = component.getEvent("addressEvent");
            addressEvent.setParams({
                "isSaved" : true,
                "addressRemoved" : false,
                "addressType" : component.get('v.addressType')
            });
            addressEvent.fire();
        }else {
            
            var addressEvent = component.getEvent("addressEvent");
            addressEvent.setParams({
                "isSaved" : false,
                "addressRemoved" : true,
                "addressType" : component.get('v.addressType')
            });
            addressEvent.fire();
        }
    },
    clearSearch: function (component, event, helper) {
        component.set('v.uspsAddressInput', null);
        component.set('v.uspsZipInput', null);
        component.set('v.searchResult', null);
    },
    clearValidationResult: function (component, event, helper) {
        //component.set('v.uspsAddressInput', null);
        component.set('v.searchResult', null);
    },
    clearAddress : function(component, event, helper) {
        
        var addresses= component.get("v.addressValues");
        helper.clearAddressHelper(component, addresses); // Calling helper for Clearing Input data
        component.set('v.addressValues', addresses);
    },
    
    searchForCityState : function(component, event, helper) {
       
        component.set('v.showSpinner', true);       
        var addresses = component.get('v.addressValues');
        helper.searchForCityStateHelper(component,addresses);
	
    },
    
    /* If needed in future*****
    searchForZipCode: function (component, event, helper) {
        console.log('address search started');

        component.set('v.showSpinner', true);
        var addresses = component.get('v.addressValues');
        console.log('Addressvalues--Zip Code'+ JSON.stringify(addresses));
        var street = component.get('v.uspsAddressInput');
        helper.searchForZipCodeHelper(component,street,addresses); // Calling help for Zip Code Search

        //helper.setAddressInput(component);
        console.log('address search ended');
    }, */
    
    searchForAddress : function(component, event, helper) {
    
        component.set('v.showSpinner', true);
        var inputStreet = component.get('v.uspsAddressInput');
        var inputZipCode = component.get('v.uspsZipInput');
		helper.searchForAddressHelper( component, inputStreet,inputZipCode); // calling Helper for searching address input by user		
       
    },
    // Set the Validated Address from Result
    setValidationResult : function(component,event,helper){
     	component.set('v.showSpinner', true);
        helper.clearParseData(component);
   
	}
    
    
})