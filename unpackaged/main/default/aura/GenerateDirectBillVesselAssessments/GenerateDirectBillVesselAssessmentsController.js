/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	doInit : function(component, event, helper) {
		helper.generateDirectBillVesselAssessements(component,event);
	},
    confirmAction : function(component, event, helper) {
        helper.confirm(component,event);
    },
    cancel: function(component, event, helper) {
          $A.get("e.force:closeQuickAction").fire();
    }
})