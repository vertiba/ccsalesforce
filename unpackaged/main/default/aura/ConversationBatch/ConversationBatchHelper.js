/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	doInitHelper : function(component) {		
        var filters = component.get('v.filter');        
        if($A.util.isEmpty(filters)){
            filters['paginationOffset']= component.get('v.paginationOffset');
        }
       // var pageReference = component.get("v.pageReference");
       
        this.loadData(component);
        // Setting values to pass other component
        var objAttributeParam = component.get("v.objAttributeParam");
        component.set("v.jobName",objAttributeParam.jobName);
        component.set("v.jobId",objAttributeParam.jobId);
        component.set("v.displayName",objAttributeParam.displayName);
        component.set("v.selectedDate" , objAttributeParam.selectedDate);
        component.set("v.selectedStatus" , objAttributeParam.status);        
	},
    // To display details of Batch with all Stages
    displayBatchDetailsHelper : function(component,event) {
        var row = event.currentTarget.title;
        var jobName = component.get('v.jobName');     
        var jobId = component.get('v.jobId');
        var displayName = component.get('v.displayName');
        component.set('v.batchId',row);
        var filters = component.get('v.filter');
        var urlpath = 'batch/'+row+'/stages';
        filters['jobId']= jobId;
        filters['eventDate']= component.get("v.selectedDate");
        // Passing values to UI of Conversation Stage Component
        var objAttributes = {'jobName' : jobName };
        objAttributes['displayName']= displayName;
        objAttributes['batchId']= row;
        objAttributes['jobId']= jobId;
        objAttributes['status'] = component.get('v.selectedStatus');
        objAttributes['selectedDate'] = component.get('v.selectedDate');        
        this.gotoComponent(component, 'ConversationStage', filters,urlpath,objAttributes);        
    }, 
    displayExceptionsHelper : function(component,event) {
        var row = event.currentTarget.title;
        var jobId = component.get('v.jobId');
        var jobName = component.get('v.jobName');
        var displayName = component.get('v.displayName');
        var urlpath='failed';
        var filters = component.get('v.filter');
        filters['batchId']=row;
        filters['paginationOffset']=component.get('v.paginationOffset');
        filters['eventDate']= component.get('v.selectedDate');
        filters['jobName']= jobName;
        filters['jobId'] = jobId;
        filters['filterCriteria']={
            status : component.get('v.selectedStatus')
        };
        // values for UI to pass on to ConversationExceptionLog component 
        var objAttributes = {'jobName' :jobName };
        objAttributes['displayName']= displayName;
        objAttributes['batchId']= row;
        objAttributes['jobId']= jobId;
        objAttributes['stageId']='';
        objAttributes['stageName']= '';
        objAttributes['status'] = component.get('v.selectedStatus');
        objAttributes['selectedDate'] = component.get('v.selectedDate');        
        this.gotoComponent(component, 'ConversationExceptionLog', filters, urlpath, objAttributes);
    },
    // to handle status changes
    statusChangeHelper : function(component,event) {
        var status = component.find("selectedJobStatus").get('v.value');
        var filters = component.get('v.filter');
         filters['filterCriteria'] = {
            status : status
        };
        this.loadData(component);
    },
    // to handle date changes
    changeInDateHelper : function(component,event) {
        var selectedDate= component.get("v.selectedDate");
        var jobName = component.get("v.jobName");
        component.set('v.objectName','job/' +jobName +'/date/' + component.get('v.selectedDate'));
        this.loadData(component);   
    },
    // to handle navigation to next page 
    handleNextHelper : function(component,event) {
        var AttributeParam ={'paginationOffset' : component.get('v.paginationOffset')};
        AttributeParam['status']= component.get('v.selectedStatus');
        AttributeParam['objectName']= component.get('v.objectName');
        AttributeParam['selectedDate']= component.get('v.selectedDate');
        AttributeParam['pageNumber']= component.get('v.pageNumber');
        this.handleNextLoad(component, AttributeParam);        
    },
    // to handle navigation to previous page
    handlePreviousHelper : function(component,event) {
        var AttributeParam ={'paginationOffset' : component.get('v.paginationOffset')};
        AttributeParam['status']= component.get('v.selectedStatus');
        AttributeParam['objectName']= component.get('v.objectName');
        AttributeParam['selectedDate']= component.get('v.selectedDate');
        AttributeParam['pageNumber']= component.get('v.pageNumber');
        this.handlePreviousLoad(component, AttributeParam);
    }   
    
})