({
    getRecordsToDisplay: function (component) {
        var action = component.get("c.getCloseoutAssessment");
        var actParams = {
            'recordId': component.get('v.recordId')           
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
                if(results === null || results.caseRecord === undefined){
                    component.set('v.showSpinner', false);
                    sforce.one.showToast({
                        "title": "Error",
                        "message": "Invalid data",
                        "type" : "error"
                    });
                    sforce.console.getEnclosingTabId(
                        $A.getCallback(function(result) {
                            sforce.console.closeTab(result.id);
                            
                        })
                    );
                }else{
                    component.set('v.dataToDisplay', results); 
                   this.eligbleForApproval(component);
                    if(!component.get("v.noApplicableProcess")){                    
                        this.getSubTypePicklists(component);
                        
                    }
                }
                
            }else {
                var errors = response.getError();
                component.set('v.showSpinner', false);
                sforce.one.showToast({
                    "title": "Error",
                    "message": JSON.stringify(errors),
                    "type" : "error"
                });           
            }
        });       
        $A.enqueueAction(action);
    },
    eligbleForApproval : function(component) {
    var assessmentDataWrapper = component.get("v.dataToDisplay");
    var noApplicableProcess = false;
    if(assessmentDataWrapper[0].caseRecord.IsLocked__c || (assessmentDataWrapper[0].caseRecord.Status === 'In Progress' && assessmentDataWrapper[0].caseRecord.SubStatus__c === 'Pending Review')){
        noApplicableProcess =true;
    }else if(assessmentDataWrapper[0].caseRecord.Type ==='Regular' || assessmentDataWrapper[0].caseRecord.Type ==='Escape' ){
            if(assessmentDataWrapper[0].caseRecord.IntegrationStatus__c === 'Ready to Send to TTX' || 
                assessmentDataWrapper[0].caseRecord.IntegrationStatus__c ==='In Transit to TTX' ||
                assessmentDataWrapper[0].caseRecord.IntegrationStatus__c === 'Sent to TTX'){
                if(assessmentDataWrapper[0].recordTypeName == 'BPPAssessment'||assessmentDataWrapper[0].recordTypeName == 'VesselAssessment'){
                    noApplicableProcess =true;
            }
        }
    }
    if(noApplicableProcess== true){
        
        sforce.one.showToast({               
            "message": "No Applicable Approval Process Found",
            "type" : "Warning"
        });
        
        component.set('v.noApplicableProcess', true);
        sforce.console.getEnclosingTabId(
            $A.getCallback(function(result) {
                sforce.console.closeTab(result.id);
            })
        );
        
      }
    },
    getAdjustmentReasonPicklists: function(component, event) {
        var action = component.get("c.fetchAdjustmentReason");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var adjustmentReason = [];
                for(var key in result) {
                    adjustmentReason.push({key: key, value: result[key]});
                }
                component.set("v.adjustmentReason", adjustmentReason);
            }
             component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },    
    getSubTypePicklists: function(component) {
        var assessmentDataWrapper = component.get("v.dataToDisplay");
        var typeValue=assessmentDataWrapper[0].caseRecord.Type ;
        var recordTypeName = assessmentDataWrapper[0].caseRecord.RecordType.Name;
        var action = component.get("c.fetchSubType");
        action.setParams({ 
            'typeValue' : typeValue,
            'recordTypeName': recordTypeName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                var subType = [];
                if(!$A.util.isEmpty(results)){
                    for(var key in results){
                        subType.push({key: key, value: results[key]});
                    }
                }
                component.set("v.subType", subType);
            }
        });
        $A.enqueueAction(action);
    }    
})