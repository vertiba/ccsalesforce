/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    getFieldsToDisplay: function (component) {
        var action = component.get("c.getLightningComponentFields");
        var actParams = {
            'comparatorType': component.get("v.comparatorType")
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
                component.set('v.fields', results);
            } else {
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                this.showToast(component, 'ERROR', message, 'sticky', 'error');
            }
        });
        $A.enqueueAction(action);
    },
    getRecordsToDisplay: function (component) {
        var comparatorName = component.get('v.comparatorType');        
        var statementStatus = component.get("v.statementStatus");        
        var certifyLineItems = component.get("v.certifyLineItems");
        var submitted = component.get("v.submitted");
        var processed = component.get("v.processed");
        var action = component.get("c.getRecordsForDisplay");
        var actParams = {
            'recordId': component.get('v.recordId'),
            'comparatorType': component.get('v.comparatorType')
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != undefined && results != '' && results.length > 0) {       
                    component.set("v.noLineItems",false);
                    component.set("v.unSavedChangesExist", false);
                    // it is different in differnet component.
                    var costFieldName = 'This Year Cost';
                    if (comparatorName === 'StatementToStatementComparator' ||
                        comparatorName === 'LineItemToStatementComparator') {
                        costFieldName = 'This Year\'s Reported Cost';
                    } else if (comparatorName === 'StatementToLineItemComparator' ||
                        comparatorName === 'LineItemToLineItemComparator') {
                        costFieldName = 'This Year\'s Assessed Cost';
                        //costFieldName = 'Latest Reported Cost';
                    }
                    component.set("v.readOnlyRecord", results[0].isReadOnlyRecord);
                    for (var index in results) {
                        var readRecord = results[index]['readRecord'];
                        var editRecord = results[index]['editRecord'];
                        //read record is null or cost in read record should be empty and 
                        //Edit record should be there and has value in Cost__c
                        if (($A.util.isEmpty(editRecord) || $A.util.isEmpty(editRecord['Cost__c']))
                            && (!$A.util.isEmpty(readRecord) && !$A.util.isEmpty(readRecord['Cost__c']))) {
                            results[index][costFieldName] = readRecord['Cost__c'];
                            results[index]['isChanged'] = true;
                            results[index]['Style'] = 'highlight';
                            component.set('v.unSavedChangesExist', true);
                            if (comparatorName === 'LineItemToLineItemComparator') {
                                results[index]['Manual Adjusted Cost'] = null;
                                results[index]['This Year Description'] = "";
                            } else if (comparatorName === 'StatementToLineItemComparator') {
                                results[index]['Manual Adjusted Cost'] = null;
                            }
                        }
                        if (comparatorName === 'StatementToLineItemComparator' &&
                            $A.util.isEmpty(readRecord)) {
                            //Flag record for deletion as per ASR-6062
                            results[index]['toBeDeleted'] = true;
                        } else {
                            results[index]['toBeDeleted'] = false;
                        }
                    }
                    var fields = component.get("v.fields");
                    for (var i = 0; i < fields.length; i++) {
                        for (var j = 0; j < results.length; j++) {
                            results[j].nonchangedColumnNames.push(fields[i].Label__c);
                        }
                    }
                    component.set('v.dataToDisplay', results);
                    component.set('v.listAllData', results);                    
                    var dataToDisplay = component.get('v.dataToDisplay');                           
                    //ASR-1712 - need to hide CertifyNoLineItems checknox if already last year line items are there and make it false 
                    if (comparatorName === 'LineItemToStatementComparator') {
                        component.set("v.showCertifyLineItemsCb", false);
                        component.set("v.certifiedNoAssetLineItems",false);      
                        if(certifyLineItems) {
                            component.set("v.certifyLineItems", false);
                            this.updateStatement(component,component.get("v.recordId"),false,'certifiedNoAssetLineItems');
                        }                   
                    }                   
                }
                else {
                    //Added check for Part III, if statement is submitted, don't show buttons
                    if(!$A.util.isUndefined(statementStatus) 
                        && (statementStatus === submitted || statementStatus === processed) 
                        && comparatorName === 'LeasedLineItemToStatementComparator') {
                        component.set("v.readOnlyRecord",true); 
                    }
                    if (!$A.util.isUndefined(statementStatus)
                        && (statementStatus === submitted || statementStatus === processed) 
                        && comparatorName === 'LineItemToStatementComparator'
                        && certifyLineItems) {
                        component.set("v.readOnlyRecord", true);
                    }
                    //Added check for Part II, if statement is submitted, don't show buttons
                    component.set("v.noLineItems",true);                              
                    var noLineItems = component.get("v.noLineItems");
                    //ASR-1712 - need to make next button disbaled if there are no line items in part II 
                    //and user has not checked CertifyNoLineItems checknox
                    if (comparatorName === 'LineItemToStatementComparator') {
                        if(!certifyLineItems && noLineItems) {
                            component.set("v.unSavedChangesExist", true);
                        }                        
                    }             
                }
            } else {
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                this.showToast(component, 'ERROR', message, 'sticky', 'error');
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    assignstatementReportedAssetsRecordTypes: function (component) {
        var action = component.get("c.getstatementReportedAssetsRecordTypes");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != undefined && results != '') {
                    if (results.length > 0) {
                        component.set("v.recordTypeIdOwnedAssets", results[0]);
                        component.set("v.recordTypeIdLeasedAssets", results[1]);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveRecords: function (component, recordsToSave) {
        var action = component.get("c.saveUpdatedRecords");
        var actParams = {
            'recordId': component.get('v.recordId'),
            'recordsToSave': recordsToSave,
            'comparatorType': component.get('v.comparatorType')
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //As per client suggestion, page has to be refreshed,
                //Not just the component data.
                this.showToast(component, 'SUCCESS', 'Updated Records Successfully!', 'dismissible', 'success');
                if (component.get('v.isCommunityPage') === false) {
                    $A.get('e.force:refreshView').fire();
                } else {
                    //Repopulating the table and some data in parent component
                    //instead of refreshing the page for community
                    component.set('v.unSavedChangesExist', false);
                    this.getRecordsToDisplay(component);
                    component.getEvent("refreshDataEvent").fire();
                }
            } else {
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                component.set('v.showSpinner', false);
                this.showToast(component, 'ERROR', message, 'sticky', 'error');
            }
        });
        $A.enqueueAction(action);
    },
    getFormType: function (component) {
        var action = component.get("c.getFormTypeOfStatement");
        var actParams = {
            'recordId': component.get("v.recordId")
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
                component.set('v.formType', results);
            } else {
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                this.showToast(component, 'ERROR', message, 'sticky', 'error');
            }
        });
        $A.enqueueAction(action);
    },
    showToast: function (component, title, messageToShow, mode, typeofMsg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": mode,
            "type": typeofMsg,
            "title": title,
            duration:' 3000',
            mode: 'pester',
            "message": messageToShow
        });
        toastEvent.fire();
    },
    filterRecords: function (component) {
        var filterData = [];
        var term = component.get("v.filter");
        var listofAllData = component.get("v.listAllData");
        var dataToDisplay = component.get("v.listAllData");
        var comparatorName = component.get('v.comparatorType');
        var costFieldName = 'This Year Cost';
        var manualAdjustedCostFieldName = 'Manual Adjusted Cost';
        if (comparatorName === 'StatementToStatementComparator' ||
            comparatorName === 'LineItemToStatementComparator') {
            costFieldName = 'This Year\'s Reported Cost';
        } else if (comparatorName === 'StatementToLineItemComparator' ||
            comparatorName === 'LineItemToLineItemComparator') {
            costFieldName = 'This Year\'s Assessed Cost';
        }        
        if (!$A.util.isUndefined(term) && !$A.util.isEmpty(term)) {
            if (!$A.util.isUndefined(dataToDisplay) && !$A.util.isEmpty(dataToDisplay)) {
                var searchTerm;
                if (term.includes("\\")) {
                    var idx = term.indexOf("\\");
                    searchTerm = ((helper.addEscapeToSearch(idx, 0, "\\", term)).toLowerCase()).trim();
                } else {
                    searchTerm = (("" + term).toLowerCase()).trim();
                }
                filterData = dataToDisplay.filter(function (fn) {
                    var result = false;
                    if (!$A.util.isUndefined(fn.editRecord)) {
                        var data = ("" + JSON.stringify(Object.keys(fn.editRecord).map(function (key) { if (key === "Subcategory__c" || key === "AcquisitionYear__c") { return fn.editRecord[key]; } else { return '' } }))).toLowerCase();
                        var res = data.search(searchTerm);
                        if (res != -1) {
                            result = true;
                        }
                    } else if (!$A.util.isUndefined(fn.readRecord)) {
                        var data = ("" + JSON.stringify(Object.keys(fn.readRecord).map(function (key) { if (key === "Subcategory__c" || key === "AcquisitionYear__c") { return fn.readRecord[key]; } else { return '' } }))).toLowerCase();
                        var res = data.search(searchTerm);
                        if (res != -1) {
                            result = true;
                        }
                    }
                    return result;
                });               
                component.set("v.filterData", filterData);
                for (var index in filterData) {
                    var readRecord = filterData[index]['readRecord'];
                    var editRecord = filterData[index]['editRecord'];
                    //read record is null or cost in read record should be empty and 
                    //Edit record should be there and has value in Cost__c
                    if (($A.util.isEmpty(editRecord) || $A.util.isEmpty(editRecord['Cost__c']))
                        && (!$A.util.isEmpty(readRecord) && !$A.util.isEmpty(readRecord['Cost__c']))) {
                        filterData[index][costFieldName] = readRecord['Cost__c'];
                        filterData[index]['isChanged'] = true;
                        component.set('v.unSavedChangesExist', true);
                    }                    
                    if (comparatorName === 'StatementToLineItemComparator' &&
                        $A.util.isEmpty(readRecord)) {
                        //Flag record for deletion as per ASR-6062
                        filterData[index]['toBeDeleted'] = true;
                    } else {
                        filterData[index]['toBeDeleted'] = false;
                    }
                }
                component.set("v.dataToDisplay", filterData);                
            }
        }
        else {           
            component.set("v.filterData", null);
            component.set("v.dataToDisplay", listofAllData);
        }
    },
    updateStatement: function (component,statementId,certifiedFieldValue,fieldToUpdate) {
        debugger;
        if (fieldToUpdate === 'certifiedNoAssetLineItems'){
            //ASR-1712 - need to update CertifyNoLineItems checknox value on statement if user check/uncheck it
            var action = component.get("c.updateStatementCertifiedNoAssetLineItemsCB");
            var actParams = {
                'statementId': statementId,
                'certifiedNoAssetLineItems': certifiedFieldValue
            }
            action.setParams(actParams);
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    this.showToast(component, 'SUCCESS', 'Updated record successfully', 'sticky', 'Success');
                }
                else {
                    component.set("v.unSavedChangesExist", true);
                    let errors = response.getError();
                    let message = 'Unknown error';
                    if (errors && Array.isArray(errors) && errors.length > 0) {
                        message = errors[0].message;
                    }
                    this.showToast(component, 'ERROR', message, 'sticky', 'error');
                }
                component.set('v.showSpinner', false);
                component.getEvent("refreshDataEvent").fire();
            });
            $A.enqueueAction(action);
        }
        else if (fieldToUpdate === 'noPropertyBelongingtoOthers'){
            //ASR-1712 - need to update CertifyNoLineItems checknox value on statement if user check/uncheck it
            var action = component.get("c.updateStatementNoPropertyBelongingtoOthersCB");
            var actParams = {
                'statementId': statementId,
                'noPropertyBelongingtoOthers': certifiedFieldValue
            }
            action.setParams(actParams);
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    this.showToast(component, 'SUCCESS', 'Updated record successfully', 'sticky', 'Success');
                }
                else {
                    component.set("v.unSavedChangesExist", true);
                    let errors = response.getError();
                    let message = 'Unknown error';
                    if (errors && Array.isArray(errors) && errors.length > 0) {
                        message = errors[0].message;
                    }
                    this.showToast(component, 'ERROR', message, 'sticky', 'error');
                }
                component.set('v.showSpinner', false);
                component.getEvent("refreshDataEvent").fire();
            });
            $A.enqueueAction(action);
        }
    }
})