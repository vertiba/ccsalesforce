/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        helper.fetchFactors(component, event, helper);
    },
    closeQuickAction : function(component, event, helper) {
        var closeQuickAction = $A.get('e.force:closeQuickAction');
        if(closeQuickAction) closeQuickAction.fire();
    },
    submit : function(component, event, helper) {
        helper.updateLineItemFactor(component, event, helper);
    }
})