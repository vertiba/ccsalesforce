/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    fetchFactors : function(component, event, helper) {
        var action = component.get('c.getMatchingFactors');
        var params = {
            assessmentLineItemId : component.get('v.recordId')
        };
        action.setParams(params);
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set('v.matchingFactorsResult', results);
            } else {
                console.log('error');
                console.log(response.getError());

                let errors = response.getError();
                let message = 'Unknown error'; // Default error message

                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message + '\n' + errors[0].stackTrace;
                }

                // Display the message
                console.error(message);

                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    message : message,
                    title   : 'Error!',
                    type    : 'error'
                });
                toastEvent.fire();
            }
        });

        $A.enqueueAction(action);
    },
    updateLineItemFactor : function(component, event, helper) {
        var action = component.get('c.setFactor');
        var params = {
            assessmentLineItemId : component.get('v.recordId'),
            factorId             : component.get('v.record').DepreciationFactor__c
        };
        action.setParams(params);
        action.setCallback(this, function (response) {
            if(response.getState() === 'SUCCESS') {
                var toastEvent = $A.get('e.force:showToast');

                var message = 'Depreciation factor successfully updated';
                toastEvent.setParams({
                    message : message,
                    title   : 'Success!',
                    type    : 'success'
                });
                toastEvent.fire();

                var closeQuickAction = $A.get('e.force:closeQuickAction');
                if(closeQuickAction) closeQuickAction.fire();

                $A.get('e.force:refreshView').fire();
            } else {
                console.log('error');
                console.log(response.getError());

                let errors = response.getError();
                let message = 'Unknown error'; // Default error message

                // Retrieve the error message sent by the server
                if(errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message + '\n' + errors[0].stackTrace;
                }

                // Display the message
                console.error(message);

                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    message: message,
                    title: 'Error!',
                    type: 'error'
                });
                toastEvent.fire();
            }
        });

        $A.enqueueAction(action);
    }
})