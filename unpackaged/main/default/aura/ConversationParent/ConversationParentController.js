/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
	displayBatches: function (component, event, helper) {
        var fil = component.get('v.filter');
        if($A.util.isEmpty(fil)){
            //component.set('v.filter',{});
        } else{
            //removing filters, that have been added in between. So that initial filters alone
            //are applied. If there are more filters, then another option is to create a pageName  
            //attribute, hold its value and redirect to that page.
            delete fil.stageId;
            delete fil.batchId;
            delete fil.pageNumber;
        }
        helper.gotoComponent(component, 'ConversationBatch', fil);
    }
})