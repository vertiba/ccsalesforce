/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    // to Load Data from Server
    loadData : function(component) {
        this.toggleSpinner(component);
        var action = component.get("c.conversationCallString");
        var extName = component.get('v.objectName');
        var actParams = {
            'extName': extName,
            'filterCondition':component.get('v.filter')
        }
        action.setParams(actParams);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                var resultMap =response.getReturnValue();
                if(resultMap!=undefined && resultMap!=''){
                    var resultLst = JSON.parse(resultMap);
                    if(!$A.util.isUndefined(resultLst.response)) {
                        var serverMessage = JSON.parse(resultLst.response);
                        this.showToast(component, 'Information', serverMessage.response,'dismissible','info');                      
                    }  
                   else if(resultLst!=undefined && resultLst.length != 0){
                        component.set('v.data', resultLst);
                        this.showToast(component, 'Success', resultLst.length +' Records Fetched','dismissible','success');                        
                    } else {
                        this.showToast(component, 'Warning', $A.get("$Label.c.Conversation_Monitoring_No_Records_to_Display"),'pester','warning');
                    }
                    if(resultLst.length >= 10){
                        component.set('v.disableShowNext',false);
                    }else{
                        component.set('v.disableShowNext',true);
                    }
                }
            } else {
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                this.showToast(component, 'ERROR', message,'sticky', 'error');
            }
            this.toggleSpinner(component);
        });
        $A.enqueueAction(action);  
    },
    // to handle Spinner
    toggleSpinner : function(component) {
        var spinner = component.find("spinner");
        $A.util.toggleClass(spinner, "slds-hide");
        $A.util.toggleClass(spinner, "slds-show");
    },
    // to open other Component 
    gotoComponent:function(component, cmpName,filtercondition,objectName,objAttribute){
        var navService = component.find("navService");
        var pageReference = {
            
            type: "standard__component",
            attributes: {
                componentName: 'c__' + cmpName    
            },    
            state: {
                filter : filtercondition,
                objectName: objectName,
                objAttributeParam : objAttribute   
            }
        };        
        component.set("v.pageReference", pageReference);        
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            var workspaceAPI = component.find("workspace");
            workspaceAPI.openTab({
                pageReference : pageReference, 
                url: url,
                focus: true
            }).then(function(response) {
                var lbl = cmpName;
                if(cmpName =='ConversationBatch'){
                    lbl = 'Job Details'
                }else if (cmpName =='ConversationStage'){
                    lbl ='Stage Details'
                }else if(cmpName =='ConversationExceptionLog'){
                    lbl ='Failure'
                }
                workspaceAPI.setTabLabel({
                    tabId: response,
                    label: lbl
                });
            }).catch(function(error) {
                console.log(error);
            });
        }));         
      
    },
    // to Display Toast
    showToast : function(component, title, messageToShow, mode, typeofMsg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": mode,
            "type": typeofMsg,
            "title": title,
            "message": messageToShow
        });
        toastEvent.fire();
    },    
    // Handle navigation to Next
    handleNextLoad : function(component,objAttribute){
        component.set('v.data',[]);
        var objAttribute = objAttribute;
        var paginationOffsetCounter= 10;
        var filter = component.get('v.filter');
        filter['paginationOffset'] = objAttribute.paginationOffset + paginationOffsetCounter;
        filter['filterCriteria'] = {'status': objAttribute.status};
        component.set('v.selectedDate',objAttribute.selectedDate);
        component.set('v.objectName',objAttribute.objectName);
        this.loadData(component);
        component.set('v.paginationOffset',filter['paginationOffset']);
        component.set('v.pageNumber',component.get('v.pageNumber')+1);
        
    },
    // to Handle navigation to Previous
    handlePreviousLoad : function(component,objAttribute){
        component.set('v.data',[]);
        var objAttribute = objAttribute;
        var paginationOffsetCounter= 10;
        var filter = component.get('v.filter');
        filter['paginationOffset']=objAttribute.paginationOffset - paginationOffsetCounter;
        filter['filterCriteria']={'status': objAttribute.status};
        component.set('v.selectedDate',objAttribute.selectedDate);
        component.set('v.objectName',objAttribute.objectName);
        this.loadData(component);
        component.set('v.paginationOffset',filter['paginationOffset']);
        var pageNb = component.get('v.pageNumber');
        if(pageNb !== 1){
            component.set('v.pageNumber',component.get('v.pageNumber')-1);
        }        
    }
})