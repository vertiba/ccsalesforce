/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit: function (component, event, helper) {
        component.set('v.showSpinner', true);
        helper.getRecordsToDisplay(component); 
        helper.getAdjustmentReasonPicklists(component);
  },
    handleSubmit: function (component, event, helper) {
        component.set('v.showSpinner', true);
        var assessmentDataWrapper = component.get("v.dataToDisplay");
        var action = component.get("c.updateAndSubmit");
        var actParams = {
            'assessmentDataWrapper': JSON.stringify(assessmentDataWrapper)
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var results = response.getReturnValue();
                component.set('v.dataToDisplay', results);
                component.set('v.showSpinner', false);
                if (results != 'System Error. Please contact your Organisation Admin for it') {
                    sforce.one.showToast({
                        "title": "Success",
                        "message": "Submitted For Approval",
                        "type": "Success"
                    });
                    sforce.console.getEnclosingTabId(
                        $A.getCallback(function (result) {
                            sforce.console.closeTab(result.id);

                        })
                    );
                } else {
                    sforce.one.showToast({
                        "title": "ERROR",
                        "message": "No Applicable Approval Process Found",
                        "type": "Warning"
                    });
                    sforce.console.getEnclosingTabId(
                        $A.getCallback(function (result) {
                            sforce.console.closeTab(result.id);

                        })
                    );
                }
            } else {
                var errors = response.getError();
                console.log('errors+++'+JSON.stringify(errors));
                console.log('errors[0]+++'+errors[0]);
                console.log('errors[0]+++'+errors[0].message);
                //If error is due to adjustment reason is required, don't close the tab and user should be on screen to correct the data
                if(errors[0].message === $A.get("$Label.c.RequiredAdjustmentReasonMsg")){
                    component.set('v.showSpinner', false);
                    sforce.one.showToast({
                        "title": "Error",
                        "message": JSON.stringify(errors[0].message),
                        "type": "error"
                    });
                }
                //If error is due to other reason, close the tab and display the error in the toast message
                else {
                    component.set('v.showSpinner', false);
                    sforce.one.showToast({
                        "title": "Error",
                        "message": JSON.stringify(errors[0].message),
                        "type": "error"
                    });
                    sforce.console.getEnclosingTabId(
                        $A.getCallback(function (result) {
                            sforce.console.closeTab(result.id);
                        })
                    );
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    handleAdjustmentReasonChange : function(component, event, helper) {
        var assessmentDataWrapper = component.get("v.dataToDisplay");
        var selPickListValue = component.find("adjustmentReasonPicklist").get("v.value");
        assessmentDataWrapper.caseRecord.AdjustmentReason__c = selPickListValue;
        for(var i=0 ; i<assessmentDataWrapper.length;i++ ) {
            for(j=0;j<assessmentDataWrapper[i].length;j++) {
                assessmentDataWrapper[i].escapeRollCorrection[j].AdjustmentReason__c = selPickListValue;
            }
        }
        component.set('v.dataToDisplay', assessmentDataWrapper);
        
    },
    handleSubTypeChange : function(component, event, helper) {
        var assessmentDataWrapper = component.get("v.dataToDisplay");
        var selPickListValue = component.find("subTypePicklist").get("v.value");
        assessmentDataWrapper.caseRecord.SubType__c = selPickListValue;        
        for(var i=0 ; i<assessmentDataWrapper.length;i++ ) {
            for(j=0;j<assessmentDataWrapper[i].length;j++) {
                assessmentDataWrapper[i].escapeRollCorrection[j].SubType__c = selPickListValue;
            }
        }
        component.set('v.dataToDisplay', assessmentDataWrapper);
    },
    handleCancel : function(component, event, helper){
        helper.doCancel(component);        
    }
})