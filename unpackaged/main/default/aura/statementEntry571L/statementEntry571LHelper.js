/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    backHelper: function (component) {
        component.set("v.disableNext", false);
        var currentStep = Number(component.get("v.currentStep") - 1);
        component.set("v.currentStep", currentStep);
        var disableBack = component.get("v.disableBack");
        
        if (currentStep == 1) {
            disableBack = true;
        } else if(currentStep == 2){
            //ASR-9825 Changes Starts
            component.find("recordData").reloadRecord(true);
            var ownLandatBusinessLocation = component.get("v.record.OwnLandatBusinessLocation__c");
            component.set("v.ownLandatBusinessLocation", ownLandatBusinessLocation);
            var controllingInterest = component.get("v.record.ControllingInterest__c");
            component.set("v.controllingInterest", controllingInterest);
            var realPropertyAtTimeOfAcquisition = component.get("v.record.RealPropertyAtTimeOfAcquisition__c");
            component.set("v.realPropertyAtTimeOfAcquisition", realPropertyAtTimeOfAcquisition);
            //ASR-9825 Changes Ends
        }
        else {
            disableBack = false;
        }
        component.set("v.disableBack", disableBack);
    },
    //this method sets fields as required or not based on the parameter passed
    setRequired : function(component,fieldName,required){
        let field = component.find(fieldName);
        if(typeof(field) != 'undefined'){
           field.set('v.required',required);
        }
    },
    nextHelper: function (component,event) {
        component.find("recordData").reloadRecord(true);
        //ASR-9825 Changes Starts
        var ownLandatBusinessLocation = component.get("v.record.OwnLandatBusinessLocation__c");
        component.set("v.ownLandatBusinessLocation", ownLandatBusinessLocation);
        var controllingInterest = component.get("v.record.ControllingInterest__c");
        component.set("v.controllingInterest", controllingInterest);
        var realPropertyAtTimeOfAcquisition = component.get("v.record.RealPropertyAtTimeOfAcquisition__c");
        component.set("v.realPropertyAtTimeOfAcquisition", realPropertyAtTimeOfAcquisition);
        //ASR-9825 Changes Ends
        var currentStep = Number(component.get("v.currentStep"));
        var record = component.get("v.record");
        var status =  record.Status__c;
        if (!$A.util.isUndefinedOrNull(status) && status === 'In Progress') {
            var accountingContactNumber;
            if (!$A.util.isUndefinedOrNull(component.find("accountingContactNumber"))) {
                accountingContactNumber = component.find("accountingContactNumber").get("v.value");
            }
            //ASR-10664 default Telephone Number (PreparersPhoneNumber_c) to BusinessPhone_c
            var businessPhone;
            if (!$A.util.isUndefinedOrNull(component.find("businessPhone"))) {
                businessPhone = component.find("businessPhone").get("v.value");
            }
            if (!$A.util.isUndefinedOrNull(businessPhone)
                && ($A.util.isUndefinedOrNull(component.get("v.localTelephoneNumber"))
                    || businessPhone !== component.get("v.localTelephoneNumber"))) {
                component.set("v.localTelephoneNumber", businessPhone);
            }
        }
        
        var message;
        var isSaved = false;
        if (currentStep == 1) {
            if (component.get("v.isMailingAddressSaved")) {
                isSaved = true;
            } else {
                if (component.get("v.record.MailingAddress__c ") != null) {
                    isSaved = true;
                }
                message = "Mailing Address is missing";
            }
         	if(component.get("v.record.BusinessEmail__c") != undefined || component.get("v.record.BusinessEmail__c") != null)  {
                component.set('v.loggedInUserEmail',component.get("v.record.BusinessEmail__c"));
            }
        } else if (currentStep == 2) {
            if (component.get("v.isAccountingAddressSaved")) {
                isSaved = true;
            }else if(component.get("v.record.AccountingAddress__c") != null) {
                 isSaved = true;
            } else {
                message = 'Enter location of general ledger and all related accounting records (include zip code) is required.'
            }
        } else {
            isSaved = true;
        }
        
        if (!isSaved) {
            this.showToast(
                "Error",
                "error",
                message,
                "sticky"
            );
            return;
        }
        component.set("v.disableBack", false);
        
        var saveExecuted = false;
        /* Validate any required fields */
        var inputFields = component.find("inputField");
        if (!Array.isArray(inputFields)) {
            inputFields = [inputFields];
        }

        var steps = component.get("v.steps");
        var lastStep = Number(steps[steps.length - 1]);
        var disableNext = component.get("v.disableNext");
        if (currentStep == lastStep) disableNext = true;
        else disableNext = false;
        component.set("v.disableNext", disableNext);
        currentStep++;
        component.set("v.currentStep", currentStep);
        
        //ASR-7869 - calling this method to get Statement Reported Assets and their Child Reported Asset Schdeules Records to show on Schedule-D
        this.retrieveStatementReportedAssetsData(component, event);
    },    
    submitHelper: function (component, event) {
        event.preventDefault();
        var fields = event.getParam("fields");
        var record = component.get("v.record");
        if (
            record.Form__c == "571-L - Biopharma" &&
            record.Property__r.SubmittedBiotechCertification__c != "Yes"
        ) {
            if (
                $A.util.isEmpty(
                    component.get("v.record").SignatureBioTechCertificate__c
                )
            ) {
                this.showToast(
                    "Waring",
                    "Warning",
                    "Please provide Signature for BioTech Certificate",
                    "sticky"
                );
                return false;
            }
        }
        if ($A.util.isEmpty(component.get("v.record").Signature__c) && component.get("v.loggedInUserProfile") === 'Taxpayer') {
            return false;
        }
        fields.SignatureBioTechCertificate__c = component.get(
            "v.record"
        ).SignatureBioTechCertificate__c;        
        fields.Signature__c = component.get("v.record").Signature__c;
        fields.Status__c = "Submitted";
        var now = new Date();
        var formatedDateTime = now.toISOString();
        fields.VIP_Date_Time_Submitted__c = formatedDateTime;
        /* Till now all the forms are submitted in each step. Submitting final step. */
        component.set("v.underSubmition",true);
    	component.set("v.showSpinner", true);
        component.find("finalEditForm").submit(fields);
    },
    submitPrintHelper: function (component, event) {
        event.preventDefault();
        var fields = event.getParam("fields");
        fields.SignatureBioTechCertificate__c = component.get(
            "v.record"
        ).SignatureBioTechCertificate__c;        
        fields.Signature__c = component.get("v.record").Signature__c;
        
        /* it saves the final page content and then in PDF it will be shown */
        component.find("finalEditForm").submit(fields);
    },
    showToast: function (title, type, message, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            type: type,
            mode: mode,
            duration:' 3000',
            mode: 'pester',
            message: message
        });
        toastEvent.fire();
    },
    // Check if already document is loaded for LeasedEquipement
    statementHasFiles: function (component) {
        var action = component.get("c.getcontentDocumentLinks");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.disableNextForUploader", !returnValue);
                component.set("v.afterUpload", !returnValue);
            }
        });
        $A.enqueueAction(action);
    },
    // return recordTypeName of Property 
    returnPropertyRecordType: function (component) {
        var action = component.get("c.getStatementRecordType");
        action.setParams({
            'recordId': component.get('v.recordId')
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if (returnValue != null && returnValue != undefined) {
                    if (returnValue == 'LeasedEquipment') {
                        this.statementHasFiles(component);
                    } else {
                        component.set('v.disableNextForUploader', false);
                    }
                }                
            }
        });
        $A.enqueueAction(action);
    },
    fileUploader: function (component, event) {
        var uploadFinished = event.getParam("UploadFinished");
        var disableNextForUploader = component.get('v.disableNextForUploader');
        if (!(!$A.util.isUndefined(uploadFinished)) && uploadFinished == false) {
            component.set('v.disableNextForUploader', true);
        } else {
            component.set('v.disableNextForUploader', false);
        }
    },
    retrieveStatementReportedAssetsData: function (component, event) {   
        //ASR-7869 - Get Statement Reported Assets and their Child Reported Asset Schdeules Records to show on Schedule-D             
        var action = component.get("c.getStatementReportedAssets");
        var actParams = {
            'recordId': component.get('v.recordId')            
        }
        action.setParams(actParams);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();                
                if (results != undefined) {                          
                    var isUnMatched = false;
                    for (var i = 0; i < results.length; i++) {                        
                        if (!results[i].IsMatchedAmount__c) {
                            isUnMatched = true;
                            break;
                        }
                    }                   
                    if (isUnMatched) {
                        component.set("v.scheduleDHasUnsavedChanges", true);
                    } else {
                        component.set("v.scheduleDHasUnsavedChanges", false);
                    }
                    component.set("v.statementReportedAssetsData", results);
                }                
            }            
        });
        $A.enqueueAction(action);
    },
    returnOnSubmitRedirection: function (component, event) {
		//This method will redirect the user to the page based on boolean value check.
        var action = component.get("c.getTaxPayerInfo");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if (returnValue) {
                    //Redirecting to Certificate page
                    var recordID = component.get("v.recordId");
                    var commURL = window.location.origin;
                    var myPDFpage = commURL+'/sfassessor/CertificateConfirmation?recordId='+recordID;	
                    var pdfObj = document.createElement("a");
                    pdfObj.href = myPDFpage;
                    pdfObj.click();
                } else {
                    console.log('refresh');
                    $A.get('e.force:refreshView').fire();
                }
            } 
        });
        $A.enqueueAction(action);        
    },
    
    //ASR-8469 - this function get called when user clicks on Print PDF
    print: function (component, event) {
		//This method will redirect the user to the page based on boolean value check.
        var action = component.get("c.getTaxPayerInfo");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                var recordID = component.get("v.recordId");
                var inputURL = window.location.origin;
                var myPDFpage ='';
                if (returnValue) {
                    //Redirecting to Print page when in Community
                    myPDFpage = inputURL+'/sfassessor/PrintPDF571L?recordId='+recordID;	
                } else {
                    //Redirecting to Print page when in Smart
                    myPDFpage = inputURL+'/apex/PrintPDF571L?recordId='+recordID;
                }
                var pdfObj = document.createElement("a");
                pdfObj.href = myPDFpage;
                pdfObj.target="_blank";
                pdfObj.click();
            } 
        });
        $A.enqueueAction(action);        
    },
    retrieveTaxPayerInformation: function (component, event) {
        var action = component.get("c.retrieveTaxPayerInfo");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result+++'+JSON.stringify(result));
                component.set('v.loggedInUserEmail',result.Email);
                component.set('v.loggedInUserProfile',result.Profile.Name);
            } 
        });
        $A.enqueueAction(action);
    },
    retrieveDefaultAcquistionYear: function (component, event) {
        var action = component.get("c.getDefaultAcquistionYear");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.defaultAcqYear",result);
            }
        });
        $A.enqueueAction(action);
    },
    loaddata: function (component, event)
    { 
        component.set("v.showSpinner", true);
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.showSpinner", false);
            }), 5000
        );
    }
});