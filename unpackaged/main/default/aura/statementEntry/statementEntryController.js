/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    doInit : function(component, event, helper) {
        component.set('v.currentYear', new Date().getFullYear());
        component.set('v.showLocationAddressEntry', false);
        component.set('v.showMailingAddressEntry', false);
    },
    back : function(component, event, helper) {
        component.set('v.disableNext', false);

        var currentStep = Number(component.get('v.currentStep'));
        console.log('currentStep starts as=' + currentStep);
        currentStep--
        console.log('currentStep now=' + currentStep);
        component.set('v.currentStep', currentStep);
        //TODO cleanup/consolidate/improve setting of form step
        var record = component.get('v.record');
        //record.FormStep__c = currentStep.toString();
        component.set('v.record', record);


        var disableBack = component.get('v.disableBack');
        if(currentStep == 1) disableBack = true;
        else disableBack = false;
        component.set('v.disableBack', disableBack);
    },
    next : function(component, event, helper) {
        component.set('v.disableBack', false);

        var saveExecuted = false;
        /* Validate any required fields */
        var inputFields = component.find('inputField');
        if(!Array.isArray(inputFields)) {
            inputFields = [inputFields];
        }
        console.log('inputFields=' + inputFields);
        for(var i=0; i < inputFields.length; i++) {
            /* var checkValidity = inputFields[i].checkValidity;
            console.log('checkValidity=' + checkValidity);
            inputFields[i].showHelpMessageIfInvalid(); */
        }

        /* Submit any forms */
        /* var editForms = component.find('editForm');
        if(!Array.isArray(editForms)) {
            editForms = [editForms];
        }
        for(var i=0; i < editForms.length; i++) {
            saveExecuted = true;
            editForms[i].submit();
        }
        if(saveExecuted) {
            console.log('save complete, refreshing view');
            component.find('recordData').reloadRecord(true);
            $A.get('e.force:refreshView').fire();
        } */

        var currentStep = Number(component.get('v.currentStep'));
        console.log('currentStep starts as=' + currentStep);
        currentStep++;
        console.log('currentStep now=' + currentStep);
        component.set('v.currentStep', currentStep);
        //TODO cleanup/consolidate/improve setting of form step
        // var record = component.get('v.record');
        // //record.FormStep__c = currentStep.toString();
        // component.set('v.record', record);

        var steps = component.get('v.steps');
        var lastStep = Number(steps[steps.length - 1]);
        var disableNext = component.get('v.disableNext');
        if(currentStep == lastStep) disableNext = true;
        else disableNext = false;
        component.set('v.disableNext', disableNext);
    },
    handleFormError: function (component, event) {
        var errorResult = JSON.parse(JSON.stringify(event.getParams()));
        console.log('onerror: ', errorResult);
    },
    setStep : function(component, event, helper) {
        //component.set('v.currentStep', event.srcElement.name);
    },
    editMailingAddress : function(component, event, helper) {
        component.set('v.showMailingAddressEntry', true);
    },
    submit : function(component, event, helper) {
        var record = component.get('v.record');
        record.Status__c = 'Submitted';
        component.set('v.record', record);

        /* console.log('current status');
        console.log(component.find('status').get('v.value'));
        component.find('status').set('v.value', 'Submitted');
        console.log('new status');
        console.log(component.find('status').get('v.value')); */



        /* Submit any forms */
        var editForms = component.find('editForm');
        console.log('found ' + editForms.length + ' editForms');
        console.log(editForms);

        var saveExecuted = false;
        if(!Array.isArray(editForms)) {
            editForms = [editForms];
        }
        for(var i=0; i < editForms.length; i++) {
            saveExecuted = true;
            editForms[i].submit();
        }
        console.log('saveExecuted=' + saveExecuted);
        if(saveExecuted) {
            var toastEvent = $A.get('e.force:showToast');
            toastEvent.setParams({
                message: 'Statement successfully submitted',
                mode: 'sticky',
                title: 'Success!',
                type: 'success'
            });
            toastEvent.fire();

            console.log('save complete, refreshing view');
            $A.get('e.force:refreshView').fire();
        }
    }
})