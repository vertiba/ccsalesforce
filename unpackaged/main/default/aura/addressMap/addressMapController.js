/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    handleRecordUpdated : function (component, event, helper) {
        var record = component.get('v.record');
        component.set('v.mapMarkers', [
            {
                location: {
                    Street     : record.LocationStreetLine__c,
                    City       : record.LocationCity__c,
                    State      : record.LocationState__c == null ? '' : record.LocationState__c.split('-')[1],
                    PostalCode : record.LocationPostalCode__c,
                    Country    : record.LocationCountry__c
                },
                title: record.Name
            }
        ]);
    }
})