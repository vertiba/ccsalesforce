/*Threat Intelligence Passphrase: 9fbef606107a605d69c0edbcd8029e5d*/
({
    toggleModal : function(component, event, helper) {
		var showModal = component.get('v.showModal');
        if(showModal === true){
            component.set("v.costAdjustmentType",'');
            component.set("v.showModal",false);
        } else {
            component.set("v.showModal",true);
            component.set("v.costAdjustmentType",event.getSource().get("v.label"));
        }
    }
})